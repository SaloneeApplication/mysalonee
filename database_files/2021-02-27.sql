-- phpMyAdmin SQL Dump
-- version 5.0.4deb2~bpo10+1+bionic1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 27, 2021 at 01:34 PM
-- Server version: 5.7.33-0ubuntu0.18.04.1
-- PHP Version: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_saloon`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` bigint(15) NOT NULL,
  `password` text NOT NULL,
  `image` text NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `email`, `mobile`, `password`, `image`, `status`) VALUES
(1, 'Administrator123123', 'admin@gmail.com', 9879872211, 'admin@123', 'uploads/admin/unnamed (1).jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_provider_branch_id` int(11) NOT NULL,
  `service_provider_service_id` int(11) NOT NULL,
  `service_provider_slot_id` int(11) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `short_description` text,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` datetime DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `image`, `short_description`, `created_time`, `modified_time`, `status`) VALUES
(1, 'Mens Hair Cut', 'uploads/categories/1610759837.png', 'Hair Style', '2021-01-18 15:29:00', '2021-02-20 09:51:07', 0),
(2, 'Facial', 'uploads/categories/1610759837.png', 'Facial', '2021-01-18 15:29:00', '2021-02-20 09:51:10', 0),
(3, 'Womens Hair Cut', 'uploads/categories/1610759837.png', 'Womens Hair Cut', '2021-01-18 19:17:41', '2021-02-20 09:50:59', 0),
(4, 'Hair Color', 'uploads/categories/1610759837.png', 'Herbal Hair Color', '2021-01-18 19:18:59', '2021-02-21 11:14:20', 1),
(5, 'Manicure', 'uploads/categories/1610759837.png', 'Manicure at home', '2021-01-18 19:19:53', '2021-02-20 09:51:03', 0),
(6, 'Facial', 'uploads/categories/download (2).jpg', 'Smart Facial', '2021-02-20 07:37:41', '2021-02-20 09:50:47', 1),
(7, 'Hair Cut', 'uploads/categories/images.jpg', 'Its smart Hair Cutting', '2021-02-20 07:38:45', '2021-02-27 07:47:53', 1),
(8, 'Test', 'uploads/categories/Capture.PNG', 'test', '2021-02-21 05:45:06', '2021-02-25 16:56:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `city_name_en` varchar(355) CHARACTER SET utf8 NOT NULL,
  `city_name_ar` varchar(355) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name_en`, `city_name_ar`, `created_at`) VALUES
(1, 'Abha', 'أبها', '2020-12-18 17:18:51'),
(2, 'Abqaiq', 'ابقيق', '2020-12-18 17:18:51'),
(3, 'Abu Earish', 'أبو عريش', '2020-12-18 17:18:51'),
(4, 'Adam', 'اضم', '2020-12-18 17:18:51'),
(5, 'Afif', 'عفيف', '2020-12-18 17:18:51'),
(6, 'Ahad Rafidah', 'أحد رفيدة', '2020-12-18 17:18:51'),
(7, 'Ahd almusaraha', 'احد المسارحة', '2020-12-18 17:18:51'),
(8, 'Al Adid', 'العديد', '2020-12-18 17:18:51'),
(9, 'Al Ahsa', 'الأحساء', '2020-12-18 17:18:51'),
(10, 'Al Aqiq', 'العقيق', '2020-12-18 17:18:51'),
(11, 'Al Badayea', 'البدائع', '2020-12-18 17:18:51'),
(12, 'Al Birk', 'البرك', '2020-12-18 17:18:51'),
(13, 'Al Bukayriyah', 'البكيرية', '2020-12-18 17:18:51'),
(14, 'Al Ghazala', 'الغزالة', '2020-12-18 17:18:51'),
(15, 'Al Hait', 'الحائط', '2020-12-18 17:18:51'),
(16, 'Al Hinakiyah', 'الحناكية', '2020-12-18 17:18:51'),
(17, 'Al Jumum', 'الجموم', '2020-12-18 17:18:51'),
(18, 'Al Kamil', 'الكامل', '2020-12-18 17:18:51'),
(19, 'Al Lith', 'الليث', '2020-12-18 17:18:51'),
(20, 'Al Majaridah', 'المجاردة', '2020-12-18 17:18:51'),
(21, 'Al Majma\'ah', 'المجمعة', '2020-12-18 17:18:51'),
(22, 'Al Makhwah', 'المخواة', '2020-12-18 17:18:51'),
(23, 'Al Mandaq', 'المندق', '2020-12-18 17:18:51'),
(24, 'Al Mithnab', 'المذنب', '2020-12-18 17:18:51'),
(25, 'Al Muwayh', 'المويه', '2020-12-18 17:18:51'),
(26, 'Al Nabhaniyah', 'النبهانية', '2020-12-18 17:18:51'),
(27, 'Al Nairyah', 'النعيرية', '2020-12-18 17:18:51'),
(28, 'Al Owiqela', 'العويقيلة', '2020-12-18 17:18:51'),
(29, 'Al Qara', 'القرى', '2020-12-18 17:18:51'),
(30, 'Al Qunfudhah', 'القنفذة', '2020-12-18 17:18:51'),
(31, 'Al Shamly', 'الشملي', '2020-12-18 17:18:51'),
(32, 'Al Shimasiyah', 'الشماسية', '2020-12-18 17:18:51'),
(33, 'Al Shinan', 'الشنان', '2020-12-18 17:18:51'),
(34, 'Al Sulayyil', 'السليل', '2020-12-18 17:18:51'),
(35, 'Al Sulimy', 'السليمي', '2020-12-18 17:18:51'),
(36, 'Al Wajh', 'الوجه', '2020-12-18 17:18:51'),
(37, 'Al Zulfi', 'الزلفي', '2020-12-18 17:18:51'),
(38, 'Al-Aflaj', 'الأفلاج', '2020-12-18 17:18:51'),
(39, 'Alays', 'العيص', '2020-12-18 17:18:51'),
(40, 'Al-Bad\'', 'البدع', '2020-12-18 17:18:51'),
(41, 'Al-Baha', 'الباحة', '2020-12-18 17:18:51'),
(42, 'Aldarb', 'الدرب', '2020-12-18 17:18:51'),
(43, 'Alddayir', 'الدائر', '2020-12-18 17:18:51'),
(44, 'Alearida', 'العارضة', '2020-12-18 17:18:51'),
(45, 'Aleidabiu', 'العيدابي', '2020-12-18 17:18:51'),
(46, 'Al-Ghat', 'الغاط', '2020-12-18 17:18:51'),
(47, 'Al-Hariq', 'الحريق', '2020-12-18 17:18:51'),
(48, 'Alharth', 'الحرث', '2020-12-18 17:18:51'),
(49, 'Al-Kharj', 'الخرج', '2020-12-18 17:18:51'),
(50, 'Alkharkhir', 'الخرخير', '2020-12-18 17:18:51'),
(51, 'Al-Muzahmiyya', 'المزاحمية', '2020-12-18 17:18:51'),
(52, 'Al-Namas', 'النماص', '2020-12-18 17:18:51'),
(53, 'Al-Quway\'iyah', 'القويعية', '2020-12-18 17:18:51'),
(54, 'Alriyth', 'الريث', '2020-12-18 17:18:51'),
(55, 'Altwal', 'الطوال', '2020-12-18 17:18:51'),
(56, 'AlUla', 'العلا', '2020-12-18 17:18:51'),
(57, 'Ar Rass', 'الرس', '2020-12-18 17:18:51'),
(58, 'Arar', 'عرعر', '2020-12-18 17:18:51'),
(59, 'Ardyat', 'العرضيات', '2020-12-18 17:18:51'),
(60, 'Asyah', 'الأسياح', '2020-12-18 17:18:51'),
(61, 'Badr', 'بدر', '2020-12-18 17:18:51'),
(62, 'Badr aljanub', 'بدر الجنوب', '2020-12-18 17:18:51'),
(63, 'Bahrah', 'بحرة', '2020-12-18 17:18:51'),
(64, 'Baljurashi', 'بلجرشي', '2020-12-18 17:18:51'),
(65, 'Balqarn', 'بلقرن', '2020-12-18 17:18:51'),
(66, 'Bani Hasan', 'بني حسن', '2020-12-18 17:18:51'),
(67, 'Baqaa', 'بقعاء', '2020-12-18 17:18:51'),
(68, 'Bareq', 'بارق', '2020-12-18 17:18:51'),
(69, 'Baysh', 'بيش', '2020-12-18 17:18:51'),
(70, 'Bisha', 'بيشة', '2020-12-18 17:18:51'),
(71, 'Buraidah', 'بريدة', '2020-12-18 17:18:51'),
(72, 'Damad', 'ضمد', '2020-12-18 17:18:51'),
(73, 'Dammam', 'الدمام', '2020-12-18 17:18:51'),
(74, 'Dawadmi', 'الدوادمي', '2020-12-18 17:18:51'),
(75, 'Dhahran', 'الظهران', '2020-12-18 17:18:51'),
(76, 'Dhahran Al Janub', 'ظهران الجنوب', '2020-12-18 17:18:51'),
(77, 'Dharyah', 'ضرية', '2020-12-18 17:18:51'),
(78, 'Dhurma', 'ضرما', '2020-12-18 17:18:51'),
(79, 'Diriyah', 'الدرعية', '2020-12-18 17:18:51'),
(80, 'Duba', 'ضباء', '2020-12-18 17:18:51'),
(81, 'Dumat al-Jandal', 'دومة الجندل', '2020-12-18 17:18:51'),
(82, 'Fayfa\'', 'فيفاء', '2020-12-18 17:18:51'),
(83, 'Ghamd Al Znad', 'غامد الزناد', '2020-12-18 17:18:51'),
(84, 'Hafar Al-Batin', 'حفر الباطن', '2020-12-18 17:18:51'),
(85, 'Hail', 'حائل', '2020-12-18 17:18:51'),
(86, 'Hajrah', 'الحجرة', '2020-12-18 17:18:51'),
(87, 'Haql', 'حقل', '2020-12-18 17:18:51'),
(88, 'Harub', 'هروب', '2020-12-18 17:18:51'),
(89, 'Hofuf', 'الهفوف', '2020-12-18 17:18:51'),
(90, 'Hotat Bani Tamim', 'حوطة بني تميم', '2020-12-18 17:18:51'),
(91, 'Hubuna', 'حبونا', '2020-12-18 17:18:51'),
(92, 'Huraymila', 'حريملاء', '2020-12-18 17:18:51'),
(93, 'Jeddah', 'جدة', '2020-12-18 17:18:51'),
(94, 'Jizan', 'جازان', '2020-12-18 17:18:51'),
(95, 'Jubail', 'الجبيل', '2020-12-18 17:18:51'),
(96, 'Juzur fursan', 'جزر فرسان', '2020-12-18 17:18:51'),
(97, 'Khabash', 'خباش', '2020-12-18 17:18:51'),
(98, 'Khafji', 'الخفجي', '2020-12-18 17:18:51'),
(99, 'Khamis Mushait', 'خميس مشيط', '2020-12-18 17:18:51'),
(100, 'Kharma', 'الخرمة', '2020-12-18 17:18:51'),
(101, 'Khaybar', 'خيبر', '2020-12-18 17:18:51'),
(102, 'Khobar', 'الخبر', '2020-12-18 17:18:51'),
(103, 'Khulays', 'خليص', '2020-12-18 17:18:51'),
(104, 'Mahayel Aseer', 'محايل عسير', '2020-12-18 17:18:51'),
(105, 'Mahd adh Dhahab', 'مهد الذهب', '2020-12-18 17:18:51'),
(106, 'Makkah', 'مكة المكرمة', '2020-12-18 17:18:51'),
(107, 'Marat', 'مرات', '2020-12-18 17:18:51'),
(108, 'Mawqq', 'موقق', '2020-12-18 17:18:51'),
(109, 'Maysan ', 'ميسان', '2020-12-18 17:18:51'),
(110, 'Medina', 'المدينة المنورة', '2020-12-18 17:18:51'),
(111, 'Najran', 'نجران', '2020-12-18 17:18:51'),
(112, 'Qaryat al-Ulya', 'قرية العليا', '2020-12-18 17:18:51'),
(113, 'Qatif', 'القطيف', '2020-12-18 17:18:51'),
(114, 'Qilwah', 'قلوة', '2020-12-18 17:18:51'),
(115, 'Qurayyat', 'القريات', '2020-12-18 17:18:51'),
(116, 'Rabigh', 'رابغ', '2020-12-18 17:18:51'),
(117, 'Rafha', 'رفحاء', '2020-12-18 17:18:51'),
(118, 'Ranyah', 'رنية', '2020-12-18 17:18:51'),
(119, 'Ras Tanura', 'رأس تنورة', '2020-12-18 17:18:51'),
(120, 'Rijal Almaa', 'رجال ألمع', '2020-12-18 17:18:51'),
(121, 'Rimah', 'رماح', '2020-12-18 17:18:51'),
(122, 'Riyadh', 'الرياض', '2020-12-18 17:18:51'),
(123, 'Riyadh Al Khabra', 'رياض الخبراء', '2020-12-18 17:18:51'),
(124, 'Sabia', 'صبيا', '2020-12-18 17:18:51'),
(125, 'Sakakah', 'سكاكا', '2020-12-18 17:18:51'),
(126, 'Samta', 'صامطة', '2020-12-18 17:18:51'),
(127, 'Sarat Ubaida', 'سراة عبيدة', '2020-12-18 17:18:51'),
(128, 'Shaqraa', 'شقراء', '2020-12-18 17:18:52'),
(129, 'Sharura ', 'شرورة', '2020-12-18 17:18:52'),
(130, 'Tabarjal', 'طبرجل', '2020-12-18 17:18:52'),
(131, 'Tabuk', 'تبوك', '2020-12-18 17:18:52'),
(132, 'Taif', 'الطائف', '2020-12-18 17:18:52'),
(133, 'Tanomah', 'تنومة', '2020-12-18 17:18:52'),
(134, 'Tareeb', 'طريب', '2020-12-18 17:18:52'),
(135, 'Tathlith ', 'تثليث', '2020-12-18 17:18:52'),
(136, 'Tayma', 'تيماء', '2020-12-18 17:18:52'),
(137, 'Thadig', 'ثادق', '2020-12-18 17:18:52'),
(138, 'Thar', 'ثار', '2020-12-18 17:18:52'),
(139, 'Turaif', 'طريف', '2020-12-18 17:18:52'),
(140, 'Turubah', 'تربة', '2020-12-18 17:18:52'),
(141, 'Uglat Al Sugour', 'عقلة الصقور', '2020-12-18 17:18:52'),
(142, 'Umluj', 'أملج', '2020-12-18 17:18:52'),
(143, 'Unaizah', 'عنيزة', '2020-12-18 17:18:52'),
(144, 'Uyun AlJiwa', 'عيوان الجواء', '2020-12-18 17:18:52'),
(145, 'Wadi ad-Dawasir', 'وادي الدواسر', '2020-12-18 17:18:52'),
(146, 'Wadi Al Fra', 'وادي الفرع', '2020-12-18 17:18:52'),
(147, 'Yadamuh', 'يدمه', '2020-12-18 17:18:52'),
(148, 'Yanbu', 'ينبع', '2020-12-18 17:18:52');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `modified_time` int(11) DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location_id`, `name`, `created_by`, `created_time`, `modified_by`, `modified_time`, `status`) VALUES
(1, 'Abu Dhabi', 1, '2021-01-18 14:25:42', NULL, NULL, 1),
(2, 'Dubai', 1, '2021-01-18 14:25:42', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `promocodes`
--

CREATE TABLE `promocodes` (
  `id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `coupon_code` varchar(55) NOT NULL,
  `short_desc` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `discount_per` varchar(55) NOT NULL,
  `only_for` varchar(55) NOT NULL COMMENT 'new users or all',
  `expiry_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `from_added` varchar(55) NOT NULL DEFAULT 'admin',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `promocodes`
--

INSERT INTO `promocodes` (`id`, `service_provider_id`, `coupon_code`, `short_desc`, `image`, `discount_per`, `only_for`, `expiry_date`, `status`, `from_added`, `created_time`, `modified_time`) VALUES
(3, 1, 'SALONEE222', 'asda', 'uploads/promocodes/linux shared hosting ad.png', '21', 'all', '0000-00-00', 1, 'service_provider', '2021-02-27 12:10:55', '2021-02-27 12:10:55');

-- --------------------------------------------------------

--
-- Table structure for table `salesperson`
--

CREATE TABLE `salesperson` (
  `salesperson_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `password` text NOT NULL,
  `location` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesperson`
--

INSERT INTO `salesperson` (`salesperson_id`, `name`, `email`, `mobile`, `password`, `location`, `image`, `created_time`, `modified_time`, `status`) VALUES
(5, 'Mallik Kompelli', 'mallikarjun.kompelli@gmail.com', 8179602883, '123456', '1', 'uploads/admin/me3234.png', '2021-02-12 17:28:55', '2021-02-12 17:28:55', 1),
(6, 'Arjun', 'mallikkmpll@gmail.com', 7013598480, 'TeQZOA', '1', 'uploads/admin/me.png', '2021-02-12 17:33:20', '2021-02-12 17:33:20', 1),
(7, 'Mallik Kompelli', 'mallikarjun.kompelli123@gmail.com', 18179602883, 'ETJKs9', '2', 'uploads/admin/default-image.png', '2021-02-12 17:44:01', '2021-02-12 17:44:01', 0),
(8, 'Mallik Kompelli', 'mallikarjun.kompelli12313@gmail.com', 123123123, 'wdTFXz', '0', 'uploads/admin/me.png', '2021-02-12 18:48:48', '2021-02-12 18:48:48', 1),
(9, 'Kishore', 'kishore@gmail.com', 9090998989, 'kOja89', '144', 'uploads/admin/what-is-the-meaning-of-sales-executive_0da19d73-a2d6-482c-bdbd-a6f5385ab48a.jpg', '2021-02-15 03:42:13', '2021-02-15 03:42:13', 1),
(10, 'Test', 'mastanrao.konduri7@gmail.com', 949392737, 'oxFS3g', '0', 'uploads/admin/Capture.PNG', '2021-02-15 05:45:22', '2021-02-15 05:45:22', 1),
(11, 'Test User', 'puvvalaudaykiran@gmail.com', 9999966666, '4aO60S', '2', 'uploads/admin/Capture.PNG', '2021-02-15 06:34:43', '2021-02-15 06:34:43', 0);

-- --------------------------------------------------------

--
-- Table structure for table `salespersons_service_providers`
--

CREATE TABLE `salespersons_service_providers` (
  `id` int(11) NOT NULL,
  `salesperson_id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `salespersons_service_providers`
--

INSERT INTO `salespersons_service_providers` (`id`, `salesperson_id`, `service_provider_id`, `created_at`) VALUES
(1, 6, 23, '2021-02-12 18:40:59'),
(2, 6, 24, '2021-02-12 18:49:48'),
(3, 5, 25, '2021-02-20 10:05:04'),
(4, 5, 30, '2021-02-27 10:56:39'),
(5, 5, 31, '2021-02-27 11:02:07');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `category_id`, `name`, `created_time`, `modified_time`, `status`) VALUES
(1, 1, 'Simple', '2021-01-18 21:01:32', '2021-02-20 10:13:23', 1),
(2, 1, 'Trendy', '2021-01-18 21:01:32', NULL, 1),
(3, 1, 'Modern', '2021-01-18 21:01:32', NULL, 1),
(4, 1, 'Normal', '2021-01-18 21:01:32', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services_prices`
--

CREATE TABLE `services_prices` (
  `sp_id` int(11) NOT NULL,
  `service_provider_service_id` int(11) NOT NULL,
  `price_for` varchar(55) NOT NULL COMMENT 'salonee,home,both',
  `base_price` decimal(10,2) NOT NULL,
  `price_type` varchar(55) NOT NULL,
  `sale_price` decimal(10,2) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_prices`
--

INSERT INTO `services_prices` (`sp_id`, `service_provider_service_id`, `price_for`, `base_price`, `price_type`, `sale_price`, `created_time`, `modified_time`) VALUES
(4, 7, 'home', '1000.00', 'custom_price_weekend', '800.00', '2021-02-27 11:43:56', '2021-02-27 11:43:56'),
(5, 8, 'home', '1600.00', 'custom_price_special_date', '1400.00', '2021-02-27 10:42:45', '2021-02-27 10:42:45'),
(6, 8, 'salonee', '1000.00', 'custom_price_weekend', '900.00', '2021-02-27 10:42:45', '2021-02-27 10:42:45');

-- --------------------------------------------------------

--
-- Table structure for table `services_special_prices`
--

CREATE TABLE `services_special_prices` (
  `scp_id` int(11) NOT NULL,
  `service_provider_service_id` int(11) NOT NULL,
  `sp_id` int(11) NOT NULL,
  `price_for` varchar(55) NOT NULL,
  `type` varchar(155) NOT NULL COMMENT 'special date or every monday example ',
  `custom_on` varchar(155) NOT NULL,
  `sales_price` decimal(10,2) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services_special_prices`
--

INSERT INTO `services_special_prices` (`scp_id`, `service_provider_service_id`, `sp_id`, `price_for`, `type`, `custom_on`, `sales_price`, `created_time`, `modified_time`) VALUES
(1, 5, 5, 'home', 'custom_price_special_date', '2021-02-28', '1300.00', '2021-02-27 10:27:36', '2021-02-27 10:27:36'),
(2, 5, 1, 'salonee', 'custom_price_weekend', 'satuerday', '800.00', '2021-02-27 10:27:36', '2021-02-27 10:27:36'),
(3, 5, 1, 'salonee', 'custom_price_weekend', 'sunday', '800.00', '2021-02-27 10:27:36', '2021-02-27 10:27:36'),
(4, 7, 4, 'home', 'custom_price_weekend', 'monday', '800.00', '2021-02-27 10:39:37', '2021-02-27 10:39:37'),
(5, 7, 4, 'home', 'custom_price_weekend', 'tuesday', '800.00', '2021-02-27 10:39:37', '2021-02-27 10:39:37'),
(6, 8, 5, 'home', 'custom_price_special_date', '2021-03-03', '1300.00', '2021-02-27 10:42:45', '2021-02-27 10:42:45'),
(7, 8, 5, 'home', 'custom_price_special_date', '2021-03-11', '1300.00', '2021-02-27 10:42:45', '2021-02-27 10:42:45'),
(8, 8, 6, 'salonee', 'custom_price_weekend', 'satuerday', '800.00', '2021-02-27 10:42:45', '2021-02-27 10:42:45'),
(9, 8, 6, 'salonee', 'custom_price_weekend', 'sunday', '800.00', '2021-02-27 10:42:45', '2021-02-27 10:42:45');

-- --------------------------------------------------------

--
-- Table structure for table `service_provider`
--

CREATE TABLE `service_provider` (
  `service_provider_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `business_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` bigint(15) NOT NULL,
  `password` text NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `business_licence` text NOT NULL,
  `is_approved` tinyint(2) NOT NULL DEFAULT '0',
  `is_email_verified` tinyint(2) NOT NULL DEFAULT '0',
  `city` int(11) NOT NULL,
  `address` text NOT NULL,
  `referer_by` varchar(55) NOT NULL DEFAULT 'salesperson' COMMENT 'salesperson,main_branch',
  `created_time` datetime DEFAULT NULL,
  `user_type` varchar(55) NOT NULL DEFAULT 'main_branch',
  `modified_time` datetime DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_provider`
--

INSERT INTO `service_provider` (`service_provider_id`, `name`, `business_name`, `email`, `mobile`, `password`, `image`, `business_licence`, `is_approved`, `is_email_verified`, `city`, `address`, `referer_by`, `created_time`, `user_type`, `modified_time`, `status`) VALUES
(1, 'Shiva Prasad', 'Unique Salone', 'serviceprovider@gmail.com', 8074888053, '123456', 'uploads/images.jpg', '1612677617.pdf', 1, 1, 1, 'Hyderabad', 'salesperson', NULL, 'main_branch', '2021-01-19 05:22:15', 2),
(21, 'Mastan', 'ABC Saloon', 'test@gmail.com', 9599399333, '', NULL, '1612680724.pdf', 0, 0, 1, 'Hyderabad', 'salesperson', NULL, 'main_branch', NULL, 2),
(23, 'KVR Techno', 'KVR Techno', 'mallikarjun.kompell123i@gmail.com', 8179602883, 'DLpbYM', NULL, 'uploads/certificates/ePoS_settings_chrome_manual.pdf', 0, 0, 1, 'Hyderabad', 'salesperson', NULL, 'main_branch', NULL, 1),
(24, 'KVR Techno', 'KVR Techno', 'mallikarjun.kompelli@gmail.com', 7013598480, '1T2bx0', NULL, 'uploads/certificates/Unit test 09 Paper and Solution.pdf', 0, 0, 1, 'Hyderabad', 'salesperson', NULL, 'main_branch', NULL, 2),
(25, 'KVR Techno', 'KVR Techno', 'mallikarjun.kompellikvr@gmail.com', 798888222, '0z0Lyy', NULL, 'uploads/certificates/webrtc_internals_dump (1).txt', 0, 0, 12, 'Warangal\r\nWarangal', 'salesperson', NULL, 'main_branch', NULL, 3),
(27, 'KVR Techno', 'KVR Techno', 'branch@gmail.com', 123123, 'zXRkGX', NULL, 'uploads/certificates/webrtc_internals_dump.txt', 0, 0, 12, 'L.G. Heights, 1st Floor, Above Hitech City Rd, near Happi mobiles, Madhapur, Telangana 500085\r\n', 'main_branch', NULL, 'sub_branch', NULL, 1),
(28, 'KVR Techno', 'KVR Techno', 'asdpelli@gmail.com', 23123123, 'S9lkJi', NULL, 'uploads/certificates/favicon (1).ico', 0, 0, 1, 'asd asd asasd asd asasd asd asasd asd as', 'main_branch', NULL, 'sub_branch', NULL, 3),
(29, 'Mastan', 'test', 'mrkonduir@gmail.com', 9493912754, 'EkBf5g', NULL, 'uploads/certificates/dummy.pdf', 0, 0, 1, 'testt', 'main_branch', NULL, 'sub_branch', NULL, 1),
(30, 'Mlali ', 'asd', 'mlli@mgll.com', 1233212321, '1DWIRN', NULL, 'uploads/certificates/linux shared hosting ad.png', 0, 0, 1, 'asdasds', 'salesperson', NULL, 'main_branch', NULL, 3),
(31, 'arjun', 'asdsad', 'asda@asds.com', 1233213123, 'PJnXV9', NULL, 'uploads/certificates/xyz domain sell.png', 0, 0, 6, 'asdasd', 'salesperson', NULL, 'main_branch', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_provider_branches`
--

CREATE TABLE `service_provider_branches` (
  `service_provider_branche_id` int(11) NOT NULL,
  `service_provider_main_branch_id` int(11) DEFAULT NULL,
  `service_provider_id` int(11) NOT NULL,
  `address` text NOT NULL,
  `location_id` int(11) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_provider_branches`
--

INSERT INTO `service_provider_branches` (`service_provider_branche_id`, `service_provider_main_branch_id`, `service_provider_id`, `address`, `location_id`, `created_time`, `status`) VALUES
(1, NULL, 0, 'M.M. Heights, 1st Floor, Above Hitech City Rd, near Happi mobiles, Madhapur, Telangana 500081', 1, '2021-01-18 14:47:10', 1),
(2, NULL, 1, 'K.K. Heights, 2nd Floor, Above Hitech City Rd, near Happi mobiles, Madhapur, Telangana 500081', 2, '2021-01-18 15:00:45', 1),
(3, NULL, 1, 'R.K. Heights, 3rd Floor, Above Hitech City Rd, near Happi mobiles, Madhapur, Telangana 500081', 1, '2021-01-18 15:01:14', 0),
(4, NULL, 1, 'L.G. Heights, 1st Floor, Above Hitech City Rd, near Happi mobiles, Madhapur, Telangana 500085', 2, '2021-01-18 21:45:36', 0),
(10, NULL, 1, 'kk heights', 1, '2021-01-20 12:20:15', 0),
(11, NULL, 1, 'Ziani Area\r\nHamdan Street\r\nAbu Dhabi', 2, '2021-01-23 10:22:09', 0),
(13, 1, 27, 'L.G. Heights, 1st Floor, Above Hitech City Rd, near Happi mobiles, Madhapur, Telangana 500085\r\n', 12, '2021-02-20 11:53:30', 1),
(14, 1, 28, 'asd asd asasd asd asasd asd asasd asd as', 1, '2021-02-20 12:04:58', 1),
(15, 1, 29, 'testt', 1, '2021-02-21 05:41:32', 1),
(16, 30, 30, 'testt', 1, '2021-02-21 05:41:32', 1),
(17, 31, 31, 'asdasd', 6, '2021-02-27 11:02:07', 1),
(18, 1, 1, 'kamlaapur', 6, '2021-02-27 11:02:07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_provider_categories`
--

CREATE TABLE `service_provider_categories` (
  `service_provider_category_id` int(11) NOT NULL,
  `service_provider_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_provider_categories`
--

INSERT INTO `service_provider_categories` (`service_provider_category_id`, `service_provider_id`, `category_id`, `created_time`, `status`) VALUES
(19, 1, 4, '2021-02-26 17:39:33', 1),
(20, 1, 6, '2021-02-26 17:39:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_provider_employees`
--

CREATE TABLE `service_provider_employees` (
  `employee_id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `service_provider_branch_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` bigint(15) NOT NULL,
  `password` text NOT NULL,
  `image` text NOT NULL,
  `is_email_verified` tinyint(2) NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` datetime DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_provider_ratings`
--

CREATE TABLE `service_provider_ratings` (
  `service_provider_rating_id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `service_provider_branch_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `rating` decimal(10,2) NOT NULL,
  `feedback` text NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_provider_services`
--

CREATE TABLE `service_provider_services` (
  `service_provider_service_id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `service_provider_branch_id` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `category_id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_at` varchar(155) NOT NULL DEFAULT 'salonee' COMMENT 'at home , or at salonee, at both ',
  `add_ons` text NOT NULL,
  `product_to_use` text NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_provider_services`
--

INSERT INTO `service_provider_services` (`service_provider_service_id`, `service_provider_id`, `service_provider_branch_id`, `description`, `category_id`, `service_name`, `service_at`, `add_ons`, `product_to_use`, `created_time`, `status`) VALUES
(7, 1, 13, 'test', 6, 'facial', 'home', 'asd', 'ad', '2021-02-27 10:39:37', 1),
(8, 1, 14, 'tess', 4, 'tes', 'both', 'sadfg', 'asdf', '2021-02-27 10:42:45', 0);

-- --------------------------------------------------------

--
-- Table structure for table `service_provider_slots`
--

CREATE TABLE `service_provider_slots` (
  `service_provider_slot_id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `service_provider_branch_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_provider_subscriptions`
--

CREATE TABLE `service_provider_subscriptions` (
  `service_provider_subscription_id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `subscription_plan_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `payment_id` int(11) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_plans`
--

CREATE TABLE `subscription_plans` (
  `subscription_plan_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `duration` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` datetime DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` bigint(15) NOT NULL,
  `password` text NOT NULL,
  `dob` date NOT NULL,
  `gender` enum('MALE','FEMALE') NOT NULL,
  `location_id` int(11) NOT NULL,
  `is_email_verified` tinyint(4) NOT NULL,
  `device_token` text NOT NULL,
  `auth_token` text NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_time` datetime DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`booking_id`),
  ADD KEY `service_provider_branch_id` (`service_provider_branch_id`),
  ADD KEY `service_provider_slot_id` (`service_provider_slot_id`),
  ADD KEY `service_provider_service_id` (`service_provider_service_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `promocodes`
--
ALTER TABLE `promocodes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coupon_code` (`coupon_code`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `salesperson`
--
ALTER TABLE `salesperson`
  ADD PRIMARY KEY (`salesperson_id`);

--
-- Indexes for table `salespersons_service_providers`
--
ALTER TABLE `salespersons_service_providers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `salesperson_id` (`salesperson_id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `services_prices`
--
ALTER TABLE `services_prices`
  ADD PRIMARY KEY (`sp_id`),
  ADD KEY `service_provider_service_id` (`service_provider_service_id`);

--
-- Indexes for table `services_special_prices`
--
ALTER TABLE `services_special_prices`
  ADD PRIMARY KEY (`scp_id`),
  ADD KEY `service_provider_service_id` (`service_provider_service_id`),
  ADD KEY `sp_id` (`sp_id`);

--
-- Indexes for table `service_provider`
--
ALTER TABLE `service_provider`
  ADD PRIMARY KEY (`service_provider_id`);

--
-- Indexes for table `service_provider_branches`
--
ALTER TABLE `service_provider_branches`
  ADD PRIMARY KEY (`service_provider_branche_id`),
  ADD KEY `service_provider_id` (`service_provider_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `service_provider_branches_ibfk_1` (`service_provider_main_branch_id`);

--
-- Indexes for table `service_provider_categories`
--
ALTER TABLE `service_provider_categories`
  ADD PRIMARY KEY (`service_provider_category_id`),
  ADD KEY `service_provider_id` (`service_provider_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `service_provider_employees`
--
ALTER TABLE `service_provider_employees`
  ADD KEY `service_provider_branch_id` (`service_provider_branch_id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `service_provider_ratings`
--
ALTER TABLE `service_provider_ratings`
  ADD PRIMARY KEY (`service_provider_rating_id`);

--
-- Indexes for table `service_provider_services`
--
ALTER TABLE `service_provider_services`
  ADD PRIMARY KEY (`service_provider_service_id`),
  ADD KEY `service_provider_id` (`service_provider_id`),
  ADD KEY `service_provider_branch_id` (`service_provider_branch_id`);

--
-- Indexes for table `service_provider_slots`
--
ALTER TABLE `service_provider_slots`
  ADD PRIMARY KEY (`service_provider_slot_id`),
  ADD KEY `service_provider_branch_id` (`service_provider_branch_id`),
  ADD KEY `service_provider_id` (`service_provider_id`);

--
-- Indexes for table `service_provider_subscriptions`
--
ALTER TABLE `service_provider_subscriptions`
  ADD PRIMARY KEY (`service_provider_subscription_id`),
  ADD KEY `service_provider_id` (`service_provider_id`),
  ADD KEY `subscription_plan_id` (`subscription_plan_id`);

--
-- Indexes for table `subscription_plans`
--
ALTER TABLE `subscription_plans`
  ADD PRIMARY KEY (`subscription_plan_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `location_id` (`location_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `promocodes`
--
ALTER TABLE `promocodes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `salesperson`
--
ALTER TABLE `salesperson`
  MODIFY `salesperson_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `salespersons_service_providers`
--
ALTER TABLE `salespersons_service_providers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `services_prices`
--
ALTER TABLE `services_prices`
  MODIFY `sp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `services_special_prices`
--
ALTER TABLE `services_special_prices`
  MODIFY `scp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `service_provider`
--
ALTER TABLE `service_provider`
  MODIFY `service_provider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `service_provider_branches`
--
ALTER TABLE `service_provider_branches`
  MODIFY `service_provider_branche_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `service_provider_categories`
--
ALTER TABLE `service_provider_categories`
  MODIFY `service_provider_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `service_provider_ratings`
--
ALTER TABLE `service_provider_ratings`
  MODIFY `service_provider_rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service_provider_services`
--
ALTER TABLE `service_provider_services`
  MODIFY `service_provider_service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `service_provider_slots`
--
ALTER TABLE `service_provider_slots`
  MODIFY `service_provider_slot_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service_provider_subscriptions`
--
ALTER TABLE `service_provider_subscriptions`
  MODIFY `service_provider_subscription_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscription_plans`
--
ALTER TABLE `subscription_plans`
  MODIFY `subscription_plan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_ibfk_1` FOREIGN KEY (`service_provider_branch_id`) REFERENCES `service_provider_branches` (`service_provider_branche_id`),
  ADD CONSTRAINT `bookings_ibfk_2` FOREIGN KEY (`service_provider_slot_id`) REFERENCES `service_provider_slots` (`service_provider_slot_id`),
  ADD CONSTRAINT `bookings_ibfk_3` FOREIGN KEY (`service_provider_service_id`) REFERENCES `service_provider_services` (`service_provider_service_id`);

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`);

--
-- Constraints for table `service_provider_branches`
--
ALTER TABLE `service_provider_branches`
  ADD CONSTRAINT `service_provider_branches_ibfk_1` FOREIGN KEY (`service_provider_main_branch_id`) REFERENCES `service_provider` (`service_provider_id`),
  ADD CONSTRAINT `service_provider_branches_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `cities` (`id`);

--
-- Constraints for table `service_provider_categories`
--
ALTER TABLE `service_provider_categories`
  ADD CONSTRAINT `service_provider_categories_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `service_provider` (`service_provider_id`),
  ADD CONSTRAINT `service_provider_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`);

--
-- Constraints for table `service_provider_employees`
--
ALTER TABLE `service_provider_employees`
  ADD CONSTRAINT `service_provider_employees_ibfk_1` FOREIGN KEY (`service_provider_branch_id`) REFERENCES `service_provider_branches` (`service_provider_branche_id`),
  ADD CONSTRAINT `service_provider_employees_ibfk_2` FOREIGN KEY (`service_provider_id`) REFERENCES `service_provider` (`service_provider_id`);

--
-- Constraints for table `service_provider_services`
--
ALTER TABLE `service_provider_services`
  ADD CONSTRAINT `service_provider_services_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `service_provider` (`service_provider_id`),
  ADD CONSTRAINT `service_provider_services_ibfk_2` FOREIGN KEY (`service_provider_branch_id`) REFERENCES `service_provider_branches` (`service_provider_branche_id`);

--
-- Constraints for table `service_provider_slots`
--
ALTER TABLE `service_provider_slots`
  ADD CONSTRAINT `service_provider_slots_ibfk_1` FOREIGN KEY (`service_provider_branch_id`) REFERENCES `service_provider_branches` (`service_provider_branche_id`),
  ADD CONSTRAINT `service_provider_slots_ibfk_2` FOREIGN KEY (`service_provider_id`) REFERENCES `service_provider` (`service_provider_id`);

--
-- Constraints for table `service_provider_subscriptions`
--
ALTER TABLE `service_provider_subscriptions`
  ADD CONSTRAINT `service_provider_subscriptions_ibfk_1` FOREIGN KEY (`service_provider_id`) REFERENCES `service_provider` (`service_provider_id`),
  ADD CONSTRAINT `service_provider_subscriptions_ibfk_2` FOREIGN KEY (`subscription_plan_id`) REFERENCES `subscription_plans` (`subscription_plan_id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `location` (`location_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
