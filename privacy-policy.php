<?php 
include('header.php');
$content = $funcObject->privacyPrivacy($con);

while($row = mysqli_fetch_array($content))
{
    $privacy_policy = $row['privacy_policy'];
}

?>
<style>
    .card-p-policy {
        padding: 30px;
        text-align: justify;
    }
    .card-p-policy tt {
        color: #d06690;
    }
    .card-p-policy ul {
        display: block !important;
        list-style-type: disc !important;
    }
    .card-p-policy ul li {
        max-width: 100% !important;
        display: list-item !important;
    }
</style>
<div class="container-fluid">
    <div class="_header"></div>
    <div class="container p-0">
        <nav aria-label="breadcrumb" class="_custmBrdcrmb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
            </ol>
        </nav>
    </div>

    <div class="nearShops _nrShops">
        <div class="container">
            <div class="card card-p-policy">
                <?php echo $privacy_policy; ?>
            </div>    
        </div>
    </div>
</div>
<?php 
include('footer.php');
?>