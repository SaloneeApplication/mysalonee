<?php 
include('header.php');
include('controllers/cart.php');

$user_id = @$_SESSION['user_id'];

if($user_id == ""){
   if(isset($_SERVER['HTTPS'])){
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    }
    else{
        $protocol = 'http';
    }
  	$protocol = $protocol."://" . $_SERVER['HTTP_HOST'];	
    echo '<script> var base_url = "'.$protocol.'"; </script>';
    echo '<script> window.location.replace(base_url); </script>';
}

$cartController = new cart();
$promoCodes = $funcObject->getAllUserPromocodes($con, 'all');

$cart_services = $cartController->getServiceList($user_id);
$cart_products = $cartController->cart_products($user_id);

?>

<style type="text/css">
.modal {
  overflow-y:auto;
}
.img_container{
    display: inline-block;
    font-size: 40px;
    line-height: 50px;
    color:#c96c92;
    width: 50px;
    height: 50px;
    text-align: center;
    vertical-align: bottom;
    left: 50px;
    margin-left: 45%;
    margin-top: 10%;
}

.help { display: inline-block; position: relative; }

.help-button {
  color: #222;
  font-weight: bold;
  text-decoration: none;
}

.info-box {
    border-radius: 15px;
    border: 2px solid #bdbdbd;
    box-shadow: 8px 10px 10px 0px #888888;
    background-color: #ddd;
    display: none;
    color: #d55c91;
    /*background-color: #ddd;*/
    /*display: none;*/
    /*color: #888;*/
    font-family: sans-serif;
    font-size: smaller;
    padding: 10px;
    width: 170px;
    position: absolute;
    right: -190px;
    bottom: 0;
}

.info-box::after {
    border-top: 20px solid transparent;
    border-right: 20px solid #ddd;
    content: " ";
    display: block;
    position: absolute;
    left: -20px;
    bottom: 0;
    width: 0;
    height: 0;
}

.info-box .close-button {
    border: 1px dotted #222;
    color: #222;
    float: right;
    line-height: 0.6em;
    padding: 0;
    text-decoration: none;
}

.info-box .close-button:hover {
    border-color: #aaa;
    color: #aaa;
}
</style>

<link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<div class="container-fluid">
    <div class="_header"></div>
    <nav aria-label="breadcrumb" class="_custmBrdcrmb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="saloon-shops.php">Shops</a></li>
            <li class="breadcrumb-item active" aria-current="page">Cart</li>
        </ol>
    </nav>
    <div class="container-fluid pad15">
        <div class="row">
            <?php
            if($cart_services || $cart_products)
            {?>
                <div class="col-md-4">
                    <div class="login" style="padding-top: 15px;">
                        <p><strong style="margin-left: 18px;">Fill The Details</strong></p>
                        <form method="POST" id="booking-form">
                            <input type="hidden" id="service_id" >
                            <input type="hidden" id="service_provider_id">
                            <div class="form-group mb-4">
                                <label>Service At</label>
                                <div class="form-group mb-2">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Salon <input type="radio" checked class="form-check-input" id="type" onclick="set_type('salon')" name="type" value="salonee">
                                    </label>
                                  </div>
                                  <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Home <input type="radio" class="form-check-input" id="type" name="type" value="home" onclick="set_type('home')"> 
                                    </label>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <input type="text" id="sDate" class="form-control" autocomplete="off" required>
                                <label class="form-control-placeholder p-0" for="sDate">Date</label>
                            </div>
                            <div class="form-group mb-2">
                                <div class="boxedList">
                                    <div class="flxRow">
                                    <p id="slotLabel">Select Available Slot</p> 
                                    <i class="fa fa-caret-down" aria-hidden="true" id="showSlots"></i> 
                                </div>
                                    <div id="slotsList" style="display: none;">
                                    <ul id="slotData"></ul>
                                </div>
                                </div>
                            </div>
                            <!-- <div class="form-group mb-2">
                                <select name="service" class="form-control" autocomplete="off" required>
                                    <option hidden disabled selected value>
                                        Add List of Services </option>
                                    <option value="service1">service-1</option>
                                    <option value="service2">service-2</option>
                                    <option value="service3">service-3</option>
                                </select>
                            </div> -->
                            <div class="form-group mb-2" id="is-coupon">
                                <label>Coupon<div class="help">
                                    <div class="info-box">
                                        <a href="#" class="close-button">&times;</a>
                                        <h6>Promo Codes</h6>
                                        <?php
                                        while($rowCoupon = mysqli_fetch_array($promoCodes))
                                        {?>
                                            <div><?php echo $rowCoupon['coupon_code']; ?> -- <?php echo $rowCoupon['discount_per']."% OFF"; ?> 
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <a class='help-button' href='#' title="Click to know more">[?]</a>
                                </div></label>
                                <div class="row" style="padding: 0px 15px 0px 15px;">
                                    <input type="text" id="coupon" name="coupon" value="" class="form-control col-md-8" autocomplete="off" required/>
                                    <button type="button" id="apply-coupon" class="btn btn-sm theme-btn col-md-4">Apply</button>
                                    <button type="button" id="cancel-coupon" class="btn btn-sm theme-btn col-md-4">clear</button>
                                    <p id="c_success" style="color: green;">Coupon Applied</p>
                                    <p id="c_fail" style="color: red;">Coupon not available</p>
                                </div>
                            </div>
                            <div class="form-group mb-4">
                                <label>Preferences</label>
                                <div class="form-group mb-2">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Male <input type="radio" class="form-check-input" name="optradio">
                                    </label>
                                  </div>
                                  <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Female <input type="radio" class="form-check-input" name="optradio"> 
                                    </label>
                                  </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
<?php
if($cart_services){
?>
<div class="cartPage cart-service-wp">
<ul>
<li class="colmn1">Service Details</li>
<li class="colmn2">Base Price</li>
<li class="colmn2">Sale Price</li>
<li class="colmn4">Total</li>
</ul>
<?php 
foreach($cart_services as $row){
?>
<div class="card cart_services cart-list-<?=$row['cart_id']?>">
<input type="hidden" name="service_id[]" value="<?php echo $row['service_id'];?>">
<input type="hidden" name="service_provider_id" value="<?php echo $row['service_provider_id'];?>">
<input type="hidden" class="salon_price" value="<?php echo $row['salon_price'];?>">
<input type="hidden" class="home_price" value="<?php echo $row['home_price'];?>">
<input type="hidden" class="base_salon_price" value="<?php echo $row['base_salon_price'];?>">
<input type="hidden" class="base_home_price" value="<?php echo $row['base_home_price'];?>">

<div class="colmn1">
    <div class="d-flex">
        <div class="_thumb">
            <img src="<?php echo ADMIN_URL.$row['image'];?>" alt="cart_item">
        </div>
        <div style="margin-top: 33px;">
            <h5><?php echo $row['service_name'];?></h5>
        </div>
    </div>
</div>
<?php
/*    $service_prices = $funcObject->getServicePrices($con, $type = 'salonee', $row['service_id']);
    while($row1 = mysqli_fetch_array($service_prices))
    {
        $base_price = $row1['base_price'];
        $sale_price = $row1['sale_price'];
        $home_price = $row1['sale_price'];
    }*/
?>
<div class="colmn2">
    <h4>AED <span class="show-service-home-price"><?php echo $row['base_salon_price'];?></span></h4>
</div>
<div class="colmn2">
    <h4>AED <span class="show-service-price"><?php echo $row['salon_price'];?></span></h4>
</div>
<div class="colmn4">
    <h4 class="service_price1">AED <span class="show-service-price"><?php echo $row['salon_price'];?></span></h4> 
</div>
<div class="colmn5">
    <a href="javascript:;" onclick="delete_cart(<?php echo $row['cart_id'];?>)" style="margin-bottom: 13px; color:#d55c91"><i class="fa fa-trash-o"></i></a>
</div>
</div>
<?php
}?>

<div class="card _Total">
<div class="my_flcx">
<div class="_lblsCrt">
    <p><b><span class="service-cart-num"></span> item (s)</b></p> 
</div>
<div class="_pricesCrt">
    <p id="service_total"></p>
</div>
</div>
<div class="my_flcx">
<div class="_lblsCrt">
    <p>Sub Total</p> 
</div>
<div class="_pricesCrt">
    <p id="service_sub_total"></p>
</div>
</div>
<div class="my_flcx _brdrs">
<div class="_lblsCrt">
    <h2>Grand Total</h2>
</div>
<div class="_pricesCrt">
    <h2 id="service_grand_total"></h2>
</div>
</div>
</div>  
</div>
<?php
}
?>
<?php
                    if($cart_products)
                    {?>
                        <div class="cartPage product-cart-wp">
                            <ul>
                                <li class="colmn1">Product Details</li>
                                <li class="colmn2">Price</li>
                                <li class="colmn3">Quantity</li>
                                <li class="colmn4">Total</li>
                            </ul>
                            <?php
                            foreach($cart_products as $row)
                            {?>
                                <div class="card cart_products cart-list-<?=$row['cart_id']?>">
                                    <input type="hidden" name="product_id[]" value="<?php echo $row['product_id'];?>">
                                    <div class="colmn1">
                                        <div class="d-flex">
                                            <div class="_thumb">
                                                <img src="<?php echo user_base_url.$row['product_image']; ?>" alt="cart_item">
                                            </div>
                                            <div>
                                                <h5><?php echo $row['product_title'];?></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="colmn2">
                                        <h4>AED <?php echo $row['sale_price'];?></h4>
                                    </div>
                                    <div class="colmn3"> 
                                        <div class="_countActions">
                                        <input type="button" value="-" class="count_action1 inc1" onclick="update_qty('<?=$row['cart_id']?>','minus')"/>
                                        <input type="text" name="quantity[]" class="quantity i-qty" value="<?=$row['quantity']?>" maxlength="2" max="10" size="1" id="number"  readonly="readonly"/>
                                        <input type="button" value="+" class="count_action1 dec1" onclick="update_qty('<?=$row['cart_id']?>','add')"/>
                                        </div> 
                                    </div>
                                    <div class="colmn4">
                                        <input type="hidden" name="product_price" class="product_price price i-price" value="<?php echo ($row['sale_price']);?>">
                                        <h4 class="product_price1">AED <?php echo round($row['sale_price']*$row['quantity'],2);?> </h4> 
                                    </div>
                                    <div class="colmn5">
                                        <a href="javascript:;" onclick="delete_cart(<?php echo $row['cart_id'];?>)" style="margin-bottom: 13px; color:#d55c91"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </div>
                            <?php
                            }?>
        
                            <div class="card _Total">
                                <div class="my_flcx">
                                    <div class="_lblsCrt">
                                        <p><b><span class="product-cart-num"></span> item</b></p> 
                                    </div>
                                    <div class="_pricesCrt">
                                        <p id="product_total"></p>
                                    </div>
                                </div>
                                <div class="my_flcx">
                                    <div class="_lblsCrt">
                                        <p>Sub Total</p> 
                                    </div>
                                    <div class="_pricesCrt">
                                        <p id="product_sub_total"></p>
                                    </div>
                                </div>
                                <div class="my_flcx">
                                    <div class="_lblsCrt">
                                        <p>Delivery Fee</p> 
                                    </div>
                                    <div class="_pricesCrt">
                                        <p>AED 0.00</p>
                                    </div>
                                </div>
                                <div class="my_flcx _brdrs">
                                    <div class="_lblsCrt">
                                        <h2>Grand Total</h2>
                                    </div>
                                    <div class="_pricesCrt">
                                        <h2 id="product_grand_total"></h2>
                                    </div>
                                </div>
                                <div class="my_flcx _brdrs">
                                    <div class="_lblsCrt">
                                        <h2>Payable</h2>
                                    </div>
                                    <div class="_pricesCrt">
                                        <input type="hidden" id="total_amount">
                                        <h2 id="total_payable"></h2>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    <?php
                    }?>   
                    <div class="form-group mb-2">
                        <button type="button" onclick="pay();" id="btn-pay" class="btn theme-btn">Proceed To Pay</button>
                    </div>
                </div>
            <?php
            }
            else
            {?>
                <div class="jumbotron col-lg-6 col-md-6 col-sm-6 col-xs-6 offset-3 float-md-center text-center">
                  <h4>Your cart is empty.</h4>
                </div>
            <?php  
            }
            ?>
        </div>
    </div>
</div>
<?php include('footer.php');?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<?php
if($cart_services || $cart_products){
?>
<script>
var service_provider_id = $('.cart_services').find("input[name='service_provider_id']").val();
var service_type = 'salonee';
function set_type(type){
	service_type = type;
	if($('.cart_services').length>0){
		$('.cart_services').each(function(){
			var $this =$(this); 
			if(type=='home'){
				$this.find( ".show-service-price" ).html($this.find( ".home_price" ).val());
				$this.find( ".show-service-home-price" ).html($this.find( ".base_home_price" ).val());
			}
			else{
				$this.find( ".show-service-price" ).html($this.find( ".salon_price" ).val());
				$this.find( ".show-service-home-price" ).html($this.find( ".base_salon_price" ).val());
			}
		});
		totalAmountService();
		totalAmountProduct();
	}
}
function totalAmountService(){
	var sum1 = 0;
	setTimeout(function (){
		var sum = 0;
		if($('.cart_services').length>0){
			var services_count=0;
			$('.cart_services').each(function(){
				var $this =$(this); 
				services_count++;
				if(service_type=='salonee'){
					sum +=  parseFloat($this.find( ".salon_price" ).val());
				}
				else{
					sum +=  parseFloat($this.find( ".home_price" ).val());
				}
			});

			$('.service-cart-num').html(services_count);
			$('#service_total').html('<b>AED '+sum+'</b>');
			$('#service_sub_total').html('AED '+sum);
			$('#service_grand_total').html('AED '+sum);
		}
		else{
			$('.cart-service-wp').remove();;
		}
	}, 800);	
}
totalAmountService();

function totalAmountProduct(){
	console.log('ok');
	var sum1 = 0;
	setTimeout(function (){
		if($('.cart_products').length>0){
			var product_count = 0;
			$('.cart_products').each(function(){
				product_count++;
				var $this =$(this); 
				$qty = parseFloat($this.find( ".i-qty" ).val());
				$amount = parseFloat($this.find( ".i-price" ).val());
				sum1= sum1+($amount*$qty);
				//console.log($qty+' '+$amount);
			});

			$('.product-cart-num').html(product_count);
			$('#product_total').html('<b>AED '+sum1+'</b>');
			$('#product_sub_total').html('AED '+sum1);
			$('#product_grand_total').html('AED '+sum1);
		}
		else{
			$('.product-cart-wp').remove();
		}
		totalAmountBoth();
	}, 800);	
}
totalAmountProduct();
function totalAmountBoth(){
	var sum2 = 0;
	setTimeout(function (){
		if($('.cart_services').length>0||$('.cart_products').length>0){
			$('.cart_services').each(function(){
				var $this =$(this); 
				if(service_type=='salonee'){
					sum2 +=  parseFloat($this.find( ".salon_price" ).val());
				}
				else{
					sum2 +=  parseFloat($this.find( ".home_price" ).val());
				}
				//sum2 += parseFloat(this.value);
			});
			$('.cart_products').each(function(){
				var $this =$(this); 
				$qty = parseFloat($this.find( ".i-qty" ).val());
				$amount = parseFloat($this.find( ".i-price" ).val());
				sum2= sum2+($amount*$qty);
				//console.log($qty+' '+$amount);
			});
		
			$('#total_payable').html('<b>AED '+sum2+'</b>');
		}
		else{
			location.reload();
		}
	}, 800);	
}

function update_qty(id,qty_type){
	$(".cart-list-"+id).LoadingOverlay("show");
	$.ajax({
		type:'POST',
		url:'controllers/updateCarts.php',
		data:{cart_id:id,type:qty_type},
/*            contentType: false,
		processData: false,*/
		dataType: 'json',
		success:function(res){
			$(".cart-list-"+id).LoadingOverlay("hide");
			if(res.status=='ok'){
				$('.cart-list-'+id+' .i-qty').val(res.qty);
				gprice = parseFloat($('.cart-list-'+id+' .i-price').val());
				$('.cart-list-'+id+' .product_price1').html(res.qty*gprice);
				totalAmountProduct()
			}
			else{
				toastr.error(res.message, 'Error', {timeOut: 5000});
			}
		}
	});
}
function delete_cart(cart_id){
	$(".cart-list-"+cart_id).LoadingOverlay("show");
	$.ajax({
		type:'POST',
		url:'controllers/removeCart.php',
		data:{cart_id:cart_id,user_id:'<?=$user_id?>',type:''},
		dataType: 'json',
		success:function(res){
			$(".cart-list-"+cart_id).LoadingOverlay("hide");
			console.log(res);
			if(res.status=='ok'){
				$('.cart-list-'+cart_id).remove();
				totalAmountService();
				totalAmountProduct();
				iziToast.success({
					
					title: 'Success',
					message: 'Item deleted successfully.',
					position: 'topRight',
					displayMode: 2
				});
			}
			else{
				iziToast.error({
					title: 'Error',
					message: res.message,
					position: 'topRight',
					displayMode: 2
				});
			}
			//setInterval('location.reload()', 1500); 
		}
	});

}


jQuery(document).ready(function($){
    $('.help-button').on('click', function(e){
        e.preventDefault();
        $(this).siblings('.info-box').show();
        });

        $('.close-button').on('click', function(e){
            e.preventDefault();
            $(this).parents('.info-box').hide();
        });
        $('#c_success').hide();
        $('#c_fail').hide();
        $('#is-coupon').hide();
        $('#cancel-coupon').hide();
        $('#apply-coupon').on('click', function(e){
        if(($('#coupon').val() == '') || ($('#coupon').val() == undefined)){
          $('#c_fail').text('Enter Coupon Code');
          $('#c_fail').show();        
        }else{
            var formData = new FormData();        
            formData.append('coupon', $('#coupon').val());
            formData.append('date', $("#sDate").val());
            $.ajax({
                type:'POST',
                url:'controllers/check_coupon.php',
                data:formData,
                contentType: false,
                processData: false,
                success:function(res){
                    var res = JSON.parse(res);
                    if(res.status == 200)
                    {
                      $totalDiscount = $("#original_amount").val() - ((res.data / 100) * $("#original_amount").val());
                      $('#total_amount').val($totalDiscount)
                      $("#total_payable").val('AED '+$totalDiscount);
                      $('#c_success').text(res.message);
                      $('#c_success').show();
                      $('#c_fail').hide();
                    } else {
                      $("#price").val('Estimated Price: '+$("#original_amount").val()+' AED');
                      $('#total_amount').val($("#original_amount").val())
                      $('#c_fail').text(res.message);
                      $('#c_fail').show();
                      $('#c_success').hide();
                    }                              
                }
            });
        }
    });  
    $('#cancel-coupon').on('click', function(e){
        cancelCoupon();
    });  
});

function cancelCoupon() {
    $('#coupon').val('');
    $('#apply-coupon').show()
    $('#c_success').hide();
    $('#c_fail').hide();
    $('#cancel-coupon').hide()
}

$('#sDate').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight:true,
    autoclose:true,
    startDate:"<?php echo date("Y-m-d");?>",
    endDate :"<?php echo date("Y-m-d",strtotime("+20 days"));?>"

});

$('#bookformModal').click(function () {
    $('#bookModal').modal('hide');
    $('#type').val('salonee')
})

$('#bookformModal1').click(function () {
    $('#bookModal').modal('hide');
    $('#type').val('home')
})

$('#showSlots').click(function() {
    $('#slotsList').toggle('100');
    $(this).toggleClass("fa-caret-up fa-caret-down");
});

$("#sDate").change(function () { 

    var type = $("input[name=type]").val();
    var date = $(this).val();
    $('#is-coupon').show()
    $('#c_success').hide();
    $('#c_fail').hide();
    $('#coupon').val('');
    var formData = new FormData();

    formData.append('service_provider_id', service_provider_id);
    formData.append('date', date);
    formData.append('type', type);

    $.ajax({
        type:'POST',
        url:'controllers/get_slots_by_sp_id.php',
        data:formData,
        contentType: false,
        processData: false,
        success:function(res){
            $("#slotData").show();
            $('#slotsList').toggle('100');
            $("#slotData").html(res);                          
        }
    });
});

function pay()
{
    var user_id = "<?php echo @$_SESSION['user_id'];?>";

    if(user_id == '')
    {
        $('#bookModalForm').modal('hide');
        $('#loginModal').modal('show');
    }
    else
    {
        var slot_time = $('input:radio[name=slot_time]:checked').val();
        var service_id = $("input[name='service_id[]']")
              .map(function(){return $(this).val();}).get();
        var product_id = $("input[name='product_id[]']")
              .map(function(){return $(this).val();}).get();
        var quantity = $("input[name='quantity[]']")
              .map(function(){return $(this).val();}).get();
        var coupon_code = $("#coupon").val();
        var type = $("#type").val();
        var type = service_type;
        var slot_date = $("#sDate").val();

        if(slot_date == "")
        {
            alert('Please select Date');
            return false;
        }
        if(slot_time == undefined)
        {
            alert('Please select Slot');
            return false;
        }

        var url = 'payment/payment1.php'; 
        var form = $('<form action="' + url + '" method="post">' +
          '<input type="text" name="user_id" value="' + user_id + '" />' +
          '<input type="text" name="slot_date" value="' + slot_date + '" />' +
          '<input type="text" name="slot_time" value="' + slot_time + '" />' +
          '<input type="text" name="service_id" value="' + service_id + '" />' +
          '<input type="text" name="product_id" value="' + product_id + '" />' +
          '<input type="text" name="quantity" value="' + quantity + '" />' +
          '<input type="text" name="coupon_code" value="' + coupon_code + '" />' +              
          '<input type="text" name="service_provider_id" value="' + service_provider_id + '" />' +
          '<input type="text" name="type" value="' + type + '" />' +
          '</form>');
        $('body').append(form);
        form.submit();
    }
}
</script>
<?php
}
?>
