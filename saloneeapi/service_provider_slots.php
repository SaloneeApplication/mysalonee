<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();

	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	//Retrieving Form Fields
	$service_provider_branch_id = $request->service_provider_branch_id;	
	
	if($con)
	{
		$funcObject = new functions();
		$data = array();
		
		$res = $funcObject->serviceProviderSlots($con, $service_provider_branch_id);

		while($row = mysqli_fetch_array($res))
		{
			$slots = array();
			$slots["service_provider_slot_id"] = $row["service_provider_slot_id"];
			$slots["service_provider_id"] = $row["service_provider_id"];
			$slots["service_provider_branch_id"] = $row["service_provider_branch_id"];
			$slots["date"] = date('d-m-Y', strtotime($row["date"]));
			$slots["time"] = date('H:i A', strtotime($row["time"]));
			array_push($data,$slots);
		}
		
		$result = array("status"=>"200","slots"=>$data);
		echo  json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>