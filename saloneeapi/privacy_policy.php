<?php 

	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();

	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	//Retrieving Form Fields
	$country_id = $request->country_id;	
	$language = $request->language;	

	if($con)
	{
		$funcObject = new functions();
		$data = array();
		
		$res = $funcObject->getPrivacyPolicy($con, $country_id, $language);

		while($row = mysqli_fetch_array($res))
		{
			$privacy_policy = array();
			$privacy_policy["country_id"] = $row["country_id"];
			$privacy_policy["language"] = $row["language"];
			$privacy_policy["terms_condition"] = $row["terms_condition"];
			$privacy_policy["privacy_policy"] = $row["privacy_policy"];
			$privacy_policy["faq"] = $row["faq"];
			
			
			array_push($data,$privacy_policy);
		}
		
		$result = array("status"=>"200","privacy_policy"=>$data);
		
		echo  json_encode($result,JSON_INVALID_UTF8_IGNORE);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>