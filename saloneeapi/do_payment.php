<?php
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
    include('../payment/config.php');
    require_once dirname(__DIR__).'/core/Controller.php';
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	$user_id = $request->user_id;
	$slot_date = $request->slot_date;
	$slot_time = $request->slot_time;
	$service_id = $request->service_id;
	$service_provider_id = $request->service_provider_id;
	$service_type = $request->service_type;
    $order_id = time() . mt_rand() . $user_id;
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
	    $sql = "SELECT sale_price
    	FROM services_prices
    	WHERE service_provider_service_id = '$service_id' AND price_for = '$service_type'";
        $recordSet = mysqli_query($con,$sql);

        $data = array();
        while($row = mysqli_fetch_array($recordSet))
        {
            $total_amount = $row["sale_price"];
        }

        $slot_date = date('Y-m-d', strtotime($slot_date));
        $slot_time = date('H:i:s', strtotime($slot_time));

        $sql1 = "INSERT INTO service_slots(service_id, service_provider_id, user_id, total_amount, slot_date, service_type, order_id) 
                VALUES('$service_id', '$service_provider_id', '$user_id', '$total_amount', '$slot_date.$slot_time', '$service_type', '$order_id')";
        $recordSet1 = mysqli_query($con,$sql1);

        if( $recordSet1 ){
            $slot_id = $con->insert_id;
            
            $funcObject = new functions();
		    $recordSet = $funcObject->getUserDetails($user_id,$con);
		    $user = mysqli_fetch_array($recordSet);

            $ccavenue=array();
            $ccavenue['merchant_id']=MERCHANT_ID;
            $ccavenue['order_id']=$order_id;
            $ccavenue['amount']=$total_amount;
            $ccavenue['currency']="AED";
            $ccavenue['redirect_url']=REDIRECT_URL;
            $ccavenue['cancel_url']=CANCEL_URL;
            $ccavenue['billing_name']=$user['name'];
            $ccavenue['billing_email']=$user['email'];
            $ccavenue['billing_address']=$user['area'];
            $ccavenue['billing_city']='Solapur';
            $ccavenue['billing_tel']=$user['mobile'];
            $ccavenue['billing_country']='India';

            $merchant_data='';
            foreach ($ccavenue as $key => $value){
                $merchant_data.=$key.'='.$value.'&';
            }
            $controller = new Controller();
            $encrypted_data=$controller->ccavenue_encrypt($merchant_data,WORKING_KEY);

            $resp=array();
            $resp['status']='200';
            $resp['action']='https://secure.ccavenue.ae/transaction/transaction.do?command=initiateTransaction';
            $resp['access_code']=ACCESS_CODE;
            $resp['encRequest']=$encrypted_data;
            echo json_encode($resp);
            exit;

        } else {
            $result = array("status"=>'400',"message"=>"Something went wrong, Please try again.");
			echo json_encode($result);
        }
        
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>