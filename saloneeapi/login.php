<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	

require_once '../vendor/firebase/php-jwt/src/BeforeValidException.php';
require_once '../vendor/firebase/php-jwt/src/ExpiredException.php';
require_once '../vendor/firebase/php-jwt/src/SignatureInvalidException.php';
require_once '../vendor/firebase/php-jwt/src/JWT.php';

use \Firebase\JWT\JWT;

	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	$email = $request->email;
	$password = $request->password;
	$latitude = $request->latitude;
	$longitude = $request->longitude;
	$device_token = $request->device_token;

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		
		$message = $funcObject->userDetails($email,$password,$con);
		if($message == "correct")
		{
			$userId = $funcObject->getUserId($email,$con);

			//*****JWT Logic Start*******
			$secret_key = "Salonee";
			$issuer_claim = "Salonee"; // this can be the servername
			$audience_claim = "Salonee";
			$issuedat_claim = time(); // issued at
			$notbefore_claim = $issuedat_claim + 10; //not before in seconds
			$expire_claim = $issuedat_claim + 60; // expire time in seconds
			$token = array(
				"iss" => $issuer_claim,
				"aud" => $audience_claim,
				"iat" => $issuedat_claim,
				"nbf" => $notbefore_claim,
				"exp" => $expire_claim,
				"data" => array(
					"id" => $userId,
					"email" => $email,
			));
		
			http_response_code(200);
		
			$jwt = JWT::encode($token, $secret_key);
			//*******JWT Logic End*********
			
			$recordSet = $funcObject->getUserDetails($userId,$con);
			while($row = mysqli_fetch_array($recordSet))
			{
			    $username = $row["name"];
			    $email = $row["email"];
			    $mobile = $row["mobile"];
			    $image = $row["image"];
			    $area = $row["area"];
			    $gender = $row["gender"];
			    $dob = $row["dob"]!=""?$row["dob"]:"";
				$mobile_code = $row["mobile_code"];
			}
			$result = array(
				"status"=>'200',
				"message"=>"Logged In Successfully",
				"user_id"=>$userId,
				"username"=>$username, 
				"email" => $email, 
				"mobile"=>$mobile, 
				"image"=>$image, 
				"area"=>$area, 
				"gender"=>$gender, 
				"dob"=>$dob,
				"mobile_code"=> $mobile_code,
				"jwt" => $jwt,
				"expireAt" => $expire_claim
			);

			// update latitude longitude device_token
			$sql = "UPDATE user SET latitude = '$latitude', longitude = '$longitude', device_token = '$device_token' WHERE user_id = '$userId' ";
			$res = mysqli_query($con, $sql);

			echo  json_encode($result);
		}
		else
		{
			$result = array("status"=>'400',"message"=>$message);
    		echo  json_encode($result);
		}
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>