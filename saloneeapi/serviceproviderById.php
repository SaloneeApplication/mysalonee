<?php 

	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	$service_provider_id = $request->service_provider_id;

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();

	if($con)
	{
		$funcObject = new functions();
		
		$data = array();
		
		$recordSet = $funcObject->getServiceProviderByID($con,$service_provider_id);
		while($row = mysqli_fetch_array($recordSet))
		{
			$service_provider = array();
			$service_provider["service_provider_id"] = $row["service_provider_id"];
			$service_provider["name"] = $row["name"];
			$service_provider["business_name"] = $row["business_name"];			
			$service_provider["email"] = $row["email"];
			$service_provider["mobile"] = $row["mobile"];
			$service_provider["image"] = $row["image"];			
			$service_provider["address"] = $row["address"];
			
			array_push($data,$service_provider);
		}
		
		$result = array("status"=>"200","serviceprovider"=>$data);
		echo json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}

?>