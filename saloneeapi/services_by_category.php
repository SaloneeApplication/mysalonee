<?php 

	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();

	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	//Retrieving Form Fields
	$category_id = $request->category_id;	
	
	if($con)
	{
		$funcObject = new functions();
		$data = array();
		
		$res = $funcObject->servicesByCategory($con, $category_id);

		while($row = mysqli_fetch_array($res))
		{
			$services = array();
			$services["service_provider_id"] = $row["service_provider_id"];
			$services["business_name"] = $row["business_name"];
			$services["service_name"] = $row["service_name"];
			$services["service_image"] = $row["service_image"];
			$services["service_at"] = $row["service_at"];			
			$services["open_time"] = $row["open_time"];
			$services["close_time"] = $row["close_time"];
			$services["rating"] = $row["rating"];
			$services["shop_id"] = $row["shop_id"];
			$services["gender"] = $row["gender"];
			
			array_push($data,$services);
		}
		
		$result = array("status"=>"200","services"=>$data);
		echo  json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>