<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	
    $service_provider_id = $request->service_provider_id;
    $user_id = $request->user_id;
    
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		$data = array();
		$data1 = array();
		$data2 = array();
		
      

		$res = $funcObject->shopdetailsList($con,$service_provider_id);
		while($row = mysqli_fetch_array($res))
		{        
//                        $slots = [];
//                        $servicetimeresl = $funcObject->serviceTimes($con,$service_provider_id); 
//                        if ($servicetimeresl && mysqli_num_rows($servicetimeresl) > 0) {
//                            while($timerow = mysqli_fetch_array($servicetimeresl)) {
//                                $day_slots = [];
//                                $service_from_time = NULL;
//                                $service_to_time = NULL;
//                                if ($timerow['from_time']) {
//                                    $service_from_time = $timerow['from_time'];                                    
//                                }
//                                if ($timerow['to_time']) {
//                                    $service_to_time = $timerow['to_time'];                                    
//                                }
//                                $day_slots = ["day_name" => $timerow['day_name'], "from_time" => $service_from_time, "to_time" => $service_to_time, "availability" => $timerow['available_status']];
//                                array_push($slots, $day_slots);
//                            }
//                        }
                        $shopdetails = array();
			$shopdetails["service_provider_id"] = $row["service_provider_id"];
			$shopdetails["business_name"] = $row["business_name"];
			$shopdetails["image"] = $row["image"];
			$shopdetails["address"] = $row["address"];
//			$shopdetails["available_slots"] = $slots;
			
            $shopTimeRes = NULL;
            $shopTimeRecordSet = $funcObject->shopWeekDayAvailability($con, $service_provider_id);
            if($shopTimeRecordSet && mysqli_num_rows($shopTimeRecordSet) > 0){
                $shopTimeRes = mysqli_fetch_assoc($shopTimeRecordSet);
            }
            $shopdetails["timings"] = $shopTimeRes;
            
            $ratingRes = [];
            $ratingRecordSet = $funcObject->getProviderRating($con, $service_provider_id);
            if ($ratingRecordSet && mysqli_num_rows($ratingRecordSet) > 0) {
                while($ratingrow = mysqli_fetch_assoc($ratingRecordSet)) {
                    array_push($ratingRes, $ratingrow);
                }
            }                       
            $shopdetails["rating"] = $ratingRes;
                        
			array_push($data,$shopdetails);

			
		}


		$res1 = $funcObject->ourservicesList($con,$service_provider_id);
		while($row = mysqli_fetch_array($res1))
		{       
                        $service_at_home_price = NULL;
                        $service_at_salon_price = NULL;
                        $priceres1 = $funcObject->servicecharge($con,$row["service_provider_service_id"], $row["service_at"]); 
                        if ($priceres1 && mysqli_num_rows($priceres1) > 0) {
                            while($pricerow = mysqli_fetch_array($priceres1)) {
                                if ($pricerow['price_for'] === "home") {
                                    $service_at_home_price = $pricerow['sales_price'];                                    
                                } else if ($pricerow['price_for'] === "salonee") {
                                    $service_at_salon_price = $pricerow['sales_price'];                                    
                                }
                            }
                        }
                        $ourservices = array();
			$ourservices["service_id"] = $row["service_provider_service_id"];
			$ourservices["service_provider_id"] = $row["service_provider_id"];
			$ourservices["service_name"] = $row["service_name"];
            $ourservices["image"] = $row["image"];
            $ourservices["description"] = $row["description"];
            $ourservices["category_id"] = $row["category_id"];
            $ourservices["service_at"] = $row["service_at"];
            $ourservices["service_at_home_price"] = $service_at_home_price;
            $ourservices["service_at_salon_price"] = $service_at_salon_price;
            $cartStatusRes = $funcObject->checkItemInCart($con,$service_provider_id, $user_id, 'service', $row["service_provider_service_id"]);
            $ourservices["cart_status"] = $cartStatusRes;

          
			array_push($data1,$ourservices);
		}

		$res2 = $funcObject->ourproductsList($con,$service_provider_id);

		while($res2 && $row = mysqli_fetch_array($res2))
		{
			$ourproducts = array();
			$ourproducts["sale_price"] = $row["sale_price"];
			$ourproducts["product_title"] = $row["product_title"];
			$ourproducts["product_image"] = $row["product_image"];
            $ourproducts["base_price"] = $row["base_price"];
			$ourproducts["product_description"] = $row["product_description"];
            $ourproducts["product_id"] = $row["id"];
			$cartStatusRes = $funcObject->checkItemInCart($con,$service_provider_id, $user_id, 'product', $row["id"]);
            $ourproducts["cart_status"] = $cartStatusRes;

			array_push($data2,$ourproducts);
		}

	
		
		$result = array("status"=>"200", "shop_details"=>$data, "ourservices"=>$data1, "ourproducts"=>$data2);
		echo json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>