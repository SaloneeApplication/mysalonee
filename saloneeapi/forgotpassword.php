<?php
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require '../vendor/autoload.php';
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	$email = $request->email;
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
	    $funcObject = new functions();
		$emailExistence = $funcObject->checkUserEmail($email,$con);
		
		if($emailExistence)
		{
			$recordSet = $funcObject->getUserDataByEmail($email,$con);
			while($row = mysqli_fetch_array($recordSet))
			{
				$name = $row["name"];
				$email = $row["email"];
				$password = $row["password"];
			}

			$to = $email; 
			$from = 'no-reply@salonee.com'; 
			$fromName = 'Salonee'; 
				
			$subject = "Salonee Password Recovery Email"; 
				
			$htmlContent = ' 
				<html lang="en">

				<head>
					<meta charset="utf-8">
					<title>Gigx</title>
				
				
					<meta name="viewport" content="width=device-width, initial-scale=1">
					<link rel="shortcut icon" type="image/ico" href="http://salonee.com/assets/images/favicon.png" />
					<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
				
				</head>
				
				<body>
				
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
								<img src="http://salonee.com/assets/images/logo.png" class="img-fluid">
						</div>
						<div class="col-md-12">
						<h1>Hi '.$name.',</h1>
						
						<p>You recently requested for password recovery of your GigX Company Account.</p>
						<p>Please access your account with the the following credentials : </p>
						<p><b>Login Id : </b> '.$email.'</p>
						<p><b>Password : </b> '.$password.'</p>
						<p>For further enquiries please email us at <a href = "mailto:ondemand@salonee.com">ondemand@salonee.com</a> or <a href = "mailto:expert@salonee.com">expert@salonee.com</a></p>
						<p>We look forward to working for you! The <a href= "http://salonee.com">salonee.com</a> Team.</p>
						<p>This is  an automated email, Please do not reply to this email.</p>
						</div>
					</div>
				</div>
				
				
				
				
				</body>
				<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
				<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
				<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
				<!-- <script src="http://salonee.com/assets/js/bootstrap.js"></script> -->
				
				</html>'; 
				
			
			$mail = new PHPMailer(true);
			try {
			    $mail->SMTPDebug = 0;                                       
			    $mail->isSMTP();                                            
			    $mail->Host       = 'smtp.gmail.com';                    
			    $mail->SMTPAuth   = true;                             
			    $mail->Username   = 'akshay.markacommunications@gmail.com';                 
			    $mail->Password   = 'Akshay@123';                        
			    $mail->SMTPSecure = 'tls';                              
			    $mail->Port       = 587;  
			    $mail->setFrom( $from, $fromName );
			    $mail->addAddress($email);
			    $mail->isHTML(true);                                  
			    $mail->Subject = $subject;
			    $mail->Body    = $htmlContent;
			    $mail->send();
				$result = array("status"=>'200',"message"=>"Please check your email for password recovery");
			} catch (Exception $e) {
				$result = array("status"=>'400',"message"=>"It seems you dont have an account with us! Please Sign Up to create an account");
			}
			echo json_encode($result);
		
		} else {
			$result = array("status"=>'400',"message"=>"This Email ID is not availabale with us.");
			echo json_encode($result);
		}
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>