<?php 

	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();

	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
		
	
	if($con)
	{
		$funcObject = new functions();
		$data = array();
		
		$res = $funcObject->getPromoCode($con);

		while($row = mysqli_fetch_array($res))
		{
			$promocode = array();
			$promocode["coupon_code"] = $row["coupon_code"];
			$promocode["discount_per"] = $row["discount_per"];
			$promocode["only_for"] = $row["only_for"];
			$promocode["image"] = $row["image"];
			$promocode["short_desc"] = $row["short_desc"];
			
			array_push($data,$promocode);
		}
		
		$result = array("status"=>"200","promocode"=>$data);
		echo  json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>