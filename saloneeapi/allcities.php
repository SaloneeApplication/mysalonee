<?php 

	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		
		$data = array();
		
		$recordSet = $funcObject->getAllCities($con);
		while($row = mysqli_fetch_array($recordSet))
		{
			$countries = array();
			$countries["id"] = $row["id"];
			$countries["country_id"] = $row["country_id"];
			$countries["name"] = $row["name"];			
			$countries["image"] = $row["image"];
			
			array_push($data,$countries);
		}
		
		$result = array("status"=>"200","cities"=>$data);
		echo json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}

?>