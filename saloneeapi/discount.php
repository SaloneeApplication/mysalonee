<?php 

	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();

	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	//Retrieving Form Fields
	$coupon_code = $request->coupon_code;	
	
	if($con)
	{
		$funcObject = new functions();
		$data = array();
		
		$res = $funcObject->getDiscountByPromoCode($con, $coupon_code);

		while($row = mysqli_fetch_array($res))
		{
			$discount = array();
			$discount["coupon_code"] = $row["coupon_code"];
			$discount["discount_per"] = $row["discount_per"];
			$discount["only_for"] = $row["only_for"];
			
			array_push($data,$discount);
		}
		
		$result = array("status"=>"200","discount"=>$data);
		echo  json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>