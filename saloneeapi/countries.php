<?php 

	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		
		$data = array();
		
		$recordSet = $funcObject->getCountries($con);
		while($row = mysqli_fetch_array($recordSet))
		{
			$countries = array();
			$countries["country_id"] = $row["id"];
			$countries["country_name"] = $row["country_name"];			
			$countries["telephone_code"] = $row["telephone_code"];
			$countries["language"] = $row["language"];
			$countries["flag"] = $row["country_flag"];
			
			array_push($data,$countries);
		}
		
		$result = array("status"=>"200","countries"=>$data);
		echo json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}

?>