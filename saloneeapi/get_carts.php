<?php 

	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();

	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	//Retrieving Form Cart
	$user_id = $request->user_id;	
	
	if($con)
	{
		$funcObject = new functions();
		$service_data = array();
		$product_data = array();
		
		$res = $funcObject->getCartServiceList($con, $user_id);

		while($row = mysqli_fetch_array($res))
		{
			$services = array();
			$services["cart_id"] = $row["cart_id"];
			$services["quantity"] = $row["quantity"];
			$services["salon_price"] = $row["salon_price"];
			$services["home_price"] = $row["home_price"];
			$services["service_id"] = $row["service_id"];
			$services["service_name"] = $row["service_name"];
			$services["service_image"] = $row["image"];
			// $services["base_salon_price"] = $row["base_salon_price"];			
			// $services["base_home_price"] = $row["base_home_price"];
			// $services["salon_price"] = $row["salon_price"];
			// $services["home_price"] = $row["home_price"];
			
			array_push($service_data,$services);
		} 

		$res = $funcObject->getCartProductList($con, $user_id);

		while($row = mysqli_fetch_array($res))
		{ 
			$products = array();
			$products["cart_id"] = $row["cart_id"];
			$products["quantity"] = $row["quantity"];
			$products["amount"] = $row["amount"];
			$products["product_id"] = $row["product_id"];
			$products["product_title"] = $row["product_title"];
			$products["product_image"] = $row["product_image"];
			// $products["base_price"] = $row["base_price"];			
			// $products["sale_price"] = $row["sale_price"];
			 
			array_push($product_data,$products);
		}

		
		$result = array("status"=>"200","services"=>$service_data,"products"=>$product_data);
		echo  json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>
