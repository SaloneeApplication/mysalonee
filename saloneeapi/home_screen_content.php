<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		$data = array();
		$data1 = array();
		$data2 = array();
		$data3 = array();
		
		$res = $funcObject->categoriesList($con);

		while($row = mysqli_fetch_array($res))
		{
			$categories = array();
			$categories["category_id"] = $row["category_id"];
			$categories["name"] = $row["name"];
			$categories["category_for"] = $row["category_for"];
			$categories["short_description"] = $row["short_description"];
			$categories["image"] = $row["image"];
			$categories["mobile_image"] = $row["mobile_image"];
			array_push($data,$categories);
		}

		$res1 = $funcObject->banners($con);

		while($row = mysqli_fetch_array($res1))
		{
			$banners = array();
			$banners["id"] = $row["id"];
			$banners["image"] = $row["image"];
			array_push($data1,$banners);
		}

		$res2 = $funcObject->shopList($con);

		while($row = mysqli_fetch_array($res2))
		{
			$shop_list = array();
			$shop_list["shop_id"] = $row["service_provider_id"];
			$shop_list["shop_name"] = $row["business_name"];
			$shop_list["image"] = $row["image"];
			$shop_list["rating"] = '4.2';
			$shop_list["latitude"] = $row["latitude"];
			$shop_list["longitude"] = $row["longitude"];
			array_push($data2,$shop_list);
		}

		$res3 = $funcObject->customerSpeaks($con);

		while($row = mysqli_fetch_array($res3))
		{
			$customer_speaks = array();
			$customer_speaks["image"] = '/uploads/'.$row["image"];
			array_push($data3,$customer_speaks);
		}
		
		$result = array("status"=>"200", "categories"=>$data, "banners"=>$data1, "near_by_shops"=>$data2, 'customer_speaks'=>$data3);
		echo json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>