<?php 
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	//Retrieving Form Fields
	$mobile_code = $request->mobile_code;
	$mobile = $request->mobile;
	
	$otp = generateNumericOTP(4);
	$message = "Use $otp as your verification code on Salonee. Do not share your OTP with anyone.";

	$url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=ee7dzmQudDi&MobileNo=$mobile_code$mobile&SenderID=TESTID&Message=".urlencode($message)."&ServiceName=INTERNATIONAL";

	
    $ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$output = curl_exec($ch); 
    curl_close($ch);

	$response = explode(':', $output);
	$status = array_pop($response);
	
	if (strpos($status, 'success') !== false) {
		$result = array( "status" => "200", "otp" => $otp );
	} else {
		$result = array( "status" => "400", "message" => $output );
	}
	echo json_encode($result);
	
	function generateNumericOTP($n) 
	{
		$generator = "1357902468";
	    $result = "";
	  
	    for ($i = 1; $i <= $n; $i++) {
	        $result .= substr($generator, (rand()%(strlen($generator))), 1);
	    }
	  
	    // Return result
	    return $result;
	}

?>