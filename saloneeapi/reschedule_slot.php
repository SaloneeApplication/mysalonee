<?php 

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
include("dbConnection.php");
include("functions.php");

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);  

$dbObject = new dbConnection();

$con = $dbObject->getConnection();
	
$user_id = $request->user_id;
$slot_id = $request->slot_id;
$service_provider_id = $request->service_provider_id;
$slot_time = $request->slot_time;
$slot_date = $request->slot_date;

$result = [];

    $sql = "SELECT s.*,sp.time_slot_freq,p.same_time_slots_allowed,p.allowed_slots_count,p.per_day_slots_limit,p.price_for 
            FROM service_slots s, service_provider_services sp ,services_prices p 
            WHERE s.service_id = sp.service_provider_service_id 
            AND sp.service_provider_service_id = p.service_provider_service_id 
            AND s.slot_id = '$slot_id'
            AND sp.service_provider_id = '$service_provider_id'
            HAVING p.price_for = s.service_type";
    $recordSet = mysqli_query($con,$sql);

    while($row = mysqli_fetch_array($recordSet))
    {
        $freq = $row['time_slot_freq'];
        $service_type = $row['service_type'];
        $service_id = $row['service_id'];
        $same_time_slots_allowed = $row['same_time_slots_allowed'];
        $allowed_slots_count = $row['allowed_slots_count'];
        $per_day_slots_limit = $row['per_day_slots_limit'];
    }

    $sql1 = "SELECT * from user where user_id = '$user_id'";
    $recordSet1 = mysqli_query($con,$sql1);

    while($user = mysqli_fetch_array($recordSet1))
    {

    }

    $act_check_time=$slot_date." ".date("H:i",strtotime($slot_time)).":00";

    $sql2 = "SELECT slot_id from service_slots 
            where service_id = '$service_id' and slot_date='$act_check_time' and service_type='$service_type'";

    $recordSet2 = mysqli_query($con,$sql2);
    $total_booked_slots = mysqli_num_rows($recordSet2);

    if($total_booked_slots>=$per_day_slots_limit && $per_day_slots_limit!=0)
    {
        //per day slots booking limit over 
        $result['status'] = 400;
        $result['message'] = 'Slot not available';
    }
    else
    {
        //if same time slot not allowed 
        if($same_time_slots_allowed==0)
        {
            if($total_booked_slots>0)
            {
                $result['status'] = 400;
                $result['message'] = 'Slot not available';
            }
            else
            {
                $sql3 = "UPDATE service_slots SET slot_date='$act_check_time', booking_type='rescheduled' 
                        WHERE slot_id='$slot_id' and service_provider_id='$service_provider_id'";
                        
                $rowsAffected3 = mysqli_query($con,$sql3);

                $result['status'] = 200;
                $result['message'] = 'Slot rescheduled successfully';

                /*$message='';
                $message.='Hi '.ucwords($user['name']).'<br/>';
                $message.='Your slot has been rescheduled to '.$act_check_time.'<br/>';
                $message.='<br/>Thanking you';
                $this->phpmailer->sendMail($user['email'],"Slot has been rescheduled.",$message);*/
            }
        }
        else
        {
            if($allowed_slots_count < $total_booked_slots)
            {
                //allowed limit over slots
                $result['status'] = 400;
                $result['message'] = 'Slot not available';
            }
            else
            {
                $sql4 = "UPDATE service_slots SET slot_date='$act_check_time', booking_type='rescheduled' 
                WHERE slot_id='$slot_id' and service_provider_id='$service_provider_id'";

                $rowsAffected4 = mysqli_query($con,$sql4);

                $result['status'] = 200;
                $result['message'] = 'Slot rescheduled successfully';
                
                /*$message='';
                $message.='Hi '.ucwords($user['name']).'<br/>';
                $message.='Your slot has been rescheduled to '.$act_check_time.'<br/>';
                $message.='<br/>Thanking you';
                $this->phpmailer->sendMail($user['email'],"Slot has been rescheduled.",$message);*/
            }
        }
    }

    echo json_encode($result);
?>