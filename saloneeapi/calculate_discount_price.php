<?php 

	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();

	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);
	
	//Retrieving Form Fields
	$promo_code = $request->promo_code;	

	$base_price = $request->base_price;	
	$service_provider_id = $request->service_provider_id;	
	
	if($con)
	{
		$funcObject = new functions();
		$data = array();
		
		$res = $funcObject->getPromoCodeDeatils($con, $promo_code);
		$row = mysqli_fetch_array($res);
	    $discount = $row["discount_per"];
		$expiry_date = $row["expiry_date"];
		
		if($expiry_date>= date('Y-m-d')){
			$calculated_price = array();
			$discount_price = $base_price-($base_price*$discount/100);
			$calculated_price["discount_price"] = $discount_price;
			array_push($data,$calculated_price);
			$result = array("status"=>"200","calculated_price"=>$data);
			echo  json_encode($result);
		}else{
			$data['error'] =  "Invalid Promo Code";
			$result = array("status"=>"400","error"=>$data);
			echo  json_encode($result);
		}		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>