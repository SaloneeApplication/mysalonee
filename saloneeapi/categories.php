<?php 
	header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
	include("dbConnection.php");
	include("functions.php");
	
	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);	
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		$data = array();
		
		$res = $funcObject->categoriesList($con);

		while($row = mysqli_fetch_array($res))
		{
			$categories = array();
			$categories["category_id"] = $row["category_id"];
			$categories["name"] = $row["name"];
			$categories["category_for"] = $row["category_for"];
			$categories["short_description"] = $row["short_description"];
			$categories["image"] = $row["image"];
			$categories["mobile_image"] = $row["mobile_image"];
			array_push($data,$categories);
		}
		
		$result = array("status"=>"200","categories"=>$data);
		echo  json_encode($result);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>