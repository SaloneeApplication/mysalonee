<?php 

require_once '../vendor/firebase/php-jwt/src/BeforeValidException.php';
require_once '../vendor/firebase/php-jwt/src/ExpiredException.php';
require_once '../vendor/firebase/php-jwt/src/SignatureInvalidException.php';
require_once '../vendor/firebase/php-jwt/src/JWT.php';
use \Firebase\JWT\JWT;

class functions
{
	public function checkUserMobile($mobile,$con)
    {
        $sql = "select count(*) from user where mobile = '".$mobile."'";

        $recordSet = mysqli_query($con,$sql);
        while($row = mysqli_fetch_array($recordSet))
        {
            $count = $row[0];
        }
        if($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }     
    }

	public function checkUserEmail($email,$con)
	{
        $sql = "select count(*) from user where email = '".$email."'";

        $recordSet = mysqli_query($con,$sql);
        while($row = mysqli_fetch_array($recordSet))
        {
            $count = $row[0];
        }
        if($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }     
	}
		
	public function userDetails($email,$password,$con)
	{
		$emailExistence = $this->checkUserEmail($email,$con);
		if($emailExistence)
		{
			$sql = "select password from user where email = '".$email."'";
			$recordSet = mysqli_query($con,$sql);
			while($row = mysqli_fetch_array($recordSet))
			{
				$dbPassword = $row[0];
			}
			if($dbPassword == $password)
			{
				return "correct";
			}
			else
			{
				return "Incorrect Password";
			}
		}
		else
		{
			return "Incorrect Email";
		}
	}
		
	public function getUserId($email,$con)
	{
		$sql = "select user_id from user where email = '".$email."'";
		$recordSet = mysqli_query($con,$sql);
		while($row = mysqli_fetch_array($recordSet))
		{
			$talentId = $row[0];
		}
		return $talentId;
	}
			
	public function getUserDetails($userId,$con)
	{
	    $sql = "SELECT * from user where user_id = $userId";
	    $recordSet = mysqli_query($con,$sql);
	    return $recordSet;
	}

    public function categoriesList($con)
    {
        $sql = "SELECT * from category where status=1 order by name";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }
	
	public function getUserDataByEmail($email,$con)
	{
	    $sql = "SELECT * from user where email = '$email'";
	    $recordSet = mysqli_query($con,$sql);
	    return $recordSet;
	}

    public function banners($con)
    {
        $sql = "SELECT * from banners where status=1 order by image";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }

    public function customerSpeaks($con)
    {
        $sql = "SELECT * from customer_speaks WHERE status = 1";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }

    public function shopList( $con, $latitude = false, $longitude = false, $distance = false )
    {
        $sql = "SELECT service_provider_id,latitude,longitude, address, business_name, image FROM service_provider WHERE status = 1";

        if( $latitude && $longitude && $distance ){
                $sql = "SELECT service_provider_id, latitude, longitude, address, business_name, image, SQRT( POW(69.1 * (latitude - $latitude), 2) + POW(69.1 * ($longitude - longitude) * COS(latitude / 57.3), 2)) AS distance FROM service_provider HAVING distance < $distance ORDER BY distance";
        }
		
        $recordSet = mysqli_query($con,$sql);        
        return $recordSet;
    }

    public function shopWeekDayAvailability($con, $service_provider_id)
    { 
        $currentDayNum = date('w');
        $currentday = "";
        if ($currentDayNum == 0) {
            $currentday = "sunday";            
        } else if ($currentDayNum == 1) {
            $currentday = "monday";            
        } else if ($currentDayNum == 2) {
            $currentday = "tuesday";            
        } else if ($currentDayNum == 3) {
            $currentday = "wednesday";            
        } else if ($currentDayNum == 4) {
            $currentday = "thursday";            
        } else if ($currentDayNum == 5) {
            $currentday = "friday";            
        } else if ($currentDayNum == 6) {
            $currentday = "saturday";            
        }
        $sql = "SELECT id, day_name, from_time, to_time, available_status FROM `service_provider_timings` WHERE service_provider_id = '$service_provider_id' AND day_name ='$currentday' LIMIT 1";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }

    public function shopDetails($con, $shop_id)
    {
        $sql = "SELECT * FROM service_provider WHERE service_provider_id = '$shop_id'";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }

    public function servicesByCategory($con, $category_id)
    {
        $sql = "select sp.service_provider_id,sp.business_name,sps.service_name,sps.image as service_image,sps.service_at,spt.day_name,spt.from_time as open_time,spt.to_time as close_time,r.rating,sp.service_provider_id as shop_id,c.category_for as gender from service_provider_categories spc LEFT JOIN category c on spc.category_id=c.category_id JOIN service_provider sp on spc.service_provider_id=sp.service_provider_id LEFT JOIN service_provider_services sps on sp.service_provider_id=sps.service_provider_id LEFT JOIN service_provider_timings spt on sp.service_provider_id=spt.service_provider_id LEFT JOIN ratings r on sp.service_provider_id=r.service_provider_id where spc.category_id='$category_id' group by sp.service_provider_id";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }

    public function serviceProviderSlots($con, $service_provider_branch_id)
    {
        $sql = "SELECT service_provider_slot_id, service_provider_id, service_provider_branch_id, date, time
        		FROM service_provider_slots 
        		WHERE service_provider_branch_id = '$service_provider_branch_id'";
        $recordSet = mysqli_query($con,$sql);
        return $recordSet;
    }
		
	public function getCountries($con)
	{
	    $sql = "select * from service_countries where status=1 order by country_name";
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}
	
	public function getAllCities($con)
	{
	    $sql = "select * from service_cities where status=1 order by name";
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}
	
	public function getServiceProviderByID($con, $service_provider_id)
	{
	    $sql = "select * from service_provider WHERE service_provider_id = '$service_provider_id' and status=1";
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}
	
	public function getCities($con, $country_id)
	{
	    $sql = "select * from service_cities WHERE country_id = '$country_id' and status=1";
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}

	public function appIntro($con)
	{
	    $sql = "select * from app_intro";
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}

	public function changeUserPassword($user_id, $newPassword, $con)
	{
		$sql = "update user set password = '".$newPassword."' where user_id = $user_id";
	    $rowsAffected = mysqli_query($con,$sql);
		
		if($rowsAffected > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public function getDiscountByPromoCode($con, $coupon_code)
	{
	    $sql = "select * from promocodes WHERE coupon_code = '$coupon_code' and status=1";
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}	
	
	public function getPromoCode($con){
	$sql = "select * from promocodes WHERE status=1 and service_provider_id=0";
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}
	public function getPromoCodeDeatils($con,$coupon_code){
	$sql = "select * from promocodes WHERE status=1 and service_provider_id=0 and coupon_code='$coupon_code'" ;
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}
	public function getPrivacyPolicy($con, $country_id, $language){
	$sql = "select * from privacy_policy WHERE country_id='$country_id' and language='$language'";
	    $recordSet = mysqli_query($con,$sql);
		return $recordSet;
	}




/* new code */


public function shopdetailsList($con, $service_provider_id)
		{
			$sql = "select * from service_provider WHERE service_provider_id = '$service_provider_id' and status=1";
			$recordSet = mysqli_query($con,$sql);
			return $recordSet;
		}

        public function getProviderRating($con, $service_provider_id)
		{
            $sql = "
                SELECT      id, rating, comment, u.name AS userName
                FROM        ratings r 
                LEFT JOIN   user u ON r.user_id = u.user_id
                WHERE       r.service_provider_id = '$service_provider_id' AND r.status = 1;
            ";
            $recordSet = mysqli_query($con,$sql);
            return $recordSet;
		}
	

		public function ourservicesList($con,$service_provider_id)
		{
			$sql = "SELECT * from service_provider_services WHERE service_provider_id = '$service_provider_id' and status=1";
                        $recordSet = mysqli_query($con,$sql);
			return $recordSet;
		}

		public function ourproductsList($con,$service_provider_id)
		{
			$sql = "SELECT * from products WHERE service_provider_id = '$service_provider_id'";
			$recordSet = mysqli_query($con,$sql);
			return $recordSet;
		}
		public function getCartServiceList($con,$user_id)
		{
		    $sql = "SELECT c.cart_id,c.quantity,ss.service_provider_service_id as service_id, ss.service_name, ss.image, ss.service_provider_id, c.amount as salon_price, c.home_price
            FROM cart c
			JOIN service_provider_services ss ON ss.service_provider_service_id = c.service_id
			JOIN service_provider sp ON sp.service_provider_id = ss.service_provider_id 
			WHERE c.type = 'service' AND c.user_id = '$user_id' AND c.status='1'";
			$recordSet = mysqli_query($con,$sql);
			return $recordSet;
		}

		public function getCartProductList($con,$user_id)
		{
		    $sql = "SELECT c.cart_id,c.quantity, p.id as product_id, p.product_title, p.product_image, c.amount FROM cart c
			JOIN products p ON p.id = c.product_id
			WHERE c.type = 'product' AND c.user_id = '$user_id' AND c.status = 1";
			$recordSet = mysqli_query($con,$sql);
			return $recordSet;
		}
                
                public function servicecharge($con,$service_id,$type=NULL)
                {
                        $sql = "SELECT * FROM services_special_prices WHERE service_provider_service_id = '$service_id' AND price_for = '$type'";
                        $recordSet = mysqli_query($con,$sql);
                        return $recordSet;
                }

                public function serviceTimes($con, $service_provider_id)
		{
			$sql = "select * from service_provider_timings WHERE service_provider_id = '$service_provider_id'";
			$recordSet = mysqli_query($con,$sql);
			return $recordSet;
		}
		
		public function booking_history($con, $user_id)
        {
            $sql = "
                SELECT
                    ss.slot_id,
                    ss.total_amount,
                    ss.slot_date,
                    ss.service_ids,
                    sps.service_provider_service_id,
                    sps.service_name,
                    sp.name,
                    sp.business_name,
                    sp.image AS shop_image,
                    sp.address,
                    sp.city,
                    sp.country_code
                FROM service_slots ss
                JOIN service_provider_services sps ON sps.service_provider_service_id = ss.service_id
                JOIN service_provider sp ON sp.service_provider_id = ss.service_provider_id
                WHERE ss.user_id = '$user_id'
                ORDER BY ss.created_time DESC;
            ";
            $recordSet = mysqli_query($con,$sql);
            $results = [];
            while($row = mysqli_fetch_assoc($recordSet))
            {
                $services = '';
                $products = '';
                $slot_id = $row['slot_id'];
                $service_ids = $row['service_ids'];
                if($service_ids) {
                    $servicesQry = "SELECT service_name FROM service_provider_services WHERE service_provider_service_id IN ($service_ids);";
                    $serviceSet = mysqli_query($con,$servicesQry);
                    while($service_row = mysqli_fetch_array($serviceSet)) {
                        $services .= $service_row['service_name'] .', ';
                    }
                }

                $productsQry = "
                    SELECT  p.product_title as product_name
                    FROM    orders o 
                    JOIN    service_slots_item ssi ON o.service_slot_id = ssi.slot_id AND ssi.type = 'product'
                    JOIN    products p ON ssi.product_id = p.id
                    WHERE   o.service_slot_id = '$slot_id';
                ";
                $productSet = mysqli_query($con, $productsQry);
                while($prod_row = mysqli_fetch_array($productSet)) {
                    $products .= $prod_row['product_name'] .', ';
                }
                $row['services'] = $services;
                $row['products'] = $products;
                array_push($results, $row);
            }
            return $results;
        }
    
        public function booking_details($con, $slot_id)
        {
            $sql = "
                SELECT   ss.slot_id, ss.service_ids
                FROM     service_slots ss
                WHERE    ss.slot_id = '$slot_id'
                ORDER BY ss.created_time DESC;
            ";
            $recordSet = mysqli_query($con,$sql);
            $results = [];
            while($row = mysqli_fetch_array($recordSet))
            {
                $services = '';
                $products = '';
                $slot_id = $row['slot_id'];
//                    $service_ids = $row['service_ids'];
                if($slot_id) {
                    $servicesQry = "
                        SELECT  sps.service_provider_service_id,
                                sps.service_name,
                                sps.image AS service_image,
                                sp.business_name,
                                sp.address,
                                ssi.amount,
                                ssi.qty
                        FROM    service_slots_item ssi
                        JOIN    service_provider_services sps ON ssi.service_id = sps.service_provider_service_id
                        JOIN    service_provider sp ON sp.service_provider_id = sps.service_provider_id
                        WHERE   ssi.slot_id = '$slot_id' and ssi.type = 'service';
                    ";
                    $serviceSet = mysqli_query($con,$servicesQry);
                    if($serviceSet && mysqli_num_rows($serviceSet) > 0) {
                        $services = mysqli_fetch_all($serviceSet, MYSQLI_ASSOC);
                        $results['services'] = $services;  
                    }
                }

                $productsQry = "
                    SELECT  p.product_title as product_name,
                            p.sale_price as product_price,
                            p.product_image AS product_image,
                            sp.business_name,
                            sp.address,
                            ssi.amount,
                            ssi.qty
                    FROM    service_slots_item ssi
                    JOIN    products p ON ssi.product_id = p.id
                    JOIN    orders o on ssi.slot_id = o.service_slot_id
                    JOIN    service_provider sp ON sp.service_provider_id = o.service_provider_id
                    WHERE   ssi.slot_id = '$slot_id' and ssi.type = 'product';
                ";
                $productSet = mysqli_query($con,$productsQry);
                if($productSet && mysqli_num_rows($productSet) > 0) {
                    $products = mysqli_fetch_all($productSet, MYSQLI_ASSOC);
                    $results['products'] = $products;  
                }
            }
            return $results;
        }
                
        public function productsBookingHistory($con, $user_id)
        {
            $sql = "
                SELECT
                    p.id AS product_id,
                    p.product_title, 
                    p.base_price, 
                    p.sale_price,
                    p.product_image,
                    o.order_id,
                    o.total_amount,
                    o.date_of_order,
                    o.order_status,
                    o.payment_status,
                    sp.name,
                    sp.business_name,
                    sp.email,
                    sp.mobile,
                    sp.image,
                    sp.address,
                    sp.city,
                    sp.country_code,
                    sp.latitude,
                    sp.longitude
                FROM orders o 
                JOIN order_products op ON o.order_id = op.order_id
                JOIN products p ON op.product_id = p.id
                JOIN service_provider sp ON o.service_provider_id = sp.service_provider_id
                WHERE o.order_status IN ('pending', 'completed') AND o.user_id = '$user_id' 
                ORDER BY o.date_of_order DESC;
            ";
            $recordSet = mysqli_query($con,$sql);
            return $recordSet;
        }
        
        	public function schedule_booking($con, $user_id)
		{
			$sql = "
                            SELECT sp.service_provider_id ,sp.name,sp.business_name,sp.mobile,sp.image,sp.city,sp.address,
                                sps.service_name, 
                                sps.service_provider_service_id, 
                                sps.image as service_image, 
                                ss.total_amount, 
                                ss.slot_date, 
                                ss.slot_id
                            FROM service_slots ss
                            JOIN service_provider_services sps ON sps.service_provider_service_id = ss.service_id
                            JOIN service_provider sp ON sp.service_provider_id = ss.service_provider_id
                            WHERE ss.user_id = '$user_id' 
                            ORDER BY ss.created_time DESC;
                        ";
                        $recordSet = mysqli_query($con,$sql);
                        return $recordSet;
		}
                
        public function productsschedule_booking($con, $user_id)
        {
            $sql = "
                SELECT
                    p.id AS product_id,
                    p.product_title, 
                    p.base_price, 
                    p.sale_price,
                    p.product_image,
                    o.order_id,
                    o.total_amount,
                    o.date_of_order,
                    o.order_status,
                    o.payment_status,
                    sp.name,
                    sp.business_name,
                    sp.email,
                    sp.mobile,
                    sp.image,
                    sp.address,
                    sp.city,
                    sp.country_code,
                    sp.latitude,
                    sp.longitude
                FROM orders o 
                JOIN order_products op ON o.order_id = op.order_id
                JOIN products p ON op.product_id = p.id
                JOIN service_provider sp ON o.service_provider_id = sp.service_provider_id
                WHERE o.order_status IN ('pending', 'completed') AND o.user_id = '$user_id' 
                ORDER BY o.date_of_order DESC;
            ";
            $recordSet = mysqli_query($con,$sql);
            return $recordSet;
        }
        
        public function getAvailableSlots($con, $service_provider_id,$date,$type){
            
            $day = strtolower(date("l",strtotime($date)));

            $sql = "SELECT * FROM service_provider_timings WHERE service_provider_id = '$service_provider_id' AND day_name = '$day'";
	        $recordSet = mysqli_query($con,$sql);

            return $recordSet;
        }
        public function getTotalBookedSlot($con, $service_provider_id,$date,$type){
            
            $sql = "SELECT count(slot_id) from service_slots where DATE(slot_date) = '$date' and service_type = '$type' and service_provider_id='$service_provider_id'";
			$recordSet = mysqli_query($con,$sql);

			$total_booked_slots = mysqli_num_rows($recordSet);
			return $total_booked_slots;
        }


        
        public function checkItemInCart($con,$service_provider_id, $user_id, $type, $itemId)
		{
            $status = '0';
            if ($user_id) {
                $serviceId = 'IS NULL';
                $productId = 'IS NULL';
                if ($type === 'service') {
                    $serviceId = ' = ' . $itemId;                        
                } else {
                    $productId = ' = ' . $itemId;
                }
                $sql = "SELECT cart_id FROM `cart` WHERE user_id = $user_id AND provider_id = $service_provider_id AND type = '$type' and service_id $serviceId and product_id $productId;";
                $recordSet = mysqli_query($con,$sql);
                if($recordSet && mysqli_num_rows($recordSet) > 0) {
                    $status = 1;
                } 
            }
            return $status;
        }
        public function getSearchData($con, $keyword){
            $sql ="SELECT * FROM (SELECT category_id id, name, 'category' type, '' category_name, category_for, category_id
			FROM category
			WHERE name 
			LIKE '" . $keyword . "%'

			UNION

			SELECT s.service_provider_service_id id, s.service_name name, 'service' type, c.name category_name, '' category_for, c.category_id
			FROM service_provider_services s JOIN category c ON c.category_id = s.category_id
			WHERE s.service_name 
			LIKE '" . $keyword . "%' ) as a ORDER BY a.name LIMIT 0,6
			";
			
			$searchdata = mysqli_query($con,$sql);
            return $searchdata;
        }
        
        public function verifyJWT($headers)
		{

				$secret_key = "Salonee";
				$jwt = null;
				if(isset($headers['Authorization'])) {
				$authHeader = $headers['Authorization'];
				$arr = explode(" ", $authHeader);

				$jwt = $arr[1];

				if($jwt){

					try {

						$decoded = JWT::decode($jwt, $secret_key, array('HS256'));
						// Access is granted. Add code of the operation here 
					} catch (Exception $e){

					http_response_code(401);

					echo json_encode(array(
						"status"=>'400',"message"=>"Access denied. ".$e->getMessage()
					)); exit;
				}

				}
				else
				{
					$result = array("status"=>'400',"message"=>"Access denied!");
					echo  json_encode($result); exit;
				}
				
				}
				else
				{
					$result = array("status"=>'400',"message"=>"Authorization Token not exist!");
					echo  json_encode($result); exit;
				}

		} 

}
?>
