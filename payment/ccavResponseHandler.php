<?php include('Crypto.php')?>
<?php include('config.php')?>
<?php

	//error_reporting(0);
	
	ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
	
	$workingKey=WORKING_KEY;		//Working Key should be provided here.
	$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
	$rcvdString=decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
	$order_status="";
	$decryptValues=explode('&', $rcvdString);
	$dataSize=sizeof($decryptValues);

	for($i = 0; $i < $dataSize; $i++) 
	{
		$information=explode('=',$decryptValues[$i]);
		//print_r($information);die;
		if($i==0)	$order_id=$information[1];
		if($i==3)	$order_status=$information[1];
		if($i==10)	$total_amount=$information[1];
		if($i==26)	$slot_id=$information[1];
		if($i==27)	$user_id=$information[1];
		if($i==28)	$promo_code=$information[1];
		if($i==29)	$promo_amount=$information[1];
	}

	if($order_status==="Success")
	{
		include("../config/dbConnection.php");

		$dbObject = new dbConnection();

		$con = $dbObject->getConnection();
		
		//empty cart
		$sql = "UPDATE cart SET status = 2 WHERE user_id = '$user_id'";
		$rowsAffected = mysqli_query($con,$sql);

		$sql1 = "UPDATE service_slots SET total_amount = '$total_amount',
										 coupon_code = '$promo_code',
										 coupon_amount = '$promo_amount',
										payment_status = 'completed',
										order_id = '$order_id'
										WHERE slot_id = '$slot_id'";

		$rowsAffected = mysqli_query($con,$sql1);

        if($rowsAffected > 0)
        {
        	$sql = "SELECT email 
    		FROM user 
    		WHERE user_id = '$user_id'";
		    $recordSet = mysqli_query($con,$sql);

		    $data = array();
		    while($row = mysqli_fetch_array($recordSet))
			{
				$email = $row["email"];
			}

			header('Location: '.$_SERVER["REQUEST_SCHEME"].'://'.$_SERVER["HTTP_HOST"].'/payment_status.php?status='.$order_status);
        } 
        else
        {
        	header('Location: '.$_SERVER["REQUEST_SCHEME"].'://'.$_SERVER["HTTP_HOST"].'/payment_status.php?status=0');
        }
	}
	else if($order_status==="Aborted")
	{
		header('Location: '.$_SERVER["REQUEST_SCHEME"].'://'.$_SERVER["HTTP_HOST"].'/payment_status.php?status='.$order_status);
	}
	else if($order_status==="Failure")
	{
		header('Location: '.$_SERVER["REQUEST_SCHEME"].'://'.$_SERVER["HTTP_HOST"].'/payment_status.php?status='.$order_status);
	}
	else
	{
		echo "<br>Security Error. Illegal access detected";
		header('Location: '.$_SERVER["REQUEST_SCHEME"].'://'.$_SERVER["HTTP_HOST"].'/payment_status.php?status='.$order_status);
	}

	echo "<br><br>";

	/*echo "<table cellspacing=4 cellpadding=4>";
	for($i = 0; $i < $dataSize; $i++) 
	{
		$information=explode('=',$decryptValues[$i]);
	    	echo '<tr><td>'.$information[0].'</td><td>'.$information[1].'</td></tr>';
	}

	echo "</table><br>";
	echo "</center>";*/
?>
