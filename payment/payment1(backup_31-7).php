<?php
include('config.php');
include("../config/dbConnection.php");
$dbObject = new dbConnection();
$con = $dbObject->getConnection();
$user_id = $_POST['user_id'];
$slot_date = $_POST['slot_date'];
$slot_time = $_POST['slot_time'];
$order_id = time() . mt_rand() . $user_id;
$coupon_code = @$_POST['coupon_code'];
$service_provider_id = @$_POST['service_provider_id'];
$service_id = $_POST['service_id'];
$product_id = $_POST['product_id'];
$quantity = $_POST['quantity'];
$service_id_arr = explode (",", $service_id);
$product_id_arr = explode (",", $product_id);
$total_amount_services = 0;
for($i=0; $i < count($service_id_arr); $i++) {
	$type = $_POST['type'];
	$sql = "SELECT sale_price FROM services_prices WHERE service_provider_service_id = '$service_id_arr[$i]' AND price_for = '$type'";
	$recordSet = mysqli_query($con,$sql);
	while($row = mysqli_fetch_array($recordSet))
	{
		$total_amount_services += $row["sale_price"];
	}
} 
$total_amount_products = 0;
for($i=0; $i < count($product_id_arr); $i++) {
//	$sql = "SELECT sale_price FROM products WHERE id = '$product_id_arr[$i]'";
	$sql = "SELECT c.cart_id,c.quantity,p.sale_price FROM cart c
			JOIN products p ON p.id = c.product_id
			WHERE c.type = 'product' and p.id = '$product_id_arr[$i]' AND c.user_id = '$user_id' AND c.status = 1";
	
	$recordSet = mysqli_query($con,$sql);
	while($row = mysqli_fetch_array($recordSet))
	{
		$total_amount_products += round($row["sale_price"]*$row["quantity"],2);
	}
}
$total_amount = $total_amount_services + $total_amount_products;
$discountAmount = 0;
if(!empty($coupon_code)) {
    $sql = "SELECT * FROM promocodes WHERE coupon_code='$coupon_code' AND status=1";
    $rowsAffected = mysqli_query($con,$sql);
    	while($row = mysqli_fetch_array($rowsAffected)) {
         $date_now = date('Y-m-d');
         $date2    = $row['expiry_date'];
        if($date2 <= $date_now ) {
        	$discountAmount = (($row['discount_per'] / 100) * $total_amount);
        	$total_amount = $total_amount - $discountAmount;
        }
    }
}
else
{
    $discountAmount = $total_amount;
}
$slot_date = date('Y-m-d', strtotime($slot_date));
$slot_time = date('H:i:s', strtotime($slot_time));
$sql1 = "INSERT INTO service_slots(service_ids, service_id, service_provider_id, user_id, total_amount, slot_date, service_type, order_id) 
			VALUES('$service_id', '$service_id_arr[0]', '$service_provider_id', '$user_id', '$total_amount', '$slot_date.$slot_time', '$type', '$order_id')";
$recordSet1 = mysqli_query($con,$sql1);
$slot_id = $con->insert_id;
?>
<html>
<head>
</head>
<body>
	<form method="post" name="customerData" id="customerData" action="ccavRequestHandler.php" style="display:none;">
		<table width="40%" height="100" border='1' align="center"><caption><font size="4" color="blue"><b>Integration Kit</b></font></caption></table>
			<table width="40%" height="100" border='1' align="center">
				<tr>
					<td>Parameter Name:</td><td>Parameter Value:</td>
				</tr>
				<tr>
					<td colspan="2"> Compulsory information</td>
				</tr>
				<tr>
					<td>Merchant Id	:</td><td><input type="text" name="merchant_id" value="<?php echo MERCHANT_ID; ?>"/></td>
				</tr>
				<tr>
					<td>Order Id	:</td><td><input type="text" name="order_id" value="<?php echo $order_id;?>"/></td>
				</tr>
				<tr>
					<td>Amount	:</td><td><input type="text" name="amount" value="<?php echo $total_amount;?>"/></td>
				</tr>
				<tr>
					<td>Currency	:</td><td><input type="text" name="currency" value="AED"/></td>
				</tr>
				<tr>
					<td>Redirect URL	:</td><td><input type="text" name="redirect_url" value="<?php echo REDIRECT_URL; ?>"/></td>
				</tr>
			 	<tr>
			 		<td>Cancel URL	:</td><td><input type="text" name="cancel_url" value="<?php echo CANCEL_URL; ?>"/></td>
			 	</tr>
			 	<tr>
					<td>Language	:</td><td><input type="text" name="language" value="EN"/></td>
				</tr>
		     	<tr>
		     		<td colspan="2">Billing information(optional):</td>
		     	</tr>
		        <tr>
		        	<td>Billing Name	:</td><td><input type="text" name="billing_name" value="Charli"/></td>
		        </tr>
		        <tr>
		        	<td>Billing Address	:</td><td><input type="text" name="billing_address" value="Room no 1101, near Railway station Ambad"/></td>
		        </tr>
		        <tr>
		        	<td>Billing City	:</td><td><input type="text" name="billing_city" value="Indore"/></td>
		        </tr>
		        <tr>
		        	<td>Billing State	:</td><td><input type="text" name="billing_state" value="MP"/></td>
		        </tr>
		        <tr>
		        	<td>Billing Zip	:</td><td><input type="text" name="billing_zip" value="425001"/></td>
		        </tr>
		        <tr>
		        	<td>Billing Country	:</td><td><input type="text" name="billing_country" value="India"/></td>
		        </tr>
		        <tr>
		        	<td>Billing Tel	:</td><td><input type="text" name="billing_tel" value="9595226054"/></td>
		        </tr>
		        <tr>
		        	<td>Billing Email	:</td><td><input type="text" name="billing_email" value="atul.kadam@avenues.info"/></td>
		        </tr>
		        <tr>
		        	<td colspan="2">Shipping information(optional)</td>
		        </tr>
		        <tr>
		        	<td>Shipping Name	:</td><td><input type="text" name="delivery_name" value="Chaplin"/></td>
		        </tr>
		        <tr>
		        	<td>Shipping Address	:</td><td><input type="text" name="delivery_address" value="room no.701 near bus stand"/></td>
		        </tr>
		        <tr>
		        	<td>shipping City	:</td><td><input type="text" name="delivery_city" value="Hyderabad"/></td>
		        </tr>
		        <tr>
		        	<td>shipping State	:</td><td><input type="text" name="delivery_state" value="Andhra"/></td>
		        </tr>
		        <tr>
		        	<td>shipping Zip	:</td><td><input type="text" name="delivery_zip" value="425001"/></td>
		        </tr>
		        <tr>
		        	<td>shipping Country	:</td><td><input type="text" name="delivery_country" value="India"/></td>
		        </tr>
		        <tr>
		        	<td>Shipping Tel	:</td><td><input type="text" name="delivery_tel" value="9595226054"/></td>
		        </tr>
		        <tr>
		        	<td>Slot ID	:</td><td><input type="text" name="merchant_param1" value="<?php echo $slot_id;?>"/></td>
		        </tr>
		        <tr>
		        	<td>User ID	:</td><td><input type="text" name="merchant_param2" value="<?php echo $user_id;?>"/></td>
		        </tr>
				<tr>
					<td>Promo Code	:</td><td><input type="text" name="merchant_param3" value="<?php echo $coupon_code;?>"/></td>
				</tr>
				<tr>
					<td>coupon Amount	:</td><td><input type="text" name="merchant_param4" value="<?php echo $discountAmount;?>"/></td>
				</tr>
				<tr>
					<td>Vault Info.	:</td><td><input type="text" name="customer_identifier" value=""/></td>
				</tr>
		        <tr>
		        	<td></td><td><INPUT TYPE="submit" value="CheckOut"></td>
		        </tr>
	      	</table>
	      </form>
	</body>
</html>
<script type="text/javascript">
	document.forms["customerData"].submit();
</script>