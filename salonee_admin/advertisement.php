<?php
require_once dirname(__DIR__) . '/controller/AdminAdvertisements.php';
$controller = new AdminAdvertisements();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$advertisements=$page_data['advertisements'];
include "includes/header.php";
?>
      <!-- BEGIN sidebar -->
      <?php include('includes/sidebar.php');?>
      <!--  //END sidebar -->

      <!-- BEGIN main -->

        <div class="main__content">
          <div class="w-50">
            <div class="input-group mb-3">
              <input type="text" class="form-control _search" placeholder="Search" id="searchTd">
              <div class="input-group-append">
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
          </div>



          <br>
          <div class="clearfix"></div>

       <div class="pad_3">
          <div class="filtrBtns">
          <a class="btn login-btn btn-sm" style="width:170px;color:white" href="<?php echo admin_base_url.'advertisement.php';?>" id="">All Ads &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
          <a class="btn login-btn btn-sm" style="width:170px;color:white"  href="<?php echo admin_base_url.'advertisement.php?adv=active';?>"  style="width:100px;color:white"  id="">Active Ads  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
          <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'advertisement.php?adv=pending';?>" style="width:100px;color:white"  id="">Pending Ads  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
          <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'advertisement.php?adv=rejected';?>" style="width:100px;color:white"  id="">Rejected Ads  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
          <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'advertisement.php?adv=approved';?>" style="width:100px;color:white"  id="">Approved Ads  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
          <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'advertisement.php?adv=expired';?>" style="width:100px;color:white"  id="">Expired Ads  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
          </div>    
            <div class="table-responsive">

              <table class="table themeTable usersTable adTable">
                <thead>
                  <tr>
                  <th>S.No</th>
                    <th>Banner</th>
                    <th>From Date</th>
                    <th>To Date</th>
                    <th>Clicks</th>
                    <th>Amount</th>
                    <th>City</th>
                    <th>Date of Create</th>
                    <th>Status</th>
                    <th>Ad Status</th>
                    <th>Action</th>
                    <!-- <th>Publish</th> -->
                  </tr>
                </thead>
                <tbody id="saloonTable">
                <?php 
                $i=1;
                foreach($advertisements as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td class="usrImg"><a href="'.user_base_url.$prf['banner'].'" target="_blank"><img src="'.user_base_url.$prf['banner'].'" width="200px" alt="advertiment banner" title="advertisement banner" /></a></td>';
                  echo '<td>'.$prf['from_date'].'</td>';
                  echo '<td>'.$prf['expiry_date'].'</td>';
                  echo '<td>'.$prf['clicks'].'</td>';
                  echo '<td><strong>AED '.$prf['amount'].'</strong></td>';
                  echo '<td>'.$prf['city_name'].'</td>';
                  echo '<td>'.date("Y-M-d H:i A",strtotime($prf['created_time'])).'</td>';
                  echo '<td>'; 
                  switch($prf['status']){
                    case 'pending':
                    case 'inprocess':
                      echo '<label class="badge badge-pill badge-warning">In Process</label>';
                      break;
                    case 'active':
                      echo '<label class="badge badge-pill badge-success">Active</label>';
                      break;
                    case 'disable':
                      echo '<label class="badge badge-pill badge-warning">Disabled</label>';
                      break;
                    case 'expired':
                      echo '<label class="badge badge-pill badge-danger">Expired</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">Oops</label>';
                    break;
                  }
                  echo '</td>';
                  echo '<td>'; 
                  switch($prf['approve_status']){
                    case 'inprocess':
                      echo '<label class="badge badge-pill badge-warning">In Process</label>';
                      break;
                    case 'approved':
                      echo '<label class="badge badge-pill badge-success">Approved</label>';
                      break;
                    case 'pending':
                      echo '<label class="badge badge-pill badge-warning">Pending</label>';
                      break;
                    case 'rejected':
                      echo '<label class="badge badge-pill badge-danger">Rejected</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">Oops</label>';
                    break;
                  }
                  echo '</td>';
                  echo '<td> ';
                  if($prf['approve_status']=='pending'){
                    echo '<a  href="'.admin_base_url.'view-service-provider.php?id='.$prf['advertisement_id'].'&sid='.$prf['service_provider_id'].'&type=approve_ad" data-id="'.$prf['advertisement_id'].'" data-sid="'.$prf['service_provider_id'].'" class=" approve_now badge badge-success"> <i class="fa fa-check"></i> Approve </a>';
                    echo '<a  href="javascipt:return false;" data-id="'.$prf['advertisement_id'].'"  data-sid="'.$prf['service_provider_id'].'" class=" reject_ad badge badge-danger"> <i class="fa fa-times"></i> Reject </a>';
                  }
                  
                  echo '</td>';
                  echo '</tr>';
                } ?>   

                </tbody>
              </table>

            </div>
            <div class="clearfix"></div>
            <!-- pagination  -->
            <div class='pagination-container tablePaging'>
              <div class="pagination">
                <ol id="numbers"></ol>
              </div>
            </div>


          </div>


        </div>
    </div>
    </main>
    <!--  //END main -->

    <!-- <div class="layout__container-after"></div> -->



  </div>


  <!-- partial -->
  <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Add Advertisement</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="login">
            <form>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <textarea type="text" id="adDesc" rows="5" class="form-control" autocomplete="off" ></textarea>
                    <label class="form-control-placeholder p-0" for="adDesc">Advertisement Description</label>
                  </div>
                </div>
              </div>
             
              <div class="form-group">
                <label for="file-upload" class="custom-file-upload sadmnUpload">
                  UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                </label>
                <input id="file-upload" name='upload_cont_img' type="file" style="display:none;">
              </div>

              <div class="form-group">
                <button type="button" class="btn theme-btn">Submit</button>
              </div>
            </form>
          </div>

        </div>

      </div>
    </div>
  </div>

    <!-- partial -->
    <div class="modal fade pswdModal" id="rejectAD" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Reject Advertiment</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="rejectAD" />
              <input type="hidden" name="id" id="id_a" value="" />
              <input type="hidden" name="sid" id="sid_a" value="" />
                <div class="form-group">
                  <textarea type="text" name="reason" class="form-control" required></textarea>
                  <label class="form-control-placeholder p-0" for="refund_amount">Reason</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Reject Now</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- end popup -->

  <?php include 'includes/footer.php';?>
</body>
<script>
$(".reject_ad").click(function(){
  var id=$(this).data("id");
  var sid=$(this).data("sid");
  $("#id_a").val(id);
  $("#sid_a").val(sid);
  $("#rejectAD").modal('show');
})
</script>
</html>