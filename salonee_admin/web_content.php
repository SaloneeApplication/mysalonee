<?php
require_once dirname(__DIR__) . '/controller/AdminWebContent.php';
$controller = new AdminWebContent();
$controller->load();
$page_data=$controller->pageData();
$title = $page_data['title']; 
$page = $page_data['page'];
$sub_menu = 'home_content';
$adminId=$page_data['admin_id'];
include "includes/header.php";
include('includes/sidebar.php');
?>

<div class="main__content">
    <div class="w-50">
        <h5>Home Page Content - Website</h5><br>
    </div>
    <div class="clearfix"></div>
    <div class="login" style="margin-left: -150px;">
        <form method="POST" action="<?php echo admin_base_url;?>web_content.php">
            <div class="row">
            	<input type="hidden" name="type" value="addAboutUs">
                <div class="col-sm-4">
                    <div class="form-group">
                        <select id="language_id" name="language_id" class="form-control" required>
                        	<option selected value="English">English</option>
                        	<option value="Arabic">Arabic</option>
                        	<option value="Hindi">Hindi</option>
                        </select>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <h6>About Us</h6><br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="about_us_title" id="about_us_title"/>
                                <label class="form-control-placeholder p-0" for="about_us_title">About Us Title</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="about_us_heading" id="about_us_heading">
                                <label class="form-control-placeholder p-0" for="about_us_heading">About Us Heading</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea type="text" id="about_us" name="about_us" class="form-control" autocomplete="off" ></textarea>
                                <label class="form-control-placeholder p-0" for="about_us">About Us</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h6>Categories</h6><br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="why_choose_us" id="why_choose_us">
                                <label class="form-control-placeholder p-0" for="why_choose_us">Why Choose Us</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="categories_heading" id="categories_heading">
                                <label class="form-control-placeholder p-0" for="categories_heading">Categories Heading</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="categories_title" id="categories_title">
                                <label class="form-control-placeholder p-0" for="categories_title">Categories Title</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea type="text" id="categories_desc" name="categories_desc" class="form-control" autocomplete="off" ></textarea>
                                <label class="form-control-placeholder p-0" for="categories_desc">Categories Description</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="cat_women_title" id="cat_women_title">
                                <label class="form-control-placeholder p-0" for="cat_women_title">Categories For Women</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="cat_men_title" id="cat_men_title">
                                <label class="form-control-placeholder p-0" for="cat_men_title">Categories For Men</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="cat_children_title" id="cat_children_title">
                                <label class="form-control-placeholder p-0" for="cat_children_title">Categories For Children</label>
                            </div>
                        </div>
                    </div>
                    <h6>Compare Prices</h6><br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="compare_prices" id="compare_prices">
                                <label class="form-control-placeholder p-0" for="compare_prices">Compare Prices</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea type="text" id="compare_prices_desc" name="compare_prices_desc" class="form-control" autocomplete="off" ></textarea>
                                <label class="form-control-placeholder p-0" for="compare_prices_desc">Compare Prices Description</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h6>Shops</h6><br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="choose_from_the_best" id="choose_from_the_best">
                                <label class="form-control-placeholder p-0" for="choose_from_the_best">Choose From The Best</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea type="text" id="shops_desc" name="shops_desc" class="form-control" autocomplete="off" ></textarea>
                                <label class="form-control-placeholder p-0" for="shops_desc">Shops Description</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h6>Download Apps</h6><br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="apps_title" id="apps_title">
                                <label class="form-control-placeholder p-0" for="apps_title">Apps Title</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea type="text" id="apps_desc" name="apps_desc" class="form-control" autocomplete="off" ></textarea>
                                <label class="form-control-placeholder p-0" for="apps_desc">Apps Description</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h6>Payments</h6><br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="payments_heading" id="payments_heading">
                                <label class="form-control-placeholder p-0" for="payments_heading">Payments Heading</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="payments_title" id="payments_title">
                                <label class="form-control-placeholder p-0" for="payments_title">Payments Title</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea type="text" id="payments_desc" name="payments_desc" class="form-control" autocomplete="off" ></textarea>
                                <label class="form-control-placeholder p-0" for="payments_desc">Payments Description</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h6>Service</h6><br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="service_heading" id="service_heading">
                                <label class="form-control-placeholder p-0" for="service_heading">Service Heading</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="service_title" id="service_title">
                                <label class="form-control-placeholder p-0" for="service_title">Service Title</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea type="text" id="service_desc" name="service_desc" class="form-control" autocomplete="off" ></textarea>
                                <label class="form-control-placeholder p-0" for="service_desc">Service Description</label>
                            </div>
                        </div>
                    </div>
                    <h6>News</h6><br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="welcome_text" id="welcome_text">
                                <label class="form-control-placeholder p-0" for="welcome_text">Welcome Text</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="our_latest_news" id="our_latest_news">
                                <label class="form-control-placeholder p-0" for="our_latest_news">Our Latest News Title</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea type="text" id="news_desc" name="news_desc" class="form-control" autocomplete="off" ></textarea>
                                <label class="form-control-placeholder p-0" for="news_desc">News Description</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" style="width: 15%!important;" class="btn theme-btn">Submit</button>
            </div>
        </form>
    </div>
</div>
<?php include 'includes/footer.php';?>
<script type="text/javascript">

    $(document).ready(function(){

        var language_id = 'English';
        load_data(language_id);

        $("#language_id").change(function () {

            language_id = $(this).val();
            load_data(language_id)
        });
    });

	function load_data(language_id)
    {
      	$.ajax({
	        url:"<?php echo admin_base_url.'web_content.php';?>",
	        method:"POST",
	        data:{type:'getAboutUs',language_id:language_id},
	        success:function(resp){
				var resp=JSON.parse(resp);
                var res = resp.data;
                
				$("#about_us_title").val(res.about_us_title);
                $("#about_us_heading").val(res.about_us_heading);
                $("#about_us").val(res.about_us);
                $("#why_choose_us").val(res.why_choose_us);
                $("#categories_heading").val(res.categories_heading);
                $("#categories_title").val(res.categories_title);
                $("#categories_desc").val(res.categories_desc);
                $("#cat_women_title").val(res.cat_women_title);
                $("#cat_men_title").val(res.cat_men_title);
                $("#cat_children_title").val(res.cat_children_title);
                $("#compare_prices").val(res.compare_prices);
                $("#compare_prices_desc").val(res.compare_prices_desc);
                $("#choose_from_the_best").val(res.choose_from_the_best);
                $("#shops_desc").val(res.shops_desc);
                $("#apps_title").val(res.apps_title);
                $("#apps_desc").val(res.apps_desc);
                $("#payments_heading").val(res.payments_heading);
                $("#payments_title").val(res.payments_title);
                $("#payments_desc").val(res.payments_desc);
                $("#service_heading").val(res.service_heading);
                $("#service_title").val(res.service_title);
                $("#service_desc").val(res.service_desc);
                $("#welcome_text").val(res.welcome_text);
                $("#our_latest_news").val(res.our_latest_news);
                $("#news_desc").val(res.news_desc);
        	}
      	})
  	}
</script>