<?php
require_once dirname(__DIR__) . '/controller/AdminUsers.php';
$controller=new AdminUsers();
$controller->load();
$page_data=$controller->pageData();
$title = $page_data['title']; $page = $page_data['page'];
$adminId=$page_data['admin_id'];
$users=$controller->getUsers();
include("includes/header.php"); 
if($_SESSION['auth_type']=='salesperson'){
    header("Location:sales-person.php");
    exit;
}
?>
<!-- BEGIN sidebar -->
<?php include('includes/sidebar.php');?>
<!--  //END sidebar -->
        <div class="main__content">
          <div class="w-50" style="max-width:530px;">
            <form action="" method="post">
            <div class="input-group mb-3 input-daterange">
                  <div class="input-group-append append_form_data">
                      <select name="search_type" id="search_type" class="btn theme-btn">
                        <option value="by_email" selected="">Email</option>
                        <option value="by_phone">Phone</option>
                        <option value="by_customerid">User ID</option>
                      </select>
                    </div>
                    <input type="text" class="form-control search_by_other" name="search" id="" placeholder="Enter User Email">

                    <div class="input-group-append">
                      <button class="btn theme-btn" type="submit">Search</button>
                    </div>
                  </div>
            </form>
  
          </div>
          <!-- <div class="w-50">
            <div class="input-group mb-3">
              <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
              <div class="input-group-append">
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
          </div> -->


          <div class="pad_3">
            <div class="table-responsive">

              <table class="table theme_Table dataTable usersTable " id="example">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <!--<th>User</th>-->
                    <th>Name [UserID]</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <!--<th>City</th>-->
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>

      <?php $i=1; foreach($users as $u){  ?>  
                  <tr>
                    <td><?php echo $i;?></td>
                    <!--<td class="usrImg">-->
                    <!--  <img src="<?php echo base_url;?>uploads/user_images/avatar-male.png" alt="profile" class="rounded-circle">-->
                    <!--</td>-->
                    <td><?php echo $u['name'].' [ '.$u['user_id'].' ]';?></td>
                    <td><?php echo $u['email'];?></td>
                    <td><?php echo $u['mobile'];?></td>
                    <!--<td><?php echo $u['city_name'];?></td>-->
                    <td><?php  if($u['status']==1){
                      echo '<span class="badge badge-pill badge-success">Active</span>'; 
                    }else{
                      echo '<span class="badge badge-pill badge-danger">Blocked</span>'; 
                    }?></td>
                    <td>
                      <?php 
                        if($u['status']==1){
                          ?>
                          <a onclick="return confirm('Are you sure want to block this user');" href="<?php echo admin_base_url.'users.php?type=block_user&id='.$u['user_id'];?>" class="badge badge-danger"><i class="fa fa-times"></i> Block</a>
                          <?php
                        }else{
                          ?>
                          <a href="<?php echo admin_base_url.'users.php?type=active_user&id='.$u['user_id'];?>" class="badge badge-success"><i class="fa fa-check"></i> Active</a>
                          <?php
                        }
                      ?>
                        <a href="<?php echo admin_base_url.'view-user.php?id='.$u['user_id'];?>" class="badge badge-primary"><i class="fa fa-eye"></i> View</a>
                    </td>
                  </tr> 
                  <?php $i++; } ?>
                  
                </tbody>
              </table>

            </div>
            <div class="clearfix"></div>

            <!-- pagination 
            <div class='pagination-container tablePaging'>
              <div class="pagination">
                <ol id="numbers"></ol>
              </div>
            </div> -->


          </div>

        </div>


      </main>
      <!--  //END main -->

      <!-- <div class="layout__container-after"></div> -->

    </div>

    <?php include 'includes/footer.php';?>

</body>

<style>
  table.table.theme_Table td{
    vertical-align: middle;
  }
</style>

<script>
 
$(document).ready(function() {
    $('#example').dataTable({
      "sPaginationType": "full_numbers",
      "language": { 
            "zeroRecords": "No Users list found",           
        },
      aLengthMenu: [
        [10,25, 50, 100, 200, -1],
        [10,25, 50, 100, 200, "All"]
    ],
    // iDisplayLength: -1
    });
});

 
 


$("#search_type").change(function(){
  var type=$(this).val();
  $(".search_by_other").remove();
  if(type=='by_email'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }else if(type=='by_phone'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Phone">');
  }else if(type=='by_customerid'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User ID">');
  }else{
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }
});
</script>
</html>