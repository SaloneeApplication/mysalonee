<?php

require_once dirname(__DIR__) . '/controller/AdminProductOrders.php';
$sales = new AdminProductOrders();
$sales->init();
$persons=array();
$page_data=$sales->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$products=$page_data['products'];
include "includes/header.php";
?>
            <!-- BEGIN sidebar -->
            <?php include 'includes/sidebar.php';?>
            <!--  //END sidebar -->
<style>
table.table.themeTable td, table.table.themeTable th {
  max-width:none;
}
</style>
            <!-- BEGIN main -->
                <div class="main__content">
                    <!-- <div class="w-50">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
                            <div class="input-group-append">
                                <button class="btn theme-btn" type="submit">Go</button>
                            </div>
                        </div>
                    </div> -->
                 
                  <div class="pad_3">
                  <div class="filtrBtns">
                    <a class="btn login-btn btn-sm" style="width:170px;color:white" href="<?php echo admin_base_url.'product-orders.php';?>" id="">All Orders  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    <a class="btn login-btn btn-sm" style="width:170px;color:white"  href="<?php echo admin_base_url.'product-orders.php?pro=delivered';?>"  style="width:100px;color:white"  id="">Delivered  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'product-orders.php?pro=inprocess';?>" style="width:100px;color:white"  id="">In Process &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'product-orders.php?pro=refund_pending';?>" style="width:100px;color:white"  id="">Refund Pending &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'product-orders.php?pro=refunded';?>" style="width:100px;color:white"  id="">Refunded &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'product-orders.php?pro=cancelled';?>" style="width:100px;color:white"  id="">Cancelled &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                        <div class="table-responsive">
                            <table class="table theme_Table dataTable" id="example">
                                <thead>
                                    <tr>
                                      <th>S.No</th>
                                      <th>Order ID</th>
                                      <th>Service Provider</th>
                                      <th>User</th>
                                      <th>No.Products</th>
                                      <th>Total Amount</th>
                                      <th>Commissions</th>
                                      <th>Payment Status</th>
                                      <th>Order Status</th>
                                       <th>Shipping Status</th>
                                      <th>Date</th>
                                      <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody >
                                <?php 
                $i=1;
                foreach($products as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['order_id'].'</td>';
                  echo '<td><a href="'.admin_base_url.'view-service-provider.php?id='.$prf['service_provider_id'].'">'.$prf['service_provider_name'].'</a></td>';
                  echo '<td><a href="'.admin_base_url.'view-user.php?id='.$prf['user_id'].'">'.$prf['user_name'].'</a></td>';
               
                  echo '<td>'.$prf['total_products'].'</td>';
                  echo '<td><strong>AED '.$prf['total_amount'].'</strong></td>';
                  echo '<td> 
                  A.C <strong>AED '.$prf['admin_commission'].'</strong> <br/>
                  T.C <strong>AED '.$prf['transaction_charges'].'</strong> <br/>
                  </td>';
                  echo '<td>'; 
                  switch($prf['payment_status']){
                    case 'pending':
                      echo '<label class="badge badge-pill badge-warning">Pending</label>';
                      break;
                    case 'completed':
                      echo '<label class="badge badge-pill badge-success">Success</label>';
                      break;
                    case 'shipped':
                      echo '<label class="badge badge-pill badge-primary">Shipped</label>';
                      break;
                    case 'cancelled':
                    case 'failed':
                      echo '<label class="badge badge-pill badge-danger">Cancelled</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">In Process</label>';
                    break;
                  }
                  echo '</td>';
                  echo '<td>'; 
                  if($prf['payment_status']=='completed'){
                    switch($prf['order_status']){
                      case 'pending':
                      case 'inprocess':
                        case 'on_hold':
                        if($prf['shipping_status']=='inprocess'){
                          echo '<label class="badge badge-pill badge-warning">In Process</label>';
                        }elseif($prf['shipping_status']=='shipped'){
                          echo '<label class="badge badge-pill badge-primary">Shipped</label>';
                        }else{
                          echo '<label class="badge badge-pill badge-warning">In Process</label>';
                        }
                        break;
                      case 'completed':
                        echo '<label class="badge badge-pill badge-success">Delivered</label>';
                        break;
                      case 'shipped':
                        echo '<label class="badge badge-pill badge-primary">Shipped</label>';
                        break;
                        case 'cancelled':
                        echo '<label class="badge badge-pill badge-danger">Cancelled</label>';
                        break;
                        case 'refunded':
                        echo '<label class="badge badge-pill badge-danger">Refunded</label>';
                        break;
                        case 'refund_pending':
                          echo '<label class="badge badge-pill badge-danger">Refund Pending</label>';
                          break;
                      default:
                      echo '<label class="badge badge-pill badge-warning">In Process</label>';
                      break;
                    }
                  }else{
                    echo '<label class="badge badge-pill badge-danger">Failed</label>';
                  }
                 
                  echo '</td>';
                     echo '<td>'.$prf['shipping_status'].'</td>';
                  echo '<td>'.date("Y-M-d H:i A",strtotime($prf['date_of_order'])).'</td>';
                  echo '<td><button class="badge badge-primary view_products" data-oid="'.$prf['order_id'].'" >View Products</button>';
                  if($prf['payment_status']=='completed' && $prf['order_status']=='cancelled'){
                    echo '<a  href="javascipt:;" data-oid="'.$prf['order_id'].'" class=" refund_now badge badge-warning"> <i class="fa fa-check"></i> Refund </a>';
                  }
                  echo '</td>';
                 
                  echo '</tr>';
                } ?>   
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <!-- pagination  -->
                        <div class='pagination-container tablePaging'>
                            <div class="pagination">
                                <ol id="numbers"></ol>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>

        <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Add Sales Person</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="login">
                    <form action="salesperson.php" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="type" value="addSalesPerson" />
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <input type="text" id="title" class="form-control" name="name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="title">Name</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <select class="form-control" autocomplete="off" name="location" required>
                                <option value="0"> Select Location</option>
                                <?php $cities=array(); foreach($cities as $cit){ 
                                  echo '<option value="'.$cit['id'].'">'.$cit['city_name_en'].'</option>';
                                }?>
                            </select>
                            <label class="form-control-placeholder p-0" for="title">Location</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="code">Email</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="mobile" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="value">Mobile</label>
                                </div>
                            </div>
                        </div>
                      <div class="form-group">
                        <label for="file-upload" class="custom-file-upload sadmnUpload">
                          UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                        </label>
                        <input id="file-upload" name='image' type="file"  style="display:none;">
                      </div>

                      <div class="form-group">
                        <button type="submit" class="btn theme-btn">Submit</button>
                      </div>
                    </form>
                  </div>

                </div>

              </div>
            </div>
          </div>
          
    <!-- order products partial -->
    <div class="modal fade pswdModal" id="orderedProducts" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Ordered Products</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <input type="hidden" name="type" value="updateOrderStatus" />
              <input type="hidden" name="oid" id="oid" value="" />
                <div class="row mb-5">
<div class="col-md-12">

<table class="example product_cls" id="product_cls">
<tr><th>Product Name</th><th>Image</th><th>Price</th><th>QTY</th></tr>
<tr><td>Product Name</td><td>Image</td><td>Price</td><td>QTY</td></tr>
</table>
</div>
                </div>


                <div class="form-group">
                </div>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->

    
    <!-- partial -->
    <div class="modal fade pswdModal" id="refundPOP" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Refund</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="refundOrderID" />
              <input type="hidden" name="id" id="orderid" value="" />
                <div class="form-group">
                  <input type="text" name="refund_amount" class="form-control" id="refund_amount" required/>
                  <label class="form-control-placeholder p-0" for="refund_amount">Refund Amount</label>
                </div>
                <div class="form-group">
                  <input type="text" name="refund_deductions" class="form-control" value="0" id="refund_deductions" required/>
                  <label class="form-control-placeholder p-0" for="refund_deductions">Refund Deductions</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Refund Now</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- end popup -->
        <?php include 'includes/footer.php';?>
<style>
  #example tr th{
    white-space: nowrap;
  }
</style>
 <script>
  $(document).ready(function() {
      $('#example').dataTable({
        "sPaginationType": "full_numbers",
        "language": { 
              "zeroRecords": "No Records found",           
          },
          aLengthMenu: [
          [10,25, 50, 100, 200, -1],
          [10,25, 50, 100, 200, "All"]
      ],
      // iDisplayLength: -1
      });
  });

</script>



    </body>
    <script>
        $(".view_products").click(function(){
          var oid=$(this).data("oid");
          $("#orderedProducts").modal('show');
          $.ajax({
            url:"<?php echo admin_base_url.'view-user.php';?>",
            method:"POST",
            data:{oid:oid,type:"getOrderedProducts"},
            success:function(data){
              $("#product_cls").html(data);
            }
          });
        })
$(".refund_now").click(function(){
  var oid=$(this).data("oid");
  $("#orderid").val(oid);
  $("#refundPOP").modal('show');
})
    </script>
</html>