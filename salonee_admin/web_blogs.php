<?php
require_once dirname(__DIR__) . '/controller/AdminWebBlogs.php';
$controller = new AdminWebBlogs();
$controller->load();
$blogs = $controller->getBlogs();
$page_data=$controller->pageData();
$title = $page_data['title']; $page = $page_data['page'];
$sub_menu = 'blogs';
$adminId=$page_data['admin_id'];
include "includes/header.php";
include('includes/sidebar.php');

?>

<div class="main__content">
    <div class="w-50">
        <h5>Blogs - Website</h5><br>
    </div>
    <div class="clearfix"></div>
        <h2 class="card-title">Blogs </h2>
        <div class="rightBtn">
            <button class="btn login-btn" data-toggle="modal" data-target="#add_slide_modal" id="myButton">Add &nbsp;<i
                class="fa fa-plus" aria-hidden="true"></i></button>
        </div>
        <div class="pad_3">
            <div class="table-responsive">
                <table class="table themeTable " >
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Image</th>
                            <th>Title</th>                                
                            <th>Short Desc</th>                                
                            <th>Description</th>                                
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $sn=1;
                        if(count($blogs)> 0)
                        {
                            foreach($blogs as $row)
                            {?>
                                <tr>
                                    <td><?php echo $sn++; ?></td>
                                    <td><img   width="120px" height="100px" src="<?php echo $row['image']; ?>"></td>
                                    <td><?php echo $row['name']; ?></td>
                                    <td><?php echo substr(strip_tags($row['short_desc']),0,20)."...";?></td>
                                    <td><?php echo substr(strip_tags($row['description']),0,20)."...";?></td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="javascript:;" data-toggle="modal" data-target="#edit_slide_modal<?php echo $row['blog_id'];?>" class="edit_slide" style="padding:10px;"><i class="fa fa-edit"></i></a>
                                            <a href="web_blogs.php?type=delete_blog&blog_id=<?php echo $row['blog_id'];?>" onclick="return confirm('Are you sure you want to Delete?')" style="padding:10px;"><i class="fa fa-trash"></i></a>
                                        </div>
                                        <div class="modal fade text-left" id="edit_slide_modal<?php echo $row['blog_id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel18" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel18">Edit Blog</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>            
                                                    <div class="modal-body">
                                                        <form method="post" action="web_blogs.php" enctype="multipart/form-data">
                                                            <div class="form-body">
                                                                <div class="text-center">
                                                                    <img id="slide_image" src="<?php echo $row['image']; ?>" width="100px" src="" class="img-fluid mb-1">
                                                                </div>
                                                                <input type="hidden" name="type" value="editBlog" />
                                                                <input type="hidden" name="blog_id" value="<?php echo $row['blog_id'];?>">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label class="form-control-placeholder p-0" for="name">Title</label>
                                                                            <input type="text" name="name" value="<?php echo $row['name'];?>" class="form-control" autocomplete="off" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label class="form-control-placeholder p-0" for="short_desc">Short Description</label>
                                                                            <textarea type="text" name="short_desc" class="form-control" autocomplete="off" required><?php echo $row['short_desc'];?></textarea> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label class="form-control-placeholder p-0" for="desc">Description</label>
                                                                            <textarea type="text" name="desc" class="form-control" autocomplete="off" required><?php echo $row['description'];?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="image">Image (540 * 611 pixels for uniformity and best quality)<span class="danger">*</span></label>
                                                                            <input type="file" id="image" class="form-control" placeholder="Image" name="image">
                                                                        </div>
                                                                    </div>
                                                                   
                                                                </div>
                                                            </div>
                                                            <div class="form-actions text-right">
                                                                <button type="submit" class="btn theme-btn"><i class="la la-check-square-o"></i> Save </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                        else
                        {?>
                            <tr><td class="text-center" colspan="6">No Data</td></tr>
                        <?php
                        }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include 'includes/footer.php';?>
<div class="modal fade text-left" id="add_slide_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17">Add Blog</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>            
            <div class="modal-body">
                <form name="slide_form" id="slide_form" method="post" action="web_blogs.php" enctype="multipart/form-data">
                    <div class="form-body">
                        <input type="hidden" name="type" value="addBlog" />
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-placeholder p-0" for="name">Title</label>
                                    <input type="text" name="name" class="form-control" autocomplete="off" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-placeholder p-0" for="short_desc">Short Description</label>
                                    <textarea type="text" name="short_desc" class="form-control" autocomplete="off" required></textarea> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-placeholder p-0" for="desc">Description</label>
                                    <textarea type="text" name="desc" class="form-control" autocomplete="off" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="image">Image (540 * 611 pixels for uniformity and best quality)<span class="danger">*</span></label>
                                    <input type="file" id="image" class="form-control" placeholder="Image" name="image" required="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-right">
                        <button type="submit" class="btn theme-btn"><i class="la la-check-square-o"></i> Save </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.16.1/standard-all/ckeditor.js"></script>
<script>
CKEDITOR.replace('short_desc', {
  height: 150,
  width: 750,
});
CKEDITOR.replace('desc', {
  height: 150,
  width: 750,
});
</script>