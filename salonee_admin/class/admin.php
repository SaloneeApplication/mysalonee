<?php 
	class admin
	{
		public function checkEmail($email,$con)
		{
			$sql1 = "select count(*) from admin where email = '".$email."'";
			$recordSet1 = mysqli_query($con,$sql1);
			while($row1 = mysqli_fetch_array($recordSet1))
			{
				$count1 = $row1[0];
			}
			if($count1 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function checkAdminDetails($email,$password,$con)
		{
			$emailExistence = $this->checkEmail($email,$con);
			if($emailExistence)
			{
				$sql2 = "select password from admin where email = '".$email."'";
				$recordSet2 = mysqli_query($con,$sql2);
				
				while($row2 = mysqli_fetch_array($recordSet2))
				{
					$dbPassword2 = $row2[0];
				}
				if($dbPassword2 == $password)
				{
					return "correct";
				}
				else
				{
					return "Incorrect Password";
				}	
			}
			else
			{
				return "Email Does not Exist";
			}
		}

		public function checkSubEmployee($email,$password,$con){
			$sql1 = "select email from salesperson where email = '".$email."'";
			$recordSet1 = mysqli_query($con,$sql1);
			$count=0;
			while($row1 = mysqli_fetch_array($recordSet1))
			{
				$count++;
			}
			if($count>0){
				$sql2 = "select password from salesperson where email = '".$email."'";
				$recordSet2 = mysqli_query($con,$sql2);
				
				while($row2 = mysqli_fetch_array($recordSet2))
				{
					$dbPassword2 = $row2[0];
				}
				if($dbPassword2 == $password)
				{
					return "correct";
				}else
				{
					return "Incorrect Password";
				}	
			}else{
				return "Email Does not Exist";

			}
		}
		
		public function getAdminId($email,$con)
		{
			$sql3 = "select a.status,a.admin_id,a.user_role,r.role_access from admin a,roles r where a.user_role=r.role_id and a.email = '".$email."'";
			$recordSet3 = mysqli_query($con,$sql3);
			while($row3 = mysqli_fetch_array($recordSet3))
			{
				$adminId3 = $row3;
			}
			return $adminId3;
		}
			
		public function getEmployeeAdminId($email,$con)
		{
			$sql3 = "select salesperson_id,status from salesperson where email = '".$email."'";
			$recordSet3 = mysqli_query($con,$sql3);
			while($row3 = mysqli_fetch_array($recordSet3))
			{
				$adminId3 = $row3;
			}
			return $adminId3;
		}
		
		public function getadminDetails($adminId,$con)
		{
			$sql4 = "select a.*,r.role_name,r.role_access from admin a,roles r where a.user_role=r.role_id and a.admin_id = $adminId";
			$recordSet4 = mysqli_query($con,$sql4);
			return $recordSet4;
		}
		public function getadminEmployeeDetails($adminId,$con)
		{
			$sql4 = "select s.*,(select city_name_en from cities where id=s.location) as city_name from salesperson s where s.salesperson_id = $adminId";
			$recordSet4 = mysqli_query($con,$sql4);
			return $recordSet4;
		}
		public function setPassword($adminId,$newPassword,$con)
		{
			$sql5 = "update admin set password = '".$newPassword."',modified_time = now() where admin_id = $adminId";
			$rowsAffected5 = mysqli_query($con,$sql5);
			if($rowsAffected5 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function updateadminProfile($adminId,$name,$businessName,$mobile,$image,$con)
		{
			$sql6 = "update admin set name = '".$name."',business_name = '".$businessName."',mobile = '".$mobile."',image = '".$image."',modified_time = now() where admin_id = $adminId";
			$rowsAffected6 = mysqli_query($con,$sql6);
			if($rowsAffected6 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function getLocations($con)
		{
			$sql7 = "select * from location where status = 1 ORDER BY name ASC";
			$recordSet7 = mysqli_query($con,$sql7);
			return $recordSet7;
		}
		
		public function getServiceProviders($con)
		{
			$sql8 = "select * from service_provider ORDER BY service_provider_id DESC";
			$recordSet8 = mysqli_query($con,$sql8);
			return $recordSet8;	
		}

		public function approve_service_provider($service_provider_id,$con)
		{
			$sql9 = "update service_provider set status = 1 where service_provider_id = ".$service_provider_id." ";
			$rowsAffected9 = mysqli_query($con,$sql9);

			$sql9 = "update service_provider_branches set status = 1 where service_provider_id = ".$service_provider_id." and service_provider_main_branch_id  = ".$service_provider_id."  ";
			$rowsAffected9 = mysqli_query($con,$sql9);
			if($rowsAffected9 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function reject_service_provider($service_provider_id,$con)
		{
			$sql10 = "update service_provider set status = 2 where service_provider_id = ".$service_provider_id." ";
			$rowsAffected10 = mysqli_query($con,$sql10);
			if($rowsAffected10 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

?>