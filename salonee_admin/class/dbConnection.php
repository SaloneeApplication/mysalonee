<?php
require_once dirname(__DIR__).'/../config/constants.php';

	/*
	1)Version : 1.0
	2)File Created By : Mr.Shiva Prasad 
	3)File Created On : 12/01/2021
	4)File Modified By : 
	5)File Modified On : 
	*/	
	/*
	Functions
	(1.0)public function getConnection()--Returns ResourceId Or False
	(1.0)public function selectDatabase()--Returns True Or False
	*/	
	class dbConnection
	{//Class 'dbConnection' Opens Here
		
		//Declaring Variables
		private $server = db_server;
		private $userName = db_username;
		private $password = db_password;
		private $database = db_database;
		//Defining Member Functions
		
		
		//(1.0)public function getConnection()--Returns ResourceId Or False
		public function getConnection()
		{//Function 'getConnection'	Opens Here
		
			$con = mysqli_connect($this->server,$this->userName,$this->password,$this ->database);
			return $con;
			
		}//Function 'getConnection'	Closes Here
		
		
				
	}//Class 'dbConnection' Closes Here
?>