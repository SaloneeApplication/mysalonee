<?php
require_once dirname(__DIR__) . '/controller/AdminFeaturedProfiles.php';
$controller = new AdminFeaturedProfiles();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$featured=$page_data['featured'];
include "includes/header.php";
?>
      <!-- BEGIN sidebar -->
      <?php include('includes/sidebar.php');?>
      <!--  //END sidebar -->

      <!-- BEGIN main -->

        <div class="main__content">
          <!-- <div class="w-50">
            <div class="input-group mb-3">
              <input type="text" class="form-control _search" placeholder="Search" id="searchTd">
              <div class="input-group-append">
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
          </div> -->
  

          <div class="pad_3">
          <div class="filtrBtns">
          <a class="btn login-btn btn-sm" style="width:170px;color:white" href="<?php echo admin_base_url.'featured-profiles.php';?>" id="">All F.Profiles &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
          <a class="btn login-btn btn-sm" style="width:170px;color:white"  href="<?php echo admin_base_url.'featured-profiles.php?fea=active';?>"  style="width:100px;color:white"  id="">Active  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
          <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'featured-profiles.php?fea=disabled';?>" style="width:100px;color:white"  id="">Disabled &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
          <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'featured-profiles.php?fea=expired';?>" style="width:100px;color:white"  id="">Expired &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
          </div>      
            <div class="table-responsive">

              <table class="table theme_Table dataTable usersTable adTable" id="example">
                <thead>
                  <tr>
                  <th>S.No</th>
                    <th>Days</th>
                    <th>Last Used Date</th>
                    <th>Service Provider</th>
                    <th>Clicks</th>
                    <th>Amount</th>
                    <th>Order ID</th>
                    <th>Status</th>
                    <th>Date of Create</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                $i=1;
                foreach($featured as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td><span title="total days left">'.$prf['days'].'</span>/<span title="Total days purchased">'.$prf['total_days'].'</span></td>';
                  echo '<td>'.$prf['last_used_date'].'</td>';
                  echo '<td><a href="'.admin_base_url.'view-service-provider.php?id='.$prf['service_provider_id'].'">'.$prf['name'].'</a></td>';
                  echo '<td>'.$prf['clicks'].'</td>';
                  if(round($prf['amount'])==0){
                    echo '<td><strong>[ From Plan ]</strong></td>';
                  }else{
                    echo '<td><strong>AED '.$prf['amount'].'</strong></td>';
                  }
                  echo '<td>'.$prf['order_id'].'</td>';
                  echo '<td>'; 
                  switch($prf['status']){
                    case 'pending':
                    case 'inprocess':
                      echo '<label class="badge badge-pill badge-warning">In Process</label>';
                      break;
                    case 'active':
                      echo '<label class="badge badge-pill badge-success">Active</label>';
                      break;
                    case 'disable':
                      echo '<label class="badge badge-pill badge-warning">Disabled</label>';
                      break;
                    case 'expired':
                      echo '<label class="badge badge-pill badge-danger">Expired</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">Oops</label>';
                    break;
                  }
                  echo '</td>';
                  echo '<td>'.date("Y-M-d H:i A",strtotime($prf['created_time'])).'</td>';
                  echo '</tr>';
                } ?>   
                </tbody>
              </table>

            </div>
            <div class="clearfix"></div>
            <!-- pagination  -->
            <div class='pagination-container tablePaging'>
              <div class="pagination">
                <ol id="numbers"></ol>
              </div>
            </div>


          </div>


        </div>
    </div>
    </main>
    <!--  //END main -->

    <!-- <div class="layout__container-after"></div> -->



  </div>


  <!-- partial -->
  <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Add Advertisement</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="login">
            <form>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <textarea type="text" id="adDesc" rows="5" class="form-control" autocomplete="off" ></textarea>
                    <label class="form-control-placeholder p-0" for="adDesc">Advertisement Description</label>
                  </div>
                </div>
              </div>
             
              <div class="form-group">
                <label for="file-upload" class="custom-file-upload sadmnUpload">
                  UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                </label>
                <input id="file-upload" name='upload_cont_img' type="file" style="display:none;">
              </div>

              <div class="form-group">
                <button type="button" class="btn theme-btn">Submit</button>
              </div>
            </form>
          </div>

        </div>

      </div>
    </div>
  </div>

    <!-- partial -->
    <div class="modal fade pswdModal" id="rejectAD" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Reject Advertiment</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="rejectAD" />
              <input type="hidden" name="id" id="id_a" value="" />
              <input type="hidden" name="sid" id="sid_a" value="" />
                <div class="form-group">
                  <textarea type="text" name="reason" class="form-control" required></textarea>
                  <label class="form-control-placeholder p-0" for="refund_amount">Reason</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Reject Now</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- end popup -->

  <?php include 'includes/footer.php';?>

<script>
  $(document).ready(function() {
      $('#example').dataTable({
        "sPaginationType": "full_numbers",
        "language": { 
              "zeroRecords": "No Records found",           
          },
          aLengthMenu: [
          [10,25, 50, 100, 200, -1],
          [10,25, 50, 100, 200, "All"]
      ],
      // iDisplayLength: -1
      });
  });

</script>



</body>
<script>
$(".reject_ad").click(function(){
  var id=$(this).data("id");
  var sid=$(this).data("sid");
  $("#id_a").val(id);
  $("#sid_a").val(sid);
  $("#rejectAD").modal('show');
})
</script>
</html>