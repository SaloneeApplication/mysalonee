<?php 
require_once dirname(__DIR__) . '/controller/AdminServiceProviders.php';
$controller=new AdminServiceProviders();
$controller->init();
$page_data=$controller->pageData();
$serviceProviders = $page_data['service_providers'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$adminId=$page_data['admin_id'];
include("includes/header.php");
?>

            <!-- BEGIN sidebar -->
            <?php include('includes/sidebar.php');?>
            <!--  //END sidebar -->

            <!-- BEGIN main -->
                <div class="main__content">
                <div class="w-50" style="max-width:530px;">
            <form action="" method="post">
            <div class="input-group mb-3 input-daterange">
                  <div class="input-group-append append_form_data">
                      <select name="search_type" id="search_type" class="btn theme-btn">
                        <option value="by_email" selected="">Email</option>
                        <option value="by_phone">Phone</option>
                      <!--  <option value="by_customerid">User ID</option>-->
                      </select>
                    </div>
                    <input type="text" class="form-control search_by_other" name="search" id="" placeholder="Enter User Email">

                    <div class="input-group-append">
                      <button class="btn theme-btn" type="submit">Search</button>
                    </div>
                  </div>
            </form>
          </div>
                    <!-- <div class="w-50">
                        <div class="input-group mb-3 ">
                            <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
                            <div class="input-group-append">
                                <button class="btn theme-btn" type="submit">Go</button>
                            </div>
                        </div>
                    </div> -->
                    <div class="pad_3">
                        <div class="table-responsive">
                            <table class="table theme_Table dataTable" id="example" >
                                <thead>
                                    <tr>
                                        <th width="1">S.No</th>
                                        <th width="5">Name</th>
                                        <th width="15">Business Name</th>
                                        <th width="25">Email Id</th>
                                        <th width="20">Phone No</th>
                                        <th width="20">Licence</th>
                                        <th width="20">Status</th>
                                        <th width="10">Actions</th>
                                    </tr>
                                </thead>
                                <tbody >
                                    <?php $i=1;
                                    while($row = $serviceProviders->fetch())
                                    {?>
                                        <tr>
                                            <td><?= $i++; ?></td>
                                            <td><?= $row['name']; ?></td>
                                            <td><?= $row['business_name']; ?></td>
                                            <td><?= $row['email']; ?></td>
                                            <td><?= $row['mobile']; ?></td>
                                            <td><a target="blank" href="<?php echo user_base_url; echo $row['business_licence'] ?>"><i class="fa fa-file fa-2x"></i></a></td>
                                            <td>
                                               <?php
                                               if($row['status']==1){
                                                    echo '<label class="badge badge-pill badge-success">Active</label>';
                                               }elseif($row['status']==0){
                                                echo '<label class="badge badge-pill badge-warning">Approval Pending</label>';
                                               }elseif($row['status']==3){
                                                echo '<label class="badge badge-pill badge-danger">Rejected</label>';
                                               }else{
                                                echo '<label class="badge badge-pill badge-danger">Blocked</label>';
                                               }

                                               ?>
                                            </td>
                                            <td>
                                                <?php
                                                
                                                if($row['status'] == 1)
                                                {
                                                    echo '<a href="'.admin_base_url.'service-provider.php?type=block&id='.$row['service_provider_id'].'" class="badge badge-danger"><i class="fa fa-times"></i> Block</a> ';

                                                }
                                                else if($row['status'] == 2)
                                                {
                                                    echo '<a href="'.admin_base_url.'service-provider.php?type=activate&id='.$row['service_provider_id'].'" class="badge badge-success"><i class="fa fa-times"></i> Activate</a> '; 
                                                }
                                                else if($row['status'] == 3)
                                                {
                                                   
                                                }
                                                else if($row['status'] == 0)
                                                {
                                                    echo '<a href="'.admin_base_url.'service-provider.php?type=approve&id='.$row['service_provider_id'].'" class="badge badge-success"><i class="fa fa-check"></i> Approve</a> ';
                                                    echo '<a href="javascript:;" class="badge badge-danger reject_sid" data-id="'.$row['service_provider_id'].'"><i class="fa fa-times"></i> Reject</a> ';
                                                }
                                                echo '<a href="'.admin_base_url.'view-service-provider.php?id='.$row['service_provider_id'].'" class="badge badge-primary"><i class="fa fa-eye"></i> View</a> '; 
                                                ?>
                                            </td>
                                        </tr>
                                    <?php
                                    }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <!-- pagination 
                        <div class='pagination-container tablePaging'>
                            <div class="pagination">
                                <ol id="numbers"></ol>
                            </div>
                        </div> -->
                    </div>
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>
        <?php include 'includes/footer.php';?>
        
    <!-- partial -->
    <div class="modal fade pswdModal" id="rejectSID" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Reject Service Provider</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="rejectSID" />
              <input type="hidden" name="id" id="sid" value="" />
                <div class="form-group">
                  <textarea class="form-control" name="desc" id="desc" required  rows="5" ></textarea>
                  <label class="form-control-placeholder p-0" for="category">Reason</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Submit</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    
        <script>

$(document).ready(function() {
    $('#example').dataTable({
      "sPaginationType": "full_numbers",
      "language": { 
            "zeroRecords": "No Service Providers found.",           
        },
      aLengthMenu: [
        [10,25, 50, 100, 200, -1],
        [10,25, 50, 100, 200, "All"]
    ],
    // iDisplayLength: -1
      });
});


    $(".reject_sid").click(function(){
        var id=$(this).data("id");
        $("#sid").val(id);
        $("#rejectSID").modal('show');
    //     var id=$(this).data("id");
    //     $.ajax({
    //       type:'POST',
    //       url:'service_provider_action.php',
    //       data:{id:id, type:1},
    //       success:function(html){
              
    //           swal({
    //           type: "success",
    //           text: 'Approved Successfully!',
    //           showConfirmButton: false,
    //           timer: 1500
    //         });
    //           setInterval('location.reload()', 1500); 
              
    //       }
    //   });
    });
    $("#search_type").change(function(){
  var type=$(this).val();
  $(".search_by_other").remove();
  if(type=='by_email'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }else if(type=='by_phone'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Phone">');
  }else if(type=='by_customerid'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User ID">');
  }else{
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }
});
        </script>
    </body>
</html>
