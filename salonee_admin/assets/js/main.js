// image upload 
$('#file-upload').change(function() {
  var i = $(this).prev('label').clone();
  var file = $('#file-upload')[0].files[0].name;
  $(this).prev('label').text(file);
});
// table pagination and search 
$(document).ready(function(){
  $(function() {
    const rowsPerPage = 6;
    const rows = $('.themeTable tbody tr');
    const rowsCount = rows.length;
    const pageCount = Math.ceil(rowsCount / rowsPerPage); // avoid decimals
    const numbers = $('#numbers');
    
    // Generate the pagination.
    for (var i = 0; i < pageCount; i++) {
      numbers.append('<li><a href="#">' + (i+1) + '</a></li>');
    }
      
    // Mark the first page link as active.
    $('#numbers li:first-child a').addClass('active');
  
    // Display the first set of rows.
    displayRows(1);
    
    // On pagination click.
    $('#numbers li a').click(function(e) {
      var $this = $(this);
      
      e.preventDefault();
      
      // Remove the active class from the links.
      $('#numbers li a').removeClass('active');
      
      // Add the active class to the current link.
      $this.addClass('active');
      
      // Show the rows corresponding to the clicked page ID.
      displayRows($this.text());
    });
    
    // Function that displays rows for a specific page.
    function displayRows(index) {
      var start = (index - 1) * rowsPerPage;
      var end = start + rowsPerPage;
      
      // Hide all rows.
      rows.hide();
      
      // Show the proper rows for this page.
      rows.slice(start, end).show();
    }
  });
  
        
  // swal  service providers actions
  $(".trash_icon").click(function () {
    swal({
      title: "Are you sure?",
      text: "You want to delete this Account",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
    }).then(result => {
      if (result.dismiss === 'cancel') {
        swal.closeModal();
        return;
      }
      swal({
        type: "success",
        text: 'Account has been deleted!',
        showConfirmButton: false,
        timer: 1500
      });

    },
    )
  })
  // approve service provider  
  $("#approve").click(function(){
      var id = $(this).data('id');
      $.ajax({
          type:'POST',
          url:'service_provider_action.php',
          data:{id:id, type:1},
          success:function(html){
              
              swal({
              type: "success",
              text: 'Approved Successfully!',
              showConfirmButton: false,
              timer: 1500
            });
              setInterval('location.reload()', 1500); 
              
          }
      });
  });

  //reject service provider  
  $("#reject").click(function(){
      var id = $(this).data('id');
      $.ajax({
          type:'POST',
          url:'service_provider_action.php',
          data:{id:id, type:2},
          success:function(html){
              
              swal({
              type: "success",
              text: 'Rejected Successfully!',
              showConfirmButton: false,
              timer: 1500
            });
              setInterval('location.reload()', 1500); 
              
          }
      });
  });


  // //cateogies 
  // $(".fa-trash").click(function () {
  //   swal({
  //     title: "Are you sure?",
  //     text: "You want to delete this Category",
  //     type: "warning",
  //     showCancelButton: true,
  //     confirmButtonColor: "#DD6B55",
  //     confirmButtonText: "Yes, delete it!",
  //   }).then(result => {
  //     if (result.dismiss === 'cancel') {
  //       swal.closeModal();
  //       return;
  //     }
  //     swal({
  //       type: "success",
  //       text: 'Category has been deleted!',
  //       showConfirmButton: false,
  //       timer: 1500
  //     });
  //   },
  //   );
  // });

  
// button value toggle  advertisemenet
$('.publsh').click(function() {
  if ($(this).val() == "PUBLISH") {
     $(this).val("UNPUBLISH");
     $(this).removeClass("grn");
       $(this).addClass("pnk");
       swal({
          type: "success",
          text: 'Advertisement is Publish Successfully',
          showConfirmButton: false,
          timer: 1500
        });
  }
  else {
     $(this).val("PUBLISH");
     $(this).removeClass("pnk");
       $(this).addClass("grn");
       
  }
});
});




