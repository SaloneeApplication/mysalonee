<style type="text/css">
	.dropdown-container {
    display: none;
  }
  .dropdown-container a {
    font-size: 14px !important;
    padding-left: 50px!important;
    font-weight: 600 !important;
}

  .dropdown-btn{
      cursor: pointer;
      color:#333 !important;
  }
</style>
<aside class="sidebar">
	<div class="sidebar__header">
	  <a href="dashboard.php">
		<img src="./assets/img/logo1.png" alt="logo" class="admin_logo">
	  </a>
	</div>
	<div class="sidebar__content">
	  <ul class="sidebar__content-menu">
	<?php if($_SESSION['auth_type']=='admin'){
			$access_pages=json_decode($_SESSION['role_access']);
			if(in_array('dashboard',$access_pages)){
		?>
		<li class="<?php if(@$page == 'dashboard') { echo 'sidebar--active'; }?>"><a href="dashboard.php"><i class="fa fa-dashboard fa-lg"></i> Dashboard</a></li>
		<?php } 
		if(in_array('cms',$access_pages)){
			 ?>

		<li class="<?php if(@$page == 'Content') { echo 'sidebar--active'; }?>">
      <a class="dropdown-btn"><i class="fa fa-file-text-o fa-lg"></i> CMS 
        <i class="fa fa-caret-down"></i>
      </a>
      <div class="dropdown-container">
        <a <?php if(@$sub_menu == 'home_content') { echo 'style="color: #d66292;"'; }?> href="web_content.php">Home Page Content</a>
        <a <?php if(@$sub_menu == 'sliders') { echo 'style="color: #d66292;"'; }?> href="web_banners.php">Sliders</a>
        <a <?php if(@$sub_menu == 'nav_links') { echo 'style="color: #d66292;"'; }?> href="web_nav_links.php">Navigation Links</a>
        <a <?php if(@$sub_menu == 'blogs') { echo 'style="color: #d66292;"'; }?> href="web_blogs.php">Blogs</a>
      </div>
    </li>
		<?php } 
		if(in_array('users',$access_pages)){
			 ?>

		<li class="<?php if(@$page == 'users') { echo 'sidebar--active'; }?>"><a href="users.php"><i class="fa fa-user-circle-o fa-lg"></i> Users</a></li>
		<?php } 
		if(in_array('service_provider',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'service-provider') { echo 'sidebar--active'; }?>"><a href="service-provider.php"><i class="fa fa-address-card-o fa-lg"></i> Service Provider</a></li>
		<?php } 
		if(in_array('countries',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'countries') { echo 'sidebar--active'; }?>"><a href="countries.php">Countries</a></li>
		<?php } 
		if(in_array('city',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'city') { echo 'sidebar--active'; }?>"><a href="cities.php">City</a></li>
		<?php } 
		if(in_array('salespersons',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'salespersons') { echo 'sidebar--active'; }?>"><a href="salesperson.php"><i class="fa fa-user-o fa-lg"></i> Salespersons</a></li>
		<?php } 
		if(in_array('employees',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'employees') { echo 'sidebar--active'; }?>"><a href="employees.php"><i class="fa fa-users fa-lg"></i> Employees</a></li>
		<?php } 
		if(in_array('service_bookings',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'services_bookings') { echo 'sidebar--active'; }?>"><a href="service-bookings.php"><i class="fa fa-calendar-check-o fa-lg"></i> Service Bookings</a></li>
		<?php } 
		if(in_array('category',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'category') { echo 'sidebar--active'; }?>"><a href="category.php"><i class="fa fa-tags fa-lg"></i> Category</a></li>
		<?php } 
		if(in_array('promo_codes',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'promocodes') { echo 'sidebar--active'; }?>"><a href="promocode.php"><i class="fa fa-gift fa-lg"></i> Promo Codes</a></li>
		<?php } 
		if(in_array('advertisement',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'advertisement') { echo 'sidebar--active'; }?>"><a href="advertisement.php"><i class="fa fa-bullhorn fa-lg"></i> Advertisement</a></li>
		<?php } 
		if(in_array('featured_profiles',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'featured_profiles') { echo 'sidebar--active'; }?>"><a href="featured-profiles.php"><i class="fa fa-trophy fa-lg"></i> Featured Profiles</a></li>
		<?php } 
		if(in_array('products',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'products') { echo 'sidebar--active'; }?>"><a href="products.php"><i class="fa fa-shopping-bag fa-lg"></i> Products</a></li>
		<?php } 
		if(in_array('orders',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'orders') { echo 'sidebar--active'; }?>"><a href="orders.php"><i class="fa fa-shopping-cart fa-lg"></i> Orders</a></li>
		<?php } 
		if(in_array('product_orders',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'product_orders') { echo 'sidebar--active'; }?>"><a href="product-orders.php"><i class="fa fa-cubes fa-lg"></i> Product Orders</a></li>
		<?php } 
		if(in_array('subscription',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'subscription') { echo 'sidebar--active'; }?>"><a href="subscription.php"><i class="fa fa-refresh fa-lg"></i> Subscription</a></li>
		<?php } 
		if(in_array('ratings',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'ratings') { echo 'sidebar--active'; }?>"><a href="ratings.php"><i class="fa fa-star fa-lg"></i> Ratings</a></li>
		<?php } 
		
		if(in_array('services_reports',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'service_reports') { echo 'sidebar--active'; }?>"><a href="service-reports.php"><i class="fa fa-file-excel-o fa-lg"></i> Services Reports</a></li>
		<?php } 
		if(in_array('service_provider_reports',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'service_provider_reports') { echo 'sidebar--active'; }?>"><a href="service-provider-reports.php"><i class="fa fa-file-excel-o fa-lg"></i> Service Provider Reports</a></li>
		<?php } 
		if(in_array('user_reports',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'user_reports') { echo 'sidebar--active'; }?>"><a href="user-reports.php"><i class="fa fa-file-excel-o fa-lg"></i> User Reports</a></li>
		<?php } 
		if(in_array('product_reports',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'product_reports') { echo 'sidebar--active'; }?>"><a href="product-reports.php"><i class="fa fa-file-excel-o fa-lg"></i> Product Reports</a></li>
		<?php } 
		if(in_array('revenue_reports',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'revenue_reports') { echo 'sidebar--active'; }?>"><a href="revenue-reports.php"><i class="fa fa-file-excel-o fa-lg"></i> Revenue Reports</a></li>
		<?php } 
		if(in_array('support_tickets',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'support_tickets') { echo 'sidebar--active'; }?>"><a href="support-tickets.php"><i class="fa fa-ticket fa-lg"></i> Support Tickets</a></li>
		<?php }
		if(in_array('privacy_policy',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'privacy_policy') { echo 'sidebar--active'; }?>"><a href="privacy-policy.php">Privacy Policy FAQ</a></li>
		<?php }  
		if(in_array('settings',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'settings') { echo 'sidebar--active'; }?>"><a href="settings.php"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
		<?php }
			 ?>
		<?php }elseif($_SESSION['auth_type']=='salesperson'){
			?>
<li class="<?php if(@$page == 'sales_person_dashboard') { echo 'sidebar--active'; }?>"><a href="sales-dashboard.php">Dashboard</a></li>
<li class="<?php if(@$page == 'service_providers') { echo 'sidebar--active'; }?>"><a href="sales-person.php">Service Providers</a></li>
		
			<?php
		} ?>
	  </ul>
	</div>
</aside>
<main class="main">

 <div class="main__header">
          <div class="main__header-heading">
            <h1>
				<?php if($_SESSION['auth_type']=='admin'){
					echo 'Administrator';
				}elseif($_SESSION['auth_type']=='salesperson'){
					echo 'Sales Person';
				} ?>
			</h1>
          </div>

          <div class="main__header-user">

            <button type="button" class="main__header-user-toggle" data-toggle="dropdown" data-toggle-for="user-menu"
              role="button">
              <span class="main__header-user-toggle-picture">
				<?php 
					if($image == "")
					{
				?>
                <img src="./assets/img/admin/avatar-male.png" alt="">
				<?php 
					}
					else
					{
						echo " <img src='".admin_base_url."$image' alt=''>";
					}
				?>
              </span>
            </button>
            <!-- <div class="main__header-user-menu dropdown-menu" > -->
            <ul class="main__header-user-menu-content dropdown-menu">
              <li>
                <div class="main__header-user-menu-header ">
                  <h3><?php echo $name;?></h3>
                  <span>Role : <?php echo $role_name;?></span>
                </div>
              </li>
              <li data-toggle="modal" data-target="#myProfile"><a class="d-m" href="#"><i
                    class="icon icon--profile"></i>My profile</a></li>
              <li data-toggle="modal" data-target="#changePassword"><a class="d-m" href="#"><i
                    class="icon icon--help"></i>Change Password</a></li>
              <li><a class="d-m" href="signOut.php"><i class="icon icon--sign-out"></i>Sign out</a></li>
            </ul>
            <!-- </div> -->
          </div>
        </div>
<script type="text/javascript">
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>