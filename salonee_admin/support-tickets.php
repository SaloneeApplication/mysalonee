<?php

require_once dirname(__DIR__) . '/controller/AdminSupportTicket.php';
$sales = new AdminSupportTicket();
$sales->init();
$persons=array();
$page_data=$sales->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$tickets=$page_data['tickets'];
include "includes/header.php";
?>
            <!-- BEGIN sidebar -->
            <?php include 'includes/sidebar.php';?>
            <!--  //END sidebar -->
<style>
table.table.themeTable td, table.table.themeTable th {
  max-width:none;
}
</style>
            <!-- BEGIN main -->
                <div class="main__content">
                    <div class="pad_3">
                  <div class="filtrBtns">
                    <a class="btn login-btn btn-sm" style="width:170px;color:white" href="<?php echo admin_base_url.'support-tickets.php';?>" id="">All Tickets  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    <a class="btn login-btn btn-sm" style="width:170px;color:white"  href="<?php echo admin_base_url.'support-tickets.php?tic=open';?>"  style="width:100px;color:white"  id="">Open  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'support-tickets.php?tic=closed';?>" style="width:100px;color:white"  id="">Closed&nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>
                        <div class="table-responsive">
                            <table class="table themeTable " >
                                <thead>
                                    <tr>
                                      <th>S.No</th>
                                      <th>Ticket ID</th>
                                      <th>User</th>
                                      <th>Created Date</th>
                                      <th>Title</th>
                                      <th>Order ID</th>
                                      <th>Attachment</th>
                                      <th>Status</th>
                                      <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="saloonTable">
                                <?php 
                $i=1;
                foreach($tickets as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['ticket_id'].'</td>';
                  echo '<td><a href="'.admin_base_url.'view-user.php?id='.$prf['user_id'].'">'.$prf['name'].'</a></td>';
                  echo '<td>'.date("Y-M-d H:i A",strtotime($prf['date_of_create'])).'</td>';
                  echo '<td>'.substr($prf['title'],0,150).' ...</td>';
                  echo '<td>'.$prf['order_id'].'</td>';
                  echo '<td>';
                    if($prf['attachment']!=''){
                        echo '<a href="'.$prf['attachment'].'" target="_blank"><i class="fa fa-download"></i> Download</a>';
                    }
                  echo '</td>';

                  echo '<td>'; 
                  switch($prf['status']){
                    case 'open':
                      echo '<label class="badge badge-pill badge-warning">Open</label>';
                      break;
                    case 'close':
                      echo '<label class="badge badge-pill badge-success">Closed</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">In Process</label>';
                    break;
                  }
                  echo '</td>';
              
                  echo '<td>';
                  if($prf['status']=='open'){
                    echo '<a  href="javascipt:;" data-tid="'.$prf['ticket_id'].'" class=" view_ticket badge badge-primary"> <i class="fa fa-eye"></i> View Ticket </a>&nbsp;<br/>';
                    echo '<a  href="javascipt:;" data-tid="'.$prf['ticket_id'].'" class=" replay_now badge badge-warning"> <i class="fa fa-reply"></i> Reply </a>&nbsp;<br/>';
                  }
                  
                  echo '</td>';
                 
                  echo '</tr>';
                } ?>   
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <!-- pagination  -->
                        <div class='pagination-container tablePaging'>
                            <div class="pagination">
                                <ol id="numbers"></ol>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>

        <div class="modal fade pswdModal" id="viewTicket" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Ticket Info</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="login">
                    <div class="col-md-10 offset-2 ticket_description">
                        <div class="form-group">
                            <label>Issue Title</label>
                            <p>asdad</p>
                        </div>
                        <div class="form-group">
                            <label>Issue Description</label>
                            <p>asdad</p>
                        </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

    <!-- order products partial -->
    <div class="modal fade pswdModal" id="replayTicket" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Reply</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <form action="" method="POST" >
            <div class="login">
              <input type="hidden" name="type" value="replyTicket" />
              <input type="hidden" name="tid" id="tid_i" value="" />
              <div class="form-group">
                    <textarea class="form-control" name="reply" placeholder="" required></textarea>
                  <label class="form-control-placeholder p-0" for="refund_amount">Reply to Ticket</label>
                </div>
                <div class="form-group">
                <button type="submit" class="btn theme-btn">Submit</button>

                </div>
            </div>
            </form>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->

    
    <!-- partial -->
    <div class="modal fade pswdModal" id="refundPOP" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Refund</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="refundOrderID" />
              <input type="hidden" name="id" id="orderid" value="" />
                <div class="form-group">
                  <input type="text" name="refund_amount" class="form-control" id="refund_amount" required/>
                  <label class="form-control-placeholder p-0" for="refund_amount">Refund Amount</label>
                </div>
                <div class="form-group">
                  <input type="text" name="refund_deductions" class="form-control" value="0" id="refund_deductions" required/>
                  <label class="form-control-placeholder p-0" for="refund_deductions">Refund Deductions</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Refund Now</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- end popup -->
        <?php include 'includes/footer.php';?>
    </body>
    <script>
        $(".replay_now").click(function(){
          var tid=$(this).data("tid");
          $("#tid_i").val(tid)
          $("#replayTicket").modal('show');
        })
$(".view_ticket").click(function(){
  var tid=$(this).data("tid");
  $.ajax({
    url:"<?php echo admin_base_url.'support-tickets.php';?>",
    method:"POST",
    data:{tid:tid,type:"getTicketInfo"},
    success:function(data){
        $(".ticket_description").html(data);
        $("#viewTicket").modal('show');
    }
});
});
    </script>
</html>