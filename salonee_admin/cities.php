<?php
require_once dirname(__DIR__) . '/controller/AdminCities.php';
require_once dirname(__DIR__) . '/controller/AdminCountries.php';
$controller = new AdminCities();
$controllerCountries = new AdminCountries();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$cities=$controller->getCities();
$Countries = $controllerCountries->getActiveCountries();
$editCountries = $controllerCountries->getActiveCountries();
include "includes/header.php";
?>

      <!-- BEGIN sidebar -->
      <?php include('includes/sidebar.php');?>
      <!--  //END sidebar -->

      <!-- BEGIN main -->
      <main>
        <div class="main__content">
          <div class="w-50">
            <div class="input-group mb-3">
              <input type="text" class="form-control _search" id="category_search" placeholder="Search">
              <div class="input-group-append">
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
          </div>
          <div class="rightBtn">
            <button class="btn login-btn" data-toggle="modal" data-target="#addcity" id="myButton">Add &nbsp;<i
                class="fa fa-plus" aria-hidden="true"></i></button>
          </div>
          <br><br>
          <div class="clearfix"></div>
          <ul class="paginationTable salonee_category category-card">

            <?php while($row=$cities->fetch()){ ?>
            <li class=" tableItem">
              <span><img src="<?php echo admin_base_url.$row['image'];?>" width="100" alt="category"></span>
              <p>
                <b><?php echo $row['country_name']; ?></b>
                <b> <?php echo ucwords($row['name']); ?></b>
                <?php echo ($row['is_top'] == 1) ? 'Top Priory' : '';?>
              </p>
              <small><i class="fa fa-pencil-square-o edit_cities" data-id="<?php echo $row['id'];?>" data-country-id="<?php echo $row['country_id'];?>"  data-city-name="<?php echo $row['name'];?>" data-is-top="<?php echo $row['is_top'];?>" aria-hidden="true"></i> 
              
              <?php if($row['status']==1){?>
              <i class="fa fa-eye-slash deactivatecity" style="color:red;" title="Disable" data-id="<?php echo $row['id'];?>" aria-hidden="true"> </i>
              <?php }else{ ?>
                <i class="fa fa-eye activatecity" style="color:green;" title="Enable" data-id="<?php echo $row['id'];?>" aria-hidden="true"> </i>
              <?php } ?>
                  
              </small>
            </li>
            <?php }?>
            
            
          </ul>
          <div class="pagination-container">
            <p class='paginacaoCursor beforePagination' id=""><i class="fa fa-chevron-left" aria-hidden="true"></i></p>
            <p class='paginacaoCursor afterPagination' id=""><i class="fa fa-chevron-right" aria-hidden="true"></i></p>
          </div>


        </div>
      </main>
      <!--  //END main -->

      <!-- <div class="layout__container-after"></div> -->

    </div>

    <!-- add category modal  -->
    <div class="modal fade pswdModal" id="addcity" tabindex="-1" role="dialog" aria-labelledby="addCityTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addCityTitle">Add City</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="<?php echo admin_base_url;?>cities.php" method="post" id="city_im" onsubmit="return validateForm()" name="city_im" enctype="multipart/form-data">
              <input type="hidden" name="type" value="addNewCity" />
              <div class="form-group mt-4 mb-5">
                <select name="country_id" id="country_id" class="form-control" autocomplete="off" required >
                  <option hidden disabled selected value> -- Select Country -- </option>
            <?php while($row=$Countries->fetch()){ ?>
                  <option value="<?php echo $row['id'];?>"><?php echo ucwords($row['country_name']);?></option>
                <?php } ?>
                </select>
              </div>
                <div class="form-group mt-4 mb-5">
                  <input type="text" id="city_name" name="city_name" class="form-control" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="city_name">City Name</label>
                </div>
                <div class="form-group mt-4 mb-5">
                <select name="is_top" id="is_top" class="form-control" autocomplete="off" required >
                  <option hidden disabled selected value> -- Set as Top Priority -- </option>
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
                </div>
                <div class="form-group mb-5">
                  <label for="file-upload" class="custom-file-upload">
                    UPLOAD CITY IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                  </label>
                  <input id="file-upload" name='city_image' type="file"  style="display:none;">
                </div>
                <div class="form-group">
                  <input type="submit" id="citySubmit" class="btn theme-btn citySubmit" value="Submit">
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>

    <!-- add category modal  -->
    <div class="modal fade pswdModal" id="edit_cities" tabindex="-1" role="dialog" aria-labelledby="addCategoryTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="EditCityTitle">Edit City</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="<?php echo admin_base_url;?>cities.php" method="post" enctype="multipart/form-data">
              <input type="hidden" name="type" value="updateCity" />
              <input type="hidden" name="city_id" id="city_id" value="" />
              <div class="form-group mt-4 mb-5">
                <select name="country_id" id="country_id1" class="form-control" autocomplete="off" required >
                  <option hidden disabled selected value> -- Select Country -- </option>
            <?php while($row=$editCountries->fetch()){ ?>
                  <option value="<?php echo $row['id'];?>"><?php echo ucwords($row['country_name']);?></option>
                <?php } ?>
                </select>
              </div>
                <div class="form-group mt-4 mb-5">
                  <input type="text" id="city_name1" name="city_name" class="form-control" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="city_name">City Name</label>
                </div>
                <div class="form-group mt-4 mb-5">
                <select name="is_top" id="is_top1" class="form-control" autocomplete="off" required >
                  <option hidden disabled selected value> -- Set as Top Priority -- </option>
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
                </div>
                <div class="form-group mb-5">
                  <label for="file-upload1" class="custom-file-upload">
                    UPLOAD CITY IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                  </label>
                  <input id="file-upload1" name='city_image' type="file" style="display:none;">
                </div>
                <div class="form-group">
                  <input type="submit" id="cityUpdate" class="btn theme-btn cityUpdate" value="Submit">
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>


    <?php include 'includes/footer.php';?>
    <script>
    $(".edit_cities").click(function(){
      var cityid=$(this).data("id");
      var country_id=$(this).data("country-id");
      var name=$(this).data("city-name");
      var is_top=$(this).data("is-top");
      $("#city_id").val(cityid);
      $("#country_id1").val(country_id);
      $("#city_name1").val(name);
      $("#is_top1").val(is_top);
      $("#edit_cities").modal('show');

    });
      $("form[name='city_im']").submit(function(e){
        if(typeof $('#file-upload')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select city image',
          });
          e.preventDefault();
          return false;
        }
      })
      $('#file-upload1').change(function() {
        var i = $(this).prev('label').clone();
        var file = $('#file-upload1')[0].files[0].name;
        $(this).prev('label').text(file);
      });
       
  $(".deactivatecity").click(function () {
    swal({
      title: "Are you sure?",
      text: "You want to disable this city",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, Deactivate it!",
    }).then(result => {
      if (result.dismiss === 'cancel') {
        swal.closeModal();
        return;
      }
      $.ajax({
        url:"<?php echo admin_base_url.'cities.php';?>",
        method:"POST",
        data:{type:'disablecity',city_id:$(this).data("id")},
        success:function(resp){
          var resp=JSON.parse(resp);
          if(resp.status==1){
           location.reload();
          }
        }
      })
      
    },
    );
  });
  $(".activatecity").click(function () {
    swal({
      title: "Are you sure?",
      text: "You want to enable this city",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, Activate it!",
    }).then(result => {
      if (result.dismiss === 'cancel') {
        swal.closeModal();
        return;
      }
      $.ajax({
        url:"<?php echo admin_base_url.'cities.php';?>",
        method:"POST",
        data:{type:'enablecity',city_id:$(this).data("id")},
        success:function(resp){
          var resp=JSON.parse(resp);
          if(resp.status==1){
           location.reload();
          }
        }
      })
    },
    );
  });
    </script>
</body>
</html>