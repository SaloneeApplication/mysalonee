
<?php 
require_once dirname(__DIR__) . '/controller/AdminServiceBookings.php';
$controller=new AdminServiceBookings();
$controller->init();
$page_data=$controller->pageData();
$service_bookings = $page_data['service_bookings'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$adminId=$page_data['admin_id'];
include("includes/header.php");
?>

            <!-- BEGIN sidebar -->
            <?php include('includes/sidebar.php');?>
            <!--  //END sidebar -->

            <!-- BEGIN main -->
                <div class="main__content">
               
                    <!-- <div class="w-50">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
                            <div class="input-group-append">
                                <button class="btn theme-btn" type="submit">Go</button>
                            </div>
                        </div>
                      
                    </div> -->

                <div class="pad_3">
                  <div class="filtrBtns">
                    <a class="btn login-btn btn-sm" style="width:170px;color:white" href="<?php echo admin_base_url.'service-bookings.php';?>" id="">All Slots &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        <a class="btn login-btn btn-sm" style="width:170px;color:white"  href="<?php echo admin_base_url.'service-bookings.php?slots=pending';?>"  style="width:100px;color:white"  id="">Pending Slots &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'service-bookings.php?slots=completed';?>" style="width:100px;color:white"  id="">Completed Slots &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        <a class="btn login-btn btn-sm"   style="width:190px;color:white" href="<?php echo admin_base_url.'service-bookings.php?slots=cancelled';?>" style="width:100px;color:white"  id="">Cancelled Slots &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        <a class="btn login-btn btn-sm" style="width:220px;color:white"   href="<?php echo admin_base_url.'service-bookings.php?slots=rescheduled';?>" style="width:100px;color:white"  id="">Rescheduled Slots &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  </div>

                        <div class="table-responsive">
                            <table class="table theme_Table dataTable" id="example" >
                                <thead>
                                <tr>
                    <th>S.No</th>
                  <!--  <th>Slot ID</th>-->
                    <th>Order ID</th>
                    <th>User</th>
                    <th>Service Provider</th>
                    <th>Service</th>
                    <th>Service At</th>
                    <th>Amount</th>
                    <th>Commissions</th>
                    <th>City</th>
                    <th>Status</th>
                    <th>Date of Slot</th>
                  </tr>
                                </thead>
                                <tbody >
                                <?php 
                $i=1;
                foreach($service_bookings as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                 // echo '<td>'.$prf['service_slot_id'].'</td>';
                  echo '<td>'.$prf['order_id'].'</td>';
                  echo '<td class="service_provlink" onclick="window.location.href=\''.admin_base_url.'view-user.php?id='.$prf['user_id'].'\';">'.$prf['user_name'].'</td>';
                  echo '<td class="service_provlink" onclick="window.location.href=\''.admin_base_url.'view-service-provider.php?id='.$prf['service_provider_id'].'\';">'.$prf['name'].'</td>';
                  echo '<td>'.$prf['service_name'].'</td>';
                  echo '<td>'.$prf['service_type'].'</td>';
                  echo '<td><strong>AED '.$prf['total_amount'].'</strong></td>';
                  echo '<td> 
                  A.C <strong>AED '.$prf['admin_commission'].'</strong> <br/>
                  T.C <strong>AED '.$prf['transaction_charges'].'</strong> <br/>
                  </td>';
                  echo '<td>'.$prf['city_name'].'</td>';
                  echo '<td>'; 
                  switch($prf['service_status']){
                    case 'pending':
                    case 'inprocess':
                      echo '<label class="badge badge-pill badge-warning">Pending</label>';
                      break;
                    case 'completed':
                      echo '<label class="badge badge-pill badge-success">Completed</label>';
                      break;
                    case 'rescheduled':
                      echo '<label class="badge badge-pill badge-primary">Rescheduled</label>';
                      break;
                    case 'cancelled':
                      echo '<label class="badge badge-pill badge-danger">Cancelled</label>';
                      break;
                  }
                  echo '</td>';
                  echo '<td>'.date("Y-M-d H:i A",strtotime($prf['slot_date'])).'</td>';
                  echo '</tr>';
                } ?>   
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <!-- pagination  -->
                        <div class='pagination-container tablePaging'>
                            <div class="pagination">
                                <ol id="numbers"></ol>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>
        <?php include 'includes/footer.php';?>
        
    <!-- partial -->
    <div class="modal fade pswdModal" id="rejectSID" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Reject Service Provider</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="rejectSID" />
              <input type="hidden" name="id" id="sid" value="" />
                <div class="form-group">
                  <textarea class="form-control" name="desc" id="desc" required  rows="5" ></textarea>
                  <label class="form-control-placeholder p-0" for="category">Reason</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Submit</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    

<style>
  #example tr th{
    white-space: nowrap;
  }
</style>

<script> 

$(document).ready(function() {
    $('#example').dataTable({
      "sPaginationType": "full_numbers",
      "language": { 
            "zeroRecords": "No Records found.",           
        },
      aLengthMenu: [
        [10,25, 50, 100, 200, -1],
        [10,25, 50, 100, 200, "All"]
    ],
    // iDisplayLength: -1
      });
});



    $(".reject_sid").click(function(){
        var id=$(this).data("id");
        $("#sid").val(id);
        $("#rejectSID").modal('show');
    //     var id=$(this).data("id");
    //     $.ajax({
    //       type:'POST',
    //       url:'service_provider_action.php',
    //       data:{id:id, type:1},
    //       success:function(html){
              
    //           swal({
    //           type: "success",
    //           text: 'Approved Successfully!',
    //           showConfirmButton: false,
    //           timer: 1500
    //         });
    //           setInterval('location.reload()', 1500); 
              
    //       }
    //   });
    });
    $("#search_type").change(function(){
  var type=$(this).val();
  $(".search_by_other").remove();
  if(type=='by_email'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }else if(type=='by_phone'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Phone">');
  }else if(type=='by_customerid'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User ID">');
  }else{
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }
});
        </script>
    </body>
</html>
