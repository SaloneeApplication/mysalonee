<?php
require_once dirname(__DIR__) . '/controller/AdminSubscriptions.php';
$controller = new AdminSubscriptions();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$plans=$page_data['plans'];
include "includes/header.php";
?>
      <!-- BEGIN sidebar -->
      <?php include('includes/sidebar.php');?>
      <!--  //END sidebar -->

      <!-- BEGIN main -->
        <div class="main__content">
          <div class="w-50">
            <div class="input-group mb-3 _hide">
              <input type="text" class="form-control _search" placeholder="Search" id="searchTd">
              <div class="input-group-append">
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
          </div>
          
          <div class="rightBtn">
            <button class="btn login-btn" data-toggle="modal" data-target="#addPromoCode" id="myButton">Add &nbsp;<i
                class="fa fa-plus" aria-hidden="true"></i></button>
          </div>

          <br>
          <div class="clearfix"></div>

          <div class="pad_3">
            <div class="table-responsive">

              <table class="table theme_Table dataTable usersTable adTable" id="example">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Plan Name</th>
                    <th>Duration</th>
                    <th>Price</th>
                    <th>Free Adv days</th>
                    <th>Free Featured Days</th>
                    <th>Recommended</th>
                    <th>status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                $i=1;
                foreach($plans as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['name'].'</td>';
                  echo '<td>';
                  echo ($prf['duration']==1)?$prf['duration'].' Month':$prf['duration'].' Months';
                  echo '</td>';
                  echo '<td>AED '.$prf['price'].'</td>';
                  echo '<td>'.$prf['free_advertisements_days'].'</td>';
                  echo '<td>'.$prf['free_featured_profiles_days'].'</td>';
                  echo '<td>';
                  echo ($prf['recommand_plan']==1)?'<span class="badge badge-pill badge-warning">Recommended</span>':'';
                  echo  '</td>';
                  echo '<td>'; 
                  switch($prf['status']){
                    case '0':
                      echo '<label class="badge badge-pill badge-warning">Inactive</label>';
                      break;
                    case '1':
                      echo '<label class="badge badge-pill badge-success">Active</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">In Process</label>';
                    break;
                  }
                  echo '</td>';
                  echo '<td>';
                  echo '<a  href="javascript:return false;" data-id="'.$prf['subscription_plan_id'].'" class="editNow refund_now badge badge-warning"> <i class="fa fa-edit"></i> Edit </a> &nbsp;';
                  if($prf['status']=='1'){
                      echo '<a  href="'.admin_base_url.'subscription.php?type=blockPlan&id='.$prf['subscription_plan_id'].'" class=" refund_now badge badge-danger"> <i class="fa fa-times"></i> Block </a>';
                    }else{
                      echo '<a  href="'.admin_base_url.'subscription.php?type=activePlan&id='.$prf['subscription_plan_id'].'" class=" refund_now badge badge-success"> <i class="fa fa-check"></i> Active </a>';
                  }
                  if($prf['recommand_plan']=='0'){
                    echo '<a  href="'.admin_base_url.'subscription.php?type=setRecommand&id='.$prf['subscription_plan_id'].'" class=" refund_now badge badge-primary"> <i class="fa fa-check"></i> Set Recommend </a>';
                  }
                  echo '</td>';
                 
                  echo '</tr>';
                } ?>   

                </tbody>
              </table>

            </div>
            <div class="clearfix"></div>

            <!-- pagination  -->
            <div class='pagination-container tablePaging'>
              <div class="pagination">
                <ol id="numbers"></ol>
              </div>
            </div>


          </div>


        </div>
    </div>
    </main>
    <!--  //END main -->

    <!-- <div class="layout__container-after"></div> -->



  </div>


  <!-- partial -->
  <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">New Subscription Plan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="login">
            <form action="" method="POST">
            <input type="hidden" name="type"  value="newPlan" />
                <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <input type="text" id="adName" class="form-control" name="plan_name" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="adName">Plan Name</label>
                      </div>
                    </div>
                  </div>
                <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="text" id="duration" class="form-control" name="duration"  autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="duration">Duration ( No. Months )</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="text" id="price" class="form-control"  name="price" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="price">Price ( AED )</label>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="number" id="duration" class="form-control" min="0" name="free_adver"  autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="duration">Free Advertisements Days</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="number" id="price" class="form-control" min="0"  name="free_featured" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="price">Free Featured Profiles Days</label>
                      </div>
                    </div>
                  </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <textarea type="text" id="adDesc" rows="5" name="desc" class="form-control" autocomplete="off" ></textarea>
                    <label class="form-control-placeholder p-0" for="adDesc">Description</label>
                  </div>
                </div>
              </div>
             

              <div class="form-group">
                <button type="submit" class="btn theme-btn">Submit</button>
              </div>
            </form>
          </div>

        </div>

      </div>
    </div>
  </div>
<!-- end modal -->


  <!-- start edit plan -->
  <div class="modal fade pswdModal" id="editPlanData" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Edit Subscription Plan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="login">
            <form action="" method="POST" id="form_data">
            
                
            </form>
          </div>

        </div>

      </div>
    </div>
  </div>
<!-- end edit plan -->
  <?php include 'includes/footer.php';?>
<script>
    $(document).ready(function() {
      $('#example').dataTable({
        "sPaginationType": "full_numbers",
        "language": { 
              "zeroRecords": "No Records found",           
          },
          aLengthMenu: [
          [10,25, 50, 100, 200, -1],
          [10,25, 50, 100, 200, "All"]
      ],
      // iDisplayLength: -1
      });
  });
</script>


</body>
<script>
$(".editNow").click(function(){
    var id=$(this).data("id");
    $("#plan_id").val(id);
    $("#editPlanData").modal('show');
    $.ajax({
      url:"<?php echo admin_base_url.'subscription.php';?>",
      method:"POST",
      data:{id:id,type:"editPlan"},
      success:function(data){
        $("#form_data").html(data);
      }
    });
}); 

</script>
</html>