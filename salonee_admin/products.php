<?php

require_once dirname(__DIR__) . '/controller/AdminProducts.php';
$sales = new AdminProducts();
$sales->init();
$persons=array();
$page_data=$sales->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$products=$page_data['products'];
include "includes/header.php";
?>
            <!-- BEGIN sidebar -->
            <?php include 'includes/sidebar.php';?>
            <!--  //END sidebar -->
<style>
table.table.themeTable td, table.table.themeTable th {
  max-width:none;
}
</style>
            <!-- BEGIN main -->
                <div class="main__content">
                    <!-- <div class="w-50">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
                            <div class="input-group-append">
                                <button class="btn theme-btn" type="submit">Go</button>
                            </div>
                        </div>
                    </div> -->
                 
                    <div class="pad_3">
                    <div class="filtrBtns">
                    <a class="btn login-btn btn-sm" style="width:170px;color:white" href="<?php echo admin_base_url.'products.php';?>" id="">All Products  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    <a class="btn login-btn btn-sm" style="width:170px;color:white"  href="<?php echo admin_base_url.'products.php?pro=active';?>"  style="width:100px;color:white"  id="">Active  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'products.php?pro=inactive';?>" style="width:100px;color:white"  id="">In Active &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'products.php?pro=blocked';?>" style="width:100px;color:white"  id="">Block &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    </div>
                        <div class="table-responsive">
                            <table class="table theme_Table dataTable" id="example">
                                <thead>
                                    <tr>
                                      <th>S.No</th>
                                      <th>Product Title</th>
                                      <th>Image</th>
                                      <th>Service Provider</th>
                                      <th>Base Price</th>
                                      <th>Sale Price</th>
                                      <th>Date</th>
                                      <th>Product Status</th>
                                      <th>Total Sold</th>
                                      <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody >
                                <?php 
                $i=1;
                foreach($products as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['product_title'].'</td>';
                  echo '<td><img src="'.$prf['product_img'].'" width="100px" /></td>';
                  echo '<td><a href="'.admin_base_url.'view-service-provider.php?id='.$prf['service_provider_id'].'">'.$prf['name'].'</a></td>';
                  echo '<td><strong>AED '.$prf['base_price'].'</strong></td>';
                  echo '<td><strong>AED '.date("Y-m-d",strtotime($prf['created_time']   )).'</strong></td>';
                  echo '<td>'.$prf['product_title'].'</td>';
                  echo '<td>'; 
                  switch($prf['product_status']){
                    case 'inactive':
                      echo '<label class="badge badge-pill badge-warning">Inactive</label>';
                      break;
                    case 'active':
                      echo '<label class="badge badge-pill badge-success">Active</label>';
                      break;
                    case 'block':
                      echo '<label class="badge badge-pill badge-danger">Blocked</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">In Active</label>';
                    break;
                  }
                  echo '</td>';
                  echo '<td>'.$prf['total_sold'].'</td>';
                  echo '<td>';
                  if($prf['product_status']=='active' ){
                    echo '<a  href="'.admin_base_url.'products.php?type=blockProduct&id='.$prf['id'].'" data-oid="'.$prf['id'].'" class="  badge badge-danger"> <i class="fa fa-check"></i> Block </a>';
                  }elseif($prf['product_status']=='block' || $prf['product_status']=='inactive'){
                    echo '<a  href="'.admin_base_url.'products.php?type=activeProduct&id='.$prf['id'].'" data-oid="'.$prf['id'].'" class="  badge badge-success"> <i class="fa fa-check"></i> Active </a>';
                  }
                  echo '</td>';
                 
                  echo '</tr>';
                } ?>   
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <!-- pagination  -->
                        <div class='pagination-container tablePaging'>
                            <div class="pagination">
                                <ol id="numbers"></ol>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>

        <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Add Sales Person</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="login">
                    <form action="salesperson.php" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="type" value="addSalesPerson" />
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <input type="text" id="title" class="form-control" name="name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="title">Name</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <select class="form-control" autocomplete="off" name="location" required>
                                <option value="0"> Select Location</option>
                                <?php $cities=array(); foreach($cities as $cit){ 
                                  echo '<option value="'.$cit['id'].'">'.$cit['city_name_en'].'</option>';
                                }?>
                            </select>
                            <label class="form-control-placeholder p-0" for="title">Location</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="code">Email</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="mobile" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="value">Mobile</label>
                                </div>
                            </div>
                        </div>
                      <div class="form-group">
                        <label for="file-upload" class="custom-file-upload sadmnUpload">
                          UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                        </label>
                        <input id="file-upload" name='image' type="file"  style="display:none;">
                      </div>

                      <div class="form-group">
                        <button type="submit" class="btn theme-btn">Submit</button>
                      </div>
                    </form>
                  </div>

                </div>

              </div>
            </div>
          </div>
          
    <!-- order products partial -->
    <div class="modal fade pswdModal" id="orderedProducts" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Ordered Products</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <input type="hidden" name="type" value="updateOrderStatus" />
              <input type="hidden" name="oid" id="oid" value="" />
                <div class="row mb-5">
<div class="col-md-12">

<table class="product_cls" id="product_cls">
<tr><th>Product Name</th><th>Image</th><th>Price</th><th>QTY</th></tr>
<tr><td>Product Name</td><td>Image</td><td>Price</td><td>QTY</td></tr>
</table>
</div>
                </div>


                <div class="form-group">
                </div>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->

    
        <?php include 'includes/footer.php';?>

<script>
  $(document).ready(function() {
      $('#example').dataTable({
        "sPaginationType": "full_numbers",
        "language": { 
              "zeroRecords": "No Records found",           
          },
          aLengthMenu: [
          [10,25, 50, 100, 200, -1],
          [10,25, 50, 100, 200, "All"]
      ],
      // iDisplayLength: -1
      });
  });

</script>


    </body>
</html>