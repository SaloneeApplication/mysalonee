<?php
require_once dirname(__DIR__) . '/controller/AdminIndex.php';
$controller = new AdminIndex();
$controller->init();
if(isset($_SESSION['message']))
{
    $message = $_SESSION['message'];    
}
if(isset($_SESSION['success']))
{
    $success = $_SESSION['success'];    
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Salonee Admin | Sign in </title>
    <link rel="stylesheet" href="./assets/bootstrap-4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/style.css">
    <link rel="stylesheet" href="./assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- fonts  -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
    
	<link rel = "stylesheet" href = "./assets/css/flash.min.css">
		<style>
	    .image-container{
        background: url('<?php echo user_base_url;?>assets/img/admin/bg1.jpg')no-repeat;
    background-size: cover;
    background-position: center;
    min-height: 100vh;
}
.login form {
    min-width: 310px;
    max-width: 70%;
    margin: 0 auto;
}
.form-container {
    display: flex;
    justify-content: center;
}

.form-box{
    display: flex;
    flex-direction: column;
    justify-content: center;
    min-height: 100vh;
}

.form-box h4{
    font-weight: bold;
    color: #fff;
}
	    
	</style>
</head>
<body>
    
    
      
    <section class="login">
<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-6 d-none d-md-block image-container"></div>

			<div class="col-lg-6 col-md-6 form-container">
				<div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
					<div class="logo mb-3">
                        <a href="<?php echo user_base_url;?>">
                    <img src="<?php echo user_base_url;?>assets/img/logo2.png" width="200px" alt="Salonee" class="logo"><a>
					</div>
					<div class="heading mb-4">
						<h4>Login into your account</h4>
					</div>
                  <form method = "post" action = "validateLogin.php">
                    <div class="form-group">
                        <input type="text" id="email" name="email" class="form-control" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="name">Email</label>
                    </div>

                    <div class="form-group mb_1">
                        <input type="password" id="password-field" name="password" class="form-control" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="password-field">Password</label>
                        <span toggle="#password-field" class="fa fa-eye field-icon toggle-password"></span>
                    </div>
                    <p id="forgot_pswd"><a href="#" data-toggle="modal" data-target="#forgotPassword">Forgot Password ?</a></p>
                    <div class="form-group">
                        <button class="btn login-btn" id="myButton">Login</button>
                    </div>

                   
                </form>
				</div>
			</div>
		</div>
	</div>
</div>
</section>


    
    

    <!-- forgot password modal  -->
    <div class="modal fade pswdModal" id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="forgotPasswordTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="col-12 modal-title text-center" id="forgotPasswordLongTitle">Forgot Password</h5>
                    <button type="button" class="close forgot_close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
                </div>
                <div class="modal-body">
                    <div class="login">
                        <form action="" method="POST">
                        <input type="hidden" name="type" value="forgotpassword" />
                            <div class="form-group mb-5 mt-4">
                                <input type="email" id="forgot_email" name="email" class="form-control" autocomplete="off" required>
                                <label class="form-control-placeholder p-0" for="email">Enter your Mail id</label>
                            </div>

                            <div class="form-group">
                                <button class="btn theme-btn">Submit</button>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>
    </div>
   
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="./assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>
    <script src = "./assets/js/flash.min.js"></script>
    <!-- <script src="./assets/js/main.js"></script> -->

    <script>
        $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        
   
    </script>
    <script>
   iziToast.settings({
      timeout: 3000, // default timeout
      resetOnHover: true,
      // icon: '', // icon class
      transitionIn: 'flipInX',
      transitionOut: 'flipOutX',
      position: 'topRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
      onOpen: function () {
        console.log('callback abriu!');
      },
      onClose: function () {
        console.log("callback fechou!");
      }
    });

    <?php 
        if(isset($_SESSION['message']))
        {
    ?>
            iziToast.error({title: 'Error', message: '<?php echo $message;?>'});
    <?php
        unset($_SESSION['message']);
        }
    ?>
    <?php 
        if(isset($_SESSION['success']))
        {
    ?>
            iziToast.success({title: 'Success', message: '<?php echo $success;?>'});
    <?php
        unset($_SESSION['success']);
        }
    ?>
    
	$(".forgot_close").click(function(){
        $("#forgot_email").val('');
    });
</script>
</body>

</html>