<?php

require_once dirname(__DIR__) . '/controller/AdminUserReports.php';
$sales = new AdminUserReports();
$sales->init();
$persons=array();
$page_data=$sales->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$report=$page_data['report'];
include "includes/header.php";
?>
            <!-- BEGIN sidebar -->
            <?php include 'includes/sidebar.php';?>
            <!--  //END sidebar -->
<style>
table.table.themeTable td, table.table.themeTable th {
  max-width:none;
}
</style>
            <!-- BEGIN main -->
                <div class="main__content">
                    <div class="pad_3">
                    <div id="search_pattern" class="filtrBtns" <?php echo (@$_GET['report']!='custom_date')?"style='display:block;'":"style='display:none;'";?>> 
                        <a class="btn login-btn btn-sm" style="width:170px;color:white" href="<?php echo admin_base_url.'user-reports.php';?>" id="">Daily  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        <a class="btn login-btn btn-sm" style="width:170px;color:white"  href="<?php echo admin_base_url.'user-reports.php?report=weekly';?>"  style="width:100px;color:white"  id="">Weekly  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'user-reports.php?report=monthly';?>" style="width:100px;color:white"  id="">Monthly&nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        <a class="btn login-btn btn-sm"  style="width:190px;color:white"  href="<?php echo admin_base_url.'user-reports.php?report=yearly';?>" style="width:100px;color:white"  id="">Yearly&nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                        <a class="btn login-btn btn-sm" id="custom_date" style="width:190px;color:white"  href="javascript: return false;" style="width:100px;color:white"  id="">Custom Date&nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    </div>
                    <div id="date_range"  <?php echo (@$_GET['report']=='custom_date')?"style='display:block;'":"style='display:none;'";?>>
                        <form action="" method="get" >
                        <input type="hidden" name="report" value="custom_date" />
                            <div class="row">
                                <div class="form-group col-sm-3" style="width:250px;">
                                    <label>From Date</label>
                                    <input type="date" name="from" value="<?php echo @$_GET['from'];?>" class="form-control" id="from_date"/>
                                </div>
                                <div class="form-group col-sm-3" style="width:250px;">
                                    <label>To Date</label>
                                    <input type="date" name="to" value="<?php echo @$_GET['to'];?>"  class="form-control" id="from_date"/>
                                </div>
                                <div class="form-group col-sm-6" style="">
                                    <button type="submit" class="btn login-btn btn-sm" style="width:120px"><i class="fa fa-search"></i> Search</button>
                                    <button type="button" id="back_to_date" class="btn login-btn btn-sm" style="width:100px"><i class="fa fa-arrow-left"></i> Back</button>
                                </div>
                            </div>
                        </form>
                    </div>

                        <div class="table-responsive">
                        <button id="btnExport" class="btn login-btn btn-sm" style="width:100px; margin: 0.5em 0em" onclick="fnExcelReport();"><i class="fa fa-download"></i> EXPORT </button>

<table id="reports_table" class="table themeTable " >
         <thead>
                                    <tr>
                                      <th>S.No</th>
                                      <th>User</th>
                                      <th>No.Bookings</th>
                                      <th>No.Cancelled</th>
                                      <th>Spend Amount</th>
                                      <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody id="saloonTable">
                                <?php 
                $i=1; 
                foreach($report as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['name'].' ( '.$prf['user_id'].' )</td>';
                  echo '<td>'.$prf['total_bookings'].'</td>';
                  echo '<td>'.$prf['total_cancelled'].'</td>';
                  echo '<td>AED '.$prf['total_spent'].'</td>';
                  echo '<td>'.$prf['date'].'</td>';
                  echo '</tr>';
                } ?>   
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <!-- pagination  -->
                        <div class='pagination-container tablePaging'>
                            <div class="pagination">
                                <ol id="numbers"></ol>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>

        <div class="modal fade pswdModal" id="viewTicket" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Ticket Info</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="login">
                    <div class="col-md-10 offset-2 ticket_description">
                        <div class="form-group">
                            <label>Issue Title</label>
                            <p>asdad</p>
                        </div>
                        <div class="form-group">
                            <label>Issue Description</label>
                            <p>asdad</p>
                        </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>

    <!-- order products partial -->
    <div class="modal fade pswdModal" id="replayTicket" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Reply</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          <form action="" method="POST" >
            <div class="login">
              <input type="hidden" name="type" value="replyTicket" />
              <input type="hidden" name="tid" id="tid_i" value="" />
              <div class="form-group">
                    <textarea class="form-control" name="reply" placeholder="" required></textarea>
                  <label class="form-control-placeholder p-0" for="refund_amount">Reply to Ticket</label>
                </div>
                <div class="form-group">
                <button type="submit" class="btn theme-btn">Submit</button>

                </div>
            </div>
            </form>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->

    
    <!-- partial -->
    <div class="modal fade pswdModal" id="refundPOP" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Refund</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="refundOrderID" />
              <input type="hidden" name="id" id="orderid" value="" />
                <div class="form-group">
                  <input type="text" name="refund_amount" class="form-control" id="refund_amount" required/>
                  <label class="form-control-placeholder p-0" for="refund_amount">Refund Amount</label>
                </div>
                <div class="form-group">
                  <input type="text" name="refund_deductions" class="form-control" value="0" id="refund_deductions" required/>
                  <label class="form-control-placeholder p-0" for="refund_deductions">Refund Deductions</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Refund Now</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- end popup -->
        <?php include 'includes/footer.php';?>
    </body>
    <script>
        $(".replay_now").click(function(){
          var tid=$(this).data("tid");
          $("#tid_i").val(tid)
          $("#replayTicket").modal('show');
        })
$(".view_ticket").click(function(){
  var tid=$(this).data("tid");
  $.ajax({
    url:"<?php echo admin_base_url.'support-tickets.php';?>",
    method:"POST",
    data:{tid:tid,type:"getTicketInfo"},
    success:function(data){
        $(".ticket_description").html(data);
        $("#viewTicket").modal('show');
    }
});
});

$("#custom_date").click(function(){
    $("#date_range").show();
    $("#search_pattern").hide();
});

$("#back_to_date").click(function(){
    $("#date_range").hide();
    $("#search_pattern").show();
});

function fnExcelReport()
{
    var tab_text="<table border='2px'><tr >";
    var textRange; var j=0;
    tab = document.getElementById('reports_table'); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    return (sa);
}
    </script>
</html>