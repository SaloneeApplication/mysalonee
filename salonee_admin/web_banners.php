<?php
require_once dirname(__DIR__) . '/controller/AdminWebBanners.php';
$controller = new AdminWebBanners();
$controller->load();
$banners = $controller->getBanners();
$page_data=$controller->pageData();
$title = $page_data['title']; $page = $page_data['page'];
$adminId=$page_data['admin_id'];
include "includes/header.php";
include('includes/sidebar.php');

?>

<div class="main__content">
    <div class="w-50">
        <h5>Banners - Website</h5><br>
    </div>
    <div class="clearfix"></div>
        <h2 class="card-title">Banners (Add Maximum Of 3 Banners)</h2>
        <div class="rightBtn">
            <button class="btn login-btn" data-toggle="modal" data-target="#add_slide_modal" id="myButton">Add &nbsp;<i
                class="fa fa-plus" aria-hidden="true"></i></button>
        </div>
        <div class="pad_3">
            <div class="table-responsive">
                <table class="table themeTable " >
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Image</th>                                
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $sn=1;
                        if(count($banners)> 0)
                        {
                            foreach($banners as $row)
                            {?>
                                <tr>
                                    <td><?php echo $sn++; ?></td>
                                    <td><img  width = "250px" src="<?php echo $row['image']; ?>"></td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <a href="javascript:;" data-toggle="modal" data-target="#edit_slide_modal<?php echo $row['id'];?>" class="edit_slide"><i class="fa fa-edit"></i></a>
                                            <a href="web_banners.php?type=delete_banner&banner_id=<?php echo $row['id'];?>" onclick="return confirm('Are you sure you want to Delete?')"><i class="fa fa-trash"></i></a>
                                        </div>
                                        <div class="modal fade text-left" id="edit_slide_modal<?php echo $row['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel18" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myModalLabel18">Edit Slide</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>            
                                                    <div class="modal-body">
                                                        <form method="post" action="web_banners.php" enctype="multipart/form-data">
                                                            <div class="form-body">
                                                                <div class="text-center">
                                                                    <img id="slide_image" src="<?php echo $row['image']; ?>" width="100px" src="" class="img-fluid mb-1">
                                                                </div>
                                                                <input type="hidden" name="type" value="editBanner" />
                                                                <input type="hidden" name="banner_id" value="<?php echo $row['id'];?>">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="image">Image (1920 * 985 pixels for uniformity and best quality)<span class="danger">*</span></label>
                                                                            <input type="file" id="image" class="form-control" placeholder="Image" name="image">
                                                                        </div>
                                                                    </div>
                                                                   
                                                                </div>
                                                            </div>
                                                            <div class="form-actions text-right">
                                                                <button type="submit" class="btn theme-btn"><i class="la la-check-square-o"></i> Save </button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                        else
                        {?>
                            <tr><td class="text-center" colspan="5">No Data</td></tr>
                        <?php
                        }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php include 'includes/footer.php';?>
<div class="modal fade text-left" id="add_slide_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel17"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>            
            <div class="modal-body">
                <form name="slide_form" id="slide_form" method="post" action="web_banners.php" enctype="multipart/form-data">
                    <div class="form-body">
                        <input type="hidden" name="type" value="addBanner" />
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="image">Image (1920 * 985 pixels for uniformity and best quality)<span class="danger">*</span></label>
                                    <input type="file" id="image" class="form-control" placeholder="Image" name="image" required="">
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="form-actions text-right">
                        <button type="submit" class="btn theme-btn"><i class="la la-check-square-o"></i> Save </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>