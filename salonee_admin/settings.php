<?php
require_once dirname(__DIR__) . '/controller/AdminPortal.php';
$controller = new AdminPortal();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
include "includes/header.php";
?>
      <!-- BEGIN sidebar -->
      <?php include('includes/sidebar.php');?>
      <!--  //END sidebar -->

      <!-- BEGIN main -->

        <div class="main__content">
         
 

          <div class="pad_3">
          <div class="filtrBtns">
                <a class="btn login-btn btn-sm" style="width:220px;color:white" href="<?php echo admin_base_url.'settings.php';?>" id="">Common Settings &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                <a class="btn login-btn btn-sm" style="width:170px;color:white"  href="<?php echo admin_base_url.'settings.php?set=pricings';?>"  style="width:100px;color:white"  id="">Pricings  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                <a class="btn login-btn btn-sm" style="width:170px;color:white"  href="<?php echo admin_base_url.'settings.php?set=smtp';?>"  style="width:100px;color:white"  id="">SMTP  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
            </div>      

                <!-- Common Settings  -->
                <?php 
                    if(@$_GET['set']=='pricings'){
                        ?>
    
<form action="" method="POST">
    <input type="hidden" name="type" value="pricing" />
        <div class="row">
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="featured_profile_plan_per_day">Featured Profile plan per day  (AED)</label>
                        <input type="text" id="featured_profile_plan_per_day" name="featured_profile_plan_per_day" class="form-control" value="<?php echo $controller->getOption("featured_profile_plan_per_day");?>" autocomplete="off" required>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="featured_profiles_admin_commission_per_day">Featured Profile Per day Admin Commission   (AED)</label>
                        <input type="text" id="featured_profiles_admin_commission_per_day" name="featured_profiles_admin_commission_per_day" class="form-control"value="<?php echo $controller->getOption("featured_profiles_admin_commission_per_day");?>"  autocomplete="off" required>
                </div>  
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="advertisement_per_day">Advertisement Per day (AED)</label>
                        <input type="text" id="advertisement_per_day" name="advertisement_per_day" class="form-control"value="<?php echo $controller->getOption("advertisement_per_day");?>"  autocomplete="off" required>
                </div>  
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="advertisement_per_day_admin_commission">Advertisement Per day Admin Commission(AED)</label>
                        <input type="text" id="advertisement_per_day_admin_commission" name="advertisement_per_day_admin_commission" class="form-control"value="<?php echo $controller->getOption("advertisement_per_day_admin_commission");?>"  autocomplete="off" required>
                </div>  
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="cancellation_slot_charges">Cancellation Slot Charges (AED)</label>
                        <input type="text" id="cancellation_slot_charges" name="cancellation_slot_charges" class="form-control"value="<?php echo $controller->getOption("cancellation_slot_charges");?>"  autocomplete="off" required>
                </div>  
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="cancellation_product_charges">Cancellation Product Charges (AED)</label>
                        <input type="text" id="cancellation_product_charges" name="cancellation_product_charges" class="form-control"value="<?php echo $controller->getOption("cancellation_product_charges");?>"  autocomplete="off" required>
                </div>  
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 offset-3">
            <center> <button class="btn login-btn btn-sm" type="submit" style="width:170px;color:white" style="width:100px;color:white"  id="">Save  &nbsp;<i class="fa fa-save" aria-hidden="true"></i></button></center>
 
            </div>
        </div>
      
</form>

                        <?php
                    }elseif(@$_GET['set']=='smtp'){

                        ?>

<form action="" method="POST">
    <input type="hidden" name="type" value="smtp" />
        <div class="row"> 
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="smtp_host">SMTP Host</label>
                        <input type="text" id="smtp_host" name="smtp_host" class="form-control" value="<?php echo $controller->getOption("smtp_host");?>" autocomplete="off" required>
                </div>  
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="smtp_email">SMTP Email</label>
                        <input type="text" id="smtp_email" name="smtp_email" class="form-control" value="<?php echo $controller->getOption("smtp_email");?>"  autocomplete="off" required>
                </div>  
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="smtp_email_password">SMTP Password</label>
                        <input type="password" id="smtp_email_password" name="smtp_email_password" class="form-control" value="<?php echo $controller->getOption("smtp_email_password");?>"  autocomplete="off" required>
                </div>  
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="smtp_port">SMTP Port</label>
                        <input type="text" id="smtp_port" name="smtp_port" class="form-control" value="<?php echo $controller->getOption("smtp_port");?>"  autocomplete="off" required>
                </div>  
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="smtp_status">SMTP Status</label>
                        <select name="smtp_status" class="form-control">
                            <option value="enable" <?php echo ($controller->getOption("smtp_status")=="enable")?"selected":"";?>>Enable</option>
                            <option value="disable" <?php echo ($controller->getOption("smtp_status")=="disable")?"selected":"";?>>Disable</option>
                        </select>
                </div>  
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 offset-3">
            <center> <button class="btn login-btn btn-sm" type="submit" style="width:170px;color:white" style="width:100px;color:white"  id="">Save  &nbsp;<i class="fa fa-save" aria-hidden="true"></i></button></center>
 
            </div>
        </div>
        </form>

<form action="" method="POST">
    <input type="hidden" name="type" value="testEmail" />
        <div class="row">
            <div class="col-sm-4 offset-3">
                <div class="form-group">
                        <label class="form-control-placeholder p-0" for="test_email">Test Email</label>
                        <input type="email" id="test_email" name="test_email" class="form-control" value=""  autocomplete="off" required>
                </div>  
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 offset-3">
            <center> <button class="btn login-btn btn-sm" type="submit" style="width:170px;color:white" style="width:100px;color:white"  id="">Send  &nbsp;<i class="fa fa-paper-plane" aria-hidden="true"></i></button></center>
 
            </div>
        </div>
        </form>
      

                        <?php

                    }else{
                        ?>

                <form action="" method="POST">
                    <input type="hidden" name="type" value="common" />
                        <div class="row">
                            <div class="col-sm-4 offset-3">
                                <div class="form-group">
                                        <label class="form-control-placeholder p-0" for="slot_dispute_disable_before_days">Slot Dispute Disable Before Days </label>
                                        <input type="text" id="slot_dispute_disable_before_days" name="slot_dispute_disable_before_days" class="form-control" value="<?php echo $controller->getOption("slot_dispute_disable_before_days");?>" autocomplete="off" required>
                                </div>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 offset-3">
                                <div class="form-group">
                                        <label class="form-control-placeholder p-0" for="product_dispute_disable_before_days">Product Dispute Disable Before Days </label>
                                        <input type="text" id="product_dispute_disable_before_days" name="product_dispute_disable_before_days" class="form-control"value="<?php echo $controller->getOption("product_dispute_disable_before_days");?>"  autocomplete="off" required>
                                </div>  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 offset-3">
                            <center> <button class="btn login-btn btn-sm" type="submit" style="width:170px;color:white" style="width:100px;color:white"  id="">Save  &nbsp;<i class="fa fa-save" aria-hidden="true"></i></button></center>
                 
                            </div>
                        </div>
                      
                </form>
                        <?php
                    }
                ?>
                
              
            <div class="clearfix"></div>

            <!-- pagination  -->
            <div class='pagination-container tablePaging'>
              <div class="pagination">
                <ol id="numbers"></ol>
              </div>
            </div>


          </div>


        </div>
    </div>
    </main>
    <!--  //END main -->

    <!-- <div class="layout__container-after"></div> -->



  </div>


  <!-- partial -->
  <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Subscription Plan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="login">
            <form>
                <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <input type="text" id="adName" class="form-control" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="adName">Name</label>
                      </div>
                    </div>
                  </div>
                <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="text" id="duration" class="form-control" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="duration">Duration</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="text" id="price" class="form-control" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="price">Price</label>
                      </div>
                    </div>
                  </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <textarea type="text" id="adDesc" rows="5" class="form-control" autocomplete="off" ></textarea>
                    <label class="form-control-placeholder p-0" for="adDesc">Description</label>
                  </div>
                </div>
              </div>
             

              <div class="form-group">
                <button type="button" class="btn theme-btn">Submit</button>
              </div>
            </form>
          </div>

        </div>

      </div>
    </div>
  </div>
  <?php include 'includes/footer.php';?>


</body>

</html>