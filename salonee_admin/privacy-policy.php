<?php
require_once dirname(__DIR__) . '/controller/AdminPrivayPolicy.php';
require_once dirname(__DIR__) . '/controller/AdminCountries.php';
$controller = new AdminPrivayPolicy();
$controllerCountries = new AdminCountries();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$privacyArray = $controller->getPrivacy();
$Countries = $controllerCountries->getActiveCountries();
$editCountries = $controllerCountries->getActiveCountries();
include "includes/header.php";
?>

      <!-- BEGIN sidebar -->
      <?php include('includes/sidebar.php');?>
      <!--  //END sidebar -->

      <!-- BEGIN main -->
      <main>
        <div class="main__content">
          <div class="w-50">
            <div class="input-group mb-3">
              <input type="text" class="form-control _search" id="category_search" placeholder="Search">
              <div class="input-group-append">
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
          </div>
          <div class="rightBtn">
            <button class="btn login-btn" data-toggle="modal" data-target="#addPrivacy" id="myButton">Add &nbsp;<i
                class="fa fa-plus" aria-hidden="true"></i></button>
          </div>
          <br><br>
          <div class="clearfix"></div>
          <ul class="paginationTable salonee_category category-card">

            <?php while($row=$privacyArray->fetch()){ ?>
            <li class=" tableItem">
<!--               <span><img src="<?php echo admin_base_url.$row['country_flag'];?>" width="100" alt="category"></span> -->
              <p>
                <b>Country : <span><?php echo $row['country_name']; ?></span></b>
                <b> Language : <span><?php echo ucwords($row['language']); ?></span></b>               
              </p>
              <small><i class="fa fa-pencil-square-o edit_privacy" data-id="<?php echo $row['id'];?>" data-country-id="<?php echo $row['country_id'];?>"  data-language="<?php echo $row['language'];?>" data-terms_condition='<?php echo $row['terms_condition'];?>'  data-privacy_policy='<?php echo $row['privacy_policy'];?>'  data-faq='<?php echo $row['faq'];?>' aria-hidden="true"></i> 
              
              <?php if($row['status']==1){?>
              <i class="fa fa-eye-slash deactivateprivacy" style="color:red;" title="Disable" data-id="<?php echo $row['id'];?>" aria-hidden="true"> </i>
              <?php }else{ ?>
                <i class="fa fa-eye activateprivacy" style="color:green;" title="Enable" data-id="<?php echo $row['id'];?>" aria-hidden="true"> </i>
              <?php } ?>
                  
              </small>
            </li>
            <?php }?>
            
            
          </ul>
          <div class="pagination-container">
            <p class='paginacaoCursor beforePagination' id=""><i class="fa fa-chevron-left" aria-hidden="true"></i></p>
            <p class='paginacaoCursor afterPagination' id=""><i class="fa fa-chevron-right" aria-hidden="true"></i></p>
          </div>


        </div>
      </main>
      <!--  //END main -->

      <!-- <div class="layout__container-after"></div> -->

    </div>

    <!-- add category modal  -->
    <div class="modal fade pswdModal" id="addPrivacy" tabindex="-1" role="dialog" aria-labelledby="addPrivayPolicy"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="width:200%;">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addPrivayPolicy">Add Privacy Policy Settings</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="<?php echo admin_base_url;?>privacy-policy.php" method="post" id="privacy_im"  name="privacy_im" enctype="multipart/form-data">
              <input type="hidden" name="type" value="addNewPrivacy" />
              <div class="form-group mt-4 mb-5">
                <select name="country_id" id="country_id" class="form-control" autocomplete="off" required >
                  <option hidden disabled selected value> -- Select Country -- </option>
            <?php while($row=$Countries->fetch()){ ?>
                  <option value="<?php echo $row['id'];?>"><?php echo ucwords($row['country_name']);?></option>
                <?php } ?>
                </select>
              </div>
                <div class="form-group mt-4 mb-5">
                  <select name="language" id="language" class="form-control" autocomplete="off" required>
                    <option hidden disabled selected value> -- Select Language -- </option>
                    <option value="EN">English</option>
                    <option value="AR">Arabic</option>
                  </select>
                </div>
                <div class="form-group mt-4 mb-5" style="font-weight:700;">
                  <label for="tc">Terms & Conditions</label><br/>
                  <textarea cols="80" id="terms_condition" name="terms_condition" rows="10" class="form-control" data-sample-short required="required"></textarea>

                </div>
                <div class="form-group mt-4 mb-5" style="font-weight:700;">
                  <label for="city_name">Privacy Policy</label>
                  <textarea cols="80" id="privacy_policy" name="privacy_policy" rows="10" class="form-control" data-sample-short required="required"></textarea>

                </div>
                <div class="form-group mt-4 mb-5" style="font-weight:700;">
                  <label for="city_name">FAQ's</label>
                  <textarea cols="80" id="faq" name="faq" rows="10" class="form-control" data-sample-short required="required"></textarea>

                </div>
                <div class="form-group">
                  <input type="submit" id="privacySubmit" class="btn theme-btn citySubmit" value="Submit">
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>

    <!-- add category modal  -->
    <div class="modal fade pswdModal" id="edit_privacy" tabindex="-1" role="dialog" aria-labelledby="addCategoryTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="EditCityTitle">Edit Privacy Policy Settings</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="<?php echo admin_base_url;?>privacy-policy.php" method="post" enctype="multipart/form-data" name="privacy_im1">
              <input type="hidden" name="type" value="updatePrivacy" />
              <input type="hidden" name="privacy_id" id="privacy_id1" value="" />
              <div class="form-group mt-4 mb-5">
                <select name="country_id" id="country_id1" class="form-control" autocomplete="off" required >
                  <option hidden disabled selected value> -- Select Country -- </option>
            <?php while($row=$editCountries->fetch()){ ?>
                  <option value="<?php echo $row['id'];?>"><?php echo ucwords($row['country_name']);?></option>
                <?php } ?>
                </select>
              </div>
                <div class="form-group mt-4 mb-5">
                  <select name="language" id="language1" class="form-control" autocomplete="off" required>
                    <option hidden disabled selected value> -- Select Language -- </option>
                    <option value="EN">English</option>
                    <option value="AR">Arabic</option>
                  </select>
                </div>
                <div class="form-group mt-4 mb-5" style="font-weight:700;">
                  <label for="tc">Terms & Conditions</label><br/>
                  <textarea cols="80" id="terms_condition1" name="terms_condition" rows="10" class="form-control" data-sample-short required="required"></textarea>

                </div>
                <div class="form-group mt-4 mb-5" style="font-weight:700;">
                  <label for="city_name">Privacy Policy</label>
                  <textarea cols="80" id="privacy_policy1" name="privacy_policy" rows="10" class="form-control" data-sample-short required="required"></textarea>

                </div>
                <div class="form-group mt-4 mb-5" style="font-weight:700;">
                  <label for="city_name">FAQ's</label>
                  <textarea cols="80" id="faq1" name="faq" rows="10" class="form-control" data-sample-short required="required"></textarea>

                </div>
                <div class="form-group">
                  <input type="submit" id="privacyUpdate" class="btn theme-btn privacyUpdate" value="Submit">
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>


    <?php include 'includes/footer.php';?>
    <script src="https://cdn.ckeditor.com/4.16.1/standard-all/ckeditor.js"></script>
  <script>
    CKEDITOR.replace('terms_condition', {
      height: 150,
      width: 400,
    });
    CKEDITOR.replace('privacy_policy', {
      height: 150,
      width: 400,
    });
    CKEDITOR.replace('faq', {
      height: 150,
      width: 400,
    });
    CKEDITOR.replace('terms_condition1', {
      height: 150,
      width: 400,
    });
    CKEDITOR.replace('privacy_policy1', {
      height: 150,
      width: 400,
    });
    CKEDITOR.replace('faq1', {
      height: 150,
      width: 400,
    });
  </script>
    <script>
    $(".edit_privacy").click(function(){
      var privacy_id=$(this).data("id");
      var country_id=$(this).data("country-id");
      var language=$(this).data("language");
      var terms_condition=$(this).data("terms_condition");
      var privacy_policy=$(this).data("privacy_policy");
      var faq=$(this).data("faq");
      $("#privacy_id1").val(privacy_id);
      $("#country_id1").val(country_id);
      $("#language1").val(language);
      CKEDITOR.instances.terms_condition1.setData(terms_condition)
      CKEDITOR.instances.privacy_policy1.setData(privacy_policy)
      CKEDITOR.instances.faq1.setData(faq)
      $("#edit_privacy").modal('show');

    });
      $("form[name='privacy_im']").submit(function(e) {
        if(CKEDITOR.instances['terms_condition'].getData().replace(/<[^>]*>/gi, '').length == 0) {
          iziToast.error({
              title: 'Error',
              message: 'Please add terms and conditions',
          });
          e.preventDefault();
          return false;
        }
        if(CKEDITOR.instances['privacy_policy'].getData().replace(/<[^>]*>/gi, '').length == 0) {
          iziToast.error({
              title: 'Error',
              message: 'Please add privacy policy',
          });
          e.preventDefault();
          return false;
        }
        if(CKEDITOR.instances['faq'].getData().replace(/<[^>]*>/gi, '').length == 0) {
          iziToast.error({
              title: 'Error',
              message: 'Please add FAQ',
          });
          e.preventDefault();
          return false;
        }
      })
      $("form[name='privacy_im1']").submit(function(e) {
        if(CKEDITOR.instances['terms_condition1'].getData().replace(/<[^>]*>/gi, '').length == 0) {
          iziToast.error({
              title: 'Error',
              message: 'Please add terms and conditions',
          });
          e.preventDefault();
          return false;
        }
        if(CKEDITOR.instances['privacy_policy1'].getData().replace(/<[^>]*>/gi, '').length == 0) {
          iziToast.error({
              title: 'Error',
              message: 'Please add privacy policy',
          });
          e.preventDefault();
          return false;
        }
        if(CKEDITOR.instances['faq1'].getData().replace(/<[^>]*>/gi, '').length == 0) {
          iziToast.error({
              title: 'Error',
              message: 'Please add FAQ',
          });
          e.preventDefault();
          return false;
        }
      })      
      $('#file-upload1').change(function() {
        var i = $(this).prev('label').clone();
        var file = $('#file-upload1')[0].files[0].name;
        $(this).prev('label').text(file);
      });
       
  $(".deactivateprivacy").click(function () {
    swal({
      title: "Are you sure?",
      text: "You want to disable this privacy policy & FAQ",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, Deactivate it!",
    }).then(result => {
      if (result.dismiss === 'cancel') {
        swal.closeModal();
        return;
      }
      $.ajax({
        url:"<?php echo admin_base_url.'privacy-policy.php';?>",
        method:"POST",
        data:{type:'disableprivacy',privacy_id:$(this).data("id")},
        success:function(resp){
          var resp=JSON.parse(resp);
          if(resp.status==1){
           location.reload();
          }
        }
      })
      
    },
    );
  });
  $(".activateprivacy").click(function () {
    swal({
      title: "Are you sure?",
      text: "You want to enable this privacy policy & FAQ",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, Activate it!",
    }).then(result => {
      if (result.dismiss === 'cancel') {
        swal.closeModal();
        return;
      }
      $.ajax({
        url:"<?php echo admin_base_url.'privacy-policy.php';?>",
        method:"POST",
        data:{type:'enableprivacy',privacy_id:$(this).data("id")},
        success:function(resp){
          var resp=JSON.parse(resp);
          if(resp.status==1){
           location.reload();
          }
        }
      })
    },
    );
  });
    </script>
</body>
</html>