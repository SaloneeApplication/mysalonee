<?php
require_once dirname(__DIR__) . '/controller/AdminUsers.php';
$controller=new AdminUsers();
$controller->load();
$page_data=$controller->pageViewUserData();
$title = $page_data['title']; $page = $page_data['page'];
$adminId=$page_data['admin_id'];
$total_sb=$page_data['total_sb'];
$total_p=$page_data['total_p'];
$total_pen=$page_data['total_pen'];
// $on_hold=$page_data['on_hold'];
$pending_ref_orders=$page_data['pending_ref_orders'];
$done_ref_orders=$page_data['done_ref_orders'];
$slots_booked=$page_data['slots_booked'];
$products=$page_data['products'];
$uid=$_GET['id'];
$user=$controller->getMemberInfo("user",$uid);
include("includes/header.php"); 
if($_SESSION['auth_type']=='salesperson'){
    header("Location:sales-person.php");
    exit;
}
?>
<!-- BEGIN sidebar -->
<style>
.service_provlink:hover{
  cursor:pointer;
}
</style>
<?php include('includes/sidebar.php');?>
<!--  //END sidebar -->
        <div class="main__content">
         
        <div class="pad_3">  
           
           <div class="detailsView"> 
                 <div class="col6">
                   <label>Name</label>
                   <span><?php echo $user['name'];?></span>
                 </div>
                 <div class="col6">
                   <label>Gender</label>
                   <span><?php echo $user['gender'];?></span>
                 </div> 
               <div class="col6">
                 <label>Email</label>
                 <span><?php echo $user['email'];?></span>
               </div>
               <div class="col6">
                 <label>Phone No</label>
                 <span><?php echo $user['mobile'];?></span>
               </div> 
             <!-- <div class="col6">
               <label>Date of Birth</label>
               <span><?php echo ($user['dob']!='')?$user['dob']:"NA";?></span>
             </div> -->
             <div class="col6">
               <label>Area</label>
               <span><?php echo $user['area'];?></span>
             </div>
             <div class="col6">
               <label>Date Of Registered</label>
               <span><?php echo date("Y-m-d",strtotime($user['created_time']));?></span>
             </div>
             <div class="col6">
               <label>Account Status</label>
               <span><?php 
                switch($user['status']){
                  case 1:
                    echo '<label class="badge badge-success"><i class="fa fa-check"></i> Active</label>';
                    break;
                  case 2:
                    echo '<label class="badge badge-danger"><i class="fa fa-times"></i> Blocked</label>';
                    break;
                }
                ?></span>
             </div>
           </div>
           <div class="btnBlock">
             <button class="btn theme-btn btn-sm" onclick="window.location='<?php echo admin_base_url;?>view-user.php?id=<?php echo $uid;?>&type=send_password';">Send Password</button>
             <?php if($user['status']==1){
               echo '<button class="btn theme-btn" onclick="window.location=\''.admin_base_url.'view-user.php?id='.$uid.'&type=block_user\';"> Block</button>';
             }else{
              echo '<button class="btn theme-btn" onclick="window.location=\''.admin_base_url.'view-user.php?id='.$uid.'&type=active_user\';"> Activate</button>';
             }
             ?>
           </div>
           <div class="displayCards">
             <ul>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Total Slots Booked</p>
                   <h3><?php echo $total_sb;?></h3>
                 </div>
               </li>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Total Products Booked</p>
                   <h3><?php echo $total_p;?></h3>
                 </div>
               </li>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Pending Refunds</p>
                   <h3><?php echo $total_pen;?></h3>
                 </div>
               </li>
               <!-- <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>On Hold Wallet</p>
                   <h3>AED <?php echo $on_hold;?></h3>
                 </div>
               </li> -->
            </ul>
            </div>
            <strong>

<h3>Pending Refunds</h3></strong>
<br/>
            <div class="table-responsive">
              <table class="" id="example">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Order ID</th>
                    <th>Order Type</th>
                    <th>Order Amount</th>
                    <th>Commissions</th>
                    <th>Date of Order</th>
                    <!-- <th>Order Status</th> -->
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody id="saloonTable">
                <?php 
                $i=1;
                foreach($pending_ref_orders as $prf){

                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['order_id'].'</td>';
                  echo '<td>'.ucwords($prf['order_type']).'</td>';
                  echo '<td>AED '.$prf['total_amount'].'</td>';
                  echo '<td>
                            Admin Commission AED '.$prf['admin_commission'].'<br/>
                            Txn Charges AED '.$prf['transaction_charges'].'<br/>
                         </td>';
                  echo '<td>'.$prf['date_of_order'].'</td>';

                  // echo '<td> <label class="badge badge-pill badge-danger">Refund Pending</label>';
                  // echo '</td>';
                  echo '<td> <a  href="javascipt:;" data-oid="'.$prf['order_id'].'" class=" refund_now badge badge-warning"> <i class="fa fa-check"></i> Refund </a></td>';
                         
                  echo '</tr>';
                } ?>    
                </tbody>
              </table>
            </div>

              <br/>
              <br/>
              <h3>Refunded Info</h3></strong>
<br/>
<div class="table-responsive">
           <table class="example" id="example3">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Order ID</th>
                    <th>Order Type</th>
                    <th>Order Amount</th>
                    <th>Commissions</th>
                    <th>Date of Order</th>
                    <th>Refunded Date</th>
                  </tr>
                </thead>
                <tbody id="saloonTable">
                <?php 
                $i=1;
                foreach($done_ref_orders as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['order_id'].'</td>';
                  echo '<td>'.ucwords($prf['order_type']).'</td>';
                  echo '<td>
                  Total <strong>AED '.$prf['total_amount'].'</strong><br/>
                  Refund <strong>AED '.$prf['refunded_amount'].'</strong><br/>
                  Refund Deductions <strong>AED '.$prf['refunded_deductions'].'</strong><br/>
                  </td>';
                  echo '<td>
                            Admin Commission AED '.$prf['admin_commission'].'<br/>
                            Txn Charges AED '.$prf['transaction_charges'].'<br/>
                         </td>';
                  echo '<td>'.$prf['date_of_order'].'</td>';
                  echo '<td>'.date("Y-m-d",strtotime($prf['modified_time'])).'</td>';

                  echo '</tr>';
                } ?>    
                </tbody>
              </table>
              </div>

              <br/>
              <br/>
<h3>Slots Booked </h3></strong>
<br/>
        <div class="table-responsive">
           <table class="example" id="example1">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Slot ID</th>
                    <th>Order ID</th>
                    <th>Service Provider</th>
                    <th>Service</th>
                    <th>Service At</th>
                    <th>Amount</th>
                    <th>Commissions</th>
                    <th>City</th>
                    <th>Status</th>
                    <th>Date of Slot</th>
                  </tr>
                </thead>
                <tbody id="saloonTable">
                <?php 
                $i=1;
                foreach($slots_booked as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['service_slot_id'].'</td>';
                  echo '<td>'.$prf['order_id'].'</td>';
                  echo '<td class="service_provlink" onclick="window.location.href=\''.admin_base_url.'view-service-provider.php?id='.$prf['service_provider_id'].'\';">'.$prf['name'].'</td>';
                  echo '<td>'.$prf['service_name'].'</td>';
                  echo '<td>'.$prf['service_type'].'</td>';
                  echo '<td><strong>AED '.$prf['total_amount'].'</strong></td>';
                  echo '<td> 
                  A.C <strong>AED '.$prf['admin_commission'].'</strong> <br/>
                  T.C <strong>AED '.$prf['transaction_charges'].'</strong> <br/>
                  </td>';
                  echo '<td>'.$prf['city_name'].'</td>';
                  echo '<td>'; 
                  switch($prf['service_status']){
                    case 'pending':
                    case 'inprocess':
                      echo '<label class="badge badge-pill badge-warning">Pending</label>';
                      break;
                    case 'completed':
                      echo '<label class="badge badge-pill badge-success">Completed</label>';
                      break;
                    case 'rescheduled':
                      echo '<label class="badge badge-pill badge-primary">Rescheduled</label>';
                      break;
                    case 'cancelled':
                      echo '<label class="badge badge-pill badge-danger">Cancelled</label>';
                      break;
                  }
                  echo '</td>';
                  echo '<td>'.date("Y-M-d H:i A",strtotime($prf['slot_date'])).'</td>';
                  echo '</tr>';
                } ?>   
                </tbody>

              </table>
              </div>
              <br/>
              <br/>
<h3>Products Ordered  </h3></strong>
<br/>
      <div class="table-responsive">
           <table class="example" id="example2">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Order ID</th>
                    <th>No.Products</th>
                    <th>Total Amount</th>
                    <th>Commissions</th>
                    <th>Service Provider</th>
                    <th>Order Status</th>
                    <th>Date</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody id="saloonTable">
                <?php 
                $i=1;
                foreach($products as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['order_id'].'</td>';
                  echo '<td>'.$prf['total_products'].'</td>';
                  echo '<td><strong>AED '.$prf['total_amount'].'</strong></td>';
                  echo '<td> 
                  A.C <strong>AED '.$prf['admin_commission'].'</strong> <br/>
                  T.C <strong>AED '.$prf['transaction_charges'].'</strong> <br/>
                  </td>';
                  echo '<td class="service_provlink" onclick="window.location.href=\''.admin_base_url.'view-service-provider.php?id='.$prf['service_provider_id'].'\';">'.$prf['service_provider_name'].'</td>';
                  echo '<td>'; 
                  switch($prf['shipping_status']){
                    case 'pending':
                    case 'inprocess':
                      echo '<label class="badge badge-pill badge-warning">In Process</label>';
                      break;
                    case 'delivered':
                      echo '<label class="badge badge-pill badge-success">Delivered</label>';
                      break;
                    case 'shipped':
                      echo '<label class="badge badge-pill badge-primary">Shipped</label>';
                      break;
                    case 'cancelled':
                      echo '<label class="badge badge-pill badge-danger">Cancelled</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">In Process</label>';
                    break;
                  }
                  echo '</td>';
                  echo '<td>'.date("Y-M-d H:i A",strtotime($prf['date_of_order'])).'</td>';
                  echo '<td><button class="badge badge-primary view_products" data-oid="'.$prf['order_id'].'" >View Products</button></td>';
                  echo '</tr>';
                } ?>   
                </tbody>
              </table>
              </div>
        </div>


      </main>
      <!--  //END main -->

      <!-- <div class="layout__container-after"></div> -->

    </div>

    <?php include 'includes/footer.php';?>

    <!-- partial -->
    <div class="modal fade pswdModal" id="refundPOP" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Refund  <br/><span style="color:red;font-size:13px;">Cancellation Charges : AED <?php echo $controller->getOption("cancellation_product_charges");?></span></h5>
           
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="refundOrderID" />
              <input type="hidden" name="id" id="orderid" value="" />
                <div class="form-group">
                  <input type="text" name="refund_amount" class="form-control" id="refund_amount" required/>
                  <label class="form-control-placeholder p-0" for="refund_amount">Refund Amount</label>
                </div>
                <div class="form-group">
                  <input type="text" name="refund_deductions" class="form-control" value="<?php echo $controller->getOption("cancellation_product_charges");?>" id="refund_deductions" required/>
                  <label class="form-control-placeholder p-0" for="refund_deductions">Refund Deductions</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Refund Now</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- end popup -->
    
    <!-- order products partial -->
    <div class="modal fade pswdModal" id="orderedProducts" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Ordered Products</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <input type="hidden" name="type" value="updateOrderStatus" />
              <input type="hidden" name="oid" id="oid" value="" />
                <div class="row mb-5">
<div class="col-md-12">

<table class="product_cls" id="product_cls">
<tr><th>Product Name</th><th>Image</th><th>Price</th><th>QTY</th></tr>
<tr><td>Product Name</td><td>Image</td><td>Price</td><td>QTY</td></tr>
</table>
</div>
                </div>


                <div class="form-group">
                </div>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->
</body>
<script>
$("#search_type").change(function(){
  var type=$(this).val();
  $(".search_by_other").remove();
  if(type=='by_email'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }else if(type=='by_phone'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Phone">');
  }else if(type=='by_customerid'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User ID">');
  }else{
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }
});
$("#example").DataTable({
  // searching:false
  "language": {
      "emptyTable": "No Pending Refunds right now"
    }
});

$("#example1").DataTable({
  // searching:false
  "language": {
      "emptyTable": "There is no slots booked till now."
    }
});
$("#example2").DataTable({
  // searching:false
  "language": {
      "emptyTable": "There is no products booked till now."
    }
});
$("#example3").DataTable({
  // searching:false
  "language": {
      "emptyTable": "There is no refunds data"
    }
});
$(".refund_now").click(function(){
  var oid=$(this).data("oid");
  $("#orderid").val(oid);
  $("#refundPOP").modal('show');
})
$(".view_products").click(function(){
  var oid=$(this).data("oid");
  $("#orderedProducts").modal('show');
  $.ajax({
    url:"<?php echo admin_base_url.'view-user.php';?>",
    method:"POST",
    data:{oid:oid,type:"getOrderedProducts"},
    success:function(data){
      $("#product_cls").html(data);
    }
  })
})
</script>
</html>