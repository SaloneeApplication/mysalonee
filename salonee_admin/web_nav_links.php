<?php
require_once dirname(__DIR__) . '/controller/AdminNavLinks.php';
$controller = new AdminNavLinks();
$controller->load();
$page_data=$controller->pageData();
$title = $page_data['title']; 
$page = $page_data['page'];
$sub_menu = 'nav_links';
$adminId=$page_data['admin_id'];
include "includes/header.php";
include('includes/sidebar.php');
?>

<div class="main__content">
    <div class="w-50">
        <h5>Navigation Links - Website</h5><br>
    </div>
    <div class="clearfix"></div>
    <div class="login" style="margin-left: -150px;">
        <form method="POST" action="<?php echo admin_base_url;?>web_nav_links.php">
            <div class="row">
            	<input type="hidden" name="type" value="addNavLinks">
                <div class="col-sm-4">
                    <div class="form-group">
                        <select id="language_id" name="language_id" class="form-control" required>
                        	<option selected value="English">English</option>
                        	<option value="Arabic">Arabic</option>
                        	<option value="Hindi">Hindi</option>
                        </select>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="locator" id="locator">
                                <label class="form-control-placeholder p-0" for="locator">Locator</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="login" id="login">
                                <label class="form-control-placeholder p-0" for="login">Login</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="register" id="register">
                                <label class="form-control-placeholder p-0" for="register">Register</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="join_us" id="join_us">
                                <label class="form-control-placeholder p-0" for="join_us">Join Us</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="my_account" id="my_account">
                                <label class="form-control-placeholder p-0" for="my_account">My Account</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="logout" id="logout">
                                <label class="form-control-placeholder p-0" for="logout">Logout</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="profile" id="profile">
                                <label class="form-control-placeholder p-0" for="profile">Profile</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="booking_history" id="booking_history">
                                <label class="form-control-placeholder p-0" for="booking_history">Booking History</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="schedule_booking" id="schedule_booking">
                                <label class="form-control-placeholder p-0" for="schedule_booking">Schedule Booking</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="change_password" id="change_password">
                                <label class="form-control-placeholder p-0" for="change_password">Change Password</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="notifications" id="notifications">
                                <label class="form-control-placeholder p-0" for="notifications">Notifications</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="discounts" id="discounts">
                                <label class="form-control-placeholder p-0" for="discounts">Discounts</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="about_us" id="about_us"/>
                                <label class="form-control-placeholder p-0" for="about_us">About Us</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="features" id="features">
                                <label class="form-control-placeholder p-0" for="features">Features</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" id="compare_prices" name="compare_prices" class="form-control" autocomplete="off">
                                <label class="form-control-placeholder p-0" for="compare_prices">Compare Prices</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="categories" id="categories">
                                <label class="form-control-placeholder p-0" for="categories">categories</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="blog" id="blog">
                                <label class="form-control-placeholder p-0" for="blog">blog</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="how_it_works" id="how_it_works">
                                <label class="form-control-placeholder p-0" for="how_it_works">How It Works</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" id="download" name="download" class="form-control" autocomplete="off">
                                <label class="form-control-placeholder p-0" for="download">Download</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="contact" id="contact">
                                <label class="form-control-placeholder p-0" for="contact">Contact</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="partner_with_salonee" id="partner_with_salonee">
                                <label class="form-control-placeholder p-0" for="partner_with_salonee">Partner With Salonee</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="privacy_policy" id="privacy_policy">
                                <label class="form-control-placeholder p-0" for="privacy_policy">Privacy Policy</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" autocomplete="off" name="terms_conditions" id="terms_conditions">
                                <label class="form-control-placeholder p-0" for="terms_conditions">Terms & Conditions</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" id="faq" name="faq" class="form-control" autocomplete="off">
                                <label class="form-control-placeholder p-0" for="faq">FAQ</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" style="width: 15%!important;" class="btn theme-btn">Submit</button>
            </div>
        </form>
    </div>
</div>
<?php include 'includes/footer.php';?>
<script type="text/javascript">

    $(document).ready(function(){

        var language_id = 'English';
        load_data(language_id);

        $("#language_id").change(function () {

            language_id = $(this).val();
            load_data(language_id);
        });
    });

	function load_data(language_id)
    {
      	$.ajax({
	        url:"<?php echo admin_base_url.'web_nav_links.php';?>",
	        method:"POST",
	        data:{type:'getNavLinks',language_id:language_id},
	        success:function(resp){
				var resp=JSON.parse(resp);
                var res = resp.data;
				$("#locator").val(res.locator);
                $("#login").val(res.login);
                $("#register").val(res.register);
                $("#join_us").val(res.join_us);
                $("#profile").val(res.profile);
                $("#my_account").val(res.my_account);
                $("#logout").val(res.logout);
                $("#booking_history").val(res.booking_history);
                $("#schedule_booking").val(res.schedule_booking);
                $("#change_password").val(res.change_password);
                $("#notifications").val(res.notifications);
                $("#discounts").val(res.discounts);
                $("#about_us").val(res.about_us);
                $("#features").val(res.features);
                $("#compare_prices").val(res.compare_prices);
                $("#categories").val(res.categories);
                $("#blog").val(res.blog);
                $("#how_it_works").val(res.how_it_works);
                $("#download").val(res.download);
                $("#contact").val(res.contact);
                $("#partner_with_salonee").val(res.partner_with_salonee);
                $("#privacy_policy").val(res.privacy_policy);
                $("#terms_conditions").val(res.terms_conditions);
                $("#faq").val(res.faq);
        	}
      	})
  	}
</script>