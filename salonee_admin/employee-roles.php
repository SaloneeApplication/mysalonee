<?php

require_once dirname(__DIR__) . '/controller/AdminEmployees.php';
$sales = new AdminEmployees();
$sales->load();
$cities=$sales->getCities();
$roles=$sales->getRoles();
$page_data=$sales->getEmployeeRolePage();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
include "includes/header.php";
$access_pages=['dashboard',
'users',
'service_provider',
'salespersons',
'employees',
'service_bookings',
'category',
'promo_codes',
'advertisement',
'featured_profiles',
'products',
'orders',
'product_orders',
'subscription',
'ratings',
'services_reports',
'service_provider_reports',
'user_reports',
'product_reports',
'revenue_reports',
'support_tickets',
'settings',
'countries',
];
?>
            <!-- BEGIN sidebar -->
            <?php include 'includes/sidebar.php';?>
            <!--  //END sidebar -->
<style>
table.table.themeTable td, table.table.themeTable th {
  max-width:none;
}
</style>
            <!-- BEGIN main -->
                <div class="main__content">
                    <div class="w-50">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
                            <div class="input-group-append">
                                <button class="btn theme-btn" type="submit">Go</button>
                            </div>
                        </div>
                    </div>
                    <div class="rightBtn">
                        <button   class="btn login-btn" data-toggle="modal" data-target="#addPromoCode" id="myButton">Add &nbsp;<i
                            class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                   
                    <div class="pad_3">
                        <div class="table-responsive">
                            <table class="table themeTable " >
                                <thead>
                                    <tr>
                                        <th width="1">S.No</th>
                                        <th width="5">Role Name</th>
                                        <th width="20">Permissions</th>
                                        <th width="20">Created At</th>
                                        <th width="20">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="saloonTable">
                                    <?php $i=1;
                                     foreach($roles as $per){
                                      echo '<tr>';
                                      echo '<td>'.$i.'</td>';
                                      echo '<td>'.$per['role_name'].'</td>';
                                      echo '<td>'.count(json_decode($per['role_access'])).'/22</td>';
                                      echo '<td>'.date("Y-m-d",strtotime($per['created_at'])).'</td>';
                                      echo '<td><a href="javascript: return;" data-id="'.$per['role_id'].'" data-role_name="'.$per['role_name'].'" class="badge badge-primary edit_role"><i class="fa fa-edit"></i> Edit</a>';
                                      echo '<input type="hidden" id="role_permissions_'.$per['role_id'].'"';
                                      
                                      echo "value='".$per['role_access']."'";
                                      echo ' />';
                                      echo '</td>';
                                      echo '</tr>';
                                      $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <!-- pagination  -->
                        <div class='pagination-container tablePaging'>
                            <div class="pagination">
                                <ol id="numbers"></ol>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>
        <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Add User Role</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="login">
                    <form action="" enctype="multipart/form-data" method="POST">
                    <input type="hidden" name="type" value="addSalesPerson" />
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <input type="hidden" id="type" class="form-control" name="type" value="addNewRole">
                            <input type="text" id="title" class="form-control" name="name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="title">Role Name</label>
                          </div>
                        </div>
                      </div>
                      <h5>Permissions</h5>
                      <div class="row">
                      <?php for($i=0;$i<count($access_pages);$i++){
                           ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><?php echo ucwords(str_replace("_"," ",$access_pages[$i]));?></label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Yes <input type="radio" value="yes" name="<?php echo $access_pages[$i];?>"/>
                                    <label>No <input type="radio" value="no" name="<?php echo $access_pages[$i];?>"  />
                                </div>
                            </div>
                        <?php }?>
                        </div>

                      <div class="form-group">
                        <button type="submit" class="btn theme-btn">Save</button>
                      </div>
                    </form>
                  </div>

                </div>

              </div>
            </div>
          </div>
          <!-- end popup -->
          <!-- start popup -->
          <div class="modal fade pswdModal" id="editRole" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Edit User Role</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="login">
                    <form action="" enctype="multipart/form-data" method="POST">
                    <input type="hidden" name="type" value="addSalesPerson" />
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <input type="hidden" id="type" class="form-control" name="type" value="updateRole">
                            <input type="hidden" id="role_id" class="form-control" name="role_id" value="">
                            <input type="text" id="role_name_edit" class="form-control" name="name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="title">Role Name</label>
                          </div>
                        </div>
                      </div>
                      <h5>Permissions</h5>
                      <div class="row" id="role_permissions">
                      <?php for($i=0;$i<count($access_pages);$i++){
                           ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label><?php echo ucwords(str_replace("_"," ",$access_pages[$i]));?></label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Yes <input type="radio" value="yes" id="edit_yes_<?php echo $access_pages[$i];?>" name="<?php echo $access_pages[$i];?>"/>
                                    <label>No <input type="radio" value="no" id="edit_no_<?php echo $access_pages[$i];?>"  name="<?php echo $access_pages[$i];?>"  />
                                </div>
                            </div>
                        <?php }?>
                        </div>

                      <div class="form-group">
                        <button type="submit" class="btn theme-btn">Save</button>
                      </div>
                    </form>
                  </div>

                </div>

              </div>
            </div>
          </div>
          
        <?php include 'includes/footer.php';?>
        <script>
          $(".edit_role").click(function(){
            var id=$(this).data("id");
            var role_name=$(this).data("role_name");
            if(role_name=='Super Admin'){
              swal({
              type: "error",
              text: 'Sorry. There is no access for super admin permissions change.',
              showConfirmButton: false,
              timer: 3000
            });
              return false;
            }
            $("#role_id").val(id);
            $("#role_name_edit").val(role_name);
            var permissions=$("#role_permissions_"+id).val();
            permissions=JSON.parse(permissions);
            $.each(permissions,function(index,value){
              $("#edit_yes_"+value).attr("checked","checked");
            });
            $("#editRole").modal('show');
          });
        </script>
    </body>
</html>