<?php
require_once dirname(__DIR__) . '/controller/Salesperson.php';
$controller = new Salesperson();
$controller->load();
$persons=$controller->getSalesPersons();
$page_data=$controller->pageViewSalespersonData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$active_sp=$page_data['active_sp'];
$total_sp=$page_data['total_sp'];
$pending_sp=$page_data['pending_sp'];
$rejected_sp=$page_data['rejected_sp'];

$uid=$_GET['id'];
if($uid==''){
    header("Location:salesperson.php");
    exit;
}
$serviceProviders =$controller->getServiceProvidersSalesperson($uid);
$user=$controller->getMemberInfo("salesperson",$uid);
include("includes/header.php"); 
if($_SESSION['auth_type']=='salesperson'){
    header("Location:sales-person.php");
    exit;
}
?>
<!-- BEGIN sidebar -->

<?php include('includes/sidebar.php');?>
<link href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css" rel="stylesheet" >
<style>
.service_provlink:hover{
  cursor:pointer;
}
button.dt-button{
    background-color:#d66292;
    color:white;
}
button.dt-button:hover{
    background-color:#d66292!important;
    color:white;
}
</style>
<!--  //END sidebar -->
        <div class="main__content">
         
        <div class="pad_3">  
        <br/>
        <center><?php if(is_file($user['image'])){echo ($user['image']!='')?'<img src="'.admin_base_url.$user['image'].'" width="100px" />':""; }?> <br/>
          </center> 
           <div class="detailsView"> 
                 <div class="col6">
                   <label>Name</label>
                   <span><?php echo $user['name'];?></span>
                 </div>
               <div class="col6">
                 <label>Email</label>
                 <span><?php echo $user['email'];?></span>
               </div>
               <div class="col6">
                 <label>Phone No</label>
                 <span><?php echo $user['mobile'];?></span>
               </div> 
             <div class="col6">
               <label>City</label>
               <span><?php echo $user['location_name'];?></span>
             </div>
             <div class="col6">
               <label>Date Of Registered</label>
               <span><?php echo date("Y-m-d",strtotime($user['created_time']));?></span>
             </div>
             <div class="col6">
               <label>Account Status</label>
               <span><?php 
                switch($user['status']){
                  case 1:
                    echo '<label class="badge badge-success"><i class="fa fa-check"></i> Active</label>';
                    break;
                  case 2:
                    echo '<label class="badge badge-danger"><i class="fa fa-times"></i> Blocked</label>';
                    break;
                  case 0:
                    echo '<label class="badge badge-danger"> Inactive</label>';
                    break;
                }
                ?></span>
             </div>
           </div>
           <div class="btnBlock">
             <button class="btn theme-btn btn-sm" onclick="window.location='<?php echo admin_base_url;?>view-salesperson.php?id=<?php echo $uid;?>&type=send_password';">Send Password</button>
             <?php if($user['status']==1){
               echo '<button class="btn theme-btn" onclick="window.location=\''.admin_base_url.'view-salesperson.php?id='.$uid.'&type=block_user1\';"> Block</button>';
             }else{
              echo '<button class="btn theme-btn" onclick="window.location=\''.admin_base_url.'view-salesperson.php?id='.$uid.'&type=active_user1\';"> Activate</button>';
             }
             ?>
           </div>
           <div class="displayCards">
             <ul>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Active Service Providers</p>
                   <h3><?php echo $active_sp;?></h3>
                 </div>
               </li>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Total Service Providers</p>
                   <h3><?php echo $total_sp;?></h3>
                 </div>
               </li>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Pending Service Providers</p>
                   <h3><?php echo $pending_sp;?></h3>
                 </div>
               </li>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Rejected Service Providers</p>
                   <h3><?php echo $rejected_sp;?></h3>
                 </div>
               </li>
             
            </ul>
            </div>
            <strong>

            <h3>Service Providers</h3></strong>
            <br/>
            <div class="table-responsive">
           <table class="" id="example">
                <thead>
                <tr>
                <th>S.No</th>
                <th>Name</th>
                <th>Business Name</th>
                <th>Email Id</th>
                <th>Phone No</th>
                <th>Licence</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
                </thead>
                <tbody id="saloonTable">
                <?php $i=1;
                                    while($row = $serviceProviders->fetch())
                                    {?>
                                        <tr>
                                            <td><?= $i++; ?></td>
                                            <td><?= $row['name']; ?></td>
                                            <td><?= $row['business_name']; ?></td>
                                            <td><?= $row['email']; ?></td>
                                            <td><?= $row['mobile']; ?></td>
                                            <td><a target="blank" href="<?php echo user_base_url; echo $row['business_licence'] ?>"><i class="fa fa-file fa-2x"></i></a></td>
                                            <td>
                                               <?php
                                               if($row['status']==1){
                                                    echo '<label class="badge badge-pill badge-success">Active</label>';
                                               }elseif($row['status']==0){
                                                echo '<label class="badge badge-pill badge-warning">Approval Pending</label>';
                                               }elseif($row['status']==3){
                                                echo '<label class="badge badge-pill badge-danger">Rejected</label>';
                                               }else{
                                                echo '<label class="badge badge-pill badge-danger">Blocked</label>';
                                               }

                                               ?>
                                            </td>
                                            <td>
                                                <?php
                                                
                                                if($row['status'] == 1)
                                                {
                                                    echo '<a href="'.admin_base_url.'service-provider.php?type=block&id='.$row['service_provider_id'].'" class="badge badge-danger"><i class="fa fa-times"></i> Block</a> ';

                                                }
                                                else if($row['status'] == 2)
                                                {
                                                    echo '<a href="'.admin_base_url.'service-provider.php?type=activate&id='.$row['service_provider_id'].'" class="badge badge-success"><i class="fa fa-times"></i> Activate</a> '; 
                                                }
                                                else if($row['status'] == 3)
                                                {
                                                   
                                                }
                                                else if($row['status'] == 0)
                                                {
                                                    echo '<a href="'.admin_base_url.'service-provider.php?type=approve&id='.$row['service_provider_id'].'" class="badge badge-success"><i class="fa fa-check"></i> Approve</a> ';
                                                    echo '<a href="javascript:;" class="badge badge-danger reject_sid" data-id="'.$row['service_provider_id'].'"><i class="fa fa-times"></i> Reject</a> ';
                                                }
                                                echo '<a href="'.admin_base_url.'view-service-provider.php?id='.$row['service_provider_id'].'" class="badge badge-primary"><i class="fa fa-eye"></i> View</a> '; 
                                                ?>
                                            </td>
                                        </tr>
                                    <?php
                                    }?>
                </tbody>
              </table>
                                  </div>                      


        </div>


      </main>
      <!--  //END main -->

      <!-- <div class="layout__container-after"></div> -->

    </div>

    <?php include 'includes/footer.php';?>

    <!-- partial -->
    <div class="modal fade pswdModal" id="refundPOP" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Refund</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="refundOrderID" />
              <input type="hidden" name="id" id="orderid" value="" />
                <div class="form-group">
                  <input type="text" name="refund_amount" class="form-control" id="refund_amount" required/>
                  <label class="form-control-placeholder p-0" for="refund_amount">Refund Amount</label>
                </div>
                <div class="form-group">
                  <input type="text" name="refund_deductions" class="form-control" value="0" id="refund_deductions" required/>
                  <label class="form-control-placeholder p-0" for="refund_deductions">Refund Deductions</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Refund Now</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- end popup -->
    
    <!-- order products partial -->
    <div class="modal fade pswdModal" id="orderedProducts" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Ordered Products</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <input type="hidden" name="type" value="updateOrderStatus" />
              <input type="hidden" name="oid" id="oid" value="" />
                <div class="row mb-5">
<div class="col-md-12">

<table class="product_cls" id="product_cls">
<tr><th>Product Name</th><th>Image</th><th>Price</th><th>QTY</th></tr>
<tr><td>Product Name</td><td>Image</td><td>Price</td><td>QTY</td></tr>
</table>
</div>
                </div>


                <div class="form-group">
                </div>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->
</body>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script>
$("#search_type").change(function(){
  var type=$(this).val();
  $(".search_by_other").remove();
  if(type=='by_email'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }else if(type=='by_phone'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Phone">');
  }else if(type=='by_customerid'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User ID">');
  }else{
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }
});
$("#example").DataTable({
  // searching:false
  "language": {
      "emptyTable": "There is no wallet transactions."
    },
    dom: 'Bfrtip',
    buttons: [
         'excel'
    ]
});

</script>
</html>