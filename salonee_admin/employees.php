<?php

require_once dirname(__DIR__) . '/controller/AdminEmployees.php';
$sales = new AdminEmployees();
$sales->load();
$roles=$sales->getRoles();
$emplyees=$sales->getEmployees();
$page_data=$sales->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
include "includes/header.php";
?>
<!-- BEGIN sidebar -->
<?php include 'includes/sidebar.php';?>
<!--  //END sidebar -->
<style>
table.table.themeTable td, table.table.themeTable th {
  max-width:none;
}
</style>
            <!-- BEGIN main -->
                <div class="main__content">
                    <div class="w-50">
                        <div class="input-group mb-3 _hide">
                            <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
                            <div class="input-group-append">
                                <button class="btn theme-btn" type="submit">Go</button>
                            </div>
                        </div>
                    </div>
                    <div class="rightBtn" style="margin-right:150px;float:left;width:50px;">
                        <button   class="btn login-btn" data-toggle="modal" data-target="#addPromoCode" id="myButton">Add &nbsp;<i
                            class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                    <div class="rightBtn">
                    <button onclick="window.location.href='<?php echo admin_base_url."employee-roles.php";?>';" class="btn login-btn" >User Roles &nbsp;<i
                            class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                   
                    <div class="pad_3">
                        <div class="table-responsive">
                            <table class="table theme_Table dataTable"  id="example">
                                <thead>
                                    <tr>
                                        <th width="1">S.No</th>
                                        <th width="5">Name</th>
                                        <th width="75" style="max-width:300px;">Email</th>
                                        <th width="20">Phone</th>
                                        <th width="20">User Role</th>
                                        <th width="20">Created At</th>
                                        <th width="20">Status</th>
                                        <th width="20">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; foreach($emplyees as $per){
                                      echo '<tr>';
                                      echo '<td>'.$i.'</td>';
                                      echo '<td>'.$per['name'].'</td>';
                                      echo '<td>'.$per['email'].'</td>';
                                      echo '<td>'.$per['mobile'].'</td>';
                                      echo '<td>'.$per['role_name'].'</td>';
                                      echo '<td>'.date("Y-m-d",strtotime($per['created_at'])).'</td>';
                                      if($per['status']=='1'){
                                        echo '<td><label class="badge  badge-pill  badge-success">Active</label></td>';
                                        echo '<td><a href="employees.php?type=block_user&id='.$per['admin_id'].'" class="badge badge-danger"><i class="fa fa-times"></i> Block</a>';

                                      }else{
                                        echo '<td><label class="badge  badge-pill  badge-danger">Blocked</label></td>';
                                        echo '<td><a href="employees.php?type=active_user&id='.$per['admin_id'].'" class="badge badge-success"><i class="fa fa-check"></i> Activate</a>';
                                      }
                                      echo '<a href="employees.php?type=send_password&id='.$per['admin_id'].'" class="badge badge-primary"><i class="fa fa-key"></i> Send Password</a>';
                                      echo '</td>';
                                      echo '</tr>';
                                      $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <!-- pagination  -->
                        <div class='pagination-container tablePaging'>
                            <div class="pagination">
                                <ol id="numbers"></ol>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>
        <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Add Employee</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="login">
                    <form action="" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="type" value="addAdminEmployee" />
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <input type="text" id="title" class="form-control" name="name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="title">Name</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <select class="form-control" autocomplete="off" name="role" required>
                                <option value="0"> Select Role</option>
                                <?php foreach($roles as $cit){ 
                                  if($cit['role_id']==1){
                                    continue;
                                  }
                                  echo '<option value="'.$cit['role_id'].'">'.$cit['role_name'].'</option>';
                                }?>
                            </select>
                            <label class="form-control-placeholder p-0" for="title">Role</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="code">Email</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="mobile" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="value">Mobile</label>
                                </div>
                            </div>
                        </div>
                      <div class="form-group">
                        <label for="file-upload" class="custom-file-upload sadmnUpload">
                          UPLOAD PIC <i class="fa fa-camera" aria-hidden="true"></i>
                        </label>
                        <input id="file-upload" name='image' type="file"  style="display:none;">
                      </div>

                      <div class="form-group">
                        <button type="submit" class="btn theme-btn">Submit</button>
                      </div>
                    </form>
                  </div>

                </div>

              </div>
            </div>
          </div>
          
        <?php include 'includes/footer.php';?>


<script>
  $(document).ready(function() {
    $('#example').dataTable({
      "sPaginationType": "full_numbers",
      "language": { 
            "zeroRecords": "No Employees found.",           
        },
      aLengthMenu: [
        [10,25, 50, 100, 200, -1],
        [10,25, 50, 100, 200, "All"]
    ],
    // iDisplayLength: -1
      });
});
</script>


    </body>
</html>