<?php
@session_start();
//Include Files
include "class/dbConnection.php";
include "class/admin.php";
//Retrieving Form Fields
$email = $_REQUEST["email"];
$password = $_REQUEST["password"];
//Creating dbObject for dbConnection
$dbObject = new dbConnection();
$con = $dbObject->getConnection();
if ($con) {
    //Creating admin Object
    $adminObj = new admin();
    $message = $adminObj->checkAdminDetails($email, $password, $con);
    if ($message == "correct") {
        $adminId = $adminObj->getAdminId($email, $con);
        if($adminId['status']!=1 && $adminId['admin_id']!=1){
            $_SESSION['message'] = 'Your account blocked. please contact us.';
            header("Location:index.php");
            exit;
        }
        $_SESSION["adminId"] = $adminId['admin_id'];
        $_SESSION["auth_type"] = 'admin';
        $_SESSION["user_role"] = $adminId['user_role'];
        $_SESSION["role_access"] = $adminId['role_access'];
        header('Location:dashboard.php');
    } else {
        $message = $adminObj->checkSubEmployee($email, $password, $con);
        if ($message == "correct") {
            $adminId = $adminObj->getEmployeeAdminId($email, $con);
            $status=$adminId['status'];
            if($status==1){
                $_SESSION["adminId"] = $adminId['salesperson_id'];
                $_SESSION["auth_type"] = 'salesperson';
                header('Location:sales-person.php');
            }else{
                $_SESSION['message'] = 'Your Profile has been blocked by Admin';
                header("Location:index.php");
            }
           
        } else {
            $_SESSION['message'] = $message;
            header("Location:index.php");
        }
    }
} else {
    echo mysqli_errno() . "<br/>" . mysqli_error();
}
