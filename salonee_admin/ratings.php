<?php

require_once dirname(__DIR__) . '/controller/AdminRatings.php';
$sales = new AdminRatings();
$sales->init();
$persons=array();
$page_data=$sales->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$ratings=$page_data['ratings'];
include "includes/header.php";
?>
            <!-- BEGIN sidebar -->
            <?php include 'includes/sidebar.php';?>
            <!--  //END sidebar -->
<style>
table.table.themeTable td, table.table.themeTable th {
  max-width:none;
}
</style>
            <!-- BEGIN main -->
                <div class="main__content">
                  <div class="col-md-12">
                  <a class="btn login-btn btn-sm" style="width:170px;color:white" href="<?php echo admin_base_url.'ratings.php';?>" id="">Active  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                  <a class="btn login-btn btn-sm" style="width:170px;color:white"  href="<?php echo admin_base_url.'ratings.php?pro=deleted';?>"  style="width:100px;color:white"  id="">Deleted  &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                </a>
                </div>
                <ul class="salonee_locations paginationTable _rating">

<?php foreach($ratings as $r){ ?>
          <li class="tableItem">             
            <div class="fourFive">
              <p><b>Service: <?php echo $r['service_name'];?></b></p>
                             
              <p><?php echo $r['comment']; ?></p>

            <div class="rating_footr">
              <div class="_stars">
                <b>Rating :</b>
                <small class="number-rating"><?php echo $r['rating'];?></small>
                  <div class="stars-outer">
                    <div class="stars-inner"></div>
                  </div> 
                </div> 
                <div>
                  <b>Reviewed in:</b> <b>
                    <?php
                        if(date("Y-m-d")==date("Y-m-d",strtotime($r['created_time']))){
                          echo 'Today';
                        }elseif(date("Y-m-d",strtotime("-1 day"))==date("Y-m-d",strtotime($r['created_time']))){
                          echo 'Yesterday';
                        }else{
                          $earlier = new DateTime(date("Y-m-d"));
                          $later = new DateTime(date("Y-m-d",strtotime($r['created_time'])));
                          
                          $diff = $later->diff($earlier)->format("%a");
                          echo $diff.' days';
                        } 
                    ?>  
                  </b>
                </div>
                <div>
                  <b>Comment By:</b> <b><?php echo ($r['user_name']!='')?$r['user_name']:"Anonymous";?></b> 
                    <?php if($r['status']==1){
                        ?>
<a href="<?php echo admin_base_url;?>ratings.php?type=deleteRating&id=<?php echo $r['id'];?>" class="badge  badge-danger" onclick="return confirm('are you sure want to delete ?'
                  );"><i style="color:white;" class="fa fa-trash"></i> Delete</a>
                        <?php 
                    }else{
                        ?>
<a href="<?php echo admin_base_url;?>ratings.php?type=restoreRating&id=<?php echo $r['id'];?>" class="badge  badge-primary" onclick="return confirm('are you sure want to restore rating ?'
                  );"><i style="color:white;" class="fa fa-undo"></i> Restore</a>
                        <?php 
                    }
                    ?>
                  
                </div>
            </div>

          </div>
          </li>
          <?php }?>

        
        </ul>
        <div class="pagination-container">
          <p class='paginacaoCursor beforePagination' id=""><i class="fa fa-chevron-left" aria-hidden="true"></i></p>
          <p class='paginacaoCursor afterPagination' id=""><i class="fa fa-chevron-right" aria-hidden="true"></i></p>
        </div>
               
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>

    
        <?php include 'includes/footer.php';?>
        
<!-- new styles  -->
<style>
  ul.salonee_locations._rating {
    width: 95%;
    flex-flow: column;
}
ul.salonee_locations._rating li{
  display: initial;
}
._stars small{
  display: none;
}
.rating_footr {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    margin-top: 1em;
    margin-bottom: .5em;
}
.rating_footr b{
  font-size: 14px;
  font-weight: 600;
}
.rating_footr>div {
    flex: 0 0 33%;
    max-width: 33%;
}
</style>





<style>

ul.salonee_locations.paginationTable._rating li {
    padding: 12px 15px;
}
ul.salonee_locations.paginationTable._rating p {
    padding: 0;
}
ul.salonee_locations.paginationTable._rating small {
    font-size: 14px;
    font-weight: 600;
}
ul.salonee_locations.paginationTable._rating p>b {
    font-weight: 600;
    font-size: 15px;
    margin-bottom: 4px;
}


  .stars-outer {
  position: relative;
  display: inline-block;
}

.stars-inner {
  position: absolute;
  top: 0;
  left: 0;
  white-space: nowrap;
  overflow: hidden;
  width: 0;
}

.stars-outer::before {
  content: "\f005 \f005 \f005 \f005 \f005";
  font-family: "FontAwesome";
  font-weight: 900;
  color: #ccc;
}

.stars-inner::before {
  content: "\f005 \f005 \f005 \f005 \f005";
  font-family: "FontAwesome";
  font-weight: 900;
  color: #f8ce0b;
}
</style>

    <script>

// Initial Ratings
const ratings = {
fourFive: 4.5,
threeFIve: 3.5,
twoFive: 2.5,
three: 3.0,
four: 4.0
}

// Total Stars
const starsTotal = 5;

// Run getRatings when DOM loads
document.addEventListener('DOMContentLoaded', getRatings);

// Form Elements
const productSelect = document.getElementById('product-select');
const ratingControl = document.getElementById('rating-control');

// Init product
let product;

// Product select change
if(productSelect){
productSelect.addEventListener('change', (e) => {
product = e.target.value;
// Enable rating control
ratingControl.disabled = false;
ratingControl.value = ratings[product];
});
};
if(ratingControl){
// Rating control change
ratingControl.addEventListener('blur', (e) => {
const rating = e.target.value;

// Make sure 5 or under
if (rating > 5) {
  alert('Please rate 1 - 5');
  return;
}

// Change rating
ratings[product] = rating;

getRatings();
});
};
// Get ratings
function getRatings() {
for (let rating in ratings) {
  // Get percentage
  const starPercentage = (ratings[rating] / starsTotal) * 100;

  // Round to nearest 10
  const starPercentageRounded = `${Math.round(starPercentage / 10) * 10}%`;

  // Set width of stars-inner to percentage
  document.querySelector(`.${rating} .stars-inner`).style.width = starPercentageRounded;

  // Add number rating
  document.querySelector(`.${rating} .number-rating`).innerHTML = ratings[rating];
}
}
</script>
    </body>
</html>