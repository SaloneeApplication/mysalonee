<?php
require_once dirname(__DIR__) . '/controller/AdminPortal.php';
$controller = new AdminPortal();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
include "includes/header.php";
?>
      <!-- BEGIN sidebar -->
      <?php include('includes/sidebar.php');?>
      <!--  //END sidebar -->

      <!-- BEGIN main -->

        <div class="main__content">
          <div class="w-50">
            <div class="input-group mb-3">
              <input type="text" class="form-control _search" placeholder="Search" id="searchTd">
              <div class="input-group-append">
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
          </div>

          
          <div class="rightBtn">
            <button class="btn login-btn" data-toggle="modal" data-target="#addPromoCode" id="myButton">Add &nbsp;<i
                class="fa fa-plus" aria-hidden="true"></i></button>
          </div>

          <br>
          <div class="clearfix"></div>

          <div class="pad_3">
            <div class="table-responsive">

              <table class="table themeTable usersTable adTable">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Image</th>
                    <th>Admin Portals</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody id="saloonTable">
                  <tr>
                    <td>01</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>
                    
                  </tr>
                  <tr>
                    <td>02</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>                    
                  </tr>
                  <tr>
                    <td>03</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>                    
                  </tr>
                  <tr>
                    <td>04</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>                    
                  </tr>
                  <tr>
                    <td>05</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>                    
                  </tr>
                  <tr>
                    <td>06</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>                    
                  </tr>
                  <tr>
                    <td>07</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>                    
                  </tr>

                  <tr>
                    <td>08</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>                    
                  </tr>
                  <tr>
                    <td>09</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>                    
                  </tr>
                  <tr>
                    <td>10</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>                    
                  </tr>
                  <tr>
                    <td>11</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>                    
                  </tr>
                  <tr>
                    <td>12</td>
                    <td>
                        <span class="circlImg">
                            <img src="./assets/img/admin/business-report.png" alt="business-report">
                        </span>
                    </td>
                    <td>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                        Dolores tenetur ipsam maiores nostrum, consequatur reprehenderit, consequatur reprehenderit.</td>
                    <td>
                        <span>
                            <img src="./assets/img/admin/edit.png" alt="edit" class="edit_icon">
                          </span>
                      <span>
                        <img src="./assets/img/admin/trash.png" alt="trash" class="trash_icon">
                      </span>
                    </td>                    
                  </tr>

                </tbody>
              </table>

            </div>
            <div class="clearfix"></div>

            <!-- pagination  -->
            <div class='pagination-container tablePaging'>
              <div class="pagination">
                <ol id="numbers"></ol>
              </div>
            </div>


          </div>


        </div>
    </div>
    </main>
    <!--  //END main -->

    <!-- <div class="layout__container-after"></div> -->



  </div>


  <!-- partial -->
  <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Subscription Plan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="login">
            <form>
                <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <input type="text" id="adName" class="form-control" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="adName">Name</label>
                      </div>
                    </div>
                  </div>
                <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="text" id="duration" class="form-control" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="duration">Duration</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="text" id="price" class="form-control" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="price">Price</label>
                      </div>
                    </div>
                  </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <textarea type="text" id="adDesc" rows="5" class="form-control" autocomplete="off" ></textarea>
                    <label class="form-control-placeholder p-0" for="adDesc">Description</label>
                  </div>
                </div>
              </div>
             

              <div class="form-group">
                <button type="button" class="btn theme-btn">Submit</button>
              </div>
            </form>
          </div>

        </div>

      </div>
    </div>
  </div>
  <?php include 'includes/footer.php';?>


</body>

</html>