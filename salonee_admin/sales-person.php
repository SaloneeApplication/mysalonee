<?php 
require_once dirname(__DIR__) . '/controller/Employees.php';
$emp = new Employees();
$emp->load();
$persons=$emp->getServiceProviders();
$cities=$emp->getCities();
$page_data=$emp->pageData();
$title=$page_data['title'];
$page=$page_data['page'];
include("includes/header.php");
?>
<!-- BEGIN sidebar -->
<?php include('includes/sidebar.php');?>
<!--  //END sidebar -->
<style>
table.table.themeTable td, table.table.themeTable th {
  max-width:none;
}
</style>
            <!-- BEGIN main -->
                <div class="main__content">
                    <div class="w-50">
                        <div class="input-group mb-3 _hide">
                            <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
                            <div class="input-group-append">
                                <button class="btn theme-btn" type="submit">Go</button>
                            </div>
                        </div>
                    </div>
                    <div class="rightBtn">
                        <button class="btn login-btn" data-toggle="modal" data-target="#addPromoCode" id="myButton">Add &nbsp;<i
                            class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                    <div class="pad_3">
                        <div class="table-responsive">
                            <table class="table theme_Table dataTable" id="example">
                                <thead>
                                    <tr>
                                        <th width="1">S.No</th>
                                        <th width="5">Name</th>
                                        <th width="15">Business Name</th>
                                        <th width="25">Email </th>
                                        <th width="20">Phone </th>
                                        <th width="20">Licence</th>
                                        <th width="20">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1;
                                    foreach($persons as $row)
                                    {?>
                                        <tr>
                                            <td><?= $i++; ?></td>
                                            <td><?= $row['name']; ?></td>
                                            <td><?= $row['business_name']; ?></td>
                                            <td><?= $row['email']; ?></td>
                                            <td><?= $row['mobile']; ?></td>
                                            <td><a target="blank" href="<?php echo base_url1;?>uploads/<?= $row['business_licence'] ?>"><i class="fa fa-file fa-2x"></i></a></td>
                                            <?php
                                                if($row['status']=='1'){
                                                    echo '<td><label class="badge  badge-pill  badge-success">Active</label></td>';
                                                }else if($row['status']=='3'){
                                                    echo '<td><label class="badge  badge-pill  badge-warning">Pending</label></td>';
                                                }else{
                                                    echo '<td><label class="badge  badge-pill  badge-danger">Deactive</label></td>';
                                                }
                                            ?>
                                        </tr>
                                    <?php
                                    }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <!-- pagination  -->
                        <div class='pagination-container tablePaging'>
                            <div class="pagination">
                                <ol id="numbers"></ol>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>

        <!-- modal -->
        
        <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Add Service Provider </h5>
                  <button type="button" class="close new_service" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="login">
                    <form action="sales-person.php" enctype="multipart/form-data" onsubmit="return validateForm();" method="post">
                    <input type="hidden" name="type" value="addServiceProvider" />
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" id="name" class="form-control" name="name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="name">Name *</label>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" id="business_name" class="form-control" name="business_name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="business_name">Business Name *</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="email">Email *</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="mobile" id="mobile" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="mobile">Mobile *</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select class="form-control" autocomplete="off" id="city" name="city" required>
                                      <option value="0"> Select Location *</option>
                                        <?php foreach($cities as $cit){ 
                                          echo '<option value="'.$cit['id'].'">'.$cit['city_name_en'].'</option>';
                                        }?>
                                    </select>
                                    <label class="form-control-placeholder p-0" for="city">City *</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" id="address" name="address" required></textarea>
                                    <label class="form-control-placeholder p-0" for="address">Address *</label>
                                </div>
                            </div>
                        </div>
                      <div class="form-group">
                        <label for="file-upload" class="custom-file-upload sadmnUpload">
                          Business Licence<i class="fa fa-camera" aria-hidden="true"></i>
                        </label>
                        <input id="file-upload" name='image' type="file"  style="display:none;">
                      </div>

                      <div class="form-group">
                        <button type="submit" class="btn theme-btn">Submit</button>
                      </div>
                    </form>
                  </div>

                </div>

              </div>
            </div>
          </div>
          <!-- modal end -->
          
<?php include('includes/footer.php');?>

<script>

$(document).ready(function() {
    $('#example').dataTable({
      "sPaginationType": "full_numbers",
      "language": { 
            "zeroRecords": "Sorry, No service providers are found.",           
        },
        aLengthMenu: [
        [10,25, 50, 100, 200, -1],
        [10,25, 50, 100, 200, "All"]
    ],
    // iDisplayLength: -1
    });
});



function validateForm(){
  var name=$("#name").val();
  var business_name=$("#business_name").val();
  var email=$("#email").val();
  var mobile=$("#mobile").val();
  var city=$("#city").val();
  var address=$("#address").val();
  var size=document.getElementById("file-upload").files[0];
  name=$.trim(name);
  business_name=$.trim(business_name);
  email=$.trim(email);
  mobile=$.trim(mobile);
  address=$.trim(address);
  if(name.length==0){
    iziToast.error({
      title: 'Error',
      message: 'Please enter name',
    });
    return false;
  }
  if(address.length==0){
    iziToast.error({
      title: 'Error',
      message: 'Please enter address',
    });
    return false;
  }
  if(business_name.length==0){
    iziToast.error({
      title: 'Error',
      message: 'Please enter business name',
    });
    return false;
  }
  if(mobile.length==0){
      iziToast.error({
      title: 'Error',
      message: 'Please enter mobile number',
    });
  return false;
    }
    var regex = /^[a-z A-Z]*$/;
    if(!regex.test(name)){
      iziToast.error({
      title: 'Error',
      message: 'Please enter valid name ',
    });
    return false;
    }
    var regex = /^[0-9]*$/;
    if(!regex.test(mobile)){
      iziToast.error({
      title: 'Error',
      message: 'Please enter valid mobile number ',
    });
    return false;
    }
    if(city=="0"){
      iziToast.error({
      title: 'Error',
      message: 'Please select location',
  });
  return false;
    }
    if(typeof document.getElementById("file-upload").files[0]=='undefined'){
      iziToast.error({
      title: 'Error',
      message: 'Please select business licence',
  });
  return false;
    }
}
$(".new_service").click(function(){
  location.reload();
});
</script>
    </body>
</html>