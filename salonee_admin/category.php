<?php
require_once dirname(__DIR__) . '/controller/AdminCategories.php';
$controller = new AdminCategories();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$categories=$controller->getCategories();
include "includes/header.php";
?>

      <!-- BEGIN sidebar -->
      <?php include('includes/sidebar.php');?>
      <!--  //END sidebar -->

      <!-- BEGIN main -->
      <main>
        <div class="main__content">
          <div class="w-50">
            <div class="input-group mb-3">
              <input type="text" class="form-control _search" id="category_search" placeholder="Search">
              <div class="input-group-append">
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
          </div>
          <div class="rightBtn">
            <button class="btn login-btn" data-toggle="modal" data-target="#addCategory" id="myButton">Add &nbsp;<i
                class="fa fa-plus" aria-hidden="true"></i></button>
          </div>
          <br><br>
          <div class="clearfix"></div>
          <ul class="paginationTable salonee_category category-card category-list">

            <?php while($row=$categories->fetch()){ ?>
            <li class=" tableItem">
              <span><img src="<?php echo admin_base_url.$row['image'];?>" width="100" alt="category"></span>
              <p>
                <b><?php echo $row['name'].' <custom style="font-size:14px;font-weight:600;">[ '.ucwords($row['category_for']).' ]</custom>';?></b>
                <?php echo $row['short_description'];?>
              </p>
              <small><i class="fa fa-pencil-square-o edit_category" data-id="<?php echo $row['category_id'];?>" data-for="<?php echo $row['category_for'];?>"  data-short-name="<?php echo $row['short_description'];?>" data-category-name="<?php echo $row['name'];?>" aria-hidden="true"></i> 
              
              <?php if($row['status']==1){?>
              <i class="fa fa-eye-slash deactivatecategory " style="color:red;" title="Disable" data-id="<?php echo $row['category_id'];?>" aria-hidden="true"> </i>
              <?php }else{ ?>
                <i class="fa fa-eye activatecategory " style="color:green;" title="Enable" data-id="<?php echo $row['category_id'];?>" aria-hidden="true"> </i>
              <?php } ?>
                  
              </small>
            </li>
            <?php }?>
            
            
          </ul>
          <div class="pagination-container">
            <p class='paginacaoCursor beforePagination' id=""><i class="fa fa-chevron-left" aria-hidden="true"></i></p>
            <p class='paginacaoCursor afterPagination' id=""><i class="fa fa-chevron-right" aria-hidden="true"></i></p>
          </div>


        </div>
      </main>
      <!--  //END main -->

      <!-- <div class="layout__container-after"></div> -->

    </div>

    <!-- add category modal  -->
    <div class="modal fade pswdModal" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="addCategoryTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addCategoryTitle">Add Category</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="<?php echo admin_base_url;?>category.php" method="post" id="categor_im" onsubmit="return validateForm()" name="categor_im" enctype="multipart/form-data">
              <input type="hidden" name="type" value="addNewCategory" />
              <div class="form-group mt-4 mb-5">
                <select name="category_for" id="category_for" class="form-control" autocomplete="off" required >
                  <option hidden disabled selected value> -- Select Category For -- </option>
                  <option value="women">Women</option>
                  <option value="men">Men</option>
                  <option value="children">Children</option>
                </select>
              </div>
                <div class="form-group mt-4 mb-5">
                  <input type="text" id="category" name="category" class="form-control" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="category">Category Name</label>
                </div>
                <div class="form-group mt-4 mb-5">
                  <textarea name="short_d" id="short_d"  class="form-control" autocomplete="off" required></textarea>
                  <label class="form-control-placeholder p-0" for="short_d">Short Description</label>
                </div>
                <div class="form-group mb-5">
                  <label for="file-upload" class="custom-file-upload">
                    UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                  </label>
                  <input id="file-upload" name='category_image' type="file"  style="display:none;">
                </div>
                <div class="form-group mb-5">
                  <label for="file-upload2" class="custom-file-upload">
                    MOBILE IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                  </label>
                  <input id="file-upload2" name='mobile_category_image' type="file"  style="display:none;">
                </div>
                <div class="form-group">
                  <input type="submit" id="categorySubmit" class="btn theme-btn categorySubmit" value="Submit">
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>

    <!-- add category modal  -->
    <div class="modal fade pswdModal" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="addCategoryTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addCategoryTitle">Edit Category</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="<?php echo admin_base_url;?>category.php" method="post" enctype="multipart/form-data">
              <input type="hidden" name="type" value="updateCategory" />
              <input type="hidden" name="cat_id" id="cat_id" value="" />
              <div class="form-group mt-4 mb-5">
                <select name="category_for" id="category_for1" class="form-control" autocomplete="off" required >
                  <option hidden disabled selected value> -- Select Category For -- </option>
                  <option value="women">Women</option>
                  <option value="men">Men</option>
                  <option value="children">Children</option>
                </select>
              </div>
                <div class="form-group mt-4 mb-5">
                  <input type="text" id="category1" name="category" class="form-control" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="category">Category Name</label>
                </div>
                <div class="form-group mt-4 mb-5">
                  <textarea name="short_d" id="short_d1"  class="form-control" autocomplete="off" required></textarea>
                  <label class="form-control-placeholder p-0" for="short_d">Short Description</label>
                </div>
                <div class="form-group mb-5">
                  <label for="file-upload1" class="custom-file-upload">
                    UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                  </label>
                  <input id="file-upload1" name='category_image' type="file"  style="display:none;">
                </div>
                <div class="form-group mb-5">
                  <label for="file-upload3" class="custom-file-upload">
                    MOBILE IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                  </label>
                  <input id="file-upload3" name='mobile_category_image' type="file"  style="display:none;">
                </div>
                <div class="form-group">
                  <input type="submit" id="categorySubmit" class="btn theme-btn categorySubmit" value="Submit">
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>


    <?php include 'includes/footer.php';?>
    <script>
    $(".edit_category").click(function(){
      var catid=$(this).data("id");
      var sname=$(this).data("short-name");
      var cname=$(this).data("category-name");
      var for1=$(this).data("for");
      $("#cat_id").val(catid);
      $("#category1").val(cname);
      $("#short_d1").val(sname);
      $("#category_for1").val(for1);
      $("#editCategory").modal('show');

    });
      $("form[name='categor_im']").submit(function(e){
        if(typeof $('#file-upload')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select category image',
          });
          e.preventDefault();
          return false;
        }
        if(typeof $('#file-upload2')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select category mobile image',
          });
          e.preventDefault();
          return false;
        }
      });
      
      $('#file-upload1').change(function() {
        var i = $(this).prev('label').clone();
        var file = $('#file-upload1')[0].files[0].name;
        $(this).prev('label').text(file);
      });
       
  $(".deactivatecategory").click(function () {
    swal({
      title: "Are you sure?",
      text: "You want to disable this category",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, Deactivate it!",
    }).then(result => {
      if (result.dismiss === 'cancel') {
        swal.closeModal();
        return;
      }
      $.ajax({
        url:"<?php echo admin_base_url.'category.php';?>",
        method:"POST",
        data:{type:'disablecategory',cat_id:$(this).data("id")},
        success:function(resp){
          var resp=JSON.parse(resp);
          if(resp.status==1){
           location.reload();
          }
        }
      })
      
    },
    );
  });
  $(".activatecategory").click(function () {
    swal({
      title: "Are you sure?",
      text: "You want to disable this category",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, Activate it!",
    }).then(result => {
      if (result.dismiss === 'cancel') {
        swal.closeModal();
        return;
      }
      $.ajax({
        url:"<?php echo admin_base_url.'category.php';?>",
        method:"POST",
        data:{type:'enablecategory',cat_id:$(this).data("id")},
        success:function(resp){
          var resp=JSON.parse(resp);
          if(resp.status==1){
           location.reload();
          }
        }
      })
    },
    );
  });
  $('#file-upload2').change(function() {
  var i = $(this).prev('label').clone();
  var file = $('#file-upload2')[0].files[0].name;
  $(this).prev('label').text(file);
});
    </script>
</body>
</html>