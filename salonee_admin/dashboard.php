<?php
require_once dirname(__DIR__) . '/controller/AdminDashboard.php';
$controller=new AdminDashboard();
$controller->load();
$page_data=$controller->pageData();
$title = $page_data['title']; $page = $page_data['page'];
$adminId=$page_data['admin_id'];
$products_chart=$page_data['products_chart'];
$slots_chart=$page_data['slots_chart'];
// include("includes/header.php"); 
if($_SESSION['auth_type']=='salesperson'){
    header("Location:sales-person.php");
    exit;
}
// include("class/dbConnection.php");
// include("class/admin.php");
// $dbObject = new dbConnection();
// $con = $dbObject->getConnection();
// if($con)
// {
//   $role_name="Admin";
//   $role_access=array();
//   $adminObj = new admin();
//   $city_name ="";
//   if($_SESSION['auth_type']=='admin'){
//     $role_name="Admin";
//     $res = $adminObj->getAdminDetails($adminId,$con);
//     while($row = mysqli_fetch_array($res))
//     {
//       $image = $row['image'];
//       $name = $row['name'];
//       $email = $row['email'];
//       $mobile = $row['mobile'];
//       $role_name = $row['role_name'];
//       $role_access = $row['role_access'];
//     }
//     $role_access=json_decode($role_access);
//   }elseif($_SESSION['auth_type']=='salesperson'){
//     $role_name="Sales Person";
//     $res = $adminObj->getadminEmployeeDetails($adminId,$con);
//     while($row = mysqli_fetch_array($res))
//     {
//       $image = $row['image'];
//       $name = $row['name'];
//       $email = $row['email'];
//       $city_name = $row['city_name'];
//       $mobile = $row['mobile'];
//     }
//   }
  
// }
// else
// {
//   echo mysqli_errno()."<br/>".mysqli_error();
// }
include("includes/header.php"); 
?>

      <!-- <div class="layout__container-before"></div> -->
      <?php include('includes/sidebar.php');?>
      <!-- BEGIN main -->
     
        <div class="main__content">

           <div class="Dshbrd_Cards">
             <ul>
               <li>
                 <span><a href="<?php echo admin_base_url;?>service-bookings.php">
                   <img src="<?php echo admin_base_url;?>assets/img/svg/01.svg" alt=""></a>
                 </span>
                 <div> 
                 <a href="<?php echo admin_base_url;?>service-bookings.php">
                   <h3>Bookings</h3></a>
                 </div>
               </li>               
              <li>
                <span><a href="<?php echo admin_base_url;?>dashboard.php">
                  <img src="<?php echo admin_base_url;?>assets/img/svg/02.svg" alt=""></a>
                </span>
                <div> <a href="<?php echo admin_base_url;?>service-bookings.php">
                  <h3>Slot Availability</h3></a>
                </div>
              </li>
              <li>
                <span><a href="<?php echo admin_base_url;?>service-reports.php">
                  <img src="<?php echo admin_base_url;?>assets/img/svg/03.svg" style="min-width: 55px; padding: 0" alt="">
                  </a>
                </span>
                <div> 
                <a href="<?php echo admin_base_url;?>revenue-reports.php">
                  <h3>Earnings</h3></a>
                </div>
              </li>
              <li>
                <span>  <a href="<?php echo admin_base_url;?>users.php">
                  <img src="<?php echo admin_base_url;?>assets/img/svg/04.svg" alt=""></a>
                </span>
                <div> 
                  <a href="<?php echo admin_base_url;?>users.php">
                  <h3>Customers</h3></a>
                </div>
              </li>
              <li>
                <span> <a href="<?php echo admin_base_url;?>advertisement.php">
                  <img src="<?php echo admin_base_url;?>assets/img/svg/05.svg" alt=""></a>
                </span>
                <div>  <a href="<?php echo admin_base_url;?>advertisement.php">
                  <h3>Promotions</h3></a>
                </div>
              </li>
              <li>
                <span> <a href="<?php echo admin_base_url;?>employees.php">
                  <img src="<?php echo admin_base_url;?>assets/img/svg/06.svg" alt=""></a>
                </span>
                <div> 
                <a href="<?php echo admin_base_url;?>employees.php">
                  <h3>Sub admins</h3></a>
                </div>
              </li>
              <li>
                <span><a href="<?php echo admin_base_url;?>revenue-reports.php">
                  <img src="<?php echo admin_base_url;?>assets/img/svg/07.svg" alt=""></a>
                </span>
                <div> <a href="<?php echo admin_base_url;?>revenue-reports.php">
                  <h3>Revenue</h3></a>
                </div>
              </li>
              <li>
                <span><a href="<?php echo admin_base_url;?>privacy-policy.php">
                  <img src="<?php echo admin_base_url;?>assets/img/svg/08.svg" style="min-width: 55px; padding: 0" alt=""></a>
                </span>
                <div> <a href="<?php echo admin_base_url;?>privacy-policy.php">
                  <h3>On Hold Amount</h3></a>
                </div>
              </li>
             </ul>
           </div>
		    
           <div class="status_cards">
             <ul>
               <li class="_crd crd-50">
                 <h3>Upcoming Appointments</h3>
                 <div class="_crdBody">
                  <div class="appointment-card">
                    <div class="appointment-date">
                      <div class="date-txt">5</div>
                      <div class="month-txt">July 21</div>
                    </div>
                    <div class="appointment-info">
                    <div class="appointment-type">Hair Cut</div>
                    <div class="appointment-time">04:30pm</div>
                    </div>
                  </div>
                  <div class="appointment-card">
                    <div class="appointment-date">
                      <div class="date-txt">6</div>
                      <div class="month-txt">July 21</div>
                    </div>
                    <div class="appointment-info">
                    <div class="appointment-type">Hair Color</div>
                    <div class="appointment-time">04:30pm</div>
                    </div>
                  </div>
                  <div class="appointment-card">
                    <div class="appointment-date">
                      <div class="date-txt">7</div>
                      <div class="month-txt">July 21</div>
                    </div>
                    <div class="appointment-info">
                    <div class="appointment-type">Pedicure</div>
                    <div class="appointment-time">06:30pm</div>
                    </div>
                  </div>
                  <div class="appointment-card">
                    <div class="appointment-date">
                      <div class="date-txt">10</div>
                      <div class="month-txt">July 21</div>
                    </div>
                    <div class="appointment-info">
                    <div class="appointment-type">Spa</div>
                    <div class="appointment-time">03:30pm</div>
                    </div>
                  </div>
                </div>
               </li>
               <li class="_crd crd-50">
                <h3>Reports</h3>
                <div class="" style="padding: 0 0.75em">
                  <ul class="nav nav-tabs">
                    <li ><a data-toggle="tab" href="#tab1" class="active">Finance</a></li>
                    <li><a data-toggle="tab" href="#tab2">Customers</a></li>
                    <li><a data-toggle="tab" href="#tab3">Operations</a></li> 
                  </ul>
                
                  <div class="tab-content">
                    <div id="tab1" class="tab-pane fade in active show">
                      <canvas id="finance" width="210" height="150"></canvas>
                    </div>
                    <div id="tab2" class="tab-pane fade">
                      <canvas id="customers" width="210" height="150"></canvas>
                    </div>
                    <div id="tab3" class="tab-pane fade">
                      <canvas id="operations" width="210" height="150"></canvas>
                    </div> 
                  </div>
               </div>
              </li>

              <li class="_crd crd-50">
                <h3>Scheduler</h3>
                <div class="ftco-section">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="calendar calendar-first" id="calendar_first">
                          <div class="calendar_header">
                              <button class="switch-month switch-left"> <i class="fa fa-chevron-left"></i></button>
                               <h2></h2>
                              <button class="switch-month switch-right"> <i class="fa fa-chevron-right"></i></button>
                          </div>
                          <div class="calendar_weekdays"></div>
                          <div class="calendar_content"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="_crd crd-50">
                <h3>Testimony/ Reviews</h3>
                <div class="_crdBody">
                <p class="testimonial-text">Thanks to saloonee app, my grooming was awsome have become a thing of the past.<br/>
                <span><b>-Mallika Ahmed</b></span></p>
                <p class="testimonial-text">Thanks to saloonee app, my grooming was awsome have become a thing of the past.<br/>
                  <span><b>-Mallika Ahmed</b></span></p>
                  <p class="testimonial-text">Thanks to saloonee app, my grooming was awsome have become a thing of the past.<br/>
                    <span><b>-Mallika Ahmed</b></span></p>
                    <p class="testimonial-text">Thanks to saloonee app, my grooming was awsome have become a thing of the past.<br/>
                      <span><b>-Mallika Ahmed</b></span></p>
              </div>
              </li>



              <li class="_crd _cntr">
                <div> <a href="<?php echo admin_base_url;?>service-provider.php">
                  <h3>Branch</h3></a>
                </div>
                <span><a href="<?php echo admin_base_url;?>service-provider.php">
                  <img src="<?php echo admin_base_url;?>assets/img/svg/09.svg" alt=""></a>
                </span> 
              </li>

              <li class="_crd _cntr"> 
                <div> <a href="<?php echo admin_base_url;?>promocode.php">
                  <h3>Offers/Deals</h3></a>
                </div>
                <span><a href="<?php echo admin_base_url;?>promocode.php">
                  <img src="<?php echo admin_base_url;?>assets/img/svg/10.svg" alt=""></a>
                </span> 
              </li>

              <li class="_crd _cntr"> 
                <div> <a href="<?php echo admin_base_url;?>products.php">
                  <h3>Store Products</h3></a>
                </div>
                <span><a href="<?php echo admin_base_url;?>products.php">
                  <img src="<?php echo admin_base_url;?>assets/img/svg/11.svg" alt=""></a>
                </span> 
              </li>

              <li class="_crd _cntr"> 
                <div> <a href="<?php echo admin_base_url;?>ratings.php">
                  <h3>Ratings</h3></a>
                </div>
                <span><a href="<?php echo admin_base_url;?>ratings.php">
                  <img src="<?php echo admin_base_url;?>assets/img/svg/12.svg" alt=""></a>
                </span> 
              </li>
             </ul>
           </div>
		   
		</div>

      </main>
      <!--  //END main -->

      <!-- <div class="layout__container-after"></div> -->



    </div>

  <style>
    .layout__container .main{
      background-color: #dfdfdf;
    }
    .Dshbrd_Cards ul, 
    .status_cards ul {
      margin: 0;
      padding: 0;
      list-style-type: none;
      width: 100%;
      display: flex;
      flex-wrap: wrap;
      padding-left: 1em;
    }
    .Dshbrd_Cards ul li,
    .status_cards ul li._crd{
      background-color: #d56291;
      min-height: 135px;
      text-align: center;
      min-width: 22.5%;
      max-width: 22.5%;
      margin: 0.75em;
      padding: 1em;
      margin-top: 0.5em;
      justify-content: center;
      /* align-items: center; */
      display: flex;
      flex-flow: column;
      border-radius: 10px;
    } 
    .Dshbrd_Cards ul li h3,
    .status_cards ul li h3{
      font-weight: 500;
      font-size: 1.1em;
      color: white;
      text-transform: uppercase;
      font-family: 'Montserrat', sans-serif;
    }
    

    .status_cards ul li._crd{
      background-color: #fff;
      text-align: left;
      justify-content: flex-start;
      align-items: flex-start;
      padding: 1em .5em;
    }
    .status_cards ul li h3{
      color: #444;
      padding: 0em .5em;
    }
    .status_cards ul li p{
      font-size: 12px;
      margin-bottom: 10px;
      color: #444;
    }
    .status_cards ._crdBody {
      width: 100%;
      max-height: 215px;
      overflow-y: auto;
      padding: 0em .5em;
    }
    .Dshbrd_Cards ul li span img {
      width: 100%;
      max-width: 35px;
      padding: .5em 0;
  }
  .status_cards ul .nav-tabs {
    display: flex;
    flex-flow: column;
    border: 0;
    padding: 0;
}
.status_cards ul .nav-tabs a{
  font-size: 12px;
  color: #333;
  text-transform: uppercase;
  text-decoration: none;
}
.status_cards ul .nav-tabs li a.active{
  font-weight: bold;
}

.status_cards canvas{
  min-width: 210px;
  min-height: 150px;
}
.status_cards li._crd._cntr{
  text-align: center;
  justify-content: center;
  align-items: center;
}
.status_cards li._crd._cntr span img {
    width: 100%;
    max-width: 100px;
}
._crdBody::-webkit-scrollbar {
  -webkit-appearance: none;
  height: 4px;
  width: 6px; 
}
._crdBody::-webkit-scrollbar-track {
  background-color: #aaa;
}
._crdBody::-webkit-scrollbar-thumb {
  border-radius: 4px;
  background-color: #ddd;
  -webkit-box-shadow: inset 0 0 1px rgba(255,255,255,0.2);
  border: 3px solid #ddd;
}

.testimonial-text{
  border: 1px solid pink;
    padding: 10px;
    border-radius: 10px;
}
/**Abhisek Panda Styles**/
.appointment-card {
    margin-bottom: 15px;
}
.appointment-info {
    padding: 12px 5px;
    background: #eeeeee;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
    display: flex;
    flex-direction: column;
}
.appointment-date {
    padding: 10px;
    background: #d56291;
    color: #fff;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
    float: left;
    text-align: center;
}
.appointment-card:nth-child(odd) .appointment-date {
    background: #d56291;
}
.appointment-card:nth-child(even) .appointment-date {
    background: #444;
}
.appointment-date .date-txt {
    line-height: 1;
    font-size: 20px;
    font-weight: 700;
}
.appointment-date .month-txt {
    font-size: 14px;
    line-height: 1;
}
.appointment-type {
    line-height: 1;
    color: #333;
    padding-bottom: 2px;
}
.appointment-time {
    line-height: 1;
    font-size: 13px;
    color: #999;
}
.appointment-card:nth-child(odd) .appointment-type {
    color: #d56291;
}
.appointment-card:nth-child(even) .appointment-type {
    color: #444;
}
	</style>
    
<?php include 'includes/footer.php';?>

<script>
  new Chart(document.getElementById("finance"), {
    type: 'bar',
    data: {
      labels: ["1900", "1950", "1999", "2050"],
      datasets: [
        {
          label: "Africa",
          backgroundColor: "#3e95cd",
          data: [133,221,783,2478]
        }, {
          label: "Europe",
          backgroundColor: "#8e5ea2",
          data: [408,547,675,734]
        }
      ]
    },
    options: {
      title: {
        display: false,
        text: 'Population growth (millions)'
      }
    }
});


new Chart(document.getElementById("customers"), {
    type: 'bar',
    data: {
      labels: ["1900", "1950", "1999", "2050"],
      datasets: [
        {
          label: "Africa",
          backgroundColor: "#0acda7",
          data: [133,221,783,2478]
        }, {
          label: "Asia",
          backgroundColor: "#c7aa24",
          data: [408,547,675,734]
        }
      ]
    },
    options: {
      title: {
        display: false,
        text: 'Population growth (millions)'
      }
    }
});


new Chart(document.getElementById("operations"), {
    type: 'bar',
    data: {
      labels: ["1900", "1950", "1999", "2050"],
      datasets: [
        {
          label: "Africa",
          backgroundColor: "#1ff0ee",
          data: [133,221,783,2478]
        }, {
          label: "Asia",
          backgroundColor: "#cf0808",
          data: [408,547,675,734]
        }
      ]
    },
    options: {
      title: {
        display: false,
        text: 'Population growth (millions)'
      }
    }
});
</script>
</body>
</html>
