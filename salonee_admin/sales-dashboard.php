<?php 
require_once dirname(__DIR__) . '/controller/EmployeeDashboard.php';
$emp = new EmployeeDashboard();
$emp->load();
$persons=$emp->getServiceProviders();
$cities=$emp->getCities();
$page_data=$emp->pageData();
$title=$page_data['title'];
$page=$page_data['page'];
$total_p=$page_data['total_p'];
$total_a=$page_data['total_a'];
$total_pp=$page_data['total_pp'];
$total_b=$page_data['total_b'];
include("includes/header.php");
?>
<!-- BEGIN sidebar -->
<?php include('includes/sidebar.php');?>
<!--  //END sidebar -->
<style>
table.table.themeTable td, table.table.themeTable th {
  max-width:none;
}
</style>
            <!-- BEGIN main -->
                <div class="main__content">
                 
<div class="displayCards">
             <ul>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Total Providers</p>
                   <h3><?php echo $total_p;?></h3>
                 </div>
               </li>
               <li>
                <span>
				<i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                </span>
                <div>
                  <p>Active Providers</p>
                  <h3><?php echo $total_a;?></h3>
                </div>
              </li>
			  <li>
                <span>
				<i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                </span>
                <div>
                  <p>Pending Providers</p>
                  <h3><?php echo $total_pp;?></h3>
                </div>
              </li>
			  <li>
                <span>
				<i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                </span>
                <div>
                  <p>Blocked Providers</p>
                  <h3><?php echo $total_b;?></h3>
                </div>
              </li>
			</ul>
			</div>
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>

        <!-- modal -->
        
        <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Add Service Provider </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="login">
                    <form action="sales-person.php" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="type" value="addServiceProvider" />
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" id="title" class="form-control" name="name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="title">Name</label>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" id="business_name" class="form-control" name="business_name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="title">Business Name</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="code">Email</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="mobile" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="value">Mobile</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select class="form-control" autocomplete="off" id="city" name="city" required>
                                      <option value="0"> Select Location</option>
                                        <?php foreach($cities as $cit){ 
                                          echo '<option value="'.$cit['id'].'">'.$cit['city_name_en'].'</option>';
                                        }?>
                                    </select>
                                    <label class="form-control-placeholder p-0" for="city">City</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" id="address" name="address" required></textarea>
                                    <label class="form-control-placeholder p-0" for="address">Address</label>
                                </div>
                            </div>
                        </div>
                      <div class="form-group">
                        <label for="file-upload" class="custom-file-upload sadmnUpload">
                          Business Licence<i class="fa fa-camera" aria-hidden="true"></i>
                        </label>
                        <input id="file-upload" name='image' type="file" required style="display:none;">
                      </div>

                      <div class="form-group">
                        <button type="submit" class="btn theme-btn">Submit</button>
                      </div>
                    </form>
                  </div>

                </div>

              </div>
            </div>
          </div>
          <!-- modal end -->
          
<?php include('includes/footer.php');?>
    </body>
</html>