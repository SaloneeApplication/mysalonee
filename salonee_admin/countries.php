<?php
require_once dirname(__DIR__) . '/controller/AdminCountries.php';
$controller = new AdminCountries();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
$countries=$controller->getCountries();
include "includes/header.php";
?>

      <!-- BEGIN sidebar -->
      <?php include('includes/sidebar.php');?>
      <!--  //END sidebar -->

      <!-- BEGIN main -->
      <main>
        <div class="main__content">
          <div class="w-50">
            <div class="input-group mb-3">
              <input type="text" class="form-control _search" id="category_search" placeholder="Search">
              <div class="input-group-append">
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
          </div>
          <div class="rightBtn">
            <button class="btn login-btn" data-toggle="modal" data-target="#addCountry" id="myButton">Add &nbsp;<i
                class="fa fa-plus" aria-hidden="true"></i></button>
          </div>
          <br><br>
          <div class="clearfix"></div>
          <ul class="paginationTable salonee_category category-card">

            <?php while($row=$countries->fetch()){ ?>
            <li class=" tableItem">
              <span><img src="<?php echo admin_base_url.$row['country_flag'];?>" width="100" alt="country"></span>
              <p>
                <b><?php echo $row['country_name'].' <custom style="font-size:14px;font-weight:600;">[ '.ucwords($row['telephone_code']).' ]</custom>';?></b>
                <?php echo $row['language'];?>
              </p>
              <small><i class="fa fa-pencil-square-o edit_country" data-id="<?php echo $row['id'];?>" data-telephone-code="<?php echo $row['telephone_code'];?>" data-country-name="<?php echo $row['country_name'];?>" data-language="<?php echo $row['language'];?>" aria-hidden="true"></i> 
              
              <?php if($row['status']==1){?>
              <i class="fa fa-eye-slash deactivatecountry " style="color:red;" title="Disable" data-id="<?php echo $row['id'];?>" aria-hidden="true"> </i>
              <?php }else{ ?>
                <i class="fa fa-eye activatecountry " style="color:green;" title="Enable" data-id="<?php echo $row['id'];?>" aria-hidden="true"> </i>
              <?php } ?>
                  
              </small>
            </li>
            <?php }?>
            
            
          </ul>
          <div class="pagination-container">
            <p class='paginacaoCursor beforePagination' id=""><i class="fa fa-chevron-left" aria-hidden="true"></i></p>
            <p class='paginacaoCursor afterPagination' id=""><i class="fa fa-chevron-right" aria-hidden="true"></i></p>
          </div>


        </div>
      </main>
      <!--  //END main -->

      <!-- <div class="layout__container-after"></div> -->

    </div>

    <!-- add category modal  -->
    <div class="modal fade pswdModal" id="addCountry" tabindex="-1" role="dialog" aria-labelledby="addCountryTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addCountryTitle">Add Country</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="<?php echo admin_base_url;?>countries.php" method="post" id="country_im" onsubmit="return validateForm()" name="country_im" enctype="multipart/form-data">
              <input type="hidden" name="type" value="addNewCountry" />
<!--               <div class="form-group mt-4 mb-5">
                <select name="country_for" id="country_for" class="form-control" autocomplete="off" required >
                  <option hidden disabled selected value> -- Select Category For -- </option>
                  <option value="women">Women</option>
                  <option value="men">Men</option>
                  <option value="children">Children</option>
                </select>
              </div> -->
                <div class="form-group mt-4 mb-5">
                  <input type="text" id="country_name" name="country_name" class="form-control" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="country_name">Country Name</label>
                </div>
                <div class="form-group mt-4 mb-5">
                  <input type="text" id="telephone_code" name="telephone_code" class="form-control" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="telephone_code">Telephone Code</label>
                </div>
                <div class="form-group mt-4 mb-5">
                  <input type="text" id="language" name="language" class="form-control" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="language">Language</label>
                </div>
                <div class="form-group mb-5">
                  <label for="file-upload" class="custom-file-upload">
                    UPLOAD COUNTRY FLAG <i class="fa fa-camera" aria-hidden="true"></i>
                  </label>
                  <input id="file-upload" name='country_flag' type="file"  style="display:none;">
                </div>
                <div class="form-group">
                  <input type="submit" id="countrySubmit" class="btn theme-btn countrySubmit" value="Submit">
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>

    <!-- add category modal  -->
    <div class="modal fade pswdModal" id="editCountry" tabindex="-1" role="dialog" aria-labelledby="addCountryTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addCountryTitle">Edit Country</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="<?php echo admin_base_url;?>countries.php" method="post" enctype="multipart/form-data">
              <input type="hidden" name="type" value="updateCountry" />
              <input type="hidden" name="country_id" id="country_id" value="" />
                <div class="form-group mt-4 mb-5">
                  <input type="text" id="country_name1" name="country_name" class="form-control" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="country_name">Country Name</label>
                </div>
                <div class="form-group mt-4 mb-5">
                  <input type="text" id="telephone_code1" name="telephone_code" class="form-control" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="telephone_code">Telephone Code</label>
                </div>
                <div class="form-group mt-4 mb-5">
                  <input type="text" id="language1" name="language" class="form-control" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="language">Language</label>
                </div>
                <div class="form-group mb-5">
                  <label for="file-upload1" class="custom-file-upload">
                    UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                  </label>
                  <input id="file-upload1" name='country_flag' type="file"  style="display:none;">
                </div>
                <div class="form-group">
                  <input type="submit" id="countrySubmit1" class="btn theme-btn countrySubmit1" value="Submit">
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>


    <?php include 'includes/footer.php';?>
    <script>
    $(".edit_country").click(function(){
      var country_id=$(this).data("id");
      var country_name=$(this).data("country-name");
      var telephone_code=$(this).data("telephone-code");
      var language=$(this).data("language");
      $("#country_id").val(country_id);
      $("#country_name1").val(country_name);
      $("#telephone_code1").val(telephone_code);
      $("#language1").val(language);
      $("#editCountry").modal('show');

    });
      $("form[name='country_im']").submit(function(e){
        if(typeof $('#file-upload')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select Country image',
          });
          e.preventDefault();
          return false;
        }
      })
      $('#file-upload1').change(function() {
        var i = $(this).prev('label').clone();
        var file = $('#file-upload1')[0].files[0].name;
        $(this).prev('label').text(file);
      });
       
  $(".deactivatecountry").click(function () {
    swal({
      title: "Are you sure?",
      text: "You want to disable this country",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, Deactivate it!",
    }).then(result => {
      if (result.dismiss === 'cancel') {
        swal.closeModal();
        return;
      }
      $.ajax({
        url:"<?php echo admin_base_url.'countries.php';?>",
        method:"POST",
        data:{type:'disablecountry',country_id:$(this).data("id")},
        success:function(resp){
          var resp=JSON.parse(resp);
          if(resp.status==1){
           location.reload();
          }
        }
      })
      
    },
    );
  });
  $(".activatecountry").click(function () {
    swal({
      title: "Are you sure?",
      text: "You want to active this country",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, Activate it!",
    }).then(result => {
      if (result.dismiss === 'cancel') {
        swal.closeModal();
        return;
      }
      $.ajax({
        url:"<?php echo admin_base_url.'countries.php';?>",
        method:"POST",
        data:{type:'enablecountry',country_id:$(this).data("id")},
        success:function(resp){
          var resp=JSON.parse(resp);
          if(resp.status==1){
           location.reload();
          }
        }
      })
    },
    );
  });
    </script>
</body>
</html>