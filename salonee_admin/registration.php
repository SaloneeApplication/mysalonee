<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Salonee | Admin Dashboard</title>
        <!-- fonts  -->
        <link
            href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap"
            rel="stylesheet">
        <link rel="stylesheet" href="./assets/bootstrap-4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="./assets/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="./assets/css/theme.css">
        <link rel="stylesheet" href="./assets/css/style.css">
    </head>
    <body>
        <!-- partial:index.partial.html -->
        <div class="layout">
        <div class="layout__container">
            <!-- BEGIN main -->
            <div class="main__content">
                <div class="w-50">
                    <h5>Service Provider Registration</h5>
                </div>
                <br><br>
                <br><br>
                <div class="clearfix"></div>
                <div class="login">
                    <form method="POST" action="add_service_provider.php" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="name">Name</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="text" name="business_name" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="name">Business Name</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="code">Email</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="mobile" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="value">Mobile</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="file-upload" class="custom-file-upload sadmnUpload">
                            UPLOAD BUSINESS LICENCE <i class="fa fa-camera" aria-hidden="true"></i>
                            </label>
                            <input id="file-upload" name='upload_file' type="file" style="display:none;">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn theme-btn">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>
        <!-- partial -->
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
        <script src="./assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>
        <!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js'></script> -->
        <!-- <script src="./assets/js/script.js"></script> -->
        <script src="./assets/js/main.js"></script>
        <script src="./assets/js/pagination.js"></script>
        <script src="./assets/js/sweetalert2.all.min.js"></script>
        <script>
            $(".trash_icon").click(function () {
                swal({
                  title: "Are you sure?",
                  text: "You want to delete this Category",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                }).then(result => {
                  if (result.dismiss === 'cancel') {
                    swal.closeModal();
                    return;
                  }
                  swal({
                    type: "success",
                    text: 'Category has been deleted!',
                    showConfirmButton: false,
                    timer: 1500
                  });
            
                },
                )
              })
        </script>
    </body>
</html>