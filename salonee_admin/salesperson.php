<?php

require_once dirname(__DIR__) . '/controller/Salesperson.php';
$sales = new Salesperson();
$sales->load();
$cities=$sales->getCities();
$persons=$sales->getSalesPersons();
$page_data=$sales->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$adminId=$page_data['admin_id'];
include "includes/header.php";
?>
            <!-- BEGIN sidebar -->
            <?php include 'includes/sidebar.php';?>
            <!--  //END sidebar -->
<style>
table.table.themeTable td, table.table.themeTable th {
  max-width:none;
}
</style>
            <!-- BEGIN main -->
                <div class="main__content">
                    <div class="w-50">
                        <div class="input-group mb-3 _hide" >
                            <input type="text" class="form-control _search" id="search_Td" placeholder="Search">
                            <div class="input-group-append">
                                <button class="btn theme-btn" type="submit">Go</button>
                            </div>
                        </div>
                    </div>
                    <div class="rightBtn">
                        <button class="btn login-btn" data-toggle="modal" data-target="#addPromoCode" id="myButton">Add &nbsp;<i
                            class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                    <div class="pad_3">
                        <div class="table-responsive">
                            <table class="table theme_Table dataTable" id="example">
                                <thead>
                                    <tr>
                                        <th width="1">S.No</th>
                                        <th width="5">Name</th>
                                        <th width="75" style="max-width:300px;">Email</th>
                                        <th width="20">Phone</th>
                                        <th width="20">Location</th>
                                        <th width="20">Reg.Date</th>
                                        <th width="20">Total.S.Reg Count</th>
                                        <th width="20">Status</th>
                                        <th width="20">Action</th>
                                    </tr>
                                </thead>
                                <tbody >
                                    <?php $i=1; foreach($persons as $per){
                                      echo '<tr>';
                                      echo '<td>'.$i.'</td>';
                                      echo '<td>'.$per['name'].'</td>';
                                      echo '<td>'.$per['email'].'</td>';
                                      echo '<td>'.$per['mobile'].'</td>';
                                      echo '<td>'.$per['city_name'].'</td>';
                                      echo '<td>'.date("Y-m-d",strtotime($per['created_time'])).'</td>';
                                      echo '<td>'.$per['total_services_providers_count'].'</td>';
                                      if($per['status']=='1'){
                                        echo '<td><label class="badge  badge-pill  badge-success">Active</label></td>';
                                        echo '<td><a href="salesperson.php?type=block_user&id='.$per['salesperson_id'].'" class="badge badge-danger"><i class="fa fa-times"></i> Block</a>';

                                      }else{
                                        echo '<td><label class="badge  badge-pill  badge-danger">Deactive</label></td>';
                                        echo '<td><a href="salesperson.php?type=active_user&id='.$per['salesperson_id'].'" class="badge badge-success"><i class="fa fa-check"></i> Activate</a>';
                                      }
                                      echo '<a href="view-salesperson.php?id='.$per['salesperson_id'].'" class="badge badge-primary"><i class="fa fa-eye"></i> View</a>';
                                      echo '</td>';
                                      echo '</tr>';
                                      $i++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <!-- pagination  -->
                        <!-- <div class='pagination-container tablePaging'>
                            <div class="pagination">
                                <ol id="numbers"></ol>
                            </div>
                        </div> -->
                    </div>
                </div>
            </main>
            <!--  //END main -->
            <!-- <div class="layout__container-after"></div> -->
        </div>
        <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Add Salesperson</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="login">
                    <form action="salesperson.php" enctype="multipart/form-data" onsubmit="return validateSales();" method="post">
                    <input type="hidden" name="type" value="addSalesPerson" />
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <input type="text" id="sales_name" class="form-control" name="name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="title">Name</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group">
                            <select class="form-control" autocomplete="off" id="location_sales" name="location" required>
                                <option value="0" selected> Select Location</option>
                                <?php foreach($cities as $cit){ 
                                  echo '<option value="'.$cit['id'].'">'.$cit['city_name_en'].'</option>';
                                }?>
                            </select>
                            <label class="form-control-placeholder p-0" for="title">Location</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="code">Email</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="mobile" id="sales_mobile" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="value">Mobile</label>
                                </div>
                            </div>
                        </div>
                      <div class="form-group">
                        <label for="file-upload" class="custom-file-upload sadmnUpload">
                          UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                        </label>
                        <input id="file-upload" name='image' type="file"  style="display:none;">
                      </div>
                         <center> <span id="sales_img" style="text-align:center;"></span></center>
                      <div class="form-group">
                        <button type="submit" class="btn theme-btn">Submit</button>
                      </div>
                    </form>
                  </div>

                </div>

              </div>
            </div>
          </div>
          
        <?php include 'includes/footer.php';?>
    </body>
    <script>
$(document).ready(function() {
    $('#example').dataTable({
      "sPaginationType": "full_numbers",
      "language": { 
            "zeroRecords": "No Saler Person found.",           
        },
        aLengthMenu: [
        [10,25, 50, 100, 200, -1],
        [10,25, 50, 100, 200, "All"]
    ],
    // iDisplayLength: -1
    });
});
// $("#search_Td").on("keyup", function () {
//           var value = $(this).val().toLowerCase();
//           $("#saloonTable tr").filter(function () {
//           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
//            
//           });
//         });
function validateSales(){
  
  var sales_mobile=$("#sales_mobile").val();
  var sales_name=$("#sales_name").val();
  var location_sales=$("#location_sales").val();
  sales_mobile=$.trim(sales_mobile);
  sales_name=$.trim(sales_name);
    if(sales_name.length==0){
        iziToast.error({
          title: 'Error',
          message: 'Please enter name',
      });
    return false;
    }
    if(sales_mobile.length==0){
      iziToast.error({
      title: 'Error',
      message: 'Please enter mobile number',
  });
  return false;
    }
    var regex = /^[a-z A-Z]*$/;
    if(!regex.test(sales_name)){
      iziToast.error({
      title: 'Error',
      message: 'Please enter valid name ',
    });
    return false;
    }
    var regex = /^[0-9]*$/;
    if(!regex.test(sales_mobile)){
      iziToast.error({
      title: 'Error',
      message: 'Please enter valid mobile number ',
    });
    return false;
    }
    if(location_sales=="0"){
      iziToast.error({
      title: 'Error',
      message: 'Please select location',
  });
  return false;
    }
  //  return true;
}
$("#file-upload").change(function(){
  var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("file-upload").files[0]);
        oFReader.onload = function (oFREvent) {
            // document.getElementById("sales_img").src = oFREvent.target.result;
            var size=document.getElementById("file-upload").files[0]['size']/1024;
            var type=document.getElementById("file-upload").files[0]['type'];
            size=Math.round(size);
            // $("#sales_img").html("<img src='"+oFREvent.target.result+"' width='100px' /> <br/> <b>Size : "+size+" KB</b> <br/> <br/>");
            $("#sales_img").html("<img src='"+oFREvent.target.result+"' width='100px' /> <br/> <span>Size : "+size+" KB</span> <br/> <span>Type : "+type+" </span> <br/> <br/>");
        };
});
    </script>
</html>