<aside class="sidebar">
	<div class="sidebar__header">
	<?php if($_SESSION['auth_type']=='admin'){ ?>
	  <a href="dashboard.php">
		<img src="./assets/img/logo1.png" alt="logo" class="admin_logo">
	  </a>
	  <?php }else{ 
		  ?>
		   <a href="sales-dashboard.php">
		<img src="./assets/img/logo1.png" alt="logo" class="admin_logo">
	  </a>
		  <?php 
	  } ?>
	</div>
	<div class="sidebar__content">
	  <ul class="sidebar__content-menu">
	<?php if($_SESSION['auth_type']=='admin'){
			$access_pages=json_decode($_SESSION['role_access']);
			if(in_array('dashboard',$access_pages)){
		?>
		<li class="<?php if(@$page == 'dashboard') { echo 'sidebar--active'; }?>"><a href="dashboard.php">Dashboard</a></li>
		<?php } 
		if(in_array('users',$access_pages)){
			 ?>

		<li class="<?php if(@$page == 'users') { echo 'sidebar--active'; }?>"><a href="users.php">Users</a></li>
		<?php } 
		if(in_array('service_provider',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'service-provider') { echo 'sidebar--active'; }?>"><a href="service-provider.php">Service Providers</a></li>
		<?php } 
		if(in_array('countries',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'countries') { echo 'sidebar--active'; }?>"><a href="countries.php">Countries</a></li>
		<?php } 
		if(in_array('city',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'city') { echo 'sidebar--active'; }?>"><a href="cities.php">Cities</a></li>
		<?php } 
		if(in_array('salespersons',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'salespersons') { echo 'sidebar--active'; }?>"><a href="salesperson.php">Salespersons</a></li>
		<?php } 
		if(in_array('employees',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'employees') { echo 'sidebar--active'; }?>"><a href="employees.php">Employees</a></li>
		<?php } 
		if(in_array('service_bookings',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'services_bookings') { echo 'sidebar--active'; }?>"><a href="service-bookings.php">Service Bookings</a></li>
		<?php } 
		if(in_array('category',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'category') { echo 'sidebar--active'; }?>"><a href="category.php">Category</a></li>
		<?php } 
		if(in_array('promo_codes',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'promocodes') { echo 'sidebar--active'; }?>"><a href="promocode.php">Promo Codes</a></li>
		<?php } 
		if(in_array('advertisement',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'advertisement') { echo 'sidebar--active'; }?>"><a href="advertisement.php">Advertisement</a></li>
		<?php } 
		if(in_array('featured_profiles',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'featured_profiles') { echo 'sidebar--active'; }?>"><a href="featured-profiles.php">Featured Profiles</a></li>
		<?php } 
		if(in_array('products',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'products') { echo 'sidebar--active'; }?>"><a href="products.php">Products</a></li>
		<?php } 
		if(in_array('orders',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'orders') { echo 'sidebar--active'; }?>"><a href="orders.php">Orders</a></li>
		<?php } 
		if(in_array('product_orders',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'product_orders') { echo 'sidebar--active'; }?>"><a href="product-orders.php">Product Orders</a></li>
		<?php } 
		if(in_array('subscription',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'subscription') { echo 'sidebar--active'; }?>"><a href="subscription.php">Subscription</a></li>
		<?php } 
		if(in_array('ratings',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'ratings') { echo 'sidebar--active'; }?>"><a href="ratings.php">Ratings</a></li>
		<?php } 
		if(in_array('services_reports',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'service_reports') { echo 'sidebar--active'; }?>"><a href="service-reports.php">Services Reports</a></li>
		<?php } 
		if(in_array('service_provider_reports',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'service_provider_reports') { echo 'sidebar--active'; }?>"><a href="service-provider-reports.php">Service Provider Reports</a></li>
		<?php } 
		if(in_array('user_reports',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'user_reports') { echo 'sidebar--active'; }?>"><a href="user-reports.php">User Reports</a></li>
		<?php } 
		if(in_array('product_reports',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'product_reports') { echo 'sidebar--active'; }?>"><a href="product-reports.php">Product Reports</a></li>
		<?php } 
		if(in_array('revenue_reports',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'revenue_reports') { echo 'sidebar--active'; }?>"><a href="revenue-reports.php">Revenue Reports</a></li>
		<?php } 
		if(in_array('support_tickets',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'support_tickets') { echo 'sidebar--active'; }?>"><a href="support-tickets.php">Support Tickets</a></li>
		<?php } 
		if(in_array('privacy_policy',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'privacy_policy') { echo 'sidebar--active'; }?>"><a href="privacy-policy.php">Privacy Policy FAQ</a></li>
		<?php }
		if(in_array('settings',$access_pages)){
			 ?>
		<li class="<?php if(@$page == 'settings') { echo 'sidebar--active'; }?>"><a href="settings.php">Settings</a></li>
		<?php } 
			 ?>
		<?php }elseif($_SESSION['auth_type']=='salesperson'){
			?>
<li class="<?php if(@$page == 'sales_person_dashboard') { echo 'sidebar--active'; }?>"><a href="sales-dashboard.php">Dashboard</a></li>
<li class="<?php if(@$page == 'service_providers') { echo 'sidebar--active'; }?>"><a href="sales-person.php">Service Providers</a></li>
		
			<?php
		} ?>
	  </ul>
	</div>
</aside>
<!-- <main class="main">

 <div class="main__header">
          <div class="main__header-heading">
            <h1>
				<?php if($_SESSION['auth_type']=='admin'){
					echo 'Administrator';
				}elseif($_SESSION['auth_type']=='salesperson'){
					echo 'Sales Person';
				} ?>
			</h1>
          </div>

          <div class="main__header-user">

            <button type="button" class="main__header-user-toggle" data-toggle="dropdown" data-toggle-for="user-menu"
              role="button">
              <span class="main__header-user-toggle-picture">
				<?php 
					if($image == "")
					{
				?>
                <img src="./assets/img/admin/avatar-male.png" alt="">
				<?php 
					}
					else
					{
						echo " <img src='".admin_base_url."$image' alt=''>";
					}
				?>
              </span>
            </button>
            <ul class="main__header-user-menu-content dropdown-menu">
              <li>
                <div class="main__header-user-menu-header ">
                  <h3><?php echo $name;?></h3>
                  <span>Role : <?php echo $role_name;?></span>
                </div>
              </li>
              <li data-toggle="modal" data-target="#myProfile"><a class="d-m" href="#"><i
                    class="icon icon--profile"></i>My Profile</a></li>
              <li data-toggle="modal" data-target="#changePassword"><a class="d-m" href="#"><i
                    class="icon icon--help"></i>Change Password</a></li>
              <li><a class="d-m" href="signOut.php"><i class="icon icon--sign-out"></i>Sign Out</a></li>
            </ul>
          </div>
        </div> -->