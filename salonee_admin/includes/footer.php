<!-- change password modal  -->
    <div class="modal fade pswdModal" id="changePassword" tabindex="-1" role="dialog"
      aria-labelledby="changePasswordTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="changePasswordTitle">Change Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form method = "post" action = "<?php echo admin_base_url.'ajax.php';?>">
              <input type="hidden" name="type" value="changePassword" />
                <div class="form-group mt-4 ">
                  <input type="password" id="op" class="form-control" name = "opass" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="op">Old Password</label>
                </div>

                <div class="form-group ">
                  <input type="password" id="np" class="form-control" name = "npass" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="np">New Password</label>
                </div>

                <div class="form-group mb-5">
                  <input type="password" id="cp" class="form-control" name = "n1pass" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="cp">Confirm Password</label>
                </div>


                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Submit</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>

    <!-- my profile modal  -->
    <div class="modal fade pswdModal" id="myProfile" tabindex="-1" role="dialog" aria-labelledby="myProfileTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <!-- profile information -->
        <div class="modal-content"  id="profileInfo">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="myProfileTitle">Profile</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="profile_details">
                <div class="pro_img">
				<?php 
					if($image == ""){
				?>
            <img src="./assets/img/admin/avatar-male.png" alt="avatar-male">
				  <?php 
					}
					else
					{
						echo "<img src='".admin_base_url."$image' alt='avatar-male'>";
					}
				  ?>
                </div>
                <ul>
                <li>
                    <span>Name</span>
                    <p><?php echo $name;?></p>
                  </li>
                  <li>
                    <span>City Name</span>
                    <p><?php echo $city_name;?></p>
                  </li>
                  <li>
                    <span>Email Id</span>
                    <p><?php echo $email;?></p>
                  </li>
                  <li>
                    <span>Mobile Number</span>
                    <p><?php echo $mobile;?></p>
                  </li>
                  
                </ul>
               
              </div>

            <div class="login">
              <form>
                <div class="form-group">
                  <button type="button" class="btn theme-btn" onclick="showEditProfile()">Edit Profile</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <!-- edit profile fields -->
          <div class="modal-content" id="editPrfile">
            <div class="modal-header">
              <h5 class="col-12 modal-title text-center" id="editProfileTitle"> Edit Profile</h5>
              <button  onclick="closeEdit()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="">
              </div>
			   <form method = "post" action = "<?php echo admin_base_url.'ajax.php';?>" enctype = "multipart/form-data">
         <input type="hidden" name="type" value="updateProfile" />
              <div class="picture-container">
                <div class="picture">
					<?php
						if($image == "")
						{
							echo "<img src='./assets/img/admin/avatar-male.png' class='picture-src' id='wizardPicturePreview' title=''>";
						}
						else
						{
							echo "<img src='".admin_base_url."$image' class='picture-src' id='wizardPicturePreview' title=''>";
						}
                    
					
					?>
                    <input type="file" id="wizard-picture" name = "filUpload111" class="" >
                    <i class="fa fa-camera" aria-hidden="true"></i>
                </div>
                 <!-- <h6 class="">Choose Picture</h6> -->
                </div>
           
              
              <div class="login">
				  <input type = "hidden" id = "hidImage" name = "hidImage1" value = "<?php echo $image;?>" />
                  <div class="form-group mt-4 ">
                    <input type="text" id="name"name = "name"  class="form-control" autocomplete="off" value = "<?php echo $name;?>" required>
                    <label class="form-control-placeholder p-0" for="name">Name</label>
                  </div>
  
                  <div class="form-group">
                    <input type="text" id="email" name = "email" class="form-control" autocomplete="off" value = "<?php echo $email;?>" required >
                    <label class="form-control-placeholder p-0" for="name">Email Id</label>
                  </div>
  
                  <div class="form-group">
                    <input type="text" id="mn" name = "mobile" class="form-control" autocomplete="off" value = "<?php echo $mobile;?>" required>
                    <label class="form-control-placeholder p-0" for="mn">Mobile Number</label>
                  </div>

                  
                  <div class="form-group">
                    <button type="submit" class="btn theme-btn">Save</button>
                  </div>
               
              </div>
			 </form>
            </div>
          </div>


      </div>
    </div>


    <!-- partial -->
    <script src="<?php echo admin_base_url;?>assets/bootstrap-4.3.1/js/popper.min.js"></script>
    <script src='<?php echo admin_base_url;?>assets/js/jquery-3.3.1.min.js'></script>
    <script src="<?php echo admin_base_url;?>assets/js/sweetalert2.all.min.js"></script>
    <script src="<?php echo admin_base_url;?>assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>
    <script src="<?php echo admin_base_url;?>assets/js/main.js"></script>
    <script src="<?php echo admin_base_url;?>assets/js/pagination.js"></script>
    <script src="<?php echo admin_base_url;?>assets/js/flash.min.js"></script>
	 <script src="<?php echo admin_base_url;?>assets/js/bootstrap-select.min.js"></script>
	 <script src="<?php echo admin_base_url;?>assets/js/jquery.validate.js"></script>
	 <script src="<?php echo base_url;?>vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo admin_base_url;?>assets/js/popper.js"></script>
  <script src="<?php echo admin_base_url;?>assets/js/main1.js"></script>

  <script src='<?php echo admin_base_url;?>assets/js/Chart.min.js'></script>
<script>
  $(document).ready(function(){
    $("#wizard-picture").change(function(){
        readURL(this);
    });
});
    $(document).on('click', '#success', function (e) {
        swal({
          type: "success",
          text: 'Location Added Successfully',
          showConfirmButton: false,
          timer: 1500
        })
      });
      var y = document.getElementById("editPrfile");
      var x = document.getElementById("profileInfo");
      y.style.display = "none";
      function showEditProfile() {
        if (x.style.display === "none") {
          x.style.display = "block";
          y.style.display = "none";
        } else {
          x.style.display = "none";
          y.style.display = "block";
        }
      }
      function closeEdit(){
        y.style.display = "none";
        x.style.display = "block";
      }
      $(document).ready(function () {
        $("#searchTd").on("keyup", function () {
          var value = $(this).val().toLowerCase();
          $("#saloonTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
        $("#category_search").on("keyup", function () {
          var value = $(this).val().toLowerCase();
          $(".salonee_category li p b").filter(function () {
            $(this).parent().parent().toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
        // swal 
        $(".trash_icon").click(function () {
          swal({
            title: "Are you sure?",
            text: "You want to delete this Account",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
          }).then(result => {
            if (result.dismiss === 'cancel') {
              swal.closeModal();
              return;
            }
            swal({
              type: "success",
              text: 'Account has been deleted!',
              showConfirmButton: false,
              timer: 1500
            });

          },
          )
        })
        // Prepare the preview for profile picture
        $("#wizard-picture").change(function () {
          readURL(this);
        });
      });
      function readURL(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
          }
          reader.readAsDataURL(input.files[0]);
        }
      }

     

      // EDIT PROFILE FORM VALIDATIONS 
      $("#editProfileForm").validate({
        // Specify the validation rules
        rules: {
          FULLNAME: {
            required: true,
            minlength: 3,
            maxlength: 50,
          },
          SIGNUPEMAIL: {
            required: true,
            email: true,
          },
          MOBILE: {
            required: true,
            number: true,
            minlength: 10,
            maxlength: 15,
          },
          SIGNPASSWORD: {
            required: true,
            minlength: 8,
            maxlength: 15,
          },
          SERVICE: {
            required: true,
          },
          LOCATION: {
            required: true,
          },
          ACCNTNUM: {
            required: true,
          },
          IFSC: {
            required: true,
          }
        },
        // Specify the validation error messages
        messages: {
          FULLNAME: {
            required: "Please Enter Full Name",
            minlength: "Full Name must be minimum 3 characters",
            maxlength: "Maximum 50 characters only"
          },
          SIGNUPEMAIL: {
            required: "Please Enter Email Address",
            email: "Please Enter a Valid Email Address",
          },

          MOBILE: {
            required: "Please Enter Mobile Number",
            number: "Please Enter Digits Only",
            minlength: "Minimum 10 Digits required",
            maxlength: "Maximum 15 Digits only"
          },
          SIGNPASSWORD: {
            required: "Please Enter Password",
            minlength: "Password must be minimum 8 characters",
            maxlength: "Maximum 15 characters only"
          },
          SERVICE: {
            required: "Please Select Service",
          },
          LOCATION: {
            required: "Please Enter Location",
          },
          ACCNTNUM: {
            required: "Please Enter Account Number",
          },
          IFSC: {
            required: "Please Enter IFSC",
          }

        }

      });
      // CHANGE PASSWORD FORM VALIDATIONS 
      $("#changePswdForm").validate({
        // Specify the validation rules
        rules: {
          oldPswd: {
            required: true,
            minlength: 8,
            maxlength: 15,
          },
          newPswd: {
            required: true,
            minlength: 8,
            maxlength: 15,
          },
          cnfrmPswd: {
            required: true,
            minlength: 8,
            maxlength: 15,
          },
        },
        // Specify the validation error messages
        messages: {
          oldPswd: {
            required: "Please Enter Password",
            minlength: "Password must be minimum 8 characters",
            maxlength: "Maximum 15 characters only"
          },
          newPswd: {
            required: "Please Enter Password",
            minlength: "Password must be minimum 8 characters",
            maxlength: "Maximum 15 characters only"
          },
          cnfrmPswd: {
            required: "Please Enter Password",
            minlength: "Password must be minimum 8 characters",
            maxlength: "Maximum 15 characters only"
          },
        }

      });
    </script>
	<script>
	iziToast.settings({
      timeout: 3000, // default timeout
      resetOnHover: true,
      // icon: '', // icon class
      transitionIn: 'flipInX',
      transitionOut: 'flipOutX',
      position: 'topRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
      onOpen: function () {
        console.log('callback abriu!');
      },
      onClose: function () {
        console.log("callback fechou!");
      }
    });

	<?php 
		if(isset($_SESSION['success']))
		{
	?>
			iziToast.success({timeout: 5000, title: 'Success!', message: '<?php echo $success;?>'});
	<?php
		unset($_SESSION['success']);
		}
	?>
	
	<?php 
		if(isset($_SESSION['failure']))
		{
	?>
			iziToast.error({title: 'Error', message: '<?php echo $failure;?>'});
	<?php
		unset($_SESSION['failure']);
		}
	?>
	
	<?php 
		if(isset($_SESSION['message']))
		{
	?>
			iziToast.error({title: 'Error', message: '<?php echo $message;?>'});
	<?php
		unset($_SESSION['failure']);
		}
	?>
	
</script>
<script>
      $(function () {
        $('.selectpicker').selectpicker();
      });
      <?php  if(@$_SESSION['alert_type']!=''){ ?>
          swal({
            type: "<?php echo $_SESSION['alert_type']; ?>",
            text: '<?php echo $_SESSION['alert_msg']; ?>',
            showConfirmButton: false,
            timer: 3000
          });
          <?php 
           $_SESSION['alert_type']='';
           $_SESSION['alert_msg']='';
      }?>
</script>
