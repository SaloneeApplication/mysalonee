<?php 
	$adminId=$page_data['admin_id'];
	if(isset($_SESSION['success']))
	{
		$success = $_SESSION['success'];		
	}
	if(isset($_SESSION['failure']))
	{
		$failure = $_SESSION['failure'];	
	}
	include("class/dbConnection.php");
	include("class/admin.php");
	$dbObject = new dbConnection();
	$con = $dbObject->getConnection();
	if($con)
	{
		$role_name="Admin";
		$role_access=array();
		$adminObj = new admin();
		if($_SESSION['auth_type']=='admin'){
			$role_name="Admin";
			$res = $adminObj->getAdminDetails($adminId,$con);
			while($row = mysqli_fetch_array($res))
			{
				$image = $row['image'];
				$name = $row['name'];
				$email = $row['email'];
				$mobile = $row['mobile'];
				$role_name = $row['role_name'];
				$role_access = $row['role_access'];
			}
			$role_access=json_decode($role_access);
		}elseif($_SESSION['auth_type']=='salesperson'){
			$role_name="Sales Person";
			$res = $adminObj->getadminEmployeeDetails($adminId,$con);
			while($row = mysqli_fetch_array($res))
			{
				$image = $row['image'];
				$name = $row['name'];
				$email = $row['email'];
				$city_name = $row['city_name'];
				$mobile = $row['mobile'];
			}
		}
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $title;?></title>
  <link rel="stylesheet" href="<?php echo admin_base_url;?>assets/css/bootstrap-select.min.css">
  <!-- fonts  -->
  <link
    href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap"
    rel="stylesheet">
 
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
  
  <link rel="stylesheet" href="<?php echo admin_base_url;?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url;?>vendor/datatables/jquery.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo admin_base_url;?>assets/bootstrap-4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo admin_base_url;?>assets/css/theme.css">
  <link rel="stylesheet" href="<?php echo admin_base_url;?>assets/css/style.css">
  <?php if(strpos($_SERVER['PHP_SELF'],'salonee_admin/dashboard.php')!==false){
?>
<link rel="stylesheet" href="<?php echo admin_base_url;?>assets/css/style1.css">
<?php 
  }
  ?>
	
  <link rel="stylesheet" href="<?php echo admin_base_url;?>assets/css/flash.min.css">

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
  <style>
    .bootstrap-select .bs-ok-default::after {
      width: 0.3em;
      height: 0.6em;
      border-width: 0 0.1em 0.1em 0;
      transform: rotate(45deg) translateY(0.5rem);
    }

    .btn.dropdown-toggle:focus {
      outline: none !important;
    }
  </style>
</head>

<body>

<div class="layout">
<div class="layout__container">