<?php 
	session_start();
	
	//Included Files
	
	include("class/admin.php");	
	include("class/dbConnection.php");


	//Retrieving Session Variable
	if(isset($_SESSION["adminId"]))
	{
		$adminId = $_SESSION["adminId"];
		//echo $adminId;
	}
	else
	{
		header('Location:index.php?message=Unauthorized%20Access%20Prohobited');
	}
	
	//Retrieving Form Fields
	//Creating dbObject 
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	if($con)
	{
		$service_provider_id = $_POST['id'];
		$serviceProviderObj = new admin();

		if($_POST['type'] == 1)
		{
			$res = $serviceProviderObj->approve_service_provider($service_provider_id,$con);
			if($res)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			$res = $serviceProviderObj->reject_service_provider($service_provider_id,$con);
			if($res)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
	

?>