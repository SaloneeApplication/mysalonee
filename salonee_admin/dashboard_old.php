<?php
require_once dirname(__DIR__) . '/controller/AdminDashboard.php';
$controller=new AdminDashboard();
$controller->load();
$page_data=$controller->pageData();
$title = $page_data['title']; $page = $page_data['page'];
$adminId=$page_data['admin_id'];
$products_chart=$page_data['products_chart'];
$slots_chart=$page_data['slots_chart'];
include("includes/header.php"); 
if($_SESSION['auth_type']=='salesperson'){
    header("Location:sales-person.php");
    exit;
}
?>
<!-- BEGIN sidebar -->
<?php include('includes/sidebar.php');?>
<!--  //END sidebar -->
        <div class="main__content">
          <div class="w-50">
            <!-- <div class="input-group mb-3">
              <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
              <div class="input-group-append">
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div> -->
          </div>


            <div class="table-responsive">
              
<div class="displayCards">
             <ul>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Total Slots Booked</p>
                   <h3>90%</h3>
                 </div>
               </li>
               <li>
                <span>
				<i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                </span>
                <div>
                  <p>Today Slots</p>
                  <h3>0</h3>
                </div>
              </li>
			  <li>
                <span>
				<i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                </span>
                <div>
                  <p>Upcoming Slots</p>
                  <h3>0</h3>
                </div>
              </li>
			  <li>
                <span>
				<i class="fa fa-credit-card fa-2x" style="color:white;"></i>
                </span>
                <div>
                  <p>Wallet</p>
                  <h3>SAR 1000</h3>
                </div>
              </li>
			</ul>
      <center> <canvas id="line-chart"  style="max-width:900px !important;" ></canvas></center>
			
			</div>
            <div class="clearfix"></div>

            <!-- pagination  -->
            <div class='pagination-container tablePaging'>
              <div class="pagination">
                <ol id="numbers"></ol>
              </div>
            </div>
          </div>
        </div>
      </main>
      <!--  //END main -->

      <!-- <div class="layout__container-after"></div> -->
      </div>
<?php include 'includes/footer.php';?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
</body>

<script>
new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: <?php echo json_encode($slots_chart['dates']);?>,
    datasets: [{ 
        data: <?php echo json_encode($slots_chart['amount']);?>,
        label: "Slots Txns",
        borderColor: "#8e5ea2",
        fill: false
      }, { 
        data: <?php echo json_encode($products_chart['amount']);?>,
        label: "Products Txns",
        borderColor: "#3cba9f",
        fill: false
      }, { 
        data: <?php echo json_encode($slots_chart['admin_commission']);?>,
        label: "Slots Commission",
        borderColor: "#e8c3b9",
        fill: false
      }
      , { 
        data: <?php echo json_encode($products_chart['admin_commission']);?>,
        label: "Products Commissions",
        borderColor: "#c45850",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Transactions  ( Last 10 days ) AED'
    }
  }
});

</script>
</html>