<?php
	session_start();
	
	//Retrieving Session Variable
	
	if(isset($_SESSION["adminId"]))
	{
		$adminId = $_SESSION["adminId"];
			
		unset($_SESSION["adminId"]);
		session_destroy();
		session_start();
		$_SESSION['success'] = 'Signed Out Successfully';
		header('Location:index.php');
	}
	else
	{
		$_SESSION['message'] = 'Unauthorized Access Prohibited';
		header('Location:index.php');
	}
?>