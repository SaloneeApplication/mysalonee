<?php
require_once dirname(__DIR__) . '/controller/UserPayouts.php';
$controller = new UserPayouts();
$controller->init();
$page_data=$controller->pageData();
$balance=$controller->getBalance();
$bank=$controller->getBankDetails();
$withdrawals=$controller->getWithdrawalsData();
$member_access=$controller->getMemberAccess();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
include("includes/header.php");
?>

	<?php include("includes/sidebar.php");?>
  <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <div class="main__content">
          <div class="col-md-12">
            <div class="w-50">
              <div class="input-group mb-3 _hide">
                <input type="text" class="form-control _search"  id="searchTd" placeholder="Search">
                <div class="input-group-append">
                  <button class="btn theme-btn" type="submit">Go</button>
                </div>
              </div>
            </div>
            <div class="pull-right rightBtn">
              <button class="btn login-btn" data-toggle="modal"  data-target="#addDiscount"  id="myButton">Early Request &nbsp;<i
                  class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
            <div class="pull-right rightBtn">
              <button class="btn login-btn" data-toggle="modal" data-target="#editBank" id="myButton">Update Bank &nbsp;<i
                  class="fa fa-bank" aria-hidden="true"></i></button>
            </div>
          </div>
<div class="pad_3">  
          <div class="table-responsive">

            <table class="table themeTable" >
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Amount</th>
                  <th>Date</th>
                  <th>Bank Details</th>
                  <th>Status</th>
                  <th>Approve/Reject Date</th>
                  <th>Remark</th>
                </tr>
              </thead>
              <tbody id="saloonTable"> 
              <?php $i=1; foreach($withdrawals as $with){?>
                <tr>
                    <td><?php echo $i++;?></td>
                    <td> <?php echo $with['currency'].' '.$with['request_amount'];?></td>
                    <td><?php echo date("Y-m-d",strtotime($with['created_time']));?></td>
                    <td><?php 
                    echo "<strong>Bank Name : </strong>".$with['bank_name']."<br/>";
                    echo "<strong>Account Number :</strong> ".$with['account_number']."<br/>";
                    echo "<strong>Account Name :</strong> ".$with['account_name']."<br/>";
                    echo "<strong>IFSC Code : </strong>".$with['ifsc_code']."<br/>";
                    ?></td>
                    <td><?php
                    switch($with['status']){
                      case "approved":
                        echo '<label class="badge badge-pill badge-success">Approved</label>';
                        break;
                      case "rejected":
                        echo '<label class="badge badge-pill badge-danger">Rejected</label>';
                        break;
                      case "pending":
                        echo '<label class="badge badge-pill badge-warning">Pending</label>';
                        break;
                      default:
                        echo '<label class="badge badge-pill badge-primary">In Process</label>';
                      break;
                    }
                     ?></td>
                    <td><?php 
                      switch($with['status']){
                        case "approved":
                          echo $with['approved_date'];
                          break;
                        case "rejected":
                          echo $with['rejected_date'];
                          break;
                        default:
                          echo '';
                        break;
                      }
                    ?></td>
                    <td><?php echo $with['remark'];?></td>
                </tr>
                <?php }?>
              </tbody>
            </table>

          </div>
          <div class="clearfix"></div>
            <div class='pagination-container tablePaging _hide'>
                  <div class="pagination">
                    <ol id="numbers"></ol>
                  </div>
            </div>
     </div>
</div>

    <!-- partial -->
    <div class="modal fade pswdModal" id="addDiscount" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Payout Request<br/> <span style="font-size:15px;color:red;"> [ Wallet AED <?php echo $balance;?> ]</span></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post"> 
              <input type="hidden" name="type" value="withdrawRequest" />
               
                <div class="row mb-5">
                  <div class="col-md-12">

                    <!-- <div class="form-group">
                      <input type="text" id="amount" min="100" max="<?php echo $balance;?>" name="amount" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="amount">Withdraw Amount</label>
                    </div> -->

                    <div class="form-group">
                      <input type="text" id="bank_name" value="<?php echo @$bank['bank_name'];?>" readonly name="bank_name" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="bank_name">Bank Name</label>
                    </div>

                    <div class="form-group">
                      <input type="number" id="account_number" value="<?php echo @$bank['account_number'];?>" readonly name="account_number" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="account_number">Bank Account Number</label>
                    </div>

                    <div class="form-group">
                      <input type="text" id="account_name" value="<?php echo @$bank['account_name'];?>" readonly  name="account_name" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="account_name">Bank Account Name</label>
                    </div>

                    <div class="form-group">
                      <input type="text" id="ifsc" name="ifsc"  value="<?php echo @$bank['ifsc_code'];?>" readonly  class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="ifsc">IFSC Code</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Confirm </button>
                </div>
              </form>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- partial -->

    
    <!-- bank details update -->
    <div class="modal fade pswdModal" id="editBank" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Bank Details </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post"> 
              <input type="hidden" name="type" value="bankDetails" />
                <div class="row mb-5">
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="text" id="bank_name" name="bank_name" class="form-control" value="<?php echo @$bank['bank_name'];?>" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="bank_name">Bank Name</label>
                    </div>
                    <div class="form-group">
                      <input type="number" id="account_number" value="<?php echo @$bank['account_number'];?>"  name="account_number" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="account_number">Bank Account Number</label>
                    </div>

                    <div class="form-group">
                      <input type="text" id="account_name" value="<?php echo @$bank['account_name'];?>"  name="account_name" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="account_name">Bank Account Name</label>
                    </div>

                    <div class="form-group">
                      <input type="text" id="ifsc" name="ifsc" value="<?php echo @$bank['ifsc_code'];?>"  class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="ifsc">IFSC Code</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Submit</button>
                </div>
              </form>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- partial -->
<?php include("includes/footer.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script>
$(".edit_service").click(function(){
  var id=$(this).data("id");
  var base_price=$(this).data("base_price");
  var sale_price=$(this).data("sale_price");

  var price_type=$(this).data("price_type");
  var sp_price=$(this).data("sp_price");
  var special_data=$(this).data("special_data");
  
  $("#salonee_price_type").val(price_type);
  $("#salonee_special_date_tag").hide();
  $("#salonee_weekend_name").hide();
  $("#salonee_special_price_tag").hide();
  
  if(price_type=='custom_price_weekend'){
  $("#salonee_special_price_tag").show();
    $("#salonee_weekend_name").show();
    special_data=special_data.toLowerCase();
    special_data=special_data.split(",");
    $("#salonee_weekend_name_value").val(special_data)
    $("#salonee_special_price").val(sp_price)
  }else if(price_type=='custom_price_special_date'){
    $("#salonee_special_date_tag").show();
   $("#salonee_special_price_tag").show();
    $("#salonee_special_date").val(special_data);
    $("#salonee_special_price").val(sp_price)
    $('#salonee_special_date').datepicker('update');
  }else{
    $("#salonee_weekend_name_value").val('')
    $("#salonee_special_price").val('')
  }
  
  $("#dPrice").val(sale_price);
  $("#price").val(base_price);
  $("#sp_id").val(id);
  $("#addDiscount").modal('show');
});
$("#salonee_price_type").change(function(){
  $("#salonee_special_date_tag").hide();
  $("#salonee_weekend_name").hide();
  $("#salonee_special_price_tag").hide();
  if($(this).val()=='custom_price_special_date'){
    $("#salonee_special_date_tag").show();
  }else if($(this).val()=='custom_price_weekend'){
    $("#salonee_weekend_name").show();
  }
});
$(document).ready(function(){
        $("#searchTd").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#saloonTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
          $('.table').dataTable({
          "pageLength": 6
        });
      }); 
      $('#salonee_special_date').datepicker({
        multidate: true,
        format: 'yyyy-mm-dd'
      });
</script>