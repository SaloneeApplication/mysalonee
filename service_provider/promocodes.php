<?php
require_once dirname(__DIR__) . '/controller/UserPromocodes.php';
$controller = new UserPromocodes();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$page = $page_data['page']; 
$promocodes=$controller->getPromocodes();
$member_access=$controller->getMemberAccess();

include("includes/header.php");
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	if($con)
	{
		$serviceProviderObj = new serviceProvider();
		
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>

	<?php include("includes/sidebar.php");?>
    <style>input[type=date]:required:invalid::-webkit-datetime-edit {
    color: transparent;
}
input[type=date]:focus::-webkit-datetime-edit {
    color: black !important;
}
    </style>
    <div class="main__content">
        <div class="col-md-12">
          <div class="w-50">
            <div class="input-group mb-3">
              <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
              <div class="input-group-append" >
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
          </div>
          <div class="pull-right rightBtn">
            <button class="btn login-btn" data-toggle="modal"  <?php echo ($member_access=='1')?'data-target="#addPromoCode"':'onclick="window.location.href=\'upgrade.php\'"';?> id="myButton">Add &nbsp;<i
                class="fa fa-plus" aria-hidden="true"></i></button>
          </div>
        </div>
          <br>
          <div class="clearfix"></div>

          <ul class="paginationTable salonee_category promoCode">
          <?php foreach($promocodes as $promo){ ?>
            <li class=" tableItem">
                <div>
                    <span><img src="<?php echo user_base_url.$promo['image'];?>" alt="gift"></span>
                    <p class="promo-code">Use Code: <strong><?php echo $promo['coupon_code'];?></strong></p>
                    <p><?php echo $promo['short_desc'];?></p>
                </div>
              <div class="footr">
                <div>
                Exp Date: <b><?php echo ($promo['expiry_date']!='0000-00-00')?$promo['expiry_date']:'No Expiry';?></b>
                </div>
                <b>
                <!-- <i class="fa fa-pencil-square-o" aria-hidden="true"></i> -->
                 <i onclick="window.location.href='<?php echo user_base_url.'promocodes.php?id='.$promo['id'];?>&type=deletePromo';" class="fa fa-trash" aria-hidden="true"></i> </b>
              </div>              
            </li>

            <?php  } ?>
            


          </ul>

          <div class="pagination-container">
            <p class='paginacaoCursor beforePagination' id=""><i class="fa fa-chevron-left" aria-hidden="true"></i></p>
            <p class='paginacaoCursor afterPagination' id=""><i class="fa fa-chevron-right" aria-hidden="true"></i></p>
          </div>


        </div>

<!-- partial -->
  <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Add Promo Code</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="login">
            <form action="" method="post" enctype="multipart/form-data" name="categor_im">
            <input type="hidden" name="type" value="insertPromocode" />
              <div class="form-group">
                <input type="text" id="code" name="code" class="form-control" autocomplete="off" required>
                <label class="form-control-placeholder p-0" for="code">Code</label>
              </div>
              <div class="form-group">
                <textarea type="text" id="desc" name="desc" rows="5" class="form-control" autocomplete="off" required></textarea>
                <label class="form-control-placeholder p-0" for="accountNumber">Code Description</label>
              </div>
              <div class="form-group">
                <input type="text" name="discount" id="discount" class="form-control" autocomplete="off" required>
                <label class="form-control-placeholder p-0" for="discount">Discount Percentage</label>
              </div>
              <div class="form-group"  >
                <select name="only_for" id="only_for" required  class="form-control" autocomplete="off" >
                  <option hidden disabled selected value> -- Select Only For-- </option>
                  <option value="new_users">For New Users</option>
                  <option value="all">For All</option>
                </select>
              </div>
              <div class="form-group">
                <input type="date" name="expdate" id="expdate" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="expdate">Expiry Date( for empty no expiry )</label>
              </div>
              <div class="form-group">
                <label for="file-upload" class="custom-file-upload">
                  UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                </label>
                <input id="file-upload" name='upload_cont_img' type="file" style="display:none;">
              </div>

              <div class="form-group">
                <button type="submit" class="btn theme-btn">Submit</button>
              </div>
            </form>
          </div>

        </div>

      </div>
    </div>
  </div>


  <!-- partial -->		
<?php include("includes/footer.php");?>

<script>
    $("#searchTd").on("keyup", function() {
         value = $(this).val().toLowerCase(); //alert(value);
        jQuery(".salonee_category li").each(function () {
        if (jQuery(this).text().search(new RegExp(value, "i")) < 0) {
            jQuery(this).hide();
        } else {
            jQuery(this).show()
        }
    });
         
        });
        
        var filter = jQuery(this).val();
    
         
     $("form[name='categor_im']").submit(function(e){
        if(typeof $('#file-upload')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select category image',
          });
          e.preventDefault();
          return false;
        }
      });
</script>
