<?php
require_once dirname(__DIR__) . '/controller/UserFeaturedProfile.php';
$controller = new UserFeaturedProfile();
$controller->init();
$page_data=$controller->pageData();
$profiles=$controller->getFeaturedProfiles();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
include("includes/header.php");
$per_day=$controller->getOption("featured_profile_plan_per_day");
$commission=$controller->getOption("featured_profiles_admin_commission_per_day");
$member_access=$controller->getMemberAccess();
?>

	<?php include("includes/sidebar.php");?>
  <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <div class="main__content">
          <div class="col-md-12">
            <div class="w-50">
             <div class="input-group mb-3 _hide">
                <input type="text" class="form-control _search"  id="searchTd" placeholder="Search">
                <div class="input-group-append">
                  <button class="btn theme-btn" type="submit">Go</button>
                </div>
              </div>
            </div>
            <div class="pull-right rightBtn">
              <button class="btn login-btn" data-toggle="modal"   <?php echo ($member_access=='1')?'data-target="#addDiscount"':'onclick="window.location.href=\'upgrade.php\'"';?> id="myButton">Add &nbsp;<i
                  class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
          </div>

<div class="pad_3">  
          <div class="table-responsive">

            <table class="table themeTable" >
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Days Left</th>
                  <th>Clicks</th>
                  <th>Order ID</th>
                  <th>Last Active</th>
                  <th>Created At</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="saloonTable"> 

              <?php $i=1; foreach($profiles as $pro){?>
                <tr>
                    <td><?php echo $i++;?></td>
                    <td><?php echo $pro['days'];?></td>
                    <td><?php echo $pro['clicks'];?></td>
                    <td><?php echo $pro['order_id'];?></td>
                    <td><?php echo $pro['last_used_date'];?></td>
                    <td><?php echo $pro['created_time'];?></td>
                    <td><?php 
                    if($pro['days']>0){
                      if($pro['status']=='active'){
                        echo '<label class="badge badge-pill badge-success"><i class="fa fa-check-circle"></i> Active</label>';
                      }elseif($pro['status']=='expired'){
                        echo '<label class="badge badge-pill badge-danger"><i class="fa fa-clock-o"></i> Expired</label>';
                      }else{
                        echo '<label class="badge badge-pill badge-warning"><i class="fa fa-exclamation-circle"></i> Inactive</label>';
                      }
                    }else{
                        if($pro['days']==0 && $pro['last_used_date']==date("Y-m-d")){
                            if($pro['status']=='active'){
                              echo '<label class="badge badge-pill badge-success"><i class="fa fa-check-circle"></i> Active</label>';
                            }else{
                              echo '<label class="badge badge-pill badge-warning"><i class="fa fa-exclamation-circle"></i> Inactive</label>';
                            }
                        }else{
                          echo '<label class="badge badge-pill badge-danger"><i class="fa fa-clock-o"></i> Expired</label>';
                        }
                    }
                    ?></td>
                    <td><?php 
                        if($pro['days']>0){
                          if($pro['status']=='active'){
                            echo '<a href="'.user_base_url.'featured_profile.php?type=disable&id='.$pro['featured_id'].'" class="badge badge-warning" ><i class="fa fa-exclamation-circle"></i> Disable</a>';
                          }else{
                            echo '<a href="'.user_base_url.'featured_profile.php?type=activateFeature&id='.$pro['featured_id'].'" class="badge badge-success" ><i class="fa fa-check"></i> Activate</a>';
                          }
                        }
                    ?></td>
                </tr>
                <?php } ?>

              </tbody>
            </table>

          </div>
          <div class="clearfix"></div>
            <div class='pagination-container tablePaging _hide'>
                  <div class="pagination">
                    <ol id="numbers"></ol>
                  </div>
            </div>
     </div>
</div>
    <!-- partial -->
    <div class="modal fade pswdModal" id="addDiscount" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Purchase Featured Profile
            <br/>
            
            <span style="font-size:13px;color:black;">Per Day Cost :  <span style="color:red;">AED <?php echo $per_day;?></span></span></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" id="payment_feature" method="post" > 
              <input type="hidden" name="type" value="purchaseFeaturedProfile" />
                <div class="row mb-5">
                  <div class="col-md-12">
                    <div class="form-group ">
                      <input type="number" min="1"  id="days" name="days" value="1" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="days">How many days required ?</label>
                    </div>
                  </div>
                </div>
                <div class="row mb-5">
                  <div class="col-md-12">
                    <div class="form-group" id="info_data">
                    <div>
                      <div class="lables-flex">
                        <div><b>Sub Total</b></div>
                        <div><b>Admin Commission</b></div>
                        <div><b>Total Payable</b></div>
                     </div>
                     <div class="lables-flex1">
                        <div id="per_day_show">AED <?php echo $per_day;?></div>
                        <div id="commission_show">AED <?php echo $commission;?></div>
                        <div id="total_show">AED <?php echo $per_day+$commission;?></div>
                    </div>
                  </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Pay Now </button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->
<?php include("includes/footer.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script>
const per_day="<?php echo $per_day;?>";
const commission="<?php echo $commission;?>";
$(document).ready(function(){
        $("#searchTd").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#saloonTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });

        $('.table').dataTable({
          "pageLength": 6
        });
      }); 
      $('#salonee_special_date').datepicker({
        multidate: true,
        format: 'yyyy-mm-dd'
      });
      function validateForm(){
        return false;
      }
$("#payment_feature").submit(function(){
  var days =$("#days").val();
  swal({
    title: 'Redirect to Payment Gateway',
    text: 'Payment in processs...',
    showCancelButton: false,
    showConfirmButton: false
  });
  setTime(function(){
    return true;
  },3000);
});
$(document).on('change keyup',"#days",function(){
  var days =$(this).val();
  var total=parseInt(per_day)*parseInt(days);
  var commission1=parseInt(commission)*parseInt(days);
  var total_ov=parseInt(total)+parseInt(commission1);
  $("#per_day_show").html("AED "+total);
  $("#commission_show").html("AED "+commission1);
  $("#total_show").html("AED "+total_ov);

});
</script>
