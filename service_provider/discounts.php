<?php
require_once dirname(__DIR__) . '/controller/UserDiscounts.php';
$controller = new UserDiscounts();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$getServicesPrices=$controller->getServicesPrices();
include("includes/header.php");
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	if($con)
	{
		$serviceProviderObj = new serviceProvider();
		
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>

	<?php include("includes/sidebar.php");?>
  <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <div class="main__content">
          <div class="col-md-12">
            <div class="w-50">
              <div class="input-group mb-3 _hide">
                <input type="text" class="form-control _search"  id="searchTd" placeholder="Search">
                <div class="input-group-append">
                  <button class="btn theme-btn" type="submit">Go</button>
                </div>
              </div>
            </div>
            <div class="pull-right rightBtn">
              <!-- <button class="btn login-btn" data-toggle="modal" data-target="#addDiscount" id="myButton">Add &nbsp;<i
                  class="fa fa-plus" aria-hidden="true"></i></button> -->
            </div>
          </div>

<div class="pad_3">  
          <div class="table-responsive">

            <table class="table themeTable" >
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Service</th>
                  <th>Service For</th>
                  <th>Price</th>
                  <th>Discount Price</th>
                  <th>Special Price</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="saloonTable"> 
    <?php $i=1; foreach($getServicesPrices as $res){  ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $res['service_name'];?></td>
                  <td><?php echo ucwords($res['price_for']);?></td>
                  <td>Rs <?php echo $res['base_price'];?></td>
                  <td>Rs <?php echo $res['sale_price'];?></td>
                  <td><?php 
                    $special1='';
                    $sp_price='';
                    if($res['price_type']!='regular'){
                      $id=$res['sp_id'];
                      $sp=$controller->getSpecialPrices($id);
                      $special=array();
                      foreach($sp as $ss){
                        $special[]=ucwords($ss['custom_on']);
                        $sp_price=$ss['sales_price'];
                      }
                      $special1=implode(",",$special);
                      echo $special1.'<br/>';
                      echo "Rs ".$sp_price;
                    }else{
                      echo 'No Special Price';
                    }
                    ?>
                  
                  </td>
                  <td>
                    <span>
                      <i class="fa fa-pencil-square-o edit_service" data-price_type="<?php echo $res['price_type'];?>" data-id="<?php echo $res['sp_id'];?>" data-special_data="<?php echo $special1;?>" data-sp_price="<?php echo $sp_price;?>" data-sale_price="<?php echo $res['sale_price'];?>"  data-base_price="<?php echo $res['base_price'];?>" aria-hidden="true"></i> 
                    </span>
                  </td>
                </tr>
                 <?php $i++; } ?>

              </tbody>
            </table>

          </div>
          <div class="clearfix"></div>
            <div class='pagination-container tablePaging _hide'>
                  <div class="pagination">
                    <ol id="numbers"></ol>
                  </div>
            </div>
     </div>
</div>
    <!-- partial -->
    <div class="modal fade pswdModal" id="addDiscount" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Discount</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post"> 
              <input type="hidden" name="type" value="updateServiceDiscount" />
              <input type="hidden" name="sp_id" id="sp_id" value="" />
               
                <div class="row mb-5">
                  <div class="col-md-6">
                    <div class="form-group ">
                      <input type="number" step='0.01' id="price" name="base_price" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="price">Price</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="number" step='0.01' id="dPrice" name="sale_price" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="dPrice">Discount Price</label>
                    </div>
                  </div>

<div class="col-md-12">

<div class="form-group">
                    <select name="salonee_price_type" id="salonee_price_type" class="form-control" autocomplete="off" required >
                      <option hidden   value> -- Select Price Type -- </option>
                      <option value="regular">Regular</option>
                      <option value="custom_price_weekend">Custom Price - Weekend</option>
                      <option value="custom_price_special_date">Custom Price -- Special Date</option>
                    </select>
                    <label class="form-control-placeholder p-0" for="salonee_price_type">Select Price Type</label>
                  </div>


                  <div class="form-group" id="salonee_weekend_name" style="display:none;">
                    <select name="salonee_weekend_name[]" id="salonee_weekend_name_value" multiple class="form-control" autocomplete="off" >
                      <option hidden  selected value> -- Select Weekend-- </option>
                      <option value="monday">Every Monday</option>
                      <option value="tuesday">Every Tuesday</option>
                      <option value="wednesday">Every Wednesday</option>
                      <option value="thursday">Every Thursday</option>
                      <option value="friday">Every Friday</option>
                      <option value="satuerday">Every Satuerday</option>
                      <option value="sunday">Every Sunday</option>
                    </select>
                    <label class="form-control-placeholder p-0" for="salonee_weekend_name">Select Weekend</label>
                  </div>

                  <div class="form-group" id="salonee_special_date_tag" style="display:none;">
                    <input type="text" id="salonee_special_date" readonly name = "salonee_special_date" class="form-control" autocomplete="off" >
                    <label class="form-control-placeholder p-0" for="special_date">Special Dates</label>
                  </div>
                  
                  <div class="form-group" id="salonee_special_price_tag" style="display:none;">
                    <input type="number" step='0.01' id="salonee_special_price" name = "salonee_special_price" class="form-control" autocomplete="off" >
                    <label class="form-control-placeholder p-0" for="salonee_special_price">Special Price</label>
                  </div>
</div>
                  

                </div>


                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Update</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->
<?php include("includes/footer.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script>
$(".edit_service").click(function(){
  var id=$(this).data("id");
  var base_price=$(this).data("base_price");
  var sale_price=$(this).data("sale_price");

  var price_type=$(this).data("price_type");
  var sp_price=$(this).data("sp_price");
  var special_data=$(this).data("special_data");
  
  $("#salonee_price_type").val(price_type);
  $("#salonee_special_date_tag").hide();
  $("#salonee_weekend_name").hide();
  $("#salonee_special_price_tag").hide();
  
  if(price_type=='custom_price_weekend'){
  $("#salonee_special_price_tag").show();
    $("#salonee_weekend_name").show();
    special_data=special_data.toLowerCase();
    special_data=special_data.split(",");
    $("#salonee_weekend_name_value").val(special_data)
    $("#salonee_special_price").val(sp_price)
  }else if(price_type=='custom_price_special_date'){
    $("#salonee_special_date_tag").show();
   $("#salonee_special_price_tag").show();
    $("#salonee_special_date").val(special_data);
    $("#salonee_special_price").val(sp_price)
    $('#salonee_special_date').datepicker('update');
  }else{
    $("#salonee_weekend_name_value").val('')
    $("#salonee_special_price").val('')
  }
  
  $("#dPrice").val(sale_price);
  $("#price").val(base_price);
  $("#sp_id").val(id);
  $("#addDiscount").modal('show');
});
$("#salonee_price_type").change(function(){
  $("#salonee_special_date_tag").hide();
  $("#salonee_weekend_name").hide();
  $("#salonee_special_price_tag").hide();
  if($(this).val()=='custom_price_special_date'){
    $("#salonee_special_date_tag").show();
    $("#salonee_special_price_tag").show();
  }else if($(this).val()=='custom_price_weekend'){
    $("#salonee_weekend_name").show();
    $("#salonee_special_price_tag").show();
  }
});
$(document).ready(function(){
        $("#searchTd").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#saloonTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
         $('.table').dataTable({
          "pageLength": 6
        });
      }); 
      $('#salonee_special_date').datepicker({
        multidate: true,
        format: 'yyyy-mm-dd'
      });
</script>