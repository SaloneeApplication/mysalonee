<?php
	session_start();
	
	//Retrieving Session Variable
	
	if(isset($_SESSION["serviceProviderId"]))
	{
		$serviceProviderId = $_SESSION["serviceProviderId"];
		unset($_SESSION["serviceProviderId"]);
		setcookie('ssl_alert', '',-1, "/");
		session_destroy();
		session_start();
		$_SESSION['success'] = 'Signed Out Successfully';
		header('Location:index.php');
			
		
	}
	else
	{
		$_SESSION['message'] = 'Unauthorized Access Prohibited';
		header('Location:index.php');
	}
?>