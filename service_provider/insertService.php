<?php 
	session_start();
	
	//Included Files
	
	include("class/serviceProvider.php");	
	include("class/dbConnection.php");


	//Retrieving Session Variable
	
	if(isset($_SESSION["serviceProviderId"]))
	{
		
		$serviceProviderId = $_SESSION["serviceProviderId"];
		
		//echo $serviceProviderId;
	}
	else
	{
		header('Location:index.php?message=Unauthorized%20Access%20Prohobited');
	}
	
	//Retrieving Form Fields
	//Creating dbObject 
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	if($con)
	{
		$branchId = $_REQUEST['branch'];
		$serviceId = $_REQUEST['service'];
		$description = mysqli_real_escape_string($con,$_REQUEST['description']);
		$price = $_REQUEST['price'];
		$addons = mysqli_real_escape_string($con,$_REQUEST['addons']);
		$product = mysqli_real_escape_string($con,$_REQUEST['products']);
		
		
	
		$serviceProviderObj = new serviceProvider();
		
		
		
		
			$checkExistence = $serviceProviderObj->checkServiceProviderService($serviceProviderId,$serviceId,$con);
			if($checkExistence)
			{
				$_SESSION['failure'] = 'Service Already Exists';
				header('Location:services.php');
			}
			else
			{
				$insertResult = $serviceProviderObj->insertServiceProviderService($serviceProviderId,$branchId,$serviceId,$description,$price,$addons,$product,$con);
				if($insertResult)
				{
					$_SESSION['success'] = 'Service Added Successfully';
					header('Location:services.php');
				}
				else
				{
					$_SESSION['failure'] = 'Oops! Something Went Wrong';
					header('Location:services.php');
				}
				
			}
			
		
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
	

?>