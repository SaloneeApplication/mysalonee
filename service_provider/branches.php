<?php
require_once dirname(__DIR__) . '/controller/UserBranches.php';
$controller = new UserBranches();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; $page = $page_data['page']; 
$cities=$controller->getCitiesList();
$branches=$controller->getBranches();
$member_access=$controller->getMemberAccess();
include("includes/header.php");
$dbObject = new dbConnection();
$con = $dbObject->getConnection();
if($con)
{
    $serviceProviderObj = new serviceProvider();
    $recordSet2 = $serviceProviderObj->getAllServiceProviderBranches($serviceProviderId,$con);
}
else
{
    echo mysqli_errno()."<br/>".mysqli_error();
}
?>
<?php include("includes/sidebar.php");?>
<div class="main__content brancs-main">
          <div class="col-md-12">
            <div class="pull-right rightBtn">
              <button class="btn login-btn" data-toggle="modal" data-target="#addLocation" id="myButton">Add &nbsp;<i
                  class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
          </div>
          <ul class="salonee_locations paginationTable ">
			<?php 
				foreach($branches as $row2)
				{
					$location = $row2['city_name'];
					$address = $row2['address'];
					$branch_type = $row2['branch_type'];
					$status = $row2['status'];
					echo "<li class='tableItem'>";
						echo "<span><i class='fa fa-map-marker' aria-hidden='true'></i></span>";
						echo "<p>";
							echo "<b>$location [ ".ucwords(str_replace("_branch"," ",$branch_type))." ] <a href='".user_base_url."view-service-provider.php?id=".$row2['branch_id']."'><i class='fa fa-eye'></i></a> </b>";
							echo $address . "<br/>";
							if($status == 3)
							{
								echo "<b>Status : <i style = 'color:red'>Pending</i></b>";
							}
							else
							{
								echo "<b>Status : <i style = 'color:green'>Approved</i></b>";
							}
							
						echo "</p>";
					echo "</li>";
				}
			?>
          </ul>
          <div class="pagination-container">
            <p class='paginacaoCursor beforePagination' id=""><i class="fa fa-chevron-left" aria-hidden="true"></i></p>
            <p class='paginacaoCursor afterPagination' id=""><i class="fa fa-chevron-right" aria-hidden="true"></i></p>
          </div>
		  
		  <!-- add location modal  -->
    <div class="modal fade pswdModal" id="addLocation" tabindex="-1" role="dialog" aria-labelledby="addLocationTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addLocationTitle">Add Branch</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form method = "post" action = "" enctype="multipart/form-data"  name="form">
              <input type="hidden" name="type" value="addServiceProvider" />
                <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" id="title" class="form-control" name="name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="title">Name</label>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" id="business_name" class="form-control" name="business_name" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="title">Business Name</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="code">Email</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="number" name="mobile" class="form-control" autocomplete="off" required>
                                    <label class="form-control-placeholder p-0" for="value">Mobile</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select class="form-control" autocomplete="off" id="city" name="city" required>
                                      <option value="0"> Select Location</option>
                                        <?php foreach($cities as $cit){ 
                                          echo '<option value="'.$cit['id'].'">'.$cit['city_name_en'].'</option>';
                                        }?>
                                    </select>
                                    <label class="form-control-placeholder p-0" for="city">City</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" id="address" name="address" required></textarea>
                                    <label class="form-control-placeholder p-0" for="address">Address</label>
                                </div>
                            </div>
                        </div>
                      <div class="form-group">
                        <label for="file-upload" class="custom-file-upload sadmnUpload">
                          Business Licence<i class="fa fa-camera" aria-hidden="true"></i>
                        </label>
                        <input id="file-upload" name='image' type="file" style="display:none;">
                      </div>



                <div class="form-group">
                  <button type="submit" class="btn theme-btn" id="success">Submit</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
		  
        </div>
<?php include("includes/footer.php");?>

<script>
     $("form[name='form']").submit(function(e){
        if(typeof $('#file-upload')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select Business Licence',
          });
          e.preventDefault();
        return false;
        }
      });
</script>