<?php
require_once dirname(__DIR__) . '/controller/UserUpgrade.php';
$controller = new UserUpgrade();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$page = $page_data['page']; 
$plans=$controller->getPlans();
include("includes/header.php");
include("includes/sidebar.php");?>
<style>
input[type=date]:required:invalid::-webkit-datetime-edit {
color: transparent;
}
input[type=date]:focus::-webkit-datetime-edit {
    color: black !important;
}

.pricing-table-title {
  text-transform: uppercase;
  font-weight: 700;
  font-size: 2.6em;
  color: #FFF;
  margin-top: 15px;
  text-align: left;
  margin-bottom: 25px;
  text-shadow: 0 1px 1px rgba(0,0,0,0.4);
}

.pricing-table-title a {
  font-size: 0.6em;
}

.clearfix:after {
  content: '';
  display: block;
  height: 0;
  width: 0;
  clear: both;
}
/** ========================
 * Contenedor
 ============================*/
.pricing-wrapper {
  width: 960px;
  margin: 40px auto 0;
}

.pricing-table {
  margin: 0 10px;
  text-align: center;
  width: 300px;
  float: left;
  -webkit-box-shadow: 0 0 15px rgba(0,0,0,0.4);
  box-shadow: 0 0 15px rgba(0,0,0,0.4);
  -webkit-transition: all 0.25s ease;
  -o-transition: all 0.25s ease;
  transition: all 0.25s ease;
}

.pricing-table:hover {
  -webkit-transform: scale(1.06);
  -ms-transform: scale(1.06);
  -o-transform: scale(1.06);
  transform: scale(1.06);
}

.pricing-title {
  color: #FFF;
  background: #e95846;
  padding: 20px 0;
  font-size: 2em;
  text-transform: uppercase;
  text-shadow: 0 1px 1px rgba(0,0,0,0.4);
}

.pricing-table.recommended .pricing-title {
  background: #2db3cb;
}

.pricing-table.recommended .pricing-action {
  background: #2db3cb;
}

.pricing-table .price {
  background: #403e3d;
  font-size: 3.4em;
  font-weight: 700;
  padding: 20px 0;
  text-shadow: 0 1px 1px rgba(0,0,0,0.4);
}

.pricing-table .price sup {
  font-size: 0.4em;
  position: relative;
  left: 5px;
}

.table-list {
  background: #FFF;
  color: #403d3a;
}

.table-list li {
  font-size: 1.4em;
  font-weight: 700;
  padding: 12px 8px;
}

.table-list li:before {
  content: "\f00c";
  font-family: 'FontAwesome';
  color: #3fab91;
  display: inline-block;
  position: relative;
  right: 5px;
  font-size: 16px;
} 

.table-list li span {
  font-weight: 400;
}

.table-list li span.unlimited {
  color: #FFF;
  background: #e95846;
  font-size: 0.9em;
  padding: 5px 7px;
  display: inline-block;
  -webkit-border-radius: 38px;
  -moz-border-radius: 38px;
  border-radius: 38px;
}


.table-list li:nth-child(2n) {
  background: #F0F0F0;
}

.table-buy {
  background: #FFF;
  padding: 15px;
  text-align: center;
  overflow: hidden;
}

.table-buy p {
  float: center;
  color: #37353a;
  font-weight: 700;
  font-size: 2.4em;
}

.table-buy p sup {
  font-size: 0.5em;
  position: relative;
  left: 5px;
}

.table-buy .pricing-action {
  float: center;
  color: #FFF;
  background: #e95846;
  padding: 10px 16px;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  border-radius: 2px;
  font-weight: 700;
  font-size: 1.4em;
  text-shadow: 0 1px 1px rgba(0,0,0,0.4);
  -webkit-transition: all 0.25s ease;
  -o-transition: all 0.25s ease;
  transition: all 0.25s ease;
}

.table-buy .pricing-action:hover {
  background: #cf4f3e;
}

.recommended .table-buy .pricing-action:hover {
  background: #228799;  
}

/** ================
 * Responsive
 ===================*/
 @media only screen and (min-width: 768px) and (max-width: 959px) {
  .pricing-wrapper {
    width: 768px;
  }

  .pricing-table {
    width: 236px;
  }
  
  .table-list li {
    font-size: 1.3em;
  }

 }

 @media only screen and (max-width: 767px) {
  .pricing-wrapper {
    width: 420px;
  }

  .pricing-table {
    display: block;
    float: none;
    margin: 0 0 20px 0;
    width: 100%;
  }
 }

@media only screen and (max-width: 479px) {
  .pricing-wrapper {
    width: 300px;
  }
} 
* {
  margin: 0;
  padding: 0;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

body {
  background: #2e2a2a;
  color: #FFF;
  font-size: 62.5%;
  font-family: 'Roboto', Arial, Helvetica, Sans-serif, Verdana;
}

ul {
  list-style-type: none;
}

a {
  color: #e95846;
  text-decoration: none;
}
    </style>
    <div class="main__content">

<!-- upgrade plans div -->
<!-- Contenedor -->
<div class="pricing-wrapper clearfix">


  <?php foreach($plans as $p){ ?>
    <div class="pricing-table <?php echo ($p['recommand_plan']==1)?'recommended':'';?>">
      <h3 class="pricing-title"><?php echo $p['name'];?> <span style="font-size: 17px;margin: 10px;
    font-weight: bold;"><?php echo ($p['recommand_plan']==1)?'(recommended)':'';?></span></h3>
      <div class="price"><?php echo $p['currency'].' '.round($p['price']);?><sup>/<?php echo ($p['duration']==1)?$p['duration'].' Month':$p['duration'].' Months';?> </sup></div>
      <!-- Lista de Caracteristicas / Propiedades -->
      <ul class="table-list">
        <li>Salonee <span>visable to customers</span></li>
        <li><?php echo $p['free_advertisements_days'];?> Days <span> Salonee Advertisement </span></li>
        <li><?php echo $p['free_featured_profiles_days'];?> Days <span>Top listed Profiles showing to customers</span></li>
        <li>24/7 <span>Support</span></li>
      </ul>
      <!-- Contratar / Comprar -->
      <div class="table-buy">
        <!-- <p></sup></p> -->
        <a href="javascript:;" class="pricing-action buy_now" data-plan_id="<?php echo $p['subscription_plan_id'];?>">Upgrade <i class="fa fa-arrow-right"></i></a>
      </div>
    </div>
    <?php }?> 

  </div>

<!-- upgrade plans div end -->
        </div>
<form action="" name="redirect" id="upgrade_now" method="post" >
   
    <input type="submit" style="display:none;" id="submit_button" />
</form>
<?php include("includes/footer.php");?>

<script>
     $("form[name='categor_im']").submit(function(e){
        if(typeof $('#file-upload')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select category image',
          });
          e.preventDefault();
          return false;
        }
      });
      
$(".buy_now").click(function(){
  var plan_id =$(this).data("plan_id");
  swal({
    title: 'Redirect to Payment Gateway',
    text: 'Payment in processs...',
    showCancelButton: false,
    showConfirmButton: false
  });
  $.ajax({
    url:"<?php echo user_base_url;?>upgrade.php",
    method:"POST",
    data:{type:"upgradePurchase",plan_id:plan_id},
    success:function(data){
      var data=JSON.parse(data);
      $("#upgrade_now").attr("action",data.action);
      $("#upgrade_now").append('<input type=hidden name=encRequest value='+data.encrypted_data+'>');
      $("#upgrade_now").append('<input type=hidden name=access_code value='+data.access_code+'>');
      $("#upgrade_now").submit();
    }
  });
  // setTimeout(function(){
  //   $("#plan_id").val(plan_id);
  //   $("#upgrade_now").submit();
  // },2000);
});
  
</script>