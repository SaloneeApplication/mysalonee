<?php
require_once dirname(__DIR__) . '/controller/UserServices.php';
$controller = new UserServices();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$services=$controller->getUserServices();
$user_categories=$controller->getUserCategories();
$timings=$controller->getSaloneeTimings();
$member_access=$controller->getMemberAccess();
include("includes/header.php");
	
?>

	<?php include("includes/sidebar.php");?>

  <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<div class="main__content">
        <div class="col-md-12">
          <div class="w-50">
            <div class="input-group mb-3 _hide" >
              <input type="text" class="form-control _search"  id="searchTd" placeholder="Search">
              <div class="input-group-append" >
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
          </div>
          <div class="pull-right rightBtn" style="right:120px;">
              <button class="btn login-btn" data-toggle="modal" data-target="#addTimings" id="myButton">Timings &nbsp;<i
                class="fa fa-clock-o" aria-hidden="true"></i></button>
            </div>
          <div class="pull-right rightBtn">
         
            <button class="btn login-btn" data-toggle="modal" data-target="#addDiscount" id="myButton">Add &nbsp;<i
                class="fa fa-plus" aria-hidden="true"></i></button>
          </div>
        </div>
<div class="pad_3">  
          <div class="table-responsive">

            <table class="table themeTable" >
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Category</th>
                  <th>Service</th>
                  <th>Description</th>
                  <th >Price</th>
                  <th>Add Ons</th>
                  <th>Slots Freq</th>
                  <th>Product To Use</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="saloonTable"> 
				
				<?php 
        $i=1;
          foreach($services as $row3)
					{
						$id = $row3[0];
						$branch = $row3['location']."-".$row3['address'];
						$category = $row3['category'];
						$service = $row3['service_name'];
						$description = $row3['description'];
						$price = @$row3['base_price'];
						$sale_price = @$row3['sale_price'];
						$addons = $row3['add_ons'];
						$product = $row3['product_to_use'];
						$time_slot_freq = $row3['time_slot_freq'];
            $service_provider_service_id=$row3['service_provider_service_id'];
            $category=explode("-",$category);
						echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>".( ( false == is_null( $category ) && true == isset( $category[0] ) ) ? $category[0] : '' )." <br/><font size='2px'>[ For ".( ( false == is_null( $category ) && true == isset( $category[1] ) ) ? $category[1] : '' )." ]</font></td>";
							echo "<td>$service</td>";
							echo "<td>$description</td>";
							echo "<td>";

              $prices=$controller->getServicePrices($service_provider_service_id);
              foreach($prices as $pr){
                $naming=ucwords($pr['price_for']);
                echo $naming.'<br/>';
                echo 'Rs '.$pr['base_price'].' - Rs '.$pr['sale_price'].'<br/>';
              }

              echo "</td>";
							echo "<td>$addons</td>";
							echo "<td>Every $time_slot_freq Min</td>";
							echo "<td>$product</td>";
							echo "<td>";
              echo '<i class="fa fa-pencil-square-o editService" data-id="'.$row3['service_provider_service_id'].'" aria-hidden="true"></i>';
              
              if($row3['status']==1){
                ?>
               <i style='color:red;font-size:18px' onclick="window.location.href='<?php echo user_base_url;?>services.php?sid=<?php echo $row3['service_provider_service_id'];?>&status=0&type=disableservice';" class="fa fa-eye-slash" title="disable" aria-hidden="true" data-id = '<?php echo $row2[0];?>'></i>
                <?php
              }else{
                ?>
               <i style='color:green;font-size:18px' onclick="window.location.href='<?php echo user_base_url;?>services.php?sid=<?php echo $row3['service_provider_service_id'];?>&status=0&type=enableservice';" class="fa fa-eye-slash" title="disable" aria-hidden="true" data-id = '<?php echo $row2[0];?>'></i>
                <?php
              }

              
          
                  
              echo "</td>";
						echo "</tr>";
            $i++;
					}
				
				?>

              </tbody>
            </table>

          </div>
          <div class="clearfix"></div>
            <div class='pagination-container tablePaging'>
              
                  <div class="pagination" style="display:none;"> 
               <ol id="numbers"></ol>
                  </div>
                
            </div>
            




     </div>
</div>
<!-- add popup start -->
    <div class="modal fade pswdModal" id="addDiscount" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Add Service</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form method="post" action="" name="service_im" id="service_im" enctype="multipart/form-data">
              <input type="hidden" name="type" value="insertService" />
				<div class="form-group">
                  <select name="service" class="form-control" autocomplete="off" required >
                    <option hidden disabled selected value> -- Select Category -- </option>
                    <?php 
                        foreach($user_categories as $row2)
                        {
                          $category = $row2['name'];
                          $serviceId = $row2['category_id'];
                          $category_for = $row2['category_for'];
                          echo "<option value = '$serviceId'>$category <font color='gray' style='font-size:12px;font-weight:500;color:gray;'>[ For ".ucwords($category_for)." ]</font></option>";
                        }
                    ?>
                  </select>

                </div>
            <div class="form-group">
              <input type="text" name="service_name" id="service_name" class="form-control" autocomplete="off" required/>
              <label class="form-control-placeholder p-0" for="service_name">Service Name</label>
            </div>
				<div class="form-group">
					<textarea type="text" id="description" name = "description" rows="5" class="form-control" autocomplete="off" required></textarea>
					<label class="form-control-placeholder p-0" for="accountNumber">Description</label>
				</div>
        <div class="form-group">
          <label for="service-image" class="custom-file-upload sadmnUpload">
            Service Image<i class="fa fa-camera" aria-hidden="true"></i>
          </label>
          <input id="service-image" name="service_image" type="file"  style="display:none;">
        </div>
        <div class="form-group">
          <select name="service_for" id="service_for" class="form-control" autocomplete="off" required >
            <option hidden disabled selected value> -- Select Service For -- </option>
            <option value="salonee">Only Salonee</option>
            <option value="home">Only at Home</option>
            <option value="both">Both</option>
          </select>
        </div>

            <!-- salonee parent start -->
            <div id="salone_parent" style="display:none;"> <h5>At Salonee Pricing </h5>
              <div class="form-group">
                <input type="text" id="salonee_base_price" name = "salonee_base_price" class="form-control" autocomplete="off" required>
                <label class="form-control-placeholder p-0" for="salonee_base_price">Base Price</label>
              </div>
              <div class="form-group">
                <input type="text" id="salonee_sale_price" name = "salonee_sale_price" class="form-control" autocomplete="off" required>
                <label class="form-control-placeholder p-0" for="salonee_sale_price">Sale Price</label>
              </div>

              <div class="form-group">
                <select name="salonee_price_type" id="salonee_price_type" class="form-control" autocomplete="off" required >
                  <option hidden disabled selected value> -- Select Price Type -- </option>
                  <option value="regular">Regular</option>
                  <option value="custom_price_weekend">Custom Price - Weekend</option>
                  <option value="custom_price_special_date">Custom Price -- Special Date</option>
                </select>
              </div>
              <div class="form-group" id="salonee_weekend_name" style="display:none;">
                <select name="salonee_weekend_name[]" id="salonee_weekend_name_value" multiple class="form-control" autocomplete="off" >
                  <option hidden disabled selected value> -- Select Weekend-- </option>
                  <option value="monday">Every Monday</option>
                  <option value="tuesday">Every Tuesday</option>
                  <option value="wednesday">Every Wednesday</option>
                  <option value="thursday">Every Thursday</option>
                  <option value="friday">Every Friday</option>
                  <option value="saturday">Every Saturday</option>
                  <option value="sunday">Every Sunday</option>
                </select>
              </div>
              <div class="form-group" id="salonee_special_date_tag" style="display:none;">
                <input type="text" id="salonee_special_date" readonly name = "salonee_special_date" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="special_date">Special Dates</label>
              </div>
              <div class="form-group" id="salonee_special_price_tag" style="display:none;">
                <input type="text" id="salonee_special_price" name = "salonee_special_price" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="salonee_special_price">Special Price</label>
              </div>

              <div class="form-group">
              <select name="salonee_same_slot_time" id="salonee_same_slot_time" class="form-control" autocomplete="off"  >
                <option hidden disabled selected value> -- Same Slot Time booking -- </option>
                <option value="1">Yes</option>
                <option value="0">No</option>
              </select>
            </div>
            <div class="form-group" id="salonee_allowed_slots_same_tag" style="display:none;">
              <input type="number" min="1" id="salonee_allowed_slots_same" name = "salonee_allowed_slots_same" class="form-control" autocomplete="off" />
              <label class="form-control-placeholder p-0" for="salonee_allowed_slots_same">Allowed Slots Same Time</label>
            </div>
            <div class="form-group">
              <input type="number" min="0" id="salonee_day_limit_slots" name = "salonee_day_limit_slots" class="form-control" autocomplete="off" />
              <label class="form-control-placeholder p-0"  for="salonee_day_limit_slots">Per Day Slots Limit</label>
            </div>

            </div>
			<!-- salonee parent end -->
        

        <!-- home parent start -->
        <div id="home_parent" style="display:none;"> <h5>At Home Pricing </h5>
              <div class="form-group">
                <input type="text" id="home_base_price" name = "home_base_price" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="home_base_price">Base Price</label>
              </div>
              <div class="form-group">
                <input type="text" id="home_sale_price" name = "home_sale_price" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="home_sale_price">Sale Price</label>
              </div>

              <div class="form-group">
                <select name="home_price_type" id="home_price_type" class="form-control" autocomplete="off"  >
                  <option hidden disabled selected value> -- Select Price Type -- </option>
                  <option value="regular">Regular</option>
                  <option value="custom_price_weekend">Custom Price - Weekend</option>
                  <option value="custom_price_special_date">Custom Price -- Special Date</option>
                </select>
              </div>
              <div class="form-group" id="home_weekend_name" style="display:none;">
                <select name="home_weekend_name[]" id="home_weekend_name_value" multiple class="form-control" autocomplete="off" >
                  <option hidden disabled selected value> -- Select Weekend-- </option>
                  <option value="monday">Every Monday</option>
                  <option value="tuesday">Every Tuesday</option>
                  <option value="wednesday">Every Wednesday</option>
                  <option value="thursday">Every Thursday</option>
                  <option value="friday">Every Friday</option>
                  <option value="satuerday">Every Satuerday</option>
                  <option value="sunday">Every Sunday</option>
                </select>
              </div>
              <div class="form-group" id="home_special_date_tag" style="display:none;">
                <input type="text" id="home_special_date" readonly name = "home_special_date" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="special_date">Special Dates</label>
              </div>

              <div class="form-group" id="home_special_price_tag" style="display:none;">
                <input type="text" id="home_special_price" name = "home_special_price" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="home_special_price">Special Price</label>
              </div>
              <div class="form-group">
              <select name="home_same_slot_time" id="home_same_slot_time" class="form-control" autocomplete="off"  >
                <option hidden disabled selected value> -- Same Slot Time booking -- </option>
                <option value="1">Yes</option>
                <option value="0">No</option>
              </select>
            </div>
            <div class="form-group" id="home_allowed_slots_same_tag" style="display:none;">
              <input type="number" min="1" id="home_allowed_slots_same" name = "home_allowed_slots_same" class="form-control" autocomplete="off" />
              <label class="form-control-placeholder p-0" for="home_allowed_slots_same">Allowed Slots Same Time</label>
            </div>
            <div class="form-group">
              <input type="number" min="0" id="home_day_limit_slots" name = "home_day_limit_slots" class="form-control" autocomplete="off" />
              <label class="form-control-placeholder p-0"  for="home_day_limit_slots">Per Day Slots Limit</label>
            </div>
            </div>
			<!-- home parent end -->
       

			  <div class="form-group">
					<textarea type="text" id="addons" name = "addons" rows="5" class="form-control" autocomplete="off" required></textarea>
					<label class="form-control-placeholder p-0" for="accountNumber">Add Ons</label>
				</div>
				<div class="form-group">
					<textarea type="text" id="products" name = "products" rows="5" class="form-control" autocomplete="off" required></textarea>
					<label class="form-control-placeholder p-0" for="accountNumber">Products To Use</label>
				</div>
        <div class="form-group">
        <select name="slot_freq" id="slot_freq" class="form-control" autocomplete="off" required >
            <option hidden disabled selected value> -- Slot Booking Frequency Hourly -- </option>
            <option value="15">Every 15 minutes</option>
            <option value="30">Every 30 minutes</option>
            <option value="45">Every 45 minutes</option>
            <option value="60">Every 1 hour</option>
          </select>
				</div>
        
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Submit</button>
                </div>
              </form>
            </div>
          </div>

        </div>
      </div>
    </div>
<!-- add popup end -->

<!-- edit popup start -->
<div class="modal fade pswdModal" id="editPopup" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Edit Service</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form method = "post" action = "">
                <input type="hidden" name="type" value="updateService" />
                <!-- appending area start -->
                  <div id="editpop_content">
                            
                  </div>
                  <!-- appending area end -->
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Submit</button>
                </div>
              </form>
            </div>
          </div>

        </div>
      </div>
    </div>
<!-- edit popup end -->


<style>
.available_status_table tr th{
  padding:30px;
}

.available_status_table tr td{
  padding:20px;
}
</style>

<!-- shop timings -->
<div class="modal fade pswdModal" id="addTimings" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Salonee Timings</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form method = "post" action = "">
                <input type="hidden" name="type" value="updateSaloneeTimings" />

                    <table class="available_status_table">
                        <thead>
                          <tr><th>Day</th><th>From Time</th><th>To Time</th><th>Available Status</th>

                          <tr><td>Monday</td><td><input type="time" value="<?php echo @$timings[0]['from_time'];?>"  name="from_time[]" class="form-control" /></td><td><input type="time"  value="<?php echo @$timings[0]['to_time'];?>"  name="to_time[]" class="form-control" /></td><td><input <?php echo (@$timings[0]['available_status']==1)?"checked":"";?> type="checkbox" name="available_status[]" class="" value="1" /></td></tr>

                          <tr><td>Tuesday</td><td><input type="time"  value="<?php echo @$timings[1]['from_time'];?>"  name="from_time[]" class="form-control" /></td><td><input type="time"  value="<?php echo @$timings[1]['to_time'];?>"  name="to_time[]" class="form-control" /></td><td><input <?php echo (@$timings[1]['available_status']==1)?"checked":"";?>  type="checkbox" name="available_status[]" class="" value="1" /></td></tr>

                          <tr><td>Wednesday</td><td><input type="time"  value="<?php echo @$timings[2]['from_time'];?>"  name="from_time[]" class="form-control" /></td><td><input  value="<?php echo @$timings[2]['to_time'];?>" type="time" name="to_time[]" class="form-control" /></td><td><input  <?php echo (@$timings[2]['available_status']==1)?"checked":"";?>  type="checkbox" name="available_status[]" class="" value="1" /></td></tr>

                          <tr><td>Thursday</td><td><input type="time"  value="<?php echo @$timings[3]['from_time'];?>"  name="from_time[]" class="form-control" /></td><td><input  value="<?php echo @$timings[3]['to_time'];?>"  type="time" name="to_time[]" class="form-control" /></td><td><input  <?php echo (@$timings[3]['available_status']==1)?"checked":"";?>  type="checkbox" name="available_status[]" class="" value="1" /></td></tr>

                          <tr><td>Friday</td><td><input type="time"  value="<?php echo @$timings[4]['from_time'];?>"  name="from_time[]" class="form-control" /></td><td><input type="time"  value="<?php echo @$timings[4]['to_time'];?>"  name="to_time[]" class="form-control" /></td><td><input type="checkbox"  <?php echo (@$timings[4]['available_status']==1)?"checked":"";?>  name="available_status[]" class="" value="1" /></td></tr>


                          <tr><td>Saturday</td><td><input type="time"  value="<?php echo @$timings[5]['from_time'];?>"  name="from_time[]" class="form-control" /></td><td><input  value="<?php echo @$timings[5]['to_time'];?>"  type="time" name="to_time[]" class="form-control" /></td><td><input  <?php echo (@$timings[5]['available_status']==1)?"checked":"";?>  type="checkbox" name="available_status[]" class="" value="1" /></td></tr>

                          <tr><td>Sunday</td><td><input type="time"  value="<?php echo @$timings[6]['from_time'];?>"  name="from_time[]" class="form-control" /></td><td><input  value="<?php echo @$timings[6]['to_time'];?>"  type="time" name="to_time[]" class="form-control" /></td><td><input type="checkbox"  <?php echo (@$timings[6]['available_status']==1)?"checked":"";?>  name="available_status[]" class="" value="1" /></td></tr>

                        </thead>

                    </table>                        
                <div class="form-group">
                  <button type="submit" class="btn theme-btn"><i class="fa fa-save"></i> Save</button>
                </div>
              </form>
            </div>
          </div>

        </div>
      </div>
    </div>
<!-- end shop timings -->

<?php include("includes/footer.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script>
$(document).ready(function(){
        $("#searchTd").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#saloonTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
        $('.table').dataTable({
      "pageLength": 6
    });
      $('#service-image').change(function() {
        var i = $(this).prev('label').clone();
        var file = $('#service-image')[0].files[0].name;
        $(this).prev('label').text(file);
      });
      $("form[name='service_im']").submit(function(e){
          if(typeof $('#service-image')[0].files[0]=='undefined'){
            iziToast.error({
                title: 'Error',
                message: 'Please select service image',
            });
            e.preventDefault();
            return false;
          }
      });        
      }); 
      $(".editService").click(function(){
        var sid=$(this).data("id");
        $.ajax({
          url:"<?php echo user_base_url.'services.php';?>",
          method:"POST",
          data:{sid:sid,type:"getServiceData"},
          success:function(data){
            $("#editpop_content").html(data);
            $("#editPopup").modal('show');
           
            // $(document).on("","#home_special_date1").datepicker({multidate: true,format: 'yyyy-mm-dd'});
          }
        })
      });
      $(document).bind('DOMNodeInserted', function(element){
        $('#salonee_special_date1').datepicker({
              multidate: true,
              format: 'yyyy-mm-dd'
        });
        $('#home_special_date1').datepicker({
          multidate: true,
          format: 'yyyy-mm-dd'
        });

        $("#service_for1").change(function(){
          var type=$(this).val();
          $("#home_parent1").hide();
          $("#salone_parent1").hide();

          //required removing defaulty 
          $("#home_base_price1").removeAttr("required");
          $("#home_sale_price1").removeAttr("required");
          $("#home_price_type1").removeAttr("required");

          $("#salonee_base_price1").removeAttr("required");
          $("#salonee_sale_price1").removeAttr("required");
          $("#salonee_price_type1").removeAttr("required");

          
          $("#salonee_same_slot_time1").removeAttr("required");
          $("#salonee_day_limit_slots1").removeAttr("required");

          $("#home_same_slot_time1").removeAttr("required");
          $("#home_day_limit_slots1").removeAttr("required");

          if(type=='home'){
            $("#home_parent1").show();
            $("#home_base_price1").attr("required","required");
            $("#home_sale_price1").attr("required","required");
            $("#home_price_type1").attr("required","required");

            $("#home_same_slot_time1").attr("required","required");
            $("#home_day_limit_slots1").attr("required","required");

          }else if(type=='salonee'){
            $("#salone_parent1").show();
            $("#salonee_base_price1").attr("required","required");
            $("#salonee_sale_price1").attr("required","required");
            $("#salonee_price_type1").attr("required","required");

            $("#salonee_same_slot_time1").attr("required","required");
            $("#salonee_day_limit_slots1").attr("required","required");

          }else if(type=='both'){
            $("#home_parent1").show();
            $("#salone_parent1").show();

            $("#salonee_base_price1").attr("required","required");
            $("#salonee_sale_price1").attr("required","required");
            $("#salonee_price_type1").attr("required","required");

            
            $("#salonee_same_slot_time1").attr("required","required");
            $("#salonee_day_limit_slots1").attr("required","required");

            $("#home_base_price1").attr("required","required");
            $("#home_sale_price1").attr("required","required");
            $("#home_price_type1").attr("required","required");

            $("#home_same_slot_time1").attr("required","required");
            $("#home_day_limit_slots1").attr("required","required");
          }
      });
      $("#salonee_price_type1").change(function(){
        var type=$(this).val();
        $("#salonee_weekend_name1").hide();
        $("#salonee_special_date_tag1").hide();
        $("#salonee_special_price_tag1").hide();
        $("#salonee_weekend_name_value1").removeAttr("required");
        $("#salonee_special_date1").removeAttr("required");
        $("#salonee_special_price1").removeAttr("required");
        $("#salonee_special_date1").val('');
        $("#salonee_weekend_name_value1 option:selected").removeAttr("selected");
        $('#salonee_special_date1').datepicker('update');
        if(type=='custom_price_weekend'){
          $("#salonee_weekend_name1").show();
          $("#salonee_weekend_name_value1").attr("required","required");

          $("#salonee_special_price_tag1").show();
          $("#salonee_special_price1").attr("required","required");

        }else if(type=='custom_price_special_date'){
          $("#salonee_special_date_tag1").show();
          $("#salonee_special_date1").attr("required","required");

          $("#salonee_special_price_tag1").show();
          $("#salonee_special_price1").attr("required","required");
        }else{
          $("#salonee_special_price_tag1").hide();
          $("#salonee_special_price1").removeAttr("required");
        }
      });
      $("#home_price_type1").change(function(){
        var type=$(this).val();
        $("#home_weekend_name1").hide();
        $("#home_special_date_tag1").hide();
        $("#home_special_price_tag1").hide();
        $("#home_weekend_name_value1").removeAttr("required");
        $("#home_special_date1").removeAttr("required");
        $("#home_special_date1").removeAttr("required");
        $("#home_special_date1").val('');
        $('#home_special_date1').datepicker('update');
        $("#home_weekend_name_value1 option:selected").removeAttr("selected");
        if(type=='custom_price_weekend'){
          $("#home_weekend_name1").show();
          $("#home_weekend_name_value1").attr("required","required");

          $("#home_special_price_tag1").show();
          $("#home_special_price1").attr("required","required");
        }else if(type=='custom_price_special_date'){
          $("#home_special_date_tag1").show();
          $("#home_special_date1").attr("required","required");

          $("#home_special_price_tag1").show();
          $("#home_special_price1").attr("required","required");
        }else{
          $("#home_special_price_tag1").hide();
          $("#home_special_price1").removeAttr("required");

        }
      });
      $("#salonee_same_slot_time1").change(function(){
          $("#salonee_allowed_slots_same_tag1").hide();
          $("#salonee_allowed_slots_same1").removeAttr("required");
          if($(this).val()==1){
            $("#salonee_allowed_slots_same_tag1").show();
            $("#salonee_allowed_slots_same1").attr("required","required");
          }
      });
      $("#home_same_slot_time1").change(function(){
          $("#home_allowed_slots_same_tag1").hide();
          $("#home_allowed_slots_same1").removeAttr("required");
        console.log("check1 ");
          if($(this).val()==1){
           $("#home_allowed_slots_same_tag1").show();
        console.log("check2 ");
           $("#home_allowed_slots_same1").attr("required","required");
          }
      });
    });



      $("#salonee_same_slot_time").change(function(){
          $("#salonee_allowed_slots_same_tag").hide();
          $("#salonee_allowed_slots_same").removeAttr("required");
          if($(this).val()==1){
            $("#salonee_allowed_slots_same_tag").show();
            $("#salonee_allowed_slots_same").attr("required","required");
          }
      });
      $("#home_same_slot_time").change(function(){
          $("#home_allowed_slots_same_tag").hide();
          $("#home_allowed_slots_same").removeAttr("required");

          if($(this).val()==1){
           $("#home_allowed_slots_same_tag").show();
           $("#home_allowed_slots_same").attr("required","required");
          }
      });
      $("#salonee_price_type").change(function(){
        var type=$(this).val();
        $("#salonee_weekend_name").hide();
        $("#salonee_special_date_tag").hide();
        $("#salonee_special_price_tag").hide();
        $("#salonee_weekend_name_value").removeAttr("required");
        $("#salonee_special_date").removeAttr("required");
        $("#salonee_special_price").removeAttr("required");
        $("#salonee_special_date").val('');
        $("#salonee_weekend_name_value option:selected").removeAttr("selected");
        if(type=='custom_price_weekend'){
          $("#salonee_weekend_name").show();
          $("#salonee_weekend_name_value").attr("required","required");

          $("#salonee_special_price_tag").show();
          $("#salonee_special_price").attr("required","required");

        }else if(type=='custom_price_special_date'){
          $("#salonee_special_date_tag").show();
          $("#salonee_special_date").attr("required","required");

          $("#salonee_special_price_tag").show();
          $("#salonee_special_price").attr("required","required");
        }else{
          $("#salonee_special_price_tag").hide();
          $("#salonee_special_price").removeAttr("required");
        }
      });
      $("#home_price_type").change(function(){
        var type=$(this).val();
        $("#home_weekend_name").hide();
        $("#home_special_date_tag").hide();
        $("#home_special_price_tag").hide();
        $("#home_weekend_name_value").removeAttr("required");
        $("#home_special_date").removeAttr("required");
        $("#home_special_date").removeAttr("required");
        $("#home_special_date").val('');
        $("#home_weekend_name_value option:selected").removeAttr("selected");
        if(type=='custom_price_weekend'){
          $("#home_weekend_name").show();
          $("#home_weekend_name_value").attr("required","required");

          $("#home_special_price_tag").show();
          $("#home_special_price").attr("required","required");
        }else if(type=='custom_price_special_date'){
          $("#home_special_date_tag").show();
          $("#home_special_date").attr("required","required");

          $("#home_special_price_tag").show();
          $("#home_special_price").attr("required","required");
        }else{
          $("#home_special_price_tag").hide();
          $("#home_special_price").removeAttr("required");

        }
      });
      $(".fa-trash").click(function () {
		  var id = $(this).data('id');

        swal({
          title: "Are you sure?",
          text: "You want to delete this Service",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
        }).then(result => {
          if (result.dismiss === 'cancel') {
            swal.closeModal();
            return;
          }
		  else
		  {
			  $.ajax({
                type:'POST',
                url:'deleteService',
                data:'id='+id,
                success:function(html){
					
                   swal({
					type: "success",
					text: 'Category has been deleted!',
					showConfirmButton: false,
					timer: 1500
				  });
				   setInterval('location.reload()', 1500); 
                    
                }
            }); 
		  }
          

        },
        )
      });
     
      $('#salonee_special_date').datepicker({
        multidate: true,
        format: 'yyyy-mm-dd'
      });
      $('#home_special_date').datepicker({
        multidate: true,
        format: 'yyyy-mm-dd'
      });
      $("#service_for").change(function(){
          var type=$(this).val();
          $("#home_parent").hide();
          $("#salone_parent").hide();

          //required removing defaulty 
          $("#home_base_price").removeAttr("required");
          $("#home_sale_price").removeAttr("required");
          $("#home_price_type").removeAttr("required");

          $("#salonee_base_price").removeAttr("required");
          $("#salonee_sale_price").removeAttr("required");
          $("#salonee_price_type").removeAttr("required");

          
          $("#salonee_same_slot_time").removeAttr("required");
          $("#salonee_day_limit_slots").removeAttr("required");

          $("#home_same_slot_time").removeAttr("required");
          $("#home_day_limit_slots").removeAttr("required");

          if(type=='home'){
            $("#home_parent").show();
            $("#home_base_price").attr("required","required");
            $("#home_sale_price").attr("required","required");
            $("#home_price_type").attr("required","required");

            $("#home_same_slot_time").attr("required","required");
            $("#home_day_limit_slots").attr("required","required");

          }else if(type=='salonee'){
            $("#salone_parent").show();
            $("#salonee_base_price").attr("required","required");
            $("#salonee_sale_price").attr("required","required");
            $("#salonee_price_type").attr("required","required");

            $("#salonee_same_slot_time").attr("required","required");
            $("#salonee_day_limit_slots").attr("required","required");

          }else if(type=='both'){
            $("#home_parent").show();
            $("#salone_parent").show();

            $("#salonee_base_price").attr("required","required");
            $("#salonee_sale_price").attr("required","required");
            $("#salonee_price_type").attr("required","required");

            
            $("#salonee_same_slot_time").attr("required","required");
            $("#salonee_day_limit_slots").attr("required","required");

            $("#home_base_price").attr("required","required");
            $("#home_sale_price").attr("required","required");
            $("#home_price_type").attr("required","required");

            $("#home_same_slot_time").attr("required","required");
            $("#home_day_limit_slots").attr("required","required");
          }
      });
    </script>
