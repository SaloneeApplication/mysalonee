
<?php
require_once dirname(__DIR__) . '/controller/UserAdvertisement.php';
$controller = new UserAdvertisement();
$controller->init();
$page_data=$controller->pageData();
$advertisements=$controller->getAdvertisements();
$cities=$controller->getCitiesList();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$per_day=$controller->getOption("advertisement_per_day");
$commission=$controller->getOption("advertisement_per_day_admin_commission");
$member_access=$controller->getMemberAccess();
include("includes/header.php");
?>

	<?php include("includes/sidebar.php");?>
  <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <div class="main__content">
          <div class="col-md-12">
            <div class="w-50">
              <div class="input-group mb-3 _hide">
                <input type="text" class="form-control _search"  id="searchTd" placeholder="Search">
                <div class="input-group-append">
                  <button class="btn theme-btn" type="submit">Go</button>
                </div>
              </div>
            </div>
           <!-- <div class="pull-right rightBtn">-->
                <div class="pull-right ">
              <button class="btn login-btn" data-toggle="modal"  <?php echo ($member_access=='1')?'data-target="#addDiscount"':'onclick="window.location.href=\'upgrade.php\'"';?> id="myButton">Create &nbsp;<i
                  class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
          </div>

<div class="pad_3">  
          <div class="table-responsive">

            <table class="table themeTable" >
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Banner</th>
                  <th>From Date</th>
                  <th>To Date</th>
                  <th>Clicks</th>
                  <th>City</th>
                  <th>Approval Status</th>
                  <th>Ad Status</th>
                  <th>Remark</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="saloonTable"> 
              <?php $i=1; foreach($advertisements as $adv){?>
                <tr>
                    <td><?php echo $i++;?></td>
                    <td>
                    <?php if($adv['banner']!=''){ ?>
                    <img src="<?php echo user_base_url.$adv['banner'];?>" width="100px" />
                    <?php } ?>
                    </td>
                    <td><?php echo $adv['from_date'];?></td>
                    <td><?php echo $adv['expiry_date'];?></td>
                    <td><?php echo $adv['clicks'];?></td>
                    <td><?php echo $adv['city_name'];?></td>
                    <td><?php 
                      if($adv['approve_status']=='pending'){
                        echo '<label class="badge badge-pill badge-warning">Pending</label>';
                      }elseif($adv['approve_status']=='approved'){
                        echo '<label class="badge badge-pill badge-success">Approved</label>';
                      }elseif($adv['approve_status']=='rejected'){
                        echo '<label class="badge badge-pill badge-danger">Rejected</label>';
                      }else{
                        echo '<label class="badge badge-pill badge-warning">Oops</label>';
                      }
                    ?></td>
                    <td>
                    <?php 
                      if($adv['status']=='disable'){
                        echo '<label class="badge badge-pill badge-warning">Inactive</label>';
                      }elseif($adv['status']=='active'){
                        echo '<label class="badge badge-pill badge-success">Active</label>';
                      }elseif($adv['status']=='expired'){
                        echo '<label class="badge badge-pill badge-danger">Expired</label>';
                      }else{
                        echo '<label class="badge badge-pill badge-warning">Oops</label>';
                      }
                    ?>
                    </td>
                    <td><?php echo $adv['remark'];?></td>
                    <td>
                    <a href="javascript:;"  data-id="<?php echo $adv['advertisement_id'];?>" class="edit_advert badge badge-primary"><i class="fa fa-edit"></i> Edit</a>
                    <?php 
                      if($adv['approve_status']=='approved'){
                        if($adv['status']=='disable'){
                          echo '<a href="'.user_base_url.'advertisement.php?id='.$adv['advertisement_id'].'&type=enableAd" data-id="'.$adv['advertisement_id'].'" class="badge badge-success"><i class="fa fa-check"></i> Activate</a>';
                        }elseif($adv['status']=='active'){
                          echo '<a href="'.user_base_url.'advertisement.php?id='.$adv['advertisement_id'].'&type=disableAd" class="badge badge-danger"><i class="fa fa-times"></i> Disable</a>';
                        }
                      }
                    ?></td>
                </tr>
                <?php } ?>

              </tbody>
            </table>

          </div>
          <div class="clearfix"></div>
            <div class='pagination-container tablePaging _hide'>
                  <div class="pagination">
                    <ol id="numbers"></ol>
                  </div>
            </div>
     </div>
</div>

    <!-- partial -->
    <div class="modal fade pswdModal" id="addDiscount" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Advertisement</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" enctype='multipart/form-data' method="post" name="categor_im"> 
              <input type="hidden" name="type" value="advertisementPurchase" />
              <input type="hidden" name="sp_id" id="sp_id" value="" />
               
                <div class="row mb-5">
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="text" id="from_date" name="from_date" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="from_date">From Date</label>
                    </div>

                    <div class="form-group">
                      <input type="text" id="to_date" name="to_date" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="to_date">To Date</label>
                    </div>

                    <div class="form-group"  >
                      <select name="city" id="city" required  class="form-control" autocomplete="off" >
                        <option hidden disabled selected value> -- Select City-- </option>
                        <?php foreach($cities as $cit) { 
                            echo '<option value="'.$cit['id'].'">'.$cit['city_name_en'].'</option>';
                        } ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="file-upload" class="custom-file-upload">
                        UPLOAD BANNER <i class="fa fa-camera" aria-hidden="true"></i>
                      </label>
                      <input id="file-upload" accept="image/*" name='upload_cont_img' type="file" style="display:none;">
                    </div>
                    <div>
                      <div class="lables-flex">
                        <div><b>Sub Total</b></div>
                        <div><b>Admin Commission</b></div>
                        <div><b>Total Payable</b></div>
                     </div>
                     <div class="lables-flex1">
                        <div id="sub_total">AED 0.00</div>
                        <div id="commission">AED 0.00</div>
                        <div id="total">AED 0.00</div>
                    </div>
                  </div>
                  </div>
                 <!-- <table>
                      <tr><th>Sub Total</th><td>&nbsp;</td><td id="sub_total">AED 0.00</td></tr>
                      <tr><th>Admin Commission</th><td>&nbsp;</td><td id="commission">AED 0.00</td></tr>
                      <tr><th>Total Payable</th><td>&nbsp;</td><td id="total">AED 0.00</td></tr>
                  </table> -->


                  
                </div>
                
                        
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Submit</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->

    
    <!-- partial -->
    <div class="modal fade pswdModal" id="editAdvertisement" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Edit Advertisement</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" enctype='multipart/form-data' method="post" name="categor_im1"> 
              <input type="hidden" name="type" value="updateAdvertisement" />
              <input type="hidden" name="ad_id" id="ad_id" value="" />
               
                <div class="row mb-5" id="advertisement_data">
                  
                </div>
                        
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Update</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->
<?php include("includes/footer.php");?>
<form action="" name="redirect" id="upgrade_now" method="post" >
  <input type="submit" style="display:none;" id="submit_button" />
</form>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script>

const per_day="<?php echo $per_day;?>";
const commission="<?php echo $commission;?>";
$("#to_date").change(function(){
  var from=$("#from_date").val();
  var to=$("#to_date").val();
  var date1 = new Date(from);
  var date2 = new Date(to);
  var diffTime = Math.abs(date2 - date1);
  var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
  diffDays=diffDays+1;
  var total=parseInt(per_day)*diffDays;
  var total_commission=parseInt(diffDays)*parseInt(commission);
  var Payable=total_commission+total;
  $("#sub_total").html("AED "+total);
  $("#commission").html("AED "+total_commission);
  $("#total").html("AED "+Payable);
});
$("#from_date").change(function(){
  var from=$("#from_date").val();
  $("#to_date").datepicker('destroy');
  $('#to_date').datepicker({
    autoclose: true,
    startDate:from,
    todayHighlight:true,
    format: 'yyyy-mm-dd'
  });
  $("#sub_total").html("AED 0.00");
  $("#commission").html("AED 0.00");
  $("#total").html("AED 0.00");
});
$(document).ready(function(){
    $("#searchTd").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#saloonTable tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
    $('.table').dataTable({
      "pageLength": 6
    });

  }); 
  $('#from_date').datepicker({
    autoclose: true,
    todayHighlight:true,
    startDate:"<?php echo date("Y-m-d");?>",
    format: 'yyyy-mm-dd'
  });
  $('#to_date').datepicker({
    autoclose: true,
    todayHighlight:true,
    startDate:"<?php echo date("Y-m-d");?>",
    format: 'yyyy-mm-dd'
  });
  $("form[name='categor_im']").submit(function(e){
        if(typeof $('#file-upload')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select advertisement banner image',
          });
          e.preventDefault();
          return false;
        }
        swal({
          title: 'Redirect to Payment Gateway',
          text: 'Payment in processs...',
          showCancelButton: false,
          showConfirmButton: false
        });
        setTime(function(){
          return true;
        },3000);
  });
  $(".edit_advert").click(function(){
    var adid=$(this).data("id");
    $("#editAdvertisement").modal('show');
    $("#ad_id").val(adid);
    $.ajax({
      url:"<?php echo user_base_url.'advertisement.php';?>",
      method:"POST",
      data:{id:adid,type:'editAdv'},
      success:function(data){
        $("#advertisement_data").html(data);

       setTimeout(() => {
        $('#from_date1').datepicker({
          autoclose: true,
          todayHighlight:true,
          startDate:"<?php echo date("Y-m-d");?>",
          format: 'yyyy-mm-dd'
        });
        $('#to_date1').datepicker({
          autoclose: true,
          todayHighlight:true,
          startDate:"<?php echo date("Y-m-d");?>",
          format: 'yyyy-mm-dd'
        });
        $("#to_date1").change(function(){
          var from=$("#from_date1").val();
          var to=$("#to_date1").val();
          var date1 = new Date(from);
          var date2 = new Date(to);
          var diffTime = Math.abs(date2 - date1);
          var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
          diffDays=diffDays+1;
          var total=parseInt(per_day)*diffDays;
          var total_commission=parseInt(diffDays)*parseInt(commission);
          var Payable=total_commission+total;
          $("#sub_total1").html("AED "+total);
          $("#commission1").html("AED "+total_commission);
          $("#total1").html("AED "+Payable);
        });
        $("#from_date1").change(function(){
          var from=$("#from_date1").val();
          $("#to_date1").datepicker('destroy');
          $('#to_date1').datepicker({
            autoclose: true,
            startDate:from,
            todayHighlight:true,
            format: 'yyyy-mm-dd'
          });
          // $("#sub_total1").html("AED 0.00");
          // $("#commission1").html("AED 0.00");
          // $("#total1").html("AED 0.00");
        });
       }, 1000);
      }
    })

  });
</script>
