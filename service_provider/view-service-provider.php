<?php
require_once dirname(__DIR__) . '/controller/UserBranches.php';
$controller=new UserBranches();
$controller->init();
$page_data=$controller->pageViewUserData();
$title = $page_data['title'];
$page = $page_data['page'];
$total_sb=$page_data['total_sb'];
$total_p=$page_data['total_p'];
$total_wallet=$page_data['total_wallet'];
$payouts=$page_data['payouts'];
$early_paytouts=$page_data['early_paytouts'];
$total_hold=$page_data['total_hold'];
$wallet_txns=$page_data['wallet_txns'];
$done_ref_orders=$page_data['wallet_txns'];
$slots_booked=$page_data['slots_booked'];
$products=$page_data['products'];
$featured=$page_data['featured'];
$total_re=$page_data['total_re'];
$advertisements=$page_data['advertisements'];
$serviceProviderId=$page_data['user_id'];

$uid=$_GET['id'];
$user=$controller->getMemberInfo("service_provider",$uid);
include("includes/header.php"); 
?>
<!-- BEGIN sidebar -->
<style>
.service_provlink:hover{
  cursor:pointer;
}
</style>
<?php include('includes/sidebar.php');?>
<!--  //END sidebar -->
        <div class="main__content">
         
        <div class="pad_3">  
           
           <div class="detailsView"> 
                 <div class="col6">
                   <label>Name</label>
                   <span><?php echo $user['name'];?></span>
                 </div>
                 <div class="col6">
                   <label>Business Name</label>
                   <span><?php echo $user['business_name'];?></span>
                 </div> 
               <div class="col6">
                 <label>Email</label>
                 <span><?php echo $user['email'];?></span>
               </div>
               <div class="col6">
                 <label>Phone No</label>
                 <span><?php echo $user['mobile'];?></span>
               </div> 
            
             <div class="col6">
               <label>City</label>
               <span><?php echo $user['location_name'];?></span>
             </div>
             <div class="col6">
               <label>Date Of Registered</label>
               <span><?php echo date("Y-m-d",strtotime($user['created_time']));?></span>
             </div>
             <div class="col6">
               <label>Business Document</label>
               <span><a download href="<?php echo user_base_url.$user['business_licence'];?>" class="badge badge-primary" target="_blank"><i class="fa fa-download"></i> Download</a></span>
             </div>
             <div class="col6">
               <label>Membership Expiry</label>
               <span><?php echo ($user['membership_expiry']=='0000-00-00')?"No Membership":$user['membership_expiry'];?></span>
             </div>
             <div class="col6">
               <label>Account Status</label>
               <span><?php 
                switch($user['status']){
                  case 1:
                    echo '<label class="badge badge-success"><i class="fa fa-check"></i> Active</label>';
                    break;
                  case 2:
                    echo '<label class="badge badge-danger"><i class="fa fa-times"></i> Blocked</label>';
                    break;
                  case 3:
                    echo '<label class="badge badge-danger"><i class="fa fa-times"></i> Rejected</label>';
                    break;
                }
                ?></span>
             </div>
           </div>
           <div class="displayCards">
             <ul>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Total Slots Booked</p>
                   <h3><?php echo $total_sb;?></h3>
                 </div>
               </li>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Total Products Ordered</p>
                   <h3><?php echo $total_p;?></h3>
                 </div>
               </li>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Total Wallet</p>
                   <h3>AED <?php echo $total_wallet;?></h3>
                 </div>
               </li>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>On Hold Funds</p>
                   <h3>AED <?php echo $total_hold;?></h3>
                 </div>
               </li>
               <li>
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Rescheduled Slots</p>
                   <h3><?php echo $total_re;?></h3>
                 </div>
               </li>
            </ul>
            </div>
            <strong>
<h3>Wallet Tranasctions</h3></strong>
<br/>
           <table class="  " id="example">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Amount</th>
                    <th>Txn Type</th>
                    <th>Txn From</th>
                    <th>Date</th>
                    <th>SlotID/PayoutID</th>
                    <th>Remark</th>
                    <th>OrderID</th>
                  </tr>
                </thead>
                <tbody id="saloonTable">
                <?php 
                $i=1;
                foreach($wallet_txns as $prf){

                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['currency'].' '.$prf['amount'].'</td>';
                 
                  echo '<td>';
                  if($prf['type']=='debit'){
                    echo '<label class="badge badge-danger">Debit</label>';
                  }elseif($prf['type']=='credit'){
                    echo '<label class="badge badge-success">Credit</label>';
                  }else{
                    echo '<label class="badge badge-warning">Oops</label>';
                  }
                  echo '</td>';
                  echo '<td>'.$prf['amount_type'].'</td>';
                  echo '<td>'.$prf['date'].'</td>';
                  echo '<td>';
                  if($prf['amount_type']=='slot'){
                    echo $prf['slot_id'];
                  }elseif($prf['amount_type']=='withdraw'){
                    echo $prf['withdrawal_id'];
                  }else{
                    echo $prf['order_id'];
                  }
                  
                  echo '</td>';
                  echo '<td>'.$prf['remark'].'</td>';
                  echo '<td>'.$prf['order_id'].'</td>';

                  // echo '<td>
                  //           Admin Commission AED '.$prf['admin_commission'].'<br/>
                  //           Txn Charges AED '.$prf['transaction_charges'].'<br/>
                  //        </td>';

                  // echo '<td> <label class="badge badge-pill badge-danger">Refund Pending</label>';
                  // echo '</td>';
                  // echo '<td> <a  href="javascipt:;" data-oid="'.$prf['order_id'].'" class=" refund_now badge badge-warning"> <i class="fa fa-check"></i> Refund </a></td>';
                  echo '</tr>';
                }
                 ?>    
                </tbody>
              </table>


              <br/>
              <br/>

<h3>Products Ordered</h3></strong>
<br/>
           <table class="  " id="example2">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Order ID</th>
                    <th>No.Products</th>
                    <th>Total Amount</th>
                    <th>Commissions</th>
                    <th>Order Status</th>
                    <th>Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="saloonTable">
                <?php 
                $i=1;
                foreach($products as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['order_id'].'</td>';
                  echo '<td>'.$prf['total_products'].'</td>';
                  echo '<td><strong>AED '.$prf['total_amount'].'</strong></td>';
                  echo '<td> 
                  A.C <strong>AED '.$prf['admin_commission'].'</strong> <br/>
                  T.C <strong>AED '.$prf['transaction_charges'].'</strong> <br/>
                  </td>';
                  echo '<td>'; 
                  switch($prf['shipping_status']){
                    case 'pending':
                    case 'inprocess':
                      echo '<label class="badge badge-pill badge-warning">In Process</label>';
                      break;
                    case 'delivered':
                      echo '<label class="badge badge-pill badge-success">Delivered</label>';
                      break;
                    case 'shipped':
                      echo '<label class="badge badge-pill badge-primary">Shipped</label>';
                      break;
                    case 'cancelled':
                      echo '<label class="badge badge-pill badge-danger">Cancelled</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">In Process</label>';
                    break;
                  }
                  echo '</td>';
                  echo '<td>'.date("Y-M-d H:i A",strtotime($prf['date_of_order'])).'</td>';
                  echo '<td><button class="badge badge-primary view_products" data-oid="'.$prf['order_id'].'" >View Products</button></td>';
                  echo '</tr>';
                } ?>   
                </tbody>
              </table>

              <br/>
              <br/>
              
<h3>Featured Profile</h3></strong>
<br/>
           <table class="  " id="example4">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Days</th>
                    <th>Last Used Date</th>
                    <th>Clicks</th>
                    <th>Amount</th>
                    <th>Order ID</th>
                    <th>Status</th>
                    <th>Date of Create</th>
                  </tr>
                </thead>
                <tbody id="saloonTable">
                <?php 
                $i=1;
                foreach($featured as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td><span title="total days left">'.$prf['days'].'</span>/<span title="Total days purchased">'.$prf['total_days'].'</span></td>';
                  echo '<td>'.$prf['last_used_date'].'</td>';
                  echo '<td>'.$prf['clicks'].'</td>';
                  echo '<td><strong>AED '.$prf['amount'].'</strong></td>';
                  echo '<td>'.$prf['order_id'].'</td>';
                  echo '<td>'; 
                  switch($prf['status']){
                    case 'pending':
                    case 'inprocess':
                      echo '<label class="badge badge-pill badge-warning">In Process</label>';
                      break;
                    case 'active':
                      echo '<label class="badge badge-pill badge-success">Active</label>';
                      break;
                    case 'disable':
                      echo '<label class="badge badge-pill badge-warning">Disabled</label>';
                      break;
                    case 'expired':
                      echo '<label class="badge badge-pill badge-danger">Expired</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">Oops</label>';
                    break;
                  }
                  echo '</td>';
                  echo '<td>'.date("Y-M-d H:i A",strtotime($prf['created_time'])).'</td>';
                  echo '</tr>';
                } ?>   
                </tbody>
              </table>
              <br/>
              <br/>
              
<h3>Advertisements</h3></strong>
<br/>
           <table class="  " id="example5">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Banner</th>
                    <th>From Date</th>
                    <th>To Date</th>
                    <th>Clicks</th>
                    <th>Amount</th>
                    <th>City</th>
                    <th>Date of Create</th>
                    <th>Status</th>
                    <th>Ad Status</th>
                  </tr>
                </thead>
                <tbody id="saloonTable">
                <?php 
                $i=1;
                foreach($advertisements as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td><a href="'.user_base_url.$prf['banner'].'" target="_blank"><img src="'.user_base_url.$prf['banner'].'" width="200px" alt="advertiment banner" title="advertisement banner" /></a></td>';
                  echo '<td>'.$prf['from_date'].'</td>';
                  echo '<td>'.$prf['expiry_date'].'</td>';
                  echo '<td>'.$prf['clicks'].'</td>';
                  echo '<td><strong>AED '.$prf['amount'].'</strong></td>';
                  echo '<td>'.$prf['city_name'].'</td>';
                  echo '<td>'.date("Y-M-d H:i A",strtotime($prf['created_time'])).'</td>';
                  echo '<td>'; 
                  switch($prf['status']){
                    case 'pending':
                    case 'inprocess':
                      echo '<label class="badge badge-pill badge-warning">In Process</label>';
                      break;
                    case 'active':
                      echo '<label class="badge badge-pill badge-success">Active</label>';
                      break;
                    case 'disable':
                      echo '<label class="badge badge-pill badge-warning">Disabled</label>';
                      break;
                    case 'expired':
                      echo '<label class="badge badge-pill badge-danger">Expired</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">Oops</label>';
                    break;
                  }
                  echo '</td>';
                  echo '<td>'; 
                  switch($prf['approve_status']){
                    case 'inprocess':
                      echo '<label class="badge badge-pill badge-warning">In Process</label>';
                      break;
                    case 'approved':
                      echo '<label class="badge badge-pill badge-success">Approved</label>';
                      break;
                    case 'pending':
                      echo '<label class="badge badge-pill badge-warning">Pending</label>';
                      break;
                    case 'rejected':
                      echo '<label class="badge badge-pill badge-danger">Rejected</label>';
                      break;
                    default:
                    echo '<label class="badge badge-pill badge-warning">Oops</label>';
                    break;
                  }
                  echo '</td>';
                  echo '</tr>';
                } ?>   
                </tbody>
              </table>

              <br/>
              <br/>
<h3>Slots Booked </h3></strong>
<br/>
           <table class="  " id="example6">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Slot ID</th>
                    <th>Order ID</th>
                    <th>User</th>
                    <th>Service</th>
                    <th>Service At</th>
                    <th>Amount</th>
                    <th>Commissions</th>
                    <th>City</th>
                    <th>Status</th>
                    <th>Date of Slot</th>
                  </tr>
                </thead>
                <tbody id="saloonTable">
                <?php 
                $i=1;
                foreach($slots_booked as $prf){
                  echo '<tr>';
                  echo '<td>'.$i++.'</td>';
                  echo '<td>'.$prf['service_slot_id'].'</td>';
                  echo '<td>'.$prf['order_id'].'</td>';
                  echo '<td class="service_provlink" onclick="window.location.href=\''.admin_base_url.'view-user.php?id='.$prf['user_id'].'\';">'.$prf['user_name'].'</td>';
                  echo '<td>'.$prf['service_name'].'</td>';
                  echo '<td>'.$prf['service_type'].'</td>';
                  echo '<td><strong>AED '.$prf['total_amount'].'</strong></td>';
                  echo '<td> 
                  A.C <strong>AED '.$prf['admin_commission'].'</strong> <br/>
                  T.C <strong>AED '.$prf['transaction_charges'].'</strong> <br/>
                  </td>';
                  echo '<td>'.$prf['city_name'].'</td>';
                  echo '<td>'; 
                  switch($prf['service_status']){
                    case 'pending':
                    case 'inprocess':
                      echo '<label class="badge badge-pill badge-warning">Pending</label>';
                      break;
                    case 'completed':
                      echo '<label class="badge badge-pill badge-success">Completed</label>';
                      break;
                    case 'rescheduled':
                      echo '<label class="badge badge-pill badge-primary">Rescheduled</label>';
                      break;
                    case 'cancelled':
                      echo '<label class="badge badge-pill badge-danger">Cancelled</label>';
                      break;
                  }
                  echo '</td>';
                  echo '<td>'.date("Y-M-d H:i A",strtotime($prf['slot_date'])).'</td>';
                  echo '</tr>';
                } ?>   
                </tbody>

              </table>

        </div>


      </main>
      <!--  //END main -->

      <!-- <div class="layout__container-after"></div> -->

    </div>

    <?php include 'includes/footer.php';?>

    <!-- partial -->
    <div class="modal fade pswdModal" id="refundPOP" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Reject Early Payout</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="rejectEP" />
              <input type="hidden" name="id" id="id" value="" />
              <input type="hidden" name="sid" id="sid" value="" />
                <div class="form-group">
                  <textarea type="text" name="reason" class="form-control" required></textarea>
                  <label class="form-control-placeholder p-0" for="refund_amount">Reason</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Reject Now</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- end popup -->

    
    <!-- partial -->
    <div class="modal fade pswdModal" id="rejectAD" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Reject Advertiment</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="rejectAD" />
              <input type="hidden" name="id" id="id_a" value="" />
              <input type="hidden" name="sid" id="sid_a" value="" />
                <div class="form-group">
                  <textarea type="text" name="reason" class="form-control" required></textarea>
                  <label class="form-control-placeholder p-0" for="refund_amount">Reason</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Reject Now</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- end popup -->
    
    <!-- order products partial -->
    <div class="modal fade pswdModal" id="orderedProducts" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Ordered Products</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <input type="hidden" name="type" value="updateOrderStatus" />
              <input type="hidden" name="oid" id="oid" value="" />
                <div class="row mb-5">
<div class="col-md-12">

<table class="product_cls" id="product_cls">
<tr><th>Product Name</th><th>Image</th><th>Price</th><th>QTY</th></tr>
<tr><td>Product Name</td><td>Image</td><td>Price</td><td>QTY</td></tr>
</table>
</div>
                </div>


                <div class="form-group">
                </div>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->

    <!-- partial -->
    <div class="modal fade pswdModal" id="walletAC" tabindex="-1" role="dialog"
      aria-labelledby="addSubCategoryTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addSubCategoryTitle">Wallet Action</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post">
              <input type="hidden" name="type" value="walletAction" />
              <input type="hidden" name="id" id="id_w" value="" />
              <div class="form-group">
                  <select name="action_type" id="action_type" class="form-control">
                    <option value="credit">Credit</option>
                    <option value="Dredit">Debit</option>
                  </select>
                  <label class="form-control-placeholder p-0" for="refund_amount">Action</label>
                </div>
                <div class="form-group">
                  <input type="text" name="amount" class="form-control" required />
                  <label class="form-control-placeholder p-0" for="refund_amount">AED Amount</label>
                </div>
                <div class="form-group">
                  <textarea type="text" name="remark" class="form-control" required></textarea>
                  <label class="form-control-placeholder p-0" for="refund_amount">Remark</label>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Submit</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- end popup -->
</body>
<script>
$("#search_type").change(function(){
  var type=$(this).val();
  $(".search_by_other").remove();
  if(type=='by_email'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }else if(type=='by_phone'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Phone">');
  }else if(type=='by_customerid'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User ID">');
  }else{
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter User Email">');
  }
});
$("#example").DataTable({
  // searching:false
  "language": {
      "emptyTable": "There is no wallet transactions."
    }
});

$("#example1").DataTable({
  // searching:false
  "language": {
      "emptyTable": "There is no early payouts requests."
    }
});
$("#example2").DataTable({
  // searching:false
  "language": {
      "emptyTable": "There is no products orders till now."
    }
});
$("#example3").DataTable({
  // searching:false
  "language": {
      "emptyTable": "There is no payout information."
    }
});
$("#example4").DataTable({
  // searching:false
  "language": {
      "emptyTable": "There is no featured profiles data"
    }
});
$("#example5").DataTable({
  // searching:false
  "language": {
      "emptyTable": "There is no advertisements data"
    }
});
$("#example6").DataTable({
  // searching:false
  "language": {
      "emptyTable": "There is no Slots data"
    }
});
$(".wallet_action").click(function(){
  var id=$(this).data("id");
  $("#id_w").val(id);
  $("#walletAC").modal('show');
})
$(".reject_ad").click(function(){
  var id=$(this).data("id");
  var sid=$(this).data("sid");
  $("#id_a").val(id);
  $("#sid_a").val(sid);
  $("#rejectAD").modal('show');
})
$(".reject_now").click(function(){
  var id=$(this).data("id");
  var sid=$(this).data("sid");
  $("#id").val(id);
  $("#sid").val(sid);
  $("#refundPOP").modal('show');
})
$(".view_products").click(function(){
  var oid=$(this).data("oid");
  $("#orderedProducts").modal('show');
  $.ajax({
    url:"<?php echo user_base_url.'view-service-provider.php';?>",
    method:"POST",
    data:{oid:oid,type:"getOrderedProducts"},
    success:function(data){
      $("#product_cls").html(data);
    }
  })
})
</script>
</html>