<?php
require_once dirname(__DIR__) . '/controller/UserDashboard.php';
$controller = new UserDashboard();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$total_slots=$page_data['total_slots'];
$today_slots=$page_data['today_slots'];
$upcoming_slots=$page_data['upcoming_slots'];
$wallet=$page_data['wallet'];
$on_hold_balance=$page_data['on_hold_balance'];
$pending_product_orders=$page_data['pending_product_orders'];
$products_chart=$page_data['products_chart'];
$slots_chart=$page_data['slots_chart'];
$title = $page_data['title']; $page = $page_data['page']; 
$cities=$controller->getCitiesList();
include("includes/header.php");
?>
<style>
._Chart canvas{
		/* max-width: 400px!important;
		max-height: 280px!important; */
	 }
	 .d-flex.my_Flx {
		justify-content: flex-start;
		align-items: flex-end;
		flex-wrap: wrap;
	}
	.d-flex.my_Flx>div {
		flex: 0 0 33%;
		max-width: 33%;
		padding: 1em
	}
</style>
	<?php include("includes/sidebar.php");?>
<div class="main__content">

          <div class=" f-right">
            <!-- <button class="btn login-btn" data-toggle="modal" data-target="#addLocation" id="myButton">Add &nbsp;<i class="fa fa-plus" aria-hidden="true"></i></button> -->

          </div>
		  
		  <div class="displayCards">
             <ul>
               <li  onclick="window.location.href='<?php echo user_base_url.'service-bookings.php';?>';">
                 <span>
                   <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                 </span>
                 <div>
                   <p>Total Slots Booked</p>
                   <h3><?php echo $total_slots;?></h3>
                 </div>
               </li>
               <li onclick="window.location.href='<?php echo user_base_url.'services-for-today.php';?>';">
                <span>
				<i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                </span>
                <div>
                  <p>Today Slots</p>
                  <h3><?php echo $today_slots;?></h3>
                </div>
              </li>
			  <li onclick="window.location.href='<?php echo user_base_url.'service-bookings.php';?>';">
                <span>
				<i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                </span>
                <div>
                  <p>Upcoming Slots</p>
                  <h3><?php echo $upcoming_slots;?></h3>
                </div>
              </li>
			  <li  onclick="window.location.href='<?php echo user_base_url.'transactions.php';?>';">
                <span>
				<i class="fa fa-credit-card fa-2x" style="color:white;"></i>
                </span>
                <div>
                  <p>Wallet</p>
                  <h3>AED <?php echo $wallet;?></h3>
                </div>
              </li>

              <li  >
                <span>
				<i class="fa fa-credit-card fa-2x" style="color:white;"></i>
                </span>
                <div>
                  <p>On Hold Wallet</p>
                  <h3>AED <?php echo $on_hold_balance;?></h3>
                </div>
              </li>
              <li onclick="window.location.href='<?php echo user_base_url;?>product_orders.php';">
                <span>
				          <i class="fa fa-superpowers fa-2x" style="color:white;"></i>
                </span>
                <div>
                  <p>P.Product Orders</p>
                <h3><?php echo $pending_product_orders;?></h3>
                </div>
              </li>
            
			</ul>
     <center> <canvas id="line-chart"  style="max-width:900px !important;" ></canvas></center>
			</div>
     
        </div>
<?php include("includes/footer.php");?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script>
new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: <?php echo json_encode($slots_chart['dates']);?>,
    datasets: [{ 
        data: <?php echo json_encode($slots_chart['amount']);?>,
        label: "Slots Amount",
        borderColor: "#8e5ea2",
        fill: false
      }, { 
        data: <?php echo json_encode($products_chart['amount']);?>,
        label: "Products Amount",
        borderColor: "#3cba9f",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Revenue ( Last 10 days ) AED'
    }
  }
});

</script>