<?php 
	session_start();
	//Include Files
	include("class/dbConnection.php");
	include("class/serviceProvider.php");
	
	//Retrieving Form Fields
	$email = $_REQUEST["email"];
	$password = $_REQUEST["password"];
	
	 //Creating dbObject for dbConnection
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		//Creating admin Object
		
		$serviceProviderObj = new serviceProvider();
		$message = $serviceProviderObj->checkServiceProviderDetails($email,$password,$con);
               
		
		if($message == "correct")
		{
			$serviceProviderId = $serviceProviderObj->getServiceProviderId($email,$con);
			$status=$serviceProviderId['status'];
			if($status==1){
				$_SESSION["serviceProviderId"] = $serviceProviderId['service_provider_id'];
				$_SESSION["branch_type"] = $serviceProviderId['user_type'];
				header('Location:dashboard.php');
			}else{
				if($status==0){
					$_SESSION['message'] = 'Your account activation in process . try again later.';
				}elseif($status==3){
					$_SESSION['message'] = 'Your account got rejected please contact us.';
				}else{
					$_SESSION['message'] = 'Your account has been blocked please contact us.';
				}
				header("Location:index.php");
			}
		}
		else
		{
			$_SESSION['message'] = $message;
			header("Location:index.php");
		} 
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>