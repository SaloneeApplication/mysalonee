<?php
require_once dirname(__DIR__) . '/controller/UserProducts.php';
$controller = new UserProducts();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$categories=$controller->getCategories();
$products=$controller->getProducts();
$member_access=$controller->getMemberAccess();
include("includes/header.php"); 
?>

	<?php include("includes/sidebar.php");?>
  <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <div class="main__content">
          <div class="col-md-12">
            <div class="w-50">
              <div class="input-group mb-3">
                <input type="text" class="form-control _search"  id="searchTd" placeholder="Search">
                <div class="input-group-append">
                  <button class="btn theme-btn" type="submit">Go</button>
                </div>
              </div>
            </div>
            <div class="pull-right rightBtn">
              <button class="btn login-btn" data-toggle="modal"  <?php echo ($member_access=='1')?'data-target="#addDiscount"':'onclick="window.location.href=\'upgrade.php\'"';?>  id="myButton">Add &nbsp;<i
              class="fa fa-plus" aria-hidden="true"></i></button>
            </div>
          </div>

<div class="pad_3">  
          <div class="table-responsive">

            <table class="table themeTable" >
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Image</th>
                  <th>Product Name</th>
                  <th>For Category</th>
                  <th>Base Price</th>
                  <th>Sale Price</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="saloonTable"> 
               <?php
                $i=1;
               foreach($products as $pro){ 
                 echo '<tr>';
                  echo '<td>'.$i.'</td>';
                  echo '<td><img src="'.user_base_url.$pro['product_image'].'" width="100px" /></td>';
                  echo '<td>'.$pro['product_title'].'</td>';
                  echo '<td>'.$pro['category_name'].'</td>';
                  echo '<td>'.$pro['base_price'].'</td>';
                  echo '<td>'.$pro['sale_price'].'</td>';
                  echo '<td>';
                  if($pro['product_status']=='active'){
                    echo '<label class="badge badge-success badge-pill">Active</label>';
                  }else{
                    echo '<label class="badge badge-danger badge-pill">Inactive</label>';
                  }
                  echo '</td>';
                echo '<td>';
                if($pro['product_status']=='active'){
                  echo '<a href="'.user_base_url.'products.php?type=disableProduct&pid='.$pro['id'].'" class="badge badge-danger"><i class="fa fa-times"></i> Disable</a>';
                }else{
                  echo '<a href="'.user_base_url.'products.php?type=enableProduct&pid='.$pro['id'].'" class="badge badge-success"><i class="fa fa-check"></i> Enable</a>';
                }
                echo '&nbsp;<a href="javascript:;" onclick="editProduct('.$pro['id'].')" class="badge badge-primary"><i class="fa fa-edit"></i> Edit</a>';
                
                echo '</td>';
                 echo '</tr>';
                $i++;
               }
?> 

              </tbody>
            </table>
          </div>
          <div class="clearfix"></div>
            <div class='pagination-container tablePaging'>
                  <div class="pagination">
                    <ol id="numbers"></ol>
                  </div>
            </div>
     </div>
</div>

    <!-- partial -->
    <div class="modal fade pswdModal" id="addDiscount" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Add Product</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" name='categor_im' enctype="multipart/form-data" method="post"> 
              <input type="hidden" name="type" value="addNewProduct" />
                <div class="row mb-5">
                  <div class="col-md-12">
                    <div class="form-group ">
                      <input type="text" id="product_name" name="product_name" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="product_name">Product Name</label>
                    </div>
                  </div>
<div class="col-md-12">
<div class="form-group">
                    <select name="category" class="form-control" autocomplete="off" required >
                      <option hidden   value> -- Select Category  -- </option>
                      <option value="0">For All Categories</option>
                      <?php foreach($categories as $cat){ 
                     echo  '<option value="'.$cat['category_id'].'">For '.$cat['name'].' Only</option>';  } ?>
                    </select>
                  </div>
</div>
                  
                  <div class="col-md-12">
                    <div class="form-group ">
                      <input type="text" id="base_price " name="base_price" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="base_price">Base Price</label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group ">
                      <input type="text" id="sale_price " name="sale_price" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="sale_price">Sale Price</label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group ">
                      <textarea type="text" id="description " name="description" class="form-control" autocomplete="off" required></textarea>
                      <label class="form-control-placeholder p-0" for="description">Description</label>
                    </div>
                  </div>
                  <div class="col-md-12">
                  <div class="form-group">
                    <label for="file-upload" class="custom-file-upload">
                      UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                    </label>
                    <input id="file-upload" name='upload_cont_img' type="file" style="display:none;">
                  </div>
                  </div>

                </div>


                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Submit</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->

    
    <!-- edit produt  -->
    <div class="modal fade pswdModal" id="editProduct" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Edit Product</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" name='categor_im2' enctype="multipart/form-data" method="post"> 
              <input type="hidden" name="type" value="editProduct" />
                <div class="row mb-5" id="edit_data">


                </div>


                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Update</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->
<?php include("includes/footer.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script>
$(document).ready(function(){
        $("#searchTd").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#saloonTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      }); 
     $("form[name='categor_im']").submit(function(e){
        if(typeof $('#file-upload')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select File',
          });
          e.preventDefault();
          return false;
        }
      })

      function editProduct(pid){
        $("#editProduct").modal('show');
        $.ajax({
          url:"<?php echo user_base_url.'products.php';?>",
          method:"POST",
          data:{pid:pid,type:"getEditProduct"},
          success:function(data){
            $("#edit_data").html(data);
          }
        })

      }
</script>