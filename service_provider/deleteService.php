<?php 
	session_start();
	
	//Included Files
	
	include("class/serviceProvider.php");	
	include("class/dbConnection.php");


	//Retrieving Session Variable
	
	if(isset($_SESSION["serviceProviderId"]))
	{
		
		$serviceProviderId = $_SESSION["serviceProviderId"];
		
		//echo $serviceProviderId;
	}
	else
	{
		header('Location:index.php?message=Unauthorized%20Access%20Prohobited');
	}
	
	//Retrieving Form Fields
	//Creating dbObject 
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	if($con)
	{
		$serviceProviderServiceId = $_REQUEST['id'];
		$serviceProviderObj = new serviceProvider();
		
		$deleteResult = $serviceProviderObj->deleteServiceProviderService($serviceProviderServiceId,$con);
		if($deleteResult)
		{
			return true;
		}
		else
		{
			return false;
		}
		
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
	

?>