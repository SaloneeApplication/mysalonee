<?php 
	session_start();
	
	//Included Files
	
	include("class/serviceProvider.php");	
	include("class/dbConnection.php");


	//Retrieving Session Variable
	
	if(isset($_SESSION["serviceProviderId"]))
	{
		
		$serviceProviderId = $_SESSION["serviceProviderId"];
		
		//echo $serviceProviderId;
	}
	else
	{
		header('Location:index.php?message=Unauthorized%20Access%20Prohobited');
	}
	
	//Retrieving Form Fields
	$name = $_REQUEST["name"];
	$businessName = $_REQUEST["businessname"];
	$mobile = $_REQUEST["mobile"];
	$tempImage = $_FILES['filUpload111']['tmp_name'];
	if($tempImage != "")
	{
		$target_path = "uploads/";
		$target_path = $target_path . basename( $_FILES['filUpload111']['name']);
		$file_path = basename( $_FILES['filUpload111']['name']);
		
		//$extension = end(explode(".", $file_path));
		$explode = explode(".", $_FILES["filUpload111"]["name"]);
        $extension = end($explode);
		$image = time().".".$extension;
		move_uploaded_file($_FILES['filUpload111']['tmp_name'], "uploads/".$image);
	
	}
	else
	{
		$image = $_REQUEST["hidImage1"];
	}
	
	//Creating dbObject 
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	if($con)
	{
		$serviceProviderObj = new serviceProvider();
		
		$updateResult = $serviceProviderObj->updateServiceProviderProfile($serviceProviderId,$name,$businessName,$mobile,$image,$con);
		if($updateResult)
			{
				$_SESSION['success'] = 'Service Provider Profile Updated Successfully';
				header('Location:dashboard.php');
			}
			else
			{
				$_SESSION['failure'] = 'Oops! Something Went Wrong';
				header('Location:dashboard.php');
			}
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
	

?>