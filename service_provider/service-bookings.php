<?php
require_once dirname(__DIR__) . '/controller/UserBookings.php';
$controller = new UserBookings();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$services=$controller->getServiceSlots();
include("includes/header.php");
$timings=$controller->getSaloneeTimings();
$disabled_weeks='';
$days=array("monday"=>1,"tuesday"=>2,"wednesday"=>3,"thursday"=>4,"friday"=>5,"saturday"=>6,"sunday"=>0);
foreach($timings as $time){
  if($time['available_status']==0){
    $disabled_weeks.=$days[$time['day_name']];
  }
}

?>
	<?php include("includes/sidebar.php");?>
  <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    
    <div class="main__content">
      <div class="col-md-12">
        <div class="w-50">
          <form action="" method="post">
            <div class="input-group mb-3 input-daterange">
                <div class="input-group-append append_form_data">
                  <select name="search_type" id="search_type" class="btn theme-btn">
                    <option value="by_orderid" selected>By Order ID</option>
                    <option value="by_customerid">By Customer ID</option>
                    <option value="by_date">By Date</option>
                  </select>
                </div>
                <input type="text" class="form-control search_by_other" name="search" id="" placeholder="Enter Order ID">

                <div class="input-group-append">
                  <button class="btn theme-btn" type="submit">Search</button>
                </div>
            </div>
          </form>
        </div> 
        <div class="w-50">
            
            <div class="input-group mb-3">
              <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
              <div class="input-group-append">
                <button class="btn theme-btn" type="submit">Go</button>
              </div>
            </div>
        </div>
      </div>
          <!--
        <div class="rightBtn">
          <button class="btn login-btn" data-toggle="modal" data-target="#addSlots" id="myButton">Add &nbsp;<i
              class="fa fa-plus" aria-hidden="true"></i></button>
        </div> -->
<br>
        <div class="pad_3">  
          <div class="table-responsive">

            <table class="table themeTable" >
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Customer</th>
                  <th>Service</th>
                  <th>Type</th>
                  <th>Date</th>
                  <th>Time Slot</th>
                  <th>Amount</th>
                  <th>Order ID</th>
                  <th>Location</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody id="saloonTable"> 
                  <?php


$i=1;
foreach($services as $s){
    echo '<tr>';
    echo '<td>'.$i.'</td>';
    echo '<td>'.$s['name'].'</td>';
    echo '<td>'.$s['category_name'].'( '.$s['service_name'].' )</td>';
    echo '<td>At '.ucwords($s['service_type']).'</td>';
    echo '<td>'.date("Y-m-d",strtotime($s['slot_date'])).'</td>';
    echo '<td>'.date("H:i A",strtotime($s['slot_date'])).'</td>';
    echo '<td>'.$s['total_amount'].'</td>';
    echo '<td>'.$s['order_id'].'</td>';
    echo '<td>'.$s['service_address'].'</td>';
    echo '<td>';
      if($s['service_status']=='pending'){
        echo '<span class="badge badge-pill badge-warning">Pending</span>';
      }elseif($s['service_status']=='completed'){
        echo '<span class="badge badge-pill badge-success">Completed</span>';
      }elseif($s['service_status']=='inprocess'){
        echo '<span class="badge badge-pill badge-warning">InProcess</span>';
      }elseif($s['service_status']=='cancelled'){
        echo '<span class="badge badge-pill badge-danger">Cancelled</span>';
      }else{
        echo '<span class="badge badge-pill badge-danger">Oops</span>';
      }
    echo '</td>';
    echo '<td>';
    if($s['service_status']=='pending'){
      echo '<a class="badge badge-primary " onclick="reschdule('.$s['slot_id'].')" data-slot_id="'.$s['slot_id'].'" href="javascript:;"><i class="fa fa-calendar"></i> Reschdule</a>';
      echo '<a class="badge badge-success " onclick="slotcompleted('.$s['slot_id'].')" data-slot_id="'.$s['slot_id'].'"  href="javascript:;"><i class="fa fa-check"></i> Completed</a>';
    }else{

    }
    echo '</td>';
    echo '</tr>';
    $i++;
}

?>
              </tbody>
            </table>

          </div>
          <div class="clearfix"></div>

          <!-- pagination  -->
            <div class='pagination-container tablePaging'>              
                  <div class="pagination">
                    <ol id="numbers"></ol>
                  </div>                
            </div>


        </div>
        
      </div>

<!-- reschedule timings start -->
<div class="modal fade pswdModal" id="addTimings" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Slot Reschedule</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form method = "post" action = "">
                <input type="hidden" name="type" value="rescheduleSlotTime" />
                <input type="hidden" name="sid" id="sid" value="" />
                <div class="form-group">
                  <input type="text"  id="slot_date" name = "slot_date" class="form-control" autocomplete="off" />
                  <label class="form-control-placeholder p-0"  for="slot_date">Slot Date</label>
                </div>
                <div class="form-group">
                <div class="boxedList" id="slots_data" style="display:none;">
                                    <div class="flxRow">
                                        <p>Select Available Slot</p> 
                                    </div>
                                        <div id="slotsList" style="">
                                        <ul>
                                            
                                        </ul>
                                    </div>
                                    </div>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn theme-btn"><i class="fa fa-save"></i> Save</button>
                </div>
              </form>
            </div>
          </div>

        </div>
      </div>
    </div>
<!-- reschedule timings end -->
<?php include("includes/footer.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<style>

</style>
<script>
const disabled_weeks="<?php echo $disabled_weeks;?>";
$(document).ready(function(){
        $("#searchTd").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#saloonTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      }); 
if(disabled_weeks!=''){
  $('#slot_date').datepicker({
    format: 'yyyy-mm-dd',
    daysOfWeekDisabled:disabled_weeks,
    todayHighlight:true,
    autoclose:true,
    startDate:"<?php echo date("Y-m-d");?>",
    endDate :"<?php echo date("Y-m-d",strtotime("+20 days"));?>"
  });
}else{
  $('#slot_date').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight:true,
    autoclose:true,
    startDate:"<?php echo date("Y-m-d");?>",
    endDate :"<?php echo date("Y-m-d",strtotime("+20 days"));?>"

  });
}
function slotcompleted(sid){
  $.ajax({
    url:"<?php echo user_base_url."services-for-today.php";?>",
    method:"POST",
    data:{type:"slotCompleted",sid:sid},
    success:function(data){
      var data=JSON.parse(data);
      if(data.status==200){
        location.reload();
      }else{
        swal("", data.msg, "error");
        return false;
      }
    }
  });
}
function reschdule(sid){
  $("#sid").val(sid);
  $("#addTimings").modal('show');

}
$("#slot_date").change(function(){
  $("#slots_data").show();
  $("#slotsList ul").html('<center>Loading...</center>');
  var sid=$("#sid").val();
  var slot_date=$(this).val();
  $.ajax({
    url:"<?php echo user_base_url."services-for-today.php";?>",
    method:"POST",
    data:{type:"getSlots",sid:sid,slot_date:slot_date},
    success:function(data){
      $("#slots_data").show();
      $("#slotsList ul").html(data);
    }
  });
});

$(".close").click(function(){
  $("#slot_date").val('');
  $("#slots_data").hide();
  $("#slotsList ul").html('');
});
$("#search_type").change(function(){
  var type=$(this).val();
  $(".search_by_other").remove();
  $(".search_by_date").remove();
  if(type=='by_orderid'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter Order ID">');
  }else if(type=='by_customerid'){
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter Customer ID">');
  }else if(type=='by_date'){
    $(".append_form_data").after('<input type="text" name="search_from" id="search_from" class="form-control search_by_date search_by_date2" value="<?php echo date("Y-m-d");?>"><div class="input-group-addon search_by_date">to</div><input type="text" name="search_to"  class="form-control search_by_date search_by_date2" value="<?php echo date("Y-m-d");?>">');

    $('.search_by_date2').each(function() {
      $(this).datepicker({format: 'yyyy-mm-dd', todayHighlight:true,
    autoclose:true});
    });
  }else{
    $(".append_form_data").after('<input type="text" name="search" class="form-control search_by_other"  placeholder="Enter Order ID">');
  }
});
</script>