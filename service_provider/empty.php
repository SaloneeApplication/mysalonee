<?php
require_once dirname(__DIR__) . '/controller/UserUpgrade.php';
$controller = new UserUpgrade();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$page = $page_data['page']; 
$promocodes=$controller->getPromocodes();
include("includes/header.php");
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	if($con)
	{
		$serviceProviderObj = new serviceProvider();
		
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>

	<?php include("includes/sidebar.php");?>
    <style>input[type=date]:required:invalid::-webkit-datetime-edit {
    color: transparent;
}
input[type=date]:focus::-webkit-datetime-edit {
    color: black !important;
}
    </style>
    <div class="main__content">



        </div>

<!-- partial -->
  <div class="modal fade pswdModal" id="addPromoCode" tabindex="-1" role="dialog" aria-labelledby="addPromoCodeTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="col-12 modal-title text-center" id="addPromoCodeTitle">Add Promo Code</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="login">
            <form action="" method="post" enctype="multipart/form-data" name="categor_im">
            <input type="hidden" name="type" value="insertPromocode" />
              <div class="form-group">
                <input type="text" id="code" name="code" class="form-control" autocomplete="off" required>
                <label class="form-control-placeholder p-0" for="code">Code</label>
              </div>
              <div class="form-group">
                <textarea type="text" id="desc" name="desc" rows="5" class="form-control" autocomplete="off" required></textarea>
                <label class="form-control-placeholder p-0" for="accountNumber">Code Description</label>
              </div>
              <div class="form-group">
                <input type="text" name="discount" id="discount" class="form-control" autocomplete="off" required>
                <label class="form-control-placeholder p-0" for="discount">Discount Percentage</label>
              </div>
              <div class="form-group"  >
                <select name="only_for" id="only_for" required  class="form-control" autocomplete="off" >
                  <option hidden disabled selected value> -- Select Only For-- </option>
                  <option value="new_users">For New Users</option>
                  <option value="all">For All</option>
                </select>
              </div>
              <div class="form-group">
                <input type="date" name="expdate" id="expdate" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="expdate">Expiry Date( for empty no expiry )</label>
              </div>
              <div class="form-group">
                <label for="file-upload" class="custom-file-upload">
                  UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
                </label>
                <input id="file-upload" name='upload_cont_img' type="file" style="display:none;">
              </div>

              <div class="form-group">
                <button type="submit" class="btn theme-btn">Submit</button>
              </div>
            </form>
          </div>

        </div>

      </div>
    </div>
  </div>


  <!-- partial -->		
<?php include("includes/footer.php");?>

<script>
     $("form[name='categor_im']").submit(function(e){
        if(typeof $('#file-upload')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select category image',
          });
          e.preventDefault();
          return false;
        }
      });
</script>