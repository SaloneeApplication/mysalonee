
<?php
require_once dirname(__DIR__) . '/controller/UserCategories.php';
$controller = new UserCategories();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$categories=$controller->getAllCategories();
$user_categories=$controller->getUserCategoires();
$member_access=$controller->getMemberAccess();
include("includes/header.php");?>
<?php 
	$dbObject = new dbConnection();
	$con = $dbObject->getConnection();
	if($con)
	{
		$serviceProviderObj = new serviceProvider();
		$recordSet2 = $serviceProviderObj->getServiceProviderCategories($serviceProviderId,$con);
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>

	<?php include("includes/sidebar.php");?>
<div class="main__content">
	<div class="col-md-12">
		<div class="w-50">
			<div class="input-group mb-3">
				<input type="text" class="form-control _search" id="searchTd" placeholder="Search">
				<div class="input-group-append">
					<button class="btn theme-btn" type="submit">Go</button>
				</div>
			</div
		</div>
		<div class="pull-right rightBtn">
			<button class="btn login-btn" data-toggle="modal" data-target="#addCategory" id="myButton">Add &nbsp;<i
				class="fa fa-plus" aria-hidden="true"></i></button>
		</div>
	</div>
	<br>
	<div class="clearfix"></div>
	<ul class="paginationTable salonee_category category-card">
	    <?php
	   foreach($user_categories as $row2)
		{?>
    		<li class=" tableItem">
    			<span><img src="<?php echo admin_base_url;?><?php echo $row2['image'];?>" width = "90px" height = "80px" alt="category"></span>
    			<p>
    				<b><?php echo $row2['name'].' <custom style="font-size:14px;font-weight:600;">[ '.ucwords($row2['category_for']).' ]</custom>';?></b>
    				<?php echo $row2['short_description'];?>
    			</p>
				<?php 
					if($row2['status']==1){
						?>
						<small style='color:red;' onclick="window.location.href='<?php echo user_base_url;?>category.php?sid=<?php echo $row2['service_provider_category_id'];?>&status=0&type=disablecategory';"><i class="fa fa-eye-slash" title="disable" aria-hidden="true" data-id = '<?php echo $row2[0];?>'></i></small>
						<?php
					}else{
						?>
						<small style='color:green;'  onclick="window.location.href='<?php echo user_base_url;?>category.php?sid=<?php echo $row2['service_provider_category_id'];?>&status=1&type=enablecategory';"><i class="fa fa-eye" title="enable" aria-hidden="true" data-id = '<?php echo $row2[0];?>'></i></small>
						<?php
					} 
				?>
    			
    		</li>
    	<?php
		}?>
	</ul>
	<div class="pagination-container">
		<p class='paginacaoCursor beforePagination' id=""><i class="fa fa-chevron-left" aria-hidden="true"></i></p>
		<p class='paginacaoCursor afterPagination' id=""><i class="fa fa-chevron-right" aria-hidden="true"></i></p>
	</div>
</div>

<div class="modal fade pswdModal" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="addCategoryTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
    	<div class="modal-content">
    		<div class="modal-header">
    			<h5 class="col-12 modal-title text-center" id="addCategoryTitle">Add Category</h5>
    			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    			<span aria-hidden="true">&times;</span>
    			</button>
    		</div>
    		<div class="modal-body">
    			<div class="login">
    				<form action="" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="type" value="insertCategory" />
    					<div class="form-group mt-4 mb-5">
    				<select multiple name="category[]" class="form-control selectpicker w-100" autocomplete="off" required>
                    <option hidden disabled selected value> -- select an option -- </option>
                    <?php 
						foreach($categories as $row1)
						{
							$categoryId = $row1['category_id'];
							$category = $row1['name'];
							$category_for = $row1['category_for'];
							
							echo "<option value = '$categoryId'>$category <font color='gray' style='font-size:12px;font-weight:500;color:gray;'>[ For ".ucwords($category_for)." ]</font></option>";
						}	
					?>

                  </select>
    					</div>
    					
    					<div class="form-group">
    						<input type="submit" name="add_category" value="Submit" class="btn theme-btn">
    					</div>
    				</form>
    			</div>
    		</div>
    	</div>
    </div>
</div>
<?php include("includes/footer.php");?>
<script>
    $("#searchTd").on("keyup", function() {
         value = $(this).val().toLowerCase(); //alert(value);
        jQuery(".salonee_category li").each(function () {
        if (jQuery(this).text().search(new RegExp(value, "i")) < 0) {
            jQuery(this).hide();
        } else {
            jQuery(this).show()
        }
    });
         
        });
        
        var filter = jQuery(this).val();
    
         
      $(".fa-trash").click(function () {
		  var id = $(this).data('id');

        swal({
          title: "Are you sure?",
          text: "You want to delete this Category",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
        }).then(result => {
          if (result.dismiss === 'cancel') {
            swal.closeModal();
            return;
          }
		  else
		  {
			  $.ajax({
                type:'POST',
                url:'deleteCategory',
                data:'id='+id,
                success:function(html){
					
                   swal({
					type: "success",
					text: 'Category has been deleted!',
					showConfirmButton: false,
					timer: 1500
				  });
				   setInterval('location.reload()', 1500); 
                    
                }
            }); 
		  }
          

        },
        )
      });
         
        
    </script>
