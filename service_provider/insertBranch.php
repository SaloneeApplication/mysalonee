<?php 
	session_start();
	
	//Included Files
	
	include("class/serviceProvider.php");	
	include("class/dbConnection.php");


	//Retrieving Session Variable
	
	if(isset($_SESSION["serviceProviderId"]))
	{
		
		$serviceProviderId = $_SESSION["serviceProviderId"];
		
		//echo $serviceProviderId;
	}
	else
	{
		header('Location:index.php?message=Unauthorized%20Access%20Prohobited');
	}
	
	//Retrieving Form Fields
	//Creating dbObject 
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	if($con)
	{
		$locationId = $_REQUEST['location'];
		$address = mysqli_real_escape_string($con,$_REQUEST['address']);
	
		$serviceProviderObj = new serviceProvider();
		
		$insertResult = $serviceProviderObj->insertServiceProviderBranch($serviceProviderId,$locationId,$address,$con);
		if($insertResult)
		{
			$_SESSION['success'] = 'Branch Added Successfully';
			header('Location:dashboard.php');
		}
		else
		{
			$_SESSION['failure'] = 'Oops! Something Went Wrong';
			header('Location:dashboard.php');
		}
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
	

?>