<?php 

	class serviceProvider
	{
		public function checkEmail($email,$con)
		{
			$sql1 = "select count(*) from service_provider where email = '".$email."'";
			$recordSet1 = mysqli_query($con,$sql1);
			while($row1 = mysqli_fetch_array($recordSet1))
			{
				$count1 = $row1[0];
			}
			if($count1 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function checkServiceProviderDetails($email,$password,$con)
		{
			$emailExistence = $this->checkEmail($email,$con);
			if($emailExistence)
			{
				$sql2 = "select password from service_provider where email = '".$email."'";
				$recordSet2 = mysqli_query($con,$sql2);
				while($row2 = mysqli_fetch_array($recordSet2))
				{
					$dbPassword2 = $row2[0];
				}
				if($dbPassword2 == $password)
				{
					return "correct";
				}
				else
				{
					return "Incorrect Password";
				}	
				
			}
			else
			{
				return "Email Doesnot Exist";
			}
		}
		
		public function getServiceProviderId($email,$con)
		{
			$sql3 = "select service_provider_id,user_type,status from service_provider where email = '".$email."'";
			$recordSet3 = mysqli_query($con,$sql3);
			while($row3 = mysqli_fetch_array($recordSet3))
			{
				$serviceProviderId3 = $row3;
			}
			return $serviceProviderId3;
		}
		
		public function getServiceProviderDetails($serviceProviderId,$con)
		{
			$sql4 = "select * from service_provider where service_provider_id = $serviceProviderId";
			$recordSet4 = mysqli_query($con,$sql4);
			return $recordSet4;
		}
		
		public function setPassword($serviceProviderId,$newPassword,$con)
		{
			$sql5 = "update service_provider set password = '".$newPassword."',modified_time = now() where service_provider_id = $serviceProviderId";
			$rowsAffected5 = mysqli_query($con,$sql5);
			if($rowsAffected5 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function updateServiceProviderProfile($serviceProviderId,$name,$businessName,$mobile,$image,$con)
		{
			$sql6 = "update service_provider set name = '".$name."',business_name = '".$businessName."',mobile = '".$mobile."',image = '".$image."',modified_time = now() where service_provider_id = $serviceProviderId";
			$rowsAffected6 = mysqli_query($con,$sql6);
			if($rowsAffected6 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function getLocations($con)
		{
			$sql7 = "select * from location where status = 1 ORDER BY name ASC";
			$recordSet7 = mysqli_query($con,$sql7);
			return $recordSet7;	
		}
		
		public function insertServiceProviderBranch($serviceProviderId,$locationId,$address,$con)
		{
			$sql8 = "insert into service_provider_branches(service_provider_id,location_id,address,created_time,status) values ($serviceProviderId,$locationId,'".$address."',now(),0) ";
			$rowsAffected8 = mysqli_query($con,$sql8);
			if($rowsAffected8 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function getAllServiceProviderBranches($serviceProviderId,$con)
		{
			$sql9 = "select a.*,b.name from service_provider_branches a INNER JOIN location b ON a.location_id = b.location_id where a.service_provider_id = $serviceProviderId";
			$recordSet9 = mysqli_query($con,$sql9);
			return $recordSet9;	
		}
		
		public function getCategories($con)
		{
			$sql10 = "select * from category where status = 1";
			$recordSet10 = mysqli_query($con,$sql10);
			return $recordSet10;	
		}
		
		public function insertServiceProviderCategory($serviceProviderId,$categoryId,$con)
		{
			$sql11 = "insert into service_provider_categories(service_provider_id,category_id,created_time) values ($serviceProviderId,$categoryId,now())";
			$rowsAffected11 = mysqli_query($con,$sql11);
			if($rowsAffected11 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function getServiceProviderCategories($serviceProviderId,$con)
		{
			$sql12 = "select a.service_provider_category_id,b.category_id,b.name,b.image,b.short_description from service_provider_categories a INNER JOIN category b ON a.category_id = b.category_id";
			$recordSet12 = mysqli_query($con,$sql12);
			return $recordSet12;	
		}
		
		public function checkServiceProviderCategory($serviceProviderId,$categoryId,$con)
		{
			$sql13  = "select count(*) from service_provider_categories where service_provider_id = $serviceProviderId and category_id = $categoryId";
			$recordSet13 = mysqli_query($con,$sql13);
			while($row13 = mysqli_fetch_array($recordSet13))
			{
				$count13 = $row13[0];
			}
			if($count13 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function deleteServiceProviderCategory($serviceProviderCategoryId,$con)
		{
			$sql14 = "delete from service_provider_categories where service_provider_category_id = $serviceProviderCategoryId";
			$rowsAffected14 = mysqli_query($con,$sql14);
			if($rowsAffected14 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function getActiveServiceProviderBranches($serviceProviderId,$con)
		{
			$sql15 = "select a.*,b.name from service_provider_branches a INNER JOIN location b ON a.location_id = b.location_id where a.service_provider_id = $serviceProviderId and a.status = 1";
			$recordSet15 = mysqli_query($con,$sql15);
			return $recordSet15;	
		}
		
		public function getAllServices($con)
		{
			$sql16 = "select a.*,b.name as category from services a INNER JOIN category b ON a.category_id = b.category_id where a.status = 1";
			$recordSet16 = mysqli_query($con,$sql16);
			return $recordSet16;	
		}
		
		public function checkServiceProviderService($serviceProviderId,$serviceId,$con)
		{
			$sql17 = "select count(*) from service_provider_services where service_provider_id = $serviceProviderId and service_id = $serviceId";
			$recordSet17 = mysqli_query($con,$sql17);
			while($row17 = mysqli_fetch_array($recordSet17))
			{
				$count17 = $row17[0];
			}
			if($count17 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function insertServiceProviderService($serviceProviderId,$branchId,$serviceId,$description,$price,$addons,$product,$con)
		{
			$sql18 = "INSERT INTO `service_provider_services`(`service_provider_id`, `service_provider_branch_id`, `service_id`, `description`, `base_price`, `add_ons`, `product_to_use`, `created_time`, `status`) VALUES($serviceProviderId,$branchId,$serviceId,'".$description."','".$price."','".$addons."','".$product."',now(),1) ";
			$rowsAffected18 = mysqli_query($con,$sql18);
			if($rowsAffected18 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function getServiceProviderServices($serviceProviderId,$con)
		{
		    $sql19 = "select a.*,b.name as location,c.address,d.name as category,s.name as service from service_provider_services a INNER JOIN service_provider_branches c ON a.service_provider_branch_id = c.service_provider_branche_id INNER JOIN location b ON b.location_id = c.location_id INNER JOIN services s ON s.service_id = a.service_id INNER JOIN category d ON s.category_id = d.category_id where a.service_provider_id = $serviceProviderId";
		    $recordSet19 = mysqli_query($con,$sql19);
			return $recordSet19;	
		}
		
		public function deleteServiceProviderService($serviceProviderServiceId,$con)
		{
			$sql20 = "delete from service_provider_services where service_provider_service_id = $serviceProviderServiceId";
			$rowsAffected20 = mysqli_query($con,$sql20);
			if($rowsAffected20 > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

?>