<?php
require_once dirname(__DIR__) . '/controller/UserIndex.php';
require_once dirname(__DIR__) . '/controller/AdminCountries.php';
$controllerCountry = new AdminCountries();
$countries = $controllerCountry->getActiveCountries();
$controller = new UserIndex();
$controller->init();
$cities=$controller->getCitiesList();
if(isset($_SESSION['message']))
{
	$message = $_SESSION['message'];	
}
if(isset($_SESSION['success']))
{
	$success = $_SESSION['success'];	
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Salonee | Register Now </title>
    <link rel="stylesheet" href="<?php echo user_base_url;?>assets/bootstrap-4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo user_base_url;?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo user_base_url;?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- fonts  -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<link rel = "stylesheet" href = "<?php echo user_base_url;?>assets/css/flash.min.css">
  <style type="text/css">
.pac-container {
    background-color: #FFF;
    z-index: 20;
    position: fixed;
    display: inline-block;
    float: left;
}
.modal{
    z-index: 20;   
}
.modal-backdrop{
    z-index: 10;        
}


    
@media screen and (max-width:1024px) and (min-width:990px) {

.login form {
  min-width: 294px !important;
  max-width: 70%;
  margin: 0 auto;
}

img {
    vertical-align: middle !important;
    border-style: none;
    
}

}

@media screen and (max-width:984px) and (min-width:767px) {

.login form {
  min-width: 294px !important;
  max-width: 70%;
  margin-left: 50%;
}
img {
    vertical-align: middle !important;
    border-style: none;
    
}
}


	.image-container{
        background: url('<?php echo user_base_url;?>assets/img/admin/bg1.jpg')no-repeat;
    background-size: cover;
    min-height: 100vh;
}
img {
    vertical-align: middle !important;
    border-style: none;
    
}
.login form {
    min-width: 390px;
    max-width: 70%;
    margin: 0 auto;
}
.form-container {
    display: flex;
    justify-content: center;
}

.form-box{
    display: flex;
    flex-direction: column;
    justify-content: center;
    min-height: 100vh;
}

.form-box h4{
    font-weight: bold;
    color: #fff;
}
  </style>
</head>

<body>
    
    
    
    
<section class="login">
<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-md-12 d-none d-md-block image-container"></div>

			<div class=" form-container">
				<div class="col-lg-8 col-md-12 col-sm-9 col-xs-12 form-box text-center">
					<div class="logo mb-1 mt-3">
                        <a href="<?php echo base_url;?>">
                    <img src="<?php echo user_base_url;?>assets/img/logo2.png" width="200px" alt="Salonee" class="logo">
                    <a>
					</div>
					<div class="heading mb-4">
						<h4>Login into your account</h4>
					</div>
                 <form name="categor_im" id="categor_im" method="post" action="" enctype="multipart/form-data">
              <input type="hidden" name="type" value="newRegistration">
                <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" id="title" class="form-control" name="name" autocomplete="off" required="">
                            <label class="form-control-placeholder p-0" for="title">Name</label>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <input type="text" id="business_name" class="form-control" name="business_name" autocomplete="off" required="">
                            <label class="form-control-placeholder p-0" for="title">Business Name</label>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="email" class="form-control" autocomplete="off" required="">
                                    <label class="form-control-placeholder p-0" for="code">Email</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="mobile" class="form-control" autocomplete="off" required="">
                                    <label class="form-control-placeholder p-0" for="value">Mobile</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
<!--                                 <div class="form-group">
                                    <select class="form-control" autocomplete="off" id="city" name="city" required="">
                                      <option value="0"> Select Location</option>
                                      <?php foreach($cities as $cit){ 
                                          echo '<option value="'.$cit['id'].'">'.$cit['city_name_en'].'</option>';
                                        }?>
                                    </select>
                                    <label class="form-control-placeholder p-0" for="city">Country</label>
                                </div> -->
                                <div class="form-group">
                                    <select class="form-control" autocomplete="off" id="country_id" name="country_id" required="" onChange="getCity(this.value)">
                                      <option value="0"> Select Location</option>
                                      <?php foreach($countries as $country){ 
                                          echo '<option value="'.$country['id'].'">'.$country['country_name'].'</option>';
                                        }?>
                                    </select>
                                    <label class="form-control-placeholder p-0" for="city">Country</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select class="form-control" autocomplete="off" id="city_id" name="city_id" required="">
                                      <option value="0"> Select Location</option>
                                    </select>
                                    <label class="form-control-placeholder p-0" for="city">City</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" id="address" name="address" required=""></textarea>
                                    <label class="form-control-placeholder p-0" for="address">Address</label>
                                </div>
                            </div>
                        </div>
                      <div class="form-group col-md-8">
                        <button type="button" data-toggle="modal" data-target="#myModal" for="file-upload" class="btn theme-btn"> <i class="fa fa-map-marker" aria-hidden="true"></i> Locate Address in Map </button>
                      </div>
                      <div class="form-group">
                        <label for="shop-image-upload" class="custom-file-upload">
                          Shop Image<i class="fa fa-camera" aria-hidden="true"></i>
                        </label>
                        <input id="shop-image-upload" name="shop_image" type="file"  style="display:none;">
                      </div>
                      <div class="form-group">
                        <label for="file-upload" class="custom-file-upload">
                          Business Licence<i class="fa fa-camera" aria-hidden="true"></i>
                        </label>
                        <input id="file-upload" name="image" type="file"  style="display:none;">
                      </div>



                <div class="form-group">
                  <button type="submit" class="btn theme-btn" id="success">Submit</button>
                </div>
                <p id="forgot_pswd"><a href="index.php" >Login Now</a></p>
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <label><input type="text" id="search" placeholder="Enter Address" class="form-control" autocomplete="off" /></label>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12 modal_body_map">
              <div class="location-map" id="map_canvas-map">
                <div style="width: 600px; height: 400px;" id="map_canvas"></div>
              </div>
            </div>
          </div>
          <!--<div id="current"> -->
          <!--  Latitude : <input type="text" name="latitude" id="lat" readonly="readonly"> &nbsp;&nbsp;&nbsp;&nbsp; -->
          <!--  Longitude : <input type="text" name="longitude" id="lng" readonly="readonly">-->
          <!--</div>-->
           <div id="current" class="ml-5   mt-3 "> 
            Latitude : <input type="text" name="latitude" id="lat" readonly="readonly"> &nbsp;&nbsp;&nbsp;&nbsp; 
            Longitude : <input type="text" name="longitude" id="lng" readonly="readonly">
 
            <button type="button" class="btn btn-success ml-5" data-dismiss="modal" style="background: #D66292!important;border: #D66292!important;">OK</button>
 
         
          </div>
          <div id="location_address">
          </div>
        </div>
      </div>
    </div>
  </div>  
              </form>
				</div>
			</div>
		</div>
	</div>
</div>
</section>



 <!-- Modal -->
    <!-- AIzaSyB17jK62Jr_6Xqz-NwjkYnb7M9RmL77KTE -->
    <script type="text/javascript" src="<?php echo user_base_url;?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo user_base_url;?>assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>
	<script src = "<?php echo user_base_url;?>assets/js/flash.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyB17jK62Jr_6Xqz-NwjkYnb7M9RmL77KTE&libraries=places"></script>

    <script>
        $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
	iziToast.settings({
      timeout: 3000, // default timeout
      resetOnHover: true,
      // icon: '', // icon class
      transitionIn: 'flipInX',
      transitionOut: 'flipOutX',
      position: 'topRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
      onOpen: function () {
        console.log('callback abriu!');
      },
      onClose: function () {
        console.log("callback fechou!");
      }
    });
    $("form[name='categor_im']").submit(function(e){
        // if(($('#lat').val() == '') || ($('#lng').val() == '')) {
        //   iziToast.error({
        //       title: 'Error',
        //       message: 'Please select location details from map',
        //   });
        //   e.preventDefault();
        //   return false; 
        // }
        if(typeof $('#file-upload')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select business licence',
          });
          e.preventDefault();
          return false;
        }
        if(typeof $('#shop-image-upload')[0].files[0]=='undefined'){
          iziToast.error({
              title: 'Error',
              message: 'Please select shop image',
          });
          e.preventDefault();
          return false;
        }
  });

function getCity(val) {
  $.ajax({
    url: "<?php echo admin_base_url.'cities.php';?>",
    method:"POST",
    data:{type:'citybycountry',country_id:val},
    success: function(data){
      $('#city_id').html(data)
    }
  });
}    
	<?php 
		if(isset($_SESSION['message']))
		{
	?>
        iziToast.error({title: 'Error', message: '<?php echo $message;?>'});
	<?php
		unset($_SESSION['message']);
		}
	?>
	<?php 
        if(isset($_SESSION['success']))
        {
	?>
        iziToast.success({title: 'Success', message: '<?php echo $success;?>'});
	<?php
		unset($_SESSION['success']);
    }
	?>

  // $(document).ready(function() {

 function init() {
   var map = new google.maps.Map(document.getElementById('map_canvas'), {
     center: {
       lat: 0,
       lng: 0
     },
     zoom: 10
   });


   var searchBox = new google.maps.places.SearchBox(document.getElementById('search'));
   // map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('search'));
   google.maps.event.addListener(searchBox, 'places_changed', function() {
     // searchBox.set('map', null);
     var places = searchBox.getPlaces();
     var bounds = new google.maps.LatLngBounds();
     var i, place;
     for (i = 0; place = places[i]; i++) {
       (function(place) {
         var marker = new google.maps.Marker({
           position: place.geometry.location,
           map: map,
           draggable: true
          });

          document.getElementById('lat').value = place.geometry.location.lat()
          document.getElementById('lng').value = place.geometry.location.lng()
          bounds.extend(place.geometry.location);
          getReverseGeocodingData(place.geometry.location.lat(), place.geometry.location.lng())
          google.maps.event.addListener(marker, 'dragend', (evt) =>{
            document.getElementById('lat').value = evt.latLng.lat().toFixed(3)
            document.getElementById('lng').value = evt.latLng.lng().toFixed(3)
            getReverseGeocodingData(evt.latLng.lat().toFixed(3), evt.latLng.lng().toFixed(3))
          });


       }(place));

     }
     map.fitBounds(bounds);
     searchBox.set('search', map);
     map.setZoom(Math.min(map.getZoom(),17));
    $('#myModal').on('shown.bs.modal', function() {
      google.maps.event.trigger(map, "resize");

    });

   });
 }

function getReverseGeocodingData(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    // This is making the Geocode request
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status !== google.maps.GeocoderStatus.OK) {
            alert(status);
        }
        // This is checking to see if the Geoeode Status is OK before proceeding
        if (status == google.maps.GeocoderStatus.OK) {
            console.log(results);
            var address = (results[0].formatted_address);
            document.getElementById('address').value = address
            document.getElementById('location_address').innerHTML = `<p> Address : ${address}</p>`
        }
    });
}
 setTimeout(function() {
 }, 100);
   google.maps.event.addDomListener(window, 'load', init);

      $('#shop-image-upload').change(function() {
        var i = $(this).prev('label').clone();
        var file = $('#shop-image-upload')[0].files[0].name;
        $(this).prev('label').text(file);
      });
      $('#file-upload').change(function() {
        var i = $(this).prev('label').clone();
        var file = $('#file-upload')[0].files[0].name;
        $(this).prev('label').text(file);
      });
  // Re-init map before show modal
  $('#myModal').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget);
    // initializeGMap();
    $("#map_canvas").css("width", "100%");
  });

  // Trigger map resize event after modal shown
// });    
</script>
</body>

</html>
