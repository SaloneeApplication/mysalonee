<?php 
	session_start();
	
	//Included Files
	
	include("class/serviceProvider.php");	
	include("class/dbConnection.php");


	//Retrieving Session Variable
	
	if(isset($_SESSION["serviceProviderId"]))
	{
		
		$serviceProviderId = $_SESSION["serviceProviderId"];
		
		//echo $serviceProviderId;
	}
	else
	{
		header('Location:index.php?message=Unauthorized%20Access%20Prohobited');
	}
	
	//Retrieving Form Fields
	$currentPassword = $_REQUEST["pasCurrentPassword1"];
	$newPassword = $_REQUEST["pasNewPassword1"];
	
	//Creating dbObject 
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	if($con)
	{
		$serviceProviderObj = new serviceProvider();
		
		$recordSet = $serviceProviderObj->getServiceProviderDetails($serviceProviderId,$con);
		while($row = mysqli_fetch_array($recordSet))
		{
			$dbPassword = $row["password"];
			
			//echo $dbPassword;
		}
		 if($dbPassword == $currentPassword)
		{
			$passwordResult = $serviceProviderObj->setPassword($serviceProviderId,$newPassword,$con);
			
			if($passwordResult)
			{
				$_SESSION['success'] = 'Password Updated Successfully';
				header('Location:dashboard.php');
			}
			else
			{
				$_SESSION['failure'] = 'Oops! Something Went Wrong';
				header('Location:dashboard.php');
			}
		} 
		else
		{
			$_SESSION['failure'] = 'Invalid Current Password';
			header('Location:dashboard.php');
		}
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
	

?>