<?php
require_once dirname(__DIR__) . '/controller/UserProductOrders.php';
$controller = new UserProductOrders();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
include("includes/header.php");
$products=$controller->getOrderedProducts();

?>

	<?php include("includes/sidebar.php");?>
  <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <div class="main__content">
          <div class="col-md-12">
            <div class="w-50">
              <div class="input-group mb-3">
                <input type="text" class="form-control _search"  id="searchTd" placeholder="Search">
                <div class="input-group-append">
                  <button class="btn theme-btn" type="submit">Go</button>
                </div>
              </div>
            </div>
            <div class="pull-right rightBtn">
              <!-- <button class="btn login-btn" data-toggle="modal" data-target="#addDiscount" id="myButton">Add &nbsp;<i
                  class="fa fa-plus" aria-hidden="true"></i></button> -->
            </div>
          </div>
<style>
.product_cls tr th{
  padding:16px;
}
.product_cls tr td{
  padding:16px;
}
</style>
<div class="pad_3">  
          <div class="table-responsive">

            <table class="table themeTable" >
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Order ID</th>
                  <th>Customer Name</th>
                  <th>Product Count</th>
                  <th>Total Amount</th>
                  <th>Order Date</th>
                  <th>Order Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="saloonTable"> 
              <?php
                $i=1;
               foreach($products as $pro){ 
                 echo '<tr>';
                  echo '<td>'.$i.'</td>';
                  echo '<td>'.$pro['order_id'].'</td>';
                  echo '<td>'.$pro['customer_name'].'</td>';
                  if($pro['product_name']==1){
                    echo '<td>'.$pro['product_name'].' Product</td>';
                  }else{
                    echo '<td>'.$pro['product_name'].' Products</td>';
                  }
                  echo '<td>'.$pro['total_amount'].'</td>';
                  echo '<td>'.$pro['date_of_order'].'</td>';
                  echo '<td>';
                    if($pro['shipping_status']=='shipped'){
                      echo '<label class="badge badge-pill badge-warning">Shipped</label>';
                    }elseif($pro['shipping_status']=='pending'){
                      echo '<label class="badge badge-pill badge-warning">In Process</label>';
                    }elseif($pro['shipping_status']=='delivered'){
                      echo '<label class="badge badge-pill badge-success">Delivered</label>';
                    }elseif($pro['shipping_status']=='cancelled'){
                      echo '<label class="badge badge-pill badge-danger">Cancelled</label>';
                    }else{
                      echo '<label class="badge badge-pill badge-primary">In Process</label>';
                    }
                  echo '</td>';
                  echo '<td>';
                    echo '<a href="javascript:;" class="badge badge-primary"  onclick="viewproducts(this)" data-oid="'.$pro['order_id'].'" ><i class="fa fa-eye"></i> View Products</a>';
                  if($pro['shipping_status']!='delivered'){
                    echo '<a href="javascript:;" class="badge badge-warning" onclick="ostatus(this)" data-oid="'.$pro['order_id'].'" ><i class="fa fa-truck"></i> Order Status</a>';
                  }

                  echo '</td>';


               }
                  ?>

              </tbody>
            </table>

          </div>
          <div class="clearfix"></div>
            <div class='pagination-container tablePaging'>
                  <div class="pagination">
                    <ol id="numbers"></ol>
                  </div>
            </div>
     </div>
</div>

    <!-- partial -->
    <div class="modal fade pswdModal" id="addDiscount" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Order Status</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post"> 
              <input type="hidden" name="type" value="updateOrderStatus" />
              <input type="hidden" name="oid" id="oid" value="" />
                <div class="row mb-5">
                
<div class="col-md-12">

<div class="form-group">
                    <select name="order_status" id="order_status" class="form-control" autocomplete="off" required >
                      <option hidden   value> -- Select Order Status -- </option>
                      <option value="pending">Pending</option>
                      <option value="shipped">Shippied</option>
                      <option value="delivered">Delivered</option>
                    </select>
                  </div>
                  <div class="form-group" id="order_track" style="display:none;">
                    <input type="text" id="order_track1" name = "order_track" class="form-control" autocomplete="off" >
                    <label class="form-control-placeholder p-0" for="order_track1">Tracking ID</label>
                  </div>
</div>
                </div>


                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Update</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->

    
    <!-- order products partial -->
    <div class="modal fade pswdModal" id="orderedProducts" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Ordered Products</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <input type="hidden" name="type" value="updateOrderStatus" />
              <input type="hidden" name="oid" id="oid" value="" />
                <div class="row mb-5">
<div class="col-md-12">

<table class="product_cls" id="product_cls">
<tr><th>Product Name</th><th>Image</th><th>Price</th><th>QTY</th></tr>
<tr><td>Product Name</td><td>Image</td><td>Price</td><td>QTY</td></tr>
</table>
</div>
                </div>


                <div class="form-group">
                </div>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->
<?php include("includes/footer.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script>
$("#order_status").change(function(){
  $("#order_track").hide();
  $("#order_track1").removeAttr("required");
  if($(this).val()=='shipped'){
    $("#order_track").show();
    $("#order_track1").attr("required","required");
  }
});
$(document).ready(function(){
        $("#searchTd").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#saloonTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      }); 

      function ostatus(va){
        var oid=$(va).data("oid");
        $("#oid").val(oid);
        $("#addDiscount").modal('show');
      }
      function viewproducts(va){
        var oid=$(va).data("oid");
        $("#orderedProducts").modal('show');
        $.ajax({
          url:"<?php echo user_base_url.'product_orders.php';?>",
          method:"POST",
          data:{oid:oid,type:"getOrderedProducts"},
          success:function(data){
            $("#product_cls").html(data);
          }
        })
      }
</script>