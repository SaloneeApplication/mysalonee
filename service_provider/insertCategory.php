<?php 
	session_start();
	
	//Included Files
	
	include("class/serviceProvider.php");	
	include("class/dbConnection.php");


	//Retrieving Session Variable
	
	if(isset($_SESSION["serviceProviderId"]))
	{
		
		$serviceProviderId = $_SESSION["serviceProviderId"];
		
		//echo $serviceProviderId;
	}
	else
	{
		header('Location:index.php?message=Unauthorized%20Access%20Prohobited');
	}
	
	//Retrieving Form Fields
	//Creating dbObject 
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	if($con)
	{
		$categories = $_REQUEST['category'];
		
		
	
		$serviceProviderObj = new serviceProvider();
		$categoriesCount = count($categories);
		$insertCount = 0;
		foreach($categories as $value)
		{
			$categoryId = $value;
			$checkExistence = $serviceProviderObj->checkServiceProviderCategory($serviceProviderId,$categoryId,$con);
			if($checkExistence)
			{
				$insertCount = $insertCount +1; 
			}
			else
			{
				$insertResult = $serviceProviderObj->insertServiceProviderCategory($serviceProviderId,$categoryId,$con);
				if($insertResult)
				{
					$insertCount = $insertCount +1; 
				}
			}
			
		}
		
		
		
		if($insertCount == $categoriesCount)
		{
			$_SESSION['success'] = 'Categories Added Successfully';
			header('Location:category.php');
		}
		else
		{
			$_SESSION['failure'] = 'Oops! Something Went Wrong';
			header('Location:category.php');
		}
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
	

?>