<aside class="sidebar">
	<div class="sidebar__header">
	  <a href="dashboard.php">
		<img src="./assets/img/logo1.png" alt="logo" class="admin_logo">
	  </a>
	</div>
	<div class="sidebar__content">
	  <ul class="sidebar__content-menu">

		<li class="<?php if(@$page == 'dashboard') { echo 'sidebar--active'; }?>"><a href="dashboard.php"><i class="fa fa-dashboard fa-lg"></i> Dashboard</a></li>
		<?php if($_SESSION['branch_type']=='main_branch'){?>
		<li class="<?php if(@$page == 'branches') { echo 'sidebar--active'; }?>"><a href="branches.php"><i class="fa fa-code-fork fa-lg"></i> Branch</a></li>
		<?php } ?>
		<li class="<?php if(@$page == 'category') { echo 'sidebar--active'; }?>"><a href="category.php"><i class="fa fa-tags fa-lg"></i> Category</a></li>
		<li class="<?php if(@$page == 'services') { echo 'sidebar--active'; }?>"><a href="services.php"><i class="fa fa-scissors fa-lg"></i> Services</a></li> 
		<li class="<?php if(@$page == 'combo-services') { echo 'sidebar--active'; }?>"><a href="combo-services.php"><i class="fa fa-object-group fa-lg"></i> Combo Services</a></li>
		<li class="<?php if(@$page == 'discounts') { echo 'sidebar--active'; }?>"><a href="discounts.php"><i class="fa fa-percent fa-lg"></i> Discounts</a></li>
		<li class="<?php if(@$page == 'promocodes') { echo 'sidebar--active'; }?>"><a href="promocodes.php"><i class="fa fa-gift fa-lg"></i> Promo Codes</a></li>
		<li class="<?php if(@$page == 'services-for-today') { echo 'sidebar--active'; }?>"><a href="services-for-today.php"><i class="fa fa-clock-o fa-lg"></i> Services For Today</a></li> 
		<li class="<?php if(@$page == 'service-bookings') { echo 'sidebar--active'; }?>"><a href="service-bookings.php"><i class="fa fa-calendar-check-o fa-lg"></i> Service Bookings</a></li>
		<li class="<?php if(@$page == 'slots') { echo 'sidebar--active'; }?>"><a href="slots.php"><i class="fa fa-calendar fa-lg"></i> Slots</a></li>
		<li class="<?php if(@$page == 'products') { echo 'sidebar--active'; }?>"><a href="products.php"><i class="fa fa-shopping-bag fa-lg"></i> Products</a></li>
		<li class="<?php if(@$page == 'product_orders') { echo 'sidebar--active'; }?>"><a href="product_orders.php"><i class="fa fa-cubes fa-lg"></i> Product Orders</a></li>
		<li class="<?php if(@$page == 'service_provider_advertisement') { echo 'sidebar--active'; }?>"><a href="advertisement.php"><i class="fa fa-bullhorn fa-lg"></i> Advertisement</a></li>
		<li class="<?php if(@$page == 'user_featured_profile') { echo 'sidebar--active'; }?>"><a href="featured_profile.php"><i class="fa fa-trophy fa-lg"></i> Featured Profile</a></li>
		<li class="<?php if(@$page == 'transctions') { echo 'sidebar--active'; }?>"><a href="transactions.php"><i class="fa fa-history fa-lg"></i> Transactions</a></li>
		<li class="<?php if(@$page == 'payouts') { echo 'sidebar--active'; }?>"><a href="payouts.php"><i class="fa fa-money fa-lg"></i> Payouts</a></li>
		<li class="<?php if(@$page == 'ratings') { echo 'sidebar--active'; }?>"><a href="ratings.php"><i class="fa fa-star fa-lg"></i> Ratings</a></li>
		

	  </ul>
	</div>
</aside>
<main class="main">

 <div class="main__header">
          <div class="main__header-heading">
            <h1><?php echo $businessName; ?></h1>
          </div>

          <div class="main__header-user">

            <button type="button" class="main__header-user-toggle" data-toggle="dropdown" data-toggle-for="user-menu"
              role="button">
              <span class="main__header-user-toggle-picture">
				<?php 
					if($image == "")
					{
				?>
                <img src="./assets/img/admin/avatar-male.png" alt="">
				<?php 
					}
					else
					{
						echo " <img src='".user_base_url."$image' alt=''>";
					}
				?>
              </span>
            </button>
            <!-- <div class="main__header-user-menu dropdown-menu" > -->
            <ul class="main__header-user-menu-content dropdown-menu">
              <li>
                <div class="main__header-user-menu-header ">
                  <h3><?php echo $name;?></h3>
                  <span>Role : Service Provider</span>
                  <span>Membership Expiry : 
				  <?php if($membership_expire!='0000-00-00'){echo '<strong>'.$membership_expire.'</strong>'; }else{ echo '---'; }?>
				  </span>
                </div>
              </li>
			  <li data-toggle="modal" data-target="#upgrade"><a class="d-m" href="upgrade.php"><i
                    class="icon icon--profile"></i>Upgrade</a></li>
              <li data-toggle="modal" data-target="#myProfile"><a class="d-m" href="#"><i
                    class="icon icon--profile"></i>My profile</a></li>
              <li data-toggle="modal" data-target="#changePassword"><a class="d-m" href="#"><i
                    class="icon icon--help"></i>Change Password</a></li>
              <li><a class="d-m" href="signOut.php"><i class="icon icon--sign-out"></i>Sign out</a></li>
            </ul>
            <!-- </div> -->
          </div>
        </div>
