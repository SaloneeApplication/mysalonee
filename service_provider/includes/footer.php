<!-- change password modal  -->
    <div class="modal fade pswdModal" id="changePassword" tabindex="-1" role="dialog"
      aria-labelledby="changePasswordTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="changePasswordTitle">Change Password</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form method = "post" action = "<?php echo user_base_url.'ajax.php';?>">
              <input type='hidden' name="type" value="changePassword" />
                <div class="form-group mt-4 ">
                  <input type="password" id="op" class="form-control" name = "opass" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="op">Old Password</label>
                </div>

                <div class="form-group ">
                  <input type="password" id="np" class="form-control" name = "npass" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="np">New Password</label>
                </div>

                <div class="form-group mb-5">
                  <input type="password" id="cp" class="form-control" name = "n1pass" autocomplete="off" required>
                  <label class="form-control-placeholder p-0" for="cp">Confirm Password</label>
                </div>


                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Submit</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>

    <!-- my profile modal  -->
    <div class="modal fade pswdModal" id="myProfile" tabindex="-1" role="dialog" aria-labelledby="myProfileTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <!-- profile information -->
        <div class="modal-content"  id="profileInfo" style="display:none;">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="myProfileTitle">Profile</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

              <div class="profile_details">
                <div class="pro_img">
				<?php 
					if($image == ""){
						
				?>
                  <img src="./assets/img/admin/avatar-male.png" alt="avatar-male">
				  <?php 
					}
					else
					{
						echo "<img src='".user_base_url."$image' alt='avatar-male'>";
					}
				  ?>
                </div>
                <ul>
                  <li>
                    <span>Service Provider Name</span>
                    <p><?php echo $name;?></p>
                  </li>
				   <li>
                    <span>Business Name</span>
                    <p><?php echo $businessName;?></p>
                  </li>
                  <li>
                    <span>Email Id</span>
                    <p><?php echo $email;?></p>
                  </li>
                  <li>
                    <span>Mobile Number</span>
                    <p><?php echo $mobile;?></p>
                  </li>
                  
                </ul>
               
              </div>

            <div class="login">
              <form>
                <div class="form-group">
                  <button type="button" class="btn theme-btn" onclick="showEditProfile()">Edit Profile</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <!-- edit profile fields -->
          <div class="modal-content" id="editPrfile" style="display:block;">
            <div class="modal-header">
              <h5 class="col-12 modal-title text-center" id="editProfileTitle"> Edit Profile</h5>
              <button  onclick="closeEdit()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="">
                <!-- <img src="./assets/img/admin/avatar-male.png" alt="avatar-male"> -->
              </div>
			   <form method = "post" action = "<?php echo user_base_url.'ajax.php';?>" enctype = "multipart/form-data">
         <input type='hidden' name="type" value="updateProfile" />

              <div class="picture-container">
                <div class="picture">
					<?php
						if($image == "")
						{
							echo "<img src='./assets/img/admin/avatar-male.png' class='picture-src' id='wizardPicturePreview' title=''>";
						}
						else
						{
							echo "<img src='".user_base_url."$image' class='picture-src' id='wizardPicturePreview' title=''>";
						}
                    
					
					?>
                    <input type="file" id="wizard-picture" name = "filUpload111" class="" >
                    <i class="fa fa-camera" aria-hidden="true"></i>
                </div>
                 <!-- <h6 class="">Choose Picture</h6> -->
                </div>
           
              
              <div class="login">
				  <input type = "hidden" id = "hidImage" name = "hidImage1" value = "<?php echo $image;?>" />
                  <div class="form-group mt-4 ">
                    <input type="text" id="name" name = "name"  class="form-control" autocomplete="off" value = "<?php echo $name;?>" required>
                    <label class="form-control-placeholder p-0" for="name">Name</label>
                  </div>
				  <div class="form-group mt-4 ">
                    <input type="text" id="businessname" name = "businessname"  class="form-control" autocomplete="off" value = "<?php echo $businessName;?>" required>
                    <label class="form-control-placeholder p-0" for="name">Business Name</label>
                  </div>
  
                  <div class="form-group">
                    <input type="text" id="email" name = "email" class="form-control" autocomplete="off" value = "<?php echo $email;?>" required >
                    <label class="form-control-placeholder p-0" for="name">Email Id</label>
                  </div>
  
                  <div class="form-group">
                    <input type="text" id="mn" name = "mobile" class="form-control" autocomplete="off" value = "<?php echo $mobile;?>" required>
                    <label class="form-control-placeholder p-0" for="mn">Mobile Number</label>
                  </div>

                  
                  <div class="form-group">
                    <button type="submit" class="btn theme-btn">Save</button>
                  </div>
               
              </div>
			 </form>
            </div>
          </div>


      </div>
    </div>

<!-- subscription alert  -->
<div class="modal fade pswdModal" id="warningpopup" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="warningpopupTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close " onclick="closedSSLAlert();" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form>
                <div style="text-align: center;">
                  <img src="<?php echo user_base_url;?>assets/img/problem.svg" />
                </div>

                <div class="war-text">
                  <p style="text-align: center;">If not subscribed then Service Seeker can't book service from salonee.
                    So please do Subscription.</p>
                </div>
                <div class="form-group" style="display: flex;">
                  <button type="button" class="btn theme-btn"  onclick="subscriptionRedirect();"   style="width: 200px;">Subscription</button>
                  <button type="button" class="btn skip-btn" data-dismiss="modal"  onclick="closedSSLAlert();"  aria-label="Close">SKIP</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>

<!-- subscription alert end -->

    <!-- partial -->
    <script src="<?php echo user_base_url;?>assets/bootstrap-4.3.1/js/popper.min.js"></script>
    <script src='<?php echo user_base_url;?>assets/js/jquery-3.3.1.min.js'></script>
    <script src="<?php echo user_base_url;?>assets/js/sweetalert2.all.min.js"></script>
    <!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.10/handlebars.min.js'></script> -->
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script> -->
    <script src="<?php echo user_base_url;?>assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>

    <!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js'></script> -->
    <!-- <script src="./assets/js/script.js"></script> -->
    <script src="<?php echo user_base_url;?>assets/js/main.js"></script>
    <script src="<?php echo user_base_url;?>assets/js/pagination1.js"></script>
    <script src="<?php echo user_base_url;?>assets/js/flash.min.js"></script>
	 <script src="<?php echo user_base_url;?>assets/js/bootstrap-select.min.js"></script>
	 <script src="<?php echo base_url;?>vendor/datatables/jquery.dataTables.min.js"></script>

<script>
  $(document).ready(function(){
// Prepare the preview for profile picture
    $("#wizard-picture").change(function(){
        readURL(this);
    });
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

      var y = document.getElementById("editPrfile");
      var x = document.getElementById("profileInfo");

      // y.style.display = "none";

      function showEditProfile() {
        if (x.style.display === "none") {
          x.style.display = "block";
          y.style.display = "none";
        } else {
          x.style.display = "none";
          y.style.display = "block";
        }
        // y.style.display = "block";
      }

      function closeEdit(){
        y.style.display = "block";
        // x.style.display = "block";
      }


    </script>
	<script>
	iziToast.settings({
      timeout: 3000, // default timeout
      resetOnHover: true,
      // icon: '', // icon class
      transitionIn: 'flipInX',
      transitionOut: 'flipOutX',
      position: 'topRight', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter, center
      onOpen: function () {
        console.log('callback abriu!');
      },
      onClose: function () {
        console.log("callback fechou!");
      }
    });

	<?php 
		if(isset($_SESSION['success']))
		{
	?>
			iziToast.success({timeout: 5000, title: 'Success!', message: '<?php echo $success;?>'});
	<?php
		unset($_SESSION['success']);
		}
	?>
	
	<?php 
		if(isset($_SESSION['failure']))
		{
	?>
			iziToast.error({title: 'Error', message: '<?php echo $failure;?>'});
	<?php
		unset($_SESSION['failure']);
		}
	?>
	
	<?php 
		if(isset($_SESSION['message']))
		{
	?>
			iziToast.error({title: 'Error', message: '<?php echo $message;?>'});
	<?php
		unset($_SESSION['failure']);
		}
	?>
	
</script>
<script>
const access_member="<?php echo $access_member;?>";
const base_url="<?php echo user_base_url;?>";

      $(function () {
        $('.selectpicker').selectpicker();
        if(getSaloneeCookie("ssl_alert")=="0"){
          $("#warningpopup").modal('show');
        }
      });
      <?php  if(@$_SESSION['alert_type']!=''){ ?>
          swal({
            type: "<?php echo $_SESSION['alert_type']; ?>",
            text: '<?php echo $_SESSION['alert_msg']; ?>',
            showConfirmButton: false,
            timer: 3000
          });
          <?php 
           $_SESSION['alert_type']='';
           $_SESSION['alert_msg']='';
      }?>
      function getSaloneeCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
          let c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
      }
      function closedSSLAlert(){
        setSaloneeCookie("ssl_alert","1");
        $("#warningpopup").modal('hide');
      }
      function subscriptionRedirect(){
        setSaloneeCookie("ssl_alert","1");
        window.location.href=base_url+"upgrade.php";
      }
      function setSaloneeCookie(cname, cvalue) {
        var exdays=1;
        const d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
      }
</script>

</body>

</html>