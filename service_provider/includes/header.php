<?php
	if(isset($_SESSION['success']))
	{
		$success = $_SESSION['success'];		
	}
	
	if(isset($_SESSION['failure']))
	{
		$failure = $_SESSION['failure'];	
	}
	
	
	include("class/dbConnection.php");
	include("class/serviceProvider.php");
	$dbObject = new dbConnection();
	$con = $dbObject->getConnection();
	if($con)
	{	
		$serviceProviderObj = new serviceProvider();
		$recordSet = $serviceProviderObj->getServiceProviderDetails($serviceProviderId,$con);
		$service_provider_id="";
		while($row = mysqli_fetch_array($recordSet))
		{ 
			$image = $row['image'];
			$name = $row['name'];
			$email = $row['email'];
			$mobile = $row['mobile'];
			$businessName = $row['business_name'];
			$membership_expire = $row['membership_expiry'];
			$service_provider_id = $row['service_provider_id'];
		}
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
	$access_member=0;
	$service_query = "select service_provider_service_id from service_provider_services where service_provider_id = '".$service_provider_id."'";
	$service_count = mysqli_query($con,$service_query);
	$service_count=mysqli_num_rows($service_count);
	if($membership_expire>=date("Y-m-d")){
		$access_member=1;
	} 
	
	if(@$_COOKIE['ssl_alert']!="1"){
		if($access_member==0 && $service_count>0){
			setcookie('ssl_alert', '0', time() + (86400 * 30), "/");
		}else{
			setcookie('ssl_alert', '1', time() + (86400 * 30), "/");
		}
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title><?php echo $title;?></title>
  <link rel="stylesheet" href="<?php echo user_base_url;?>assets/css/bootstrap-select.min.css">
  <!-- fonts  -->
  <link
    href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap"
    rel="stylesheet">
  <link rel="stylesheet" href="<?php echo user_base_url;?>assets/bootstrap-4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo user_base_url;?>assets/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url;?>vendor/datatables/jquery.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo user_base_url;?>assets/css/theme.css">
  <link rel="stylesheet" href="<?php echo user_base_url;?>assets/css/style.css">
  <link rel="stylesheet" href="<?php echo user_base_url;?>assets/css/flash.min.css">

  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
  <style>
    .bootstrap-select .bs-ok-default::after {
      width: 0.3em;
      height: 0.6em;
      border-width: 0 0.1em 0.1em 0;
      transform: rotate(45deg) translateY(0.5rem);
    }

    .btn.dropdown-toggle:focus {
      outline: none !important;
    }
  </style>
</head>

<body>

<div class="layout">
<div class="layout__container">