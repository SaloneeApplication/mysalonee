<?php
require_once dirname(__DIR__) . '/controller/UserSlots.php';
$controller = new UserSlots();
$controller->init();
$page_data=$controller->pageData();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
$getServicesPrices=$controller->getServicesPrices();
$presentMonth=$controller->getThisMonthSlots();
include("includes/header.php");
?>
	<?php include("includes/sidebar.php");?>
  <link rel="stylesheet" href="<?php echo user_base_url;?>assets/css/evo-calendar.css">
  <link rel="stylesheet" href="<?php echo user_base_url;?>assets/css/evo-calendar.midnight-blue.css">
    
    <div class="main__content">
        <div class="w-50">
          <!-- <div class="input-group mb-3">
            <input type="text" class="form-control _search" id="searchTd" placeholder="Search">
            <div class="input-group-append">
              <button class="btn theme-btn" type="submit">Go</button>
            </div>
          </div> -->
        </div>
     

        <div class="pad_3">  
          <div class="table-responsive">

          <div id="demoEvoCalendar"></div>

          </div>
          <div class="clearfix"></div>

          <!-- pagination  -->
            <div class='pagination-container tablePaging'>              
                  <div class="pagination">
                    <ol id="numbers"></ol>
                  </div>                
            </div>


        </div>
        
      </div>
	  
	  <!-- add location modal  -->
 <div class="modal fade pswdModal" id="addSlots" tabindex="-1" role="dialog" aria-labelledby="addSlotsTitle"
 aria-hidden="true">
 <div class="modal-dialog modal-dialog-centered" role="document">
   <div class="modal-content">
     <div class="modal-header">
       <h5 class="col-12 modal-title text-center" id="addSlotsTitle">Add Slots</h5>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body">
       <div class="login _slot">
         <form>
          <div class="form-group">
            <input type="date" id="sDate" class="form-control" autocomplete="off" required>
            <label class="form-control-placeholder p-0" for="sDate">Date</label>
          </div>

          <div class="form-group">
            <input type="time" id="sTime" class="form-control" autocomplete="off" required>
            <label class="form-control-placeholder p-0" for="sTime">Time</label>
          </div>



           <div class="form-group">
             <button type="button" class="btn theme-btn">Submit</button>
           </div>
         </form>
       </div>

     </div>

   </div>
 </div>
</div>
<?php
  $resp=array();
  $ids=array();
  foreach($presentMonth as $nowd){
    $slot_date=date("Y-m-d",strtotime($nowd['slot_date']));
    $slot_time=date("H:i A",strtotime($nowd['slot_date']));
    $ids[]=$nowd['slot_id'];
    if($nowd['service_status']=='pending'){
        $resp[]=array(
          "name"=>ucwords($nowd['service_name']),
          "description"=>ucwords($nowd['name'])." Service booked on ".$slot_date." ".$slot_time." at ".ucwords($nowd['service_type']),
          "date"=>date("M/d/Y",strtotime($slot_date)),
          "type"=>"event",
          "color"=>"#f1c40f",
          "id"=>$nowd['slot_id']
        );
    }elseif($nowd['service_status']=='inprocess'){
      $resp[]=array(
        "name"=>ucwords($nowd['service_name']),
        "description"=>ucwords($nowd['name'])." Service booked on ".$slot_date." ".$slot_time." at ".ucwords($nowd['service_type']),
        "date"=>date("M/d/Y",strtotime($slot_date)),
        "type"=>"event",
        "color"=>"#9b59b6",
        "id"=>$nowd['slot_id']
      );
    }elseif($nowd['service_status']=='completed'){
      $resp[]=array(
        "name"=>ucwords($nowd['service_name']),
        "description"=>ucwords($nowd['name'])." Service booked on ".$slot_date." ".$slot_time." at ".ucwords($nowd['service_type']),
        "date"=>date("M/d/Y",strtotime($slot_date)),
        "type"=>"event",
        "color"=>"#2ecc71",
        "id"=>$nowd['slot_id']
      );
    }else{
      $resp[]=array(
        "name"=>ucwords($nowd['service_name']),
        "description"=>ucwords($nowd['name'])." Service booked on ".$slot_date." ".$slot_time." at ".ucwords($nowd['service_type']),
        "date"=>date("M/d/Y",strtotime($slot_date)),
        "type"=>"event",
        "color"=>"#e74c3c",
        "id"=>$nowd['slot_id']
      );
    }
  }
?>  
<?php include("includes/footer.php");?>
<script src="<?php echo user_base_url;?>assets/js/evo-calendar.js"></script>
<script>
var events=<?php echo json_encode($resp);?>;
var ids=<?php echo json_encode($ids);?>;
var base_url="<?php echo user_base_url;?>";
$(document).ready(function() {
    
    var week_date = [];
    var defaultTheme = getRandom(4);
    function getRandom(a) {
        return Math.floor(Math.random() * a);
    }

    // today 
    var today = new Date();
    $("#demoEvoCalendar").evoCalendar({
        format: "MM dd, yyyy",
        titleFormat: "MM",
        sidebarDisplayDefault:true,
      eventDisplayDefault: false,
        calendarEvents: events
    });

    // $("#demoEvoCalendar").evoCalendar("setTheme", b);
});
// selectDate
// selectYear
$('#demoEvoCalendar').on('selectYear', function(event, activeYear) {
     // code here...
  $('#demoEvoCalendar').evoCalendar('toggleEventList', false);
  var year=$(".calendar-year p").text();
  var monthIndex=parseInt($(".calendar-months li.active-month").attr("data-month-val"));
  monthIndex=monthIndex+1;
  if(monthIndex<10){
    monthIndex="0"+monthIndex;
  }
  var date=activeYear+"-"+monthIndex;
  $.ajax({
      url:"<?php echo user_base_url;?>slots.php",
      method:"POST",
      data:{date:date,type:"getByMonthSlots"},
      success:function(data){
        $('#demoEvoCalendar').evoCalendar('addCalendarEvent',JSON.parse($.trim(data)));
      }
    })
});
// selectMonth
$('#demoEvoCalendar').on('selectMonth', function(event, activeMonth, monthIndex) {
    // $('#demoEvoCalendar').evoCalendar('removeCalendarEvent', ['kNybja6', 'asDf87L']);
    $('#demoEvoCalendar').evoCalendar('toggleEventList', false);
    var year=$(".calendar-year p").text();
    monthIndex=monthIndex+1;
    if(monthIndex<10){
      monthIndex="0"+monthIndex;
    }
    var date=year+"-"+monthIndex;

    $.ajax({
      url:"<?php echo user_base_url;?>slots.php",
      method:"POST",
      data:{date:date,type:"getByMonthSlots"},
      success:function(data){
        var data1=JSON.parse($.trim(data));
        $('#demoEvoCalendar').evoCalendar('removeCalendarEvent', data1.ids);
        $('#demoEvoCalendar').evoCalendar('addCalendarEvent',data1.event);
      }
    })
});

$('#demoEvoCalendar').on('selectDate', function(event, newDate, oldDate) {
  $('#demoEvoCalendar').evoCalendar('toggleEventList', true);
  $('#demoEvoCalendar').evoCalendar('toggleSidebar', false);

});
// $("#eventListToggler").click(function(){ 
$(document).on("click","#eventListToggler",function(){
  $('#demoEvoCalendar').evoCalendar('toggleSidebar', false);
});

$(document).on("click","#sidebarToggler",function(){
  $('#demoEvoCalendar').evoCalendar('toggleEventList', false);
});
$(document).on("click",".event-container",function(){
  var id=$(this).attr("data-event-index");
  window.location.href=base_url+"service-bookings.php?sid="+id;
});
</script>
