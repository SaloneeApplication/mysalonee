<?php
require_once dirname(__DIR__) . '/controller/UserTransactions.php';
$controller = new UserTransactions();
$controller->init();
$page_data=$controller->pageData();
$transactions=$controller->getTranscations();
$title = $page_data['title'];
$page = $page_data['page'];
$serviceProviderId=$page_data['user_id'];
$title = $page_data['title']; 
$page = $page_data['page']; 
include("includes/header.php");
?>

	<?php include("includes/sidebar.php");?>
  <link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <div class="main__content">
          <div class="col-md-12">
            <div class="w-50">
              <div class="input-group mb-3 _hide">
                <input type="text" class="form-control _search"  id="searchTd" placeholder="Search">
                <div class="input-group-append">
                  <button class="btn theme-btn" type="submit">Go</button>
                </div>
              </div>
            </div>
            <div class="pull-right rightBtn">
              <a class="btn btn-xs login-btn" href="<?php echo user_base_url.'payouts.php';?>">Payouts &nbsp;<i
                  class="fa fa-credit-card" aria-hidden="true"></i></a>
            </div>
          </div>

<div class="pad_3">  
          <div class="table-responsive">

            <table class="table themeTable" >
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Date</th>
                  <th>Amount</th>
                  <th>Type</th>
                  <th>Transaction Type</th>
                  <th>Order ID</th>
                  <th>Withdrwal ID</th>
                </tr>
              </thead>
              <tbody id="saloonTable"> 
              <?php  $i=1; foreach($transactions as $trn){ ?>
                <tr>
                    <td><?php echo $i++;?></td>
                    <td><?php echo $trn['date'];?></td>
                    <td><?php echo $trn['currency'].' '.$trn['amount'];?></td>
                    <td><?php echo ($trn['type']=='credit')?'<label class="badge badge-pill badge-success">Credit</label>':'<label class="badge badge-pill badge-danger">Debit</label>';?></td>
                    <td><?php echo ucwords($trn['amount_type']);?></td>
                    <td><?php echo $trn['order_id'];?></td>
                    <td><?php echo $trn['withdrawal_id'];?></td>
                </tr>
                <?php } ?>

              </tbody>
            </table>

          </div>
          <div class="clearfix"></div>
            <div class='pagination-container tablePaging _hide'>
                  <div class="pagination">
                    <ol id="numbers"></ol>
                  </div>
            </div>
     </div>
</div>
    <!-- partial -->
    <div class="modal fade pswdModal" id="addDiscount" tabindex="-1" role="dialog"
      aria-labelledby="addDiscountTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Discount</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="login">
              <form action="" method="post"> 
              <input type="hidden" name="type" value="updateServiceDiscount" />
              <input type="hidden" name="sp_id" id="sp_id" value="" />
               
                <div class="row mb-5">
                  <div class="col-md-6">
                    <div class="form-group ">
                      <input type="text" id="price" name="base_price" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="price">Price</label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" id="dPrice" name="sale_price" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0" for="dPrice">Discount Price</label>
                    </div>
                  </div>

<div class="col-md-12">

<div class="form-group">
                    <select name="salonee_price_type" id="salonee_price_type" class="form-control" autocomplete="off" required >
                      <option hidden   value> -- Select Price Type -- </option>
                      <option value="regular">Regular</option>
                      <option value="custom_price_weekend">Custom Price - Weekend</option>
                      <option value="custom_price_special_date">Custom Price -- Special Date</option>
                    </select>
                  </div>


                  <div class="form-group" id="salonee_weekend_name" style="display:none;">
                    <select name="salonee_weekend_name[]" id="salonee_weekend_name_value" multiple class="form-control" autocomplete="off" >
                      <option hidden  selected value> -- Select Weekend-- </option>
                      <option value="monday">Every Monday</option>
                      <option value="tuesday">Every Tuesday</option>
                      <option value="wednesday">Every Wednesday</option>
                      <option value="thursday">Every Thursday</option>
                      <option value="friday">Every Friday</option>
                      <option value="satuerday">Every Satuerday</option>
                      <option value="sunday">Every Sunday</option>
                    </select>
                  </div>

                  <div class="form-group" id="salonee_special_date_tag" style="display:none;">
                    <input type="text" id="salonee_special_date" readonly name = "salonee_special_date" class="form-control" autocomplete="off" >
                    <label class="form-control-placeholder p-0" for="special_date">Special Dates</label>
                  </div>
                  
                  <div class="form-group" id="salonee_special_price_tag" style="display:none;">
                    <input type="text" id="salonee_special_price" name = "salonee_special_price" class="form-control" autocomplete="off" >
                    <label class="form-control-placeholder p-0" for="salonee_special_price">Special Price</label>
                  </div>
</div>
                  

                </div>


                <div class="form-group">
                  <button type="submit" class="btn theme-btn">Update</button>
                </div>
              </form>
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- partial -->
<?php include("includes/footer.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script>
$(".edit_service").click(function(){
  var id=$(this).data("id");
  var base_price=$(this).data("base_price");
  var sale_price=$(this).data("sale_price");

  var price_type=$(this).data("price_type");
  var sp_price=$(this).data("sp_price");
  var special_data=$(this).data("special_data");
  
  $("#salonee_price_type").val(price_type);
  $("#salonee_special_date_tag").hide();
  $("#salonee_weekend_name").hide();
  $("#salonee_special_price_tag").hide();
  
  if(price_type=='custom_price_weekend'){
  $("#salonee_special_price_tag").show();
    $("#salonee_weekend_name").show();
    special_data=special_data.toLowerCase();
    special_data=special_data.split(",");
    $("#salonee_weekend_name_value").val(special_data)
    $("#salonee_special_price").val(sp_price)
  }else if(price_type=='custom_price_special_date'){
    $("#salonee_special_date_tag").show();
   $("#salonee_special_price_tag").show();
    $("#salonee_special_date").val(special_data);
    $("#salonee_special_price").val(sp_price)
    $('#salonee_special_date').datepicker('update');
  }else{
    $("#salonee_weekend_name_value").val('')
    $("#salonee_special_price").val('')
  }
  
  $("#dPrice").val(sale_price);
  $("#price").val(base_price);
  $("#sp_id").val(id);
  $("#addDiscount").modal('show');
});
$("#salonee_price_type").change(function(){
  $("#salonee_special_date_tag").hide();
  $("#salonee_weekend_name").hide();
  $("#salonee_special_price_tag").hide();
  if($(this).val()=='custom_price_special_date'){
    $("#salonee_special_date_tag").show();
  }else if($(this).val()=='custom_price_weekend'){
    $("#salonee_weekend_name").show();
  }
});
$(document).ready(function(){
        $("#searchTd").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#saloonTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
         $('.table').dataTable({
          "pageLength": 6
        });

      }); 
      $('#salonee_special_date').datepicker({
        multidate: true,
        format: 'yyyy-mm-dd'
      });
</script>
