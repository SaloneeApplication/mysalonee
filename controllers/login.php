<?php 
	session_start();

	include("../config/dbConnection.php");
	include("functions.php");
		
	$email = $_POST['username'];
	$password = $_POST['password'];
	
	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	if($con)
	{
		$funcObject = new functions();
		
		$message = $funcObject->userDetails($email,$password,$con);

		if($message == "correct")
		{
			$userId = $funcObject->getUserId($email,$con);
			
			$recordSet = $funcObject->getUserDetails($con, $userId);
			while($row = mysqli_fetch_array($recordSet))
			{
			    $username = $row["name"];
			    $email = $row["email"];
			    $mobile = $row["mobile"];
			    $dob = $row["dob"];
			    $image = $row["image"];
			    $area = $row["area"];
			}

			$_SESSION['user_id'] = $userId;
			$_SESSION['username'] = $username;
			$_SESSION['email'] = $email;
			$_SESSION['mobile'] = $mobile;
			$_SESSION['image'] = $image;
			$_SESSION['area'] = $area;

			$result = array("status"=>'200',"message"=>"Logged In Successfully","user_id"=>$userId,"username"=>$username, "email" => $mobile, "mobile"=>$mobile, "dob"=>$dob);
			echo 1;
		}
		else
		{
			$result = array("status"=>'400',"message"=>$message);
    		echo 2;
		}
		
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	}
?>