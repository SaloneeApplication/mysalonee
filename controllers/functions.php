<?php 
	class functions
	{  	
		public function __construct()
		{
			$this->lang = @$_SESSION['lang'];
		}

		public function get_geo()
		{
			$location = file_get_contents('https://geolocation-db.com/json/'.$_SERVER['REMOTE_ADDR']);
			return $location;
		}

		public function checkUserEmail($email,$con)
	    {
	         $sql11 = "select count(*) from user where email = '".$email."'";
	        
	         $recordSet11 = mysqli_query($con,$sql11);
	         while($row11 = mysqli_fetch_array($recordSet11))
	         {
	             $count11 = $row11[0];
	         }
	         if($count11 > 0)
	         {
	             return true;
	         }
	         else
	         {
	             return false;
	         }  
	    }
	    //-------------------------------------------------------------------------------------
	    //Siva 
	    //-------------------------------------------------------------------------------------
	    public function serviceCities($con,$top=1){
            $sql9 = "select * from service_cities where is_top='$top' and status='1' ORDER BY name ASC";
            $recordSet9 = mysqli_query($con,$sql9); 
            return $recordSet9;
        }
        
        public function ourProducts($con,$id){
            
            $sql9 ="SELECT 
            p.id product_id,
            p.product_title, 
            p.product_description, 
            p.base_price, 
            p.sale_price,
            p.product_image,
            c.name
            FROM
            products p
            LEFT JOIN category c USING(category_id) where p.service_provider_id='$id'";
            
            $recordSet9 = mysqli_query($con,$sql9); 
            return $recordSet9;
        }
        
        
        //-------------------------------------------------------------------------------------
	    //Siva 
	    //-------------------------------------------------------------------------------------
		
		public function userDetails($email,$password,$con)
		{
			$emailExistence = $this->checkUserEmail($email,$con);
			if($emailExistence)
			{
				$sql12 = "select password from user where email = '".$email."'";
				$recordSet12 = mysqli_query($con,$sql12);
				while($row12 = mysqli_fetch_array($recordSet12))
				{
					$dbPassword12 = $row12[0];
				}
				if($dbPassword12 == $password)
				{
					return "correct";
				}
				else
				{
					return "Incorrect Password";
				}
			}
			else
			{
				return "Incorrect Email";
			}
		}
		
		public function getUserId($email,$con)
		{
			$sql13 = "select user_id from user where email = '".$email."'";
			$recordSet13 = mysqli_query($con,$sql13);
			while($row13 = mysqli_fetch_array($recordSet13))
			{
				$talentId = $row13[0];
			}
			return $talentId;
		}

		public function getUserDetails($con, $userId)
		{
		    $sql = "select * from user where user_id = $userId";
		    $recordSet = mysqli_query($con,$sql);
		    return $recordSet;
		}

        public function categoriesList($con)
        {
            $sql17 = "select * from category ORDER BY category_for";
            $recordSet17 = mysqli_query($con,$sql17);
            return $recordSet17;
        }

        public function categoriesListWomen($con, $limit = "")
        {
            $sql17 = "select * from category where category_for = 'Women' and status='1'";

            if($limit != "")
            {
            	$sql17 .= "LIMIT $limit";
            }
            $recordSet17 = mysqli_query($con,$sql17);
            return $recordSet17;
        }

        public function categoriesListMen($con, $limit = "")
        {
            $sql17 = "select * from category where category_for = 'Men' and status='1' ";

            if($limit != "")
            {
            	$sql17 .= "LIMIT $limit";
            }
            $recordSet17 = mysqli_query($con,$sql17);
            return $recordSet17;
        }

        public function categoriesListChildren($con, $limit = "")
        {
            $sql17 = "select * from category where category_for = 'Children' and status='1' ";

            if($limit != "")
            {
            	$sql17 .= "LIMIT $limit";
            }
            $recordSet17 = mysqli_query($con,$sql17);
            return $recordSet17;
        }

        public function servicesByCategory($con, $category_id, $sp_id)
        {
            $sql = "SELECT ss.service_provider_service_id as service_id, ss.service_provider_id, ss.service_name as name, 
		            ss.service_at, ss.description, ss.image, sp.business_name as service_provider_name, spc.sale_price
		            FROM service_provider_services ss 
		            JOIN service_provider sp ON sp.service_provider_id = ss.service_provider_id
		            JOIN services_prices spc ON spc.service_provider_service_id = ss.service_provider_service_id";

            if($category_id != "")
            {
            	$sql .= " WHERE find_in_set('$category_id',ss.category_id)";
            }
            else
            {
            	$sql .= " WHERE ss.service_provider_id = '$sp_id'";
            }

            $sql .= " AND spc.price_for <> 'home'";
            
            $recordSet = mysqli_query($con,$sql);
            return $recordSet;
        }
		
		public function getDiscounts($con)
		{
			$date = date('Y-m-d');
		    $sql = "select * from promocodes WHERE expiry_date >= '$date' order by id DESC";
		    $recordSet = mysqli_query($con,$sql);
			return $recordSet;
		}

		public function getCountries($con)
		{
		    $sql34 = "select * from countries order by country_name asc";
		    $recordSet34 = mysqli_query($con,$sql34);
			return $recordSet34;
		}
		
		public function getCities($con)
		{
		    $sql36 = "select * from cities";
		    $recordSet36 = mysqli_query($con,$sql36);
			return $recordSet36;
		}

		public function shopList($con)
	    {
	        $sql = "SELECT service_provider_id, business_name, image, mobile, address 
	        		FROM service_provider";

	        $city_id = @$_SESSION['city_id'];

	        if($city_id != "")
            {
            	$sql .= " WHERE city = '$city_id' AND status = 1";
            }
            else
            {
            	$sql .= " WHERE status = 1";
            }

	        $recordSet = mysqli_query($con,$sql);
	        return $recordSet;
	    }

	    public function shopDetails($con, $shop_id)
	    {
	        $sql = "SELECT * FROM service_provider WHERE service_provider_id = '$shop_id'";
	        $recordSet = mysqli_query($con,$sql);
	        return $recordSet;
	    }

	    public function bookingHistory($con, $user_id)
	    {
	    	$sql = "SELECT sp.*, sps.service_name, sps.service_provider_service_id, sps.image as service_image, ss.total_amount, ss.slot_date, ss.slot_id
	    			FROM service_slots ss
	    			JOIN service_provider_services sps ON sps.service_provider_service_id = ss.service_id
	    			JOIN service_provider sp ON sp.service_provider_id = ss.service_provider_id
	    			WHERE ss.user_id = '$user_id'";
	        $recordSet = mysqli_query($con,$sql);
	        return $recordSet;
	    }

	    public function bookings($con, $user_id)
	    {
	    	$sql = "SELECT sp.*, sps.service_name, sps.service_provider_service_id, sps.image as service_image, ss.total_amount, ss.slot_date, ss.slot_id
	    			FROM service_slots ss
	    			JOIN service_provider_services sps ON sps.service_provider_service_id = ss.service_id
	    			JOIN service_provider sp ON sp.service_provider_id = ss.service_provider_id
	    			WHERE ss.service_status = 'pending' AND ss.user_id = '$user_id'";
	        $recordSet = mysqli_query($con,$sql);
	        return $recordSet;
	    }

	    public function getSaloneeTimings($con, $service_provider_id)
	    {
	        $sql="SELECT * FROM service_provider_timings WHERE service_provider_id='$service_provider_id'";
	        $recordSet = mysqli_query($con,$sql);
	        return $recordSet;
	    }

	    //dynamic content
	    public function home_content($con)
	    {
	    	$lang = $this->lang;

	    	$sql="SELECT * FROM home_page_content WHERE language_id = '$lang'";
	        $recordSet = mysqli_query($con,$sql);
	        return $recordSet;
	    }

	    public function nav_links($con)
	    {
	    	$lang = $this->lang;

	    	$sql="SELECT * FROM nav_links WHERE language_id = '$lang'";
	        $recordSet = mysqli_query($con,$sql);
	        return $recordSet;
	    }

	    public function blogsList($con, $limit = "")
        {
            $sql17 = "select * from blogs";
            if($limit != "")
            {
            	$sql17 .= " LIMIT $limit";
            }
            $recordSet17 = mysqli_query($con,$sql17);
            return $recordSet17;
        }

        public function blogDetails($con, $blog_id)
        {
            $sql17 = "select * from blogs where blog_id = '$blog_id'";
            $recordSet17 = mysqli_query($con,$sql17);
            return $recordSet17;
        }

        public function banners($con)
        {
            $sql17 = "select * from banners where status = 1";
            $recordSet17 = mysqli_query($con,$sql17);
            return $recordSet17;
        }

	    public function getAllUserPromocodes($con)
	    {
	        $sql = "SELECT * FROM promocodes WHERE service_provider_id=0";
            $recordSet = mysqli_query($con, $sql);
            return $recordSet;
	    }
	    
	    public function privacyPrivacy($con)
	    {
	        $sql = "SELECT * FROM privacy_policy WHERE language = 'EN'";
            $recordSet = mysqli_query($con, $sql);
            return $recordSet;
	    }

	    public function cart_services($con, $user_id)
	    {
	    	$sql = "SELECT c.cart_id, ss.service_provider_service_id as service_id, ss.service_name, ss.image, ss.service_provider_id
	    			FROM cart c
	    			JOIN service_provider_services ss ON ss.service_provider_service_id = c.service_id
	    			WHERE c.type = 'service' AND c.user_id = '$user_id' AND c.status = 1";
	    	$recordSet = mysqli_query($con, $sql);
            return $recordSet;
	    }

	    public function getServicePrices($con, $type, $service_id)
	    {
	    	$sql = "SELECT base_price, sale_price FROM services_prices
	    			WHERE price_for = '$type' AND service_provider_service_id = '$service_id'";
	    	$recordSet = mysqli_query($con, $sql);
            return $recordSet;
	    }

	    public function cart_products($con, $user_id)
	    {
	    	$sql = "SELECT c.cart_id,c.quantity, p.id as product_id, p.product_title, p.product_image, p.base_price, p.sale_price FROM cart c
	    			JOIN products p ON p.id = c.product_id
	    			WHERE c.type = 'product' AND c.user_id = '$user_id' AND c.status = 1";
	    	$recordSet = mysqli_query($con, $sql);
            return $recordSet;
	    }

	    public function check_service_in_chart($con, $user_id,$type,$id){
			if($type=='service'){
				$sql = "SELECT c.cart_id
				FROM cart c
				WHERE c.type = 'service' AND c.user_id = '$user_id' AND c.status = 1 AND c.service_id = '$id'";
				$recordSet = mysqli_query($con, $sql);
				$rs = mysqli_num_rows($recordSet);
			}
			else{
				$sql = "SELECT c.cart_id
				FROM cart c
				WHERE c.type = 'product' AND c.user_id = '$user_id' AND c.status = 1 AND c.product_id = '$id'";
				$recordSet = mysqli_query($con, $sql);
				$rs = mysqli_num_rows($recordSet);
			}
			return $rs;
	    }
	}
?>