<?php
include("../config/dbConnection.php");
include("../config/constants.php");
include("functions.php");

$dbObject = new dbConnection();
$con = $dbObject->getConnection();

$funcObject = new functions();

$category_id = @$_POST['category_id'];
$sp_id = @$_POST['sp_id'];

$category_id = str_replace(',', '|', $category_id);

$sql = "SELECT ss.service_provider_service_id as service_id, ss.service_provider_id, ss.service_name as name, 
		            ss.service_at, ss.description, ss.image, sp.business_name as service_provider_name
		            FROM service_provider_services ss 
		            JOIN service_provider sp ON sp.service_provider_id = ss.service_provider_id";

        if($category_id != "" && $sp_id == "")
        {
        	$sql .= " WHERE CONCAT(',', ss.category_id, ',') REGEXP ',(".$category_id."),' ";
        }
        else if($category_id != "" && $sp_id != "")
        {
        	$sql .= " WHERE ss.service_provider_id = '$sp_id' AND CONCAT(',', ss.category_id, ',') REGEXP ',(".$category_id."),' ";
        }
        else
        {
        	$sql .= " WHERE ss.service_provider_id = '$sp_id'";
        }
        
        $services = mysqli_query($con,$sql);

		while($row = mysqli_fetch_array($services))
		{
			echo   '<div class="card servcs_card" style="width: 18rem;">
				        <div class="imgOuter">
				            <img class="card-img-top" data-animation="flipInY" data-timeout="400" src='.ADMIN_URL.$row['image'].' alt="Service Image">
				        </div>
				        <div class="card-body">
				            <input type="hidden" name="service_at" value='.$row['service_at'].'>
				            <input type="hidden" name="service_id" value="'.$row['service_id'].'">
				            <input type="hidden" name="service_provider_id" value='.$row['service_provider_id'].'>
				            <h5 class="card-title lineOneLimit" title='.$row['name'].'>'.$row['name'].'</h5>
				            <p class="card-text lineThreeLimit">'.$row['description'].'</p>
				            <h6 class="card-title lineOneLimit" title='.$row['service_provider_name'].'>'.$row['service_provider_name'].'</h6>
				            <div class="threeFive">
                                <div class="stars-outer">
                                    <div class="stars-inner" style="width: 70%;"></div>
                                </div>
                                <span class="number-rating">3.5</span>
                              </div>
                              <p class="mb-1 "><i><img src="assets/img/icons/clock.png" alt="clock" /></i><span> Opens at 10:00 AM</span>
                            </p>
                            <p class="pb-4 d-flex"><i><img src="assets/img/icons/loc.png" alt="loc" /></i>&nbsp;
                            <span class="lineTwoLimit"> L.G. Heights, 1st Floor, Above Hitech City Rd, near Happi mobiles, Madhapur, Telangana 500085</span></p>
				            <a href="shop-details.php?id='.$row['service_provider_id'].'" class="_btn book">View</a>
				        </div>
				    </div>';
		}