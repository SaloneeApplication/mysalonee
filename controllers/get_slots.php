<?php 

	include("../config/dbConnection.php");

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	$slot_id = $_POST['slot_id'];
	$service_provider_id = $_POST['service_provider_id'];
	$slot_date = date('Y-m-d', strtotime($_POST['slot_date']));

    $sql = "SELECT s.*,sp.time_slot_freq,p.same_time_slots_allowed,p.allowed_slots_count,p.per_day_slots_limit,p.price_for 
    		FROM service_slots s, service_provider_services sp ,services_prices p 
    		WHERE s.service_id = sp.service_provider_service_id 
    		AND sp.service_provider_service_id = p.service_provider_service_id 
    		AND s.slot_id = '$slot_id'
    		AND sp.service_provider_id = '$service_provider_id'
    		HAVING p.price_for = s.service_type";
    $recordSet = mysqli_query($con,$sql);

    while($row = mysqli_fetch_array($recordSet))
	{
		$freq = $row['time_slot_freq'];
        $service_type = $row['service_type'];
        $service_id = $row['service_id'];
        $same_time_slots_allowed = $row['same_time_slots_allowed'];
        $allowed_slots_count = $row['allowed_slots_count'];
        $per_day_slots_limit = $row['per_day_slots_limit'];
	}
	
	$day = strtolower(date("l",strtotime($slot_date)));

	$sql1 = "SELECT * FROM service_provider_timings 
			WHERE service_provider_id = '$service_provider_id' AND day_name = '$day'";
	$recordSet1 = mysqli_query($con,$sql1);

	while($row1 = mysqli_fetch_array($recordSet1))
	{
		$from_time = $row1['from_time'];
	    $to_time = $row1['to_time'];
	    $available_status = $row1['available_status'];
	    $total_minutes = (strtotime($to_time)-strtotime($from_time))/60;
	    $total_slots = $total_minutes/$freq;
	}
    
    for($i=0;$i<$total_slots;$i++){
        $time1=date("H:i A",strtotime($from_time));
        $time=date("H:i",strtotime($from_time));
        $act_check_time=$slot_date." ".date("H:i",strtotime($from_time)).":00";

        if(strtotime($act_check_time)<time()){
            //time done 
            //for booked design
            echo '<li title="salonee time over"><input type="radio"  class="booked" disabled id="radio'.$i.'" name="radios" value="" ><label for="radio'.$i.'">'.$time1.'</label></li>';
        }
        else
        {
    		$sql2 = "SELECT slot_id from service_slots 
    				where service_id = '$service_id' and slot_date = '$slot_date' and service_type = '$service_type'";
			$recordSet2 = mysqli_query($con,$sql2);

			$total_booked_slots = mysqli_num_rows($recordSet2);

            if($total_booked_slots>=$per_day_slots_limit && $per_day_slots_limit!=0)
            {
                //per day slots booking limit over 
                echo '<li title="per day slots limit booked over "><input required type="radio" class="booked" disabled id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
            }
            else
            {
                //if same time slot not allowed 
                if($same_time_slots_allowed==0)
                {
                    if($total_booked_slots>0)
                    {
                        echo '<li title="slot booked"><input required type="radio" class="booked" disabled id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                    }
                    else
                    {
                        echo '<li title="slot available"><input required type="radio"  id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                    }
                }
                else
                {
                    if($allowed_slots_count<$total_booked_slots)
                    {
                        //allowed limit over slots
                        echo '<li title="same time slot booking limit over"><input required type="radio" class="booked" disabled id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                    }
                    else
                    {
                        echo '<li title="slot available"><input type="radio" required id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                    }
                }
            }
        }

        $from_time=date("H:i",strtotime("+".$freq." minutes",strtotime($from_time)));
    }

    exit;
?>