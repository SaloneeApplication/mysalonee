<?php 

class cart{ 
		public $dbObjects = '';
		public $con= '';
		public function __construct(){
			$this->lang = @$_SESSION['lang'];
			$this->dbObjects = new dbConnection();
			$this->con = $this->dbObjects->getConnection();
		}

		public function getServiceList($user_id){//call in shop-details.php
$s = "SELECT c.cart_id,ss.service_provider_service_id as service_id, ss.service_name, ss.image, ss.service_provider_id,
(SELECT base_price FROM services_prices spc WHERE spc.service_provider_service_id= ss.service_provider_service_id AND spc.price_for='salonee' LIMIT 1) AS base_salon_price,
(SELECT base_price FROM services_prices spc WHERE spc.service_provider_service_id= ss.service_provider_service_id AND spc.price_for='home' LIMIT 1) AS base_home_price,
(SELECT sale_price FROM services_prices spc WHERE spc.service_provider_service_id= ss.service_provider_service_id AND spc.price_for='salonee' LIMIT 1) AS salon_price,
(SELECT sale_price FROM services_prices spc WHERE spc.service_provider_service_id= ss.service_provider_service_id AND spc.price_for='home' LIMIT 1) AS home_price
FROM cart c
JOIN service_provider_services ss ON ss.service_provider_service_id = c.service_id
JOIN service_provider sp ON sp.service_provider_id = ss.service_provider_id 
WHERE c.type = 'service' AND c.user_id = '$user_id' AND c.status='1'";
			$rs = mysqli_query($this->con,$s);
			if(mysqli_num_rows($rs)>0){
				$output = $rs;
			}
			else{
				$output = array();
			}
            return $output;
        }

	    public function cart_products($user_id){
	    	$s = "SELECT c.cart_id,c.quantity, p.id as product_id, p.product_title, p.product_image, p.base_price, p.sale_price FROM cart c
	    			JOIN products p ON p.id = c.product_id
	    			WHERE c.type = 'product' AND c.user_id = '$user_id' AND c.status = 1";
			$rs = mysqli_query($this->con,$s);
			if(mysqli_num_rows($rs)>0){
				$output = $rs;
			}
			else{
				$output = array();
			}
            return $output;
	    }

	}
?>