<?php 
include("../config/dbConnection.php");
$dbObject = new dbConnection();
$con = $dbObject->getConnection();
$output = array('status'=>'error','message'=>'there is some problem in cart. Try again.');	
$total_qty = $total_amount = 0;

	$service_id = @$_POST['service_id'];
	$provider = @$_POST['provider'];
	$product_id = @$_POST['product_id'];
	$user_id = @$_POST['user_id'];
	$type = @$_POST['type'];
	if($con){
		if($user_id){
			if($provider){
				$addCart =false;
				$s = "SELECT  DISTINCT(provider_id) as provider_id FROM cart WHERE user_id='$user_id' and status='1'";
				$rs = mysqli_query($con,$s);
				if(mysqli_num_rows($rs)==0){
					$addCart =true;
				}
				else if(mysqli_num_rows($rs)==1){
					foreach($rs as $set_rs){
						if($set_rs['provider_id']==$provider){
							$addCart =true;
						}
					}
				}
				if($addCart){
					if($type == 'service'){
						$sql = "SELECT ss.service_provider_service_id AS service_id, ss.service_provider_id,
		(SELECT sale_price FROM services_prices spc WHERE spc.service_provider_service_id= ss.service_provider_service_id AND spc.price_for='salonee' LIMIT 1) AS salon_price,
		(SELECT sale_price FROM services_prices spc WHERE spc.service_provider_service_id= ss.service_provider_service_id AND spc.price_for='home' LIMIT 1) AS home_price
		FROM service_provider_services ss 
		JOIN service_provider sp ON sp.service_provider_id = ss.service_provider_id WHERE ss.service_provider_service_id = '$service_id' and ss.service_provider_id='$provider'";
		/*				
						$sql = "SELECT ss.service_provider_service_id as service_id, spc.sale_price
								FROM service_provider_services ss 
								JOIN service_provider sp ON sp.service_provider_id = ss.service_provider_id
								JOIN services_prices spc ON spc.service_provider_service_id = ss.service_provider_service_id";
						$sql .= " where ss.service_provider_service_id = '$service_id'";*/
						$result = $dbObject->get_query($sql);
						if($result&&!empty($result->salon_price)&&!empty($result->home_price)){
							$output['status'] = 'ok';
							$output['message'] = '';
							$cart_id = '';
							$sql = "SELECT cart_id,quantity FROM cart where service_id = '$service_id' AND user_id = '$user_id' AND status = 1";
							$rowsAffected = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($rowsAffected))
							{
								$cart_id = $row['cart_id'];
								$quantity = $row['quantity'];
							}
							if($cart_id != "")
							{
								//$sql1 = "UPDATE CART SET quantity = ($quantity + 1) where cart_id = '$cart_id'";
								$sql1 = "UPDATE CART SET quantity = 1,amount='$result->salon_price',provider_id='$provider'  where cart_id = '$cart_id'";
							}
							else
							{
								$sql1 = "INSERT INTO cart(type, service_id, user_id, quantity,amount,provider_id) 
								VALUES('$type', '$service_id', '$user_id', 1,'$result->salon_price','$provider')";
							}
							$rowsAffected = mysqli_query($con,$sql1);
						}
						else{
							$output['message'] = 'There is no service or price';
						}
					}
					else if($type == 'product'){
						$qty_type = @$_POST['qty_type'];
						$s ="SELECT p.id, p.sale_price FROM products p where p.id='$product_id'";
						$result = $dbObject->get_query($s);
						if($result){
							$output['status'] = 'ok';
							$output['message'] = '';
							$s = "SELECT cart_id,quantity FROM cart where product_id = '$product_id' AND user_id = '$user_id' AND status = 1";
							$result_cart = $dbObject->get_query($s);
							if($result_cart){
								$qty = $result_cart->quantity;
								if($qty_type=='minus'){
									if($result_cart->quantity>1){
										$qty = $result_cart->quantity-1;
										$sql1 = "update `cart` SET quantity='$qty',amount='$result->sale_price',provider_id='$provider'  where cart_id = '$result_cart->cart_id'";
										$rowsAffected = mysqli_query($con,$sql1);
									}
									else{
										$qty=0;
										$sql = "delete from `cart` where cart_id = '$result_cart->cart_id'";
										$con->query($sql);
									}
								}
								else if($qty_type=='add'){
									if($result_cart->quantity<5){
										$qty = $result_cart->quantity+1;
										$sql = "update `cart` SET quantity='$qty',amount='$result->sale_price',provider_id='$provider'  where cart_id = '$result_cart->cart_id'";
										$con->query($sql);
									}
								}
							}
							else{
								$qty= 1;
								$sql1 = "INSERT INTO cart(type, product_id, user_id, quantity,amount,provider_id) 
								VALUES('$type', '$product_id', '$user_id', 1,'$result->sale_price',$provider)";
								$rowsAffected = mysqli_query($con,$sql1);
							}
							$output['status'] = 'ok';
							$output['message'] = '';
							$output['qty'] = $qty;
							
						}
					}
				}
				else{
					$output['message'] = 'other provider';
				}
			}
			$sql = "SELECT sum(quantity) as t_qty,sum(quantity*amount) as t_amount,provider_id FROM cart where  user_id = '$user_id' AND status = 1";
			$result = $dbObject->get_query($sql);
			$p_id = 0;
			if($result){
				if(!empty($result->provider_id)){$p_id = $result->provider_id;}
				if(!empty($result->t_qty)){$total_qty = $result->t_qty;}
				if(!empty($result->t_amount)){$total_amount = round($result->t_amount,2);}
			}
			$output['provider'] = $p_id;
			$output['items'] = $total_qty;
			$output['amount'] = $total_amount;
		}
		else{
			$output['message'] = 'Please login first!';
		}
	}
/*	else{
		echo mysqli_errno()."<br/>".mysqli_error();
	} */
	echo json_encode($output);
?>