<?php 

	include("../config/dbConnection.php");

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	$type = $_POST['type'];
	$service_provider_id = $_POST['service_provider_id'];
	$slot_date = date('Y-m-d', strtotime($_POST['date']));

	$freq = 60; //$row['time_slot_freq'];
    $same_time_slots_allowed = 0; //$row['same_time_slots_allowed'];
    $allowed_slots_count = 0; //$row['allowed_slots_count'];
    $per_day_slots_limit = 10; //$row['per_day_slots_limit'];
	
	$day = strtolower(date("l",strtotime($slot_date)));

	$sql1 = "SELECT * FROM service_provider_timings 
			WHERE service_provider_id = '$service_provider_id' AND day_name = '$day'";
	$recordSet1 = mysqli_query($con,$sql1);

	while($row1 = mysqli_fetch_array($recordSet1))
	{
		$from_time = $row1['from_time'];
	    $to_time = $row1['to_time'];
	    $available_status = $row1['available_status'];
	    $total_minutes = (strtotime($to_time)-strtotime($from_time))/60;
	    $total_slots = $total_minutes/$freq;
	}
    
    for($i=0;$i<@$total_slots;$i++)
    {
        $time1=date("H:i A",strtotime($from_time));
        $time=date("H:i",strtotime($from_time));
        $act_check_time=$slot_date." ".date("H:i",strtotime($from_time)).":00";

        if(strtotime($act_check_time)<time()){
            //time done 
            //for booked design
            echo '<li title="salonee time over"><input type="radio"  class="booked" disabled id="radio'.$i.'" name="radios" value="" ><label for="radio'.$i.'">'.$time1.'</label></li>';
        }
        else
        {
    		$sql2 = "SELECT slot_id from service_slots 
    				where slot_date = '$slot_date' and service_type = '$type'";
			$recordSet2 = mysqli_query($con,$sql2);

			$total_booked_slots = mysqli_num_rows($recordSet2);

            if($total_booked_slots>=$per_day_slots_limit && $per_day_slots_limit!=0)
            {
                //per day slots booking limit over 
                echo '<li title="per day slots limit booked over "><input required type="radio" class="booked" disabled id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
            }
            else
            {
                //if same time slot not allowed 
                if($same_time_slots_allowed==0)
                {
                    if($total_booked_slots>0)
                    {
                        echo '<li title="slot booked"><input required type="radio" class="booked" disabled id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                    }
                    else
                    {
                        echo '<li title="slot available"><input required type="radio"  id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                    }
                }
                else
                {
                    if($allowed_slots_count<$total_booked_slots)
                    {
                        //allowed limit over slots
                        echo '<li title="same time slot booking limit over"><input required type="radio" class="booked" disabled id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                    }
                    else
                    {
                        echo '<li title="slot available"><input type="radio" required id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                    }
                }
            }
        }

        $from_time=date("H:i",strtotime("+".$freq." minutes",strtotime($from_time)));
    }

    exit;


    /*$sql1 = "SELECT slot_id, service_provider_id, service_id, slot_date, total_amount
    		FROM service_slots 
    		WHERE service_id = '$service_id' AND date(slot_date) = '$date' ORDER BY slot_date";
    $recordSet = mysqli_query($con,$sql1);

    $data = array();
    while($row1 = mysqli_fetch_array($recordSet))
	{
		$slots = array();
		$slots["slot_id"] = $row1["slot_id"];
		$slots["service_provider_id"] = $row1["service_provider_id"];
		$slots["service_id"] = $row1["service_id"];
		$slots["total_amount"] = $row1["total_amount"];
		$slots["date"] = date('d-m-Y', strtotime($row1["slot_date"]));
		$slots["time"] = date('H:i A', strtotime($row1["slot_date"]));
		array_push($data, $slots);
	}
	
	$result = array("status"=>"200","slots"=>$data);
	echo json_encode($result);*/
?>