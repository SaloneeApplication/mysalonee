<?php 

	include("../config/dbConnection.php");

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
    $coupon_code = $_POST['coupon'];
    $date = $_POST['date'];    
    $sql = "SELECT * FROM promocodes WHERE coupon_code='$coupon_code' AND status=1";

    $rowsAffected = mysqli_query($con,$sql);

    if(mysqli_num_rows($rowsAffected) > 0)
    {
        while($row = mysqli_fetch_array($rowsAffected))
        {
             $date_now = date('Y-m-d');//$date;
             $date2    = $row['expiry_date'];
            if($date2 >= $date_now )
            {
                $result['data'] = (int)$row['discount_per'];
                $result['status'] = 200;
                $result['message'] = 'Coupon Applied';
            }else{
                $result['status'] = 400;
                $result['message'] = 'Coupon Expired';
            }
        }
    }
    else
    {
        $result['status'] = 400;
        $result['message'] = 'Coupon not found';
    }    

    echo json_encode($result);
?>