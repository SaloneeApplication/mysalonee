<?php 
   require_once dirname(__DIR__).'/core/Controller.php';    
	include("../config/dbConnection.php");
	include("functions.php");

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	//Retrieving Form Fields
	$name = $_POST['full_name'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$mobile_code = $_POST['mobile_code'];
	$mobile = $_POST['mobile_number'];
	$gender = $_POST['gender'];
	$area = $_POST['area'];

	function generateNumericOTP($n)
	{
		$generator = "1357902468";
	    $result = "";
	  
	    for ($i = 1; $i <= $n; $i++) {
	        $result .= substr($generator, (rand()%(strlen($generator))), 1);
	    }
	  
	    // Return result
	    return $result;
	}

	$otp = generateNumericOTP(4);
	$message = "Use ".$otp." as your verification code on MySalonee. Do not share your OTP with anyone.";
	//$url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=ee7dzmQudDi&MobileNo=".$mobile_code."".$mobile."&SenderID=TESTID&Message=".$message."&ServiceName=INTERNATIONAL";
        $postData = [
            'APIKEY'            => 'ee7dzmQudDi',
            'MobileNo'          => $mobile_code."".$mobile,
            'Message'           => $message,
            'SenderID'          => 'TESTID',
            'ServiceName'       => 'INTERNATIONAL',
            'response'          => 'json',
            ];
        $url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);//get response
        
        //Print error if any
        curl_close($ch);
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, $url);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // $output = curl_exec($ch);
    // curl_close($ch);
    $user_id = "";
    if($con)
    {
        $sql1 = "SELECT user_id, status FROM user WHERE mobile = '$mobile'";
        $recordSet = mysqli_query($con,$sql1);
        while($row = mysqli_fetch_array($recordSet))
		{
			$user_id = $row['user_id'];
		}
        
        if($user_id!="")
        {
            $sql = "UPDATE user SET name = '$name', email = '$email', password = '$password', otp = '$otp', gender = '$gender', area = '$area' WHERE user_id = '$user_id'";
            $rowsAffected = mysqli_query($con,$sql);
            
            if($rowsAffected > 0)
            {
            	echo $user_id;
            } 
            else
            {
            	echo FALSE;
            }
        }
        else
        {
            $sql = "INSERT INTO user (name, email, password, mobile_code, mobile, otp, gender, area) VALUES ('".$name."', '".$email."', '".$password."', '".$mobile_code."', '".$mobile."', '".$otp."', '".$gender."', '".$area."')";
    
            $rowsAffected = mysqli_query($con,$sql);
            $user_id = $con->insert_id;
    
            if($rowsAffected > 0)
            {	//echo $url;
            	echo $user_id;
            } 
            else
            {
            	echo FALSE;
            }
        }
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	} 
	     
?>
