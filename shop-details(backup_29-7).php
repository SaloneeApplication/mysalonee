<?php include('header.php');

$user_session_id = @$_SESSION['user_id'];
$shop_details = $funcObject->shopDetails($con, $_GET['id']);
$shop_details = mysqli_fetch_assoc($shop_details);

$products_list = $funcObject->ourProducts($con, $_GET['id']);
$services = $funcObject->servicesByCategory($con, $category_id="", $_GET['id']);

?>

<style type="text/css">
.card-wp{
	position:fixed;
	bottom:0;
    width: 100%;
}
div.nearShops ul{
	max-width:100%;
}
.nearShops .ourSrvice ul li {
    flex: 0 0 23%;
    max-width: 23%;
}
.ourSrvice .imgBoxx {
    width: 100%;
}

.addToCart{
	cursor:pointer;
    position: absolute;
    right: 7px;
    top: 4px;
    color: #FFF;
    z-index: 1;
}
.modal {
  overflow-y:auto;
}
.img_container{
    display: inline-block;
    font-size: 40px;
    line-height: 50px;
    color:#c96c92;
    width: 50px;
    height: 50px;
    text-align: center;
    vertical-align: bottom;
    left: 50px;
    margin-left: 45%;
    margin-top: 10%;
}

.help { display: inline-block; position: relative; }

.help-button {
  color: #222;
  font-weight: bold;
  text-decoration: none;
}

.info-box {
    border-radius: 15px;
    border: 2px solid #bdbdbd;
    box-shadow: 8px 10px 10px 0px #888888;
    background-color: #ddd;
    display: none;
    color: #d55c91;
    /*background-color: #ddd;*/
    /*display: none;*/
    /*color: #888;*/
    font-family: sans-serif;
    font-size: smaller;
    padding: 10px;
    width: 170px;
    position: absolute;
    right: -190px;
    bottom: 0;
}

.info-box::after {
    border-top: 20px solid transparent;
    border-right: 20px solid #ddd;
    content: " ";
    display: block;
    position: absolute;
    left: -20px;
    bottom: 0;
    width: 0;
    height: 0;
}

.info-box .close-button {
    border: 1px dotted #222;
    color: #222;
    float: right;
    line-height: 0.6em;
    padding: 0;
    text-decoration: none;
}

.info-box .close-button:hover {
    border-color: #aaa;
    color: #aaa;
}
</style>
<style>
    ._cartModal1 {
        max-width: 1000px;
        margin: 0 auto; 
        display: none;
        z-index: 999;
    }
    ._cartModal1 ._list{ 
        background: #f1f1f1;
        padding: 2em;
        box-shadow: 0px -2px 3px #ddd;
    }
    ._list h5{
            display: flex
        }
    /* .card._cartModal1::before {
        position: fixed;
        top: 0;
        left: 0;
        content: '';
        width: 100%;
        height: 100%;
        background: #00000075;
        z-index: -1;
    }  */
    input#itemCOunt {
    border: 0;
    width: auto;
    max-width: 50px;
    text-align: center;
    font-size: 20px;
    font-weight: 600;
    outline: none;
    box-shadow: none;
    cursor: default;
    background: #f1f1f1;

}
</style>

<link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<div class="container-fluid">
    <div class="_header"></div>
<div class="container" >
<nav aria-label="breadcrumb" class="_custmBrdcrmb">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="index.php">Home</a></li>
<li class="breadcrumb-item"><a href="index.php#feature-box">Shops</a></li>
<li class="breadcrumb-item active" aria-current="page">Shop Details</li>
</ol>
</nav>
</div>

    <div class="nearShops">
        <div class="container" >
            <div class="row">
                <div class="col-md-5">
                    <div class="imgOutBox">
                        <img src="<?php echo user_base_url.$shop_details['image'];?>" alt=""  />
                    </div> 
                </div>
                 <div class="col-md-5 _slon">
                    <h2 class="_font"><?php echo $shop_details['business_name'];?></h2>
                    <div class="threeFive f-2x">
                        <!-- <span class="number-rating">3.5</span> -->
                        <div class="stars-outer">
                            <div class="stars-inner" style="width: 70%;"></div>
                        </div>
                    </div>
                    <p><i><img src="assets/img/icons/clock.png" alt="clock" /></i><span> Opens at 10:00 AM</span>
                    <p><i><img src="assets/img/icons/loc.png" alt="loc" /></i><span> <?php echo $shop_details['address'];?></span></p>
                               
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="ourSrvice">
                    <h2 class="_font">Our Services</h2>
                        <ul>
<?php
foreach($services as $row){
	$isCartAdded = 0;
	if($user_session_id!=''){
		$isCartAdded = $funcObject->check_service_in_chart($con,$user_session_id,$row['service_id']);
	}
?>

<li class="products">
<div class="card " style="width: 18rem;">
    <div class="prod_IMGBlk">
        <img class="card-img-top" style="height:200px;" src="<?php echo ADMIN_URL.$row['image']; ?>" alt="strawberryShower">
    </div>
    <input type="hidden" id="service_at" name="service_at" value="<?php echo $row['service_at'];?>">
    <input type="hidden" id="service_id" name="service_id" value="<?php echo $row['service_id'];?>">
    <input type="hidden" name="service_provider_id" value="<?php echo $row['service_provider_id'];?>">
    <input type="hidden" name="service_price" value="<?php echo $row['sale_price'];?>">
    <div class="card-body">
        <h5 class="card-title"><?php echo $row['name']; ?></h5>
         <p class="card-text"><?php echo $row['description']; ?></p> 
        <!-- <p class="card-text">Get 250 ml</p> -->
        <h6>AED <?php echo $row['sale_price']; ?></h6>
        <a href="javascript:;" class="btn theme-btn add_cart_service"><?=$isCartAdded>0?'Added':'Add To Cart'?></a>
    </div>
</div>  
</li>                            

<?php
}
?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="ourProdcts">
                        <h2 class="_font">Our Products</h2>
                        <ul>
                            <?php
                            foreach($products_list as $row)
                            {?>
                                <li class="products">
                                    <div class="card " style="width: 18rem;">
                                        <div class="prod_IMGBlk">
                                            <img class="card-img-top" style="height:200px;" src="<?php echo user_base_url.$row['product_image']; ?>" alt="strawberryShower">
                                        </div>
                                        <input type="hidden" id="product_id" name="product_id" value="<?php echo $row['product_id'];?>">
                                        <input type="hidden" id="product_price" name="product_price" value="<?php echo $row['sale_price'];?>">
                                        <div class="card-body">
                                            <h5 class="card-title"><?php echo $row['product_title']; ?></h5>
                                            <!-- <p class="card-text"><?php echo $row['product_description']; ?></p> -->
                                            <!-- <p class="card-text">Get 250 ml</p> -->
                                            <h6>AED <?php echo $row['sale_price']; ?></h6>
                                            <a href="javascript:;" class="btn theme-btn add_cart">Add To Cart</a>
                                        </div>
                                    </div>  
                                </li>
                            <?php
                            }?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- cartBox -->
    <div class="card-wp">
    <div class="card _cartModal1" id="cartBox"  > 
        <div class="_list">
            <div>
                <h5><span id="dismissBox"><i class="fa fa-angle-down" aria-hidden="true"></i></span> 
                <input type="text" id="itemCOunt" value="0"> - Items</h5>
            </div>
            <div>
                <h3 id="final_amount"></h3> <a href="cart.php" class="btn">Proceed</a>
            </div>
        </div> 
    </div>
    </div>
</div>
<?php include('footer.php');?>
<!-- book modal  -->
<div class="modal fade themeModal bookModalBlock" id="bookModal" tabindex="-1" role="dialog" aria-labelledby="bookModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="col-12 modal-title text-center" id="bookModalTitle">Service</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body book-modal">
                <div class="card mt-2" id="req_saloon">
                    <div class="d-flex">
                        <div class="imgOuter1">
                            <img class="card-img-top rounded" src="assets/img/category/hair-setting.jpg" />
                        </div>
                        <div class="m-2 col_ryt">
                            <h5 >Service Provider Location</h5>
                            <p class="card-text">Some quick example text to build on the card title 
                                and make up the bulk of the card's content.</p>
                            <a href="#" id="bookformModal" data-toggle="modal" data-target="#bookModalForm" class="btnViewMore float-right" data-id="">BOOK AN APPOINTMENT</a>
                        </div>
                    </div>
                </div>
                <div class="card mt-2" id="req_home">
                    <div class="d-flex">
                        <div class="imgOuter1">
                            <img class="card-img-top rounded" src="assets/img/category/hair-setting.jpg" />
                        </div>
                        <div class="m-2 col_ryt">
                            <h5 >Service Provider Location </h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" id="bookformModal1" data-id="" data-toggle="modal" data-target="#bookModalForm" class="btnViewMore float-right">REQUEST TO HOME</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- sample modal  -->
<div class="modal fade themeModal" id="bookModalForm" tabindex="-1" role="dialog" aria-labelledby="modalFormTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="col-12 modal-title text-center" id="modalFormTitle">Enter below details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modalForm">
                    <div class="login">
                        <form method="POST" id="booking-form">
                            <input type="hidden" id="service_id">
                            <input type="hidden" id="service_provider_id">
                            <input type="hidden" id="type">
                            <div class="form-group mb-2">
                                <input type="text" id="sDate" class="form-control" autocomplete="off" required>
                                <label class="form-control-placeholder p-0" for="sDate">Date</label>
                            </div>
                            <div class="form-group mb-2">
                                <div class="boxedList">
                                    <div class="flxRow">
                                    <p id="slotLabel">Select Available Slot</p> 
                                    <i class="fa fa-caret-down" aria-hidden="true" id="showSlots"></i> 
                                </div>
                                    <div id="slotsList" style="display: none;">
                                    <ul id="slotData"></ul>
                                </div>
                                </div>
                            </div>
                            <!-- <div class="form-group mb-2">
                                <select name="service" class="form-control" autocomplete="off" required>
                                    <option hidden disabled selected value>
                                        Add List of Services </option>
                                    <option value="service1">service-1</option>
                                    <option value="service2">service-2</option>
                                    <option value="service3">service-3</option>
                                </select>
                            </div> -->
                            <div class="form-group mb-2" id="is-coupon">
                                <label>Coupon<div class="help">
                                    <div class="info-box">
                                        <a href="#" class="close-button">&times;</a>
                                        <h6>Promo Codes</h6>
                                        <?php
                                        while($rowCoupon = mysqli_fetch_array($promoCodes))
                                        {?>
                                            <div><?php echo $rowCoupon['coupon_code']; ?> -- <?php echo $rowCoupon['discount_per']."% OFF"; ?> 
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <a class='help-button' href='#' title="Click to know more">[?]</a>
                                </div></label>
                                <div class="row" style="padding: 0px 15px 0px 15px;">
                                    <input type="text" id="coupon" name="coupon" value="" class="form-control col-md-8" autocomplete="off" required/>
                                    <button type="button" id="apply-coupon" class="btn btn-sm theme-btn col-md-4">Apply</button>
                                    <button type="button" id="cancel-coupon" class="btn btn-sm theme-btn col-md-4">clear</button>
                                    <p id="c_success" style="color: green;">Coupon Applied</p>
                                    <p id="c_fail" style="color: red;">Coupon not available</p>
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label>Check the Payment</label> 
                                <input type="text" id="price" name="price" value="Estimated Price: 00.00" class="form-control" autocomplete="off" required readonly/>
                                <input type="hidden" id="total_amount">
                            </div>
                            <div class="form-group mb-4">
                                <label>Preferences</label>
                                <div class="form-group mb-2">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Male <input type="radio" class="form-check-input" name="optradio">
                                    </label>
                                  </div>
                                  <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Female <input type="radio" class="form-check-input" name="optradio"> 
                                    </label>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <button type="button" id="btn-pay" class="btn theme-btn">Proceed To Pay</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

$('.add_cart').click(function(){

    var sum = 0;
    var price = $(this).closest('li').find("input[name=product_price]").val();

    sum += parseFloat(price);
    $('#final_amount').html('AED '+sum)

    var user_id = "<?php echo @$_SESSION['user_id'];?>";

    if(user_id == '')
    {
        $('#loginModal').modal('show');
    }
    else
    {
        var value = parseInt(document.getElementById('itemCOunt').value, 10); 
        value = isNaN(value) ? 0 : value;
        if(value<10){
            value++;5
                document.getElementById('itemCOunt').value = value;  
                $('#cartBox').show("slow"); //show cartBox on #itemCOunt value >1
        }

        var service_id = '';
        var product_id = $(this).closest('li').find("input[name=product_id]").val();

        var formData = new FormData();

        formData.append('service_id', service_id);
        formData.append('product_id', product_id);
        formData.append('user_id', user_id);
        formData.append('type', 'product');

        $.ajax({
            type:'POST',
            url:'controllers/add_to_cart.php',
            data:formData,
            contentType: false,
            processData: false,
            success:function(res){
            }
        });
    }
});

$('.add_cart_service').click(function(){

    var sum = 0;
    var price = $(this).closest('li').find("input[name=service_price]").val();
    
    sum += parseFloat(price);
    $('#final_amount').html('AED '+sum)

    var user_id = "<?php echo @$_SESSION['user_id'];?>";

    if(user_id == '')
    {
        $('#loginModal').modal('show');
    }
    else
    {
        var value = parseInt(document.getElementById('itemCOunt').value, 10); 
        value = isNaN(value) ? 0 : value;
        if(value<10){
            value++;5
                document.getElementById('itemCOunt').value = value;  
                $('#cartBox').show("slow"); //show cartBox on #itemCOunt value >1
        }

        var service_id = $(this).closest('li').find("input[name=service_id]").val();
        var product_id = '';

        var formData = new FormData();

        formData.append('service_id', service_id);
        formData.append('product_id', product_id);
        formData.append('user_id', user_id);
        formData.append('type', 'service');

        $.ajax({
            type:'POST',
            url:'controllers/add_to_cart.php',
            data:formData,
            contentType: false,
            processData: false,
            success:function(res){
				toastr.success('service successfully added to cart', 'Success', {timeOut: 5000});
            }
        });
    }
});

$(document).ready(function(){  
    // dismiss cartBox
    $("#dismissBox").click(function () {
        $('#cartBox').hide("slow")
    });
});

jQuery(document).ready(function($){
  $('.help-button').on('click', function(e){
    e.preventDefault();
    $(this).siblings('.info-box').show();
  });
  
  $('.close-button').on('click', function(e){
    e.preventDefault();
    $(this).parents('.info-box').hide();
  });
  $('#c_success').hide();
  $('#c_fail').hide();
  $('#is-coupon').hide();
  $('#cancel-coupon').hide();
  $('#apply-coupon').on('click', function(e){
    if(($('#coupon').val() == '') || ($('#coupon').val() == undefined)){
      $('#c_fail').text('Enter Coupon Code');
      $('#c_fail').show();        
    }else{
        var formData = new FormData();        
        formData.append('coupon', $('#coupon').val());
        formData.append('date', $("#sDate").val());
        $.ajax({
            type:'POST',
            url:'controllers/check_coupon.php',
            data:formData,
            contentType: false,
            processData: false,
            success:function(res){
                var res = JSON.parse(res);
                if(res.status == 200)
                {
                  $totalDiscount = $("#original_amount").val() - ((res.data / 100) * $("#original_amount").val());
                  $('#total_amount').val($totalDiscount)
                  $("#price").val('Estimated Price: '+$totalDiscount+' AED');
                  $('#c_success').text(res.message);
                  $('#c_success').show();
                  $('#c_fail').hide();
                } else {
                  $("#price").val('Estimated Price: '+$("#original_amount").val()+' AED');
                  $('#total_amount').val($("#original_amount").val())
                  $('#c_fail').text(res.message);
                  $('#c_fail').show();
                  $('#c_success').hide();
                }                              
            }
        });
    }
  });  
  $('#cancel-coupon').on('click', function(e){
    cancelCoupon();
  });  
});
function cancelCoupon() {
    $('#coupon').val('');
    $('#apply-coupon').show()
    $('#c_success').hide();
    $('#c_fail').hide();
    $('#cancel-coupon').hide()
}
    $(".category_id").change(function() {
        //if(this.checked) 
        //{
            var sp_id = "<?php echo @$sp_id;?>";

            var category_id = [];
            $("input:checkbox[name=category_id]:checked").each(function() {
                category_id.push($(this).val());
            });

            var formData = new FormData();

            formData.append('category_id', category_id);
            formData.append('sp_id', sp_id);

            $.ajax({
                type:'POST',
                url:'controllers/get_services.php',
                data:formData,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    $(".mainDiv").html('<div class="img_container"><i class="fas fa-sync fa-spin"></i></div>');
                },
                success:function(res){

                    $(".cat-data").html(res);                          
                }
            });
        //}
    });

    $('#sDate').datepicker({
        format: 'yyyy-mm-dd',
        todayHighlight:true,
        autoclose:true,
        startDate:"<?php echo date("Y-m-d");?>",
        endDate :"<?php echo date("Y-m-d",strtotime("+20 days"));?>"

    });

    $('#bookformModal').click(function () {
        $('#bookModal').modal('hide');
        $('#type').val('salonee')
    })

    $('#bookformModal1').click(function () {
        $('#bookModal').modal('hide');
        $('#type').val('home')
    })

    $('#showSlots').click(function() {
        $('#slotsList').toggle('100');
        $(this).toggleClass("fa-caret-up fa-caret-down");
    });

    $('.book').click(function(){
        var service_id = $(this).closest("div.imgBoxx").find("input[name='service_id']").val();
        var service_provider_id = $(this).closest("div.imgBoxx").find("input[name='service_provider_id']").val();
        var service_at = $(this).closest("div.imgBoxx").find("input[name='service_at']").val();

        if(service_at == 'home')
        {
            $('#req_saloon').hide();
            $('#req_home').show();
        }
        if(service_at == 'salonee')
        {
            $('#req_home').hide();
            $('#req_saloon').show();
        }
        if(service_at == 'both')
        {
            $('#req_saloon').show();
            $('#req_home').show();
        }

        $('#service_id').val(service_id)
        $('#service_provider_id').val(service_provider_id)
        
    });

    $("#sDate").change(function () { 

        var service_id = $("#service_id").val();
        var service_provider_id = $("#service_provider_id").val();
        var type = $("#type").val();
        var date = $(this).val();
        $('#is-coupon').show()
        $('#c_success').hide();
        $('#c_fail').hide();
        $('#coupon').val('');
        var formData = new FormData();

        formData.append('service_id', service_id);
        formData.append('service_provider_id', service_provider_id);
        formData.append('date', date);
        formData.append('type', type);

        $.ajax({
            type:'POST',
            url:'controllers/get_solts_by_service_id.php',
            data:formData,
            contentType: false,
            processData: false,
            success:function(res){
                $("#slotData").show();
                $('#slotsList').toggle('100');
                $("#slotData").html(res);                          
            }
        });

        var formData1 = new FormData();

        formData1.append('service_id', service_id);
        formData1.append('type', type);

        $.ajax({
            type:'POST',
            url:'controllers/get_service_price.php',
            data:formData1,
            contentType: false,
            processData: false,
            success:function(res){
                var res = JSON.parse(res);
                var slots = res.slots;
                var total_amount = slots[0].sale_price;
                if(slots.length > 0)
                {
                    $("#price").val('Estimated Price: '+total_amount+' AED');
                    $("#total_amount").val(total_amount);
                    $("#original_amount").val(total_amount);
                }
                else
                {
                    $("#price").val('Estimated Price: 00:00');
                }                              
            }
        });
    });

    $("#btn-pay").click(function(){

        var user_id = "<?php echo @$_SESSION['user_id'];?>";

        if(user_id == '')
        {
            $('#bookModalForm').modal('hide');
            $('#loginModal').modal('show');
        }
        else
        {
            var slot_time = $('input:radio[name=slot_time]:checked').val();
            var service_id = $("#service_id").val();
            var coupon_code = $("#coupon").val();
            var service_provider_id = $("#service_provider_id").val();
            var type = $("#type").val();
            var slot_date = $("#sDate").val();

            var url = 'payment/payment.php';
            var form = $('<form action="' + url + '" method="post">' +
              '<input type="text" name="user_id" value="' + user_id + '" />' +
              '<input type="text" name="slot_date" value="' + slot_date + '" />' +
              '<input type="text" name="slot_time" value="' + slot_time + '" />' +
              '<input type="text" name="service_id" value="' + service_id + '" />' +
              '<input type="text" name="coupon_code" value="' + coupon_code + '" />' +              
              '<input type="text" name="service_provider_id" value="' + service_provider_id + '" />' +
              '<input type="text" name="type" value="' + type + '" />' +
              '</form>');
            $('body').append(form);
            form.submit();
        }
    });
</script>
<script type="text/javascript" src="<?='//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js'?>"></script>
<link href="<?='//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css'?>" rel="stylesheet">
