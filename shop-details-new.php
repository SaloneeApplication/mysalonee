<?php include('header.php');

$shop_details = $funcObject->shopDetails($con, $_GET['id']);

$shop_details = mysqli_fetch_assoc($shop_details);

$services = $funcObject->servicesByCategory($con, $category_id="", $_GET['id']);

?>
<div class="container-fluid">
    <div class="_header"></div>
    <nav aria-label="breadcrumb" class="_custmBrdcrmb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="index.php#feature-box">Shops</a></li>
            <li class="breadcrumb-item active" aria-current="page">Shop Details</li>
        </ol>
    </nav>

    <div class="nearShops">
       <div class="mL80" >
        <div class="row">
            <div class="col-md-5">
                <div class="imgOutBox">
                    <img src="<?php echo ADMIN_URL.$shop_details['image'];?>" alt=""  />
                </div> 
            </div>
             <div class="col-md-5 _slon">
                <h2 class="_font"><?php echo $shop_details['business_name'];?></h2>
                <div class="threeFive f-2x">
                    <!-- <span class="number-rating">3.5</span> -->
                    <div class="stars-outer">
                        <div class="stars-inner" style="width: 70%;"></div>
                    </div>
                </div>
                <p><i><img src="assets/img/icons/clock.png" alt="clock" /></i><span> Opens at 10:00 AM</span>
                <p><i><img src="assets/img/icons/loc.png" alt="loc" /></i><span> <?php echo $shop_details['address'];?></span></p>
                           
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="ourSrvice">
                <h2 class="_font">Our Services</h2>
                    <ul>
                        <?php
                        foreach($services as $row)
                        {?>
                            <li>
                                <div class="imgBoxx">
                                    <img src="<?php echo ADMIN_URL.$row['image'];?>" alt=""/>
                                    <a href="javascript:;"><?php echo $row['name'];?></a>
                                </div>
                            </li>
                        <?php
                        }?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="ourProdcts">
                    <h2 class="_font">Our Products</h2>
                    <ul>
                    <li>
                        <div class="card" style="width: 18rem;">
                            <div class="prod_IMGBlk">
                                <img class="card-img-top" src="assets/img/gallery/strawberryShower.png" alt="strawberryShower">
                            </div>
                        <div class="card-body">
                            <h5 class="card-title">The Body Shop</h5>
                            <p class="card-text">Unisex Strawberry Shower</p>
                            <p class="card-text">Get 250 ml</p>
                            <h6>AED 25.00</h6>
                            <a href="#" class="btn theme-btn" data-toggle="modal" data-target="#cartModal">Add To Cart</a>
                        </div>
                        </div>  
                    </li>
                    <li>
                        <div class="card" style="width: 18rem;">
                            <div class="prod_IMGBlk">
                                <img class="card-img-top" src="assets/img/gallery/Treehut.png" alt="Treehut">
                            </div>
                        <div class="card-body">
                            <h5 class="card-title">Tree Hut</h5>
                            <p class="card-text">Shea Sugar Srub - Moroccan</p>
                            <p class="card-text">Rose 510 g</p>
                            <h6>AED 40.00</h6>
                            <a href="#" class="btn theme-btn" data-toggle="modal" data-target="#cartModal">Add To Cart</a>
                        </div>
                        </div>  
                    </li>
                    <li>
                        <div class="card" style="width: 18rem;">
                            <div class="prod_IMGBlk">
                                <img class="card-img-top" src="assets/img/gallery/riverSideNectar.png" alt="riverSideNectar">
                            </div>
                        <div class="card-body">
                            <h5 class="card-title">Riverside Nectar</h5>
                            <p class="card-text">Lemon and Peppermint Body</p>
                            <p class="card-text">Wash 250 ml</p>
                            <h6>AED 36.00</h6>
                            <a href="#" class="btn theme-btn" data-toggle="modal" data-target="#cartModal">Add To Cart</a>
                        </div>
                        </div>  
                    </li>
                    <li>
                        <div class="card" style="width: 18rem;">
                            <div class="prod_IMGBlk">
                                <img class="card-img-top" src="assets/img/gallery/bathNbody.png" alt="bathNbody">
                            </div>
                        <div class="card-body">
                            <h5 class="card-title">Bath &amp; Body Works</h5>
                            <p class="card-text">Shea Sugar Srub - Moroccan</p>
                            <p class="card-text">Rose 510 g</p>
                            <h6>AED 25.00</h6>
                            <a href="#" class="btn theme-btn" data-toggle="modal" data-target="#cartModal">Add To Cart</a>
                        </div>
                        </div>  
                    </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>
     <!-- cartBox -->
     <div class="card _cartModal1" id="cartBox"  > 
          <div class="_list">
              <div>
                  <h5><span id="dismissBox"><i class="fa fa-angle-down" aria-hidden="true"></i></span> 
                    <input type="text" id="itemCOunt" value="0"> - Items</h5>
              </div>
              <div>
                <h3>AED 38.00</h3> <button type="button" class="btn"  >Proceed</button>
              </div>
          </div> 
    </div>
</div>
<?php include('footer.php');?>
<!-- <div class="modal fade _cartModal" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
       
      <div class="modal-body">
          <div class="_list">
              <div>
                  <h5><span data-dismiss="modal" aria-label="Close"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Your Order</h5>
              </div>
              <div>
                <h3>AED 38.00</h3> <a href="cart.php" class="btn">Proceed</a>
              </div>
          </div>
        
      </div> 
    </div>
  </div>
</div> -->

<div class="modal fade _cartModal" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
      <div class="modal-body">
          <!-- <div class="_list">
              <div>
                  <h5><span data-dismiss="modal" aria-label="Close"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Your Order</h5>
              </div>
              <div>
                <h3>AED 38.00</h3> <button type="button" class="btn" data-dismiss="modal">Proceed</button>

              </div>
          </div> -->
          <h4 class="_font dtePckrHdng">Form Filling for Booking</h4>
          <div id="picker"></div>
        <div class="bookSlotBlock">
            <h4>Preferences</h4>
            <div>
            <label for="male">Male</label>
            <input type="radio" name="preference" id="male">
            <span>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;</span> 
            <label for="female">Female</label>
            <input type="radio" name="preference" id="female">
            </div>
            <p>(Along with temperature check)</p>
            <button class="btn theme-btn"  onclick="incrementValue1()" data-dismiss="modal" aria-label="Close">Add To Cart</button>
        </div>
        
      </div> 
    </div>
  </div>
</div>

<style>
    ._cartModal1 {
        max-width: 1000px;
        margin: 0 auto; 
        display: none;
        z-index: 999;
    }
    ._cartModal1 ._list{ 
        background: #f1f1f1;
        padding: 2em;
        box-shadow: 0px -2px 3px #ddd;
    }
    ._list h5{
            display: flex
        }
    /* .card._cartModal1::before {
        position: fixed;
        top: 0;
        left: 0;
        content: '';
        width: 100%;
        height: 100%;
        background: #00000075;
        z-index: -1;
    }  */
    input#itemCOunt {
    border: 0;
    width: auto;
    max-width: 50px;
    text-align: center;
    font-size: 20px;
    font-weight: 600;
    outline: none;
    box-shadow: none;
    cursor: default;
    background: #f1f1f1;

}
</style>

<script type="text/javascript">

function incrementValue1(){ 
        var value = parseInt(document.getElementById('itemCOunt').value, 10); 
        value = isNaN(value) ? 0 : value;
        if(value<10){
            value++;
                document.getElementById('itemCOunt').value = value;  
                $('#cartBox').show("slow"); //show cartBox on #itemCOunt value >1
        } 
    }
    $(document).ready(function(){  
        // dismiss cartBox
        $("#dismissBox").click(function () {
            $('#cartBox').hide("slow")
        });
    }); 


        (function($) {
          $('#picker').markyourcalendar({
            availability: [
              ['1:00', '2:00', '3:00', '4:00', '5:00'],
              ['2:00'],
              ['3:00'],
              ['4:00'],
              ['5:00'],
              ['6:00'],
              ['7:00']
            ],
            isMultiple: true,
            onClick: function(ev, data) {
              // data is a list of datetimes
              console.log(data);
              var html = ``;
              $.each(data, function() {
                var d = this.split(' ')[0];
                var t = this.split(' ')[1];
                html += `<p>` + d + ` ` + t + `</p>`;
              });
              $('#selected-dates').html(html);
            },
            onClickNavigator: function(ev, instance) {
              var arr = [
                [
                  ['4:00', '5:00', '6:00', '7:00', '8:00'],
                  ['1:00', '5:00'],
                  ['2:00', '5:00'],
                  ['3:30'],
                  ['2:00', '5:00'],
                  ['2:00', '5:00'],
                  ['2:00', '5:00']
                ],
                [
                  ['2:00', '5:00'],
                  ['4:00', '5:00', '6:00', '7:00', '8:00'],
                  ['4:00', '5:00'],
                  ['2:00', '5:00'],
                  ['2:00', '5:00'],
                  ['2:00', '5:00'],
                  ['2:00', '5:00']
                ],
                [
                  ['4:00', '5:00'],
                  ['4:00', '5:00'],
                  ['4:00', '5:00', '6:00', '7:00', '8:00'],
                  ['3:00', '6:00'],
                  ['3:00', '6:00'],
                  ['3:00', '6:00'],
                  ['3:00', '6:00']
                ],
                [
                  ['4:00', '5:00'],
                  ['4:00', '5:00'],
                  ['4:00', '5:00'],
                  ['4:00', '5:00', '6:00', '7:00', '8:00'],
                  ['4:00', '5:00'],
                  ['4:00', '5:00'],
                  ['4:00', '5:00']
                ],
                [
                  ['4:00', '6:00'],
                  ['4:00', '6:00'],
                  ['4:00', '6:00'],
                  ['4:00', '6:00'],
                  ['4:00', '5:00', '6:00', '7:00', '8:00'],
                  ['4:00', '6:00'],
                  ['4:00', '6:00']
                ],
                [
                  ['3:00', '6:00'],
                  ['3:00', '6:00'],
                  ['3:00', '6:00'],
                  ['3:00', '6:00'],
                  ['3:00', '6:00'],
                  ['4:00', '5:00', '6:00', '7:00', '8:00'],
                  ['3:00', '6:00']
                ],
                [
                  ['3:00', '4:00'],
                  ['3:00', '4:00'],
                  ['3:00', '4:00'],
                  ['3:00', '4:00'],
                  ['3:00', '4:00'],
                  ['3:00', '4:00'],
                  ['4:00', '5:00', '6:00', '7:00', '8:00']
                ]
              ]
              var rn = Math.floor(Math.random() * 10) % 7;
              instance.setAvailability(arr[rn]);
            }
          });
        })(jQuery);
    </script>