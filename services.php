<?php 
include('header.php');

$category_id = @$_GET['category_id'];
$sp_id = @$_GET['sp_id'];
$services = $funcObject->servicesByCategory($con, @$category_id, @$sp_id);
$categories = $funcObject->categoriesList($con);
$promoCodes = $funcObject->getAllUserPromocodes($con, 'all');
?>

<style type="text/css">
.modal {
  overflow-y:auto;
}
.img_container{
    display: inline-block;
    font-size: 40px;
    line-height: 50px;
    color:#c96c92;
    width: 50px;
    height: 50px;
    text-align: center;
    vertical-align: bottom;
    left: 50px;
    margin-left: 45%;
    margin-top: 10%;
}

.help { display: inline-block; position: relative; }

.help-button {
  color: #222;
  font-weight: bold;
  text-decoration: none;
}

.info-box {
    border-radius: 15px;
    border: 2px solid #bdbdbd;
    box-shadow: 8px 10px 10px 0px #888888;
    background-color: #ddd;
    display: none;
    color: #d55c91;
    /*background-color: #ddd;*/
    /*display: none;*/
    /*color: #888;*/
    font-family: sans-serif;
    font-size: smaller;
    padding: 10px;
    width: 170px;
    position: absolute;
    right: -190px;
    bottom: 0;
}

.info-box::after {
    border-top: 20px solid transparent;
    border-right: 20px solid #ddd;
    content: " ";
    display: block;
    position: absolute;
    left: -20px;
    bottom: 0;
    width: 0;
    height: 0;
}

.info-box .close-button {
    border: 1px dotted #222;
    color: #222;
    float: right;
    line-height: 0.6em;
    padding: 0;
    text-decoration: none;
}

.info-box .close-button:hover {
    border-color: #aaa;
    color: #aaa;
}
</style>

<link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<div class="container-fluid">
    <div class="_header"></div>
    <!-- breadcrumb  -->
    <nav aria-label="breadcrumb" class="_custmBrdcrmb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url;?>index.php">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Hair Setting</li>
        </ol>
    </nav>

    <div class="d-flex myFlex">
        <div class="_aside">
            <h5 class="subHdng">Categories</h5>
            <div class="mt-4 custom-checkbox">
            <?php
            while($row = mysqli_fetch_array($categories))
            {?>
                <div class="form-check mt-2 ">
                    <?php $checked = (@$_GET['category_id'] == $row['category_id'] ? 'checked' : ''); ?>
                    <input type="checkbox" name="category_id" class="form-check-input category_id" <?= $checked;?> value="<?= $row['category_id']?>" id="customCheck<?= $row['category_id']?>">
                    <label class="form-check-label" for="customCheck<?= $row['category_id']?>"> <?= $row['name'].' ('.ucfirst($row['category_for']).')';?></label>
                </div>
            <?php
            }?>
            </div>
        </div>

        <div class="mainDiv cat-data">
            <?php
            while($row = mysqli_fetch_array($services))
            {?>
                <div class="card servcs_card" style="width: 18rem;">
                    <div class="imgOuter">
                        <img class="card-img-top revealOnScroll" data-animation="flipInY" data-timeout="400" src="<?php echo ADMIN_URL.$row['image'];?>" alt="Service Image">
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="service_at" value="<?php echo $row['service_at'];?>">
                        <input type="hidden" name="service_id" value="<?php echo $row['service_id'];?>">
                        <input type="hidden" name="service_provider_id" value="<?php echo $row['service_provider_id'];?>">
                        <h5 class="card-title lineOneLimit" title="<?php echo $row['name'];?>" ><?php echo $row['name'];?></h5>
                        
                        <p class="card-text lineThreeLimit"><?php echo $row['description'];?></p>
                        <h6 class="lineOneLimit" title="<?php echo $row['service_provider_name'];?>"><?php echo $row['service_provider_name'];?></h6>
                        <div class="threeFive">
                                <div class="stars-outer">
                                    <!--<div class="stars-inner" style="width: 70%;"></div>-->
                                </div>
                                <!--<span class="number-rating">3.5</span>-->
                              </div>
                              <p class="mb-1 "><i><img src="assets/img/icons/clock.png" alt="clock" /></i><span> Opens at 10:00 AM</span>
                            </p>
                            <p class="pb-4 d-flex"><i><img src="assets/img/icons/loc.png" alt="loc" /></i>&nbsp;
                            <span class="lineTwoLimit"> L.G. Heights, 1st Floor, Above Hitech City Rd, near Happi mobiles, Madhapur, Telangana 500085</span></p>
                        <!-- <a href="#" data-toggle="modal" data-target="#bookModal" class="_btn book">Book</a> -->
                        <a href="shop-details.php?id=<?php echo $row['service_provider_id'];?>" class="_btn book">View</a>
                    </div>
                </div>
            <?php
            }?>
        </div>
    </div>
</div>
<?php include('footer.php');?>
<!-- book modal  -->
<div class="modal fade themeModal bookModalBlock" id="bookModal" tabindex="-1" role="dialog" aria-labelledby="bookModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="col-12 modal-title text-center" id="bookModalTitle">Service</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body book-modal">
                <div class="card mt-2" id="req_saloon">
                    <div class="d-flex">
                        <div class="imgOuter1">
                            <img class="card-img-top rounded" src="assets/img/category/hair-setting.jpg" />
                        </div>
                        <div class="m-2 col_ryt">
                            <h5 >Service Provider Location</h5>
                            <p class="card-text">Some quick example text to build on the card title 
                                and make up the bulk of the card's content.</p>
                            <a href="#" id="bookformModal" data-toggle="modal" data-target="#bookModalForm" class="btnViewMore float-right" data-id="">BOOK AN APPOINTMENT</a>
                        </div>
                    </div>
                </div>
                <div class="card mt-2" id="req_home">
                    <div class="d-flex">
                        <div class="imgOuter1">
                            <img class="card-img-top rounded" src="assets/img/category/hair-setting.jpg" />
                        </div>
                        <div class="m-2 col_ryt">
                            <h5 >Service Provider Location </h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" id="bookformModal1" data-id="" data-toggle="modal" data-target="#bookModalForm" class="btnViewMore float-right">REQUEST TO HOME</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- sample modal  -->
<div class="modal fade themeModal" id="bookModalForm" tabindex="-1" role="dialog" aria-labelledby="modalFormTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="col-12 modal-title text-center" id="modalFormTitle">Enter below details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modalForm">
                    <div class="login">
                        <form method="POST" id="booking-form">
                            <input type="hidden" id="service_id">
                            <input type="hidden" id="service_provider_id">
                            <input type="hidden" id="type">
                            <div class="form-group mb-2">
                                <input type="text" id="sDate" class="form-control" autocomplete="off" required>
                                <label class="form-control-placeholder p-0" for="sDate">Date</label>
                            </div>
                            <div class="form-group mb-2">
                                <div class="boxedList">
                                    <div class="flxRow">
                                    <p id="slotLabel">Select Available Slot</p> 
                                    <i class="fa fa-caret-down" aria-hidden="true" id="showSlots"></i> 
                                </div>
                                    <div id="slotsList" style="display: none;">
                                    <ul id="slotData"></ul>
                                </div>
                                </div>
                            </div>
                            <!-- <div class="form-group mb-2">
                                <select name="service" class="form-control" autocomplete="off" required>
                                    <option hidden disabled selected value>
                                        Add List of Services </option>
                                    <option value="service1">service-1</option>
                                    <option value="service2">service-2</option>
                                    <option value="service3">service-3</option>
                                </select>
                            </div> -->
                            <div class="form-group mb-2" id="is-coupon">
                                <label>Coupon<div class="help">
                                    <div class="info-box">
                                        <a href="#" class="close-button">&times;</a>
                                        <h6>Promo Codes</h6>
                                        <?php
                                        while($rowCoupon = mysqli_fetch_array($promoCodes))
                                        {?>
                                            <div><?php echo $rowCoupon['coupon_code']; ?> -- <?php echo $rowCoupon['discount_per']."% OFF"; ?> 
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <a class='help-button' href='#' title="Click to know more">[?]</a>
                                </div></label>
                                <div class="row" style="padding: 0px 15px 0px 15px;">
                                    <input type="text" id="coupon" name="coupon" value="" class="form-control col-md-8" autocomplete="off" required/>
                                    <button type="button" id="apply-coupon" class="btn btn-sm theme-btn col-md-4">Apply</button>
                                    <button type="button" id="cancel-coupon" class="btn btn-sm theme-btn col-md-4">clear</button>
                                    <p id="c_success" style="color: green;">Coupon Applied</p>
                                    <p id="c_fail" style="color: red;">Coupon not available</p>
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <label>Check the Payment</label> 
                                <input type="text" id="price" name="price" value="Estimated Price: 00.00" class="form-control" autocomplete="off" required readonly/>
                                <input type="hidden" id="total_amount">
                            </div>
                            <div class="form-group mb-4">
                                <label>Preferences</label>
                                <div class="form-group mb-2">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Male <input type="radio" class="form-check-input" name="optradio">
                                    </label>
                                  </div>
                                  <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Female <input type="radio" class="form-check-input" name="optradio"> 
                                    </label>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <button type="button" id="btn-pay" class="btn theme-btn">Proceed To Pay</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

jQuery(document).ready(function($){
  $('.help-button').on('click', function(e){
    e.preventDefault();
    $(this).siblings('.info-box').show();
  });
  
  $('.close-button').on('click', function(e){
    e.preventDefault();
    $(this).parents('.info-box').hide();
  });
  $('#c_success').hide();
  $('#c_fail').hide();
  $('#is-coupon').hide();
  $('#cancel-coupon').hide();
  $('#apply-coupon').on('click', function(e){
    if(($('#coupon').val() == '') || ($('#coupon').val() == undefined)){
      $('#c_fail').text('Enter Coupon Code');
      $('#c_fail').show();        
    }else{
        var formData = new FormData();        
        formData.append('coupon', $('#coupon').val());
        formData.append('date', $("#sDate").val());
        $.ajax({
            type:'POST',
            url:'controllers/check_coupon.php',
            data:formData,
            contentType: false,
            processData: false,
            success:function(res){
                var res = JSON.parse(res);
                if(res.status == 200)
                {
                  $totalDiscount = $("#original_amount").val() - ((res.data / 100) * $("#original_amount").val());
                  $('#total_amount').val($totalDiscount)
                  $("#price").val('Estimated Price: '+$totalDiscount+' AED');
                  $('#c_success').text(res.message);
                  $('#c_success').show();
                  $('#c_fail').hide();
                } else {
                  $("#price").val('Estimated Price: '+$("#original_amount").val()+' AED');
                  $('#total_amount').val($("#original_amount").val())
                  $('#c_fail').text(res.message);
                  $('#c_fail').show();
                  $('#c_success').hide();
                }                              
            }
        });
    }
  });  
  $('#cancel-coupon').on('click', function(e){
    cancelCoupon();
  });  
});
function cancelCoupon() {
    $('#coupon').val('');
    $('#apply-coupon').show()
    $('#c_success').hide();
    $('#c_fail').hide();
    $('#cancel-coupon').hide()
}
    $(".category_id").change(function() {
        //if(this.checked) 
        //{
            var sp_id = "<?php echo $sp_id;?>";

            var category_id = [];
            $("input:checkbox[name=category_id]:checked").each(function() {
                category_id.push($(this).val());
            });

            var formData = new FormData();

            formData.append('category_id', category_id);
            formData.append('sp_id', sp_id);

            $.ajax({
                type:'POST',
                url:'controllers/get_services.php',
                data:formData,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    $(".mainDiv").html('<div class="img_container"><i class="fas fa-sync fa-spin"></i></div>');
                },
                success:function(res){

                    $(".cat-data").html(res);                          
                }
            });
        //}
    });

    $('#sDate').datepicker({
        format: 'yyyy-mm-dd',
        todayHighlight:true,
        autoclose:true,
        startDate:"<?php echo date("Y-m-d");?>",
        endDate :"<?php echo date("Y-m-d",strtotime("+20 days"));?>"

    });

    $('#bookformModal').click(function () {
        $('#bookModal').modal('hide');
        $('#type').val('salonee')
    })

    $('#bookformModal1').click(function () {
        $('#bookModal').modal('hide');
        $('#type').val('home')
    })

    $('#showSlots').click(function() {
        $('#slotsList').toggle('100');
        $(this).toggleClass("fa-caret-up fa-caret-down");
    });

    $('.book').click(function(){
        var service_id = $(this).closest("div.card").find("input[name='service_id']").val();
        var service_provider_id = $(this).closest("div.card").find("input[name='service_provider_id']").val();
        var service_at = $(this).closest("div.card").find("input[name='service_at']").val();

        if(service_at == 'home')
        {
            $('#req_saloon').hide();
            $('#req_home').show();
        }
        if(service_at == 'salonee')
        {
            $('#req_home').hide();
            $('#req_saloon').show();
        }
        if(service_at == 'both')
        {
            $('#req_saloon').show();
            $('#req_home').show();
        }

        $('#service_id').val(service_id)
        $('#service_provider_id').val(service_provider_id)
        
    });

    $("#sDate").change(function () { 

        var service_id = $("#service_id").val();
        var service_provider_id = $("#service_provider_id").val();
        var type = $("#type").val();
        var date = $(this).val();
        $('#is-coupon').show()
        $('#c_success').hide();
        $('#c_fail').hide();
        $('#coupon').val('');
        var formData = new FormData();

        formData.append('service_id', service_id);
        formData.append('service_provider_id', service_provider_id);
        formData.append('date', date);
        formData.append('type', type);

        $.ajax({
            type:'POST',
            url:'controllers/get_solts_by_service_id.php',
            data:formData,
            contentType: false,
            processData: false,
            success:function(res){
                $("#slotData").show();
                $('#slotsList').toggle('100');
                $("#slotData").html(res);                          
            }
        });

        var formData1 = new FormData();

        formData1.append('service_id', service_id);
        formData1.append('type', type);

        $.ajax({
            type:'POST',
            url:'controllers/get_service_price.php',
            data:formData1,
            contentType: false,
            processData: false,
            success:function(res){
                var res = JSON.parse(res);
                var slots = res.slots;
                var total_amount = slots[0].sale_price;
                if(slots.length > 0)
                {
                    $("#price").val('Estimated Price: '+total_amount+' AED');
                    $("#total_amount").val(total_amount);
                    $("#original_amount").val(total_amount);
                }
                else
                {
                    $("#price").val('Estimated Price: 00:00');
                }                              
            }
        });
    });

    $("#btn-pay").click(function(){

        var user_id = "<?php echo @$_SESSION['user_id'];?>";

        if(user_id == '')
        {
            $('#bookModalForm').modal('hide');
            $('#loginModal').modal('show');
        }
        else
        {
            var slot_time = $('input:radio[name=slot_time]:checked').val();
            var service_id = $("#service_id").val();
            var coupon_code = $("#coupon").val();
            var service_provider_id = $("#service_provider_id").val();
            var type = $("#type").val();
            var slot_date = $("#sDate").val();

            var url = 'payment/payment.php';
            var form = $('<form action="' + url + '" method="post">' +
              '<input type="text" name="user_id" value="' + user_id + '" />' +
              '<input type="text" name="slot_date" value="' + slot_date + '" />' +
              '<input type="text" name="slot_time" value="' + slot_time + '" />' +
              '<input type="text" name="service_id" value="' + service_id + '" />' +
              '<input type="text" name="coupon_code" value="' + coupon_code + '" />' +              
              '<input type="text" name="service_provider_id" value="' + service_provider_id + '" />' +
              '<input type="text" name="type" value="' + type + '" />' +
              '</form>');
            $('body').append(form);
            form.submit();
        }
    });
</script>
