<?php include('header.php');

$user_id = @$_SESSION['user_id'];

if($user_id == "")
{
    echo '<script> var base_url = "http://localhost/salonee_web/"; </script>';
    echo '<script> window.location.replace(base_url); </script>';
}

$promoCodes = $funcObject->getAllUserPromocodes($con, 'all');

$cart_services = $funcObject->cart_services($con, $user_id);
$cart_products = $funcObject->cart_products($con, $user_id);

?>

<style type="text/css">
.modal {
  overflow-y:auto;
}
.img_container{
    display: inline-block;
    font-size: 40px;
    line-height: 50px;
    color:#c96c92;
    width: 50px;
    height: 50px;
    text-align: center;
    vertical-align: bottom;
    left: 50px;
    margin-left: 45%;
    margin-top: 10%;
}

.help { display: inline-block; position: relative; }

.help-button {
  color: #222;
  font-weight: bold;
  text-decoration: none;
}

.info-box {
    border-radius: 15px;
    border: 2px solid #bdbdbd;
    box-shadow: 8px 10px 10px 0px #888888;
    background-color: #ddd;
    display: none;
    color: #d55c91;
    /*background-color: #ddd;*/
    /*display: none;*/
    /*color: #888;*/
    font-family: sans-serif;
    font-size: smaller;
    padding: 10px;
    width: 170px;
    position: absolute;
    right: -190px;
    bottom: 0;
}

.info-box::after {
    border-top: 20px solid transparent;
    border-right: 20px solid #ddd;
    content: " ";
    display: block;
    position: absolute;
    left: -20px;
    bottom: 0;
    width: 0;
    height: 0;
}

.info-box .close-button {
    border: 1px dotted #222;
    color: #222;
    float: right;
    line-height: 0.6em;
    padding: 0;
    text-decoration: none;
}

.info-box .close-button:hover {
    border-color: #aaa;
    color: #aaa;
}
</style>

<link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<div class="container-fluid">
    <div class="_header"></div>
    <nav aria-label="breadcrumb" class="_custmBrdcrmb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="saloon-shops.php">Shops</a></li>
            <li class="breadcrumb-item active" aria-current="page">Cart</li>
        </ol>
    </nav>
    <div class="container-fluid pad15">
        <div class="row">
            <?php
            if($cart_services->num_rows > 0 && $cart_products->num_rows > 0)
            {?>
                <div class="col-md-4">
                    <div class="login" style="padding-top: 15px;">
                        <p><strong style="margin-left: 18px;">Fill The Details</strong></p>
                        <form method="POST" id="booking-form">
                            <input type="hidden" id="service_id" >
                            <input type="hidden" id="service_provider_id">
                            <div class="form-group mb-4">
                                <label>Service At</label>
                                <div class="form-group mb-2">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Salon <input type="radio" checked class="form-check-input" id="type" name="type" value="salonee">
                                    </label>
                                  </div>
                                  <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Home <input type="radio" class="form-check-input" id="type" name="type" value="home"> 
                                    </label>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <input type="text" id="sDate" class="form-control" autocomplete="off" required>
                                <label class="form-control-placeholder p-0" for="sDate">Date</label>
                            </div>
                            <div class="form-group mb-2">
                                <div class="boxedList">
                                    <div class="flxRow">
                                    <p id="slotLabel">Select Available Slot</p> 
                                    <i class="fa fa-caret-down" aria-hidden="true" id="showSlots"></i> 
                                </div>
                                    <div id="slotsList" style="display: none;">
                                    <ul id="slotData"></ul>
                                </div>
                                </div>
                            </div>
                            <!-- <div class="form-group mb-2">
                                <select name="service" class="form-control" autocomplete="off" required>
                                    <option hidden disabled selected value>
                                        Add List of Services </option>
                                    <option value="service1">service-1</option>
                                    <option value="service2">service-2</option>
                                    <option value="service3">service-3</option>
                                </select>
                            </div> -->
                            <div class="form-group mb-2" id="is-coupon">
                                <label>Coupon<div class="help">
                                    <div class="info-box">
                                        <a href="#" class="close-button">&times;</a>
                                        <h6>Promo Codes</h6>
                                        <?php
                                        while($rowCoupon = mysqli_fetch_array($promoCodes))
                                        {?>
                                            <div><?php echo $rowCoupon['coupon_code']; ?> -- <?php echo $rowCoupon['discount_per']."% OFF"; ?> 
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <a class='help-button' href='#' title="Click to know more">[?]</a>
                                </div></label>
                                <div class="row" style="padding: 0px 15px 0px 15px;">
                                    <input type="text" id="coupon" name="coupon" value="" class="form-control col-md-8" autocomplete="off" required/>
                                    <button type="button" id="apply-coupon" class="btn btn-sm theme-btn col-md-4">Apply</button>
                                    <button type="button" id="cancel-coupon" class="btn btn-sm theme-btn col-md-4">clear</button>
                                    <p id="c_success" style="color: green;">Coupon Applied</p>
                                    <p id="c_fail" style="color: red;">Coupon not available</p>
                                </div>
                            </div>
                            <div class="form-group mb-4">
                                <label>Preferences</label>
                                <div class="form-group mb-2">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Male <input type="radio" class="form-check-input" name="optradio">
                                    </label>
                                  </div>
                                  <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Female <input type="radio" class="form-check-input" name="optradio"> 
                                    </label>
                                  </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <?php
                    if($cart_services->num_rows > 0)
                    {?>
                        <div class="cartPage">
                            <ul>
                                <li class="colmn1">Service Details</li>
                                <li class="colmn2">Base Price</li>
                                <li class="colmn2">Sale Price</li>
                                <li class="colmn4">Total</li>
                            </ul>
                            <?php 
                            foreach($cart_services as $row)
                            {?>
                                <div class="card cart_services">
                                    <input type="hidden" name="service_id[]" value="<?php echo $row['service_id'];?>">
                                    <input type="hidden" name="service_provider_id" value="<?php echo $row['service_provider_id'];?>">
                                    <div class="colmn1">
                                        <div class="d-flex">
                                            <div class="_thumb">
                                                <img src="<?php echo ADMIN_URL.$row['image'];?>" alt="cart_item">
                                            </div>
                                            <div style="margin-top: 33px;">
                                                <h5><?php echo $row['service_name'];?></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        $service_prices = $funcObject->getServicePrices($con, $type = 'salonee', $row['service_id']);
                                        while($row1 = mysqli_fetch_array($service_prices))
                                        {
                                            $base_price = $row1['base_price'];
                                            $sale_price = $row1['sale_price'];
                                        }
                                    ?>
                                    <div class="colmn2">
                                        <h4>AED <?php echo $base_price;?></h4>
                                    </div>
                                    <div class="colmn2">
                                        <h4>AED <?php echo $sale_price;?></h4>
                                    </div>
                                    <div class="colmn4">
                                        <input type="hidden" id="service_price" class="service_price price" value="<?php echo $sale_price;?>">
                                        <h4 class="service_price1">AED <?php echo $sale_price;?> </h4> 
                                    </div>
                                </div>
                            <?php
                            }?>
        
                            <div class="card _Total">
                                <div class="my_flcx">
                                    <div class="_lblsCrt">
                                        <p><b>1 item (s)</b></p> 
                                    </div>
                                    <div class="_pricesCrt">
                                        <p id="service_total"></p>
                                    </div>
                                </div>
                                <div class="my_flcx">
                                    <div class="_lblsCrt">
                                        <p>Sub Total</p> 
                                    </div>
                                    <div class="_pricesCrt">
                                        <p id="service_sub_total"></p>
                                    </div>
                                </div>
                                <div class="my_flcx _brdrs">
                                    <div class="_lblsCrt">
                                        <h2>Grand Total</h2>
                                    </div>
                                    <div class="_pricesCrt">
                                        <h2 id="service_grand_total"></h2>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    <?php
                    }?>
                    <?php
                    if($cart_products->num_rows > 0)
                    {?>
                        <div class="cartPage">
                            <ul>
                                <li class="colmn1">Product Details</li>
                                <li class="colmn2">Price</li>
                                <li class="colmn3">Quantity</li>
                                <li class="colmn4">Total</li>
                            </ul>
                            <?php
                            foreach($cart_products as $row)
                            {?>
                                <div class="card cart_products">
                                    <div class="colmn1">
                                        <div class="d-flex">
                                            <div class="_thumb">
                                                <img src="<?php echo user_base_url.$row['product_image']; ?>" alt="cart_item">
                                            </div>
                                            <div>
                                                <h5><?php echo $row['product_title'];?></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="colmn2">
                                        <h4>AED <?php echo $row['sale_price'];?></h4>
                                    </div>
                                    <div class="colmn3"> 
                                        <div class="_countActions">
                                        <input type="button" value="-" class="count_action1 inc1"/>
                                        <input type="text" name="quantity" class="quantity" value="1" maxlength="2" max="10" size="1" id="number" />
                                        <input type="button" value="+" class="count_action1 dec1"/>
                                        </div> 
                                    </div>
                                    <div class="colmn4">
                                        <input type="hidden" name="product_price" class="product_price price" value="<?php echo $row['sale_price'];?>">
                                        <h4 class="product_price1">AED <?php echo $row['sale_price'];?> </h4> 
                                    </div>
                                </div>
                            <?php
                            }?>
        
                            <div class="card _Total">
                                <div class="my_flcx">
                                    <div class="_lblsCrt">
                                        <p><b>1 item</b></p> 
                                    </div>
                                    <div class="_pricesCrt">
                                        <p id="product_total"></p>
                                    </div>
                                </div>
                                <div class="my_flcx">
                                    <div class="_lblsCrt">
                                        <p>Sub Total</p> 
                                    </div>
                                    <div class="_pricesCrt">
                                        <p id="product_sub_total"></p>
                                    </div>
                                </div>
                                <div class="my_flcx">
                                    <div class="_lblsCrt">
                                        <p>Delivery Fee</p> 
                                    </div>
                                    <div class="_pricesCrt">
                                        <p>AED 0.00</p>
                                    </div>
                                </div>
                                <div class="my_flcx _brdrs">
                                    <div class="_lblsCrt">
                                        <h2>Grand Total</h2>
                                    </div>
                                    <div class="_pricesCrt">
                                        <h2 id="product_grand_total"></h2>
                                    </div>
                                </div>
                                <div class="my_flcx _brdrs">
                                    <div class="_lblsCrt">
                                        <h2>Payable</h2>
                                    </div>
                                    <div class="_pricesCrt">
                                        <input type="hidden" id="total_amount">
                                        <h2 id="total_payable"></h2>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    <?php
                    }?>   
                    <div class="form-group mb-2">
                        <button type="button" onclick="pay();" id="btn-pay" class="btn theme-btn">Proceed To Pay</button>
                    </div>
                </div>
            <?php
            }
            else
            {?>
                <div class="jumbotron col-lg-6 col-md-6 col-sm-6 col-xs-6 offset-3 float-md-center text-center">
                  <h4>Your cart is empty.</h4>
                </div>
            <?php  
            }
            ?>
        </div>
    </div>
</div>
<?php include('footer.php');?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script>
var service_provider_id = $('.cart_services').find("input[name='service_provider_id']").val();

var sum = 0;
$('.service_price').each(function(){
    sum += parseFloat(this.value);

    $('#service_total').html('<b>AED '+sum+'</b>');
    $('#service_sub_total').html('AED '+sum);
    $('#service_grand_total').html('AED '+sum);
});

var sum1 = 0;
$('.product_price').each(function(){
    sum1 += parseFloat(this.value);

    $('#product_total').html('<b>AED '+sum1+'</b>');
    $('#product_sub_total').html('AED '+sum1);
    $('#product_grand_total').html('AED '+sum1);
});

var sum2 = 0;
$('.price').each(function(){
    sum2 += parseFloat(this.value);

    $('#total_payable').html('<b>AED '+sum2+'</b>');
});

const minus1 = $('.inc1');
const plus1 = $('.dec1');
minus1.click(function(e) {
    e.preventDefault();
    const input = $(this).closest("._countActions").find(".quantity");
    var value = input.val();
    if (value > 1) {
      value--;
    }
    input.val(value);
});

plus1.click(function(e) {
    e.preventDefault();
    const input = $(this).closest("._countActions").find(".quantity");
    var value = input.val();
    value++;
    input.val(value);
});

$('.count_action1').click(function(){

    var qty = $(this).closest('div._countActions').find("input[name=quantity]").val();
    var price = $(this).closest('div.cart_products').find("input[name=product_price]").val();

    var value = (parseInt(qty) * parseInt(price));

    $(this).closest('div.cart_products').find('.product_price1').html('AED '+value);

    $('#product_total').html('<b>AED '+value+'</b>');
    $('#product_sub_total').html('AED '+value);
    $('#product_grand_total').html('AED '+value);
});

jQuery(document).ready(function($){
    $('.help-button').on('click', function(e){
        e.preventDefault();
        $(this).siblings('.info-box').show();
        });

        $('.close-button').on('click', function(e){
            e.preventDefault();
            $(this).parents('.info-box').hide();
        });
        $('#c_success').hide();
        $('#c_fail').hide();
        $('#is-coupon').hide();
        $('#cancel-coupon').hide();
        $('#apply-coupon').on('click', function(e){
        if(($('#coupon').val() == '') || ($('#coupon').val() == undefined)){
          $('#c_fail').text('Enter Coupon Code');
          $('#c_fail').show();        
        }else{
            var formData = new FormData();        
            formData.append('coupon', $('#coupon').val());
            formData.append('date', $("#sDate").val());
            $.ajax({
                type:'POST',
                url:'controllers/check_coupon.php',
                data:formData,
                contentType: false,
                processData: false,
                success:function(res){
                    var res = JSON.parse(res);
                    if(res.status == 200)
                    {
                      $totalDiscount = $("#original_amount").val() - ((res.data / 100) * $("#original_amount").val());
                      $('#total_amount').val($totalDiscount)
                      $("#total_payable").val('AED '+$totalDiscount);
                      $('#c_success').text(res.message);
                      $('#c_success').show();
                      $('#c_fail').hide();
                    } else {
                      $("#price").val('Estimated Price: '+$("#original_amount").val()+' AED');
                      $('#total_amount').val($("#original_amount").val())
                      $('#c_fail').text(res.message);
                      $('#c_fail').show();
                      $('#c_success').hide();
                    }                              
                }
            });
        }
    });  
    $('#cancel-coupon').on('click', function(e){
        cancelCoupon();
    });  
});

function cancelCoupon() {
    $('#coupon').val('');
    $('#apply-coupon').show()
    $('#c_success').hide();
    $('#c_fail').hide();
    $('#cancel-coupon').hide()
}

$('#sDate').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight:true,
    autoclose:true,
    startDate:"<?php echo date("Y-m-d");?>",
    endDate :"<?php echo date("Y-m-d",strtotime("+20 days"));?>"

});

$('#bookformModal').click(function () {
    $('#bookModal').modal('hide');
    $('#type').val('salonee')
})

$('#bookformModal1').click(function () {
    $('#bookModal').modal('hide');
    $('#type').val('home')
})

$('#showSlots').click(function() {
    $('#slotsList').toggle('100');
    $(this).toggleClass("fa-caret-up fa-caret-down");
});

$("#sDate").change(function () { 

    var type = $("input[name=type]").val();
    var date = $(this).val();
    $('#is-coupon').show()
    $('#c_success').hide();
    $('#c_fail').hide();
    $('#coupon').val('');
    var formData = new FormData();

    formData.append('service_provider_id', service_provider_id);
    formData.append('date', date);
    formData.append('type', type);

    $.ajax({
        type:'POST',
        url:'controllers/get_slots_by_sp_id.php',
        data:formData,
        contentType: false,
        processData: false,
        success:function(res){
            $("#slotData").show();
            $('#slotsList').toggle('100');
            $("#slotData").html(res);                          
        }
    });
});

function pay()
{
    var user_id = "<?php echo @$_SESSION['user_id'];?>";

    if(user_id == '')
    {
        $('#bookModalForm').modal('hide');
        $('#loginModal').modal('show');
    }
    else
    {
        var slot_time = $('input:radio[name=slot_time]:checked').val();
        var service_id = $("input[name='service_id[]']")
              .map(function(){return $(this).val();}).get();
        var coupon_code = $("#coupon").val();
        var type = $("#type").val();
        var slot_date = $("#sDate").val();

        if(slot_date == "")
        {
            alert('Please select Date');
            return false;
        }
        if(slot_time == undefined)
        {
            alert('Please select Slot');
            return false;
        }

        var url = 'payment/payment1.php'; 
        var form = $('<form action="' + url + '" method="post">' +
          '<input type="text" name="user_id" value="' + user_id + '" />' +
          '<input type="text" name="slot_date" value="' + slot_date + '" />' +
          '<input type="text" name="slot_time" value="' + slot_time + '" />' +
          '<input type="text" name="service_id" value="' + service_id + '" />' +
          '<input type="text" name="coupon_code" value="' + coupon_code + '" />' +              
          '<input type="text" name="service_provider_id" value="' + service_provider_id + '" />' +
          '<input type="text" name="type" value="' + type + '" />' +
          '</form>');
        $('body').append(form);
        form.submit();
    }
}
</script>