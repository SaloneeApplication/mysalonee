<?php include('header.php');

$shop_list = $funcObject->shopList($con);

?>
<div class="container-fluid">
    <div class="_header"></div>
    <nav aria-label="breadcrumb" class="_custmBrdcrmb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Shops</a></li>
            <li class="breadcrumb-item"><a href="#">Bangs Salon</a></li> 
            <li class="breadcrumb-item active" aria-current="page">The Body Shop</li>
        </ol>
    </nav>

    <div class="nearShops _shopDetails _prodDetlPage">
       <div class="mL80 mR80" >
        <div class="row">
            <div class="col-md-4">
                <div class="imgOutBox ">
                    <img src="assets/img/gallery/strawberryShower.png" alt="strawberryShower">
                </div> 
            </div>
             <div class="col-md-5 _slon">
                <h2 class="_font">The Body Shop</h2> 
                            <p class="card-text">Unisex Strawberry Shower</p>
                            <p class="card-text">Get 250 ml</p>
                            <h6>AED 25.00</h6>    
                            <a href="#" class="btn theme-btn _absltBtm" >Add To Cart</a>    
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h2 class="_font _abtProdHdng">About Product</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                    Ipsum fugit ad laboriosam dolorem culpa sequi eius nesciunt excepturi, 
                    unde perspiciatis officia velit aut ut possimus quaerat obcaecati sed. 
                    Labore nihil optio ipsa exercitationem aspernatur laborum molestias. 
                    Magni perspiciatis minus excepturi voluptates eligendi, accusantium quisquam quae vitae 
                    incidunt commodi voluptatum hic repudiandae similique, minima veniam debitis. 
                    Omnis incidunt accusamus neque iste eius atque assumenda, quod molestias recusandae 
                    dolor iure voluptates alias qui quo eaque possimus, necessitatibus deserunt itaque. 
                    Accusantium, praesentium. Amet laborum ipsam sit sunt excepturi earum cumque maxime, 
                    vitae ratione esse, odio harum! Obcaecati nemo eum ratione amet magni natus.</p>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="ourProdcts">
                    <h2 class="_font">Related Products</h2>
                    <ul>
                    <li>
                        <div class="card" style="width: 18rem;">
                            <div class="prod_IMGBlk">
                                <img class="card-img-top" src="assets/img/gallery/strawberryShower.png" alt="strawberryShower">
                            </div>
                        <div class="card-body">
                            <h5 class="card-title">The Body Shop</h5>
                            <p class="card-text">Unisex Strawberry Shower</p>
                            <p class="card-text">Get 250 ml</p>
                            <h6>AED 25.00</h6>
                            <a href="#" class="btn theme-btn" data-toggle="modal" data-target="#cartModal">Add To Cart</a>
                        </div>
                        </div>  
                    </li>
                    <li>
                        <div class="card" style="width: 18rem;">
                            <div class="prod_IMGBlk">
                                <img class="card-img-top" src="assets/img/gallery/Treehut.png" alt="Treehut">
                            </div>
                        <div class="card-body">
                            <h5 class="card-title">Tree Hut</h5>
                            <p class="card-text">Shea Sugar Srub - Moroccan</p>
                            <p class="card-text">Rose 510 g</p>
                            <h6>AED 40.00</h6>
                            <a href="#" class="btn theme-btn" data-toggle="modal" data-target="#cartModal">Add To Cart</a>
                        </div>
                        </div>  
                    </li>
                    <li>
                        <div class="card" style="width: 18rem;">
                            <div class="prod_IMGBlk">
                                <img class="card-img-top" src="assets/img/gallery/riverSideNectar.png" alt="riverSideNectar">
                            </div>
                        <div class="card-body">
                            <h5 class="card-title">Riverside Nectar</h5>
                            <p class="card-text">Lemon and Peppermint Body</p>
                            <p class="card-text">Wash 250 ml</p>
                            <h6>AED 36.00</h6>
                            <a href="#" class="btn theme-btn" data-toggle="modal" data-target="#cartModal">Add To Cart</a>
                        </div>
                        </div>  
                    </li>
                    <li>
                        <div class="card" style="width: 18rem;">
                            <div class="prod_IMGBlk">
                                <img class="card-img-top" src="assets/img/gallery/bathNbody.png" alt="bathNbody">
                            </div>
                        <div class="card-body">
                            <h5 class="card-title">Bath &amp; Body Works</h5>
                            <p class="card-text">Shea Sugar Srub - Moroccan</p>
                            <p class="card-text">Rose 510 g</p>
                            <h6>AED 25.00</h6>
                            <a href="#" class="btn theme-btn" data-toggle="modal" data-target="#cartModal">Add To Cart</a>
                        </div>
                        </div>  
                    </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    </div>
</div>
 <?php include('footer.php');?>


 

<!-- Modal -->
<div class="modal fade _cartModal" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="cartModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
       
      <div class="modal-body">
          <div class="_list">
              <div>
                  <h5><span data-dismiss="modal" aria-label="Close"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Your Order</h5>
              </div>
              <div>
                <h3>AED 38.00</h3> <button type="button" class="btn" data-dismiss="modal">Proceed</button>
              </div>
          </div>
        
      </div> 
    </div>
  </div>
</div>

<style>
     
</style>