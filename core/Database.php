<?php
defined("BASEPATH") or exit("No direct script access allow");

class Database{

    private $host=db_server;
    private $username=db_username;
    private $password=db_password;
    private $database=db_database;

    public function __construct(){
        $this->host=db_server;
        $this->username=db_username;
        $this->password=db_password;
        $this->database=db_database;
    }

    public function init(){
        try {
            $dbh = new PDO('mysql:host='.$this->host.';dbname='.$this->database, $this->username, $this->password);
            return $dbh;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}