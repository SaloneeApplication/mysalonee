<?php
if (file_exists('../config/constants.php'))
{
    define("APPPATH","../");
    include '../config/constants.php';
    include '../config/config.php';

}else{
    define("APPPATH","./");
    include './config/constants.php';
    include './config/config.php';
}

include 'Common.php';
include 'Input.php';
include 'Security.php';
include 'Utf8.php';
include 'Encryption.php';
include 'Session.php';
include 'Database.php';
include 'Mailer.php';

class Controller{
    public $input;
    public $session;
    public $encryption;
    public $db;
    public $phpmailer;
    public function __construct(){
        $this->input=new Input();
        $this->encryption=new Encryption();
        $this->session=new Session();
        $this->db=new Database();
        $this->db=$this->db->init();
        $this->initMailer();
    }
    // starting
    public function init(){

    }
    public function getSMPTHost(){
        return $this->phpmailer->getHostName();
    }
    public function initMailer(){
        $this->phpmailer=new Mailer($this->getOption("smtp_host"),$this->getOption("smtp_email"),$this->getOption("smtp_email_password"),$this->getOption("smtp_port"),$this->getOption("smtp_status"));
    }
    public function randomPassword($len=8) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $len; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    public function getCitiesList(){
        $city=$this->db->prepare("select * from cities where country_id=784 order by city_name_en asc");
        $city->execute();
        return $city->fetchAll();
    }
    public function getAllCategories(){
        $city=$this->db->prepare("select * from category where status = 1");
        $city->execute();
        return $city->fetchAll();
    }
    public function getOption($name){
        $dbress=$this->db->prepare("select option_value from salonee_options where option_name=:name");
        $dbress->bindParam(":name",$name);
        $dbress->execute();
        $dbress= $dbress->fetch();
        return $dbress['option_value'];
    }
    public function getMemberAccess(){
        $user=$this->db->prepare("select membership_expiry from service_provider where service_provider_id=:uid");
        $user->bindParam(":uid",$_SESSION['serviceProviderId']);
        $user->execute();
        $user=$user->fetch();
        $member_access=0; // no access
        if($user['membership_expiry']>=date("Y-m-d")){
            $member_access=1;  // access enable
        } 
        return $member_access;
    }
    public  function getMemberInfo($type='service_provider',$user_id=null){
        if($user_id==null){
            return false;
        }
        if($type=='user'){
            $u=$this->db->prepare("select u.*,if(u.location_id!='',(select city_name_en from cities where id=u.location_id),'') as location_name from user u where u.user_id=:user_id");
            $u->bindParam(":user_id",$user_id);
        }elseif($type=='service_provider'){
            $u=$this->db->prepare("select s.*,if(s.city!='',(select city_name_en from cities where id=s.city),'') as location_name from service_provider s where s.service_provider_id=:user_id");
            $u->bindParam(":user_id",$user_id);
        }elseif($type=='salesperson'){
            $u=$this->db->prepare("select s.*,if(s.location!='',(select city_name_en from cities where id=s.location),'') as location_name from salesperson s where s.salesperson_id=:user_id");
            $u->bindParam(":user_id",$user_id);
        }
        $u->execute();
        $u=$u->fetch();
        return $u;
    }
    public function sendSMS($mobile,$message){
        if($mobile==''){
            return false;
        }
        $message=urlencode($message);
        $url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=ee7dzmQudDi&MobileNo=$mobile&SenderID=TESTID&Message=$message&ServiceName=INTERNATIONAL";
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);    
    }

    public function getAdminPageAccess($page){
        $adminId=$_SESSION['adminId'];
        $dbress=$this->db->prepare("select r.role_access from admin a,roles r where a.user_role=r.role_id and a.admin_id=$adminId");
        $dbress->execute();
        $roles=$dbress->fetch();
        $roles=$roles['role_access'];
        $roles=json_decode($roles);
        if(in_array($page,$roles)){
            return true;
        }else{
            return false;
        }
    }
    
    //ccavenue code 

    public function ccavenue_encrypt($plainText,$key)
	{
		$key = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$openMode = openssl_encrypt($plainText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
		$encryptedText = bin2hex($openMode);
		return $encryptedText;
	}

    public function ccavenue_decrypt($encryptedText,$key)
	{
		$key = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText = $this->hextobin($encryptedText);
		$decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
		return $decryptedText;
	}
    public  function ccavenue_pkcs5_pad ($plainText, $blockSize)
	{
	    $pad = $blockSize - (strlen($plainText) % $blockSize);
	    return $plainText . str_repeat(chr($pad), $pad);
	}

    public function hextobin($hexString) 
    { 
        $length = strlen($hexString); 
        $binString="";   
        $count=0; 
        while($count<$length) 
        {       
            $subString =substr($hexString,$count,2);           
            $packedString = pack("H*",$subString); 
            if ($count==0)
        {
            $binString=$packedString;
        } 
            
        else 
        {
            $binString.=$packedString;
        } 
            
        $count+=2; 
        } 
          return $binString; 
    } 

    public function encrypt($data) {
        $encryption_key = base64_decode('c2Fsb25lZV9pdHNfYV9icmFuZA==');
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
        return base64_encode($encrypted . '::' . $iv);
    }
    public function decrypt($data) {
        $encryption_key = base64_decode('c2Fsb25lZV9pdHNfYV9icmFuZA==');
        list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }
    //ccavenue code end
}