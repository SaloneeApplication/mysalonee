<?php
defined("BASEPATH") or exit("No direct script access allow");
include '../third_party/PHPMailer/PHPMailer.php';
include '../third_party/PHPMailer/OAuth.php';
include '../third_party/PHPMailer/Exception.php';
include '../third_party/PHPMailer/SMTP.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Mailer {

    public $phpmailer;
    public $from_mail;
    public function __construct($smtp_host,$smtp_email,$smtp_password,$smtp_port,$smtp_status){ 
        $this->phpmailer=new PHPMailer(false);
        $this->phpmailer->SMTPDebug = false;   
        if($smtp_status=="enable"){
            $this->phpmailer->isSMTP();                                          
            $this->phpmailer->Host       = $smtp_host;                   
            $this->phpmailer->SMTPAuth   = true;                                   
            $this->phpmailer->Username   = $smtp_email;                    
            $this->phpmailer->Password   = $smtp_password;                               
            $this->phpmailer->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         
            $this->phpmailer->Port       = $smtp_port;     
        } 
        $this->from_mail=$smtp_email;            
    }

  
    public function getHostName(){  
        return $this->phpmailer->Host;
    }
    public function sendMail($to,$subject,$message){
        if($_SERVER['HTTP_HOST']=='localhost'){
            // return true;
        }
        $this->phpmailer->setFrom($this->from_mail, "MySalonee");
        $this->phpmailer->addAddress($to, '');     // Add a recipient
        $this->phpmailer->addReplyTo($this->from_mail, "MySalonee");
        // Content
        $this->phpmailer->isHTML(true);                                  // Set email format to HTML
        $this->phpmailer->Subject = $subject;
        $this->phpmailer->Body    = $message;
        $this->phpmailer->AltBody = strip_tags($message);
        $this->phpmailer->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $this->phpmailer->send();
        // exit;
        return true;
    }
}
