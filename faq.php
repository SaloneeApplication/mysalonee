<?php 
include('header.php');
$content = $funcObject->privacyPrivacy($con);

while($row = mysqli_fetch_array($content))
{
    $faq = $row['faq'];
}

?>
<style>
    .card-faq {
        padding: 30px;
        text-align: justify;
    }
    .card-faq tt {
        color: #d06690;
    }
    .card-faq ul {
        display: block !important;
        list-style-type: disc !important;
    }
    .card-faq ul li {
        max-width: 100% !important;
        display: list-item !important;
    }
</style>
<div class="container-fluid">
    <div class="_header"></div>
    <div class="container p-0">
        <nav aria-label="breadcrumb" class="_custmBrdcrmb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">FAQ</li>
            </ol>
        </nav>
    </div>

    <div class="nearShops _nrShops">
        <div class="container">
            <div class="card card-faq">
                <?php echo $faq; ?>
            </div>
        </div>
    </div>
</div>
<?php 
include('footer.php');
?>