<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserBranches extends Controller{
    public $user_id;
    public $auth_type;
    public $member_access=0;
    public $uinfo;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        $user=$this->db->prepare("select membership_expiry from service_provider where service_provider_id=:uid");
        $user->bindParam(":uid",$this->user_id);
        $user->execute();
        $this->uinfo=$user->fetch();
        if($this->uinfo['membership_expiry']>=date("Y-m-d")){
            $this->member_access=1;
        } 
        
    }
    
    public function init(){
        if($this->branch_type!='main_branch'){
            $referer=$_SERVER['HTTP_REFERER'];
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"you don't have access"));
            redirect($referer);
        }
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                if($this->branch_type!='main_branch'){
                    $referer=$_SERVER['HTTP_REFERER'];
                    $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"you don't have access"));
                    redirect($referer);
                }
                $this->addServiceProvider();
            }elseif($type=='getOrderedProducts'){
                $this->getOrderedProducts();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            // if($type=='active_user'){
                
            //     $this->activateUser();
            // }elseif($type=='block_user'){
            //     $this->deactivateUser();
            // }
            
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Branches';
        $data['page']='branches';
        $data['user_id']=$this->user_id;
        return $data;
    }
    
    public function addServiceProvider(){
        $referer=$_SERVER['HTTP_REFERER'];
        $aid=$this->user_id;
        $name=$this->input->post("name",true);
        $business_name=$this->input->post("business_name",true);
        $email=$this->input->post("email",true);
        $mobile=$this->input->post("mobile",true);
        $address=$this->input->post("address",true);
        $city=$this->input->post("city",true);
        if($email==''){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Required"));
            redirect($referer);
            exit;
        }
       
        $che=$this->db->prepare("select email from service_provider where email=:email");
        $che->bindParam(":email",$email);
        $che->execute();
        $count=$che->rowCount();
        if($count>0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Exist"));
            redirect($referer);

            exit;
        }

        if(strpos($email,"@")==false){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Invalid Email"));
            redirect($referer);

            exit;
        }
        $che=$this->db->prepare("select mobile from service_provider where mobile=:mobile");
        $che->bindParam(":mobile",$mobile);
        $che->execute();
        $count=$che->rowCount();
        if($count>0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Mobile Number Exist"));
            redirect($referer);

            exit;
        }
        $tmp_name = $_FILES["image"]["tmp_name"];
        $path=dirname(__DIR__).'/salonee_admin/uploads/certificates/'.basename($_FILES["image"]["name"]);
        $file='uploads/certificates/'.basename($_FILES["image"]["name"]);
        move_uploaded_file($tmp_name,$path);
     
        $country_code=971;
        $password=$this->randomPassword(6);
        $dbres=$this->db->prepare("insert into service_provider (name,business_name,email,mobile,password,business_licence,status,city,address,user_type,referer_by,country_code) values (:name,:business_name,:email,:mobile,:password,:image,0,:city,:address,'sub_branch','main_branch',:country_code)");
        $dbres->bindParam(":name",$name);
        $dbres->bindParam(":business_name",$business_name);
        $dbres->bindParam(":email",$email);
        $dbres->bindParam(":mobile",$mobile);
        $dbres->bindParam(":password",$password);
        $dbres->bindParam(":image",$file);
        $dbres->bindParam(":city",$city);
        $dbres->bindParam(":address",$address);
        $dbres->bindParam(":country_code",$country_code);
        $dbres->execute();
        $spid=$this->db->lastInsertId();
        
        $dbress=$this->db->prepare("insert into service_provider_branches (service_provider_main_branch_id,service_provider_id,address,location_id) values (:sid,:spid,:address,:city)");
        $dbress->bindParam(":sid",$aid);
        $dbress->bindParam(":spid",$spid);
        $dbress->bindParam(":address",$address);
        $dbress->bindParam(":city",$city);
        $dbress->execute();

        //wallet creation
        $dbress=$this->db->prepare("insert into service_provider_wallet (service_provider_id,amount) values (:spid,0)");
        $dbress->bindParam(":spid",$spid);
        $dbress->execute();

        $sms_msg="Hai Welcome to Salonee your business account created and under verification process.";
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $message='';
        $message.='Hi Welcome to Salonee <br/>';
        $message.='Thanking you for registered with us<br/>';
        $message.='Your login details : <br/>';
        $message.='Email : '.$email.'<br/>';
        $message.='Password : '.$password.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Thank you for register us",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Branch added"));
        redirect($referer);
        exit;
    }

    public function getBranches(){
        $bran=$this->db->prepare("select sp.service_provider_id as branch_id,s.address,sp.status,if(s.service_provider_main_branch_id=s.service_provider_id,'main_branch','sub_branch') as branch_type,if(s.location_id!='' || s.location_id!=0,(select city_name_en from cities where id=s.location_id),'') as city_name from service_provider_branches s, service_provider sp where s.service_provider_id=sp.service_provider_id and s.service_provider_main_branch_id=:id order by s.created_time desc");
        $bran->bindParam(":id",$this->user_id);
        $bran->execute();
        $bran=$bran->fetchAll();
        return $bran;
    }
    
    public function pageViewUserData(){
        if($this->input->get("id",true)==''){
            header("location:".user_base_url."branches.php");
            exit;
        }
        
        $uid=$this->input->get("id",true);

        $dbress=$this->db->prepare("select service_provider_branche_id from service_provider_branches where service_provider_main_branch_id=:uid and service_provider_id=:sid");
        $dbress->bindParam(":sid",$uid);
        $dbress->bindParam(":uid",$this->user_id);
        $dbress->execute();
        if($dbress->rowCount()==0){
            $this->session->set_userdata(array("alert_type"=>"warning","alert_msg"=>"Permissions denied"));
            header("location:".user_base_url."branches.php");
            exit;
        }
        $data=array();
        $data['title']='Service Provider | View Service Provder';
        $data['page']='service-provider';
        $data['user_id']=$this->user_id;

        $total_sb=$this->db->prepare("select * from service_slots where service_provider_id=:uid and payment_status='completed' and service_status='completed'");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['total_sb']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select * from service_slots where service_provider_id=:uid and payment_status='completed' and booking_type='rescheduled'");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['total_re']=$total_sb->rowCount();
        
        $total_sb=$this->db->prepare("select order_id from orders where order_type='product' and payment_status='completed' and order_status='completed' and service_provider_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['total_p']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select sum(total_amount) as total from orders where  payment_status='completed' and order_status='on_hold' and service_provider_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $total_sb=$total_sb->fetch();
        $data['total_hold']=$total_sb['total'];

        $total_sb=$this->db->prepare("select amount from service_provider_wallet where service_provider_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $total_sb=$total_sb->fetch();
        $data['total_wallet']=$total_sb['amount'];

        // $on_hold=$this->db->prepare("select round(coalesce(sum(total_amount),0),1) as total_amount from orders where payment_status='completed' and order_status ='on_hold' and user_id=:uid");
        // $on_hold->bindParam(":uid",$uid);
        // $on_hold->execute();
        // $on_hold=$on_hold->fetch();
        // $data['on_hold']=$on_hold['total_amount'];

        $total_sb=$this->db->prepare("select * from service_provder_wallet_transactions where service_provider_id=:uid order by created_time desc");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['wallet_txns']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select * from service_provider_withdrawals where service_provider_id=:uid order by created_time desc");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['payouts']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select * from service_provider_withdrawals where service_provider_id=:uid and withdrawal_type='early' order by created_time desc");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['early_paytouts']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select s.service_type,o.*,s.service_status,s.slot_date,s.service_provider_id,sp.name,if(sp.city!='',(select city_name_en from cities where id=sp.city),'') as city_name,if(s.service_id!='',(select service_name from service_provider_services where service_provider_service_id=s.service_id),'') as service_name,if(s.user_id!='',(select name from user where user_id=s.user_id),'') as user_name from orders o,service_slots s,service_provider sp where o.service_slot_id=s.slot_id and s.service_provider_id=sp.service_provider_id and o.order_type='slot' and o.payment_status='completed' and s.service_provider_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        // echo json_encode($this->db->errorInfo());
        $data['slots_booked']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select o.*,(select count(id) from order_products where order_id=o.order_id) as total_products,(select name from service_provider where service_provider_id=o.service_provider_id) as service_provider_name from orders o where o.order_type='product' and o.payment_status='completed'  and o.service_provider_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['products']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select * from featured_profiles where service_provider_id=:uid order by created_time desc");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['featured']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.service_provider_id=:uid order by a.created_time desc");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['advertisements']=$total_sb->fetchAll();

        return $data;
    }
    
    public function getOrderedProducts(){
        $oid=$this->input->post("oid",true);
        echo '<tr><th>Product Name</th><th>Image</th><th>Price</th><th>QTY</th></tr>';
        $db=$this->db->prepare("select o.*,p.product_title,p.product_image from order_products o ,products p where o.product_id=p.id and o.order_id=:oid");
        $db->bindParam(":oid",$oid);
        $db->execute();
        while($row=$db->fetch()){
            echo '<tr>';
                echo '<td>'.$row['product_title'].'</td>';
                echo '<td><img src="'.user_base_url.$row['product_image'].'" width="100px" /></td>';
                echo '<td>AED '.$row['total_amount'].'</td>';
                echo '<td>'.$row['qty'].'</td>';
            echo '/<tr>';

        }
        exit;
    }
    public function getMainBranchInfo($id){
        $dbres=$this->db->prepare("select * from service_provider where service_provider_id in (select service_provider_main_branch_id from service_provider_branches where service_provider_id=:id)");
        $dbres->bindParam(":id",$id);
        $dbres->execute();
        return $dbres->fetch();
    }
}