<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminUsers extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("users")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function load(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }elseif($type=='refundOrderID'){
                $this->refundOrderID();
            }elseif($type=='getOrderedProducts'){
                $this->getOrderedProducts();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }elseif($type=='send_password'){
                $this->sendUserPassword();
            }
        }
    }

    public function sendUserPassword(){
        $cid=$this->input->get("id",true);
        $che=$this->db->prepare("select * from user where user_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();
        $email=$user['email'];
        $name=$user['name'];
        $password=$user['password'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account login password is '.$password.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account login password.",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully password has been sent to the Registered User"));
        redirect(admin_base_url.'view-user.php?id='.$cid);
        exit;
    }
    public function activateUser(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("id",true);
        if(strpos($referer,"view-user")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$cid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $dbres=$this->db->prepare("update user set status=1 where user_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully User activated "));
        $che=$this->db->prepare("select * from user where user_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();
        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been activated.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account resumed.",$message);
        redirect($referer);
        exit;
    }
    public function deactivateUser(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("id",true);
        if(strpos($referer,"view-user")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$cid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $dbres=$this->db->prepare("update user set status=2 where user_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully User deactivated "));
        $che=$this->db->prepare("select * from user where user_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();
        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been blocked. please contact us.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account blocked.",$message);
        redirect($referer);
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | User\'s Details';
        $data['page']='users';
        $data['admin_id']=$this->admin_id;
        return $data;
    }
    public function pageViewUserData(){
        if($this->input->get("id",true)==''){
            header("location:".admin_base_url."users.php");
            exit;
        }
        $uid=$this->input->get("id",true);
        $data=array();
        $data['title']='Admin | View User';
        $data['page']='users';
        $data['admin_id']=$this->admin_id;
        $total_sb=$this->db->prepare("select order_id from orders where order_type='slot' and payment_status='completed' and order_status='completed' and user_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['total_sb']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select order_id from orders where order_type='product' and payment_status='completed' and order_status='completed' and user_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['total_p']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select order_id from orders where order_type='product' and payment_status='completed' and order_status in ('cancelled','refund_pending') and user_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['total_pen']=$total_sb->rowCount();

        // $on_hold=$this->db->prepare("select round(coalesce(sum(total_amount),0),1) as total_amount from orders where payment_status='completed' and order_status ='on_hold' and user_id=:uid");
        // $on_hold->bindParam(":uid",$uid);
        // $on_hold->execute();
        // $on_hold=$on_hold->fetch();
        // $data['on_hold']=$on_hold['total_amount'];

        $total_sb=$this->db->prepare("select * from orders where order_type='product' and payment_status='completed' and order_status in ('cancelled','refund_pending') and user_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['pending_ref_orders']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select * from orders where order_type='product' and payment_status='completed' and order_status in ('refunded') and user_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['done_ref_orders']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select s.service_type,o.*,s.service_status,s.slot_date,s.service_provider_id,sp.name,if(sp.city!='',(select city_name_en from cities where id=sp.city),'') as city_name,if(s.service_id!='',(select service_name from service_provider_services where service_provider_service_id=s.service_id),'') as service_name from orders o,service_slots s,service_provider sp where o.service_slot_id=s.slot_id and s.service_provider_id=sp.service_provider_id and o.order_type='slot' and o.payment_status='completed' and s.user_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        // echo json_encode($this->db->errorInfo());
        $data['slots_booked']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select o.*,(select count(id) from order_products where order_id=o.order_id) as total_products,(select name from service_provider where service_provider_id=o.service_provider_id) as service_provider_name from orders o where o.order_type='product' and o.payment_status='completed'  and o.user_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['products']=$total_sb->fetchAll();

        return $data;
    }
    public function getUsers(){
        $search_type=$this->input->post("search_type",true);
        $search=$this->input->post("search",true);
        $where='';
        if($search_type=='by_email'){
            $where=' where u.email=:search ';
        }elseif($search_type=='by_phone'){
            $where=' where u.mobile=:search ';
        }elseif($search_type=='by_customerid'){
            $where=' where u.user_id=:search ';
        }
        $users=$this->db->prepare("select u.*,if(u.location_id!='',(select city_name_en from cities where id=u.location_id),'') as city_name from user u  $where order by created_time desc");
        if($search_type!=''){
            $users->bindParam(":search",$search);
        }
        $users->execute();
        return $users->fetchAll();
    }

    public function refundOrderID(){
        $referer=$_SERVER['HTTP_REFERER'];

        $oid=$this->input->post("id",true);
        $refund_amount=$this->input->post("refund_amount",true);
        $refund_deductions=$this->input->post("refund_deductions",true);

        
        $od=$this->db->prepare("select * from orders where order_id=:id and user_type='user'");
        $od->bindParam(":id",$oid);
        $od->execute();
        $od=$od->fetch();

        $order_type=$od['order_type'];
        $order_id=$od['order_id'];

        $che=$this->db->prepare("select * from user where user_id=:id");
        $che->bindParam(":id",$od['user_id']);
        $che->execute();
        $user=$che->fetch();

        $up=$this->db->prepare("update orders set order_status='refunded',refunded_amount=:refunded_amount,refunded_deductions=:refunded_deductions,shipping_status='cancelled',tracking_id=NULL,remark='User Cancelled the order after payment' where order_id=:oid");
        $up->bindParam(":oid",$order_id);
        $up->bindParam(":refunded_amount",$refund_amount);
        $up->bindParam(":refunded_deductions",$refund_deductions);
        $up->execute();

        if($order_type=='slot'){
            $service_slot_id=$od['service_slot_id'];
            $up=$this->db->prepare("update service_slots set service_status='cancelled',payment_status='cancelled' where slot_id =:slot_id ");
            $up->bindParam(":slot_id ",$service_slot_id);
            $up->execute();
        }

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your Order '.$order_id.'  Refund initiated its take 7 working days to credit to your bank account<br/>
            Payment Info : <br/>
            Refunded Amount : AED '.$refund_amount.' <br/>
            Refund Deductions : AED '.$refund_deductions.'<br/>
        ';
        $message.='<br/>Thanking you';

        $this->phpmailer->sendMail($email,"Refund initiated | Salonee ",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Refund initiated "));
        redirect($referer);
        exit;

    }

    public function getOrderedProducts(){
        $oid=$this->input->post("oid",true);
        echo '<tr><th>Product Name</th><th>Image</th><th>Price</th><th>QTY</th></tr>';
        $db=$this->db->prepare("select o.*,p.product_title,p.product_image from order_products o ,products p where o.product_id=p.id and o.order_id=:oid");
        $db->bindParam(":oid",$oid);
        $db->execute();
        while($row=$db->fetch()){
            echo '<tr>';
                echo '<td>'.$row['product_title'].'</td>';
                echo '<td><img src="'.user_base_url.$row['product_image'].'" width="100px" /></td>';
                echo '<td>AED '.$row['total_amount'].'</td>';
                echo '<td>'.$row['qty'].'</td>';
            echo '/<tr>';

        }
        exit;
    }
    
}