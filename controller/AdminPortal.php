<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminPortal extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("settings")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='common'){
                $this->commonSave();
            }elseif($type=='pricing'){
                $this->savePricing();
            }elseif($type=='smtp'){
                $this->saveSMTP();
            }elseif($type=='testEmail'){
                $this->sendTestEmail();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Settings';
        $data['page']='settings';
        $data['admin_id']=$this->admin_id;
        return $data;
    }

    
    public function sendTestEmail(){
        $referer=$_SERVER['HTTP_REFERER'];
        $test_email=$this->input->post("test_email",true);
      
        $email=$test_email;
        $name="Test Mailer Salonee";
        $message='';
        $message.='Hi <br/>';
        $message.='This is one test mail from salonee';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Test Email | Salonee.",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Test mail sent."));
        redirect($referer);
        exit;
    }
    public function saveSMTP(){
        $referer=$_SERVER['HTTP_REFERER'];
        $smtp_host=$this->input->post("smtp_host",true);
        $smtp_email=$this->input->post("smtp_email",true);
        $smtp_email_password=$this->input->post("smtp_email_password",true);
        $smtp_port=$this->input->post("smtp_port",true);
        $smtp_status=$this->input->post("smtp_status",true); 
        
        $dbres=$this->db->prepare("update salonee_options set option_value=:smtp_host where option_name='smtp_host'");
        $dbres->bindParam(":smtp_host",$smtp_host);
        $dbres->execute();

        $dbres=$this->db->prepare("update salonee_options set option_value=:smtp_email where option_name='smtp_email'");
        $dbres->bindParam(":smtp_email",$smtp_email);
        $dbres->execute();

        $dbres=$this->db->prepare("update salonee_options set option_value=:smtp_email_password where option_name='smtp_email_password'");
        $dbres->bindParam(":smtp_email_password",$smtp_email_password);
        $dbres->execute();

        $dbres=$this->db->prepare("update salonee_options set option_value=:smtp_port where option_name='smtp_port'");
        $dbres->bindParam(":smtp_port",$smtp_port);
        $dbres->execute();

        $dbres=$this->db->prepare("update salonee_options set option_value=:smtp_status where option_name='smtp_status'");
        $dbres->bindParam(":smtp_status",$smtp_status);
        $dbres->execute();

        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully SMTP settings saved."));
        redirect($referer);
        exit;
    }
    public function savePricing(){
        $referer=$_SERVER['HTTP_REFERER'];
        $featured_profile_plan_per_day=$this->input->post("featured_profile_plan_per_day",true);
        $featured_profiles_admin_commission_per_day=$this->input->post("featured_profiles_admin_commission_per_day",true);

        $advertisement_per_day=$this->input->post("advertisement_per_day",true);
        $advertisement_per_day_admin_commission=$this->input->post("advertisement_per_day_admin_commission",true);

        $cancellation_slot_charges=$this->input->post("cancellation_slot_charges",true);
        $cancellation_product_charges=$this->input->post("cancellation_product_charges",true);

        
        $dbres=$this->db->prepare("update salonee_options set option_value=:featured_profile_plan_per_day where option_name='featured_profile_plan_per_day'");
        $dbres->bindParam(":featured_profile_plan_per_day",$featured_profile_plan_per_day);
        $dbres->execute();
        $dbres=$this->db->prepare("update salonee_options set option_value=:featured_profiles_admin_commission_per_day where option_name='featured_profiles_admin_commission_per_day'");
        $dbres->bindParam(":featured_profiles_admin_commission_per_day",$featured_profiles_admin_commission_per_day);
        $dbres->execute();

        $dbres=$this->db->prepare("update salonee_options set option_value=:advertisement_per_day where option_name='advertisement_per_day'");
        $dbres->bindParam(":advertisement_per_day",$advertisement_per_day);
        $dbres->execute();
        $dbres=$this->db->prepare("update salonee_options set option_value=:advertisement_per_day_admin_commission where option_name='advertisement_per_day_admin_commission'");
        $dbres->bindParam(":advertisement_per_day_admin_commission",$advertisement_per_day_admin_commission);
        $dbres->execute();

        $dbres=$this->db->prepare("update salonee_options set option_value=:cancellation_slot_charges where option_name='cancellation_slot_charges'");
        $dbres->bindParam(":cancellation_slot_charges",$cancellation_slot_charges);
        $dbres->execute();
        $dbres=$this->db->prepare("update salonee_options set option_value=:cancellation_product_charges where option_name='cancellation_product_charges'");
        $dbres->bindParam(":cancellation_product_charges",$cancellation_product_charges);
        $dbres->execute();


        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully pricing settings saved."));
        redirect($referer);
        exit;
    }
    public function commonSave(){
        $referer=$_SERVER['HTTP_REFERER'];
        $slot_dispute_disable_before_days=$this->input->post("slot_dispute_disable_before_days",true);
        $product_dispute_disable_before_days=$this->input->post("product_dispute_disable_before_days",true);
        
        $dbres=$this->db->prepare("update salonee_options set option_value=:slot_dispute_disable_before_days where option_name='slot_dispute_disable_before_days'");
        $dbres->bindParam(":slot_dispute_disable_before_days",$slot_dispute_disable_before_days);
        $dbres->execute();

        $dbres=$this->db->prepare("update salonee_options set option_value=:product_dispute_disable_before_days where option_name='product_dispute_disable_before_days'");
        $dbres->bindParam(":product_dispute_disable_before_days",$product_dispute_disable_before_days);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Common settings saved."));
        redirect($referer);
        exit;
    }
}