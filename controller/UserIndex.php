<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserIndex extends Controller{
    public function __construct(){
        parent::__construct();
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='forgotpassword'){
                $this->forgotpassword();
            }elseif($type=='newRegistration'){
                $this->newRegistration();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
           
        }
    }
    public function forgotpassword(){
        $email=$this->input->post("email",true);

        $check=$this->db->prepare("select service_provider_id,email,password from service_provider where email=:email");
        $check->bindParam(":email",$email);
        $check->execute();
        if($check->rowCount()==0){
            $_SESSION['message'] = 'Please enter valid email address';	
            header("location:index.php");
            exit;
        }

        $user=$check->fetch();
        $email=$user['email'];
        $password=$user['password'];
           
        $message='';
        $message.='Hi '.ucwords($user['name']).'<br/>';
        $message.='Your login password is  '.$password.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($user['email'],"Forgot Password ! Salonee",$message);
        $_SESSION['success'] = 'Your password has been sent to your registered email.';	
        header("location:index.php");
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Dashboard';
        $data['page']='dashboard';
        $data['user_id']=$this->user_id;
        return $data;
    }
    public function newRegistration(){
        
        $referer=$_SERVER['HTTP_REFERER'];
        $name=$this->input->post("name",true);
        $business_name=$this->input->post("business_name",true);
        $email=$this->input->post("email",true);
        $mobile=$this->input->post("mobile",true);
        $address=$this->input->post("address",true);
        $city=22;//$this->input->post("city",true);
        $country_id=$this->input->post("country_id",true);
        $city_id=$this->input->post("city_id",true);
        $latitude=$this->input->post("latitude",true);
        $longitude=$this->input->post("longitude",true);
        if($email==''){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Required"));
            $_SESSION['message']="Email Required";
            redirect($referer);
            exit;
        }
      
        $che=$this->db->prepare("select email from service_provider where email=:email");
        $che->bindParam(":email",$email);
        $che->execute();
        $count=$che->rowCount();
        if($count>0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Exist"));
            $_SESSION['message']="Email Exist";
            redirect($referer);
            exit;
        }

        if(strpos($email,"@")==false){
            // $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Invalid Email"));
            $_SESSION['message']="Invalid Email";
            redirect($referer);
            exit;
        }
        $che=$this->db->prepare("select mobile from service_provider where mobile=:mobile");
        $che->bindParam(":mobile",$mobile);
        $che->execute();
        $count=$che->rowCount();
        if($count>0){
            // $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Mobile Number Exist"));
            $_SESSION['message']="Mobile Number Exist";
            redirect($referer);
            exit;
        }
        $tmp_name = $_FILES["image"]["tmp_name"];
        $path=dirname(__DIR__).'/service_provider/uploads/certificates/'.basename($_FILES["image"]["name"]);
        $file='uploads/certificates/'.basename($_FILES["image"]["name"]);
        move_uploaded_file($tmp_name,$path);

        $tmp_name_shop = $_FILES["shop_image"]["tmp_name"];
        $path_shop=dirname(__DIR__).'/service_provider/uploads/shop/'.basename($_FILES["shop_image"]["name"]);
        $file_shop='uploads/shop/'.basename($_FILES["shop_image"]["name"]);
        move_uploaded_file($tmp_name_shop,$path_shop);
     
        $country_code=971;
        $password=$this->randomPassword(6);
        $dbres=$this->db->prepare("insert into service_provider (name,business_name,email,mobile,password,business_licence,image,status,city,address,user_type,referer_by,country_code,country_id,city_id,latitude,longitude) values (:name,:business_name,:email,:mobile,:password,:image,:shop_image,0,:city,:address,'sub_branch','main_branch',:country_code,:country_id,:city_id,:latitude,:longitude)");
        $dbres->bindParam(":name",$name);
        $dbres->bindParam(":business_name",$business_name);
        $dbres->bindParam(":email",$email);
        $dbres->bindParam(":mobile",$mobile);
        $dbres->bindParam(":password",$password);
        $dbres->bindParam(":image",$file);
        $dbres->bindParam(":shop_image",$file_shop);
        $dbres->bindParam(":city",$city);
        $dbres->bindParam(":address",$address);
        $dbres->bindParam(":country_code",$country_code);
        $dbres->bindParam(":country_id",$country_id);
        $dbres->bindParam(":city_id",$city_id);
        $dbres->bindParam(":latitude",$latitude);
        $dbres->bindParam(":longitude",$longitude);
        $dbres->execute();
        // echo json_encode($dbres->errorInfo()); die;
        $spid=$this->db->lastInsertId();

        //wallet creation
        $dbress=$this->db->prepare("insert into service_provider_wallet (service_provider_id,amount) values (:spid,0)");
        $dbress->bindParam(":spid",$spid);
        $dbress->execute();

        $sms_msg="Hai Welcome to Salonee your business account created and under verification process.";
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $message='';
        $message.='Hi Welcome to Salonee <br/>';
        $message.='Thanking you for registered with us<br/>';
        $message.='Your login details : <br/>';
        $message.='Email : '.$email.'<br/>';
        $message.='Password : '.$password.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Thank you for register us",$message);
        // $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Branch added"));
        $_SESSION['success']="Successfully account created.";
        redirect($referer);
        exit;
    }
}