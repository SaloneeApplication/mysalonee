<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserSlots extends Controller{
    public $user_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }elseif($type=='getByMonthSlots'){
                $this->getByMonthSlots();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Slots';
        $data['page']='slots';
        $data['user_id']=$this->user_id;
        return $data;
    }
    public function getServicesPrices(){
        $dbres=$this->db->prepare("select p.*,sp.service_name,sp.time_slot_freq from services_prices p ,service_provider_services sp where p.service_provider_service_id = sp.service_provider_service_id and sp.service_provider_id=:id");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->execute();
        return $dbres->fetchAll();
    }
    public function getThisMonthSlots(){
        $date=date("Y-m")."%";
        $dbres=$this->db->prepare("select s.*,u.name,sp.service_name from service_slots s,user u,service_provider_services sp where s.user_id=u.user_id and s.service_id=sp.service_provider_service_id and s.service_provider_id=:uid and s.slot_date like :date");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->bindParam(":date",$date);
        $dbres->execute();
        return $dbres->fetchAll();
    }
    public function getByMonthSlots(){
        $date=$this->input->post("date",true);
        $date= $date."%";
        $dbres=$this->db->prepare("select s.*,u.name,sp.service_name from service_slots s,user u,service_provider_services sp where s.user_id=u.user_id and s.service_id=sp.service_provider_service_id and s.service_provider_id=:uid and s.slot_date like :date");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->bindParam(":date",$date);
        $dbres->execute();
        $resp=array();
        $ids=array();
        while($nowd=$dbres->fetch()){
            $slot_date=date("Y-m-d",strtotime($nowd['slot_date']));
            $slot_time=date("H:i A",strtotime($nowd['slot_date']));
            $ids[]=$nowd['slot_id'];
            if($nowd['service_status']=='pending'){
                $resp[]=array(
                "name"=>ucwords($nowd['service_name']),
                "description"=>ucwords($nowd['name'])." Service booked on ".$slot_date." ".$slot_time." at ".ucwords($nowd['service_type']),
                "date"=>date("M/d/Y",strtotime($slot_date)),
                "type"=>"event",
                "color"=>"#f1c40f",
                "id"=>$nowd['slot_id']
                );
            }elseif($nowd['service_status']=='inprocess'){
            $resp[]=array(
                "name"=>ucwords($nowd['service_name']),
                "description"=>ucwords($nowd['name'])." Service booked on ".$slot_date." ".$slot_time." at ".ucwords($nowd['service_type']),
                "date"=>date("M/d/Y",strtotime($slot_date)),
                "type"=>"event",
                "color"=>"#9b59b6",
                "id"=>$nowd['slot_id']
            );
            }elseif($nowd['service_status']=='completed'){
            $resp[]=array(
                "name"=>ucwords($nowd['service_name']),
                "description"=>ucwords($nowd['name'])." Service booked on ".$slot_date." ".$slot_time." at ".ucwords($nowd['service_type']),
                "date"=>date("M/d/Y",strtotime($slot_date)),
                "type"=>"event",
                "color"=>"#2ecc71",
                "id"=>$nowd['slot_id']
            );
            }else{
            $resp[]=array(
                "name"=>ucwords($nowd['service_name']),
                "description"=>ucwords($nowd['name'])." Service booked on ".$slot_date." ".$slot_time." at ".ucwords($nowd['service_type']),
                "date"=>date("M/d/Y",strtotime($slot_date)),
                "type"=>"event",
                "color"=>"#e74c3c",
                "id"=>$nowd['slot_id']
            );
            }
        }
        $final=array();
        $final['ids']=$ids;
        $final['event']=$resp;
        echo json_encode($final);
        exit;
    }
}