<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserDashboard extends Controller{
    public $user_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Dashboard';
        $data['page']='dashboard';
        $data['user_id']=$this->user_id;
        $date=date("Y-m-d");

        $total_slots=$this->db->prepare("select s.slot_id from service_slots s ,service_provider_services sp where s.service_id=sp.service_provider_service_id and s.payment_status='completed' and sp.service_provider_id=:uid and s.service_status='completed'");
        $total_slots->bindParam(":uid",$this->user_id);
        $total_slots->execute();
        $data['total_slots']=$total_slots->rowCount();

        $today_slots=$this->db->prepare("select s.slot_id from service_slots s ,service_provider_services sp where s.service_id=sp.service_provider_service_id and s.payment_status='completed' and sp.service_provider_id=:uid and s.service_status='pending' and cast(s.slot_date as date) =:date");
        $today_slots->bindParam(":uid",$this->user_id);
        $today_slots->bindParam(":date",$date);
        $today_slots->execute();
        $data['today_slots']=$today_slots->rowCount();

        $upcoming_slots=$this->db->prepare("select s.slot_id from service_slots s ,service_provider_services sp where s.service_id=sp.service_provider_service_id and s.payment_status='completed' and sp.service_provider_id=:uid and s.service_status='pending' and cast(s.slot_date as date) >:date");
        $upcoming_slots->bindParam(":uid",$this->user_id);
        $upcoming_slots->bindParam(":date",$date);
        $upcoming_slots->execute();
        $data['upcoming_slots']=$upcoming_slots->rowCount();

        $wallet=$this->db->prepare("select round(amount,1) as amount from service_provider_wallet where service_provider_id=:uid");
        $wallet->bindParam(":uid",$this->user_id);
        $wallet->execute();
        $wallet=$wallet->fetch();
        $data['wallet']=$wallet['amount'];

        $onhold=$this->db->prepare("select round(coalesce(sum(total_amount),0),1) as total from orders where service_provider_id=:uid and payment_status='completed' and order_status='on_hold'");
        $onhold->bindParam(":uid",$this->user_id);
        $onhold->execute();
        $onhold=$onhold->fetch();
        $data['on_hold_balance']=$onhold['total'];

        $onhold=$this->db->prepare("select count(service_provider_id) as total from orders where service_provider_id=:uid and payment_status='completed' and order_status='pending'");
        $onhold->bindParam(":uid",$this->user_id);
        $onhold->execute();
        $onhold=$onhold->fetch();
        $data['pending_product_orders']=$onhold['total'];

        $startdate=date("Y-m-d",strtotime("-10 days"));
        $enddate=date("Y-m-d");

        $slots_chart=array();
        $products_chart=array();
        for($i=0;$i<=10;$i++){
            $onhold=$this->db->prepare("select sum(total_amount) as total from orders where service_provider_id=:uid and payment_status='completed' and order_status='completed' and order_type='slot' and cast(date_of_order as date)='$startdate'");
            $onhold->bindParam(":uid",$this->user_id);
            $onhold->execute();
            $onhold=$onhold->fetch();
            $total=$onhold['total'];
            $slots_chart['dates'][]=$startdate;
            $slots_chart['amount'][]=($total=='')?0:$total;

            $onhold=$this->db->prepare("select sum(total_amount) as total from orders where service_provider_id=:uid and payment_status='completed' and order_status='completed' and order_type='product' and cast(date_of_order as date)='$startdate'");
            $onhold->bindParam(":uid",$this->user_id);
            $onhold->execute();
            $onhold=$onhold->fetch();
            $total=$onhold['total'];
            $products_chart['dates'][]=$startdate;
            $products_chart['amount'][]=($total=='')?0:$total;
            $startdate=date("Y-m-d",strtotime("+1 day",strtotime($startdate)));
        }
        $data['products_chart']=$products_chart;
        $data['slots_chart']=$slots_chart;

        return $data;
    }
}