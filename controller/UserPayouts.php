<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserPayouts extends Controller{
    public $user_id;
    public $branch_type;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='withdrawRequest'){
                $this->withdrawRequest();
            }elseif($type=='bankDetails'){
                $this->bankDetails();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Payouts Requests';
        $data['page']='payouts';
        $data['user_id']=$this->user_id;
        return $data;
    }
    public function getBalance(){
        $dbres=$this->db->prepare("select amount from service_provider_wallet where service_provider_id=:uid");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->execute();
        $dbres=$dbres->fetch();
        return $dbres['amount'];
    }
    public function getWithdrawalsData(){
        $dbres=$this->db->prepare("select * from service_provider_withdrawals where service_provider_id=:uid");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->execute();
        $dbres=$dbres->fetchAll();
        return $dbres;
    }
    public function withdrawRequest(){
        $referer=$_SERVER['HTTP_REFERER'];
        $bank_name=$this->input->post("bank_name",true);
        $amount=$this->input->post("amount",true);
        $account_number=$this->input->post("account_number",true);
        $account_name=$this->input->post("account_name",true);
        $ifsc=$this->input->post("ifsc",true);

        $balance=$this->getBalance();
        if($balance<500){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"You don\'t have minimum funds to early payout request"));
            redirect($referer);
            exit;
        }
     
        
        $amount=$balance;
        $now=$balance-$amount;
        $dbres=$this->db->prepare("insert into service_provider_withdrawals (service_provider_id ,request_amount,account_number,account_name,bank_name,ifsc_code ,withdrawal_type) values (:uid,:request_amount,:account_number,:account_name,:bank_name,:ifsc_code,'early')") ;
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->bindParam(":request_amount",$amount);
        $dbres->bindParam(":account_number",$account_number);
        $dbres->bindParam(":account_name",$account_name);
        $dbres->bindParam(":bank_name",$bank_name);
        $dbres->bindParam(":ifsc_code",$ifsc);
        $dbres->execute();
        $wid=$this->db->lastInsertId();

        $dbres1=$this->db->prepare("update service_provider_wallet set amount=:now where service_provider_id=:uid");
        $dbres1->bindParam(":uid",$this->user_id);
        $dbres1->bindParam(":now",$now);
        $dbres1->execute();
        $date=date("Y-m-d");
        $dbres2=$this->db->prepare("insert into service_provder_wallet_transactions (service_provider_id ,amount,type,amount_type,withdrawal_id ,date) values (:uid,:request_amount,'debit','withdraw',:wid,:date)") ;
        $dbres2->bindParam(":uid",$this->user_id);
        $dbres2->bindParam(":request_amount",$amount);
        $dbres2->bindParam(":wid",$wid);
        $dbres2->bindParam(":date",$date);
        $dbres2->execute();

        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully amount withdraw requested."));
        redirect($referer);
        exit;
    }
    public function bankDetails(){
        $referer=$_SERVER['HTTP_REFERER'];
        $bank_name=$this->input->post("bank_name",true);
        $account_number=$this->input->post("account_number",true);
        $account_name=$this->input->post("account_name",true);
        $ifsc=$this->input->post("ifsc",true);

        $dbres=$this->db->prepare("select * from service_provider_banks where service_provider_id=:uid");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->execute();
        $count=$dbres->rowCount();

        if($count>0){
            //exist
            $dbres=$this->db->prepare("update service_provider_banks set bank_name=:bank_name,account_name=:account_name,account_number=:account_number,ifsc_code=:ifsc_code where service_provider_id=:uid");
            $dbres->bindParam(":uid",$this->user_id);
            $dbres->bindParam(":bank_name",$bank_name);
            $dbres->bindParam(":account_name",$account_name);
            $dbres->bindParam(":account_number",$account_number);
            $dbres->bindParam(":ifsc_code",$ifsc);
            $dbres->execute();

        }else{
            //new one

            $dbres=$this->db->prepare("insert into service_provider_banks (bank_name,service_provider_id,account_name,account_number,ifsc_code) values (:bank_name,:uid,:account_name,:account_number,:ifsc_code)");
            $dbres->bindParam(":uid",$this->user_id);
            $dbres->bindParam(":bank_name",$bank_name);
            $dbres->bindParam(":account_name",$account_name);
            $dbres->bindParam(":account_number",$account_number);
            $dbres->bindParam(":ifsc_code",$ifsc);
            $dbres->execute();
        }
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Bank details updated"));
        redirect($referer);
        exit;
    }
    public function getBankDetails(){
        $dbres=$this->db->prepare("select * from service_provider_banks where service_provider_id=:uid");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->execute();
        return $dbres->fetch();
    }
}