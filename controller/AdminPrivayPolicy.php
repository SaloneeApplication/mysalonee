<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminPrivayPolicy extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        // if($this->admin_id==''){
        //     redirect("index.php");
        //     exit;
        // }
        // if($this->auth_type!='admin'){
        //     session_destroy();
        //     redirect("index.php");
        //     exit;
        // }
        // if(!$this->getAdminPageAccess("city")){
        //     $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
        //     redirect("dashboard.php");
        //     exit;
        // }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addNewPrivacy'){
                $this->addNewPrivacy();
            }elseif($type=='updatePrivacy'){
                $this->updatePrivacy();
            }elseif($type=='disableprivacy'){
                $this->disablePrivacy();
            }elseif($type=='enableprivacy'){
                $this->enablePrivacy();
            }elseif($type=='citybycountry'){
                $this->getCitiesByCountry();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function enablePrivacy(){
        $privacy_id=$this->input->post("privacy_id",true);
        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update privacy_policy set status=1,created_at=:datetime where id=:privacy_id");
        $up->bindParam(":privacy_id",$privacy_id);
        $up->bindParam(":datetime",$date);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully privacy policy & FAQ Enabled"));
       
        echo json_encode(array("status"=>"1","msg"=>"success"));
        exit;
    }
    public function disablePrivacy(){
        $privacy_id=$this->input->post("privacy_id",true);
        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update privacy_policy set status=0,created_at=:datetime where id=:privacy_id");
        $up->bindParam(":privacy_id",$privacy_id);
        $up->bindParam(":datetime",$date);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully privacy policy & FAQ Disabled"));
        echo json_encode(array("status"=>"1","msg"=>"success"));
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Privacy Policy FAQ';
        $data['page']='privacy_policy';
        $data['admin_id']=$this->admin_id;
        return $data;
    }

    public function updatePrivacy(){
        $referer=$_SERVER['HTTP_REFERER'];
        $privacy_id=$this->input->post("privacy_id",true);
        $country_id=$this->input->post("country_id",true);
        $language=$this->input->post("language",true);
        $terms_condition=$this->input->post("terms_condition",true);
        $privacy_policy=$this->input->post("privacy_policy",true);
        $faq=$this->input->post("faq",true);

        // $check=$this->db->prepare("select * from privacy_policy where terms_condition=:terms_condition");
        // $check->bindParam(":terms_condition",$terms_condition);
        // $check->execute();
        // if($check->rowCount()>0){
        //     $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Policy Exist"));
        //     redirect($referer);
        //     exit;
        // }

        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update privacy_policy set country_id=:country_id,language=:language,terms_condition=:terms_condition,privacy_policy=:privacy_policy,faq=:faq,created_at=:datetime where id=:id");
        $up->bindParam(":id",$privacy_id);
        $up->bindParam(":country_id",$country_id);
        $up->bindParam(":language",$language);
        $up->bindParam(":terms_condition",$terms_condition);
        $up->bindParam(":privacy_policy",$privacy_policy);
        $up->bindParam(":datetime",$date);
        $up->bindParam(":faq",$faq);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Privacy Policy updated"));
        redirect($referer);
        exit;
    }

    public function addNewPrivacy(){
        $referer=$_SERVER['HTTP_REFERER'];
        $country_id=$this->input->post("country_id",true);
        $language=$this->input->post("language",true);
        $terms_condition=$this->input->post("terms_condition",true);
        $privacy_policy=$this->input->post("privacy_policy",true);
        $faq=$this->input->post("faq",true);

        $check=$this->db->prepare("select * from privacy_policy where terms_condition=:terms_condition");
        $check->bindParam(":terms_condition",$terms_condition);
        $check->execute();
        if($check->rowCount()>0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Policy Exist"));
            redirect($referer);
            exit;
        }

        $cate=$this->db->prepare("insert into privacy_policy (country_id,language,terms_condition,privacy_policy,faq,status) values (:country_id,:language,:terms_condition,:privacy_policy,:faq,1)");
        $cate->bindParam(":country_id",$country_id);
        $cate->bindParam(":language",$language);
        $cate->bindParam(":terms_condition",$terms_condition);
        $cate->bindParam(":privacy_policy",$privacy_policy);
        $cate->bindParam(":faq",$faq);
        $cate->execute();

        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully new Privacy Policy Added"));
        redirect($referer);
        exit;
    }

    public function getPrivacy(){

        $dbres=$this->db->prepare("select c.*, (select country_name from service_countries where service_countries.id=c.country_id)country_name from privacy_policy as c order by c.created_at desc");
        $dbres->execute();
        // var_dump($dbres->fetch());die;
        return $dbres;
    }
}