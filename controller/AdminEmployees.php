<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminEmployees extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("employees")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function load(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing

            $type=$this->input->post("type",true);
            if($type=='addAdminEmployee'){
                $this->addAdminEmployee();
            }elseif($type=='addNewRole'){
                $this->addNewRole();
            }elseif($type=='updateRole'){
                $this->updateRole();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }elseif($type=='send_password'){
                $this->sendUserPassword();
            }elseif($type=='active_user1'){
                $this->activateUser1();
            }elseif($type=='block_user1'){
                $this->deactivateUser2();
            }
        }
    
    }

    public function updateRole(){
        $referer=$_SERVER['HTTP_REFERER'];
        $access_pages=['dashboard',
        'users',
        'service_provider',
        'salespersons',
        'employees',
        'service_bookings',
        'category',
        'promo_codes',
        'advertisement',
        'featured_profiles',
        'products',
        'orders',
        'product_orders',
        'subscription',
        'ratings',
        'services_reports',
        'service_provider_reports',
        'user_reports',
        'product_reports',
        'revenue_reports',
        'support_tickets',
        'settings',
        ];
        $name=$this->input->post("name",true);
        $role_id=$this->input->post("role_id",true);

        $check=$this->db->prepare("select role_id from roles where role_name=:name and role_id!=:role_id");
        $check->bindParam(":name",$name);
        $check->bindParam(":role_id",$role_id);
        $check->execute();
        if($check->rowCount()>0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Exist Role"));
            redirect($referer);
            exit;
        }
        $permission=array();
        foreach($access_pages as $ac){
            if(isset($_POST[$ac])){
                if($_POST[$ac]=='yes'){
                    $permission[]=$ac;
                }
            }
        }
        $permission=json_encode($permission);
        $dbres=$this->db->prepare("update roles set role_access=:role_access,role_name=:role_name where role_id=:role_id");
        $dbres->bindParam(":role_id",$role_id);
        $dbres->bindParam(":role_name",$name);
        $dbres->bindParam(":role_access",$permission);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>" Role Updated"));
        redirect($referer);
        exit;


    }
    public function addNewRole(){
        $referer=$_SERVER['HTTP_REFERER'];
        $access_pages=['dashboard',
        'users',
        'service_provider',
        'salespersons',
        'employees',
        'service_bookings',
        'category',
        'promo_codes',
        'advertisement',
        'featured_profiles',
        'products',
        'orders',
        'product_orders',
        'subscription',
        'ratings',
        'services_reports',
        'service_provider_reports',
        'user_reports',
        'product_reports',
        'revenue_reports',
        'support_tickets',
        'settings',
        ];

        $name=$this->input->post("name",true);

        $check=$this->db->prepare("select role_id from roles where role_name=:name");
        $check->bindParam(":name",$name);
        $check->execute();
        if($check->rowCount()>0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Exist Role"));
            redirect($referer);
            exit;
        }

        $permission=array();
        foreach($access_pages as $ac){
            if(isset($_POST[$ac])){
                if($_POST[$ac]=='yes'){
                    $permission[]=$ac;
                }
            }
        }
        $permission=json_encode($permission);
        $dbres=$this->db->prepare("insert into roles (role_name,role_access) values (:role_name,:role_access)");
        $dbres->bindParam(":role_name",$name);
        $dbres->bindParam(":role_access",$permission);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"New Role Added"));
        redirect($referer);
        exit;

    }
    public function activateUser(){
        $id=$this->input->get("id",true);
        if($id==1){
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Invalid User ID"));
            header("location:".admin_base_url."employees.php");
            exit;
        }
        $dbres=$this->db->prepare("select * from admin where admin_id=:id");
        $dbres->bindParam(":id",$id);
        $dbres->execute();
        $user=$dbres->fetch();
        $country_code=$user['country_code'];
        $mobile=$user['mobile'];
        $email=$user['email'];
        $sms_msg="Your salonee employee account activated";
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        
        $message='';
        $message.='Hi  <br/>';
        $message.='Your account activated <br/>';
        $message.='<br/>Thanking you <br> Salonee';
        $this->phpmailer->sendMail($email,"Thanking you | Salonee",$message);


        $dbres=$this->db->prepare("update admin set status=1 where admin_id=:id");
        $dbres->bindParam(":id",$id);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Employee Activated"));
        header("location:".admin_base_url."employees.php");
        exit;

    }
    public function deactivateUser(){
        $id=$this->input->get("id",true);
        if($id==1){
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Invalid User ID"));
            header("location:".admin_base_url."employees.php");
            exit;
        }
        $dbres=$this->db->prepare("update admin set status=2 where admin_id=:id");
        $dbres->bindParam(":id",$id);
        $dbres->execute();

        $dbres=$this->db->prepare("select * from admin where admin_id=:id");
        $dbres->bindParam(":id",$id);
        $dbres->execute();
        $user=$dbres->fetch();
        $country_code=$user['country_code'];
        $mobile=$user['mobile'];
        $email=$user['email'];
        $sms_msg="Your salonee employee account has been deactived";
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);
        
        $message='';
        $message.='Hi  <br/>';
        $message.='Your account blocked. please contact us. <br/>';
        $message.='<br/>Thanking you <br> Salonee';
        $this->phpmailer->sendMail($email,"Thanking you | Salonee",$message);


        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Employee Blocked "));
        header("location:".admin_base_url."employees.php");
        exit;
    }
    public function getCities(){
        $cities=$this->db->prepare("select * from cities where country_id=784 order by city_name_en asc");
        $cities->execute();
        $resp=array();
        while($row=$cities->fetch()){
            $resp[]=$row;
        }
        return $resp;
    }

    public function addAdminEmployee(){
        $name=$this->input->post("name",true);
        $email=$this->input->post("email",true);
        $role=$this->input->post("role",true);
        $mobile=$this->input->post("mobile",true);
        if($email==''){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Required"));
            header("location:".admin_base_url."employees.php");
            exit;
        }
        $che=$this->db->prepare("select email from admin where email=:email");
        $che->bindParam(":email",$email);
        $che->execute();
        $count=$che->rowCount();
        if($count>0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Exist"));
            header("location:".admin_base_url."employees.php");
            exit;
        }

        $che=$this->db->prepare("select email from salesperson where email=:email");
        $che->bindParam(":email",$email);
        $che->execute();
        $count=$che->rowCount();
        if($count>0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Exist"));
            header("location:".admin_base_url."employees.php");
            exit;
        }


        if($role==1){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Invalid User Role"));
            header("location:".admin_base_url."employees.php");
            exit;
        }

        if(strpos($email,"@")==false){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Invalid Email"));
            header("location:".admin_base_url."employees.php");
            exit;
        }
        $che=$this->db->prepare("select mobile from admin where mobile=:mobile");
        $che->bindParam(":mobile",$mobile);
        $che->execute();
        $count=$che->rowCount();
        if($count>0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Mobile Number Exist"));
            header("location:".admin_base_url."employees.php");
            exit;
        }
        if($_FILES["image"]["size"]>0){
            $tmp_name = $_FILES["image"]["tmp_name"];
            $file='uploads/admin/'.basename($_FILES["image"]["name"]);
            $path='uploads/admin/'.basename($_FILES["image"]["name"]);
            move_uploaded_file($tmp_name,$path);
        }else{
            $file='uploads/admin/default-image.png';
        }
     
        $country_code=971;
        $password=$this->randomPassword(6);
        $dbres=$this->db->prepare("insert into admin (name,email,country_code,mobile,password,user_role,image,status) values (:name,:email,:country_code,:mobile,:password,:role,:image,1)");
        $dbres->bindParam(":name",$name);
        $dbres->bindParam(":email",$email);
        $dbres->bindParam(":mobile",$mobile);
        $dbres->bindParam(":password",$password);
        $dbres->bindParam(":role",$role);
        $dbres->bindParam(":image",$file);
        $dbres->bindParam(":country_code",$country_code);
        $dbres->execute();

        $sms_msg="Your salonee employee account created. your login details ".$email." and password is ".$password."";
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $message='';
        $message.='Hi Welcome to Salonee <br/>';
        $message.='Thanking you for admin access account created. <br/>';
        $message.='Your login details : <br/>';
        $message.='Email : '.$email.'<br/>';
        $message.='Password : '.$password.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Thanking you | Salonee",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully employee added"));
        header("location:".admin_base_url."employees.php");
        exit;
    }
    public function getSalesPersons(){
        $persons=$this->db->prepare("select (select count(id) from salespersons_service_providers where salesperson_id=s.salesperson_id) as total_services_providers_count,s.*,if(s.location!='',(select city_name_en from cities where id=s.location),'') as city_name from salesperson s order by total_services_providers_count desc");
        $persons->execute();
        $resp=array();
        while($row=$persons->fetch()){
            $resp[]=$row;
        }
        return $resp;
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Employees';
        $data['page']='employees';
        $data['admin_id']=$this->admin_id;
        return $data;
    }
    public function getEmployeeRolePage(){
        $data=array();
        $data['title']='Admin | Employee Roles';
        $data['page']='employees';
        $data['admin_id']=$this->admin_id;
        return $data;
    }
    public function pageViewSalespersonData(){
        $data=array();
        $data['title']='Admin | View Sales Person ';
        $data['page']='salespersons';
        $data['admin_id']=$this->admin_id;
        $uid=$this->input->get("id",true);

        $total_sb=$this->db->prepare("select s.id from salespersons_service_providers s ,service_provider sp where s.service_provider_id=sp.service_provider_id and salesperson_id=:uid and sp.status=1");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['active_sp']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select s.id from salespersons_service_providers s ,service_provider sp where s.service_provider_id=sp.service_provider_id and salesperson_id=:uid ");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['total_sp']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select s.id from salespersons_service_providers s ,service_provider sp where s.service_provider_id=sp.service_provider_id and salesperson_id=:uid and sp.status=0");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['pending_sp']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select s.id from salespersons_service_providers s ,service_provider sp where s.service_provider_id=sp.service_provider_id and salesperson_id=:uid and sp.status=3");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['rejected_sp']=$total_sb->rowCount();

        return $data;
    }

    
    public function sendUserPassword(){
        $cid=$this->input->get("id",true);
        if($id==1){
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Invalid User ID"));
            header("location:".admin_base_url."employees.php");
            exit;
        }
        $che=$this->db->prepare("select * from admin where admin_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();
        $password=$user['password'];

        $country_code=$user['country_code'];
        $mobile=$user['mobile'];
        $sms_msg="Your salonee employee login  password is ".$password."";
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account login password is '.$password.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account login password.",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Employee password has been send to registered email."));
        redirect(admin_base_url.'employees.php');
        exit;
    }
    public function activateUser1(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("id",true);
        if(strpos($referer,"view-salesperson")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$cid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $dbres=$this->db->prepare("update salesperson set status=1 where salesperson_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully salesperson activated "));
        $che=$this->db->prepare("select * from salesperson where salesperson_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been activated.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account resumed.",$message);
        redirect($referer);
        exit;
    }
    public function deactivateUser2(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("id",true);
        if(strpos($referer,"view-salesperson")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$cid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $dbres=$this->db->prepare("update salesperson set status=2 where salesperson_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Salesperson deactivated "));
        $che=$this->db->prepare("select * from salesperson where salesperson_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();
        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been blocked. please contact us.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account blocked.",$message);
        redirect($referer);
        exit;
    }
    public function getEmployees(){
        $dbres=$this->db->prepare("select a.*,r.role_name from admin a,roles r where  a.user_role=r.role_id and a.user_role!=1");
        $dbres->execute();
        return $dbres->fetchAll();
    }
    public function getRoles(){
        $dbres=$this->db->prepare("select * from roles ");
        $dbres->execute();
        return $dbres->fetchAll();
    }

}