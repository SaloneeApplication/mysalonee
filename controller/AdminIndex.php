<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminIndex extends Controller{
    public function __construct(){
        parent::__construct();
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='forgotpassword'){
                $this->forgotpassword();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
           
        }
    }
    public function forgotpassword(){
        $email=$this->input->post("email",true);

        $check=$this->db->prepare("select admin_id,email,password from admin where email=:email");
        $check->bindParam(":email",$email);
        $check->execute();
        if($check->rowCount()==0){
            $check=$this->db->prepare("select salesperson_id,email,password from salesperson where email=:email");
            $check->bindParam(":email",$email);
            $check->execute();
            if($check->rowCount()==0){
                $_SESSION['message'] = 'Please enter valid email address';	
                header("location:index.php");
                exit;
            }
        }

        $user=$check->fetch();
        $email=$user['email'];
        $password=$user['password'];
           
        $message='';
        $message.='Hi '.ucwords($user['name']).'<br/>';
        $message.='Your login password is  '.$password.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($user['email'],"Forgot Password ! Salonee",$message);
        $_SESSION['success'] = 'Your password has been sent to your registered email.';	
        header("location:index.php");
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Dashboard';
        $data['page']='dashboard';
        $data['user_id']=$this->user_id;
        return $data;
    }
}