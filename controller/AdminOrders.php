<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminOrders extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("orders")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }elseif($type=='refundOrderID'){
                $this->refundOrderID();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }elseif($type=='approve_ad'){
                $this->approveAd();
            }
        }
    }
    
    public function refundOrderID(){
        $referer=$_SERVER['HTTP_REFERER'];

        $oid=$this->input->post("id",true);
        $refund_amount=$this->input->post("refund_amount",true);
        $refund_deductions=$this->input->post("refund_deductions",true);

        
        $od=$this->db->prepare("select * from orders where order_id=:id and user_type='user'");
        $od->bindParam(":id",$oid);
        $od->execute();
        $od=$od->fetch();

        $order_type=$od['order_type'];
        $order_id=$od['order_id'];

        $che=$this->db->prepare("select * from user where user_id=:id");
        $che->bindParam(":id",$od['user_id']);
        $che->execute();
        $user=$che->fetch();

        $up=$this->db->prepare("update orders set order_status='refunded',refunded_amount=:refunded_amount,refunded_deductions=:refunded_deductions,shipping_status='cancelled',tracking_id=NULL,remark='User Cancelled the order after payment' where order_id=:oid");
        $up->bindParam(":oid",$order_id);
        $up->bindParam(":refunded_amount",$refund_amount);
        $up->bindParam(":refunded_deductions",$refund_deductions);
        $up->execute();

        if($order_type=='slot'){
            $service_slot_id=$od['service_slot_id'];
            $up=$this->db->prepare("update service_slots set service_status='cancelled',payment_status='cancelled' where slot_id =:slot_id ");
            $up->bindParam(":slot_id ",$service_slot_id);
            $up->execute();
        }

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your Order '.$order_id.'  Refund initiated its take 7 working days to credit to your bank account<br/>
            Payment Info : <br/>
            Refunded Amount : AED '.$refund_amount.' <br/>
            Refund Deductions : AED '.$refund_deductions.'<br/>
        ';
        $message.='<br/>Thanking you';

        $this->phpmailer->sendMail($email,"Refund initiated | Salonee ",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Refund initiated "));
        redirect($referer);
        exit;

    }

    public function pageData(){
        $data=array();
        $data['title']='Admin | Orders';
        $data['page']='orders';
        $data['admin_id']=$this->admin_id;
        $data['products']=$this->getProducts();
        return $data;
    }
    public function getProducts(){
        $adv=$this->input->get("pro",true);
        if($adv=='pending'){
            $total_sb=$this->db->prepare("select o.*,(select count(id) from order_products where order_id=o.order_id) as total_products,(select name from service_provider where service_provider_id=o.service_provider_id) as service_provider_name,(select name from user where user_id=o.user_id) as user_name from orders o where  o.payment_status='pending' and o.order_status='completed'");
        }elseif($adv=='success'){
            $total_sb=$this->db->prepare("select o.*,(select count(id) from order_products where order_id=o.order_id) as total_products,(select name from service_provider where service_provider_id=o.service_provider_id) as service_provider_name,(select name from user where user_id=o.user_id) as user_name from orders o where  o.payment_status='completed'");
        }elseif($adv=='refund_pending'){
            $total_sb=$this->db->prepare("select o.*,(select count(id) from order_products where order_id=o.order_id) as total_products,(select name from service_provider where service_provider_id=o.service_provider_id) as service_provider_name,(select name from user where user_id=o.user_id) as user_name from orders o where o.payment_status='completed' and o.order_status='cancelled'");
        }elseif($adv=='refunded'){
            $total_sb=$this->db->prepare("select o.*,(select count(id) from order_products where order_id=o.order_id) as total_products,(select name from service_provider where service_provider_id=o.service_provider_id) as service_provider_name,(select name from user where user_id=o.user_id) as user_name from orders o where o.payment_status='completed' and o.order_status='refunded'");
        }elseif($adv=='failed'){
            $total_sb=$this->db->prepare("select o.*,(select count(id) from order_products where order_id=o.order_id) as total_products,(select name from service_provider where service_provider_id=o.service_provider_id) as service_provider_name,(select name from user where user_id=o.user_id) as user_name from orders o where o.payment_status='failed' ");
        }elseif($adv=='cancelled'){
            $total_sb=$this->db->prepare("select o.*,(select count(id) from order_products where order_id=o.order_id) as total_products,(select name from service_provider where service_provider_id=o.service_provider_id) as service_provider_name,(select name from user where user_id=o.user_id) as user_name from orders o where  (o.payment_status='failed' or o.payment_status='cancelled')");
        }else{ 
            $total_sb=$this->db->prepare("select o.*,(select count(id) from order_products where order_id=o.order_id) as total_products,(select name from service_provider where service_provider_id=o.service_provider_id) as service_provider_name,(select name from user where user_id=o.user_id) as user_name from orders o where  o.payment_status='completed'");
        }
       
        $total_sb->execute();
        return $total_sb->fetchAll();
    }
    public function getAdvertisements(){
        $adv=$this->input->get("adv",true);
        if($adv=='active'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='active' order by a.created_time desc");
        }elseif($adv=='pending'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='pending' order by a.created_time desc");
        }elseif($adv=='rejected'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='rejected' order by a.created_time desc");
        }elseif($adv=='expired'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='expired' order by a.created_time desc");
        }elseif($adv=='approved'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='approved' order by a.created_time desc");
        }else{
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a order by a.created_time desc");
        }
      
        $total_sb->execute();
        return $total_sb->fetchAll();
    }
}