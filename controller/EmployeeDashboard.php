<?php
require_once dirname(__DIR__).'/core/Controller.php';

class EmployeeDashboard extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='salesperson'){
            session_destroy();
            redirect("index.php");
            exit;
        }
    }
    public function load(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    
    }

    public function getServiceProviders(){
        $admin_id=$this->session->userdata("adminId");
        $persons=$this->db->prepare("select s.* from service_provider s where s.service_provider_id in (select service_provider_id from salespersons_service_providers where salesperson_id=:id) order by s.created_time desc");
        $persons->bindParam(":id",$admin_id);
        $persons->execute();
        $resp=array();
        while($row=$persons->fetch()){
            $resp[]=$row;
        }
        return $resp;
    }
    public function pageData(){
        $data=array();
        $data['title']='Saleperson | Dashboard';
        $data['page']='sales_person_dashboard';
        $data['admin_id']=$this->admin_id;
        $total_p=$this->db->prepare("select * from salespersons_service_providers where salesperson_id=:uid");
        $total_p->bindParam(":uid",$this->admin_id);
        $total_p->execute();
        $total_p=$total_p->rowCount();

        $data['total_p']=$total_p;

        $total_p=$this->db->prepare("select * from salespersons_service_providers s where s.salesperson_id=:uid and s.service_provider_id in (select service_provider_id from service_provider where service_provider_id=s.service_provider_id and status=1)");
        $total_p->bindParam(":uid",$this->admin_id);
        $total_p->execute();
        $total_p=$total_p->rowCount();
        $data['total_a']=$total_p;

        $total_p=$this->db->prepare("select * from salespersons_service_providers s where s.salesperson_id=:uid and s.service_provider_id in (select service_provider_id from service_provider where service_provider_id=s.service_provider_id and status=0)");
        $total_p->bindParam(":uid",$this->admin_id);
        $total_p->execute();
        $total_p=$total_p->rowCount();
        $data['total_pp']=$total_p;

        $total_p=$this->db->prepare("select * from salespersons_service_providers s where s.salesperson_id=:uid and s.service_provider_id in (select service_provider_id from service_provider where service_provider_id=s.service_provider_id and status=2)");
        $total_p->bindParam(":uid",$this->admin_id);
        $total_p->execute();
        $total_p=$total_p->rowCount();
        $data['total_b']=$total_p;

        return $data;
    }
    
    public function getCities(){
        $cities=$this->db->prepare("select * from cities where country_id=784 order by city_name_en asc");
        $cities->execute();
        $resp=array();
        while($row=$cities->fetch()){
            $resp[]=$row;
        }
        return $resp;
    }
}   