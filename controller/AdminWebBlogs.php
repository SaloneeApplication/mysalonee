<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminWebBlogs extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
    }
    public function load(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addBlog'){
                $this->addBlogs();
            }
            elseif($type=='editBlog'){
                $this->editBlog();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='delete_blog'){
                $this->deleteBlog();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Manage Content';
        $data['page']='Content';
        $data['admin_id']=$this->admin_id;
        return $data;
    }

    public function getBlogs()
    {
        $sql=$this->db->prepare("select * from blogs where status=1");
        $sql->execute();
        $resp=array();
        while($row=$sql->fetch()){
            $resp[]=$row;
        }
        return $resp;
    }

    public function addBlogs()
    {
        $referer=$_SERVER['HTTP_REFERER'];

        $name = $this->input->post('name');
        $short_desc = $this->input->post('short_desc');
        $desc = $this->input->post('desc');

        if($_FILES["image"]["size"]>0){
            $tmp_name = $_FILES["image"]["tmp_name"];
            $file='uploads/blog/'.basename($_FILES["image"]["name"]);
            $path='uploads/blog/'.basename($_FILES["image"]["name"]);
            move_uploaded_file($tmp_name,$path);
        }else{
            $file='uploads/blog/default-image.png';
        }

        $dbres=$this->db->prepare("INSERT INTO blogs(name, short_desc, description, image) VALUES(:name, :short_desc, :desc, :image)");
        $dbres->bindParam(":name",$name);
        $dbres->bindParam(":short_desc",$short_desc);
        $dbres->bindParam(":desc",$desc);
        $dbres->bindParam(":image",$file);
        $dbres->execute();

        //print_r($dbres->queryString); die;
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Blog added successfully"));
        redirect($referer);
        exit;
    }

    public function editBlog()
    {
        $referer=$_SERVER['HTTP_REFERER'];
        $blog_id = $this->input->post('blog_id', TRUE);

        $name = $this->input->post('name');
        $short_desc = $this->input->post('short_desc');
        $desc = $this->input->post('desc');

        if($_FILES["image"]["size"]>0){
            $tmp_name = $_FILES["image"]["tmp_name"];
            $file='uploads/blog/'.basename($_FILES["image"]["name"]);
            $path='uploads/blog/'.basename($_FILES["image"]["name"]);
            move_uploaded_file($tmp_name,$path);

            $cate=$this->db->prepare("UPDATE blogs SET name='$name', short_desc = '$short_desc', description='$desc', 
            image = '$file' WHERE blog_id = '$blog_id' ");
            $cate->execute();

        }else{

            $cate=$this->db->prepare("UPDATE blogs SET name='$name', short_desc = '$short_desc', description='$desc' 
            WHERE blog_id = '$blog_id' ");
            $cate->execute();
        }

        //print_r($cate->queryString); die;
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Blog updated successfully"));
        redirect($referer);
        exit;
    }

    public function deleteBlog()
    {
        $referer=$_SERVER['HTTP_REFERER'];
        $blog_id = $this->input->get('blog_id', TRUE); 

        $cate=$this->db->prepare("update blogs set status = 2
                                where blog_id = '$blog_id' ");
        $cate->execute();

        //print_r($cate->queryString); die;
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Blog deleted successfully"));
        redirect($referer);
        exit;
    }
}