<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminServiceBookings extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("service_bookings")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }elseif($type=='rejectSID'){
                $this->rejectSID();
            }elseif($type=='rejectEP'){
                $this->rejectEP();
            }elseif($type=='rejectAD'){
                $this->rejectAD();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='approve'){
                $this->approveSID();
            }elseif($type=='activate'){
                $this->activateSID();
            }elseif($type=='block'){
                $this->blockSID();
            }elseif($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }elseif($type=='send_password'){
                $this->sendUserPassword();
            }elseif($type=='approve_early'){
                $this->approvePayoutEarly();
            }elseif($type=='approve_ad'){
                $this->approveAd();
            }

            
        }
    }

    public function pageData(){
        $data=array();
        $data['title']='Admin | Service Bookings';
        $data['page']='services_bookings';
        $data['admin_id']=$this->admin_id;
        $data['service_bookings']=$this->getServiceBookings();
        return $data;
    }

    public function getServiceBookings(){
        $status=$this->input->get("slots",true);
        if($status=='pending'){
            $total_sb=$this->db->prepare("select s.service_type,o.*,s.service_status,s.slot_date,s.service_provider_id,sp.name,if(sp.city!='',(select city_name_en from cities where id=sp.city),'') as city_name,if(s.service_id!='',(select service_name from service_provider_services where service_provider_service_id=s.service_id),'') as service_name,if(s.user_id!='',(select name from user where user_id=s.user_id),'') as user_name from orders o,service_slots s,service_provider sp where o.service_slot_id=s.slot_id and s.service_provider_id=sp.service_provider_id and o.order_type='slot' and s.service_status='pending' and o.payment_status='completed'");
        }elseif($status=='completed'){
            $total_sb=$this->db->prepare("select s.service_type,o.*,s.service_status,s.slot_date,s.service_provider_id,sp.name,if(sp.city!='',(select city_name_en from cities where id=sp.city),'') as city_name,if(s.service_id!='',(select service_name from service_provider_services where service_provider_service_id=s.service_id),'') as service_name,if(s.user_id!='',(select name from user where user_id=s.user_id),'') as user_name from orders o,service_slots s,service_provider sp where o.service_slot_id=s.slot_id and s.service_provider_id=sp.service_provider_id and o.order_type='slot' and s.service_status='completed' and o.payment_status='completed'");
        }elseif($status=='cancelled'){
            $total_sb=$this->db->prepare("select s.service_type,o.*,s.service_status,s.slot_date,s.service_provider_id,sp.name,if(sp.city!='',(select city_name_en from cities where id=sp.city),'') as city_name,if(s.service_id!='',(select service_name from service_provider_services where service_provider_service_id=s.service_id),'') as service_name,if(s.user_id!='',(select name from user where user_id=s.user_id),'') as user_name from orders o,service_slots s,service_provider sp where o.service_slot_id=s.slot_id and s.service_provider_id=sp.service_provider_id and o.order_type='slot' and s.service_status='cancelled' and o.payment_status='completed'");
        }elseif($status=='rescheduled'){
            $total_sb=$this->db->prepare("select s.service_type,o.*,s.service_status,s.slot_date,s.service_provider_id,sp.name,if(sp.city!='',(select city_name_en from cities where id=sp.city),'') as city_name,if(s.service_id!='',(select service_name from service_provider_services where service_provider_service_id=s.service_id),'') as service_name,if(s.user_id!='',(select name from user where user_id=s.user_id),'') as user_name from orders o,service_slots s,service_provider sp where o.service_slot_id=s.slot_id and s.service_provider_id=sp.service_provider_id and o.order_type='slot' and s.booking_type='rescheduled' and o.payment_status='completed'");
        }else{
            $total_sb=$this->db->prepare("select s.service_type,o.*,s.service_status,s.slot_date,s.service_provider_id,sp.name,if(sp.city!='',(select city_name_en from cities where id=sp.city),'') as city_name,if(s.service_id!='',(select service_name from service_provider_services where service_provider_service_id=s.service_id),'') as service_name,if(s.user_id!='',(select name from user where user_id=s.user_id),'') as user_name from orders o,service_slots s,service_provider sp where o.service_slot_id=s.slot_id and s.service_provider_id=sp.service_provider_id and o.order_type='slot' and o.payment_status='completed'");
        }
        
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        // echo json_encode($this->db->errorInfo());
        return $total_sb->fetchAll();
    }

}