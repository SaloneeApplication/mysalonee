<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminServiceProviders extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("service_provider")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }elseif($type=='rejectSID'){
                $this->rejectSID();
            }elseif($type=='rejectEP'){
                $this->rejectEP();
            }elseif($type=='rejectAD'){
                $this->rejectAD();
            }elseif($type=='walletAction'){
                $this->walletAction();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='approve'){
                $this->approveSID();
            }elseif($type=='activate'){
                $this->activateSID();
            }elseif($type=='block'){
                $this->blockSID();
            }elseif($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }elseif($type=='send_password'){
                $this->sendUserPassword();
            }elseif($type=='approve_early'){
                $this->approvePayoutEarly();
            }elseif($type=='approve_ad'){
                $this->approveAd();
            }

            
        }
    }

    public function walletAction(){
        $referer=$_SERVER['HTTP_REFERER'];
        $id=$this->input->post("id",true);
        $p_amount=$this->input->post("amount",true);
        $action_type=$this->input->post("action_type",true);
        $remark=$this->input->post("remark",true);
        if(strpos($referer,"view-service-provider")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$id;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }

         //amount update

 
         $dbres=$this->db->prepare("select amount from service_provider_wallet where service_provider_id=:uid");
         $dbres->bindParam(":uid",$id);
         $dbres->execute();
         $dbres=$dbres->fetch();
         $now= $dbres['amount'];
 
         $amount=$p_amount;
         $type="";
         if($action_type=='credit'){
            $now=$now+$p_amount;
            $type="credit";
            
         }else{
            $now=$now-$p_amount;
            $type="debit";
         }
 
         $dbres1=$this->db->prepare("update service_provider_wallet set amount=:now where service_provider_id=:uid");
         $dbres1->bindParam(":uid",$id);
         $dbres1->bindParam(":now",$now);
         $dbres1->execute();
         $date=date("Y-m-d");
         $dbres2=$this->db->prepare("insert into service_provder_wallet_transactions (service_provider_id ,amount,type,amount_type ,date,remark) values (:uid,:request_amount,'$type','penalty',:date,:remark)") ;
         $dbres2->bindParam(":uid",$id);
         $dbres2->bindParam(":request_amount",$amount);
         $dbres2->bindParam(":date",$date);
         $dbres2->bindParam(":remark",$remark);
         $dbres2->execute();
         //end amount update
         $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully wallet action completed"));

         $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
         $che->bindParam(":id",$id);
         $che->execute();
         $user=$che->fetch();
         $email=$user['email'];
         $name=$user['name'];

         $sms_msg="Your wallet balance  ".$type."ed and now balance  AED ".$now."";
         $mobile=$user['mobile'];
         $country_code=$user['country_code'];
         $phone=$country_code.$mobile;
         $this->sendSMS($phone,$sms_msg);
         
         $message='';
         $message.='Hi '.$name.'<br/>';
         $message.='Your wallet updated. 
         Amount has been '.$type.'ed  your wallet. now your wallet balance is AED'.$now.'.<br/>';
         $message.='<br/>Thanking you';
         $this->phpmailer->sendMail($email,"Your Wallet Update | Salonee.",$message);

        redirect($referer);
        exit;

    }
    public function approveAd(){
        $referer=$_SERVER['HTTP_REFERER'];
        $id=$this->input->get("id",true);
        $sid=$this->input->get("sid",true);
        if(strpos($referer,"view-service-provider")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$sid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $date=date("Y-m-d");
        $dbres=$this->db->prepare("update advertisements set status='disable',approve_status='approved' where service_provider_id=:sid and advertisement_id=:id");
        $dbres->bindParam(":sid",$sid);
        $dbres->bindParam(":id",$id);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Advertisement Approved"));
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $sms_msg="Your advertisement request approved.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);


        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your Advertisement request approved.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your Advertisement Approved | Salonee.",$message);
        redirect($referer);
        exit;
    }
    public function rejectAD(){
        $referer=$_SERVER['HTTP_REFERER'];
        $id=$this->input->post("id",true);
        $sid=$this->input->post("sid",true);
        $reason=$this->input->post("reason",true);
        if(strpos($referer,"view-service-provider")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$sid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $date=date("Y-m-d");
        $dbres=$this->db->prepare("update advertisements set approve_status='rejected',status='disable',remark=:reason where service_provider_id=:sid and advertisement_id=:id");
        $dbres->bindParam(":sid",$sid);
        $dbres->bindParam(":id",$id);
        $dbres->bindParam(":reason",$reason);
        $dbres->execute();
        // echo json_encode($dbres->errorInfo());exit;
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Advertisement Rejected"));
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $sms_msg="Your advertisement request rejected.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your advertiment rejected due to <strong> '.$reason.' </strong>. please contact us. <br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your Advertisement Rejected | Salonee.",$message);
        redirect($referer);
        exit;
    }
    
    public function approvePayoutEarly(){
        $referer=$_SERVER['HTTP_REFERER'];
        $id=$this->input->get("id",true);
        $sid=$this->input->get("sid",true);
        if(strpos($referer,"view-service-provider")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$sid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $date=date("Y-m-d");
        $dbres=$this->db->prepare("update service_provider_withdrawals set status='approved',approved_date=:adate where service_provider_id=:sid and id=:id");
        $dbres->bindParam(":sid",$sid);
        $dbres->bindParam(":id",$id);
        $dbres->bindParam(":adate",$date);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Early Payout Approved"));
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $sms_msg="Your early payout request approved.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your early payout request approved.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your Payout Request Approved | Salonee.",$message);
        redirect($referer);
        exit;
    }
    public function rejectEP(){
        $referer=$_SERVER['HTTP_REFERER'];
        $id=$this->input->post("id",true);
        $sid=$this->input->post("sid",true);
        $reason=$this->input->post("reason",true);
        if(strpos($referer,"view-service-provider")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$sid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $date=date("Y-m-d");
        $dbres=$this->db->prepare("update service_provider_withdrawals set status='rejected',remark=:reason,rejected_date=:rdate where service_provider_id=:sid and id=:id");
        $dbres->bindParam(":sid",$sid);
        $dbres->bindParam(":id",$id);
        $dbres->bindParam(":rdate",$date);
        $dbres->bindParam(":reason",$reason);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Early Payout Rejected"));
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $sms_msg="Your early payout request got rejected.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your early payout rejected.please contact us for more information.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your Payout Request Rejected | Salonee.",$message);
        redirect($referer);
        exit;
    }
    
    public function sendUserPassword(){
        $cid=$this->input->get("id",true);
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();
        $email=$user['email'];
        $name=$user['name'];
        $password=$user['password'];

        $sms_msg="Your account login password is ".$password." sent from admin";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account login password is '.$password.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account login password.",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully User password has been to registered email."));
        redirect(admin_base_url.'view-service-provider.php?id='.$cid);
        exit;
    }
    public function activateUser(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("id",true);
        if(strpos($referer,"view-service-provider")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$cid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $dbres=$this->db->prepare("update service_provider set status=1 where service_provider_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully service provider activated "));
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();
        $email=$user['email'];
        $name=$user['name'];

        $sms_msg="Your salonee account activated.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);


        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been activated.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account resumed.",$message);
        redirect($referer);
        exit;
    }
    public function deactivateUser(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("id",true);
        if(strpos($referer,"view-service-provider")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$cid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $dbres=$this->db->prepare("update service_provider set status=2 where service_provider_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully User deactivated "));
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $sms_msg="Your salonee account blocked. please contact us.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);



        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been blocked. please contact us.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account blocked.",$message);
        redirect($referer);
        exit;
    }

    public function blockSID(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("id",true);
        $dbres=$this->db->prepare("update service_provider set status=2 where service_provider_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Service Provider blocked "));
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $sms_msg="Your salonee account blocked. please contact us.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been blocked. please contact us.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account blocked.",$message);
        redirect($referer);
        exit;
    }
    public function activateSID(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("id",true);
        $dbres=$this->db->prepare("update service_provider set status=1 where service_provider_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Service Provider activated "));
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();
        $email=$user['email'];
        $name=$user['name'];

        $sms_msg="Your salonee account has been actived.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been activated.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account resumed.",$message);
        redirect($referer);
        exit;
    }
    public function rejectSID(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->post("id",true);
        $desc=$this->input->post("desc",true);

        $dbres=$this->db->prepare("update service_provider set status=3,remark=:desc where service_provider_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->bindParam(":desc",$desc);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Service provider rejected"));

        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $sms_msg="Your salonee account has been rejected. please contact us.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);


        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been rejected.<br/>';
        $message.='Reason : '.$desc.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Rejected | Salonee",$message);
        redirect($referer);
        exit;
    }
    public function approveSID(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("id",true);
        $dbres=$this->db->prepare("update service_provider set status=1 where service_provider_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Service Provider Approved "));
     
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $sms_msg="Your salonee account has been actived.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been activated.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Thank you for register us",$message);
        redirect($referer);
        exit;
    }

    public function pageData(){
        $data=array();
        $data['title']='Admin | Service Providers';
        $data['page']='service-provider';
        $data['admin_id']=$this->admin_id;
        $data['service_providers']=$this->getServiceProviders();
        return $data;
    }

    public function getServiceProviders(){
        $search_type=$this->input->post("search_type",true);
        $search=$this->input->post("search",true);
        $where='';
        if($search_type=='by_email'){
            $where=' where s.email=:search ';
        }elseif($search_type=='by_phone'){
            $where=' where s.mobile=:search ';
        }elseif($search_type=='by_customerid'){
            $where=' where s.user_id=:search ';
        }

        $dbres=$this->db->prepare("select * from service_provider s $where  ORDER BY s.service_provider_id DESC");
        if($search_type!=''){
            $dbres->bindParam(":search",$search);
        }
        $dbres->execute();
        return $dbres;
    }
    
    public function pageViewUserData(){
        if($this->input->get("id",true)==''){
            header("location:".admin_base_url."users.php");
            exit;
        }
        $uid=$this->input->get("id",true);
        $data=array();
        $data['title']='Admin | View Service Provder';
        $data['page']='service-provider';
        $data['admin_id']=$this->admin_id;

        $total_sb=$this->db->prepare("select * from service_slots where service_provider_id=:uid and payment_status='completed' and service_status='completed'");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['total_sb']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select * from service_slots where service_provider_id=:uid and payment_status='completed' and booking_type='rescheduled'");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['total_re']=$total_sb->rowCount();
        
        $total_sb=$this->db->prepare("select order_id from orders where order_type='product' and payment_status='completed' and order_status='completed' and service_provider_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['total_p']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select sum(total_amount) as total from orders where  payment_status='completed' and order_status='on_hold' and service_provider_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $total_sb=$total_sb->fetch();
        $data['total_hold']=($total_sb['total']=='')?0:$total_sb['total'];

        $total_sb=$this->db->prepare("select amount from service_provider_wallet where service_provider_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $total_sb=$total_sb->fetch();
        $data['total_wallet']=$total_sb['amount'];

        // $on_hold=$this->db->prepare("select round(coalesce(sum(total_amount),0),1) as total_amount from orders where payment_status='completed' and order_status ='on_hold' and user_id=:uid");
        // $on_hold->bindParam(":uid",$uid);
        // $on_hold->execute();
        // $on_hold=$on_hold->fetch();
        // $data['on_hold']=$on_hold['total_amount'];

        $total_sb=$this->db->prepare("select * from service_provder_wallet_transactions where service_provider_id=:uid order by created_time desc");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['wallet_txns']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select * from service_provider_withdrawals where service_provider_id=:uid order by created_time desc");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['payouts']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select * from service_provider_withdrawals where service_provider_id=:uid and withdrawal_type='early' order by created_time desc");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['early_paytouts']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select s.service_type,o.*,s.service_status,s.slot_date,s.service_provider_id,sp.name,if(sp.city!='',(select city_name_en from cities where id=sp.city),'') as city_name,if(s.service_id!='',(select service_name from service_provider_services where service_provider_service_id=s.service_id),'') as service_name,if(s.user_id!='',(select name from user where user_id=s.user_id),'') as user_name from orders o,service_slots s,service_provider sp where o.service_slot_id=s.slot_id and s.service_provider_id=sp.service_provider_id and o.order_type='slot' and o.payment_status='completed' and s.service_provider_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        // echo json_encode($this->db->errorInfo());
        $data['slots_booked']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select o.*,(select count(id) from order_products where order_id=o.order_id) as total_products,(select name from service_provider where service_provider_id=o.service_provider_id) as service_provider_name from orders o where o.order_type='product' and o.payment_status='completed'  and o.service_provider_id=:uid");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['products']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select * from featured_profiles where service_provider_id=:uid order by created_time desc");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['featured']=$total_sb->fetchAll();

        $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.service_provider_id=:uid order by a.created_time desc");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['advertisements']=$total_sb->fetchAll();

        return $data;
    }
    public function getMainBranchInfo($id){
        $dbres=$this->db->prepare("select * from service_provider where service_provider_id in (select service_provider_main_branch_id from service_provider_branches where service_provider_id=:id)");
        $dbres->bindParam(":id",$id);
        $dbres->execute();
        return $dbres->fetch();
    }
}