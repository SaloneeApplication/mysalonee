<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserAdvertisement extends Controller{
    public $user_id;
    public $branch_type;
    public $merchant_id="45990";
    public $access_code="AVFY03IE78BG61YFGB";
    public $working_key="08D12F6F9BC71D36B0A9F9D3ECFDEF57";
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if(@$_REQUEST['type']!='payment_response'){
            if($this->user_id==''){
                session_destroy();
                redirect("index.php");
                exit;
            }
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='newAdvertisementPurchase'){
                $this->newAdvertisementPurchase();
            }elseif($type=='editAdv'){
                $this->editAdvertisement();
            }elseif($type=='updateAdvertisement'){
                $this->updateAdvertisement();
            }elseif($type=='advertisementPurchase'){
                $this->advertisementPurchase();
            }
            if(@$_GET['type']=='payment_response'){
                $this->paymentResponse();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='disableAd'){
                $this->disableAd();
            }elseif($type=='enableAd'){
                $this->enableAd();
            }
        }
    }
    public function disableAd(){
        $referer=$_SERVER['HTTP_REFERER'];
        $id=$this->input->get("id",true);
        $dbres=$this->db->prepare("update advertisements set status='disable' where advertisement_id=:fid and service_provider_id=:uid");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->bindParam(":fid",$id);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully advertisement disabled"));
        redirect($referer);
        exit;
    }
    public function enableAd(){
        $referer=$_SERVER['HTTP_REFERER'];
        $id=$this->input->get("id",true);
        $dbres=$this->db->prepare("update advertisements set status='active' where advertisement_id=:fid and service_provider_id=:uid");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->bindParam(":fid",$id);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully featured Profile enabled"));
        redirect($referer);
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Advertisement';
        $data['page']='service_provider_advertisement';
        $data['user_id']=$this->user_id;
        return $data;
    }

    public function getAdvertisements(){
        $dbres=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements as a where a.service_provider_id=:uid order by created_time desc");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->execute();
        return $dbres->fetchAll();
    }
    public function advertisementPurchase(){
        $referer=$_SERVER['HTTP_REFERER'];

        $from_date=$this->input->post("from_date",true);
        $to_date=$this->input->post("to_date",true);
        $city=$this->input->post("city",true);
        
        $name=$_FILES["upload_cont_img"]['name'];
        $tmp=explode(".",$name);
        $tmp_name = $_FILES["upload_cont_img"]["tmp_name"];
        $newfilename = round(microtime(true)) . '.' . end($tmp);

        $path='uploads/advertisements/'.$newfilename;
        move_uploaded_file($tmp_name,$path);
        $banner=$path;
        if($this->getMemberAccess()==0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Please upgrade now to access."));
            redirect($referer);
            exit;
        }
        $date1 = new DateTime($from_date);
        $date2 = new DateTime($to_date);
        $interval = $date1->diff($date2);
        $days=$interval->d+1;   
                
        $per_day=$this->getOption("advertisement_per_day");
        $commission=$this->getOption("advertisement_per_day_admin_commission");

        $total=$days*$per_day;
        $commission=$days*$commission;
        $total_amount=$commission+$total;

        $city1=$this->db->prepare("select latitude,longitude from cities where id=:cid");
        $city1->bindParam(":cid",$city);
        $city1->execute();
        $city1=$city1->fetch();
        $latitude=$city1['latitude'];
        $longitude=$city1['longitude'];
        $advdata=array();
        $advdata['from_date']=$from_date;
        $advdata['to_date']=$to_date;
        $advdata['days']=$days;
        $advdata['city']=$city;
        $advdata['latitude']=$latitude;
        $advdata['longitude']=$longitude;
        $advdata['banner']=$banner;
        $advdata=json_encode($advdata);
        $order_id="S".$this->user_id.time();
        $dbres1=$this->db->prepare("insert into orders (order_id,service_provider_id,user_id,user_type,total_amount,admin_commission,payment_status,order_type,order_status,note_dev)values(:order_id,:uid,:uid,'user',:amount,:commission,'pending','advertisement','pending',:note_dev)");
        $dbres1->bindParam(":uid",$this->user_id);
        $dbres1->bindParam(":order_id",$order_id);
        $dbres1->bindParam(":amount",$total_amount);
        $dbres1->bindParam(":commission",$commission);
        $dbres1->bindParam(":note_dev",$advdata);
        $dbres1->execute();

        $user=$this->db->prepare("select s.*,c.city_name_en,(select name from countries where id=c.country_id) as country_name from service_provider s,cities c where s.city=c.id and s.service_provider_id=:service_provider_id");
        $user->bindParam(":service_provider_id",$this->user_id);
        $user->execute();
        $user=$user->fetch();

        $encid=$order_id.'~'.$this->user_id;
        $encid=$this->encrypt($encid);
        $encid=base64_encode($encid);
        $encid=rawurlencode($encid);
        setcookie("encid", $encid, time()+30*24*60*60,"/",$_SERVER['HTTP_HOST']);

        $ccavenue=array();
        $ccavenue['merchant_id']=$this->merchant_id;
        $ccavenue['order_id']=$order_id;
        $ccavenue['amount']=$total_amount;
        $ccavenue['currency']="AED";
        $ccavenue['redirect_url']=user_base_url."advertisement.php?type=payment_response";
        $ccavenue['cancel_url']=user_base_url."advertisement.php?type=payment_response";
        $ccavenue['billing_name']=$user['name'];
        $ccavenue['billing_email']=$user['email'];
        $ccavenue['billing_address']=$user['address'];
        $ccavenue['billing_city']=$user['city_name_en'];
        $ccavenue['billing_tel']=$user['mobile'];
        $ccavenue['billing_country']=$user['country_name'];

        $merchant_data='';
        foreach ($ccavenue as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }
        $encrypted_data=$this->ccavenue_encrypt($merchant_data,$this->working_key);

        $resp=array();
        $resp['encrypted_data']=$encrypted_data;
        $resp['access_code']=$this->access_code;
        $resp['status']='200';
        ?>
        <form action="https://secure.ccavenue.ae/transaction/transaction.do?command=initiateTransaction" name="redirect" id="upgrade_now" method="post" >
  <input type="submit" style="display:none;" id="submit_button" />
  <input type=hidden name=encRequest value='<?php echo $resp['encrypted_data'];?>'>
  <input type=hidden name=access_code value='<?php echo $resp['access_code'];?>'>
</form>
<script>
    window.onload=function(){
        document.querySelector("#upgrade_now").submit();
    }
</script>
<?php 
        // $resp['action']='https://secure.ccavenue.ae/transaction/transaction.do?command=initiateTransaction';
        // echo json_encode($resp);
        exit;
    }
    
    public function paymentResponse(){
        $encResponse=$_POST["encResp"];	
        if($_SERVER['REQUEST_METHOD']!='POST'){
            $this->session->set_userdata(array("alert_type"=>"warning","alert_msg"=>"Invalid Request"));
            redirect(user_base_url."dashboard.php");
            exit;
        }		
        $rcvdString=$this->ccavenue_decrypt($encResponse,$this->working_key);		
        $order_status="";
        $decryptValues=explode('&', $rcvdString);
        $order_status=$decryptValues[3];//order_status
        $order_status=explode("=",$order_status);
        $order_status=$order_status[1];

        if($order_status=="Success"){
            //payment success
            $order_id=$decryptValues[0];//order_id
            $order_id=explode("=",$order_id);
            $order_id=$order_id[1];

            $tracking_id=$decryptValues[1];//tracking_id
            $tracking_id=explode("=",$tracking_id);
            $tracking_id=$tracking_id[1];

            $encid=@$_GET['encid'];
            $append="";
            if($encid==''){
                $encid=$_COOKIE['encid'];
            }
            $encid=rawurldecode($encid);
            $encid=base64_decode($encid);
            $encid=$this->decrypt($encid);
            $encid=explode("~",$encid);
            $user_id=$encid[1];
            $ex_order_id=$encid[0];
            if($this->user_id==''){
                $append="exusr";
                //when session expiry 
                $usr=$this->db->prepare("select * from service_provider where service_provider_id=:user_id");
                $usr->bindParam(":user_id",$user_id);
                $usr->execute();
                $usr=$usr->fetch();
                $_SESSION["serviceProviderId"] = $usr['service_provider_id'];
				$_SESSION["branch_type"] = $usr['user_type'];
                $this->user_id=$this->session->userdata("serviceProviderId");
                $this->branch_type=$this->session->userdata("branch_type");
                
            }


            $order=$this->db->prepare("select * from orders where order_id=:order_id");
            $order->bindParam(":order_id",$order_id);
            $order->execute();
            $order=$order->fetch();
            $order_note=json_decode($order['note_dev'],true);
            $from_date=$order_note['from_date'];
            $to_date=$order_note['to_date'];
            $days=$order_note['days'];
            $city=$order_note['city'];
            $latitude=$order_note['latitude'];
            $longitude=$order_note['longitude'];
            $banner=$order_note['banner'];
            $total_amount=$order['total_amount'];
            if($order['payment_status']=='completed'){
                $this->session->set_userdata(array("alert_type"=>"warning","alert_msg"=>"Invalid Request"));
                redirect(user_base_url."dashboard.php");
                exit;
            }

            $dbres=$this->db->prepare("update orders set payment_status='completed',order_status='completed',txn_id=:txn_id where order_id=:order_id");
            $dbres->bindParam(":order_id",$order_id);
            $dbres->bindParam(":txn_id",$tracking_id);
            $dbres->execute();


            $dbress=$this->db->prepare("insert into advertisements (service_provider_id,days,from_date,expiry_date,order_id,status,amount,banner,city_id,latitude,longitude,approve_status) values (:service_provider_id,:days,:from_date,:expiry_date,:order_id,'disable',:amount,:banner,:city_id,:latitude,:longitude,'pending')");
            $dbress->bindParam(":service_provider_id",$this->user_id);
            $dbress->bindParam(":days",$days);
            $dbress->bindParam(":from_date",$from_date);
            $dbress->bindParam(":expiry_date",$to_date);
            $dbress->bindParam(":order_id",$order_id);
            $dbress->bindParam(":amount",$total_amount);
            $dbress->bindParam(":banner",$banner);
            $dbress->bindParam(":city_id",$city);
            $dbress->bindParam(":latitude",$latitude);
            $dbress->bindParam(":longitude",$longitude);
            $dbress->execute();
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully advertisement created. Pending Approval"));
            redirect(user_base_url."advertisement.php?".$append);
            exit;
        }else{
            //payment failed
            $encid=@$_GET['encid'];
            $append="";
            if($encid==''){
                $encid=$_COOKIE['encid'];
            }
            $encid=rawurldecode($encid);
            $encid=base64_decode($encid);
            $encid=$this->decrypt($encid);
            $encid=explode("~",$encid);
            $user_id=$encid[1];
            $ex_order_id=$encid[0];
            if($this->user_id==''){
                $append="exusr";
                //when session expiry 
                $usr=$this->db->prepare("select * from service_provider where service_provider_id=:user_id");
                $usr->bindParam(":user_id",$user_id);
                $usr->execute();
                $usr=$usr->fetch();
                $_SESSION["serviceProviderId"] = $usr['service_provider_id'];
				$_SESSION["branch_type"] = $usr['user_type'];
                $this->user_id=$this->session->userdata("serviceProviderId");
                $this->branch_type=$this->session->userdata("branch_type");
                
            }

            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Payment Failed."));
            redirect(user_base_url."advertisement.php?".$append);
            exit;
        }
    }
    public function newAdvertisementPurchase(){
        $referer=$_SERVER['HTTP_REFERER'];
        exit;
        $from_date=$this->input->post("from_date",true);
        $to_date=$this->input->post("to_date",true);
        $city=$this->input->post("city",true);

        $tmp_name = $_FILES["upload_cont_img"]["tmp_name"];
        $newfilename = round(microtime(true)) . '.' . end($temp);

        $path='uploads/advertisements/'.$newfilename;
        move_uploaded_file($tmp_name,$path);
        $banner=$path;
        if($this->getMemberAccess()==0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Please upgrade now to access."));
            redirect($referer);
            exit;
        }
        $date1 = new DateTime($from_date);
        $date2 = new DateTime($to_date);
        $interval = $date1->diff($date2);
        $days=$interval->d+1;   
                
        $per_day=$this->getOption("advertisement_per_day");
        $commission=$this->getOption("advertisement_per_day_admin_commission");

        $total=$days*$per_day;
        $commission=$days*$commission;
        $total_amount=$commission+$total;

        $city1=$this->db->prepare("select latitude,longitude from cities where id=:cid");
        $city1->bindParam(":cid",$city);
        $city1->execute();
        $city1=$city1->fetch();
        $latitude=$city1['latitude'];
        $longitude=$city1['longitude'];
        
        $order_id="S".$this->user_id.time();
        $dbres1=$this->db->prepare("insert into orders (order_id,service_provider_id,user_id,user_type,total_amount,admin_commission,payment_status,order_type,order_status,remark)values(:order_id,:uid,:uid,'user',:amount,:commission,'completed','advertisement','completed','testing')");
        $dbres1->bindParam(":uid",$this->user_id);
        $dbres1->bindParam(":order_id",$order_id);
        $dbres1->bindParam(":amount",$total_amount);
        $dbres1->bindParam(":commission",$commission);
        $dbres1->execute();


        $dbress=$this->db->prepare("insert into advertisements (service_provider_id,days,from_date,expiry_date,order_id,status,amount,banner,city_id,latitude,longitude,approve_status) values (:service_provider_id,:days,:from_date,:expiry_date,:order_id,'disable',:amount,:banner,:city_id,:latitude,:longitude,'pending')");
        $dbress->bindParam(":service_provider_id",$this->user_id);
        $dbress->bindParam(":days",$days);
        $dbress->bindParam(":from_date",$from_date);
        $dbress->bindParam(":expiry_date",$to_date);
        $dbress->bindParam(":order_id",$order_id);
        $dbress->bindParam(":amount",$total_amount);
        $dbress->bindParam(":banner",$banner);
        $dbress->bindParam(":city_id",$city);
        $dbress->bindParam(":latitude",$latitude);
        $dbress->bindParam(":longitude",$longitude);
        $dbress->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully advertisement created. Pending Approval"));
        redirect($referer);
    }
    public function updateAdvertisement(){

        $id=$this->input->post("ad_id",true);
        $referer=$_SERVER['HTTP_REFERER'];

        $from_date=$this->input->post("from_date",true);
        $to_date=$this->input->post("to_date",true);
        $city=$this->input->post("city",true);

        $dbress=$this->db->prepare("select * from advertisements where advertisement_id=:id and service_provider_id=:uid");
        $dbress->bindParam(":id",$id);
        $dbress->bindParam(":uid",$this->user_id);
        $dbress->execute();
        $adv=$dbress->fetch();
        if(round($adv['amount'])==0 && $adv['days']>0){
            $chkdate=date("Y-m-d",strtotime("+".$adv['days']." days",strtotime($from_date)));
            if($to_date>$chkdate){
                $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Please select date for ".$adv['days'].' days only'));
                redirect($referer);
                exit;
            }
        }
        if($_FILES["upload_cont_img"]["size"]>0){
            $tmp_name = $_FILES["upload_cont_img"]["tmp_name"];
            $temp = explode(".", $_FILES["upload_cont_img"]["name"]);
            $newfilename = round(microtime(true)) . '.' . end($temp);
            $path='uploads/advertisements/'.$newfilename;
            move_uploaded_file($tmp_name,$path);
            $banner=$path; 
        }else{
            $banner=$adv['banner'];
        }

        
        $city1=$this->db->prepare("select latitude,longitude from cities where id=:cid");
        $city1->bindParam(":cid",$city);
        $city1->execute();
        $city1=$city1->fetch();
        $latitude=$city1['latitude'];
        $longitude=$city1['longitude'];

        if($adv['expiry_date']==''){
            $dbress=$this->db->prepare("update advertisements set from_date=:from_date,expiry_date=:expiry_date,banner=:banner,city_id=:city,latitude=:latitude,longitude=:longitude,approve_status='pending',status='disable' where advertisement_id=:id and service_provider_id=:uid");
            $dbress->bindParam(":from_date",$from_date);
            $dbress->bindParam(":expiry_date",$to_date);
            $dbress->bindParam(":banner",$banner);
            $dbress->bindParam(":city",$city);
            $dbress->bindParam(":id",$id);
            $dbress->bindParam(":latitude",$latitude);
            $dbress->bindParam(":longitude",$longitude);
            $dbress->bindParam(":uid",$this->user_id);
            $dbress->execute();
        }else{
            $dbress=$this->db->prepare("update advertisements set banner=:banner,city_id=:city,latitude=:latitude,longitude=:longitude,approve_status='pending',status='disable' where advertisement_id=:id and service_provider_id=:uid");
            $dbress->bindParam(":banner",$banner);
            $dbress->bindParam(":city",$city);
            $dbress->bindParam(":id",$id);
            $dbress->bindParam(":uid",$this->user_id);
            $dbress->bindParam(":latitude",$latitude);
            $dbress->bindParam(":longitude",$longitude);
            $dbress->execute();
            // echo json_encode($dbress->errorInfo());exit;
        }

        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully advertisement created. Pending Approval"));
        redirect($referer);
        exit;
    }
    public function editAdvertisement(){
        $id=$this->input->post("id",true);
        $dbress=$this->db->prepare("select * from advertisements where advertisement_id=:id and service_provider_id=:uid");
        $dbress->bindParam(":id",$id);
        $dbress->bindParam(":uid",$this->user_id);
        $dbress->execute();
        $adv=$dbress->fetch();
        ?>
        <div class="col-md-12">
                    <div class="form-group">
                      <input type="text" id="from_date1" name="from_date" <?php echo (@$adv['from_date']!='')?'disable':'';?>  value="<?php echo @$adv['from_date'];?>" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0"  for="from_date1">From Date</label>
                    </div>

                    <div class="form-group">
                      <input type="text" id="to_date1" name="to_date" <?php echo (@$adv['expiry_date']!='')?'disable':'';?>  value="<?php echo @$adv['expiry_date'];?>" class="form-control" autocomplete="off" required>
                      <label class="form-control-placeholder p-0"   for="to_date1">To Date</label>
                    </div>

                    <div class="form-group"  >
                      <select name="city" id="city" required  class="form-control" autocomplete="off" >
                        <option hidden disabled  value> -- Select City-- </option>
                        <?php foreach($this->getCitiesList() as $cit) { 
                            if($adv['city_id']==$cit['id']){
                                echo '<option value="'.$cit['id'].'" selected>'.$cit['city_name_en'].'</option>';
                            }else{
                                echo '<option value="'.$cit['id'].'">'.$cit['city_name_en'].'</option>';
                            }
                        } ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="file-upload2" class="custom-file-upload">
                        UPLOAD BANNER <i class="fa fa-camera" aria-hidden="true"></i>
                      </label>
                      <input id="file-upload2" accept="image/*" name='upload_cont_img' type="file" style="display:none;">
                    </div>
                    <?php if(round($adv['amount'])==0 && $adv['days']>0){ 
                        echo 'Note: Please select date for '.$adv['days'].' days only'; } ?>
                  </div>
    <?php
    exit;
    }
}