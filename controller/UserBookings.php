<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserBookings extends Controller{
    public $user_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }elseif($type=='slotCompleted'){
                $this->slotCompleted();
            }elseif($type=='getSlots'){
                $this->getSlots();
            }elseif($type=='rescheduleSlotTime'){
                $this->rescheduleSlotTime();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Service Bookings';
        $data['page']='service-bookings';
        $data['user_id']=$this->user_id;
        return $data;
    }
    
    public function getSaloneeTimings(){
        $dbres=$this->db->prepare("select * from service_provider_timings where service_provider_id=:uid");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->execute();
        return $dbres->fetchAll();
      }
    public function slotCompleted(){
        $sid=$this->input->post("sid",true);
        $check=$this->db->prepare("select slot_id from service_slots where slot_id=:sid and  service_provider_id=:uid");
        $check->bindParam(":uid",$this->user_id);
        $check->bindParam(":sid",$sid);
        $check->execute();
        $check=$check->rowCount();
        if($check==0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Service ID Not exist with your account contact salonee admin"));

            echo json_encode(array("status"=>"400","msg"=>"Service ID Not exist with your account contact salonee admin"));
            exit;
        }
        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update service_slots set service_status='completed' ,service_completed_on=:date where slot_id=:sid and  service_provider_id=:uid");
        $up->bindParam(":uid",$this->user_id);
        $up->bindParam(":sid",$sid);
        $up->bindParam(":date",$date);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"successfully slot completed"));
        echo json_encode(array("status"=>"200","msg"=>"successfully slot completed"));
        exit;
    }

    public function getServiceSlots(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            $search_type=$this->input->post("search_type",true);
            $search=$this->input->post("search",true);
            $search_from=$this->input->post("search_from",true);
            $search_to=$this->input->post("search_to",true);
            if($search_type=='by_orderid'){
                $s=$this->db->prepare("select s.*,u.name,sp.service_name,if(sp.category_id!='',(select name from category where category_id=sp.category_id),'') as category_name from service_slots s , user u,service_provider_services sp where s.user_id=u.user_id and s.service_id=sp.service_provider_service_id  and s.payment_status='completed' and s.service_status in ('inprocess','pending','completed') and s.service_provider_id=:uid and s.order_id=:order_id order by s.created_time desc ");
                $s->bindParam(":order_id",$search);

            }elseif($search_type=='by_customerid'){
                $s=$this->db->prepare("select s.*,u.name,sp.service_name,if(sp.category_id!='',(select name from category where category_id=sp.category_id),'') as category_name from service_slots s , user u,service_provider_services sp where s.user_id=u.user_id and s.service_id=sp.service_provider_service_id  and s.payment_status='completed' and s.service_status in ('inprocess','pending','completed') and s.service_provider_id=:uid order by s.created_time desc");
                // $s->bindParam(":user_id",$search);
            }elseif($search_type=='by_date'){
                $s=$this->db->prepare("select s.*,u.name,sp.service_name,if(sp.category_id!='',(select name from category where category_id=sp.category_id),'') as category_name from service_slots s , user u,service_provider_services sp where s.user_id=u.user_id and s.service_id=sp.service_provider_service_id  and s.payment_status='completed' and s.service_status in ('inprocess','pending','completed') and s.service_provider_id=:uid and cast(s.slot_date as date) between :search_from and :search_to order by s.created_time desc ");
                $s->bindParam(":search_from",$search_from);
                $s->bindParam(":search_to",$search_to);
            }else{
                $s=$this->db->prepare("select s.*,u.name,sp.service_name,if(sp.category_id!='',(select name from category where category_id=sp.category_id),'') as category_name from service_slots s , user u,service_provider_services sp where s.user_id=u.user_id and s.service_id=sp.service_provider_service_id  and s.payment_status='completed' and s.service_status in ('inprocess','pending','completed') and s.service_provider_id=:uid order by s.created_time desc ");
            }
          

        }else{
            $sid=$this->input->get("sid",true);
            if($sid!=''){
                $s=$this->db->prepare("select s.*,u.name,sp.service_name,if(sp.category_id!='',(select name from category where category_id=sp.category_id),'') as category_name from service_slots s , user u,service_provider_services sp where s.user_id=u.user_id and s.service_id=sp.service_provider_service_id  and s.payment_status='completed' and s.service_status in ('inprocess','pending','completed') and s.service_provider_id=:uid and s.slot_id=:sid order by s.created_time desc ");
                $s->bindParam(":sid",$sid);

            }else{
                $s=$this->db->prepare("select s.*,u.name,sp.service_name,if(sp.category_id!='',(select name from category where category_id=sp.category_id),'') as category_name from service_slots s , user u,service_provider_services sp where s.user_id=u.user_id and s.service_id=sp.service_provider_service_id  and s.payment_status='completed' and s.service_status in ('inprocess','pending','completed') and s.service_provider_id=:uid order by s.created_time desc ");

            }
        }
      
        $s->bindParam(":uid",$this->user_id);
        $s->execute();
        return $s->fetchAll();

    }
    
    public function getSlots(){
        $slot_date=$this->input->post("slot_date",true);
        $sid=$this->input->post("sid",true);

        $slot=$this->db->prepare("select s.*,sp.time_slot_freq,p.same_time_slots_allowed,p.allowed_slots_count,p.per_day_slots_limit,p.price_for from service_slots s, service_provider_services sp ,services_prices p where s.service_id=sp.service_provider_service_id and sp.service_provider_service_id=p.service_provider_service_id and s.slot_id=:sid and sp.service_provider_id=:uid  having p.price_for=s.service_type");
        $slot->bindParam(":uid",$this->user_id);
        $slot->bindParam(":sid",$sid);
        $slot->execute();
        $slot=$slot->fetch();
        $freq=$slot['time_slot_freq'];
        $service_type=$slot['service_type'];
        $service_id=$slot['service_id'];
        $same_time_slots_allowed=$slot['same_time_slots_allowed'];
        $allowed_slots_count=$slot['allowed_slots_count'];
        $per_day_slots_limit=$slot['per_day_slots_limit'];

        $day=strtolower(date("l",strtotime($slot_date)));

        $time=$this->db->prepare("select * from service_provider_timings where service_provider_id=:uid and day_name=:day_name");
        $time->bindParam(":uid",$this->user_id);
        $time->bindParam(":day_name",$day);
        $time->execute();
        $time=$time->fetch();
        $from_time=$time['from_time'];
        $to_time=$time['to_time'];
        $available_status=$time['available_status'];
        $total_minutes=(strtotime($to_time)-strtotime($from_time))/60;
        $total_slots=$total_minutes/$freq;
        
        for($i=0;$i<$total_slots;$i++){
            $time1=date("H:i A",strtotime($from_time));
            $time=date("H:i",strtotime($from_time));
            $act_check_time=$slot_date." ".date("H:i",strtotime($from_time)).":00";

            if(strtotime($act_check_time)<time()){
                //time done 
                //for booked design
                echo '<li title="salonee time over"><input type="radio"  class="booked" disabled id="radio'.$i.'" name="radios" value="" ><label for="radio'.$i.'">'.$time1.'</label></li>';
            }else{
                $count=$this->db->prepare("select slot_id from service_slots where service_id=:sid and slot_date=:slot_date and service_type=:service_type");
                $count->bindParam(":slot_date",$act_check_time);
                $count->bindParam(":sid",$service_id);
                $count->bindParam(":service_type",$service_type);
                $count->execute();
                $total_booked_slots=$count->rowCount();
                   
                if($total_booked_slots>=$per_day_slots_limit && $per_day_slots_limit!=0){
                    //per day slots booking limit over 
                    echo '<li  title="per day slots limit booked over "><input type="radio" class="booked" disabled id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                }else{
                    //if same time slot not allowed 
                    if($same_time_slots_allowed==0){
                        if($total_booked_slots>0){
                            echo '<li  title="slot booked"><input type="radio" class="booked" disabled id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                        }else{
                            echo '<li title="slot available"><input type="radio"  id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                        }
                    }else{
                        if($allowed_slots_count<$total_booked_slots){
                            //allowed limit over slots
                            echo '<li title="same time slot booking limit over"><input type="radio" class="booked" disabled id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                        }else{
                            echo '<li title="slot available"><input type="radio"  id="radio'.$i.'" name="slot_time" value="'.$time.'" ><label for="radio'.$i.'">'.$time1.'</label></li>';
                        }
                        
                    }

                }
            }

            $from_time=date("H:i",strtotime("+".$freq." minutes",strtotime($from_time)));
        }

        exit;
    }

    public function rescheduleSlotTime(){
        $sid=$this->input->post("sid",true);
        $slot_time=$this->input->post("slot_time",true);
        $slot_date=$this->input->post("slot_date",true);

        $slot=$this->db->prepare("select s.*,sp.time_slot_freq,p.same_time_slots_allowed,p.allowed_slots_count,p.per_day_slots_limit,p.price_for from service_slots s, service_provider_services sp ,services_prices p where s.service_id=sp.service_provider_service_id and sp.service_provider_service_id=p.service_provider_service_id and s.slot_id=:sid and sp.service_provider_id=:uid  having p.price_for=s.service_type");
        $slot->bindParam(":uid",$this->user_id);
        $slot->bindParam(":sid",$sid);
        $slot->execute();
        $slot=$slot->fetch();
        $freq=$slot['time_slot_freq'];
        $service_type=$slot['service_type'];
        $service_id=$slot['service_id'];
        $user_id=$slot['user_id'];
        $same_time_slots_allowed=$slot['same_time_slots_allowed'];
        $allowed_slots_count=$slot['allowed_slots_count'];
        $per_day_slots_limit=$slot['per_day_slots_limit'];

        $user=$this->db->prepare("select * from user where user_id=:user_id");
        $user->bindParam(":user_id",$user_id);
        $user->execute();
        $user=$user->fetch();

        $act_check_time=$slot_date." ".date("H:i",strtotime($slot_time)).":00";

        $count=$this->db->prepare("select slot_id from service_slots where service_id=:sid and slot_date=:slot_date and service_type=:service_type");
        $count->bindParam(":slot_date",$act_check_time);
        $count->bindParam(":sid",$service_id);
        $count->bindParam(":service_type",$service_type);
        $count->execute();
        $total_booked_slots=$count->rowCount();
        if($total_booked_slots>=$per_day_slots_limit && $per_day_slots_limit!=0){
            //per day slots booking limit over 
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Booking Slot Not Available"));
        }else{
            //if same time slot not allowed 
            if($same_time_slots_allowed==0){
                if($total_booked_slots>0){
                    $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Booking Slot Not Available"));
                }else{
                    $data=$this->db->prepare("update service_slots set slot_date=:slot_date , booking_type='rescheduled' where slot_id=:sid and service_provider_id=:uid");
                    $data->bindParam(":slot_date",$act_check_time);
                    $data->bindParam(":sid",$sid);
                    $data->bindParam(":uid",$this->user_id);
                    $data->execute();
                    $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully slot rescheduled"));
                   
                    $message='';
                    $message.='Hi '.ucwords($user['name']).'<br/>';
                    $message.='Your slot is rescheduled to '.$act_check_time.'<br/>';
                    $message.='<br/>Thanking you';
                    $this->phpmailer->sendMail($user['email'],"Salonee Slot Rescheduled.",$message);
                }
            }else{
                if($allowed_slots_count<$total_booked_slots){
                    //allowed limit over slots
                    $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Booking Slot Not Available"));
                }else{
                    $data=$this->db->prepare("update service_slots set slot_date=:slot_date , booking_type='rescheduled' where slot_id=:sid and service_provider_id=:uid");
                    $data->bindParam(":slot_date",$act_check_time);
                    $data->bindParam(":sid",$sid);
                    $data->bindParam(":uid",$this->user_id);
                    $data->execute();
                    $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully slot rescheduled"));
                    
                    $message='';
                    $message.='Hi '.ucwords($user['name']).'<br/>';
                    $message.='Your slot is rescheduled to '.$act_check_time.'<br/>';
                    $message.='<br/>Thanking you';
                    $this->phpmailer->sendMail($user['email'],"Salonee Slot Rescheduled.",$message);
                }
            }
        }
        header("location:service-bookings.php");
        exit;
    }
}