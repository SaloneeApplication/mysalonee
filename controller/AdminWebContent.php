<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminWebContent extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
    }
    public function load(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addAboutUs'){
                $this->addAboutUs();
            }
            elseif($type=='getAboutUs'){
                $this->getAboutUs();
            }
        }else{
            
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Manage Content';
        $data['page']='Content';
        $data['admin_id']=$this->admin_id;
        return $data;
    }

    public function addAboutUs()
    {
        $referer=$_SERVER['HTTP_REFERER'];
        $language_id=$this->input->post("language_id",true);
        $about_us_title=$this->input->post("about_us_title",true);
        $about_us_heading=$this->input->post("about_us_heading",true);
        $about_us=$this->input->post("about_us",true);
        $why_choose_us=$this->input->post("why_choose_us",true);
        $categories_heading=$this->input->post("categories_heading",true);
        $categories_title=$this->input->post("categories_title",true);
        $categories_desc=$this->input->post("categories_desc",true);
        $cat_women_title=$this->input->post("cat_women_title",true);
        $cat_men_title=$this->input->post("cat_men_title",true);
        $cat_children_title=$this->input->post("cat_children_title",true);
        $compare_prices=$this->input->post("compare_prices",true);
        $compare_prices_desc=$this->input->post("compare_prices_desc",true);
        $choose_from_the_best=$this->input->post("choose_from_the_best",true);
        $shops_desc=$this->input->post("shops_desc",true);
        $apps_title=$this->input->post("apps_title",true);
        $apps_desc=$this->input->post("apps_desc",true);
        $payments_heading=$this->input->post("payments_heading",true);
        $payments_title=$this->input->post("payments_title",true);
        $payments_desc=$this->input->post("payments_desc",true);
        $service_heading=$this->input->post("service_heading",true);
        $service_title=$this->input->post("service_title",true);
        $service_desc=$this->input->post("service_desc",true);
        $welcome_text=$this->input->post("welcome_text",true);
        $our_latest_news=$this->input->post("our_latest_news",true);
        $news_desc=$this->input->post("news_desc",true);

        $check=$this->db->prepare("select * from home_page_content where language_id=:language_id");
        $check->bindParam(":language_id",$language_id);
        $check->execute();

        if($check->rowCount()>0)
        {
            $cate=$this->db->prepare("update home_page_content set
                                        about_us_title = '$about_us_title',
                                        about_us_heading = '$about_us_heading',
                                        about_us = '$about_us',
                                        why_choose_us = '$why_choose_us',
                                        categories_heading = '$categories_heading',
                                        categories_title = '$categories_title',
                                        categories_desc = '$categories_desc',
                                        cat_women_title = '$cat_women_title',
                                        cat_men_title = '$cat_men_title',
                                        cat_children_title = '$cat_children_title',
                                        compare_prices = '$compare_prices',
                                        compare_prices_desc = '$compare_prices_desc',
                                        choose_from_the_best = '$choose_from_the_best',
                                        shops_desc = '$shops_desc',
                                        apps_title = '$apps_title',
                                        apps_desc = '$apps_desc',
                                        payments_heading = '$payments_heading',
                                        payments_title = '$payments_title',
                                        payments_desc = '$payments_desc',
                                        service_heading = '$service_heading',
                                        service_title = '$service_title',
                                        service_desc = '$service_desc',
                                        welcome_text = '$welcome_text',
                                        our_latest_news = '$our_latest_news',
                                        news_desc = '$news_desc'
                                    where language_id = '$language_id' ");
            $cate->execute();

            //print_r($cate->queryString); die;
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Content updated successfully"));
            redirect($referer);
            exit;
        }
        
        $cate=$this->db->prepare("insert into home_page_content 
                                (
                                    language_id, 
                                    about_us_title, 
                                    about_us_heading, 
                                    about_us, 
                                    why_choose_us,
                                    categories_heading,
                                    categories_title,
                                    categories_desc,
                                    cat_women_title,
                                    cat_men_title,
                                    cat_children_title,
                                    compare_prices,
                                    compare_prices_desc,
                                    choose_from_the_best,
                                    shops_desc,
                                    apps_title,
                                    apps_desc,
                                    payments_heading,
                                    payments_title,
                                    payments_desc,
                                    service_heading,
                                    service_title,
                                    service_desc,
                                    welcome_text,
                                    our_latest_news,
                                    news_desc
                                ) 
                                values 
                                (
                                    :language_id
                                    :about_us_title,
                                    :about_us_heading,
                                    :about_us,
                                    :why_choose_us,
                                    :categories_heading,
                                    :categories_title,
                                    :categories_desc,
                                    :cat_women_title,
                                    :cat_men_title,
                                    :cat_children_title,
                                    :compare_prices,
                                    :compare_prices_desc,
                                    :choose_from_the_best,
                                    :shops_desc,
                                    :apps_title,
                                    :apps_desc,
                                    :payments_heading,
                                    :payments_title,
                                    :payments_desc,
                                    :service_heading,
                                    :service_title,
                                    :service_desc,
                                    :welcome_text,
                                    :our_latest_news,
                                    :news_desc
                                )");
        $cate->bindParam(":language_id",$language_id);
        $cate->bindParam(":about_us_title",$about_us_title);
        $cate->bindParam(":about_us_heading",$about_us_heading);
        $cate->bindParam(":about_us",$about_us);
        $cate->bindParam(":why_choose_us",$why_choose_us);
        $cate->bindParam(":categories_heading",$categories_heading);
        $cate->bindParam(":categories_title",$categories_title);
        $cate->bindParam(":categories_desc",$categories_desc);
        $cate->bindParam(":cat_women_title",$cat_women_title);
        $cate->bindParam(":cat_children_title",$cat_children_title);
        $cate->bindParam(":cat_men_title",$cat_men_title);
        $cate->bindParam(":compare_prices",$compare_prices);
        $cate->bindParam(":compare_prices_desc",$compare_prices_desc);
        $cate->bindParam(":choose_from_the_best",$choose_from_the_best);
        $cate->bindParam(":shops_desc",$shops_desc);
        $cate->bindParam(":apps_title",$apps_title);
        $cate->bindParam(":apps_desc",$apps_desc);
        $cate->bindParam(":payments_heading",$payments_heading);
        $cate->bindParam(":payments_title",$payments_title);
        $cate->bindParam(":payments_desc",$payments_desc);
        $cate->bindParam(":service_heading",$service_heading);
        $cate->bindParam(":service_title",$service_title);
        $cate->bindParam(":service_desc",$service_desc);
        $cate->bindParam(":welcome_text",$welcome_text);
        $cate->bindParam(":our_latest_news",$our_latest_news);
        $cate->bindParam(":news_desc",$news_desc);
        $cate->execute();

        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Content updated successfully"));
        redirect($referer);
        exit;
    }

    public function getAboutUs(){
        $language_id = $this->input->post("language_id",true);

        $res=$this->db->prepare("select * from home_page_content where language_id=:language_id");
        $res->bindParam(":language_id",$language_id);
        $res->execute();
        $res=$res->fetch();
        
        echo json_encode(array("status"=>"1","msg"=>"success","data"=>$res));
        exit;
    }

}