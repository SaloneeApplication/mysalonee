<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminAdvertisements extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("advertisement")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }elseif($type=='rejectAD'){
                $this->rejectAD();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }elseif($type=='approve_ad'){
                $this->approveAd();
            }
        }
    }
    
    public function rejectAD(){
        $referer=$_SERVER['HTTP_REFERER'];
        $id=$this->input->post("id",true);
        $sid=$this->input->post("sid",true);
        $reason=$this->input->post("reason",true);
        if(strpos($referer,"view-service-provider")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$sid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $date=date("Y-m-d");
        $dbres=$this->db->prepare("update advertisements set approve_status='rejected',status='disable',remark=:reason where service_provider_id=:sid and advertisement_id=:id");
        $dbres->bindParam(":sid",$sid);
        $dbres->bindParam(":id",$id);
        $dbres->bindParam(":reason",$reason);
        $dbres->execute();
        // echo json_encode($dbres->errorInfo());exit;
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Advertisement Rejected"));
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $sms_msg="Your salonee advertisement got rejected.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);


        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your advertiment rejected due to <strong> '.$reason.' </strong>. please contact us. <br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your Advertisement Rejected | Salonee.",$message);
        redirect($referer);
        exit;
    }
    
    public function approveAd(){
        $referer=$_SERVER['HTTP_REFERER'];
        $id=$this->input->get("id",true);
        $sid=$this->input->get("sid",true);
        if(strpos($referer,"view-service-provider")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$sid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $date=date("Y-m-d");
        $dbres=$this->db->prepare("update advertisements set status='disable',approve_status='approved' where service_provider_id=:sid and advertisement_id=:id");
        $dbres->bindParam(":sid",$sid);
        $dbres->bindParam(":id",$id);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Advertisement Approved"));
        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $sms_msg="Your salonee advertisement got approved.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your Advertisement request approved.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your Advertisement Approved | Salonee.",$message);
        redirect($referer);
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Advertisements';
        $data['page']='advertisement';
        $data['admin_id']=$this->admin_id;
        $data['advertisements']=$this->getAdvertisements();
        return $data;
    }
    public function getAdvertisements(){
        $adv=$this->input->get("adv",true);
        if($adv=='active'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='active' order by a.created_time desc");
        }elseif($adv=='pending'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='pending' order by a.created_time desc");
        }elseif($adv=='rejected'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='rejected' order by a.created_time desc");
        }elseif($adv=='expired'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='expired' order by a.created_time desc");
        }elseif($adv=='approved'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='approved' order by a.created_time desc");
        }else{
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a order by a.created_time desc");
        }
      
        $total_sb->execute();
        return $total_sb->fetchAll();
    }
}