<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserRatings extends Controller{
    public $user_id;
    public $branch_type;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Service Ratings';
        $data['page']='ratings';
        $data['user_id']=$this->user_id;
        return $data;
    }
    public function getRatings(){
        $cate=$this->db->prepare("select r.*,if(user_id!=0,(select name from user where user_id=r.user_id),'') as user_name,if(r.service_id!=0,(select service_name from service_provider_services where service_provider_service_id=r.service_id),'') as service_name from ratings r where r.service_provider_id=:uid");
        $cate->bindParam(":uid",$this->user_id);
        $cate->execute();
        return $cate->fetchAll();
    }
}