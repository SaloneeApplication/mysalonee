<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminRevenueReports extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("revenue_reports")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function init(){
        // $date=date("Y-m-d");
        // for($i=0;$i<100;$i++){
        //     $pamentstatus=rand(0,1);
        //     $amount=rand(111,999);
        //     $db=$this->db->prepare("insert into test (date,amount,paymentstatus) values ('$date',$amount,$pamentstatus)");
        //     $db->execute();
        //     $pamentstatus=rand(0,1);
        //     $amount=rand(111,999);
        //     $db=$this->db->prepare("insert into test (date,amount,paymentstatus) values ('$date',$amount,$pamentstatus)");
        //     $db->execute();
        //     $date=date("Y-m-d",strtotime("-1 day",strtotime($date)));
        // }

        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='getTicketInfo'){
                $this->getTicketInfo();
            }elseif($type=='replyTicket'){
                $this->replyTicket();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }elseif($type=='approve_ad'){
                $this->approveAd();
            }
        }
    }
    public function getTicketInfo(){
        $tid=$this->input->post("tid",true);
        $od=$this->db->prepare("select * from support_tickets where ticket_id=:id");
        $od->bindParam(":id",$tid);
        $od->execute();
        $od=$od->fetch();
        echo '<div class="form-group">
        <h4 style="font-weight:600;">Issue Title</h4>
        <p>'.$od['title'].'</p>
        </div>';
        echo '<div class="form-group">
            <h4  style="font-weight:600;">Issue Title</h4>
            <p>'.$od['issue_description'].'</p>
        </div>';
        exit;
    }
    public function replyTicket(){

        $tid=$this->input->post("tid",true);
        $reply=$this->input->post("reply",true);
        $referer=$_SERVER['HTTP_REFERER'];

        $od=$this->db->prepare("select * from support_tickets where ticket_id=:id");
        $od->bindParam(":id",$tid);
        $od->execute();
        $od=$od->fetch();
        $uid=$od['user_id'];
        $ticket_id=$od['ticket_id'];

        $dbres1=$this->db->prepare("update support_tickets set closed_by_admin =:closed_by_admin,reply_desc=:reply_desc,status='close'  where ticket_id=:ticket_id");
        $dbres1->bindParam(":closed_by_admin",$this->admin_id);
        $dbres1->bindParam(":reply_desc",$reply);
        $dbres1->bindParam(":ticket_id",$ticket_id);
        $dbres1->execute();

        $che=$this->db->prepare("select email,name from user where user_id=:id");
        $che->bindParam(":id",$uid);
        $che->execute();
        $user=$che->fetch();
        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your Ticket [ #'.$ticket_id.' ] replied from Salonee <br/> ';
        $message.=$reply;
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Ticket Replied [ #".$ticket_id." ] | Salonee.",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully ticket replied and closed."));
        redirect($referer);

        exit;
    }

    public function pageData(){
        $data=array();
        $data['title']='Admin | Revenue Reports';
        $data['page']='revenue_reports';
        $data['admin_id']=$this->admin_id;
        $data['revenue']=$this->getRevenue();
        return $data;
    }
    public function getRevenue(){
        $report=$this->input->get("report",true);
        if($report=='yearly'){
            $total_sb=$this->db->prepare("select year(date_of_order) AS date ,sum(case when (payment_status='completed' && order_status='completed' && order_type='featured_profile') then total_amount else 0 end) as total_featured,sum(case when (payment_status='completed' && order_status='completed' && order_type='advertisement') then total_amount else 0 end) as total_advertisement,sum(case when (payment_status='completed' && order_status='refunded' && order_type='advertisement') then refunded_amount else 0 end) as total_refunded_earnings,sum(case when (payment_status='completed' && order_status='completed' && order_type='product') then admin_commission else 0 end) as total_product_commission,sum(case when (payment_status='completed' && order_status='completed' && order_type='membership') then total_amount else 0 end) as total_subscription_amount from orders  group by year(date_of_order) ORDER BY `date_of_order` desc");
        }elseif($report=='monthly'){
            $total_sb=$this->db->prepare("select concat(date_format(date_of_order,'%Y-%M')) as date ,sum(case when (payment_status='completed' && order_status='completed' && order_type='featured_profile') then total_amount else 0 end) as total_featured,sum(case when (payment_status='completed' && order_status='completed' && order_type='advertisement') then total_amount else 0 end) as total_advertisement,sum(case when (payment_status='completed' && order_status='refunded' && order_type='advertisement') then refunded_amount else 0 end) as total_refunded_earnings,sum(case when (payment_status='completed' && order_status='completed' && order_type='product') then admin_commission else 0 end) as total_product_commission,sum(case when (payment_status='completed' && order_status='completed' && order_type='membership') then total_amount else 0 end) as total_subscription_amount from orders  group by concat(date_format(date_of_order,'%Y%m')) order by date_of_order desc");
        }elseif($report=='weekly'){
            $total_sb=$this->db->prepare("select CONCAT(DATE_FORMAT(DATE_ADD(date_of_order, INTERVAL(1-DAYOFWEEK(date_of_order)) DAY),'%Y %M %d'), ' - ',    
            DATE_FORMAT(DATE_ADD(date_of_order, INTERVAL(7-DAYOFWEEK(date_of_order)) DAY),'%Y %M %d')) AS date ,sum(case when (payment_status='completed' && order_status='completed' && order_type='featured_profile') then total_amount else 0 end) as total_featured,sum(case when (payment_status='completed' && order_status='completed' && order_type='advertisement') then total_amount else 0 end) as total_advertisement,sum(case when (payment_status='completed' && order_status='refunded' && order_type='advertisement') then refunded_amount else 0 end) as total_refunded_earnings,sum(case when (payment_status='completed' && order_status='completed' && order_type='product') then admin_commission else 0 end) as total_product_commission,sum(case when (payment_status='completed' && order_status='completed' && order_type='membership') then total_amount else 0 end) as total_subscription_amount from orders  group by yearweek(date_of_order) ORDER BY `date_of_order` desc");
        }elseif($report=='custom_date'){
            $from=$this->input->get("from",true);
            $to=$this->input->get("to",true);

            $total_sb=$this->db->prepare("select date_format(date_of_order,'%Y-%m-%d') as date ,sum(case when (payment_status='completed' && order_status='completed' && order_type='featured_profile') then total_amount else 0 end) as total_featured,sum(case when (payment_status='completed' && order_status='completed' && order_type='advertisement') then total_amount else 0 end) as total_advertisement,sum(case when (payment_status='completed' && order_status='refunded' && order_type='advertisement') then refunded_amount else 0 end) as total_refunded_earnings,sum(case when (payment_status='completed' && order_status='completed' && order_type='product') then admin_commission else 0 end) as total_product_commission,sum(case when (payment_status='completed' && order_status='completed' && order_type='membership') then total_amount else 0 end) as total_subscription_amount from orders  where cast(date_of_order as date) >= :from and cast(date_of_order as date) <= :to GROUP by concat(date_format(date_of_order,'%Y%m%d')) ORDER BY `date_of_order` desc");
            $total_sb->bindParam(":from",$from);
            $total_sb->bindParam(":to",$to);
        }else{ 
            $total_sb=$this->db->prepare("select concat(date_format(date_of_order,'%Y-%m-%d')) AS date ,sum(case when (payment_status='completed' && order_status='completed' && order_type='featured_profile') then total_amount else 0 end) as total_featured,sum(case when (payment_status='completed' && order_status='completed' && order_type='advertisement') then total_amount else 0 end) as total_advertisement,sum(case when (payment_status='completed' && order_status='refunded' && order_type='advertisement') then refunded_amount else 0 end) as total_refunded_earnings,sum(case when (payment_status='completed' && order_status='completed' && order_type='product') then admin_commission else 0 end) as total_product_commission,sum(case when (payment_status='completed' && order_status='completed' && order_type='membership') then total_amount else 0 end) as total_subscription_amount from orders  group by concat(date_format(date_of_order,'%Y%m%d')) ORDER BY `date_of_order` desc");
        }
        $total_sb->execute();
        return $total_sb->fetchAll();
    }
    public function getAdvertisements(){
        $adv=$this->input->get("adv",true);
        if($adv=='active'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='active' order by a.created_time desc");
        }elseif($adv=='pending'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='pending' order by a.created_time desc");
        }elseif($adv=='rejected'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='rejected' order by a.created_time desc");
        }elseif($adv=='expired'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='expired' order by a.created_time desc");
        }elseif($adv=='approved'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='approved' order by a.created_time desc");
        }else{
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a order by a.created_time desc");
        }
      
        $total_sb->execute();
        return $total_sb->fetchAll();
    }
}