<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserTransactions extends Controller{
    public $user_id;
    public $branch_type;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Transactions';
        $data['page']='transctions';
        $data['user_id']=$this->user_id;
        return $data;
    }

    public function getTranscations(){
        $dbres=$this->db->prepare("select * from service_provder_wallet_transactions where service_provider_id=:uid order by created_time desc");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->execute();
        return $dbres->fetchAll();
    }
}