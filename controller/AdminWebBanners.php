<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminWebBanners extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
    }
    public function load(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addBanner'){
                $this->addBanners();
            }
            elseif($type=='editBanner'){
                $this->editBanner();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='delete_banner'){
                $this->deleteBanner();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Manage Content';
        $data['page']='Content';
        $data['admin_id']=$this->admin_id;
        return $data;
    }

    public function getBanners()
    {
        $sql=$this->db->prepare("select * from banners where status=1");
        $sql->execute();
        $resp=array();
        while($row=$sql->fetch()){
            $resp[]=$row;
        }
        return $resp;
    }

    public function addBanners()
    {
        $referer=$_SERVER['HTTP_REFERER'];

        if($_FILES["image"]["size"]>0){
            $tmp_name = $_FILES["image"]["tmp_name"];
            $file='uploads/admin/'.basename($_FILES["image"]["name"]);
            $path='uploads/admin/'.basename($_FILES["image"]["name"]);
            move_uploaded_file($tmp_name,$path);
        }else{
            $file='uploads/admin/default-image.png';
        }

        $dbres=$this->db->prepare("INSERT INTO banners(image) VALUES(:image)");
        $dbres->bindParam(":image",$file);
        $dbres->execute();

        //print_r($dbres->queryString); die;
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Banner added successfully"));
        redirect($referer);
        exit;
    }

    public function editBanner()
    {
        $referer=$_SERVER['HTTP_REFERER'];
        $banner_id = $this->input->post('banner_id', TRUE);

        if($_FILES["image"]["size"]>0){
            $tmp_name = $_FILES["image"]["tmp_name"];
            $file='uploads/admin/'.basename($_FILES["image"]["name"]);
            $path='uploads/admin/'.basename($_FILES["image"]["name"]);
            move_uploaded_file($tmp_name,$path);
        }else{
            $file='uploads/admin/default-image.png';
        }

        $cate=$this->db->prepare("update banners set image = '$file'
                                where id = '$banner_id' ");
        $cate->execute();

        //print_r($cate->queryString); die;
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Banner updated successfully"));
        redirect($referer);
        exit;
    }

    public function deleteBanner()
    {
        $referer=$_SERVER['HTTP_REFERER'];
        $banner_id = $this->input->get('banner_id', TRUE); 

        $cate=$this->db->prepare("update banners set status = 2
                                where id = '$banner_id' ");
        $cate->execute();

        //print_r($cate->queryString); die;
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Banner deleted successfully"));
        redirect($referer);
        exit;
    }
}