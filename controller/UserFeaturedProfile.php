<?php
require_once dirname(__DIR__).'/core/Controller.php';
ini_set('max_execution_time', '0'); // for infinite time of execution 

class UserFeaturedProfile extends Controller{
    public $user_id;
    public $branch_type;
    public $merchant_id="45990";
    public $access_code="AVFY03IE78BG61YFGB";
    public $working_key="08D12F6F9BC71D36B0A9F9D3ECFDEF57";
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if(@$_REQUEST['type']!='payment_response'){
            if($this->user_id==''){
                session_destroy();
                redirect("index.php");
                exit;
            }
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='purchaseNewFeatured'){
                $this->purchaseNewFeatured();
            }elseif($type=='purchaseFeaturedProfile'){
                $this->purchaseFeaturedProfile();
            }
            if(@$_GET['type']=='payment_response'){
                $this->paymentResponse();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='disable'){
                $this->disableFeature();
            }elseif($type=='activateFeature'){
                $this->activateFeature();
            }
            if(@$_GET['type']=='payment_response'){
                $this->paymentResponse();
            }
        }
        // $cities=file_get_contents(base_url.'cities.json');
        // $cities=json_decode($cities);
        // $start=0;
        // foreach($cities as $cit){
        //     $code=$cit->country;
        //     $name=$cit->name;
        //     $lat=$cit->lat;
        //     $lng=$cit->lng;
        //     if($lat=='8.6317' && $lng=='-71.07833'){
        //         $start=1;
        //         continue;
        //     }
        //     if($start==1){
        //         $checkcode=strtolower($code);
        //         $dbres=$this->db->prepare("select id from countries where alpha_2='$checkcode'");
        //         $dbres->execute();
        //         $dbres=$dbres->fetch();
        //         $cid=$dbres['id'];
        //         $dbres1=$this->db->prepare("insert into cities (country_id,city_name_en,latitude,longitude) values ('$cid','$name','$lat','$lng')");
        //         $dbres1->execute();
        //     }

        // }
        // exit;
    }
    public function disableFeature(){
        $referer=$_SERVER['HTTP_REFERER'];
        $id=$this->input->get("id",true);
        $dbres=$this->db->prepare("update featured_profiles set status='disable' where featured_id=:fid and service_provider_id=:uid");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->bindParam(":fid",$id);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully featured Profile disabled"));
        redirect($referer);
        exit;
    }
    public function activateFeature(){
        $id=$this->input->get("id",true);
        $referer=$_SERVER['HTTP_REFERER'];

        $dbres=$this->db->prepare("select * from featured_profiles where featured_id=:fid and service_provider_id=:uid");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->bindParam(":fid",$id);
        $dbres->execute();
        $pro=$dbres->fetch();

        $days=$pro['days'];
        $used_dates=$pro['used_dates'];
        $last_used_date=$pro['last_used_date'];
        $used_dates=json_decode($used_dates);
        if($days!=0){
            $dbres=$this->db->prepare("update featured_profiles set status='disable' where service_provider_id=:uid and status='active'");
            $dbres->bindParam(":uid",$this->user_id);
            $dbres->execute();

            if(count($used_dates)==0){
                $used_dates=array(date("Y-m-d"));
                $last_used_date=date("Y-m-d");
                $used_dates=json_encode($used_dates);
                $days=$days-1;
            }else{ 
                if(in_array(date("Y-m-d"),$used_dates)){
                    $used_dates=json_encode($used_dates);
                }else{
                    $days=$days-1;
                    $used_dates[]=date("Y-m-d"); 
                    $last_used_date=date("Y-m-d");
                    $used_dates=json_encode($used_dates);
                }
            }   
            $dbres=$this->db->prepare("update featured_profiles set status='active', last_used_date=:last_used_date , used_dates=:used_dates , days=:days where featured_id=:fid and service_provider_id=:uid");
            $dbres->bindParam(":uid",$this->user_id);
            $dbres->bindParam(":fid",$id);
            $dbres->bindParam(":last_used_date",$last_used_date);
            $dbres->bindParam(":used_dates",$used_dates);
            $dbres->bindParam(":days",$days);
            $dbres->execute();


            // echo json_encode($this->db->errorInfo());exit;
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully featured Profile activated"));
            redirect($referer);
           
        }else{
            if($last_used_date==date("Y-m-d")){
                $dbres=$this->db->prepare("update featured_profiles set status='disable' where service_provider_id=:uid and status='active'");
                $dbres->bindParam(":uid",$this->user_id);
                $dbres->execute();
                
                $dbres=$this->db->prepare("update featured_profiles set status='active' where featured_id=:fid and service_provider_id=:uid");
                $dbres->bindParam(":uid",$this->user_id);
                $dbres->bindParam(":fid",$id);
                $dbres->execute();
                $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully featured Profile activated"));
                redirect($referer);
            }else{
                $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Featured Profile Expired. Purchase new one ."));
                redirect($referer);
            }
        }
       exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Featured Profile';
        $data['page']='user_featured_profile';
        $data['user_id']=$this->user_id;
        return $data;
    }
    public function getFeaturedProfiles(){
        $dbres=$this->db->prepare("select * from featured_profiles where service_provider_id=:uid order by created_time desc");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->execute();
        return $dbres->fetchAll();
    }

    public function purchaseFeaturedProfile(){
        $referer=$_SERVER['HTTP_REFERER'];
        $days=$this->input->post("days",true);
        $per_day=$this->getOption("featured_profile_plan_per_day");
        $commission=$this->getOption("featured_profiles_admin_commission_per_day");
        if($days==0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Minimum days greater than 1."));
            redirect($referer);
            exit;
        }
        if($this->getMemberAccess()==0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Please upgrade now to access."));
            redirect($referer);
            exit;
        }
        $total_amount=$per_day*$days;
        $commission=$commission*$days;
        $total_amount=$commission+$total_amount;
        $total_amount=round($total_amount);
        $order_id="S".$this->user_id.time();

        $advdata=array();
        $advdata['days']=$days;
        $advdata['order_id']=$order_id;
        $advdata['total_amount']=$total_amount;
        $advdata=json_encode($advdata);

        $dbres1=$this->db->prepare("insert into orders (order_id,service_provider_id,user_id,user_type,total_amount,admin_commission,payment_status,order_type,order_status,note_dev)values(:order_id,:uid,:uid,'user',:amount,:commission,'pending','featured_profile','pending',:note_dev)");
        $dbres1->bindParam(":uid",$this->user_id);
        $dbres1->bindParam(":order_id",$order_id);
        $dbres1->bindParam(":amount",$total_amount);
        $dbres1->bindParam(":commission",$commission);
        $dbres1->bindParam(":note_dev",$advdata);
        $dbres1->execute();

        $user=$this->db->prepare("select s.*,c.city_name_en,(select name from countries where id=c.country_id) as country_name from service_provider s,cities c where s.city=c.id and s.service_provider_id=:service_provider_id");
        $user->bindParam(":service_provider_id",$this->user_id);
        $user->execute();
        $user=$user->fetch();

        $encid=$order_id.'~'.$this->user_id;
        $encid=$this->encrypt($encid);
        $encid=base64_encode($encid);
        $encid=rawurlencode($encid);
        setcookie("encid", $encid, time()+30*24*60*60,"/",$_SERVER['HTTP_HOST']);

      
        $ccavenue=array();
        $ccavenue['merchant_id']=$this->merchant_id;
        $ccavenue['order_id']=$order_id;
        $ccavenue['amount']=$total_amount;
        $ccavenue['currency']="AED";
        $ccavenue['redirect_url']=user_base_url."featured_profile.php?type=payment_response";
        $ccavenue['cancel_url']=user_base_url."featured_profile.php?type=payment_response";
        $ccavenue['billing_name']=$user['name'];
        $ccavenue['billing_email']=$user['email'];
        $ccavenue['billing_address']=$user['address'];
        $ccavenue['billing_city']=$user['city_name_en'];
        $ccavenue['billing_tel']=$user['mobile'];
        $ccavenue['billing_country']=$user['country_name'];

        $merchant_data='';
        foreach ($ccavenue as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }
        $encrypted_data=$this->ccavenue_encrypt($merchant_data,$this->working_key);

        $resp=array();
        $resp['encrypted_data']=$encrypted_data;
        $resp['access_code']=$this->access_code;
        $resp['status']='200';
        ?>
        <form action="https://secure.ccavenue.ae/transaction/transaction.do?command=initiateTransaction" name="redirect" id="upgrade_now" method="post" >
  <input type="submit" style="display:none;" id="submit_button" />
  <input type=hidden name=encRequest value='<?php echo $resp['encrypted_data'];?>'>
  <input type=hidden name=access_code value='<?php echo $resp['access_code'];?>'>
</form>
<script>
    window.onload=function(){
        document.querySelector("#upgrade_now").submit();
    }
</script>
<?php 
        exit;
    }

    public function paymentResponse(){
        $encResponse=$_POST["encResp"];	
        $referer=$_SERVER['HTTP_REFERER'];
        if($_SERVER['REQUEST_METHOD']!='POST'){
            $this->session->set_userdata(array("alert_type"=>"warning","alert_msg"=>"Invalid Request"));
            redirect(user_base_url."dashboard.php");
            exit;
        }		
        $rcvdString=$this->ccavenue_decrypt($encResponse,$this->working_key);		
        $order_status="";
        $decryptValues=explode('&', $rcvdString);
        $order_status=$decryptValues[3];//order_status
        $order_status=explode("=",$order_status);
        $order_status=$order_status[1];

        if($order_status=="Success"){
            //payment success
            $order_id=$decryptValues[0];//order_id
            $order_id=explode("=",$order_id);
            $order_id=$order_id[1];

            $tracking_id=$decryptValues[1];//tracking_id
            $tracking_id=explode("=",$tracking_id);
            $tracking_id=$tracking_id[1];

            
            $encid=@$_GET['encid'];
            $append="";
            if($encid==''){
                $encid=$_COOKIE['encid'];
            }
            $encid=rawurldecode($encid);
            $encid=base64_decode($encid);
            $encid=$this->decrypt($encid);
            $encid=explode("~",$encid);
            $user_id=$encid[1];
            $ex_order_id=$encid[0];
            if($this->user_id==''){
                $append="exusr";
                //when session expiry 
                $usr=$this->db->prepare("select * from service_provider where service_provider_id=:user_id");
                $usr->bindParam(":user_id",$user_id);
                $usr->execute();
                $usr=$usr->fetch();
                $_SESSION["serviceProviderId"] = $usr['service_provider_id'];
				$_SESSION["branch_type"] = $usr['user_type'];
                $this->user_id=$this->session->userdata("serviceProviderId");
                $this->branch_type=$this->session->userdata("branch_type");
                
            }

            $order=$this->db->prepare("select * from orders where order_id=:order_id");
            $order->bindParam(":order_id",$order_id);
            $order->execute();
            $order=$order->fetch();
            $order_note=json_decode($order['note_dev'],true);
            $days=$order_note['days'];
            $total_amount=$order['total_amount'];
            if($order['payment_status']=='completed'){
                $this->session->set_userdata(array("alert_type"=>"warning","alert_msg"=>"Invalid Request"));
                redirect(user_base_url."featured_profile.php");
                exit;
            }

            $dbres=$this->db->prepare("update orders set payment_status='completed',order_status='completed',txn_id=:txn_id where order_id=:order_id");
            $dbres->bindParam(":order_id",$order_id);
            $dbres->bindParam(":txn_id",$tracking_id);
            $dbres->execute();

            $used_dates=json_encode(array());
            $dbres=$this->db->prepare("insert into featured_profiles (service_provider_id,days,used_dates,amount,order_id,status,total_days) values (:uid,:days,:used_dates,:amount,:order_id,'disable',:total_days)");
            $dbres->bindParam(":uid",$this->user_id);
            $dbres->bindParam(":days",$days);
            $dbres->bindParam(":total_days",$days);
            $dbres->bindParam(":used_dates",$used_dates);
            $dbres->bindParam(":amount",$total_amount);
            $dbres->bindParam(":order_id",$order_id);
            $dbres->execute();
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Featured Profile Purchased."));
            redirect(user_base_url."featured_profile.php?".$append);
            exit;
        }else{
            //payment failed
            
            $encid=@$_GET['encid'];
            $append="";
            if($encid==''){
                $encid=$_COOKIE['encid'];
            }
            $encid=rawurldecode($encid);
            $encid=base64_decode($encid);
            $encid=$this->decrypt($encid);
            $encid=explode("~",$encid);
            $user_id=$encid[1];
            $ex_order_id=$encid[0];
            if($this->user_id==''){
                $append="exusr";
                //when session expiry 
                $usr=$this->db->prepare("select * from service_provider where service_provider_id=:user_id");
                $usr->bindParam(":user_id",$user_id);
                $usr->execute();
                $usr=$usr->fetch();
                $_SESSION["serviceProviderId"] = $usr['service_provider_id'];
				$_SESSION["branch_type"] = $usr['user_type'];
                $this->user_id=$this->session->userdata("serviceProviderId");
                $this->branch_type=$this->session->userdata("branch_type");
                
            }
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Payment Failed."));
            redirect(user_base_url."featured_profile.php?".$append);
            exit;
        }
    }
    public function purchaseNewFeatured(){
        exit;
        $days=$this->input->post("days",true);
        $referer=$_SERVER['HTTP_REFERER'];
        $per_day=$this->getOption("featured_profile_plan_per_day");
        $commission=$this->getOption("featured_profiles_admin_commission_per_day");
        if($days==0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Minimum days greater than 1."));
            redirect($referer);
            exit;
        }
        if($this->getMemberAccess()==0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Please upgrade now to access."));
            redirect($referer);
            exit;
        }
        $total_amount=$per_day*$days;
        $commission=$commission*$days;
        $total_amount=$commission+$total_amount;
        $total_amount=round($total_amount);
        $used_dates=json_encode(array());
        $order_id="S".$this->user_id.time();
        $dbres1=$this->db->prepare("insert into orders (order_id,service_provider_id,user_id,user_type,total_amount,admin_commission,payment_status,order_type,order_status,remark)values(:order_id,:uid,:uid,'user',:amount,:commission,'completed','featured_profile','completed','testing')");
        $dbres1->bindParam(":uid",$this->user_id);
        $dbres1->bindParam(":order_id",$order_id);
        $dbres1->bindParam(":amount",$total_amount);
        $dbres1->bindParam(":commission",$commission);
        $dbres1->execute();
      
        $dbres=$this->db->prepare("insert into featured_profiles (service_provider_id,days,used_dates,amount,order_id,status,total_days) values (:uid,:days,:used_dates,:amount,:order_id,'disable',:total_days)");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->bindParam(":days",$days);
        $dbres->bindParam(":total_days",$days);
        $dbres->bindParam(":used_dates",$used_dates);
        $dbres->bindParam(":amount",$total_amount);
        $dbres->bindParam(":order_id",$order_id);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Featured Profile Purchased."));
        redirect($referer);
        exit;
    }
}