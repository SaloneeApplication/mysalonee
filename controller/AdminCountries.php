<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminCountries extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        // echo $this->auth_type; die;
        // if($this->admin_id==''){
        //     redirect("index.php");
        //     exit;
        // }
        // if($this->auth_type!='admin'){
        //     session_destroy();
        //     redirect("index.php");
        //     exit;
        // }
        // if(!$this->getAdminPageAccess("countries")){
        //     $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
        //     redirect("dashboard.php");
        //     exit;
        // }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addNewCountry'){
                $this->addNewCountry();
            }elseif($type=='updateCountry'){
                $this->updateCountry();
            }elseif($type=='disablecountry'){
                $this->disableCountry();
            }elseif($type=='enablecountry'){
                $this->enableCountry();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function enableCountry(){
        $catid=$this->input->post("country_id",true);
        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update service_countries set status=1,datetime=:datetime where id=:country_id");
        $up->bindParam(":country_id",$catid);
        $up->bindParam(":datetime",$date);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Country Enabled"));
       
        echo json_encode(array("status"=>"1","msg"=>"success"));
        exit;
    }
    public function disableCountry(){
        $catid=$this->input->post("country_id",true);
        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update service_countries set status=0,datetime=:datetime where id=:country_id");
        $up->bindParam(":country_id",$catid);
        $up->bindParam(":datetime",$date);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Country Disabled"));
        echo json_encode(array("status"=>"1","msg"=>"success"));
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Countries';
        $data['page']='countries';
        $data['admin_id']=$this->admin_id;
        return $data;
    }

    public function updateCountry(){
        $referer=$_SERVER['HTTP_REFERER'];
        $country_name=$this->input->post("country_name",true);
        $telephone_code=$this->input->post("telephone_code",true);
        $language=$this->input->post("language",true);
        $country_id=$this->input->post("country_id",true);

        $cat=$this->db->prepare("select * from service_countries where id=:id");
        $cat->bindParam(":id",$country_id);
        $cat->execute();
        $cat=$cat->fetch();
        $image=$cat['country_flag'];
        // var_dump($country_id);die;

        $check=$this->db->prepare("select * from service_countries where country_name=:country_name and telephone_code=:telephone_code id!=:id ");
        $check->bindParam(":country_name",$country_name);
        $check->bindParam(":telephone_code",$telephone_code);
        $check->bindParam(":id",$country_id);
        $check->execute();
        if($check->rowCount()>0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Country Exist"));
            redirect($referer);
            exit;
        }

        if($_FILES["country_flag"]["size"]>0){
            $tmp_name = $_FILES["country_flag"]["tmp_name"];
            $path='uploads/countries/'.basename($_FILES["country_flag"]["name"]);
            move_uploaded_file($tmp_name,$path);
            $image=$path;
        }

        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update service_countries set country_name=:country_name,telephone_code=:telephone_code,country_flag=:country_flag,datetime=:datetime,language=:language where id=:id");
        $up->bindParam(":id",$country_id);
        $up->bindParam(":country_name",$country_name);
        $up->bindParam(":telephone_code",$telephone_code);
        $up->bindParam(":country_flag",$image);
        $up->bindParam(":datetime",$date);
        $up->bindParam(":language",$language);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully  country updated"));
        redirect($referer);
    }

    public function addNewCountry(){
        $referer=$_SERVER['HTTP_REFERER'];
        $country_name=$this->input->post("country_name",true);
        $telephone_code=$this->input->post("telephone_code",true);
        $language=$this->input->post("language",true);
        
        $tmp_name = $_FILES["country_flag"]["tmp_name"];
        $path='uploads/countries/'.basename($_FILES["country_flag"]["name"]);
        move_uploaded_file($tmp_name,$path);
        $image=$path;

        $check=$this->db->prepare("select * from service_countries where country_name=:country_name and telephone_code=:telephone_code");
        $check->bindParam(":telephone_code",$telephone_code);
        $check->bindParam(":country_name",$country_name);
        $check->execute();
        if($check->rowCount()>0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Country Exist"));
            redirect($referer);
            exit;
        }

        $cate=$this->db->prepare("insert into service_countries (country_name,telephone_code,language,country_flag) values (:country_name,:telephone_code,:language,:country_flag)");
        $cate->bindParam(":country_name",$country_name);
        $cate->bindParam(":telephone_code",$telephone_code);
        $cate->bindParam(":language",$language);
        $cate->bindParam(":country_flag",$image);
        $cate->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully new Country added"));
        redirect($referer);
        exit;
    }
    public function getCountries(){

        $dbres=$this->db->prepare("select * from service_countries order by datetime desc");
        $dbres->execute();
        return $dbres;
    }

    public function getActiveCountries(){

        $dbres=$this->db->prepare("select * from service_countries where status=1 order by datetime desc");
        $dbres->execute();
        return $dbres;
    }
}