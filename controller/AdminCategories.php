<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminCategories extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("category")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addNewCategory'){
                $this->addNewCategory();
            }elseif($type=='updateCategory'){
                $this->updateCategory();
            }elseif($type=='disablecategory'){
                $this->disableCategory();
            }elseif($type=='enablecategory'){
                $this->enableCategory();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function enableCategory(){
        $catid=$this->input->post("cat_id",true);
        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update category set status=1,modified_time=:date where category_id=:id");
        $up->bindParam(":id",$catid);
        $up->bindParam(":date",$date);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully  category enabled"));
       
        echo json_encode(array("status"=>"1","msg"=>"success"));
        exit;
    }
    public function disableCategory(){
        $catid=$this->input->post("cat_id",true);
        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update category set status=0,modified_time=:date where category_id=:id");
        $up->bindParam(":id",$catid);
        $up->bindParam(":date",$date);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully  category disabled"));
        echo json_encode(array("status"=>"1","msg"=>"success"));
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Categories';
        $data['page']='category';
        $data['admin_id']=$this->admin_id;
        return $data;
    }

    public function updateCategory(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cateogry=$this->input->post("category",true);
        $short_d=$this->input->post("short_d",true);
        $cat_id=$this->input->post("cat_id",true);
        $category_for=$this->input->post("category_for",true);

        $cat=$this->db->prepare("select * from category where category_id=:id");
        $cat->bindParam(":id",$cat_id);
        $cat->execute();
        $cat=$cat->fetch();
        $image=$cat['image'];
        $image2=$cat['mobile_image'];
        $check=$this->db->prepare("select * from category where category=:name and category_for=:category_for category_id!=:category_id ");
        $check->bindParam(":category_for",$category_for);
        $check->bindParam(":name",$cateogry);
        $check->bindParam(":category_id",$cat_id);
        $check->execute();
        if($check->rowCount()>0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Category Exist"));
            redirect($referer);
            exit;
        }

        if($_FILES["category_image"]["size"]>0){
            $tmp_name = $_FILES["category_image"]["tmp_name"];
            $path='uploads/categories/'.basename($_FILES["category_image"]["name"]);
            move_uploaded_file($tmp_name,$path);
            $image=$path;
        }
        if($_FILES["mobile_category_image"]["size"]>0){
            $tmp_name2 = $_FILES["mobile_category_image"]["tmp_name"];
            $path2='uploads/categories/'.basename($_FILES["mobile_category_image"]["name"]);
            move_uploaded_file($tmp_name2,$path2);
            $image2=$path2;
        }

        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update category set name=:name,short_description=:short_description,image=:image,modified_time=:date,category_for=:category_for,mobile_image=:image2 where category_id=:id");
        $up->bindParam(":id",$cat_id);
        $up->bindParam(":name",$cateogry);
        $up->bindParam(":short_description",$short_d);
        $up->bindParam(":image",$image);
        $up->bindParam(":image2",$image2);
        $up->bindParam(":date",$date);
        $up->bindParam(":category_for",$category_for);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Category updated successfully"));
        redirect($referer);
    }

    public function addNewCategory(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cateogry=$this->input->post("category",true);
        $short_d=$this->input->post("short_d",true);
        $category_for=$this->input->post("category_for",true);
        
        $tmp_name = $_FILES["category_image"]["tmp_name"];
        $path='uploads/categories/'.basename($_FILES["category_image"]["name"]);
        move_uploaded_file($tmp_name,$path);
        $image=$path;

         
        $tmp_name2 = $_FILES["mobile_category_image"]["tmp_name"];
        $path2='uploads/categories/'.basename($_FILES["mobile_category_image"]["name"]);
        move_uploaded_file($tmp_name2,$path2);
        $image2=$path2;

        $check=$this->db->prepare("select * from category where category=:name and category_for=:category_for");
        $check->bindParam(":category_for",$category_for);
        $check->bindParam(":name",$cateogry);
        $check->execute();
        if($check->rowCount()>0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Category Exist"));
            redirect($referer);
            exit;
        }

        $cate=$this->db->prepare("insert into category (name,short_description,image,status,category_for,mobile_image) values (:name,:short_d,:image,1,:category_for,:image2)");
        $cate->bindParam(":name",$cateogry);
        $cate->bindParam(":short_d",$short_d);
        $cate->bindParam(":image",$image);
        $cate->bindParam(":image2",$image2);
        $cate->bindParam(":category_for",$category_for);
        $cate->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Category added successfully"));
        redirect($referer);
        exit;
    }
    public function getCategories(){

       // $dbres=$this->db->prepare("select * from category order by created_time desc");
        $dbres=$this->db->prepare("select * from category order by status");
        $dbres->execute();
        return $dbres;
    }
}
