<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserCategories extends Controller{
    public $user_id;
    public $auth_type;
    public $member_access=0;
    public $uinfo;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        $user=$this->db->prepare("select membership_expiry from service_provider where service_provider_id=:uid");
        $user->bindParam(":uid",$this->user_id);
        $user->execute();
        $this->uinfo=$user->fetch();
        if($this->uinfo['membership_expiry']>=date("Y-m-d")){
            $this->member_access=1;
        } 
    }
    
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
           if($type=='insertCategory'){
                if($this->branch_type!='main_branch'){
                    $referer=$_SERVER['HTTP_REFERER'];
                    $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"you don't have access"));
                    redirect($referer);
                }
                $this->insertCategory();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='enablecategory'){
                if($this->branch_type!='main_branch'){
                    $referer=$_SERVER['HTTP_REFERER'];
                    $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"you don't have access"));
                    redirect($referer);
                }
                $this->enableCategory();
            }elseif($type=='disablecategory'){
                if($this->branch_type!='main_branch'){
                    $referer=$_SERVER['HTTP_REFERER'];
                    $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"you don't have access"));
                    redirect($referer);
                }
                $this->disableCategory();
            }
        }
    }
    public function enableCategory(){
        $cid=$this->input->get("sid",true);
        $dbres=$this->db->prepare("update service_provider_categories set status=1 where service_provider_id=:id and service_provider_category_id=:sid");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->bindParam(":sid",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully category enabled"));
        header("location:".base_url('service_provider/category.php'));
        exit;
    }
    public function disableCategory(){
        $cid=$this->input->get("sid",true);
        $dbres=$this->db->prepare("update service_provider_categories set status=0 where service_provider_id=:id and service_provider_category_id=:sid");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->bindParam(":sid",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully category disabled"));
        header("location:".base_url('service_provider/category.php'));
        exit;
    }
    public function insertCategory(){
		$categories = $this->input->post("category",true);
        $total=count($categories);
        if(count($categories)==0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Please select least one category"));
            header("location:".base_url('service_provider/category.php'));
            exit;
        }
        $insertCount = 0;
        $existCount = 0;
		foreach($categories as $value)
		{
			$categoryId = $value;
            $check=$this->db->prepare("select service_provider_category_id from service_provider_categories where service_provider_id=:id and category_id=:cid");
            $check->bindParam(":id",$this->user_id);
            $check->bindParam(":cid",$categoryId);
            $check->execute();
            $check=$check->rowCount();
			if($check>0)
			{
				$existCount = $existCount +1; 
			}
			else
			{
                $dbre=$this->db->prepare("insert into service_provider_categories (service_provider_id,category_id,created_time,status) values (:id,:cid,now(),1)");
                $dbre->bindParam(":id",$this->user_id);
                $dbre->bindParam(":cid",$categoryId);
                $dbre->execute();
                $insertCount = $insertCount +1; 
			}
		}
        if($total==$insertCount){
            if($total==1){
                $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully ".$insertCount." category added "));
            }else{
                $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully ".$insertCount." categories added "));
            }
        }else{
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully ".$insertCount." categories added and ".$existCount." categories not added [ exist ]"));
        }
       
        header("location:".base_url('service_provider/category.php'));
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Categories';
        $data['page']='category';
        $data['user_id']=$this->user_id;
        return $data;
    }

    public function getUserCategoires(){
        $dbres=$this->db->prepare("select s.*,c.name,c.image,c.short_description,c.category_for   from service_provider_categories s ,category c where s.category_id=c.category_id and s.service_provider_id=:id");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->execute();
        $result=$dbres->fetchAll();
        return $result;
    }
}