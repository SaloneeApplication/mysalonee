<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminCities extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        // if($this->admin_id==''){
        //     redirect("index.php");
        //     exit;
        // }
        // if($this->auth_type!='admin'){
        //     session_destroy();
        //     redirect("index.php");
        //     exit;
        // }
        // if(!$this->getAdminPageAccess("city")){
        //     $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
        //     redirect("dashboard.php");
        //     exit;
        // }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addNewCity'){
                $this->addNewCity();
            }elseif($type=='updateCity'){
                $this->updateCity();
            }elseif($type=='disablecity'){
                $this->disableCity();
            }elseif($type=='enablecity'){
                $this->enableCity();
            }elseif($type=='citybycountry'){
                $this->getCitiesByCountry();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function enableCity(){
        $city_id=$this->input->post("city_id",true);
        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update service_cities set status=1,datetime=:datetime where id=:city_id");
        $up->bindParam(":city_id",$city_id);
        $up->bindParam(":datetime",$date);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully City Enabled"));
       
        echo json_encode(array("status"=>"1","msg"=>"success"));
        exit;
    }
    public function disableCity(){
        $city_id=$this->input->post("city_id",true);
        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update service_cities set status=0,datetime=:datetime where id=:city_id");
        $up->bindParam(":city_id",$city_id);
        $up->bindParam(":datetime",$date);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully City Disabled"));
        echo json_encode(array("status"=>"1","msg"=>"success"));
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | City';
        $data['page']='city';
        $data['admin_id']=$this->admin_id;
        return $data;
    }

    public function updateCity(){
        $referer=$_SERVER['HTTP_REFERER'];
        $city_id=$this->input->post("city_id",true);
        $country_id=$this->input->post("country_id",true);
        $city_name=$this->input->post("city_name",true);
        $is_top=$this->input->post("is_top",true);

        $cat=$this->db->prepare("select * from service_cities where id=:id");
        $cat->bindParam(":id",$city_id);
        $cat->execute();
        $cat=$cat->fetch();
        $image=$cat['image'];

        $check=$this->db->prepare("select * from service_cities where country_id=:country_id and name=:name id!=:id ");
        $check->bindParam(":country_id",$country_id);
        $check->bindParam(":name",$city_name);
        $check->bindParam(":id",$city_id);
        $check->execute();
        if($check->rowCount()>0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"City Exist"));
            redirect($referer);
            exit;
        }

        if($_FILES["city_image"]["size"]>0){
            $tmp_name = $_FILES["city_image"]["tmp_name"];
            $path='uploads/countries/'.basename($_FILES["city_image"]["name"]);
            move_uploaded_file($tmp_name,$path);
            $image=$path;
        }

        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update service_cities set country_id=:country_id,name=:city_name,is_top=:is_top,datetime=:datetime,image=:city_image where id=:id");
        $up->bindParam(":id",$city_id);
        $up->bindParam(":country_id",$country_id);
        $up->bindParam(":city_name",$city_name);
        $up->bindParam(":is_top",$is_top);
        $up->bindParam(":datetime",$date);
        $up->bindParam(":city_image",$image);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully city updated"));
        redirect($referer);
    }

    public function addNewCity(){
        $referer=$_SERVER['HTTP_REFERER'];
        $country_id=$this->input->post("country_id",true);
        $city_name=$this->input->post("city_name",true);
        $is_top=$this->input->post("is_top",true);
        
        $tmp_name = $_FILES["city_image"]["tmp_name"];
        $path='uploads/cities/'.basename($_FILES["city_image"]["name"]);
        move_uploaded_file($tmp_name,$path);
        $image=$path;

        $check=$this->db->prepare("select * from service_cities where name=:city_name");
        $check->bindParam(":city_name",$city_name);
        $check->execute();
        if($check->rowCount()>0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"City Exist"));
            redirect($referer);
            exit;
        }

        $cate=$this->db->prepare("insert into service_cities (country_id,name,image,is_top) values (:country_id,:name,:image,:is_top)");
        $cate->bindParam(":country_id",$country_id);
        $cate->bindParam(":name",$city_name);
        $cate->bindParam(":image",$image);
        $cate->bindParam(":is_top",$is_top);
        $cate->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully new City Added"));
        redirect($referer);
        exit;
    }
    public function getCities(){

        $dbres=$this->db->prepare("select c.*, (select country_name from service_countries where service_countries.id=c.country_id)country_name  from service_cities as c order by c.datetime desc");
        $dbres->execute();
        return $dbres;
    }

    public function getCitiesByCountry() {
        $country_id=$this->input->post("country_id",true);
        $check=$this->db->prepare("select * from service_cities where country_id=:country_id");
        $check->bindParam(":country_id",$country_id);
        $check->execute();
        while($row=$check->fetch()){
            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
        }
        exit;
    }
}