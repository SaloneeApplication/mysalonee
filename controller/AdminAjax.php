<?php
require_once dirname(__DIR__).'/core/Controller.php';
class AdminAjax extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='salesperson' && $this->auth_type!='admin' ){
            header("HTTP/1.1 403 Forbidden");
            exit;
        }
    }
    public function init(){
        
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='updateProfile'){
                $this->updateProfile();
            }elseif($type=='changePassword'){
                $this->changePassword();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }

    public function updateProfile(){
        $referer=$_SERVER['HTTP_REFERER'];
        $name=$this->input->post("name",true);
        // $email=$this->input->post("email",true);
        $mobile=$this->input->post("mobile",true);

        if($this->auth_type=='admin'){  

            // $emcheck=$this->db->prepare("select admin_id from admin where email=:email and admin_id!=:id");
            // $emcheck->bindParam(":id",$this->admin_id);
            // $emcheck->bindParam(":email",$email);
            // $emcheck->execute();

            // if($emcheck->rowCount()>0){
            //     $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Email Exist"));
            //     redirect($referer);
            //     exit;
            // }

            $emcheck=$this->db->prepare("select admin_id from admin where mobile=:mobile and admin_id!=:id");
            $emcheck->bindParam(":id",$this->admin_id);
            $emcheck->bindParam(":mobile",$mobile);
            $emcheck->execute();

            if($emcheck->rowCount()>0){
                $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Mobile Number Exist"));
                redirect($referer);
                exit;
            }

            $user=$this->db->prepare("select * from admin where admin_id=:id");
            $user->bindParam(":id",$this->admin_id);
            $user->execute();
            $user=$user->fetch();

            $image=$user['image'];
            if($_FILES['filUpload111']['size']>0){
                $tmp_name = $_FILES["filUpload111"]["tmp_name"];
                $file='uploads/admin/'.basename($_FILES["filUpload111"]["name"]);
                $path='uploads/admin/'.basename($_FILES["filUpload111"]["name"]);
                move_uploaded_file($tmp_name,$path);
                $image=$file;
            }

            $up=$this->db->prepare("update admin set name=:name,mobile=:mobile,image=:image where admin_id=:id");
            $up->bindParam(":id",$this->admin_id);
            // $up->bindParam(":email",$email);
            $up->bindParam(":name",$name);
            $up->bindParam(":mobile",$mobile);
            $up->bindParam(":image",$image);
            $up->execute();
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully profile updated"));
            redirect($referer);
            exit;

        }elseif($this->auth_type=='salesperson'){

            // $emcheck=$this->db->prepare("select salesperson_id from salesperson where email=:email and salesperson_id!=:id");
            // $emcheck->bindParam(":id",$this->admin_id);
            // $emcheck->bindParam(":email",$email);
            // $emcheck->execute();

            // if($emcheck->rowCount()>0){
            //     $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Email Exist"));
            //     redirect($referer);
            //     exit;
            // }

            $emcheck=$this->db->prepare("select salesperson_id from salesperson where mobile=:mobile and salesperson_id!=:id");
            $emcheck->bindParam(":id",$this->admin_id);
            $emcheck->bindParam(":mobile",$mobile);
            $emcheck->execute();

            if($emcheck->rowCount()>0){
                $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Mobile Number Exist"));
                redirect($referer);
                exit;
            }

            $user=$this->db->prepare("select * from salesperson where salesperson_id=:id");
            $user->bindParam(":id",$this->admin_id);
            $user->execute();
            $user=$user->fetch();

            $image=$user['image'];
            if($_FILES['filUpload111']['size']>0){
                $tmp_name = $_FILES["filUpload111"]["tmp_name"];
                $file='uploads/admin/'.basename($_FILES["filUpload111"]["name"]);
                $path='uploads/admin/'.basename($_FILES["filUpload111"]["name"]);
                move_uploaded_file($tmp_name,$path);
                $image=$file;
            }

            $up=$this->db->prepare("update salesperson set name=:name,mobile=:mobile,image=:image where salesperson_id=:id");
            $up->bindParam(":id",$this->admin_id);
            // $up->bindParam(":email",$email);
            $up->bindParam(":name",$name);
            $up->bindParam(":mobile",$mobile);
            $up->bindParam(":image",$image);
            $up->execute();
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully profile updated"));
            redirect($referer);
            exit;
        }else{
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Required"));
            redirect($referer);
            exit;
        }


    }
    

    public function changePassword(){
        $referer=$_SERVER['HTTP_REFERER'];
        $opass=$this->input->post("opass",true);
        $npass=$this->input->post("npass",true);
        $n1pass=$this->input->post("n1pass",true);

        if($npass!=$n1pass){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"New password and confirm new password entered wrong"));
            redirect($referer);
            exit;
        }
        if($this->auth_type=='admin'){
            
            $user=$this->db->prepare("select * from admin where admin_id=:id");
            $user->bindParam(":id",$this->admin_id);
            $user->execute();
            $user=$user->fetch();
            $oldpass=$user['password'];

            if($oldpass!=$opass){
                $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Wrong current password"));
                redirect($referer);
                exit;
            }

            $up=$this->db->prepare("update admin set password=:password where admin_id=:id");
            $up->bindParam(":id",$this->admin_id);
            $up->bindParam(":password",$npass);
            $up->execute();
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Password updated"));
            redirect($referer);
            exit;
        }elseif($this->auth_type=='salesperson'){

            $user=$this->db->prepare("select * from salesperson where salesperson_id=:id");
            $user->bindParam(":id",$this->admin_id);
            $user->execute();
            $user=$user->fetch();
            $oldpass=$user['password'];

            if($oldpass!=$opass){
                $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Wrong current password"));
                redirect($referer);
                exit;
            }

            $up=$this->db->prepare("update salesperson set password=:password where salesperson_id=:id");
            $up->bindParam(":id",$this->admin_id);
            $up->bindParam(":password",$npass);
            $up->execute();

            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Password updated"));
            redirect($referer);
            exit;
            
        }else{
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Required"));
            redirect($referer);
            exit;
        }


    }
}