<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminProductReports extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("product_reports")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='getTicketInfo'){
                $this->getTicketInfo();
            }elseif($type=='replyTicket'){
                $this->replyTicket();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }elseif($type=='approve_ad'){
                $this->approveAd();
            }
        }
    }
    public function getTicketInfo(){
        $tid=$this->input->post("tid",true);
        $od=$this->db->prepare("select * from support_tickets where ticket_id=:id");
        $od->bindParam(":id",$tid);
        $od->execute();
        $od=$od->fetch();
        echo '<div class="form-group">
        <h4 style="font-weight:600;">Issue Title</h4>
        <p>'.$od['title'].'</p>
        </div>';
        echo '<div class="form-group">
            <h4  style="font-weight:600;">Issue Title</h4>
            <p>'.$od['issue_description'].'</p>
        </div>';
        exit;
    }
    public function replyTicket(){

        $tid=$this->input->post("tid",true);
        $reply=$this->input->post("reply",true);
        $referer=$_SERVER['HTTP_REFERER'];

        $od=$this->db->prepare("select * from support_tickets where ticket_id=:id");
        $od->bindParam(":id",$tid);
        $od->execute();
        $od=$od->fetch();
        $uid=$od['user_id'];
        $ticket_id=$od['ticket_id'];

        $dbres1=$this->db->prepare("update support_tickets set closed_by_admin =:closed_by_admin,reply_desc=:reply_desc,status='close'  where ticket_id=:ticket_id");
        $dbres1->bindParam(":closed_by_admin",$this->admin_id);
        $dbres1->bindParam(":reply_desc",$reply);
        $dbres1->bindParam(":ticket_id",$ticket_id);
        $dbres1->execute();

        $che=$this->db->prepare("select email,name from user where user_id=:id");
        $che->bindParam(":id",$uid);
        $che->execute();
        $user=$che->fetch();
        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your Ticket [ #'.$ticket_id.' ] replied from Salonee <br/> ';
        $message.=$reply;
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Ticket Replied [ #".$ticket_id." ] | Salonee.",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully ticket replied and closed."));
        redirect($referer);

        exit;
    }

    public function pageData(){
        $data=array();
        $data['title']='Admin | Product Reports';
        $data['page']='product_reports';
        $data['admin_id']=$this->admin_id;
        $data['report']=$this->getReport();
        return $data;
    }
    public function getReport(){
        $report=$this->input->get("report",true);
        $pr=array();
       if($report=='custom_date'){
            $from=$this->input->get("from",true);
            $to=$this->input->get("to",true);
        }else{ 
            $from=date("Y-m-d",strtotime("-1 Month"));
            $to=date("Y-m-d");
        }
        $totals=$this->db->prepare("select p.product_title,p.id,p.service_provider_id,(select name from service_provider where service_provider_id=p.service_provider_id) as service_provider_name from products p where p.id in (select o.product_id from order_products o where o.product_id=p.id and o.order_id in (select order_id from orders where date_of_order BETWEEN :from and :to))");
        $totals->bindParam(":from",$from);
        $totals->bindParam(":to",$to);
        $totals->execute();

        while($row=$totals->fetch()){
            $pid=$row['id'];
            $service_provider_id=$row['service_provider_id'];
            $service_provider_name=$row['service_provider_name'];
            $product_title=$row['product_title'];

            $cancel=$this->db->prepare("select o.order_id from orders o where o.order_id in (select order_id from order_products where order_id=o.order_id and product_id=$pid) and  o.order_status='cancelled'");
            $cancel->execute();
            $cancel=$cancel->rowCount();

            $sold=$this->db->prepare("select o.order_id from orders o where o.order_id in (select order_id from order_products where order_id=o.order_id and product_id=$pid) and  o.order_status='completed' and o.payment_status='completed'");
            $sold->execute();
            $sold=$sold->rowCount();
            $pr[]=array("pid"=>$pid,"service_provider_id"=>"$service_provider_id","service_provider_name"=>"$service_provider_name","cancel"=>"$cancel","sold"=>"$sold","product_title"=>"$product_title");
        }
        return $pr;
    }
    public function getAdvertisements(){
        $adv=$this->input->get("adv",true);
        if($adv=='active'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='active' order by a.created_time desc");
        }elseif($adv=='pending'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='pending' order by a.created_time desc");
        }elseif($adv=='rejected'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='rejected' order by a.created_time desc");
        }elseif($adv=='expired'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='expired' order by a.created_time desc");
        }elseif($adv=='approved'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='approved' order by a.created_time desc");
        }else{
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a order by a.created_time desc");
        }
      
        $total_sb->execute();
        return $total_sb->fetchAll();
    }
}