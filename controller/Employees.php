<?php
require_once dirname(__DIR__).'/core/Controller.php';

class Employees extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='salesperson'){
            session_destroy();
            redirect("index.php");
            exit;
        }
    }
    public function load(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    
    }

    public function addServiceProvider(){
        $aid=$this->session->userdata("adminId");
        $name=$this->input->post("name",true);
        $business_name=$this->input->post("business_name",true);
        $email=$this->input->post("email",true);
        $mobile=$this->input->post("mobile",true);
        $address=$this->input->post("address",true);
        $city=$this->input->post("city",true);
        if($email==''){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Required"));
            header("location:".admin_base_url."sales-person.php");
            exit;
        }
        $che=$this->db->prepare("select email from service_provider where email=:email");
        $che->bindParam(":email",$email);
        $che->execute();
        $count=$che->rowCount();
        if($count>0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Exist"));
            header("location:".admin_base_url."sales-person.php");
            exit;
        }

        if(strpos($email,"@")==false){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Invalid Email"));
            header("location:".admin_base_url."sales-person.php");
            exit;
        }
        $che=$this->db->prepare("select mobile from service_provider where mobile=:mobile");
        $che->bindParam(":mobile",$mobile);
        $che->execute();
        $count=$che->rowCount();
        if($count>0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Mobile Number Exist"));
            header("location:".admin_base_url."sales-person.php");
            exit;
        }
        $tmp_name = $_FILES["image"]["tmp_name"];
        $file='uploads/certificates/'.basename($_FILES["image"]["name"]);
        $path='uploads/certificates/'.basename($_FILES["image"]["name"]);
        move_uploaded_file($tmp_name,$path);
     

        $password=$this->randomPassword(6);
        $dbres=$this->db->prepare("insert into service_provider (name,business_name,email,mobile,password,business_licence,status,city,address,user_type,referer_by) values (:name,:business_name,:email,:mobile,:password,:image,0,:city,:address,'main_branch','salesperson')");
        $dbres->bindParam(":name",$name);
        $dbres->bindParam(":business_name",$business_name);
        $dbres->bindParam(":email",$email);
        $dbres->bindParam(":mobile",$mobile);
        $dbres->bindParam(":password",$password);
        $dbres->bindParam(":image",$file);
        $dbres->bindParam(":city",$city);
        $dbres->bindParam(":address",$address);
        $dbres->execute();
        $spid=$this->db->lastInsertId();
        
        $dbress=$this->db->prepare("insert into service_provider_branches (service_provider_main_branch_id ,service_provider_id ,address,location_id,status) values (:service_provider_main_branch_id,:service_provider_id,:address,:location_id,0)");
        $dbress->bindParam(":service_provider_main_branch_id",$spid);
        $dbress->bindParam(":service_provider_id",$spid);
        $dbress->bindParam(":address",$address);
        $dbress->bindParam(":location_id",$city);
        $dbress->execute();


        $dbress=$this->db->prepare("insert into salespersons_service_providers (salesperson_id,service_provider_id) values (:sid,:spid)");
        $dbress->bindParam(":sid",$aid);
        $dbress->bindParam(":spid",$spid);
        $dbress->execute();

        //wallet creation
        $dbress=$this->db->prepare("insert into service_provider_wallet (service_provider_id,amount) values (:spid,0)");
        $dbress->bindParam(":spid",$spid);
        $dbress->execute();

        $message='';
        $message.='Hi Welcome to Salonee <br/>';
        $message.='Thanking you for registered with us<br/>';
        $message.='Your login details : <br/>';
        $message.='Email : '.$email.'<br/>';
        $message.='Password : '.$password.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Thank you for register us",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Service Provider added"));
        header("location:".admin_base_url."sales-person.php");
        exit;
    }
    public function getServiceProviders(){
        $admin_id=$this->session->userdata("adminId");
        $persons=$this->db->prepare("select s.* from service_provider s where s.service_provider_id in (select service_provider_id from salespersons_service_providers where salesperson_id=:id) order by s.created_time desc");
        $persons->bindParam(":id",$admin_id);
        $persons->execute();
        $resp=array();
        while($row=$persons->fetch()){
            $resp[]=$row;
        }
        return $resp;
    }
    public function pageData(){
        $data=array();
        $data['title']='Saleperson | Service Providers';
        $data['page']='service_providers';
        $data['admin_id']=$this->admin_id;
        return $data;
    }
    
    public function getCities(){
        $cities=$this->db->prepare("select * from cities where country_id=784 order by city_name_en asc");
        $cities->execute();
        $resp=array();
        while($row=$cities->fetch()){
            $resp[]=$row;
        }
        return $resp;
    }
}   