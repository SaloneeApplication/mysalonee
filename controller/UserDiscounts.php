<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserDiscounts extends Controller{
    public $user_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='updateServiceDiscount'){
                $this->updateDiscount();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }

    public function updateDiscount(){
        $id=$this->input->post("sp_id",true);
        $base_price=$this->input->post("base_price",true);
        $sale_price=$this->input->post("sale_price",true);

        $salonee_price_type=$this->input->post("salonee_price_type",true);
        $salonee_weekend_name_value=$this->input->post("salonee_weekend_name",true);
        $salonee_special_date=$this->input->post("salonee_special_date",true);
        $salonee_special_price=$this->input->post("salonee_special_price",true);
       
        $referer=$_SERVER['HTTP_REFERER'];
        $dbres=$this->db->prepare("update services_prices set base_price=:base_price,sale_price=:sale_price,price_type=:price_type where sp_id=:id and service_provider_service_id in (select service_provider_service_id from service_provider_services where service_provider_id=:uid)");
        $dbres->bindParam(":id",$id);
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->bindParam(":base_price",$base_price);
        $dbres->bindParam(":sale_price",$sale_price);
        $dbres->bindParam(":price_type",$salonee_price_type);
        $dbres->execute();

        $s=$this->db->prepare("select * from services_prices where sp_id=:id and  service_provider_service_id in (select service_provider_service_id from service_provider_services where service_provider_id=:uid) ");
        $s->bindParam(":id",$id);
        $s->bindParam(":uid",$this->user_id);
        $s->execute();
        $s=$s->fetch();
        $service_id=$s['service_provider_service_id'];
        $service_for=$s['price_for'];

        if($salonee_price_type=='custom_price_weekend'){
            $p=$this->db->prepare("delete from services_special_prices where sp_id=:id and service_provider_service_id in (select service_provider_service_id from service_provider_services where service_provider_id=:uid)");
            $p->bindParam(":id",$id);
            $p->bindParam(":uid",$this->user_id);
            $p->execute();
            foreach($salonee_weekend_name_value as $custom_on){
                $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                $dbres->bindParam(":service_provider_service_id",$service_id);
                $dbres->bindParam(":sp_id",$id);
                $dbres->bindParam(":price_for",$service_for);
                $dbres->bindParam(":type",$salonee_price_type);
                $dbres->bindParam(":custom_on",$custom_on);
                $dbres->bindParam(":sales_price",$salonee_special_price);
                $dbres->execute();
            }
        }elseif($salonee_price_type=='custom_price_special_date'){
            $p=$this->db->prepare("delete from services_special_prices where sp_id=:id and service_provider_service_id in (select service_provider_service_id from service_provider_services where service_provider_id=:uid)");
            $p->bindParam(":id",$id);
            $p->bindParam(":uid",$this->user_id);
            $p->execute();
            $salonee_special_date=explode(",",$salonee_special_date);
            foreach($salonee_special_date as $custom_on){
                $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                $dbres->bindParam(":service_provider_service_id",$service_id);
                $dbres->bindParam(":sp_id",$id);
                $dbres->bindParam(":price_for",$service_for);
                $dbres->bindParam(":type",$salonee_price_type);
                $dbres->bindParam(":custom_on",$custom_on);
                $dbres->bindParam(":sales_price",$salonee_special_price);
                $dbres->execute();
            }

        }elseif($salonee_price_type=='regular'){
            $p=$this->db->prepare("delete from services_special_prices where sp_id=:id and service_provider_service_id in (select service_provider_service_id from service_provider_services where service_provider_id=:uid)");
            $p->bindParam(":id",$id);
            $p->bindParam(":uid",$this->user_id);
            $p->execute();
        }


        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully service price discount updated"));
        redirect($referer);
        exit;

    }

    public function getServicesPrices(){
        $dbres=$this->db->prepare("select p.*,sp.service_name,sp.time_slot_freq from services_prices p ,service_provider_services sp where p.service_provider_service_id = sp.service_provider_service_id and sp.service_provider_id=:id");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->execute();
        return $dbres->fetchAll();
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Discounts';
        $data['page']='discounts';
        $data['user_id']=$this->user_id;
        return $data;
    }

    public function getSpecialPrices($spid){
        $dbres=$this->db->prepare("select * from services_special_prices where sp_id=:id");
        $dbres->bindParam(":id",$spid);
        $dbres->execute();
        return $dbres->fetchAll();
    }
}