<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminNavLinks extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
    }
    public function load(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addNavLinks'){
                $this->addNavLinks();
            }
            elseif($type=='getNavLinks'){
                $this->getNavLinks();
            }
        }else{
            
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Manage Content';
        $data['page']='Content';
        $data['admin_id']=$this->admin_id;
        return $data;
    }

    public function addNavLinks()
    {
        $referer=$_SERVER['HTTP_REFERER'];
        $language_id=$this->input->post("language_id",true);
        $locator=$this->input->post("locator",true);
        $login=$this->input->post("login",true);
        $register=$this->input->post("register",true);
        $profile=$this->input->post("profile",true);
        $my_account=$this->input->post("my_account",true);
        $logout=$this->input->post("logout",true);
        $booking_history=$this->input->post("booking_history",true);
        $schedule_booking=$this->input->post("schedule_booking",true);
        $change_password=$this->input->post("change_password",true);
        $notifications=$this->input->post("notifications",true);
        $discounts=$this->input->post("discounts",true);
        $join_us=$this->input->post("join_us",true);
        $about_us=$this->input->post("about_us",true);
        $compare_prices=$this->input->post("compare_prices",true);
        $features=$this->input->post("features",true);
        $categories=$this->input->post("categories",true);
        $blog=$this->input->post("blog",true);
        $how_it_works=$this->input->post("how_it_works",true);
        $download=$this->input->post("download",true);
        $contact=$this->input->post("contact",true);
        $partner_with_salonee=$this->input->post("partner_with_salonee",true);
        $terms_conditions=$this->input->post("terms_conditions",true);
        $privacy_policy=$this->input->post("privacy_policy",true);
        $faq=$this->input->post("faq",true);

        $check=$this->db->prepare("select * from nav_links where language_id=:language_id");
        $check->bindParam(":language_id",$language_id);
        $check->execute();

        if($check->rowCount()>0)
        {
            $cate=$this->db->prepare("update nav_links set
                                        locator = '$locator',
                                        join_us = '$join_us',
                                        login = '$login',
                                        profile = '$profile',
                                        my_account = '$my_account',
                                        logout = '$logout',
                                        booking_history = '$booking_history',
                                        schedule_booking = '$schedule_booking',
                                        change_password = '$change_password',
                                        notifications = '$notifications',
                                        discounts = '$discounts',
                                        register = '$register',
                                        about_us = '$about_us',
                                        compare_prices = '$compare_prices',
                                        features = '$features',
                                        categories = '$categories',
                                        blog = '$blog',
                                        how_it_works = '$how_it_works',
                                        download = '$download',
                                        contact = '$contact',
                                        partner_with_salonee = '$partner_with_salonee',
                                        privacy_policy = '$privacy_policy',
                                        terms_conditions = '$terms_conditions',
                                        faq = '$faq'
                                    where language_id = '$language_id' ");
            $cate->execute();

            //print_r($cate->queryString); die;
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Content updated successfully"));
            redirect($referer);
            exit;
        }
        
        $cate=$this->db->prepare("insert into nav_links 
                                (
                                    language_id,
                                    locator, 
                                    login, 
                                    register, 
                                    profile,
                                    my_account,
                                    logout,
                                    booking_history,
                                    schedule_booking,
                                    change_password,
                                    notifications,
                                    discounts,
                                    join_us,
                                    about_us, 
                                    features,
                                    compare_prices,
                                    categories,
                                    blog,
                                    how_it_works,
                                    download,
                                    contact,
                                    partner_with_salonee,
                                    privacy_policy,
                                    terms_conditions,
                                    faq
                                ) 
                                values 
                                (
                                    :language_id,
                                    :locator,
                                    :login,
                                    :register,
                                    :profile,
                                    :my_account,
                                    :logout,
                                    :booking_history,
                                    :schedule_booking,
                                    :change_password,
                                    :notifications,
                                    :discounts,
                                    :join_us,
                                    :about_us,
                                    :features,
                                    :compare_prices,
                                    :categories,
                                    :blog,
                                    :how_it_works,
                                    :download,
                                    :contact,
                                    :partner_with_salonee,
                                    :privacy_policy,
                                    :terms_conditions,
                                    :faq
                                )");
        $cate->bindParam(":language_id",$language_id);
        $cate->bindParam(":locator",$locator);
        $cate->bindParam(":login",$login);
        $cate->bindParam(":register",$register);
        $cate->bindParam(":join_us",$join_us);
        $cate->bindParam(":about_us",$about_us);
        $cate->bindParam(":features",$features);
        $cate->bindParam(":compare_prices",$compare_prices);
        $cate->bindParam(":categories",$categories);
        $cate->bindParam(":blog",$blog);
        $cate->bindParam(":how_it_works",$how_it_works);
        $cate->bindParam(":download",$download);
        $cate->bindParam(":contact",$contact);
        $cate->bindParam(":partner_with_salonee",$partner_with_salonee);
        $cate->bindParam(":privacy_policy",$privacy_policy);
        $cate->bindParam(":terms_conditions",$terms_conditions);
        $cate->bindParam(":faq",$faq);
        $cate->execute();

        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Content added successfully"));
        redirect($referer);
        exit;
    }

    public function getNavLinks(){
        $language_id = $this->input->post("language_id",true);

        $res=$this->db->prepare("select * from nav_links where language_id=:language_id");
        $res->bindParam(":language_id",$language_id);
        $res->execute();
        $res=$res->fetch();
        
        echo json_encode(array("status"=>"1","msg"=>"success","data"=>$res));
        exit;
    }

}