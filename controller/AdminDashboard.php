<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminDashboard extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
    }
    public function load(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Dashboard';
        $data['page']='dashboard';
        $data['admin_id']=$this->admin_id;
        
        $startdate=date("Y-m-d",strtotime("-10 days"));
        $enddate=date("Y-m-d");

        $slots_chart=array();
        $products_chart=array();
        for($i=0;$i<=10;$i++){
            $onhold=$this->db->prepare("select sum(total_amount) as total,sum(admin_commission) as admin_commission from orders where payment_status='completed' and order_status='completed' and order_type='slot' and cast(date_of_order as date)='$startdate'");
            $onhold->bindParam(":uid",$this->user_id);
            $onhold->execute();
            $onhold=$onhold->fetch();
            $total=$onhold['total'];
            $admin_commission=$onhold['admin_commission'];
            $slots_chart['dates'][]=$startdate;
            $slots_chart['amount'][]=($total=='')?0:$total;
            $slots_chart['admin_commission'][]=($admin_commission=='')?0:$admin_commission;
            

            $onhold=$this->db->prepare("select sum(total_amount) as total,sum(admin_commission) as admin_commission from orders where payment_status='completed' and order_status='completed' and order_type='product' and cast(date_of_order as date)='$startdate'");
            $onhold->bindParam(":uid",$this->user_id);
            $onhold->execute();
            $onhold=$onhold->fetch();
            $total=$onhold['total'];
            $admin_commission=$onhold['admin_commission'];
            $products_chart['dates'][]=$startdate;
            $products_chart['amount'][]=($total=='')?0:$total;
            $products_chart['admin_commission'][]=($admin_commission=='')?0:$admin_commission;
            $startdate=date("Y-m-d",strtotime("+1 day",strtotime($startdate)));
        }
        $data['products_chart']=$products_chart;
        $data['slots_chart']=$slots_chart;

        return $data;
    }

}