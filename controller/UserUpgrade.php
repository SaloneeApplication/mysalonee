<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserUpgrade extends Controller{
    public $user_id;
    public $auth_type;
    public $merchant_id="45990";
    public $access_code="AVFY03IE78BG61YFGB";
    public $working_key="08D12F6F9BC71D36B0A9F9D3ECFDEF57";
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if(@$_REQUEST['type']!='payment_response'){
            if($this->user_id==''){
                session_destroy();
                redirect("index.php?session");
                exit;
            }
        }
    }
    public function init(){ 
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='insertPromocode'){
                $this->insertPromocode();
            }elseif($type=='upgradePurchase'){
                $this->upgradePurchase();
            }
            if(@$_GET['type']=='payment_response'){
                $this->paymentResponse();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='deletePromo'){
                $this->deletePromocode();
            }elseif($type=='payment_response'){
                $this->paymentResponse();
            }
        }
    }

    public function upgradePurchase(){
        $referer=$_SERVER['HTTP_REFERER'];

        $plan_id=$this->input->post("plan_id",true);
        $plan=$this->db->prepare("select * from subscription_plans where subscription_plan_id=:pid");
        $plan->bindParam(":pid",$plan_id);
        $plan->execute();
        $plan=$plan->fetch();

        $commission=0;
        $total_amount=$plan['price'];
        $used_dates=json_encode(array());
        $order_id="S".$this->user_id.time();
        $dbres1=$this->db->prepare("insert into orders (order_id,service_provider_id,user_id,user_type,total_amount,admin_commission,payment_status,order_type,order_status,membership_plan_id)values(:order_id,:uid,:uid,'service_provider',:amount,:commission,'pending','membership','pending',:plan)");
        $dbres1->bindParam(":uid",$this->user_id);
        $dbres1->bindParam(":order_id",$order_id);
        $dbres1->bindParam(":amount",$total_amount);
        $dbres1->bindParam(":commission",$commission);
        $dbres1->bindParam(":plan",$plan_id);
        $dbres1->execute();

        $user=$this->db->prepare("select s.*,c.city_name_en,(select name from countries where id=c.country_id) as country_name from service_provider s,cities c where s.city=c.id and s.service_provider_id=:service_provider_id");
        $user->bindParam(":service_provider_id",$this->user_id);
        $user->execute();
        $user=$user->fetch();
        $encid=$order_id.'~'.$this->user_id;
        $encid=$this->encrypt($encid);
        $encid=base64_encode($encid);
        $encid=rawurlencode($encid);
        setcookie("encid", $encid, time()+30*24*60*60,"/",$_SERVER['HTTP_HOST']);

        $ccavenue=array();
        $ccavenue['merchant_id']=$this->merchant_id;
        $ccavenue['order_id']=$order_id;
        $ccavenue['amount']=$total_amount;
        $ccavenue['currency']="AED";
        $ccavenue['redirect_url']=user_base_url."upgrade.php?type=payment_response&encid=".$encid;
        $ccavenue['cancel_url']=user_base_url."upgrade.php?type=payment_response&encid=".$encid;
        $ccavenue['billing_name']=$user['name'];
        $ccavenue['billing_email']=$user['email'];
        $ccavenue['billing_address']=$user['address'];
        $ccavenue['billing_city']=$user['city_name_en'];
        $ccavenue['billing_tel']=$user['mobile'];
        $ccavenue['billing_country']=$user['country_name'];

        $merchant_data='';
        foreach ($ccavenue as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }
        $encrypted_data=$this->ccavenue_encrypt($merchant_data,$this->working_key);

        $resp=array();
        $resp['encrypted_data']=$encrypted_data;
        $resp['access_code']=$this->access_code;
        $resp['status']='200';
        $resp['action']='https://secure.ccavenue.ae/transaction/transaction.do?command=initiateTransaction';
        echo json_encode($resp);
        exit;
    }

    public function paymentResponse(){
        $encResponse=$_POST["encResp"];	
        if($_SERVER['REQUEST_METHOD']!='POST'){
            $this->session->set_userdata(array("alert_type"=>"warning","alert_msg"=>"Invalid Request"));
            redirect(user_base_url."dashboard.php");
            exit;
        }		
        $rcvdString=$this->ccavenue_decrypt($encResponse,$this->working_key);		
        $order_status="";
        $decryptValues=explode('&', $rcvdString);
        $order_status=$decryptValues[3];//order_status
        $order_status=explode("=",$order_status);
        $order_status=$order_status[1];

        if($order_status=="Success"){
            //payment success
            $order_id=$decryptValues[0];//order_id
            $order_id=explode("=",$order_id);
            $order_id=$order_id[1];

            $tracking_id=$decryptValues[1];//tracking_id
            $tracking_id=explode("=",$tracking_id);
            $tracking_id=$tracking_id[1];
            $encid=@$_GET['encid'];
            $append="";
            if($encid==''){
                $encid=$_COOKIE['encid'];
            }
            $encid=rawurldecode($encid);
            $encid=base64_decode($encid);
            $encid=$this->decrypt($encid);
            $encid=explode("~",$encid);
            $user_id=$encid[1];
            $ex_order_id=$encid[0];
            if($this->user_id==''){
                $append="exusr";
                //when session expiry 
                $usr=$this->db->prepare("select * from service_provider where service_provider_id=:user_id");
                $usr->bindParam(":user_id",$user_id);
                $usr->execute();
                $usr=$usr->fetch();
                $_SESSION["serviceProviderId"] = $usr['service_provider_id'];
				$_SESSION["branch_type"] = $usr['user_type'];
                $this->user_id=$this->session->userdata("serviceProviderId");
                $this->branch_type=$this->session->userdata("branch_type");
                
            }


            $order=$this->db->prepare("select * from orders where order_id=:order_id");
            $order->bindParam(":order_id",$order_id);
            $order->execute();
            $order=$order->fetch();

            if($order['payment_status']=='completed'){
                $this->session->set_userdata(array("alert_type"=>"warning","alert_msg"=>"Invalid Request"));
                redirect(user_base_url."dashboard.php");
                exit;
            }

            $dbres=$this->db->prepare("update orders set payment_status='completed',order_status='completed',txn_id=:txn_id where order_id=:order_id");
            $dbres->bindParam(":order_id",$order_id);
            $dbres->bindParam(":txn_id",$tracking_id);
            $dbres->execute();

            $plan_id=$order['membership_plan_id'];

            $plan=$this->db->prepare("select * from subscription_plans where subscription_plan_id=:pid");
            $plan->bindParam(":pid",$plan_id);
            $plan->execute();
            $plan=$plan->fetch();
            
            $free_advertisements_days=$plan['free_advertisements_days'];
            $free_featured_profiles_days=$plan['free_featured_profiles_days'];
            $duration=$plan['duration'];

            $user=$this->db->prepare("select * from service_provider where service_provider_id=:uid");
            $user->bindParam(":uid",$this->user_id);
            $user->execute();
            $user=$user->fetch();

            $expdate='0000-00-00';
            if($user['membership_expiry']=='0000-00-00'){
                $expdate=date("Y-m-d",strtotime("+$duration months"));
            }elseif($user['membership_expiry']<date("Y-m-d")){
                $expdate=date("Y-m-d",strtotime("+$duration months"));
                
            }elseif($user['membership_expiry']>=date("Y-m-d")){
                $expdate=date("Y-m-d",strtotime("+$duration months",strtotime($user['membership_expiry'])));
            }

            $dbres=$this->db->prepare("update service_provider set membership_expiry=:expdate where service_provider_id=:uid");
            $dbres->bindParam(":uid",$this->user_id);
            $dbres->bindParam(":expdate",$expdate);
            $dbres->execute();
            if($free_featured_profiles_days>0){
                $used_dates=json_encode(array());
                $total_amount=0;
                $dbres=$this->db->prepare("insert into featured_profiles (service_provider_id,days,used_dates,amount,order_id,status,total_days) values (:uid,:days,:used_dates,:amount,:order_id,'disable',:total_days)");
                $dbres->bindParam(":uid",$this->user_id);
                $dbres->bindParam(":days",$free_featured_profiles_days);
                $dbres->bindParam(":total_days",$free_featured_profiles_days);
                $dbres->bindParam(":used_dates",$used_dates);
                $dbres->bindParam(":amount",$total_amount);
                $dbres->bindParam(":order_id",$order_id);
                $dbres->execute();
            }
            if($free_advertisements_days>0){
                $total_amount=0;
                $dbress=$this->db->prepare("insert into advertisements (service_provider_id,days,order_id,status,amount,approve_status) values (:service_provider_id,:days,:order_id,'disable',:amount,'pending')");
                $dbress->bindParam(":service_provider_id",$this->user_id);
                $dbress->bindParam(":days",$free_advertisements_days);
                $dbress->bindParam(":order_id",$order_id);
                $dbress->bindParam(":amount",$total_amount);
                $dbress->execute();
            }

            $dbress=$this->db->prepare("insert into service_provider_subscriptions (service_provider_id,plan_name,duration_months,plan_amount,free_advertisements_days,free_featured_profiles_days,order_id) values (:service_provider_id,:plan_name,:duration_months,:plan_amount,:free_advertisements_days,:free_featured_profiles_days,:order_id)");
            $dbress->bindParam(":service_provider_id",$this->user_id);
            $dbress->bindParam(":plan_name",$plan['name']);
            $dbress->bindParam(":duration_months",$plan['duration']);
            $dbress->bindParam(":plan_amount",$plan['price']);
            $dbress->bindParam(":free_advertisements_days",$free_advertisements_days);
            $dbress->bindParam(":free_featured_profiles_days",$free_featured_profiles_days);
            $dbress->bindParam(":order_id",$order_id);
            $dbress->execute();

            $sms_msg="Successfully your salonee membership purchased. thanking you.";
            $mobile=$user['mobile'];
            $country_code=$user['country_code'];
            $phone=$country_code.$mobile;
            $this->sendSMS($phone,$sms_msg);

            $email=$user['email'];
            $name=$user['name'];
            $message='';
            $message.='Hi '.$name.'<br/>';
            $message.='Successfully your membership purchased your order id '.$order_id.' <br/>';
            $message.='<br/>Thanking you';
            $this->phpmailer->sendMail($email,"Membership Purchased | Salonee.",$message);
            setcookie("encid", "", time()+30*24*60*60,"/",$_SERVER['HTTP_HOST']);
            $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Membership purchased"));
            redirect(user_base_url."upgrade.php?",$append);
            exit;

        }else{
            //payment failed
            $encid=@$_GET['encid'];
            if($encid==''){
                $encid=$_COOKIE['encid'];
            }
            $encid=rawurldecode($encid);
            $encid=base64_decode($encid);
            $encid=$this->decrypt($encid);
            $encid=explode("~",$encid);
            $user_id=$encid[1];
            $ex_order_id=$encid[0];
            $append="";
            if($this->user_id==''){
            $append="ex";
            //when session expiry 
                $usr=$this->db->prepare("select * from service_provider where service_provider_id=:user_id");
                $usr->bindParam(":user_id",$user_id);
                $usr->execute();
                $usr=$usr->fetch();
                $_SESSION["serviceProviderId"] = $usr['service_provider_id'];
				$_SESSION["branch_type"] = $usr['user_type'];
                $this->user_id=$this->session->userdata("serviceProviderId");
                $this->branch_type=$this->session->userdata("branch_type");
                
                setcookie("encid", "", time()+30*24*60*60,"/",$_SERVER['HTTP_HOST']);
            }
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Payment Failed."));
            redirect(user_base_url."upgrade.php?frompage&".$append);
            exit;
        }
        
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Upgrade';
        $data['page']='upgrade';
        $data['user_id']=$this->user_id;
        return $data;
    }
 
    public function getPlans(){
        $dbres=$this->db->prepare("select * from subscription_plans where status=1 order by duration asc");
        $dbres->execute();
        return $dbres->fetchAll();
    }
    public function purchaseMembership(){
        exit;
        $plan_id=$this->input->post("plan_id",true);

        $plan=$this->db->prepare("select * from subscription_plans where subscription_plan_id=:pid");
        $plan->bindParam(":pid",$plan_id);
        $plan->execute();
        $plan=$plan->fetch();

        $commission=0;
        $total_amount=$plan['price'];
        $used_dates=json_encode(array());
        $order_id="S".$this->user_id.time();
        $dbres1=$this->db->prepare("insert into orders (order_id,service_provider_id,user_id,user_type,total_amount,admin_commission,payment_status,order_type,order_status,remark)values(:order_id,:uid,:uid,'service_provider',:amount,:commission,'completed','membership','completed','testing')");
        $dbres1->bindParam(":uid",$this->user_id);
        $dbres1->bindParam(":order_id",$order_id);
        $dbres1->bindParam(":amount",$total_amount);
        $dbres1->bindParam(":commission",$commission);
        $dbres1->execute();

        $free_advertisements_days=$plan['free_advertisements_days'];
        $free_featured_profiles_days=$plan['free_featured_profiles_days'];
        $duration=$plan['duration'];

        $user=$this->db->prepare("select * from service_provider where service_provider_id=:uid");
        $user->bindParam(":uid",$this->user_id);
        $user->execute();
        $user=$user->fetch();

        $expdate='0000-00-00';
        if($user['membership_expiry']=='0000-00-00'){
            $expdate=date("Y-m-d",strtotime("+$duration months"));
        }elseif($user['membership_expiry']<date("Y-m-d")){
            $expdate=date("Y-m-d",strtotime("+$duration months"));
            
        }elseif($user['membership_expiry']>=date("Y-m-d")){
            $expdate=date("Y-m-d",strtotime("+$duration months",strtotime($user['membership_expiry'])));
        }

        $dbres=$this->db->prepare("update service_provider set membership_expiry=:expdate where service_provider_id=:uid");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->bindParam(":expdate",$expdate);
        $dbres->execute();
        if($free_featured_profiles_days>0){
            $used_dates=json_encode(array());
            $total_amount=0;
            $dbres=$this->db->prepare("insert into featured_profiles (service_provider_id,days,used_dates,amount,order_id,status,total_days) values (:uid,:days,:used_dates,:amount,:order_id,'disable',:total_days)");
            $dbres->bindParam(":uid",$this->user_id);
            $dbres->bindParam(":days",$free_featured_profiles_days);
            $dbres->bindParam(":total_days",$free_featured_profiles_days);
            $dbres->bindParam(":used_dates",$used_dates);
            $dbres->bindParam(":amount",$total_amount);
            $dbres->bindParam(":order_id",$order_id);
            $dbres->execute();
        }
        if($free_advertisements_days>0){
            $total_amount=0;
            $dbress=$this->db->prepare("insert into advertisements (service_provider_id,days,order_id,status,amount,approve_status) values (:service_provider_id,:days,:order_id,'disable',:amount,'pending')");
            $dbress->bindParam(":service_provider_id",$this->user_id);
            $dbress->bindParam(":days",$free_advertisements_days);
            $dbress->bindParam(":order_id",$order_id);
            $dbress->bindParam(":amount",$total_amount);
            $dbress->execute();
        }

        $dbress=$this->db->prepare("insert into service_provider_subscriptions (service_provider_id,plan_name,duration_months,plan_amount,free_advertisements_days,free_featured_profiles_days,order_id) values (:service_provider_id,:plan_name,:duration_months,:plan_amount,:free_advertisements_days,:free_featured_profiles_days,:order_id)");
        $dbress->bindParam(":service_provider_id",$this->user_id);
        $dbress->bindParam(":plan_name",$plan['name']);
        $dbress->bindParam(":duration_months",$plan['duration']);
        $dbress->bindParam(":plan_amount",$plan['price']);
        $dbress->bindParam(":free_advertisements_days",$free_advertisements_days);
        $dbress->bindParam(":free_featured_profiles_days",$free_featured_profiles_days);
        $dbress->bindParam(":order_id",$order_id);
        $dbress->execute();

        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Membership purchased"));
        redirect(user_base_url."dashboard.php");
        exit;
    }

    
}