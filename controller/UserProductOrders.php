<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserProductOrders extends Controller{
    public $user_id;
    public $branch_type;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }elseif($type=='updateOrderStatus'){
                $this->updateOrderStatus();
            }elseif($type=='getOrderedProducts'){
                $this->getOrderedProductsData();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Product Orders';
        $data['page']='product_orders';
        $data['user_id']=$this->user_id;
        return $data;
    }

    public function getOrderedProducts(){
        $cate=$this->db->prepare("select o.*,if(o.order_type='product',(select count(id) from order_products where order_id=o.order_id),'') as product_name,if(o.order_type='product',(select name from user where user_id=o.user_id),'') as customer_name from orders o where o.service_provider_id=:uid and o.user_type='user' and o.order_type='product' and o.payment_status='completed' and o.shipping_status!='delivered'");
        $cate->bindParam(":uid",$this->user_id);
        $cate->execute();
        // echo json_encode($cate->fetchAll());
        return $cate->fetchAll();
    }
    public function updateOrderStatus(){
        $referer=$_SERVER['HTTP_REFERER'];
        $oid=$this->input->post("oid",true);
        $order_status=$this->input->post("order_status",true);
        $order_track=$this->input->post("order_track",true);

        $dbre1s2=$this->db->prepare("select * from orders where order_id=:uid");
        $dbre1s2->bindParam(":uid",$oid);
        $dbre1s2->execute();
        $pp=$dbre1s2->fetch();

        if($pp['shipping_status']=='delivered'){
            $this->session->set_userdata(array("alert_type"=>"warning","alert_msg"=>"Product Status updated already."));
            redirect($referer);
            exit;
        }
        if($order_status=='delivered'){

            //amount update

            $dbre1s=$this->db->prepare("select * from orders where order_id=:uid");
            $dbre1s->bindParam(":uid",$oid);
            $dbre1s->execute();
            $book=$dbre1s->fetch();

            $dbres=$this->db->prepare("select amount from service_provider_wallet where service_provider_id=:uid");
            $dbres->bindParam(":uid",$this->user_id);
            $dbres->execute();
            $dbres=$dbres->fetch();
            $now= $dbres['amount'];

            $amount=$book['total_amount'];
            $now=$now+$book['total_amount'];

            $dbres1=$this->db->prepare("update service_provider_wallet set amount=:now where service_provider_id=:uid");
            $dbres1->bindParam(":uid",$this->user_id);
            $dbres1->bindParam(":now",$now);
            $dbres1->execute();

            $date=date("Y-m-d");
            $dbres2=$this->db->prepare("insert into service_provder_wallet_transactions (service_provider_id ,amount,type,amount_type,order_id ,date) values (:uid,:request_amount,'credit','slot',:wid,:date)") ;
            $dbres2->bindParam(":uid",$this->user_id);
            $dbres2->bindParam(":request_amount",$amount);
            $dbres2->bindParam(":wid",$oid);
            $dbres2->bindParam(":date",$date);
            $dbres2->execute();
            //end amount update

        }
        
        if($order_status=='shipped'){
            $da=$this->db->prepare("update orders set shipping_status=:shipping ,tracking_id=:tid where order_id=:oid and service_provider_id=:uid ");
            $da->bindParam(":uid",$this->user_id);
            $da->bindParam(":shipping",$order_status);
            $da->bindParam(":tid",$order_track);
            $da->bindParam(":oid",$oid);
            $da->execute();
        }elseif($order_status=='delivered'){
            $da=$this->db->prepare("update orders set shipping_status=:shipping ,order_status='completed' where order_id=:oid and service_provider_id=:uid ");
            $da->bindParam(":uid",$this->user_id);
            $da->bindParam(":shipping",$order_status);
            $da->bindParam(":oid",$oid);
            $da->execute();
        }else{
            $da=$this->db->prepare("update orders set shipping_status=:shipping  where order_id=:oid and service_provider_id=:uid ");
            $da->bindParam(":uid",$this->user_id);
            $da->bindParam(":shipping",$order_status);
            $da->bindParam(":oid",$oid);
            $da->execute();
        }
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully order status updated"));
        redirect($referer);
        exit;
    }

    public function getOrderedProductsData(){
        $oid=$this->input->post("oid",true);
        echo '<tr><th>Product Name</th><th>Image</th><th>Price</th><th>QTY</th></tr>';
        $db=$this->db->prepare("select o.*,p.product_title,p.product_image from order_products o ,products p where o.product_id=p.id and o.order_id=:oid");
        $db->bindParam(":oid",$oid);
        $db->execute();
        while($row=$db->fetch()){
            echo '<tr>';
                echo '<td>'.$row['product_title'].'</td>';
                echo '<td><img src="'.user_base_url.$row['product_image'].'" width="100px" /></td>';
                echo '<td>'.$row['total_amount'].'</td>';
                echo '<td>'.$row['qty'].'</td>';
            echo '/<tr>';

        }
        exit;
    }
}