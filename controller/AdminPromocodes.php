<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminPromocodes extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("promo_codes")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='insertPromocode'){
                $this->insertPromocode();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='deletePromo'){
                $this->deletePromocode();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Promocodes';
        $data['page']='promocodes';
        $data['admin_id']=$this->admin_id;
        return $data;
    }
    public function getPromocodes(){
        $dbres=$this->db->prepare("select * from promocodes order by created_time");
        $dbres->execute();
        return $dbres->fetchAll();
    }
    
    public function insertPromocode(){
        $referer=$_SERVER['HTTP_REFERER'];
      
        $code=$this->input->post("code",true);
        $desc=$this->input->post("desc",true);
        $discount=$this->input->post("discount",true);
        $only_for=$this->input->post("only_for",true);
        $expdate=$this->input->post("expdate",true);

        $tmp_name = $_FILES["upload_cont_img"]["tmp_name"];
        $path='uploads/promocodes/'.basename($_FILES["upload_cont_img"]["name"]);
        $path1='uploads/promocodes/'.basename($_FILES["upload_cont_img"]["name"]);
        move_uploaded_file($tmp_name,$path1);
        $image=$path;

        $check=$this->db->prepare("select * from promocodes where coupon_code=:coupon_code");
        $check->bindParam(":coupon_code",$code);
        $check->execute();
        if($check->rowCount()>0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Invalid Coupon Code or Coupon Code Exists"));
            redirect($referer);
            exit;
        }
        $sid=0;
        $dbress=$this->db->prepare("insert into promocodes (service_provider_id,coupon_code ,short_desc,image,discount_per,only_for,expiry_date,status,from_added) values (:service_provider_id,:coupon_code,:short_desc,:image,:discount_per,:only_for,:expiry_date,1,'service_provider')");
        $dbress->bindParam(":service_provider_id",$sid);
        $dbress->bindParam(":coupon_code",$code);
        $dbress->bindParam(":short_desc",$desc);
        $dbress->bindParam(":image",$image);
        $dbress->bindParam(":discount_per",$discount);
        $dbress->bindParam(":only_for",$only_for);
        $dbress->bindParam(":expiry_date",$expdate);
        $dbress->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully promocode added"));
        redirect($referer);
    }
    
    public function deletePromocode(){
        $referer=$_SERVER['HTTP_REFERER'];
        $code=$this->input->get("id",true);
        $dbres=$this->db->prepare("delete from promocodes where id=:code ");
        $dbres->bindParam(":code",$code);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully promocode removed"));
        redirect($referer);
        exit;
    }
}