<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminProducts extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("products")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }elseif($type=='refundOrderID'){
                $this->refundOrderID();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='blockProduct'){
                $this->blockProduct();
            }elseif($type=='activeProduct'){
                $this->activeProduct();
            }
        }
    }
    
    
    public function activeProduct(){
        $referer=$_SERVER['HTTP_REFERER'];

        $oid=$this->input->get("id",true);
        
        $od=$this->db->prepare("select service_provider_id,product_title from products where id=:id ");
        $od->bindParam(":id",$oid);
        $od->execute();
        $od=$od->fetch();
        $pnname=$od['product_title'];

        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$od['service_provider_id']);
        $che->execute();
        $user=$che->fetch();

        $up=$this->db->prepare("update products set product_status='active' where id=:oid");
        $up->bindParam(":oid",$oid);
        $up->execute();
        

        $sms_msg="Your salonee listed product has been activated and ready for sale.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);


        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your listed product activated  [ '.$pnname.' ] <br/>
        for more information contact us
        ';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Listed Product activated | Salonee ",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Product Activated"));
        redirect($referer);
        exit;

    }

    public function blockProduct(){
        $referer=$_SERVER['HTTP_REFERER'];

        $oid=$this->input->get("id",true);
        
        $od=$this->db->prepare("select service_provider_id,product_title from products where id=:id ");
        $od->bindParam(":id",$oid);
        $od->execute();
        $od=$od->fetch();
        $pnname=$od['product_title'];

        $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $che->bindParam(":id",$od['service_provider_id']);
        $che->execute();
        $user=$che->fetch();

        $up=$this->db->prepare("update products set product_status='block' where id=:oid");
        $up->bindParam(":oid",$oid);
        $up->execute();

        $sms_msg="Your salonee listed product has been blocked.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your listed product blocked  [ '.$pnname.' ] <br/>
        for more information contact us
        ';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Listed Product Blocked | Salonee ",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Product Blocked"));
        redirect($referer);
        exit;

    }

    public function pageData(){
        $data=array();
        $data['title']='Admin | Products ';
        $data['page']='products';
        $data['admin_id']=$this->admin_id;
        $data['products']=$this->getProducts();
        return $data;
    }
    public function getProducts(){
        $adv=$this->input->get("pro",true);
        if($adv=='active'){
            $total_sb=$this->db->prepare("select p.*,s.name,concat('".base_url."','service_provider/',p.product_image) as product_img,(select count(order_id) from order_products where product_id=p.id and order_id in (select order_id from orders where payment_status='completed' and order_status='completed')) as total_sold from products p ,service_provider s where p.service_provider_id=p.service_provider_id and p.product_status='active' group by id order by p.created_time desc");
        }elseif($adv=='inactive'){
            $total_sb=$this->db->prepare("select p.*,s.name,concat('".base_url."','service_provider/',p.product_image) as product_img,(select count(order_id) from order_products where product_id=p.id and order_id in (select order_id from orders where payment_status='completed' and order_status='completed')) as total_sold from products p ,service_provider s where p.service_provider_id=p.service_provider_id and p.product_status='inactive' group by id order by p.created_time desc");
        }elseif($adv=='blocked'){
            $total_sb=$this->db->prepare("select p.*,s.name,concat('".base_url."','service_provider/',p.product_image) as product_img,(select count(order_id) from order_products where product_id=p.id and order_id in (select order_id from orders where payment_status='completed' and order_status='completed')) as total_sold from products p ,service_provider s where p.service_provider_id=p.service_provider_id  and p.product_status='block' group by id order by p.created_time desc");
        }else{ 
            $total_sb=$this->db->prepare("select p.*,s.name,concat('".base_url."','service_provider/',p.product_image) as product_img,(select count(order_id) from order_products where product_id=p.id and order_id in (select order_id from orders where payment_status='completed' and order_status='completed')) as total_sold from products p ,service_provider s where p.service_provider_id=p.service_provider_id group by id order by p.created_time desc");
        }
       
        $total_sb->execute();
        return $total_sb->fetchAll();
    }
    public function getAdvertisements(){
        $adv=$this->input->get("adv",true);
        if($adv=='active'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='active' order by a.created_time desc");
        }elseif($adv=='pending'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='pending' order by a.created_time desc");
        }elseif($adv=='rejected'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='rejected' order by a.created_time desc");
        }elseif($adv=='expired'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='expired' order by a.created_time desc");
        }elseif($adv=='approved'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='approved' order by a.created_time desc");
        }else{
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a order by a.created_time desc");
        }
      
        $total_sb->execute();
        return $total_sb->fetchAll();
    }
}

