<?php
require_once dirname(__DIR__).'/core/Controller.php';
class UserAjax extends Controller{
    public $user_id;
    public $branch_type;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }   
        
    }
    public function init(){
        
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='updateProfile'){
                $this->updateProfile();
            }elseif($type=='changePassword'){
                $this->changePassword();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }
        }
    }

    public function updateProfile(){
        $referer=$_SERVER['HTTP_REFERER'];
        $name=$this->input->post("name",true);
        $business_name=$this->input->post("businessname",true);
        $mobile=$this->input->post("mobile",true);

        $emcheck=$this->db->prepare("select service_provider_id from service_provider where mobile=:mobile and service_provider_id!=:id");
        $emcheck->bindParam(":id",$this->admin_id);
        $emcheck->bindParam(":mobile",$mobile);
        $emcheck->execute();

        if($emcheck->rowCount()>0){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Mobile Number Exist"));
            redirect($referer);
            exit;
        }

        $user=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $user->bindParam(":id",$this->user_id);
        $user->execute();
        $user=$user->fetch();

        $image=$user['image'];
        if($_FILES['filUpload111']['size']>0){
            $tmp_name = $_FILES["filUpload111"]["tmp_name"];
            $file='uploads/'.basename($_FILES["filUpload111"]["name"]);
            $path='uploads/'.basename($_FILES["filUpload111"]["name"]);
            move_uploaded_file($tmp_name,$path);
            $image=$file;
        }

        $up=$this->db->prepare("update service_provider set name=:name,mobile=:mobile,image=:image,business_name=:business_name where service_provider_id=:id");
        $up->bindParam(":id",$this->user_id);
        // $up->bindParam(":email",$email);
        $up->bindParam(":name",$name);
        $up->bindParam(":mobile",$mobile);
        $up->bindParam(":business_name",$business_name);
        $up->bindParam(":image",$image);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully profile updated"));
        redirect($referer);
        exit;
    }
    

    public function changePassword(){
        $referer=$_SERVER['HTTP_REFERER'];
        $opass=$this->input->post("opass",true);
        $npass=$this->input->post("npass",true);
        $n1pass=$this->input->post("n1pass",true);

        if($npass!=$n1pass){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"New password and confirm new password entered wrong"));
            redirect($referer);
            exit;
        }
        $user=$this->db->prepare("select * from service_provider where service_provider_id=:id");
        $user->bindParam(":id",$this->user_id);
        $user->execute();
        $user=$user->fetch();
        $oldpass=$user['password'];

        if($oldpass!=$opass){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Wrong current password"));
            redirect($referer);
            exit;
        }

        $up=$this->db->prepare("update service_provider set password=:password where service_provider_id=:id");
        $up->bindParam(":id",$this->user_id);
        $up->bindParam(":password",$npass);
        $up->execute();

        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Password updated"));
        redirect($referer);
        exit;

    }
}