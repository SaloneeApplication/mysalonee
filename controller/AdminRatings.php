<?php
require_once dirname(__DIR__).'/core/Controller.php';
class AdminRatings extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }

        if(!$this->getAdminPageAccess("ratings")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect("dashboard.php");
            exit;
        }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }elseif($type=='refundOrderID'){
                $this->refundOrderID();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='deleteRating'){
                $this->deleteRating();
            }elseif($type=='restoreRating'){
                $this->restoreRating();
            }
        }
    }
    
    public function restoreRating(){
        $referer=$_SERVER['HTTP_REFERER'];
        $oid=$this->input->get("id",true);
        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update ratings set status=1 , deleted_on='' where id=:oid");
        $up->bindParam(":oid",$oid);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully rating restored "));
        redirect($referer);
        exit;

    }
    
    public function deleteRating(){
        $referer=$_SERVER['HTTP_REFERER'];
        $oid=$this->input->get("id",true);
        $date=date("Y-m-d H:i:s");
        $up=$this->db->prepare("update ratings set status=2 , deleted_on='$date' where id=:oid");
        $up->bindParam(":oid",$oid);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully rating deleted "));
        redirect($referer);
        exit;

    }

    public function pageData(){
        $data=array();
        $data['title']='Admin | Products ';
        $data['page']='ratings';
        $data['admin_id']=$this->admin_id;
        $data['ratings']=$this->getRatings();
        return $data;
    }
    public function getRatings(){
        $adv=$this->input->get("pro",true);
        if($adv=='deleted'){
            $cate=$this->db->prepare("select r.*,if(user_id!=0,(select name from user where user_id=r.user_id),'') as user_name,if(r.service_id!=0,(select service_name from service_provider_services where service_provider_service_id=r.service_id),'') as service_name from ratings r where r.status=2 order by r.created_time desc ");
        }else{
            $cate=$this->db->prepare("select r.*,if(user_id!=0,(select name from user where user_id=r.user_id),'') as user_name,if(r.service_id!=0,(select service_name from service_provider_services where service_provider_service_id=r.service_id),'') as service_name from ratings r where r.status=1 order by r.created_time desc ");
        }
     
        $cate->execute();
        return $cate->fetchAll();
    }
    public function getAdvertisements(){
        $adv=$this->input->get("adv",true);
        if($adv=='active'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='active' order by a.created_time desc");
        }elseif($adv=='pending'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='pending' order by a.created_time desc");
        }elseif($adv=='rejected'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='rejected' order by a.created_time desc");
        }elseif($adv=='expired'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.status='expired' order by a.created_time desc");
        }elseif($adv=='approved'){
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a where a.approve_status='approved' order by a.created_time desc");
        }else{
            $total_sb=$this->db->prepare("select a.*,if(a.city_id!='',(select city_name_en from cities where id=a.city_id),'') as city_name from advertisements a order by a.created_time desc");
        }
      
        $total_sb->execute();
        return $total_sb->fetchAll();
    }
}