<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminCron extends Controller{
    public function __construct(){
        parent::__construct();
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->get("type",true);
            
        }else{
            $type=$this->input->get("type",true);
            if($type=='membership_expiry'){
                $this->membershipExpiry();
            }elseif($type=='membership_expiry_alert_7days'){
                $this->membershipExpiryAlert7days();
            }elseif($type=='membership_expiry_alert_3days'){
                $this->membershipExpiryAlert3days();
            }elseif($type=='membership_expiry_alert_1day'){
                $this->membershipExpiryAlert1day();
            }elseif($type=='slot_alert'){
                $this->slotTodayAlert();
            }else{
                header("HTTP/1.1 403 forbidden");
                exit;
            }
        }
    }
    public function slotTodayAlert(){
        $date=date("Y-m-d");
        $dbres=$this->db->prepare("SELECT s.slot_id,s.service_status,u.user_id,u.name,u.email,u.mobile_code,u.mobile,(select service_name from service_provider_services where service_provider_service_id=s.service_id) as service_name,s.slot_date,(select business_name from service_provider where service_provider_id=s.service_provider_id) as service_provider_name from service_slots s, user u where s.user_id=u.user_id and s.payment_status='completed' and (s.service_status='pending'  or s.service_status='inprocess' ) and cast(s.slot_date as date) > cast(s.created_time as date) and cast(s.slot_date as date)='$date'");
        $dbres->execute();
        while($row=$dbres->fetch()){
            $id=$row['user_id'];
            $service_name=$row['service_name'];
            $email=$row['email'];
            $name=$row['name'];
            $mobile=$row['mobile'];
            $country_code=$row['mobile_code'];
            $slot_date=$row['slot_date'];
            $service_provider_name=$row['service_provider_name'];

            $slot_date=date("Y M,D H:i:s",strtotime($slot_date));


            $sms_msg="Today your ".$service_name." slot [ ".$service_provider_name." ] at ".$slot_date." ";
            $mobile=$user['mobile'];
            $country_code=$user['country_code'];
            $phone=$country_code.$mobile;
            $this->sendSMS($phone,$sms_msg);
  
            $message='';
            $message.='Hi '.$name.'<br/>';
            $message.="Today your ".$service_name." slot [ ".$service_provider_name." ] at ".$slot_date." ";
            $message.='<br/>Thanking you';
            $this->phpmailer->sendMail($email,"Slot Alert | Salonee.",$message);
        }
    }
    public function membershipExpiryAlert1day(){
        $date=date("Y-m-d",strtotime("-1 day"));
        $dbres=$this->db->prepare("select email,service_provider_id,country_code,mobile from service_provider where membership_expiry!='0000-00-00' and membership_expiry='$date'");
        $dbres->execute();
        while($row=$dbres->fetch()){
            $id=$row['service_provider_id'];
          
            $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
            $che->bindParam(":id",$id);
            $che->execute();
            $user=$che->fetch();
            $email=$user['email'];
            $name=$user['name'];
   
            $sms_msg="Your account going to expiry on tomorrow please renewal now your membership with salonee. ";
            $mobile=$user['mobile'];
            $country_code=$user['country_code'];
            $phone=$country_code.$mobile;
            $this->sendSMS($phone,$sms_msg);
            
            $message='';
            $message.='Hi '.$name.'<br/>';
            $message.='Your account going to expiry on tomorrow please renewal now your membership with salonee.<br/>';
            $message.='<br/>Thanking you';
            $this->phpmailer->sendMail($email,"Account Expiry Alert in 3 Days | Salonee.",$message);
   
        }
    }
    public function membershipExpiryAlert3days(){
        $date=date("Y-m-d",strtotime("-3 days"));
        $dbres=$this->db->prepare("select email,service_provider_id,country_code,mobile from service_provider where membership_expiry!='0000-00-00' and membership_expiry='$date'");
        $dbres->execute();
        while($row=$dbres->fetch()){

            $id=$row['service_provider_id'];
          

            $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
            $che->bindParam(":id",$id);
            $che->execute();
            $user=$che->fetch();
            $email=$user['email'];
            $name=$user['name'];
   
            $sms_msg="Your account going to expiry in 3 days please renewal now your membership with salonee. ";
            $mobile=$user['mobile'];
            $country_code=$user['country_code'];
            $phone=$country_code.$mobile;
            $this->sendSMS($phone,$sms_msg);
            
            $message='';
            $message.='Hi '.$name.'<br/>';
            $message.='Your account going to expiry in 3 days please renewal now your membership with salonee.<br/>';
            $message.='<br/>Thanking you';
            $this->phpmailer->sendMail($email,"Account Expiry Alert in 3 Days | Salonee.",$message);
   
        }
    }
    public function membershipExpiryAlert7days(){
        $date=date("Y-m-d",strtotime("-7 days"));
        $dbres=$this->db->prepare("select email,service_provider_id,country_code,mobile from service_provider where membership_expiry!='0000-00-00' and membership_expiry='$date'");
        $dbres->execute();
        while($row=$dbres->fetch()){

            $id=$row['service_provider_id'];
          

            $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
            $che->bindParam(":id",$id);
            $che->execute();
            $user=$che->fetch();
            $email=$user['email'];
            $name=$user['name'];
   
            $sms_msg="Your account going to expiry in 7 days please renewal now your membership with salonee. ";
            $mobile=$user['mobile'];
            $country_code=$user['country_code'];
            $phone=$country_code.$mobile;
            $this->sendSMS($phone,$sms_msg);
            
            $message='';
            $message.='Hi '.$name.'<br/>';
            $message.='Your account going to expiry in 7 days please renewal now your membership with salonee.<br/>';
            $message.='<br/>Thanking you';
            $this->phpmailer->sendMail($email,"Account Expiry Alert in 7 Days | Salonee.",$message);
   
        }
    }
    public function membershipExpiry(){
        $date=date("Y-m-d");
        $dbres=$this->db->prepare("select email,service_provider_id,country_code,mobile from service_provider where membership_expiry!='0000-00-00' and membership_expiry<'$date'");
        $dbres->execute();
        while($row=$dbres->fetch()){

            $id=$row['service_provider_id'];
            $che=$this->db->prepare("update service_provider set membership_expiry='0000-00-00' where service_provider_id=:id");
            $che->bindParam(":id",$id);
            $che->execute();

            $che=$this->db->prepare("select * from service_provider where service_provider_id=:id");
            $che->bindParam(":id",$id);
            $che->execute();
            $user=$che->fetch();
            $email=$user['email'];
            $name=$user['name'];
   
            $sms_msg="Your account got expired please purchase membership and get benfits now. ";
            $mobile=$user['mobile'];
            $country_code=$user['country_code'];
            $phone=$country_code.$mobile;
            $this->sendSMS($phone,$sms_msg);
            
            $message='';
            $message.='Hi '.$name.'<br/>';
            $message.='Your account got expired please purchase membership and get benfits now. <br/>';
            $message.='<br/>Thanking you';
            $this->phpmailer->sendMail($email,"Account Expired | Salonee.",$message);
   
        }
    }
    

}