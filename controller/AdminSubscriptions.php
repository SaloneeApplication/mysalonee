<?php
require_once dirname(__DIR__).'/core/Controller.php';

class AdminSubscriptions extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("subscription")){
          $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
          redirect("dashboard.php");
          exit;
        }
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='newPlan'){
                $this->newPlan();
            }elseif($type=='editPlan'){
                $this->editPlan();
            }elseif($type=='editPlanData'){
                $this->editPlanData();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='blockPlan'){
                $this->blockPlan();
            }elseif($type=='activePlan'){
                $this->activePlan();
            }elseif($type=='setRecommand'){
                $this->setRecommand();
            }
        }
    }
    public function setRecommand(){
        $referer=$_SERVER['HTTP_REFERER'];
        $oid=$this->input->get("id",true);
        $up=$this->db->prepare("update subscription_plans set recommand_plan='0' ");
        $up->execute();
        $up=$this->db->prepare("update subscription_plans set recommand_plan='1' where subscription_plan_id=:oid");
        $up->bindParam(":oid",$oid);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Plan Recommanded updated"));
        redirect($referer);
        exit;
    }
    public function activePlan(){
        $referer=$_SERVER['HTTP_REFERER'];
        $oid=$this->input->get("id",true);
        $up=$this->db->prepare("update subscription_plans set status='1' where subscription_plan_id=:oid");
        $up->bindParam(":oid",$oid);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Plan Activated"));
        redirect($referer);
        exit;
    }
    public function blockPlan(){
        $referer=$_SERVER['HTTP_REFERER'];
        $oid=$this->input->get("id",true);
        $up=$this->db->prepare("update subscription_plans set status='0' where subscription_plan_id=:oid");
        $up->bindParam(":oid",$oid);
        $up->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Plan Blocked"));
        redirect($referer);
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Subscriptions';
        $data['page']='subscription';
        $data['admin_id']=$this->admin_id;
        $data['plans']=$this->getPlans();
        return $data;
    }

    public function getPlans(){
        $adv=$this->input->get("pro",true);
        $total_sb=$this->db->prepare("select * from subscription_plans order by duration desc");
        $total_sb->execute();
        return $total_sb->fetchAll();
    }
    
    
    public function editPlanData(){
        $referer=$_SERVER['HTTP_REFERER'];
        $plan_name=$this->input->post("plan_name",true);
        $id=$this->input->post("id",true);
        $duration=$this->input->post("duration",true);
        $price=$this->input->post("price",true);
        $free_adver=$this->input->post("free_adver",true);
        $free_featured=$this->input->post("free_featured",true);
        $desc=$this->input->post("desc",true);
        

        $up=$this->db->prepare("update subscription_plans set name=:plan_name,duration=:duration,price=:price,description=:desc,free_advertisements_days=:free_adver,free_featured_profiles_days=:free_featured where subscription_plan_id=:id");
        $up->bindParam(":id",$id);
        $up->bindParam(":plan_name",$plan_name);
        $up->bindParam(":duration",$duration);
        $up->bindParam(":price",$price);
        $up->bindParam(":desc",$desc);
        $up->bindParam(":free_adver",$free_adver);
        $up->bindParam(":free_featured",$free_featured);
        $up->execute();

        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Plan updated."));
        redirect($referer);
        exit;
    }
    public function newPlan(){
        $referer=$_SERVER['HTTP_REFERER'];
        $plan_name=$this->input->post("plan_name",true);
        $duration=$this->input->post("duration",true);
        $price=$this->input->post("price",true);
        $free_adver=$this->input->post("free_adver",true);
        $free_featured=$this->input->post("free_featured",true);
        $desc=$this->input->post("desc",true);
        

        $up=$this->db->prepare("insert into  subscription_plans (name,duration,currency,price,description,free_advertisements_days,free_featured_profiles_days,status) values (:plan_name,:duration,'AED',:price,:desc,:free_adver,:free_featured,0)");
        $up->bindParam(":plan_name",$plan_name);
        $up->bindParam(":duration",$duration);
        $up->bindParam(":price",$price);
        $up->bindParam(":desc",$desc);
        $up->bindParam(":free_adver",$free_adver);
        $up->bindParam(":free_featured",$free_featured);
        $up->execute();

        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Plan created."));
        redirect($referer);
        exit;
    }
    public function editPlan(){
        $id=$this->input->post("id",true);
        $plan=$this->db->prepare("select * from subscription_plans where subscription_plan_id=:id");
        $plan->bindParam(":id",$id);
        $plan->execute();
        $plan=$plan->fetch();
       
        ?>
        <input type="hidden" name="type"  value="editPlanData" />
            <input type="hidden" name="id" id="plan_id"  value="<?php echo $plan['subscription_plan_id'];?>" />
<div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <input type="text" id="adName" class="form-control" value="<?php echo $plan['name'];?>" name="plan_name" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="adName">Plan Name</label>
                      </div>
                    </div>
                  </div>
                <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="text" id="duration" class="form-control" value="<?php echo $plan['duration'];?>"  name="duration"  autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="duration">Duration ( No. Months )</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="text" id="price" class="form-control" value="<?php echo $plan['price'];?>"   name="price" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="price">Price ( AED )</label>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="number" id="duration" class="form-control" value="<?php echo $plan['free_advertisements_days'];?>"  min="0" name="free_adver"  autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="duration">Free Advertisements Days</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <input type="number" id="price" class="form-control" min="0" value="<?php echo $plan['free_featured_profiles_days'];?>"   name="free_featured" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="price">Free Featured Profiles Days</label>
                      </div>
                    </div>
                  </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <textarea type="text" id="adDesc" rows="5" name="desc" class="form-control" autocomplete="off" ><?php echo $plan['description'];?></textarea>
                    <label class="form-control-placeholder p-0" for="adDesc">Description</label>
                  </div>
                </div>
              </div>
             

              <div class="form-group">
                <button type="submit" class="btn theme-btn">Submit</button>
              </div>
        <?php 

        exit;
    }
}