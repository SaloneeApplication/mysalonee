<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserComboServices extends Controller{
    public $user_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
           
            $type=$this->input->post("type",true);
          
            if($type=='insertService'){
              if($this->branch_type!='main_branch'){
                  $referer=$_SERVER['HTTP_REFERER'];
                  $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"you don't have access"));
                  redirect($referer);
              }
              $this->insertService();
            }elseif($type=='getServiceData'){
              $this->getEditService();
            }elseif($type=='updateService'){
              $this->updateService();
            }elseif($type=='updateSaloneeTimings'){
              $this->updateSaloneeTimings();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='enableservice'){
                if($this->branch_type!='main_branch'){
                    $referer=$_SERVER['HTTP_REFERER'];
                    $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"you don't have access"));
                    redirect($referer);
                }
                $this->enableService();
            }elseif($type=='disableservice'){
                if($this->branch_type!='main_branch'){
                    $referer=$_SERVER['HTTP_REFERER'];
                    $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"you don't have access"));
                    redirect($referer);
                }
                $this->disableService();
            }
        }
    }
    public function enableService(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("sid",true);
        $dbres=$this->db->prepare("update service_provider_services set status=1 where service_provider_id=:id and service_provider_service_id =:sid");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->bindParam(":sid",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully service enabled"));
        redirect($referer);

        exit;
    }
    public function disableService(){
        $referer=$_SERVER['HTTP_REFERER'];

        $cid=$this->input->get("sid",true);
        $dbres=$this->db->prepare("update service_provider_services set status=0 where service_provider_id=:id and service_provider_service_id =:sid");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->bindParam(":sid",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully service disabled"));
        redirect($referer);

        exit;
    }

    public function insertService(){
        $referer=$_SERVER['HTTP_REFERER'];
        $branch=$this->input->post("branch",true);
        $service=$this->input->post("service",true);
        $service_name=$this->input->post("service_name",true);
        $description=$this->input->post("description",true);
        $service_for=$this->input->post("service_for",true);
        $addons=$this->input->post("addons",true);
        $products=$this->input->post("products",true);

        $slot_freq=$this->input->post("slot_freq",true);
        $ser1='';
        foreach($service as $ser){
          $ser1.=$ser.',';
        }
        $ser1=rtrim($ser1,",");
        // echo $ser1;
        // exit;

        $access = array("home", "salonee", "both");
        if (!in_array($service_for, $access))
        {
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Required Service for"));
            redirect($referer);
            exit;
        }
        $salonee_base_price=$this->input->post("salonee_base_price",true);
        $salonee_sale_price=$this->input->post("salonee_sale_price",true);
        $salonee_price_type=$this->input->post("salonee_price_type",true);
        $salonee_weekend_name=$this->input->post("salonee_weekend_name",true); //multiple
        $salonee_special_date=$this->input->post("salonee_special_date",true); //multiple
        $salonee_special_price=$this->input->post("salonee_special_price",true);

        $salonee_same_slot_time=$this->input->post("salonee_same_slot_time",true);
        $salonee_allowed_slots_same=$this->input->post("salonee_allowed_slots_same",true);
        $salonee_day_limit_slots=$this->input->post("salonee_day_limit_slots",true);

        $home_base_price=$this->input->post("home_base_price",true);
        $home_sale_price=$this->input->post("home_sale_price",true);
        $home_price_type=$this->input->post("home_price_type",true);
        $home_weekend_name=$this->input->post("home_weekend_name",true); //multiple
        $home_special_date=$this->input->post("home_special_date",true); //multiple
        $home_special_price=$this->input->post("home_special_price",true);
        
        $home_same_slot_time=$this->input->post("home_same_slot_time",true);
        $home_allowed_slots_same=$this->input->post("home_allowed_slots_same",true);
        $home_day_limit_slots=$this->input->post("home_day_limit_slots",true);
        
        $dbress=$this->db->prepare("insert into service_provider_services (service_provider_id,description,category_id,service_name,service_at,add_ons,product_to_use,status,time_slot_freq,package_type) values (:service_provider_id,:description,:category_id,:service_name,:service_at,:add_ons,:product_to_use,1,:time_slot_freq,'combo')");
        $dbress->bindParam(":service_provider_id",$this->user_id);
        $dbress->bindParam(":description",$description);
        $dbress->bindParam(":category_id",$ser1);
        $dbress->bindParam(":service_name",$service_name);
        $dbress->bindParam(":service_at",$service_for);
        $dbress->bindParam(":add_ons",$addons);
        $dbress->bindParam(":product_to_use",$products);
        $dbress->bindParam(":time_slot_freq",$slot_freq);
        $dbress->execute();
        $service_id=$this->db->lastInsertId();

        if($service_for=='home'){

            $dbress=$this->db->prepare("insert into services_prices (service_provider_service_id,price_for,base_price,sale_price,price_type,same_time_slots_allowed,allowed_slots_count,per_day_slots_limit) values (:service_provider_service_id,:price_for,:base_price,:sale_price,:price_type,:same_time_slots_allowed,:allowed_slots_count,:per_day_slots_limit)");
            $dbress->bindParam(":service_provider_service_id",$service_id);
            $dbress->bindParam(":price_for",$service_for);
            $dbress->bindParam(":base_price",$home_base_price);
            $dbress->bindParam(":sale_price",$home_sale_price);
            $dbress->bindParam(":price_type",$home_price_type);
            $dbress->bindParam(":same_time_slots_allowed",$home_same_slot_time);
            $dbress->bindParam(":allowed_slots_count",$home_allowed_slots_same);
            $dbress->bindParam(":per_day_slots_limit",$home_day_limit_slots);
            $dbress->execute();
            // echo json_encode( $dbress->errorInfo());exit;
            $home_price_id=$this->db->lastInsertId();

            if($home_price_type=='custom_price_weekend'){

                foreach($home_weekend_name as $custom_on){
                    $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                    $dbres->bindParam(":service_provider_service_id",$service_id);
                    $dbres->bindParam(":sp_id",$home_price_id);
                    $dbres->bindParam(":price_for",$service_for);
                    $dbres->bindParam(":type",$home_price_type);
                    $dbres->bindParam(":custom_on",$custom_on);
                    $dbres->bindParam(":sales_price",$home_special_price);
                    $dbres->execute();
                }

            }elseif($home_price_type=='custom_price_special_date'){
                $home_special_date=explode(",",$home_special_date);
                foreach($home_special_date as $custom_on){
                    $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                    $dbres->bindParam(":service_provider_service_id",$service_id);
                    $dbres->bindParam(":sp_id",$home_price_id);
                    $dbres->bindParam(":price_for",$service_for);
                    $dbres->bindParam(":type",$home_price_type);
                    $dbres->bindParam(":custom_on",$custom_on);
                    $dbres->bindParam(":sales_price",$home_special_price);
                    $dbres->execute();
                }
            }

        }elseif($service_for=='salonee'){
            
            $dbress=$this->db->prepare("insert into services_prices (service_provider_service_id,price_for,base_price,sale_price,price_type,same_time_slots_allowed,allowed_slots_count,per_day_slots_limit) values (:service_provider_service_id,:price_for,:base_price,:sale_price,:price_type,:same_time_slots_allowed,:allowed_slots_count,:per_day_slots_limit)");
            $dbress->bindParam(":service_provider_service_id",$service_id);
            $dbress->bindParam(":price_for",$service_for);
            $dbress->bindParam(":base_price",$salonee_base_price);
            $dbress->bindParam(":sale_price",$salonee_sale_price);
            $dbress->bindParam(":price_type",$salonee_price_type);
            $dbress->bindParam(":same_time_slots_allowed",$salonee_same_slot_time);
            $dbress->bindParam(":allowed_slots_count",$salonee_allowed_slots_same);
            $dbress->bindParam(":per_day_slots_limit",$salonee_day_limit_slots);
            $dbress->execute();
            $salonee_price_id=$this->db->lastInsertId();

            if($salonee_price_type=='custom_price_weekend'){

                foreach($salonee_weekend_name as $custom_on){
                    $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                    $dbres->bindParam(":service_provider_service_id",$service_id);
                    $dbres->bindParam(":sp_id",$salonee_price_id);
                    $dbres->bindParam(":price_for",$service_for);
                    $dbres->bindParam(":type",$salonee_price_type);
                    $dbres->bindParam(":custom_on",$custom_on);
                    $dbres->bindParam(":sales_price",$salonee_special_price);
                    $dbres->execute();
                }

            }elseif($salonee_price_type=='custom_price_special_date'){
                $salonee_special_date=explode(",",$home_special_date);
                foreach($salonee_special_date as $custom_on){
                    $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                    $dbres->bindParam(":service_provider_service_id",$service_id);
                    $dbres->bindParam(":sp_id",$salonee_price_id);
                    $dbres->bindParam(":price_for",$service_for);
                    $dbres->bindParam(":type",$salonee_price_type);
                    $dbres->bindParam(":custom_on",$custom_on);
                    $dbres->bindParam(":sales_price",$salonee_special_price);
                    $dbres->execute();
                }
            }

        }elseif($service_for=='both'){

            //for home base 
            $service_for='home';
            $dbress=$this->db->prepare("insert into services_prices (service_provider_service_id,price_for,base_price,sale_price,price_type,same_time_slots_allowed,allowed_slots_count,per_day_slots_limit) values (:service_provider_service_id,:price_for,:base_price,:sale_price,:price_type,:same_time_slots_allowed,:allowed_slots_count,:per_day_slots_limit)");
            $dbress->bindParam(":service_provider_service_id",$service_id);
            $dbress->bindParam(":price_for",$service_for);
            $dbress->bindParam(":base_price",$home_base_price);
            $dbress->bindParam(":sale_price",$home_sale_price);
            $dbress->bindParam(":price_type",$home_price_type);
            $dbress->bindParam(":same_time_slots_allowed",$home_same_slot_time);
            $dbress->bindParam(":allowed_slots_count",$home_allowed_slots_same);
            $dbress->bindParam(":per_day_slots_limit",$home_day_limit_slots);
            $dbress->execute();
            $home_price_id=$this->db->lastInsertId();

            if($home_price_type=='custom_price_weekend'){

                foreach($home_weekend_name as $custom_on){
                    $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                    $dbres->bindParam(":service_provider_service_id",$service_id);
                    $dbres->bindParam(":sp_id",$home_price_id);
                    $dbres->bindParam(":price_for",$service_for);
                    $dbres->bindParam(":type",$home_price_type);
                    $dbres->bindParam(":custom_on",$custom_on);
                    $dbres->bindParam(":sales_price",$home_special_price);
                    $dbres->execute();
                }

            }elseif($home_price_type=='custom_price_special_date'){
                $home_special_date=explode(",",$home_special_date);
                foreach($home_special_date as $custom_on){
                    $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                    $dbres->bindParam(":service_provider_service_id",$service_id);
                    $dbres->bindParam(":sp_id",$home_price_id);
                    $dbres->bindParam(":price_for",$service_for);
                    $dbres->bindParam(":type",$home_price_type);
                    $dbres->bindParam(":custom_on",$custom_on);
                    $dbres->bindParam(":sales_price",$home_special_price);
                    $dbres->execute();
                }
            }
            //home base end


            //for salone bases
            $service_for='salonee';
                $dbress=$this->db->prepare("insert into services_prices (service_provider_service_id,price_for,base_price,sale_price,price_type,same_time_slots_allowed,allowed_slots_count,per_day_slots_limit) values (:service_provider_service_id,:price_for,:base_price,:sale_price,:price_type,:same_time_slots_allowed,:allowed_slots_count,:per_day_slots_limit)");
                $dbress->bindParam(":service_provider_service_id",$service_id);
                $dbress->bindParam(":price_for",$service_for);
                $dbress->bindParam(":base_price",$salonee_base_price);
                $dbress->bindParam(":sale_price",$salonee_sale_price);
                $dbress->bindParam(":price_type",$salonee_price_type);
                $dbress->bindParam(":same_time_slots_allowed",$salonee_same_slot_time);
                $dbress->bindParam(":allowed_slots_count",$salonee_allowed_slots_same);
                $dbress->bindParam(":per_day_slots_limit",$salonee_day_limit_slots);
                $dbress->execute();
                $salonee_price_id=$this->db->lastInsertId();

                if($salonee_price_type=='custom_price_weekend'){

                    foreach($salonee_weekend_name as $custom_on){
                        $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                        $dbres->bindParam(":service_provider_service_id",$service_id);
                        $dbres->bindParam(":sp_id",$salonee_price_id);
                        $dbres->bindParam(":price_for",$service_for);
                        $dbres->bindParam(":type",$salonee_price_type);
                        $dbres->bindParam(":custom_on",$custom_on);
                        $dbres->bindParam(":sales_price",$salonee_special_price);
                        $dbres->execute();
                    }

                }elseif($salonee_price_type=='custom_price_special_date'){
                    $salonee_special_date=explode(",",$home_special_date);
                    foreach($salonee_special_date as $custom_on){
                        $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                        $dbres->bindParam(":service_provider_service_id",$service_id);
                        $dbres->bindParam(":sp_id",$salonee_price_id);
                        $dbres->bindParam(":price_for",$service_for);
                        $dbres->bindParam(":type",$salonee_price_type);
                        $dbres->bindParam(":custom_on",$custom_on);
                        $dbres->bindParam(":sales_price",$salonee_special_price);
                        $dbres->execute();
                    }
                }
            //for salonee bases end
        }

        $check=$this->db->prepare("select service_provider_id,membership_expiry from service_provider where service_provider_id=:user_id");
        $check->bindParam(":user_id",$this->user_id);
        $check->execute();
        $check=$check->fetch();
        
        $membership_expiry=$check['membership_expiry'];
	      $access_member=0;
        if($membership_expire>=date("Y-m-d")){
          $access_member=1;
        } 
        if($access_member==0){
          setcookie('ssl_alert', '0', time() + (86400 * 30), "/");
        }
        
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully service price added"));
        redirect($referer);
        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Combo Services';
        $data['page']='combo-services';
        $data['user_id']=$this->user_id;
        return $data;
    }

    public function getUserServices(){
        
        $dbres=$this->db->prepare("select s.*,sb.address,if(s.category_id!='',(select CONCAT(name,'-',category_for) from category where category_id=s.category_id),'') as category,if(sb.city!='',(select city_name_en from cities where id=sb.city),'') as location from service_provider_services s ,service_provider sb  where sb.service_provider_id=s.service_provider_id and s.service_provider_id=:id and s.package_type='combo'");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->execute();
        return $dbres->fetchAll();
    }

    public function getMultiplyeCategoriesName($data){
        $data=explode(",",$data);
        $name='';
        foreach($data as $da){
          $dbres=$this->db->prepare("select name,category_for from category where category_id=:id");
          $dbres->bindParam(":id",$da);
          $dbres->execute();
          $dbres=$dbres->fetch();
          $name.=$dbres['name']." <font size='2px'>[ For ".$dbres['category_for']." ]</font><br/>";
        }

        return $name;

    }

    public function getUserCategories(){
        $dbres=$this->db->prepare("select sp.*,c.name,c.category_for from service_provider_categories sp,category c where  sp.category_id=c.category_id and sp.service_provider_id=:id");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->execute();
        return $dbres->fetchAll();
    }
    public function getActiveServiceProviderBranches(){
        $dbres=$this->db->prepare("select sp.*,if(sp.location_id!='',(select city_name_en from cities where id=sp.location_id),'') as name from service_provider_branches sp where  sp.service_provider_main_branch_id=:id and sp.service_provider_id!='' and sp.status=1");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->execute();
        return $dbres->fetchAll();
    }
    public function getServicePrices($sid){
        $dbres=$this->db->prepare("select * from services_prices where service_provider_service_id=:id");
        $dbres->bindParam(":id",$sid);
        $dbres->execute();
        return $dbres->fetchAll();
    }
    public function getEditService(){
        $id=$this->input->post("sid",true);
        $user_categories=$this->getUserCategories();
        $s=$this->db->prepare("select * from service_provider_services where service_provider_service_id=:id and service_provider_id=:uid");
        $s->bindParam(":id",$id);
        $s->bindParam(":uid",$this->user_id);
        $s->execute();
        $s=$s->fetch();
        //home 
        $home=$this->db->prepare("select * from services_prices where service_provider_service_id=:id and price_for='home'");
        $home->bindParam(":id",$id);
        $home->execute();
        $home_count=$home->rowCount();
        $home=$home->fetch();

        $home_dates='';
        $home_weekend=array();
        if($home_count>0){
            $home1=$this->db->prepare("select * from services_special_prices where service_provider_service_id=:id and sp_id=:spid and price_for='home'");
            $home1->bindParam(":id",$id);
            $home1->bindParam(":spid",$home['sp_id']);
            $home1->execute();
            $home_special_price=0;
            while($row=$home1->fetch()){
                $home_special_price=$row['sales_price'];
                if($row['type']=='custom_price_special_date'){
                    $home_dates.=$row['custom_on'].',';
                }elseif($row['type']=='custom_price_weekend'){
                    $home_weekend[]=$row['custom_on'];
                }
            }
            $home_dates=rtrim($home_dates,",");
        }
        

        //salonee 
        $salonee=$this->db->prepare("select * from services_prices where service_provider_service_id=:id and price_for='salonee'");
        $salonee->bindParam(":id",$id);
        $salonee->execute();
        $salonee_count=$salonee->rowCount();

        $salonee=$salonee->fetch();

        $salonee_dates='';
        $salonee_weekend=array();
        if($salonee_count>0){
            $salonee1=$this->db->prepare("select * from services_special_prices where service_provider_service_id=:id and sp_id=:spid and price_for='salonee'");
            $salonee1->bindParam(":id",$id);
            $salonee1->bindParam(":spid",$salonee['sp_id']);
            $salonee1->execute();
            $salonee_special_price=0;
            while($row=$salonee1->fetch()){
                $salonee_special_price=$row['sales_price'];
                if($row['type']=='custom_price_special_date'){
                    $salonee_dates.=$row['custom_on'].',';
                }elseif($row['type']=='custom_price_weekend'){
                    $salonee_weekend[]=$row['custom_on'];
                }
            }
            $salonee_dates=rtrim($salonee_dates,",");

        }
        ?>
        <input type="hidden" name="sid" value="<?php echo $s['service_provider_service_id'];?>" />
        <div class="form-group">
                  <select name="service" multiple  class="form-control" autocomplete="off" required >
                    <option hidden disabled  value> -- Select Category -- </option>
                    <?php 
                    $multi=explode(",",$s['category_id']);

                        foreach($user_categories as $row2)
                        {
                          $category = $row2['name'];
                          $serviceId = $row2['category_id'];
                          $category_for = $row2['category_for'];
                          if(in_array($serviceId,$multi)){
                            echo "<option value = '$serviceId' selected>$category  <font color='gray' style='font-size:12px;font-weight:500;color:gray;'>[ For ".ucwords($category_for)." ]</font></option>";
                          }else{
                            echo "<option value = '$serviceId'>$category  <font color='gray' style='font-size:12px;font-weight:500;color:gray;'>[ For ".ucwords($category_for)." ]</font></option>";
                          }
                        }
                    ?>
                  </select>

                </div>
            <div class="form-group">
              <input type="text" name="service_name" id="service_name" value="<?php echo $s['service_name'];?>" class="form-control" autocomplete="off" required/>
              <label class="form-control-placeholder p-0" for="service_name">Service Name</label>
            </div>
				<div class="form-group">
					<textarea type="text" id="description" name = "description" rows="5" class="form-control" autocomplete="off" required><?php echo $s['description'];?></textarea>
					<label class="form-control-placeholder p-0" for="accountNumber">Description</label>
				</div>
        <div class="form-group">
          <select name="service_for" id="service_for1" class="form-control" autocomplete="off" required >
            <option  disabled  value> -- Select Service For -- </option>
            <option value="salonee" <?php echo ($s['service_at']=='salonee')?"selected":"";?>>Only Salonee</option>
            <option value="home" <?php echo ($s['service_at']=='home')?"selected":"";?>>Only at Home</option>
            <option value="both" <?php echo ($s['service_at']=='both')?"selected":"";?>>Both</option>
          </select>
        </div>

            <!-- salonee parent start -->
            <div id="salone_parent1" style="display:<?php echo ($s['service_at']=='salonee' || $s['service_at']=='both')?"block":"none";?>"> <h5>At Salonee Pricing </h5>
              <div class="form-group">
                <input type="text" id="salonee_base_price1" value="<?php echo round(@$salonee['base_price']);?>" name = "salonee_base_price" class="form-control" autocomplete="off" required>
                <label class="form-control-placeholder p-0" for="salonee_base_price1">Base Price</label>
              </div>
              <div class="form-group">
                <input type="text" id="salonee_sale_price1" value="<?php echo round(@$salonee['sale_price']);?>" name = "salonee_sale_price" class="form-control" autocomplete="off" required>
                <label class="form-control-placeholder p-0" for="salonee_sale_price1">Sale Price</label>
              </div>

              <div class="form-group">
                <select name="salonee_price_type" id="salonee_price_type1" class="form-control" autocomplete="off" required >
                  <option  disabled <?php echo (@$salonee['price_type']=='')?"selected":"";?> value> -- Select Price Type -- </option>
                  <option value="regular" <?php echo (@$salonee['price_type']=='regular')?"selected":"";?>>Regular</option>
                  <option value="custom_price_weekend"  <?php echo (@$salonee['price_type']=='custom_price_weekend')?"selected":"";?>>Custom Price - Weekend</option>
                  <option value="custom_price_special_date" <?php echo (@$salonee['price_type']=='custom_price_special_date')?"selected":"";?>>Custom Price -- Special Date</option>
                </select>
              </div>
              <div class="form-group" id="salonee_weekend_name1" style="display:<?php echo (@$salonee['price_type']=='custom_price_weekend')?"block":"none";?>;">
                <select name="salonee_weekend_name[]" id="salonee_weekend_name1" multiple class="form-control" autocomplete="off" >
                  <option  disabled <?php echo (@$salonee['price_type']=='custom_price_weekend')?"":"selected";?>  value> -- Select Weekend-- </option>
                  <option value="monday" <?php echo (in_array("monday",@$salonee_weekend))?"selected":"";?>>Every Monday</option>
                  <option value="tuesday" <?php echo (in_array("tuesday",@$salonee_weekend))?"selected":"";?>>Every Tuesday</option>
                  <option value="wednesday" <?php echo (in_array("wednesday",@$salonee_weekend))?"selected":"";?>>Every Wednesday</option>
                  <option value="thursday" <?php echo (in_array("thursday",@$salonee_weekend))?"selected":"";?>>Every Thursday</option>
                  <option value="friday" <?php echo (in_array("friday",@$salonee_weekend))?"selected":"";?>>Every Friday</option>
                  <option value="saturday" <?php echo (in_array("saturday",@$salonee_weekend))?"selected":"";?>>Every Saturday</option>
                  <option value="sunday" <?php echo (in_array("sunday",@$salonee_weekend))?"selected":"";?>>Every Sunday</option>
                </select>
              </div>
              <div class="form-group" id="salonee_special_date_tag1" style="display:<?php echo (@$salonee['price_type']=='custom_price_special_date')?"block":"none";?>;">
                <input type="text" id="salonee_special_date1" value="<?php echo (@$salonee['price_type']=='custom_price_special_date')?@$salonee_dates:"";?>" readonly name = "salonee_special_date" class="form-control" autocomplete="off" >
                <?php if(@$salonee_dates!=''){echo "<script>$('#salonee_special_date1').datepicker('update');</script>";}?>
                <label class="form-control-placeholder p-0" for="special_date">Special Dates</label>
              </div>
              <div class="form-group" id="salonee_special_price_tag1" style="display:<?php echo (@$salonee['price_type']=='regular' ||@$salonee['price_type']=='')?"none":"block";?>;">
                <input type="text" id="salonee_special_price1" name = "salonee_special_price" value="<?php echo (@$salonee['price_type']!='regular')?@round($salonee_special_price):"";?>" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="salonee_special_price_tag1">Special Price</label>
              </div>

              <div class="form-group">
              <select name="salonee_same_slot_time" id="salonee_same_slot_time1" class="form-control" autocomplete="off"  >
                <option  disabled <?php echo (@$salonee['same_time_slots_allowed']=='')?"":"";?> value> -- Same Slot Time booking -- </option>
                <option value="1" <?php echo (@$salonee['same_time_slots_allowed']==1)?"selected":"";?>>Yes</option>
                <option value="0" <?php echo (@$salonee['same_time_slots_allowed']==0)?"selected":"";?>>No</option>
              </select>
            </div>
            <div class="form-group" id="salonee_allowed_slots_same_tag1" style="display:<?php echo (@$salonee['same_time_slots_allowed']==1)?"block":"none";?>;">
              <input type="number" min="1" id="salonee_allowed_slots_same1" value="<?php echo (@$salonee['same_time_slots_allowed']==1)?@$salonee['allowed_slots_count']:"";?>" name = "salonee_allowed_slots_same" class="form-control" autocomplete="off" />
              <label class="form-control-placeholder p-0" for="salonee_allowed_slots_same1">Allowed Slots Same Time</label>
            </div>
            <div class="form-group">
              <input type="number" min="0" id="salonee_day_limit_slots1" value="<?php echo @$salonee['per_day_slots_limit'];?>" name = "salonee_day_limit_slots" class="form-control" autocomplete="off" />
              <label class="form-control-placeholder p-0"  for="salonee_day_limit_slots1">Per Day Slots Limit</label>
            </div>

            </div>
			<!-- salonee parent end -->
        

        <!-- home parent start -->
        <div id="home_parent1" style="display:<?php echo ($s['service_at']=='home' || $s['service_at']=='both')?"block":"none";?>;"> <h5>At Home Pricing </h5>
              <div class="form-group">
                <input type="text" id="home_base_price1" name = "home_base_price"  value="<?php echo round(@$home['base_price']);?>" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="home_base_price1">Base Price</label>
              </div>
              <div class="form-group">
                <input type="text" id="home_sale_price1" name = "home_sale_price" value="<?php echo round(@$home['sale_price']);?>" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="home_sale_price1">Sale Price</label>
              </div>

              <div class="form-group">
                <select name="home_price_type" id="home_price_type1" class="form-control" autocomplete="off"  >
                  <option disabled  value> -- Select Price Type -- </option>
                  <option value="regular"  <?php echo (@$home['price_type']=='regular')?"selected":"";?>>Regular</option>
                  <option value="custom_price_weekend" <?php echo (@$home['price_type']=='custom_price_weekend')?"selected":"";?>>Custom Price - Weekend</option>
                  <option value="custom_price_special_date" <?php echo (@$home['price_type']=='custom_price_special_date')?"selected":"";?>>Custom Price -- Special Date</option>
                </select>
              </div>
              <div class="form-group" id="home_weekend_name1" style="display:<?php echo (@$home['price_type']=='custom_price_weekend')?"block":"none";?>;">
                <select name="home_weekend_name[]" id="home_weekend_name_value1" multiple class="form-control" autocomplete="off" >
                  <option disabled <?php echo (@$home['price_type']=='custom_price_weekend')?"":"selected";?> value> -- Select Weekend-- </option>
                  <option value="monday" <?php echo (in_array("monday",@$home_weekend))?"selected":"";?>>Every Monday</option>
                  <option value="tuesday" <?php echo (in_array("tuesday",@$home_weekend))?"selected":"";?>>Every Tuesday</option>
                  <option value="wednesday" <?php echo (in_array("wednesday",@$home_weekend))?"selected":"";?>>Every Wednesday</option>
                  <option value="thursday" <?php echo (in_array("thursday",@$home_weekend))?"selected":"";?>>Every Thursday</option>
                  <option value="friday" <?php echo (in_array("friday",@$home_weekend))?"selected":"";?>>Every Friday</option>
                  <option value="satuerday" <?php echo (in_array("satuerday",@$home_weekend))?"selected":"";?>>Every Satuerday</option>
                  <option value="sunday" <?php echo (in_array("sunday",@$home_weekend))?"selected":"";?>>Every Sunday</option>
                </select>
              </div>
              <div class="form-group" id="home_special_date_tag1" style="display:<?php echo (@$home['price_type']=='custom_price_special_date')?"block":"none";?>;">
                <input type="text" id="home_special_date1" readonly  value="<?php echo (@$home['price_type']=='custom_price_special_date')?@$home_dates:"";?>" name = "home_special_date" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="home_special_date1">Special Dates</label>
              </div>
              <?php if(@$home_dates!=''){echo "<script>$('#home_special_date1').datepicker('update');</script>";}?>
              <div class="form-group" id="home_special_price_tag1" style="display:<?php echo (@$home['price_type']!='regular')?"block":"none";?>;">
                <input type="text" id="home_special_price1" value="<?php echo (@$home['price_type']!='regular')?@round(@$home_special_price):"";?>" name = "home_special_price" class="form-control" autocomplete="off" >
                <label class="form-control-placeholder p-0" for="home_special_price1">Special Price</label>
              </div>
              <div class="form-group">
              <select name="home_same_slot_time" id="home_same_slot_time1" class="form-control" autocomplete="off"  >
                <option  disabled <?php echo (@$home['same_time_slots_allowed']=='')?"selected":"";?>  value> -- Same Slot Time booking -- </option>
                <option value="1"  <?php echo (@$home['same_time_slots_allowed']==1)?"selected":"";?>>Yes</option>
                <option value="0"  <?php echo (@$home['same_time_slots_allowed']==0)?"selected":"";?>>No</option>
              </select>
            </div>
            <div class="form-group" id="home_allowed_slots_same_tag1" style="display:<?php echo (@$home['same_time_slots_allowed']==1)?"block":"none";?>;">
              <input type="number" min="1" id="home_allowed_slots_same1" value="<?php echo (@$home['same_time_slots_allowed']==1)?@$home['allowed_slots_count']:"";?>" name = "home_allowed_slots_same" class="form-control" autocomplete="off" />
              <label class="form-control-placeholder p-0" for="home_allowed_slots_same1">Allowed Slots Same Time</label>
            </div>
            <div class="form-group">
              <input type="number" min="0" id="home_day_limit_slots1" value="<?php echo @$home['per_day_slots_limit'];?>" name = "home_day_limit_slots" class="form-control" autocomplete="off" />
              <label class="form-control-placeholder p-0"  for="home_day_limit_slots1">Per Day Slots Limit</label>
            </div>
            </div>
			<!-- home parent end -->
       

			  <div class="form-group">
					<textarea type="text" id="addons1" name = "addons" rows="5" class="form-control" autocomplete="off" required><?php echo $s['add_ons'];?></textarea>
					<label class="form-control-placeholder p-0" for="addons1">Add Ons</label>
				</div>
				<div class="form-group">
					<textarea type="text" id="products1" name = "products" rows="5" class="form-control" autocomplete="off" required><?php echo $s['product_to_use'];?></textarea>
					<label class="form-control-placeholder p-0" for="products1">Products To Use</label>
				</div>
        <div class="form-group">
        <select name="slot_freq" id="slot_freq1" class="form-control" autocomplete="off" required >
            <option  disabled  value> -- Slot Booking Frequency Hourly -- </option>
            <option value="15" <?php echo ($s['time_slot_freq']=='15')?"selected":"";?>>Every 15 minutes</option>
            <option value="30" <?php echo ($s['time_slot_freq']=='30')?"selected":"";?>>Every 30 minutes</option>
            <option value="45" <?php echo ($s['time_slot_freq']=='45')?"selected":"";?>>Every 45 minutes</option>
            <option value="60" <?php echo ($s['time_slot_freq']=='60')?"selected":"";?>>Every 1 hour</option>
          </select>
				</div>
        <?php
        exit;
    }

    public function updateService(){
      $sid=$this->input->post("sid",true);

      
      $referer=$_SERVER['HTTP_REFERER'];
      $branch=$this->input->post("branch",true);
      $service=$this->input->post("service",true);
      $service_name=$this->input->post("service_name",true);
      $description=$this->input->post("description",true);
      $service_for=$this->input->post("service_for",true);
      $addons=$this->input->post("addons",true);
      $products=$this->input->post("products",true);

      $slot_freq=$this->input->post("slot_freq",true);

      $ser1='';
      foreach($service as $ser){
        $ser1.=$ser.',';
      }
      $ser1=rtrim($ser1,",");
      
      $access = array("home", "salonee", "both");
      if (!in_array($service_for, $access))
      {
          $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Required Service for"));
          redirect($referer);
          exit;
      }
      $salonee_base_price=$this->input->post("salonee_base_price",true);
      $salonee_sale_price=$this->input->post("salonee_sale_price",true);
      $salonee_price_type=$this->input->post("salonee_price_type",true);
      $salonee_weekend_name=$this->input->post("salonee_weekend_name",true); //multiple
      $salonee_special_date=$this->input->post("salonee_special_date",true); //multiple
      $salonee_special_price=$this->input->post("salonee_special_price",true);

      $salonee_same_slot_time=$this->input->post("salonee_same_slot_time",true);
      $salonee_allowed_slots_same=$this->input->post("salonee_allowed_slots_same",true);
      $salonee_day_limit_slots=$this->input->post("salonee_day_limit_slots",true);

      $home_base_price=$this->input->post("home_base_price",true);
      $home_sale_price=$this->input->post("home_sale_price",true);
      $home_price_type=$this->input->post("home_price_type",true);
      $home_weekend_name=$this->input->post("home_weekend_name",true); //multiple
      $home_special_date=$this->input->post("home_special_date",true); //multiple
      $home_special_price=$this->input->post("home_special_price",true);
      
      $home_same_slot_time=$this->input->post("home_same_slot_time",true);
      $home_allowed_slots_same=$this->input->post("home_allowed_slots_same",true);
      $home_day_limit_slots=$this->input->post("home_day_limit_slots",true);
      

      $dbress=$this->db->prepare("update service_provider_services set description=:description,service_name=:service_name,service_at=:service_at,add_ons=:add_ons,product_to_use=:product_to_use,time_slot_freq=:time_slot_freq where service_provider_id=:service_provider_id and service_provider_service_id=:service_provider_service_id");
      $dbress->bindParam(":service_provider_id",$this->user_id);
      $dbress->bindParam(":service_provider_service_id",$sid);
      $dbress->bindParam(":description",$description);
      $dbress->bindParam(":service_name",$service_name);
      $dbress->bindParam(":service_at",$service_for);
      $dbress->bindParam(":add_ons",$addons);
      $dbress->bindParam(":product_to_use",$products);
      $dbress->bindParam(":time_slot_freq",$slot_freq);
      $dbress->execute();
      $service_id=$sid;

      if($service_for=='home'){

          $check1=$this->db->prepare("select * from services_prices where service_provider_service_id=:service_provider_service_id and price_for='salonee'");
          $check1->bindParam(":service_provider_service_id",$service_id);
          $check1->execute();
        
          $i=0;
          while($row1=$check1->fetch()){
            $i++;
            $sp_id=$row1['sp_id'];
            $del=$this->db->prepare("delete from services_special_prices where sp_id=$sp_id and service_provider_service_id=:service_provider_service_id and price_for='salonee'");
            $del->bindParam(":service_provider_service_id",$service_id);
            $del->execute();
          }
          if($i>0){
            $del=$this->db->prepare("delete from services_prices where service_provider_service_id=:service_provider_service_id and price_for='salonee'");
            $del->bindParam(":service_provider_service_id",$service_id);
            $del->execute();
          }

          
          $check=$this->db->prepare("select * from services_prices where service_provider_service_id=:service_provider_service_id and price_for='home'");
          $check->bindParam(":service_provider_service_id",$service_id);
          $check->execute();
          $check=$check->rowCount();

          if($check==0){
            //when there is no pricing for home
            $dbress=$this->db->prepare("insert into services_prices (service_provider_service_id,price_for,base_price,sale_price,price_type,same_time_slots_allowed,allowed_slots_count,per_day_slots_limit) values (:service_provider_service_id,:price_for,:base_price,:sale_price,:price_type,:same_time_slots_allowed,:allowed_slots_count,:per_day_slots_limit)");
            $dbress->bindParam(":service_provider_service_id",$service_id);
            $dbress->bindParam(":price_for",$service_for);
            $dbress->bindParam(":base_price",$home_base_price);
            $dbress->bindParam(":sale_price",$home_sale_price);
            $dbress->bindParam(":price_type",$home_price_type);
            $dbress->bindParam(":same_time_slots_allowed",$home_same_slot_time);
            $dbress->bindParam(":allowed_slots_count",$home_allowed_slots_same);
            $dbress->bindParam(":per_day_slots_limit",$home_day_limit_slots);
            $dbress->execute();
            $home_price_id=$this->db->lastInsertId();
  
          
          }else{
            //there is have pricing for home 
            $check=$this->db->prepare("select * from services_prices where service_provider_service_id=:service_provider_service_id and price_for='home'");
            $check->bindParam(":service_provider_service_id",$service_id);
            $check->execute();
            $info=$check->fetch();
            $sp_id=$info['sp_id'];
            $dbress=$this->db->prepare("update services_prices set base_price=:base_price,sale_price=:sale_price,price_type=:price_type,same_time_slots_allowed=:same_time_slots_allowed,allowed_slots_count=:allowed_slots_count,per_day_slots_limit=:per_day_slots_limit where service_provider_service_id=:service_provider_service_id and sp_id=$sp_id");
            $dbress->bindParam(":service_provider_service_id",$service_id);
            $dbress->bindParam(":base_price",$home_base_price);
            $dbress->bindParam(":sale_price",$home_sale_price);
            $dbress->bindParam(":price_type",$home_price_type);
            $dbress->bindParam(":same_time_slots_allowed",$home_same_slot_time);
            $dbress->bindParam(":allowed_slots_count",$home_allowed_slots_same);
            $dbress->bindParam(":per_day_slots_limit",$home_day_limit_slots);
            $dbress->execute();
            $home_price_id=$sp_id;



          }

          $del=$this->db->prepare("delete from services_special_prices where sp_id=$sp_id and service_provider_service_id=:service_provider_service_id and price_for='home'");
          $del->bindParam(":service_provider_service_id",$service_id);
          $del->execute();

          if($home_price_type=='custom_price_weekend'){
  
            foreach($home_weekend_name as $custom_on){
                $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                $dbres->bindParam(":service_provider_service_id",$service_id);
                $dbres->bindParam(":sp_id",$home_price_id);
                $dbres->bindParam(":price_for",$service_for);
                $dbres->bindParam(":type",$home_price_type);
                $dbres->bindParam(":custom_on",$custom_on);
                $dbres->bindParam(":sales_price",$home_special_price);
                $dbres->execute();
            }

        }elseif($home_price_type=='custom_price_special_date'){
            $home_special_date=explode(",",$home_special_date);
            foreach($home_special_date as $custom_on){
                $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                $dbres->bindParam(":service_provider_service_id",$service_id);
                $dbres->bindParam(":sp_id",$home_price_id);
                $dbres->bindParam(":price_for",$service_for);
                $dbres->bindParam(":type",$home_price_type);
                $dbres->bindParam(":custom_on",$custom_on);
                $dbres->bindParam(":sales_price",$home_special_price);
                $dbres->execute();
            }
        }

      }elseif($service_for=='salonee'){
          
        
        $check1=$this->db->prepare("select * from services_prices where service_provider_service_id=:service_provider_service_id and price_for='home'");
        $check1->bindParam(":service_provider_service_id",$service_id);
        $check1->execute();
      
        $i=0;
        while($row1=$check1->fetch()){
          $i++;
          $sp_id=$row1['sp_id'];
          $del=$this->db->prepare("delete from services_special_prices where sp_id=$sp_id and service_provider_service_id=:service_provider_service_id and price_for='home'");
          $del->bindParam(":service_provider_service_id",$service_id);
          $del->execute();
        }
        if($i>0){
          $del=$this->db->prepare("delete from services_prices where service_provider_service_id=:service_provider_service_id and price_for='home'");
          $del->bindParam(":service_provider_service_id",$service_id);
          $del->execute();
        }

        
        $check=$this->db->prepare("select * from services_prices where service_provider_service_id=:service_provider_service_id and price_for='salonee'");
        $check->bindParam(":service_provider_service_id",$service_id);
        $check->execute();
        $check=$check->rowCount();

        if($check==0){

          $dbress=$this->db->prepare("insert into services_prices (service_provider_service_id,price_for,base_price,sale_price,price_type,same_time_slots_allowed,allowed_slots_count,per_day_slots_limit) values (:service_provider_service_id,:price_for,:base_price,:sale_price,:price_type,:same_time_slots_allowed,:allowed_slots_count,:per_day_slots_limit)");
          $dbress->bindParam(":service_provider_service_id",$service_id);
          $dbress->bindParam(":price_for",$service_for);
          $dbress->bindParam(":base_price",$salonee_base_price);
          $dbress->bindParam(":sale_price",$salonee_sale_price);
          $dbress->bindParam(":price_type",$salonee_price_type);
          $dbress->bindParam(":same_time_slots_allowed",$salonee_same_slot_time);
          $dbress->bindParam(":allowed_slots_count",$salonee_allowed_slots_same);
          $dbress->bindParam(":per_day_slots_limit",$salonee_day_limit_slots);
          $dbress->execute();
          $salonee_price_id=$this->db->lastInsertId();

        }else{

           //there is have pricing for salonee 
              
          $check=$this->db->prepare("select * from services_prices where service_provider_service_id=:service_provider_service_id and price_for='salonee'");
          $check->bindParam(":service_provider_service_id",$service_id);
          $check->execute();
           $info=$check->fetch();
           $salonee_price_id=$info['sp_id'];
           $dbress=$this->db->prepare("update services_prices set base_price=:base_price,sale_price=:sale_price,price_type=:price_type,same_time_slots_allowed=:same_time_slots_allowed,allowed_slots_count=:allowed_slots_count,per_day_slots_limit=:per_day_slots_limit where service_provider_service_id=:service_provider_service_id and sp_id=$salonee_price_id");
           $dbress->bindParam(":service_provider_service_id",$service_id);
           $dbress->bindParam(":base_price",$salonee_base_price);
           $dbress->bindParam(":sale_price",$salonee_sale_price);
           $dbress->bindParam(":price_type",$salonee_price_type);
           $dbress->bindParam(":same_time_slots_allowed",$salonee_same_slot_time);
           $dbress->bindParam(":allowed_slots_count",$salonee_allowed_slots_same);
           $dbress->bindParam(":per_day_slots_limit",$salonee_day_limit_slots);
           $dbress->execute();
           $salonee_price_id=$salonee_price_id;

        }

        $del=$this->db->prepare("delete from services_special_prices where sp_id=$sp_id and service_provider_service_id=:service_provider_service_id and price_for='salonee'");
        $del->bindParam(":service_provider_service_id",$service_id);
        $del->execute();

          if($salonee_price_type=='custom_price_weekend'){

              foreach($salonee_weekend_name as $custom_on){
                  $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                  $dbres->bindParam(":service_provider_service_id",$service_id);
                  $dbres->bindParam(":sp_id",$salonee_price_id);
                  $dbres->bindParam(":price_for",$service_for);
                  $dbres->bindParam(":type",$salonee_price_type);
                  $dbres->bindParam(":custom_on",$custom_on);
                  $dbres->bindParam(":sales_price",$salonee_special_price);
                  $dbres->execute();
              }

          }elseif($salonee_price_type=='custom_price_special_date'){
              $salonee_special_date=explode(",",$home_special_date);
              foreach($salonee_special_date as $custom_on){
                  $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                  $dbres->bindParam(":service_provider_service_id",$service_id);
                  $dbres->bindParam(":sp_id",$salonee_price_id);
                  $dbres->bindParam(":price_for",$service_for);
                  $dbres->bindParam(":type",$salonee_price_type);
                  $dbres->bindParam(":custom_on",$custom_on);
                  $dbres->bindParam(":sales_price",$salonee_special_price);
                  $dbres->execute();
              }
          }

      }elseif($service_for=='both'){

          //for home base 
          $service_for='home';
          
          $check=$this->db->prepare("select * from services_prices where service_provider_service_id=:service_provider_service_id and price_for='home'");
          $check->bindParam(":service_provider_service_id",$service_id);
          $check->execute();
          $check=$check->rowCount();

          if($check==0){
            //when there is no pricing for home
            $dbress=$this->db->prepare("insert into services_prices (service_provider_service_id,price_for,base_price,sale_price,price_type,same_time_slots_allowed,allowed_slots_count,per_day_slots_limit) values (:service_provider_service_id,:price_for,:base_price,:sale_price,:price_type,:same_time_slots_allowed,:allowed_slots_count,:per_day_slots_limit)");
            $dbress->bindParam(":service_provider_service_id",$service_id);
            $dbress->bindParam(":price_for",$service_for);
            $dbress->bindParam(":base_price",$home_base_price);
            $dbress->bindParam(":sale_price",$home_sale_price);
            $dbress->bindParam(":price_type",$home_price_type);
            $dbress->bindParam(":same_time_slots_allowed",$home_same_slot_time);
            $dbress->bindParam(":allowed_slots_count",$home_allowed_slots_same);
            $dbress->bindParam(":per_day_slots_limit",$home_day_limit_slots);
            $dbress->execute();
            $home_price_id=$this->db->lastInsertId();
  
          
          }else{
            //there is have pricing for home 
            $check=$this->db->prepare("select * from services_prices where service_provider_service_id=:service_provider_service_id and price_for='home'");
            $check->bindParam(":service_provider_service_id",$service_id);
            $check->execute();
            $info=$check->fetch();
            $sp_id=$info['sp_id'];
            $dbress=$this->db->prepare("update services_prices set base_price=:base_price,sale_price=:sale_price,price_type=:price_type,same_time_slots_allowed=:same_time_slots_allowed,allowed_slots_count=:allowed_slots_count,per_day_slots_limit=:per_day_slots_limit where service_provider_service_id=:service_provider_service_id and sp_id=$sp_id");
            $dbress->bindParam(":service_provider_service_id",$service_id);
            $dbress->bindParam(":base_price",$home_base_price);
            $dbress->bindParam(":sale_price",$home_sale_price);
            $dbress->bindParam(":price_type",$home_price_type);
            $dbress->bindParam(":same_time_slots_allowed",$home_same_slot_time);
            $dbress->bindParam(":allowed_slots_count",$home_allowed_slots_same);
            $dbress->bindParam(":per_day_slots_limit",$home_day_limit_slots);
            $dbress->execute();
            $home_price_id=$sp_id;



          }

          $del=$this->db->prepare("delete from services_special_prices where sp_id=$sp_id and service_provider_service_id=:service_provider_service_id and price_for='home'");
          $del->bindParam(":service_provider_service_id",$service_id);
          $del->execute();

          if($home_price_type=='custom_price_weekend'){
  
            foreach($home_weekend_name as $custom_on){
                $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                $dbres->bindParam(":service_provider_service_id",$service_id);
                $dbres->bindParam(":sp_id",$home_price_id);
                $dbres->bindParam(":price_for",$service_for);
                $dbres->bindParam(":type",$home_price_type);
                $dbres->bindParam(":custom_on",$custom_on);
                $dbres->bindParam(":sales_price",$home_special_price);
                $dbres->execute();
            }

        }elseif($home_price_type=='custom_price_special_date'){
            $home_special_date=explode(",",$home_special_date);
            foreach($home_special_date as $custom_on){
                $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                $dbres->bindParam(":service_provider_service_id",$service_id);
                $dbres->bindParam(":sp_id",$home_price_id);
                $dbres->bindParam(":price_for",$service_for);
                $dbres->bindParam(":type",$home_price_type);
                $dbres->bindParam(":custom_on",$custom_on);
                $dbres->bindParam(":sales_price",$home_special_price);
                $dbres->execute();
            }
        }
          
          //home base end


          //for salone bases

        $service_for='salonee';
        
        $check=$this->db->prepare("select * from services_prices where service_provider_service_id=:service_provider_service_id and price_for='salonee'");
        $check->bindParam(":service_provider_service_id",$service_id);
        $check->execute();
        $check=$check->rowCount();

        if($check==0){

          $dbress=$this->db->prepare("insert into services_prices (service_provider_service_id,price_for,base_price,sale_price,price_type,same_time_slots_allowed,allowed_slots_count,per_day_slots_limit) values (:service_provider_service_id,:price_for,:base_price,:sale_price,:price_type,:same_time_slots_allowed,:allowed_slots_count,:per_day_slots_limit)");
          $dbress->bindParam(":service_provider_service_id",$service_id);
          $dbress->bindParam(":price_for",$service_for);
          $dbress->bindParam(":base_price",$salonee_base_price);
          $dbress->bindParam(":sale_price",$salonee_sale_price);
          $dbress->bindParam(":price_type",$salonee_price_type);
          $dbress->bindParam(":same_time_slots_allowed",$salonee_same_slot_time);
          $dbress->bindParam(":allowed_slots_count",$salonee_allowed_slots_same);
          $dbress->bindParam(":per_day_slots_limit",$salonee_day_limit_slots);
          $dbress->execute();
          $salonee_price_id=$this->db->lastInsertId();

        }else{

           //there is have pricing for salonee 
           $check=$this->db->prepare("select * from services_prices where service_provider_service_id=:service_provider_service_id and price_for='salonee'");
           $check->bindParam(":service_provider_service_id",$service_id);
           $check->execute();
           $info=$check->fetch();
           $salonee_price_id=$info['sp_id'];
           $dbress=$this->db->prepare("update services_prices set base_price=:base_price,sale_price=:sale_price,price_type=:price_type,same_time_slots_allowed=:same_time_slots_allowed,allowed_slots_count=:allowed_slots_count,per_day_slots_limit=:per_day_slots_limit where service_provider_service_id=:service_provider_service_id and sp_id=$salonee_price_id");
           $dbress->bindParam(":service_provider_service_id",$service_id);
           $dbress->bindParam(":base_price",$salonee_base_price);
           $dbress->bindParam(":sale_price",$salonee_sale_price);
           $dbress->bindParam(":price_type",$salonee_price_type);
           $dbress->bindParam(":same_time_slots_allowed",$salonee_same_slot_time);
           $dbress->bindParam(":allowed_slots_count",$salonee_allowed_slots_same);
           $dbress->bindParam(":per_day_slots_limit",$salonee_day_limit_slots);
           $dbress->execute();
           $salonee_price_id=$salonee_price_id;

        }

        $del=$this->db->prepare("delete from services_special_prices where sp_id=$sp_id and service_provider_service_id=:service_provider_service_id and price_for='salonee'");
        $del->bindParam(":service_provider_service_id",$service_id);
        $del->execute();

          if($salonee_price_type=='custom_price_weekend'){

              foreach($salonee_weekend_name as $custom_on){
                  $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                  $dbres->bindParam(":service_provider_service_id",$service_id);
                  $dbres->bindParam(":sp_id",$salonee_price_id);
                  $dbres->bindParam(":price_for",$service_for);
                  $dbres->bindParam(":type",$salonee_price_type);
                  $dbres->bindParam(":custom_on",$custom_on);
                  $dbres->bindParam(":sales_price",$salonee_special_price);
                  $dbres->execute();
              }

          }elseif($salonee_price_type=='custom_price_special_date'){
              $salonee_special_date=explode(",",$home_special_date);
              foreach($salonee_special_date as $custom_on){
                  $dbres=$this->db->prepare("insert into services_special_prices (service_provider_service_id,sp_id,price_for,type,custom_on,sales_price) values (:service_provider_service_id,:sp_id,:price_for,:type,:custom_on,:sales_price)");
                  $dbres->bindParam(":service_provider_service_id",$service_id);
                  $dbres->bindParam(":sp_id",$salonee_price_id);
                  $dbres->bindParam(":price_for",$service_for);
                  $dbres->bindParam(":type",$salonee_price_type);
                  $dbres->bindParam(":custom_on",$custom_on);
                  $dbres->bindParam(":sales_price",$salonee_special_price);
                  $dbres->execute();
              }
          }
          
          //for salonee bases end
      }

      $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully service price added"));
      redirect($referer);
      exit;

    }

    public function getSaloneeTimings(){
      $dbres=$this->db->prepare("select * from service_provider_timings where service_provider_id=:uid");
      $dbres->bindParam(":uid",$this->user_id);
      $dbres->execute();
      return $dbres->fetchAll();
    }
    public function updateSaloneeTimings(){
      $from_time=$this->input->post("from_time",true);
      $to_time=$this->input->post("to_time",true);
      $available_status=$this->input->post("available_status",true);
      $referer=$_SERVER['HTTP_REFERER'];

      $dbres=$this->db->prepare("delete from service_provider_timings where service_provider_id=:uid");
      $dbres->bindParam(":uid",$this->user_id);
      $dbres->execute();
      $days=array("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday");
      for($i=0;$i<7;$i++){
        if(!isset($from_time[$i])){
          $from_time[$i]='00:00';
        }
        if(!isset($to_time[$i])){
          $to_time[$i]='00:00';
        }
        if(!isset($available_status[$i])){
          $available_status[$i]=0;
        }
        $dbres=$this->db->prepare("insert into service_provider_timings ( service_provider_id ,day_name,from_time,to_time,available_status) values (:uid,:day_name,:from_time,:to_time,:available_status)");
        $dbres->bindParam(":uid",$this->user_id);
        $dbres->bindParam(":day_name",strtolower($days[$i]));
        $dbres->bindParam(":from_time",$from_time[$i]);
        $dbres->bindParam(":to_time",$to_time[$i]);
        $dbres->bindParam(":available_status",$available_status[$i]);
        $dbres->execute();
      }
      
      $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully salonee timings updated"));
      redirect($referer);
      exit;
    }
}