<?php
require_once dirname(__DIR__).'/core/Controller.php';

class Salesperson extends Controller{
    public $admin_id;
    public $auth_type;
    public function __construct(){
        parent::__construct();
        $this->admin_id=$this->session->userdata("adminId");
        $this->auth_type=$this->session->userdata("auth_type");
        if($this->admin_id==''){
            redirect("index.php");
            exit;
        }
        if($this->auth_type!='admin'){
            session_destroy();
            redirect("index.php");
            exit;
        }
        if(!$this->getAdminPageAccess("salespersons")){
            $this->session->set_userdata(array("alert_type"=>"error","alert_msg"=>"Sorry. You don\'t have access."));
            redirect(admin_base_url."dashboard.php");
            exit;
        }
    }
    public function load(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing

            $type=$this->input->post("type",true);
            if($type=='addSalesPerson'){
                $this->addSalesPerson();
            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='active_user'){
                $this->activateUser();
            }elseif($type=='block_user'){
                $this->deactivateUser();
            }elseif($type=='send_password'){
                $this->sendUserPassword();
            }elseif($type=='active_user1'){
                $this->activateUser1();
            }elseif($type=='block_user1'){
                $this->deactivateUser2();
            }
        }
    
    }
    public function activateUser(){
        $id=$this->input->get("id",true);
        $dbres=$this->db->prepare("update salesperson set status=1 where salesperson_id=:id");
        $dbres->bindParam(":id",$id);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully salesperson Activated"));
        header("location:".admin_base_url."salesperson.php");
        exit;

    }
    public function deactivateUser(){
        $id=$this->input->get("id",true);
        $dbres=$this->db->prepare("update salesperson set status=0 where salesperson_id=:id");
        $dbres->bindParam(":id",$id);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully salesperson Deactivated"));
        header("location:".admin_base_url."salesperson.php");
        exit;
    }
    public function getCities(){
        $cities=$this->db->prepare("select * from cities where country_id=784 order by city_name_en asc");
        $cities->execute();
        $resp=array();
        while($row=$cities->fetch()){
            $resp[]=$row;
        }
        return $resp;
    }

    public function addSalesPerson(){
        $name=$this->input->post("name",true);
        $email=$this->input->post("email",true);
        $location=$this->input->post("location",true);
        $mobile=$this->input->post("mobile",true);
        if($email==''){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Required"));
            header("location:".admin_base_url."salesperson.php");
            exit;
        }
        $che=$this->db->prepare("select email from salesperson where email=:email");
        $che->bindParam(":email",$email);
        $che->execute();
        $count=$che->rowCount();
        if($count>0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Email Exist"));
            header("location:".admin_base_url."salesperson.php");
            exit;
        }

        if(strpos($email,"@")==false){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Invalid Email"));
            header("location:".admin_base_url."salesperson.php");
            exit;
        }
        $che=$this->db->prepare("select mobile from salesperson where mobile=:mobile");
        $che->bindParam(":mobile",$mobile);
        $che->execute();
        $count=$che->rowCount();
        if($count>0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Mobile Number Exist"));
            header("location:".admin_base_url."salesperson.php");
            exit;
        }
        if($_FILES["image"]["size"]>0){
            $tmp_name = $_FILES["image"]["tmp_name"];
            $file='uploads/admin/'.basename($_FILES["image"]["name"]);
            $path='uploads/admin/'.basename($_FILES["image"]["name"]);
            move_uploaded_file($tmp_name,$path);
        }else{
            $file='uploads/admin/default-image.png';
        }
     
        $country_code=971;
        $password=$this->randomPassword(6);
        $dbres=$this->db->prepare("insert into salesperson (name,email,mobile,password,location,image,status,country_code) values (:name,:email,:mobile,:password,:location,:image,1,:country_code)");
        $dbres->bindParam(":name",$name);
        $dbres->bindParam(":email",$email);
        $dbres->bindParam(":mobile",$mobile);
        $dbres->bindParam(":password",$password);
        $dbres->bindParam(":location",$location);
        $dbres->bindParam(":image",$file);
        $dbres->bindParam(":country_code",$country_code);
        $dbres->execute();

        $sms_msg="Your salonee salesperson account created your login details email ".$email." and password ".$password."";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);


        $message='';
        $message.='Hi Welcome to Salonee <br/>';
        $message.='Thanking you for registered with us<br/>';
        $message.='Your login details : <br/>';
        $message.='Email : '.$email.'<br/>';
        $message.='Password : '.$password.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Thank you for register us",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully salesperson added"));
           
        header("location:".admin_base_url."salesperson.php");
        exit;
    }
    public function getSalesPersons(){
        $persons=$this->db->prepare("select (select count(id) from salespersons_service_providers where salesperson_id=s.salesperson_id) as total_services_providers_count,s.*,if(s.location!='',(select city_name_en from cities where id=s.location),'') as city_name from salesperson s order by created_time desc");
        $persons->execute();
        $resp=array();
        while($row=$persons->fetch()){
            $resp[]=$row;
        }
        return $resp;
    }
    public function pageData(){
        $data=array();
        $data['title']='Admin | Sales Persons ';
        $data['page']='salespersons';
        $data['admin_id']=$this->admin_id;
        
    

        return $data;
    }
    public function pageViewSalespersonData(){
        $data=array();
        $data['title']='Admin | View Sales Person ';
        $data['page']='salespersons';
        $data['admin_id']=$this->admin_id;
        $uid=$this->input->get("id",true);

        $total_sb=$this->db->prepare("select s.id from salespersons_service_providers s ,service_provider sp where s.service_provider_id=sp.service_provider_id and salesperson_id=:uid and sp.status=1");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['active_sp']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select s.id from salespersons_service_providers s ,service_provider sp where s.service_provider_id=sp.service_provider_id and salesperson_id=:uid ");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['total_sp']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select s.id from salespersons_service_providers s ,service_provider sp where s.service_provider_id=sp.service_provider_id and salesperson_id=:uid and sp.status=0");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['pending_sp']=$total_sb->rowCount();

        $total_sb=$this->db->prepare("select s.id from salespersons_service_providers s ,service_provider sp where s.service_provider_id=sp.service_provider_id and salesperson_id=:uid and sp.status=3");
        $total_sb->bindParam(":uid",$uid);
        $total_sb->execute();
        $data['rejected_sp']=$total_sb->rowCount();

        return $data;
    }

    
    public function sendUserPassword(){
        $cid=$this->input->get("id",true);
        $che=$this->db->prepare("select * from salespersons where salesperson_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();
        $email=$user['email'];
        $name=$user['name'];
        $password=$user['password'];

        $sms_msg="your salonee salesperson account login password is ".$password."";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account login password is '.$password.'<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account login password.",$message);
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully User password has been to registered email."));
        redirect(admin_base_url.'view-salesperson.php?id='.$cid);
        exit;
    }
    public function activateUser1(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("id",true);
        if(strpos($referer,"view-salesperson")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$cid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $dbres=$this->db->prepare("update salesperson set status=1 where salesperson_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully salesperson activated "));
        $che=$this->db->prepare("select * from salesperson where salesperson_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();

        $sms_msg="your salonee salesperson account activated";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $email=$user['email'];
        $name=$user['name'];
        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been activated.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account resumed.",$message);
        redirect($referer);
        exit;
    }
    public function deactivateUser2(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("id",true);
        if(strpos($referer,"view-salesperson")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
            $referer=$referer."?id=".$cid;
        }elseif(strpos($referer,"?")!==false){
            $referer=explode("?",$referer);
            $referer=$referer[0];
        }
        $dbres=$this->db->prepare("update salesperson set status=2 where salesperson_id=:id");
        $dbres->bindParam(":id",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully Salesperson deactivated "));
        $che=$this->db->prepare("select * from salesperson where salesperson_id=:id");
        $che->bindParam(":id",$cid);
        $che->execute();
        $user=$che->fetch();
        $email=$user['email'];
        $name=$user['name'];

        $sms_msg="your salonee salesperson account has been blocked. please contact us.";
        $mobile=$user['mobile'];
        $country_code=$user['country_code'];
        $phone=$country_code.$mobile;
        $this->sendSMS($phone,$sms_msg);

        $message='';
        $message.='Hi '.$name.'<br/>';
        $message.='Your account has been blocked. please contact us.<br/>';
        $message.='<br/>Thanking you';
        $this->phpmailer->sendMail($email,"Your account blocked.",$message);
        redirect($referer);
        exit;
    }

    public function getServiceProvidersSalesperson($id){
        $dbres=$this->db->prepare("select * from service_provider s where s.service_provider_id in (select service_provider_id from salespersons_service_providers where salesperson_id=:id) ORDER BY s.service_provider_id DESC");
        $dbres->bindParam(":id",$id);
        $dbres->execute();
        return $dbres;
    }

}