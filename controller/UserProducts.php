<?php
require_once dirname(__DIR__).'/core/Controller.php';

class UserProducts extends Controller{
    public $user_id;
    public $branch_type;
    public function __construct(){
        parent::__construct();
        $this->user_id=$this->session->userdata("serviceProviderId");
        $this->branch_type=$this->session->userdata("branch_type");
        if($this->user_id==''){
            session_destroy();
            redirect("index.php");
            exit;
        }
        
    }
    public function init(){
        if($_SERVER['REQUEST_METHOD']=='POST'){
            //post routing
            $type=$this->input->post("type",true);
            if($type=='addServiceProvider'){
                $this->addServiceProvider();
            }elseif($type=='addNewProduct'){
                $this->addNewProduct();

            }elseif($type=='getEditProduct'){
                $this->getEditProduct();
            }elseif($type=='editProduct'){
                $this->updateProduct();

            }
        }else{
            //get routing
            $type=$this->input->get("type",true);
            if($type=='disableProduct'){
                $this->disableProduct();
            }elseif($type=='enableProduct'){
                $this->enableProduct();
            }
        }
    }
    public function disableProduct(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("pid",true);
        $dbres=$this->db->prepare("update products set product_status='inactive' where id=:sid and service_provider_id =:id");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->bindParam(":sid",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully product disabled"));
        redirect($referer);

        exit;
    }
    public function enableProduct(){
        $referer=$_SERVER['HTTP_REFERER'];
        $cid=$this->input->get("pid",true);
        $dbres=$this->db->prepare("update products set product_status='active' where id=:sid and service_provider_id =:id");
        $dbres->bindParam(":id",$this->user_id);
        $dbres->bindParam(":sid",$cid);
        $dbres->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully product activated"));
        redirect($referer);

        exit;
    }
    public function pageData(){
        $data=array();
        $data['title']='Service Provider | Products';
        $data['page']='products';
        $data['user_id']=$this->user_id;
        return $data;
    }

    public function getCategories(){
        $cate=$this->db->prepare("select c.* from category c ,service_provider_categories sp where c.category_id=sp.category_id and sp.service_provider_id=:uid order by c.name asc");
        $cate->bindParam(":uid",$this->user_id);
        $cate->execute();
        return $cate->fetchAll();
    }
    public function getProducts(){
        $cate=$this->db->prepare("select p.*,if(p.category_id!=0,(select name from category where category_id=p.category_id),'For All') as category_name from products p where p.service_provider_id=:uid order by p.created_time asc");
        $cate->bindParam(":uid",$this->user_id);
        $cate->execute();
        return $cate->fetchAll();
    }
    public function addNewProduct(){
        $referer=$_SERVER['HTTP_REFERER'];
        $product_name=$this->input->post("product_name",true);
        $category=$this->input->post("category",true);
        $base_price=$this->input->post("base_price",true);
        $sale_price=$this->input->post("sale_price",true);
        $description=$this->input->post("description",true);
        $tmp_name = $_FILES["upload_cont_img"]["tmp_name"];
        $path='uploads/products/'.basename($_FILES["upload_cont_img"]["name"]);
        move_uploaded_file($tmp_name,$path);
        $image=$path;
        if($this->getMemberAccess()==0){
            $this->session->set_userdata(array("alert_type"=>"info","alert_msg"=>"Please upgrade now to access."));
            redirect($referer);
            exit;
        }
        $dbress=$this->db->prepare("insert into products (product_title,service_provider_id  ,product_description,base_price,sale_price,product_status,product_image,category_id) values (:product_title,:service_provider_id,:product_description,:base_price,:sale_price,'active',:product_image,:category_id)");
        $dbress->bindParam(":service_provider_id",$this->user_id);
        $dbress->bindParam(":product_title",$product_name);
        $dbress->bindParam(":product_description",$description);
        $dbress->bindParam(":base_price",$base_price);
        $dbress->bindParam(":sale_price",$sale_price);
        $dbress->bindParam(":product_image",$image);
        $dbress->bindParam(":category_id",$category);
        $dbress->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully product added"));
        redirect($referer);
    }

    public function updateProduct(){
        $referer=$_SERVER['HTTP_REFERER'];
        $pid=$this->input->post("pid",true);
        $product_name=$this->input->post("product_name",true);
        $category=$this->input->post("category",true);
        $base_price=$this->input->post("base_price",true);
        $sale_price=$this->input->post("sale_price",true);
        $description=$this->input->post("description",true);

        $pro=$this->db->prepare("select * from products where id=:pid and service_provider_id=:uid");
        $pro->bindParam(":uid",$this->user_id);
        $pro->bindParam(":pid",$pid);
        $pro->execute();
        $pro=$pro->fetch();

        if($_FILES["upload_cont_img"]["size"]>0){
            $tmp_name = $_FILES["upload_cont_img"]["tmp_name"];
            $path='uploads/products/'.basename($_FILES["upload_cont_img"]["name"]);
            move_uploaded_file($tmp_name,$path);
            $image=$path;
        }else{
            $image=$pro['product_image'];
        }
       
        $dbress=$this->db->prepare("update products set product_title=:product_title,product_description=:product_description,base_price=:base_price,sale_price=:sale_price,product_image=:product_image,category_id=:category_id where id=:pid");
        $dbress->bindParam(":product_title",$product_name);
        $dbress->bindParam(":product_description",$description);
        $dbress->bindParam(":base_price",$base_price);
        $dbress->bindParam(":sale_price",$sale_price);
        $dbress->bindParam(":product_image",$image);
        $dbress->bindParam(":category_id",$category);
        $dbress->bindParam(":pid",$pid);
        $dbress->execute();
        $this->session->set_userdata(array("alert_type"=>"success","alert_msg"=>"Successfully product updated"));
        redirect($referer);
    }
    public function getEditProduct(){
        $pid=$this->input->post("pid",true);   
        $pro=$this->db->prepare("select * from products where id=:pid and service_provider_id=:uid");
        $pro->bindParam(":uid",$this->user_id);
        $pro->bindParam(":pid",$pid);
        $pro->execute();
        $pro=$pro->fetch();
        ?>
        <input type="hidden" name="pid" value="<?php echo $pid;?>" />
        <div class="col-md-12">

            <div class="form-group ">
            <input type="text" id="product_name" name="product_name" value="<?php echo $pro['product_title'];?>" class="form-control" autocomplete="off" required>
            <label class="form-control-placeholder p-0" for="product_name">Product Name</label>
            </div>
            </div>
            <div class="col-md-12">
            <div class="form-group">
            <select name="category" class="form-control" autocomplete="off" required >
            <option hidden   value> -- Select Category  -- </option>
            <option value="0" <?php echo ($pro['category_id']==0)?"selected":"";?>>For All Categories</option>
            <?php foreach($this->getCategories() as $cat){ 
                if($pro['category_id']==$cat['category_id']){
                    echo  '<option value="'.$cat['category_id'].'" selected>For '.$cat['name'].' Only</option>';  
                }else{
                    echo  '<option value="'.$cat['category_id'].'">For '.$cat['name'].' Only</option>'; 
                }
            }
                ?>
            </select>
            </div>
            </div>

            <div class="col-md-12">
            <div class="form-group ">
            <input type="text" id="base_price " name="base_price" value="<?php echo $pro['base_price'];?>" class="form-control" autocomplete="off" required>
            <label class="form-control-placeholder p-0" for="base_price">Base Price</label>
            </div>
            </div>
            <div class="col-md-12">
            <div class="form-group ">
            <input type="text" id="sale_price " name="sale_price" value="<?php echo $pro['sale_price'];?>" class="form-control" autocomplete="off" required>
            <label class="form-control-placeholder p-0" for="sale_price">Sale Price</label>
            </div>
            </div>
            <div class="col-md-12">
            <div class="form-group ">
            <textarea type="text" id="description " name="description" class="form-control" autocomplete="off" required><?php echo $pro['product_description'];?></textarea>
            <label class="form-control-placeholder p-0" for="description">Description</label>
            </div>
            </div>
            <div class="col-md-12">
            <div class="form-group">
            <label for="file-upload3" class="custom-file-upload">
            UPLOAD IMAGE <i class="fa fa-camera" aria-hidden="true"></i>
            </label>
            <input id="file-upload3" name='upload_cont_img' type="file" style="display:none;">
            </div>
            </div>
            <?php 
    exit;
    }
}