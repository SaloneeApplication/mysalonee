<?php include('header.php');

$categoriesWomen = $funcObject->categoriesListWomen($con);
$categoriesMen = $funcObject->categoriesListMen($con);
$categoriesChildren = $funcObject->categoriesListChildren($con);

?>
<div class="container-fluid">
    <div class="_header"></div>
    <nav aria-label="breadcrumb" class="_custmBrdcrmb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href='<?php echo base_url?>'>Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Categories</li>
        </ol>
    </nav>

    <section class="categories">
        <div class="theHdng">
            <h2>Services</h2>
        </div>

        <h3 class="subHdng">Services For Women</h3>

        <ul class="category_item">
            <div class="category-slider owl-carousel">
                <?php
                while($row = mysqli_fetch_array($categoriesWomen))
                {?>
                    <li>
                        <div class="img_circle">
                           
                            <img src="<?php echo ADMIN_URL.$row['image'];?>" alt="Category-Image"/>
                            <a href="services.php?category_id=<?php echo $row['category_id'];?>" class="theme-btn">Book</a>
                        </div>
                        <p><?php echo $row['name'];?></p>
                    </li>
                <?php
                }?>
            </div>
        </ul>
        <br>

        <h3 class="subHdng">Services For Men</h3>

        <ul class="category_item">
            <div class="category-slider owl-carousel">
                <?php
                while($row = mysqli_fetch_array($categoriesMen))
                {?>
                    <li>
                        <div class="img_circle">
                            <img src="<?php echo ADMIN_URL.$row['image'];?>" alt="Category-Image"/>
                            <a href="services.php?category_id=<?php echo $row['category_id'];?>" class="theme-btn">Book</a>
                        </div>
                        <p><?php echo $row['name'];?></p>
                    </li>
                <?php
                }?>
            </div>
        </ul>
        <br>

        <h3 class="subHdng">Services For Children</h3>

        <ul class="category_item">
            <div class="category-slider owl-carousel">
                <?php
                while($row = mysqli_fetch_array($categoriesChildren))
                {?>
                    <li>
                        <div class="img_circle">
                            <img src="<?php echo ADMIN_URL.$row['image'];?>" alt="Category-Image"/>
                            <a href="services.php?category_id=<?php echo $row['category_id'];?>" class="theme-btn">Book</a>
                        </div>
                        <p><?php echo $row['name'];?></p>
                    </li>
                <?php
                }?>
            </div>
        </ul>
    </section>   
</div>
 <?php include('footer.php');?>
 <script>
	$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false
        },
		768:{
            items:3,
            nav:false
        },
		1024:{
            items:4,
            nav:false
        },
        1200:{
            items:6,
            nav:true,
            loop:false
        }
    }
})
</script>
