<?php 
include('header.php'); 
$page = 'change-password';

$user_id = @$_SESSION['user_id'];

if($user_id == "")
{
    echo '<script> var base_url = "http://localhost/salonee_web/"; </script>';
    echo '<script> window.location.replace(base_url); </script>';
}

?>

<div class="container-fluid">
  <div class="_header"></div>
  <!-- breadcrumb  -->
  <nav aria-label="breadcrumb" class="_custmBrdcrmb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">My Account</a></li>
      <li class="breadcrumb-item active" aria-current="page">Change Password</li>
    </ol>
  </nav>

<div class="d-flex myFlex">
  <?php include('sidebar.php');?>

    <div class="mainDiv _bgWyt">
        <div class="wdthLmt">
            <div class="login">
                <form id="changePswdForm">
                    <div class="form-group mt-4">
                        <input type="password" id="oldPswd" name="oldPswd" class="form-control" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="op">Old Password</label>
                        <span toggle="#oldPswd" class="fa fa-eye field-icon toggle-password"></span>
                    </div>
                    <div class="form-group mt-4">
                        <input type="password" id="newPswd" name="newPswd" class="form-control" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="np">New Password</label>
                        <span toggle="#newPswd" class="fa fa-eye field-icon toggle-password"></span>
                    </div>
                    <div class="form-group mt-4">
                        <input type="password" id="cnfrmPswd" name="cnfrmPswd" class="form-control" autocomplete="off" required>
                        <label class="form-control-placeholder p-0" for="cp">Confirm Password</label>
                        <span toggle="#cnfrmPswd" class="fa fa-eye field-icon toggle-password"></span>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn theme-btn" id="saveBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div> <!--end Main Div-->


</div>
<?php include('footer.php');?>

<script type="text/javascript">

    // LOGIN FORM VALIDATIONS
    $("#changePswdForm").validate({
      // Specify the validation rules
      rules: {
        oldPswd: {
          required: true,
        },
        newPswd: {
          required: true,
          minlength: 8,
          maxlength: 15,
        },
        cnfrmPswd: {
          equalTo : "#newPswd"
        },
      },
      // Specify the validation error messages
      messages: {
        oldPswd: {
          required: "Please Enter Old Password",
        },
        newPswd: {
          required: "Please Enter New Password",
          minlength: "Password must be minimum 8 characters",
          maxlength: "Maximum 15 characters only",
        },
        newPswd: {
          required: "Please Confirm New Password",
          minlength: "Password must be minimum 8 characters",
          maxlength: "Maximum 15 characters only",
        },
      },
    });

    $("#saveBtn").click(function () {  

        var user_id = "<?php echo $user_id;?>";
        var new_password = $("#newPswd").val();
        var conf_password = $("#cnfrmPswd").val();
        var old_password = $("#oldPswd").val();
        var formData = new FormData();

        formData.append('user_id', user_id);
        formData.append('new_password', new_password);
        formData.append('conf_password', conf_password);
        formData.append('old_password', old_password);
        $.ajax({
            type:'POST',
            url:'controllers/change_password.php',
            data:formData,
            contentType: false,
            processData: false,
            success:function(res){
                var res = JSON.parse(res);
                var data = res.data;
                if(data.status == 1)
                {
                    swal({
                        type: "success",
                        text: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setInterval('location.reload()', 1500); 
                }
                else
                {
                    swal({
                        type: "error",
                        text: data.message,
                        showConfirmButton: true,
                        timer: 1500
                    });
                    //setInterval('location.reload()', 1500);
                }                               
            }
        });
    });
</script>