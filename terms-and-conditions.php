<?php

include('header.php');
$content = $funcObject->privacyPrivacy($con);

while($row = mysqli_fetch_array($content))
{
    $terms_condition = $row['terms_condition'];
}

?>
<style>
    .card-terms {
        padding: 30px;
        text-align: justify;
    }
    .card-terms tt {
        color: #d06690;
    }
    .card-terms ul {
        display: block !important;
        list-style-type: disc !important;
    }
    .card-terms ul li {
        max-width: 100% !important;
        display: list-item !important;
    }
</style>
<div class="container-fluid">
    <div class="_header"></div>
    <div class="container p-0">
        <nav aria-label="breadcrumb" class="_custmBrdcrmb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Terms and Conditions</li>
            </ol>
        </nav>
    </div>

    <div class="nearShops _nrShops">
        <div class="container">
            <div class="card card-terms">
                <?php echo $terms_condition; ?>
            </div>
        </div>
    </div>
</div>
<?php 
include('footer.php');
?>
