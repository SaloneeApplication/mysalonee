<?php 
	
	class dbConnection
	{
		private $server = "localhost";
        private $user = "root";
        private $password = "";
        private $database = "salonee_staging";
		
		/*private $server = "165.22.213.80";
		private $user = "admin_saloon";
		private $password = "oZQhVJJT71";
		private $database = "admin_saloon";*/
		
		public function getConnection()
		{
			$con = mysqli_connect($this->server,$this->user,$this->password,$this->database);
			return $con;
		}
		public function get_query($string,$single=false,$array=false){
			$con = $this->getConnection();
			$stmt = $con->prepare($string);
			$stmt->execute();
			$result = $stmt->get_result();
			$value = $result->fetch_object();
			return $value;
		}
	}
?>