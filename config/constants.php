<?php
ob_start();
@session_start();
define("BASEPATH","Core");
define("ENVIRONMENT","production");
define("UTF8_ENABLED",false);
define("ROOT",realpath('../'));
define("UPLOAD_PATH","uploads/");
if($_SERVER['HTTP_HOST']=='localhost'){
    @define("base_url","http://localhost/saloon/mysalonee");
    @define("admin_base_url","http://localhost/saloon/mysalonee/salonee_admin/");
    @define("user_base_url","http://localhost/saloon/mysalonee/");
}else{
   @define("base_url",$_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."/service_provider/");
        @define("admin_base_url",$_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."/salonee_admin/");
        @define("user_base_url",$_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."/service_provider/");
   
}

define("db_server","localhost");
define("db_username","root");
define("db_password","");
define("db_database","salonee_staging");

define('BASE_URL', $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'].'/');
define('API_URL', 'http://localhost/salonee_api/');
define('ADMIN_URL', $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."/salonee_admin/");
// define('ADMIN_URL', $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."/service_provider/");

$_SESSION['lang'] = 'English'; //default language