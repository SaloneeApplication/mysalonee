<?PHP
// defined('BASEPATH') OR exit('No direct script access allowed');
if($_SERVER['HTTP_HOST']=='localhost'){
    $config['base_url'] = 'http://localhost/saloon/';
}else{
    if(strpos($_SERVER['HTTP_HOST'],"checkinfo")!==false){
        $config['base_url'] = 'http://salonee.checkinfo.co/';
    }else{
        $config['base_url'] = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."/";
        
    }
}
$config['uri_protocol']	= 'REQUEST_URI';
$config['url_suffix'] = '';
$config['charset'] = 'UTF-8';
$config['subclass_prefix'] = 'MY_';
$config['permitted_uri_chars'] = 'a-z 0-9~%.:_\-=@$';
$config['allow_get_array'] = TRUE;
$config['encryption_key'] = 'hellow_test_world';
$config['sess_driver'] = 'files';
$config['sess_cookie_name'] = 'salonee';
$config['sess_expiration'] = 7200;
$config['sess_save_path'] = NULL;
$config['sess_match_ip'] = FALSE;
$config['sess_time_to_update'] = 300;
$config['sess_regenerate_destroy'] = FALSE;
$config['cookie_prefix']	= '';
$config['cookie_domain']	= '';
$config['cookie_path']		= '/';
$config['cookie_secure']	= FALSE;
$config['cookie_httponly'] 	= FALSE;
$config['standardize_newlines'] = FALSE;
$config['global_xss_filtering'] = FALSE;
$config['csrf_protection'] = FALSE;
$config['csrf_token_name'] = 'salonee';
$config['csrf_cookie_name'] = 'salonee';
$config['csrf_expire'] = 7200;
$config['csrf_regenerate'] = TRUE;
$config['csrf_exclude_uris'] = array();
$config['compress_output'] = TRUE;
$config['time_reference'] = 'Asia/Kolkata';
$config['rewrite_short_tags'] = FALSE;
$config['proxy_ips'] = '';


// $config['smtp_host']='mail.websitetool.in';
// $config['smtp_from_mail']='test@websitetool.in';
// $config['smtp_from_title']='Salonee';
// $config['smtp_username']='test@websitetool.in';
// $config['smtp_password']='test';
// $config['smtp_port']=587;
// $config['smtp_status']=false;